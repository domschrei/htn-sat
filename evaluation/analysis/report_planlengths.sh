#!/bin/bash

if [ ! $1 ]; then
    echo "Please provide a destination directory."
    exit
fi
dir=$1

reportfile=$dir/report_planlengths.csv
> $reportfile

olddir=`pwd`
for d in $dir/*/; do
    
    if [ -f $d/args ]; then
        problem=`cat $d/args`
        problem=`echo $problem|sed 's/ -i /_/g'`
        
        if grep -q "actions in the plan." $d/00_log.txt ; then
            # Madagascar
            planlength=`grep "actions in the plan." $d/00_log.txt|grep -oE "^[0-9]+"`
            echo "$problem $planlength" >> $reportfile
        elif [ -f $d/04_solution.txt ]; then
            # HTN-SAT encodings
            planlength=`cat $d/04_solution.txt|grep -E "^[0-9]+.*:.*"|grep -v "(nop)"|wc -l`
            echo "$problem $planlength" >> $reportfile
        elif [ -f $d/general_output ]; then
            # GTOHP
            if grep -q "Found plan as follows:" $d/general_output ; then
                planlength=`cat $d/general_output|grep -oE "^( +)[0-9]+: \("|wc -l`
                echo "$problem $planlength" >> $reportfile
            fi
        else
            echo "$problem -1" >> $reportfile
        fi
    fi
done

sort $reportfile -o $reportfile
