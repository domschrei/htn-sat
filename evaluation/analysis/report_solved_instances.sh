#!/bin/bash

if [ ! $1 ]; then
    echo "Please provide a destination directory."
    exit
fi
dir=$1

timeout_seconds=`cat $dir/timeout_seconds`

cd $dir
cat report_times.csv |awk '{print $NF}'> runtimes
paste runtimes report_times.csv > report_times_runtime1st.csv

for mode in `cat report_times_runtime1st.csv|awk '{print $4}'|sort -u|tr -d ' '|tr '\n' ' '`; do 
    #echo $mode
    file=solved_instances_$mode #`echo $mode|tr ' ' '_'`
    grep " $mode " report_times_runtime1st.csv|sort -g|grep -v ${timeout_seconds}000|awk '{print $1}' > $file
done

rm runtimes report_times_runtime1st.csv
