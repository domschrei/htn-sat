#!/usr/bin/env python3

import sys
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import re
import math
import sys
from matplotlib.ticker import MaxNLocator
import os.path

# The domain of the problem which is being plotted.
# It must be the same for all read data points.
domain = None

property = None
timeout_seconds = 99999999

"""
Reads a data file.
@param datafile (string) path to the file
"""
def read_datafile(datafile):

    global domain

    # Structure for values 
    # Key: competitor label, Value: list of index-result pairs
    values = dict()

    # Read data file line by line
    f = open(datafile, 'r')
    for l in f.readlines():
        
        # Clean line and separate by whitespace
        l = l.replace(" \n", "")
        l = l.replace("\n", "")
        words = l.split(" ")
        
        # Valid data line?
        if len(words) >= 4:
            
            # Raw values
            problem_domain = words[0]
            problem_idx = words[1]
            competitor_label = words[2] #" ".join(words[2:len(words)-1])
            result = float(words[-1]) #sum([float(v) for v in words[3:]])
            
            if domain is None:
                domain = problem_domain
                print("Domain: " + str(domain))
                
            if domain == problem_domain:
                # Add (index, result) pair of this line
                if competitor_label not in values:
                    values[competitor_label] = []
                values[competitor_label] += [[int(problem_idx), float(result)]]
                
            else:
                print("Warning: There are differing domains in the logging directories.")
                print("Only considering the domain \"" + domain + "\".")
    
    return values


def reorder_values(values):
    
    reordering = open("reordering_" + str(domain) + ".csv", 'r').readlines()[0].split(" ")
    for i in range(len(reordering)):
        if reordering[i]:
            reordering[i] = int(reordering[i])
    
    for label in values:
        competitor_values = values[label]
        for value_pair in competitor_values:
            value_pair[0] = int(reordering.index(value_pair[0])+1)


"""
Expects a dictionary as returned by read_datafile.

Returns a pair (cleaned_values, competitor_labels).
  cleaned_values is a list of [X, Y] pairs whereas X is a list of x-values 
    (problem idx) and Y is a list of corresponding y-values (performance value).
  competitor_labels is a list of strings corresponding to the label of the
    competitor with the X and Y results of the same index.
"""
def clean_values(values, timeout_seconds):
    
    cleaned_values = []
    competitor_labels = []
    
    for competitor in values:
        
        # Add competitor label
        competitor_labels += [competitor]
        
        # Which x values (problem instances) occur?
        x_vals = set()
        for [x, y] in values[competitor]:
            x_vals.add(x)

        # Calculate mean y value for each present x value
        X = []
        Y = []
        for x in x_vals:
            avg = np.mean([yval for [xval,yval] in values[competitor] if xval == x])
            X += [x]
            if property == "time":
                avg /= 1000
                if avg < timeout_seconds:
                    Y += [avg]
                else:
                    Y += [float('nan')]
            elif property == "planlength":
                if avg == -1:
                    Y += [float('nan')]
                else:
                    Y += [avg]
        cleaned_values += [[X, Y]]

    return (cleaned_values, competitor_labels)


"""

"""
def plot(cleaned_values, competitor_labels, colors_of_competitors, directory=".", output_file=None, logscale=False, excluded_competitors=[], display_size=3):
    
    if display_size == 1:
        ax = plt.figure(figsize=(4, 3)).gca()
    elif display_size == 2:
        ax = plt.figure(figsize=(5, 3.75)).gca()
    else:
        ax = plt.figure().gca()
    colors = ['#377eb8', '#ff7f00', '#f781bf', '#a65628', '#4daf4a', '#984ea3', '#999999', '#e41a1c', '#dede00']
    markers = ['s', '^', '*', 'o', '+', 'x']
    
    # For each competitor
    for i in range(len(cleaned_values)):
        
        # Data points and label
        [X, Y] = cleaned_values[i]
        label = competitor_labels[i]
        
        plot_this = True
        for excluded_competitor in excluded_competitors:
            if excluded_competitor in label:
                plot_this = False
        
        if not plot_this:
            continue
        
        # Color (if defined)
        c = None
        if label in colors_of_competitors:
            c = colors_of_competitors[label]
            print(c)
        
        # Plot
        if c is not None:
            plt.plot(X, Y, marker='+', color=c, label=label)
        else:
            plt.plot(X, Y, marker=markers[i % len(markers)], mec=colors[i % len(colors)], mfc='#ffffff00', color=colors[i % len(colors)], linestyle='--', linewidth=1, label=label)
    
    # Some displaying options and labelling
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    if logscale:
        ax.set_yscale('log') # Logscale in y direction
    plt.legend()
    plt.xlabel("Problem size")
    plt.ylim(0)
    if property == "time":
        plt.title("Run times (\"" + domain + "\" domain)")
        plt.ylabel("Runtime / s")
    else:
        plt.title("Plan lengths (\"" + domain + "\" domain)")
        plt.ylabel("Plan length")
    
    plt.tight_layout()
    
    # Show or save plot
    if output_file is None:
        plt.show()
    else:
        plt.savefig(output_file)

def print_usage():
    print("Usage : " + sys.argv[0] + " <Property> <Datafile> <Datafile options> [<Datafile> <Datafile options> ...] [<Color options>] [<General options>]")
    print()
    print("Property: `time` or `planlength`")
    print("General options:")
    print("  --logscale    Displays the plot's y values in logarithmic scale.")
    print("  -t<seconds>   Assume a timeout value of the provided amount of seconds for this visualization")
    print("  -o<filename>  Outputs the plot to the specified file instead of displaying it in a window")
    print("  --medium      Optimizes the output for a medium sized figure, increasing the font size")
    print("  --small       Optimizes the output for a small figure, increasing the font size even more")
    print()
    print("Datafile options:")
    print("  -s/<old_string>/<new_string>/")
    print("      Renames the competitor label old_string by new_string")
    print("      for each such occurence in the respective datafile")
    print("  -x<competitor>")
    print("      Excludes the competitor with the specified label")
    print()
    print("Color options:")
    print("  -c<label>:<color>")
    print("      Renders the competitor corresponding to the provided label")
    print("      (already renamed, if applicable) in the specified color")

def main():

    global timeout_seconds, property

    # Data read from the arguments
    datafiles = []
    label_replacements = []
    colors_of_competitors = dict()
    colors = []
    output_file_plot = None
    output_file_table = None
    logscale = False
    display_size = 3
    excluded_competitors = []

    # Print usage, if not enough arguments
    if len(sys.argv) <= 2:
        print_usage()
        exit()
    
    property = sys.argv[1]
    
    # Parse arguments
    for arg in sys.argv[2:]:
        
        # Logscale option
        if arg == '--logscale':
            logscale = True
        
        if arg == '--small':
            display_size = 1
        elif arg == '--medium':
            display_size = 2
        
        # Competitor label replacement option
        elif arg[0:2] == '-s':
            match = re.search(r'-s/(.+)/(.+)/', arg)
            if match is not None:            
                label_replacements[len(label_replacements)-1] += [[match.group(1), match.group(2)]]
        
        # Color option for a competitor
        elif arg[0:2] == '-c':
            match = re.search(r'-c(.+):(.+)', arg)
            if match is not None:
                colors_of_competitors[match.group(1)] = match.group(2)
        
        # Plot output file
        elif arg[0:2] == '-o':
            match = re.search(r'-o(.+)', arg)
            if match is not None:
                output_file_plot = match.group(1)
        
        # Timeout value
        elif arg[0:2] == '-t':
            match = re.search(r'-t([0-9]+)', arg)
            if match is not None:
                timeout_seconds = int(match.group(1))
        
        # Excluded competitor
        elif arg[0:2] == '-x':
            match = re.search(r'-x(.+)', arg)
            if match is not None:
                excluded_competitors += [match.group(1)]
        
        # Datafile specification
        else:
            datafiles += [arg]
            label_replacements += [[]]
            colors += [[]]

    # Structures to save the data to plot
    values = dict()
    
    # For each specified datafile
    for i in range(len(datafiles)):

        # Identify data file
        datafile = datafiles[i]
        
        # Read data file
        new_values = read_datafile(datafile)
        
        # Replace labels, if necessary
        for [str_old, str_new] in label_replacements[i]:
            new_values[str_new] = new_values.pop(str_old)
        
        # Update total values dictionary
        values.update(new_values)
    
    if os.path.isfile("reordering_" + domain + ".csv"):
        reorder_values(values)

    # Bring data in plottable form
    (cleaned_values, competitor_labels) = clean_values(values, timeout_seconds)

    # Plot and (display or save)
    plot(cleaned_values, competitor_labels, colors_of_competitors, output_file=output_file_plot, logscale=logscale, excluded_competitors=excluded_competitors, display_size=display_size)

if __name__ == '__main__':
    main()
