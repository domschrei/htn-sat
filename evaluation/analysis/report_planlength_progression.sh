#!/bin/bash

function gettime() {
    echo `echo "$1"|grep -oE "\[[0-9]+\.[0-9]+\]"|sed 's/\(\[\|\]\)//g'`
}

function getplanlength() {
    match=`echo "$@"|grep -oE " of [0-9]+"|grep -oE "[0-9]+"`
    if [ ! $match ]; then
        match=`echo "$@"|grep -oE " than [0-9]+"|grep -oE "[0-9]+"`
    fi
    if [ ! $match ]; then
        match=`echo "$@"|grep -oE " length [0-9]+"|grep -oE "[0-9]+"`
    fi
    if [ ! $match ]; then
        match=`echo "$@"|grep -oE " length: [0-9]+"|grep -oE "[0-9]+"`
    fi
    echo $match
}

if [ ! $1 ]; then
    echo "Please provide a directory."
    exit 0
fi

directory=$1

cd $directory
for d in */ ; do
    
    cd $d
    
    satfile="03_sat-output.txt"
    if [ ! -f $satfile ]; then
        satfile="general_output"
    fi
    
    if [ -f $satfile ]; then
    
        outputfile="plan_optimization.txt"
        > $outputfile

        echo "0.000 lower 1" >> $outputfile

        time_initplan=$(gettime `grep "a solution has been found" $satfile`)
        initplanlength=$(getplanlength `grep "initially found plan has an effective length of" $satfile`)
        echo $time_initplan" upper "$initplanlength >> $outputfile

        planattempt=""
        grep "find a plan shorter\|Found a plan\|No such plan" $satfile | while read -r line; do
            if [ $planattempt ] ; then
                if grep -q "Found a plan" <<<$line; then
                    #echo $(gettime $line)" upper "$planattempt >> $outputfile
                    echo $(gettime $line)" upper "$(getplanlength $line) >> $outputfile
                else
                    echo $(gettime $line)" lower "$planattempt >> $outputfile
                fi
                planattempt=""
            else
                planattempt=$(getplanlength $line)
            fi
        done

        if grep -q "Final plan of length" $satfile ; then
            line_finalplan=`grep "Final plan of length" $satfile`
        elif grep -q "Found a plan" $satfile ; then
            line_finalplan=`grep "Found a plan" $satfile | tail -1`
        else
            line_finalplan="["`cat ../timeout_seconds`".000] Timeout."
        fi
        
        best_planlength=`cat $outputfile|grep "upper"|awk '{print $3}'|sort -g|head -1`
        #echo $(gettime $line_finalplan)" result "$(getplanlength $line_finalplan) >> $outputfile
        echo $(gettime $line_finalplan)" result "$best_planlength >> $outputfile
        
        echo `cat args` $best_planlength >> ../report_planlengths.csv
        
        #sort $outputfile -g -o $outputfile
    fi
    
    cd ..

done

sort report_planlengths.csv -o report_planlengths.csv
