# HTN Evaluations

In the following, I describe how to run evaluations within the framework used during my Master project.

## Requirements

In general, you can consult the file `necessary_files`. If all these files are in their proper place, everything should work.
You need to modify this file if you are using differing executables, e.g. a differently linked Incplan, in the `solve*.sh` scripts.

### Competitors

* For running HTN-SAT, the file `htnsat.jar` and according solver execution scripts and/or the `interpreter_t-rex` binary need to be present in the `../bin` directory, as explained in the project's main README.
* To evaluate Madagascar, the three used versions `M`, `Mp`, and `MpC` can be downloaded as out-of-the-box binaries from [here](https://research.ics.aalto.fi/software/sat/madagascar/) and should be placed in the `bin/` directory as well.
* For the Incplan-patched version of Madagascar, follow the instructions on the [Incplan project page](https://github.com/StephanGocht/Incplan) to retrieve a corresponding binary.

### Utilities

* [This timeout script](https://github.com/pshved/timeout) is used; it is required in this directory.
* [GNU Parallel](https://www.gnu.org/software/parallel/) is used to manage the execution of instances (even if done fully sequentially). It must be installed on the machine.
* (Optional) For validation of resulting plans by calling `bash validate_all <eval_dir>`, the [VAL tool](https://github.com/KCL-Planning/VAL) is required as `../bin/VAL_validate`.

## Preparations

### Evaluation instances

The instances to run must be defined in a file `full_eval_instances_randomized.csv` in this directory.
Preset instance files which have been used for evaluations can be found in `instance_files/` and copied accordingly.
In the following, it is described how to define custom evaluation instances.

Create a file `evaluation_instances.csv` in this directory.
Each line contains exactly one instance, specified as follows:

```
<Domain> <Instance> <Approach> <Additional parameters>
```

* `Domain`: The domain of the instance. In the directory `../problems`, a directory of this name must exists, containing the domain specification `domain.pddl` as well as all instances of this domain to test. If the used approach is a Madagascar variant, then the directory is `../problems/non_htn` respectively.
* `Instance`: The index of the instance. In the domain directory `../problems/<Domain>/` (or `../problems/non_htn/<Domain>/` respectively), a problem specification `p<Instance>.pddl` must exist.
* `Approach`: The planning approach to employ.
* `Additional parameters`: The parameters for the previously specified planning strategy, if necessary.

For example, the file may look like this:

```
Rover 01 MpC
Barman 10 T-REX -i {-P,-b8192}
```

The first line employs Madagascar `MpC` on the problem Rover01.
The second line employs the HTN-SAT planner with the T-REX approach on the problem Barman10.

Then, to randomize the instances, execute `bash create_full_eval_instances.sh`.
This creates a file `full_eval_instances_randomized.csv` containing the previously specified evaluation instances in a random order.

If multiple executions (repetitions) of each instance are desired, edit the file `num_repetitions` to contain (only) the amount of needed repetitions per instance, and re-run `bash create_full_eval_instances.sh`.

### Parameters

Edit the file `timeout_seconds` to (only) contain the amount of seconds after which a run should be cut off.
Similarly, the number inside the file `memlimit_kb` specifies the maximum amount of kilobytes of RAM that may be allocated by the run after which it is cut off.

If desired, the amount of instances to be run _in parallel_ can be modified in the variable `num_jobs` in the script `run_all.sh`.

## Running an evaluation

Execute `bash run_all.sh`. If you are not sure if everything is set up correctly, you can set the variable `simulate` to `true` in the script `run_single.sh` to do a dry run without any actual computations (but with potentially helpful error output).

An actual run of evaluations will successively spawn temporal directories `./proc_*` containing logs and symbolic links to the files specified in `necessary_files`.
In addition, a file `runlog` is created, and some information about the run instances is logged here.

As soon as the evaluation finishes, these directories and files will vanish and all produced log information is moved to a new directory `logs/Eval_<Date and time>/`.

### Cancelling and recovering evaluations

When cancelling an evaluation, make sure that all involved processes are terminated, e.g. by using `htop`.

If an evaluation is prematurely cancelled or fails, the log information generated so far can be recovered by copying the file `runlog` as well as all existing directories `./proc_*/logs/*/` into a new log directory.

To resume a started evaluation, the line number of the first un-finished instance can be specified in the variable `start_instance` in the script `run_all.sh`. The evaluation process will then only do the evaluations from this line until the last line of instances.

## Results

As a first step towards analyzing and visualizing results, `bash report_times.sh logs/<your log directory>` can be executed to create a file `report_times.csv` in your log directory containing the run times for each instance (or the timeout value, if a run did not successfully terminate).
This file can then be further processed for visualizations or computations.
