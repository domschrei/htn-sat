#!/bin/bash

dir=$1
if [ ! dir ]; then
    echo "Please provide a destination directory."
    exit
fi

dest=$dir/sysinfo/
mkdir -p $dest

ps aux --sort=-pcpu > $dest/ps
date > $dest/date
cat /proc/cpuinfo > $dest/cpuinfo
cat /proc/meminfo > $dest/meminfo
uname -a > $dest/uname
printenv > $dest/env
cat /sys/class/thermal/thermal_zone*/temp > $dest/thermal
