#!/bin/bash

set -e

directory="logs/`date +%Y-%m-%d_%H-%M-%S_%N`_$6_`echo $4|sed 's/\//-/g'`_$5"
mkdir $directory

args="-L -b f"
command="$1 $args $2 $3"
shift 6
command="$command $@"

echo "Command: $command"

# Execute the patched Madagascar
echo "Executing patched Madagascar ..."
$command | tee ${directory}/00_log.txt

# Run incplan on the formula
echo "Executing Incplan on created DimSpec file ..."
./incplan-glucose4 f.cnf | tee -a ${directory}/00_log.txt

echo "Incplan execution finished."
if [ -f f.cnf ]; then mv f.cnf ${directory}/02_formula.cnf ; fi
