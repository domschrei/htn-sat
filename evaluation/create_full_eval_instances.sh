#!/bin/bash

repetitions=`cat num_repetitions`

> full_eval_instances.csv

while read -r line; do 
    for i in `seq 1 ${repetitions}`; do
        echo $line | grep -vE "^#" >> full_eval_instances.csv
    done
done < evaluation_instances.csv

cat full_eval_instances.csv | grep -vE "^$" | shuf > full_eval_instances_randomized.csv
