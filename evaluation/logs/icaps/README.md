This directory contains evaluation data for the ICAPS 2019 publication _Tree-REX: SAT-based Tree Exploration for Efficient and High-Quality HTN Planning_.

* The overall results, i.e. the run times and plan lengths of each of the competitors, can be found in the `results` directory.
* More detailed evaluation data including all logs of PANDA (running totSAT) and HTN-SAT (running T-REX) can be found in the respective directories `totsat` and `trex`.
