Command : ./interpreter_t-rex_minisat220 -P -b8192 -o -l-1 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.001] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.007] Processed problem encoding.
[0.007] Calculated possible fact changes of composite elements.
[0.007] Initialized instantiation procedure.
[0.007] 
[0.007] *************************************
[0.007] * * *   M a k e s p a n     0   * * *
[0.007] *************************************
[0.008] Instantiated 1,755 initial clauses.
[0.008] The encoding contains a total of 1,111 distinct variables.
[0.008] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.008] Executed solver; result: UNSAT.
[0.008] 
[0.008] *************************************
[0.008] * * *   M a k e s p a n     1   * * *
[0.008] *************************************
[0.009] Computed next depth properties: array size of 45.
[0.010] Instantiated 3,788 transitional clauses.
[0.013] Instantiated 28,448 universal clauses.
[0.013] Instantiated and added clauses for a total of 33,991 clauses.
[0.013] The encoding contains a total of 4,581 distinct variables.
[0.013] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.013] Executed solver; result: UNSAT.
[0.013] 
[0.013] *************************************
[0.013] * * *   M a k e s p a n     2   * * *
[0.013] *************************************
[0.015] Computed next depth properties: array size of 67.
[0.016] Instantiated 4,892 transitional clauses.
[0.020] Instantiated 25,010 universal clauses.
[0.020] Instantiated and added clauses for a total of 63,893 clauses.
[0.020] The encoding contains a total of 7,619 distinct variables.
[0.020] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.020] Executed solver; result: UNSAT.
[0.020] 
[0.020] *************************************
[0.020] * * *   M a k e s p a n     3   * * *
[0.020] *************************************
[0.022] Computed next depth properties: array size of 89.
[0.024] Instantiated 5,640 transitional clauses.
[0.029] Instantiated 29,420 universal clauses.
[0.029] Instantiated and added clauses for a total of 98,953 clauses.
[0.029] The encoding contains a total of 11,043 distinct variables.
[0.029] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.033] Executed solver; result: UNSAT.
[0.033] 
[0.033] *************************************
[0.033] * * *   M a k e s p a n     4   * * *
[0.033] *************************************
[0.036] Computed next depth properties: array size of 111.
[0.038] Instantiated 6,630 transitional clauses.
[0.045] Instantiated 34,752 universal clauses.
[0.045] Instantiated and added clauses for a total of 140,335 clauses.
[0.045] The encoding contains a total of 14,943 distinct variables.
[0.045] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.051] Executed solver; result: SAT.
[0.051] Solver returned SAT; a solution has been found at makespan 4.
92
solution 916 1
898 889 905 730 913 916 915 916 64 915 799 895 899 896 40 887 888 439 887 879 885 25 867 871 864 112 870 861 877 874 10 878 862 880 863 82 877 874 13 900 896 580 900 889 905 902 49 906 913 895 550 909 914 916 67 878 885 867 871 223 865 883 875 884 28 879 885 867 160 871 861 869 876 19 873 861 880 886 427 879 874 878 22 862 869 864 133 870 861 877 862 4 
[0.052] The initially found plan has an effective length of 92.
[0.053] Added 111 permanent goal conditions.
[0.054] Added 12,433 clauses counting the effective plan length.
[0.054] Attempting to find a plan shorter than 92.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.059] Found a plan. New actual length: 84.
84
[0.060] Attempting to find a plan shorter than 84.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |       284 |   13050    79101   207136 |    29003      263     26 | 38.646 % |
c [minisat220] |       434 |   13042    78080   204820 |    31904      403     23 | 38.684 % |
c [minisat220] |       659 |   12941    77055   202408 |    35094      613     27 | 39.158 % |
c [minisat220] |       996 |   12695    76576   201327 |    38603      926     27 | 40.315 % |
c [minisat220] ===============================================================================
[0.143] Found a plan. New actual length: 77.
77
[0.144] Attempting to find a plan shorter than 77.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |      1378 |   12413    71868   190640 |    26791     1204     29 | 41.641 % |
c [minisat220] |      1528 |   12088    71868   190640 |    29470     1333     29 | 43.169 % |
c [minisat220] |      1753 |   11691    66746   179002 |    32417     1482     33 | 45.035 % |
c [minisat220] |      2090 |   11487    60352   164314 |    35659     1707     36 | 45.994 % |
c [minisat220] |      2596 |   10960    57289   157366 |    39225     2098     35 | 48.472 % |
c [minisat220] ===============================================================================
[0.227] Found a plan. New actual length: 69.
69
[0.228] Attempting to find a plan shorter than 69.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |      2811 |   10960    54534   151013 |    19995     2256     36 | 48.472 % |
c [minisat220] |      2961 |   10959    54534   151013 |    21995     2405     36 | 48.477 % |
c [minisat220] |      3186 |   10813    54503   150950 |    24194     2622     36 | 49.163 % |
c [minisat220] |      3523 |   10792    53852   149414 |    26614     2949     36 | 49.262 % |
c [minisat220] ===============================================================================
[0.324] No such plan exists.
[0.324] Final plan of length 69 identified.
69
solution 916 1
898 889 905 730 913 916 64 887 403 867 871 868 7 835 915 58 872 864 112 870 861 877 874 10 878 862 82 877 874 13 878 862 869 868 184 872 861 877 874 16 910 894 550 911 916 67 878 862 869 223 865 883 28 882 866 160 871 876 19 915 823 907 55 873 864 133 870 861 4 
[0.325] Exiting.
