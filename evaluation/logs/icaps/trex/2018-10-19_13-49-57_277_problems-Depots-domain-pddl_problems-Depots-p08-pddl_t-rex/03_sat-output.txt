Command : ./interpreter_t-rex_minisat220 -P -b8192 -o -l-1 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.001] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.018] Processed problem encoding.
[0.027] Calculated possible fact changes of composite elements.
[0.029] Initialized instantiation procedure.
[0.029] 
[0.029] *************************************
[0.029] * * *   M a k e s p a n     0   * * *
[0.029] *************************************
[0.030] Instantiated 3,772 initial clauses.
[0.030] The encoding contains a total of 2,264 distinct variables.
[0.030] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.031] Executed solver; result: UNSAT.
[0.031] 
[0.031] *************************************
[0.031] * * *   M a k e s p a n     1   * * *
[0.031] *************************************
[0.032] Computed next depth properties: array size of 29.
[0.035] Instantiated 3,540 transitional clauses.
[0.042] Instantiated 17,946 universal clauses.
[0.042] Instantiated and added clauses for a total of 25,258 clauses.
[0.042] The encoding contains a total of 7,378 distinct variables.
[0.042] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.042] Executed solver; result: UNSAT.
[0.042] 
[0.042] *************************************
[0.042] * * *   M a k e s p a n     2   * * *
[0.042] *************************************
[0.046] Computed next depth properties: array size of 85.
[0.056] Instantiated 12,844 transitional clauses.
[0.075] Instantiated 56,042 universal clauses.
[0.075] Instantiated and added clauses for a total of 94,144 clauses.
[0.075] The encoding contains a total of 18,719 distinct variables.
[0.075] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.076] Executed solver; result: UNSAT.
[0.076] 
[0.076] *************************************
[0.076] * * *   M a k e s p a n     3   * * *
[0.076] *************************************
[0.082] Computed next depth properties: array size of 169.
[0.103] Instantiated 49,847 transitional clauses.
[0.123] Instantiated 129,524 universal clauses.
[0.123] Instantiated and added clauses for a total of 273,515 clauses.
[0.123] The encoding contains a total of 38,862 distinct variables.
[0.123] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.124] Executed solver; result: UNSAT.
[0.124] 
[0.124] *************************************
[0.124] * * *   M a k e s p a n     4   * * *
[0.124] *************************************
[0.131] Computed next depth properties: array size of 253.
[0.164] Instantiated 90,248 transitional clauses.
[0.194] Instantiated 191,736 universal clauses.
[0.194] Instantiated and added clauses for a total of 555,499 clauses.
[0.194] The encoding contains a total of 62,834 distinct variables.
[0.194] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.210] Executed solver; result: UNSAT.
[0.210] 
[0.210] *************************************
[0.210] * * *   M a k e s p a n     5   * * *
[0.210] *************************************
[0.219] Computed next depth properties: array size of 337.
[0.254] Instantiated 94,010 transitional clauses.
[0.295] Instantiated 213,086 universal clauses.
[0.295] Instantiated and added clauses for a total of 862,595 clauses.
[0.295] The encoding contains a total of 87,169 distinct variables.
[0.295] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.313] Executed solver; result: SAT.
[0.313] Solver returned SAT; a solution has been found at makespan 5.
38
solution 314 1
41 135 136 44 54 55 34 13 43 297 131 47 314 98 205 95 81 83 31 11 43 29 10 47 148 80 43 142 37 121 15 3 262 12 160 8 21 6 
[0.314] The initially found plan has an effective length of 38.
[0.321] Added 337 permanent goal conditions.
[0.335] Added 113,907 clauses counting the effective plan length.
[0.335] Attempting to find a plan shorter than 38.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.337] No such plan exists.
[0.337] Final plan of length 38 identified.
38
solution 314 1
41 135 136 44 54 55 34 13 43 297 131 47 314 98 205 95 81 83 31 11 43 29 10 47 148 80 43 142 37 121 15 3 262 12 160 8 21 6 
[0.338] Exiting.
