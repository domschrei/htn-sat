> Preprocessed problem (1984 ms)
Before elimination: 5749 tasks
After elimination: 5749 tasks
  5749/6717 tasks are reachable.
Initial task network before elimination: 1 tasks
Initial task network after elimination: 1 tasks
  134/322 facts are reachable.
> Created HtnProblemData instance (157 ms)
> Encoded problem into SAT (536 ms)
> Wrote formula to file (166 ms)
> Executed SAT solver (4721 ms)
> Decoded result (2 ms)
> Reported result (1 ms)
Total execution time: 7567ms. Exiting.
