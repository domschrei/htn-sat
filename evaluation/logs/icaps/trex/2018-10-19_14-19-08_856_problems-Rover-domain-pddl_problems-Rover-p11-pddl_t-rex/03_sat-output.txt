Command : ./interpreter_t-rex_minisat220 -P -b8192 -o -l-1 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.001] Reading problem encoding from file "f.cnf".
[0.002] Parsed head comment information.
[0.117] Processed problem encoding.
[0.213] Calculated possible fact changes of composite elements.
[0.214] Initialized instantiation procedure.
[0.214] 
[0.214] *************************************
[0.214] * * *   M a k e s p a n     0   * * *
[0.214] *************************************
[0.216] Instantiated 7,601 initial clauses.
[0.216] The encoding contains a total of 4,392 distinct variables.
[0.216] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.216] Executed solver; result: UNSAT.
[0.216] 
[0.216] *************************************
[0.216] * * *   M a k e s p a n     1   * * *
[0.216] *************************************
[0.227] Computed next depth properties: array size of 57.
[0.242] Instantiated 25,129 transitional clauses.
[0.261] Instantiated 68,240 universal clauses.
[0.261] Instantiated and added clauses for a total of 100,970 clauses.
[0.261] The encoding contains a total of 29,914 distinct variables.
[0.261] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.261] Executed solver; result: UNSAT.
[0.261] 
[0.261] *************************************
[0.261] * * *   M a k e s p a n     2   * * *
[0.261] *************************************
[0.293] Computed next depth properties: array size of 102.
[0.357] Instantiated 148,250 transitional clauses.
[0.397] Instantiated 206,822 universal clauses.
[0.397] Instantiated and added clauses for a total of 456,042 clauses.
[0.397] The encoding contains a total of 105,966 distinct variables.
[0.397] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.398] Executed solver; result: UNSAT.
[0.398] 
[0.398] *************************************
[0.398] * * *   M a k e s p a n     3   * * *
[0.398] *************************************
[0.454] Computed next depth properties: array size of 178.
[0.597] Instantiated 395,215 transitional clauses.
[0.665] Instantiated 363,504 universal clauses.
[0.665] Instantiated and added clauses for a total of 1,214,761 clauses.
[0.665] The encoding contains a total of 212,814 distinct variables.
[0.665] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |       103 |  211233  1200019  3266250 |   440006      101    120 |  0.742 % |
c [minisat220] ===============================================================================
[0.908] Executed solver; result: UNSAT.
[0.908] 
[0.908] *************************************
[0.908] * * *   M a k e s p a n     4   * * *
[0.908] *************************************
[0.989] Computed next depth properties: array size of 271.
[1.181] Instantiated 542,780 transitional clauses.
[1.272] Instantiated 455,181 universal clauses.
[1.272] Instantiated and added clauses for a total of 2,212,722 clauses.
[1.272] The encoding contains a total of 324,744 distinct variables.
[1.272] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |       339 |  322723  2190467  6249956 |   803171      337    114 |  0.622 % |
c [minisat220] |       489 |  322723  2190467  6249956 |   883488      487    146 |  0.622 % |
c [minisat220] ===============================================================================
[1.499] Executed solver; result: SAT.
[1.499] Solver returned SAT; a solution has been found at makespan 4.
169
solution 2026 1
21 433 22 512 47 457 21 429 22 48 838 17 422 21 427 22 18 478 483 860 19 90 51 117 52 20 494 854 71 407 21 436 22 72 478 517 69 472 21 429 22 70 847 63 272 41 250 42 64 505 37 248 38 708 29 98 51 118 52 30 473 498 33 101 51 113 52 34 552 51 325 21 302 22 52 492 25 309 21 301 22 26 746 71 73 51 114 52 72 473 484 13 85 51 113 52 14 526 17 358 15 356 16 18 868 49 386 15 354 16 50 934 17 360 25 373 26 18 477 872 67 403 25 371 26 68 943 41 253 63 270 64 42 475 862 975 71 73 72 1894 51 117 52 1408 1742 2026 17 425 18 1258 63 468 64 1845 29 98 30 1894 51 115 19 89 20 52 1614 23 93 24 1750 
[1.501] The initially found plan has an effective length of 169.
[1.502] Added 271 permanent goal conditions.
[1.514] Added 73,713 clauses counting the effective plan length.
[1.514] Attempting to find a plan shorter than 169.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |       669 |  267953  1956676  5622084 |   828364      658    314 | 25.953 % |
c [minisat220] |       819 |  267849  1956676  5622084 |   911200      805    278 | 25.982 % |
c [minisat220] |      1044 |  267767  1956676  5622084 |  1002321     1027    246 | 26.005 % |
c [minisat220] ===============================================================================
[2.306] Found a plan. New actual length: 156.
156
[2.307] Attempting to find a plan shorter than 156.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |      1432 |  267615  1954337  5616824 |   716590     1394    221 | 26.047 % |
c [minisat220] |      1582 |  267122  1954337  5616824 |   788249     1537    210 | 26.183 % |
c [minisat220] |      1807 |  266662  1954337  5616824 |   867074     1758    196 | 26.310 % |
c [minisat220] ===============================================================================
[2.916] Found a plan. New actual length: 147.
147
[2.917] Attempting to find a plan shorter than 147.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |      1971 |  266658  1946339  5598629 |   716590     1905    206 | 26.311 % |
c [minisat220] ===============================================================================
[3.302] Found a plan. New actual length: 144.
144
[3.303] Attempting to find a plan shorter than 144.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[3.358] Found a plan. New actual length: 138.
138
[3.358] Attempting to find a plan shorter than 138.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |      2184 |  266536  1946339  5598629 |   713657     2114    198 | 26.345 % |
c [minisat220] ===============================================================================
[3.543] Found a plan. New actual length: 136.
136
[3.544] Attempting to find a plan shorter than 136.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |      2340 |  266506  1946339  5598629 |   713657     2269    197 | 26.353 % |
c [minisat220] |      2490 |  266505  1945063  5595802 |   785023     2414    201 | 26.354 % |
c [minisat220] |      2715 |  266443  1945063  5595802 |   863525     2637    194 | 26.371 % |
c [minisat220] |      3052 |  266283  1945063  5595802 |   949878     2973    220 | 26.415 % |
c [minisat220] ===============================================================================
[4.281] Found a plan. New actual length: 135.
135
[4.282] Attempting to find a plan shorter than 135.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[4.351] Found a plan. New actual length: 132.
132
[4.352] Attempting to find a plan shorter than 132.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |      3225 |  266283  1945063  5595802 |   713189     3146    230 | 26.415 % |
c [minisat220] ===============================================================================
[4.673] Found a plan. New actual length: 118.
118
[4.673] Attempting to find a plan shorter than 118.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |      3456 |  266261  1943205  5591649 |   712508     3363    265 | 26.421 % |
c [minisat220] |      3606 |  266261  1943205  5591649 |   783759     3513    270 | 26.421 % |
c [minisat220] |      3831 |  266247  1943205  5591649 |   862135     3737    269 | 26.425 % |
c [minisat220] |      4168 |  266211  1942896  5590958 |   948348     4071    259 | 26.435 % |
c [minisat220] |      4674 |  265529  1942896  5590958 |  1043183     4571    263 | 26.623 % |
c [minisat220] |      5433 |  264593  1931549  5565406 |  1147502     5252    269 | 26.882 % |
c [minisat220] |      6572 |  264574  1929550  5560855 |  1262252     6371    289 | 26.887 % |
c [minisat220] |      8280 |  264140  1926012  5553031 |  1388477     8023    310 | 27.007 % |
c [minisat220] |     10842 |  263896  1924145  5548917 |  1527325    10513    410 | 27.075 % |
c [minisat220] |     14686 |  263644  1922141  5544484 |  1680057    14298    445 | 27.144 % |
c [minisat220] ===============================================================================
[19.912] Found a plan. New actual length: 117.
117
[19.913] Attempting to find a plan shorter than 117.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |     19127 |  263054  1916286  5530934 |   702853    18553    520 | 27.307 % |
c [minisat220] |     19277 |  263054  1916286  5530934 |   773139    18703    519 | 27.307 % |
c [minisat220] |     19502 |  263054  1916286  5530934 |   850453    18928    518 | 27.307 % |
c [minisat220] |     19839 |  263054  1916286  5530934 |   935498    19265    518 | 27.307 % |
c [minisat220] |     20345 |  263047  1916286  5530934 |  1029048    19768    519 | 27.309 % |
c [minisat220] |     21104 |  263045  1916198  5530747 |  1131953    20526    527 | 27.310 % |
c [minisat220] |     22243 |  263017  1916198  5530747 |  1245148    21663    533 | 27.317 % |
c [minisat220] |     23951 |  262973  1915550  5529335 |  1369663    23361    534 | 27.330 % |
c [minisat220] |     26513 |  262966  1915459  5529145 |  1506629    25917    558 | 27.332 % |
c [minisat220] ===============================================================================
[30.783] Found a plan. New actual length: 115.
115
[30.784] Attempting to find a plan shorter than 115.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |     27416 |  262963  1915459  5529145 |   702334    26819    557 | 27.332 % |
c [minisat220] |     27566 |  262963  1915459  5529145 |   772568    26969    561 | 27.332 % |
c [minisat220] |     27791 |  262963  1915459  5529145 |   849825    27194    563 | 27.332 % |
c [minisat220] |     28128 |  262904  1915459  5529145 |   934807    27529    565 | 27.349 % |
c [minisat220] |     28634 |  262904  1914951  5528002 |  1028288    28027    565 | 27.349 % |
c [minisat220] |     29393 |  262852  1914951  5528002 |  1131117    28782    571 | 27.363 % |
c [minisat220] ===============================================================================
[35.577] Found a plan. New actual length: 114.
114
[35.578] Attempting to find a plan shorter than 114.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |     30347 |  262852  1914951  5528002 |   702148    29736    587 | 27.363 % |
c [minisat220] |     30497 |  262852  1914951  5528002 |   772363    29886    586 | 27.363 % |
c [minisat220] |     30722 |  262852  1914951  5528002 |   849599    30111    584 | 27.363 % |
c [minisat220] |     31059 |  262839  1914446  5526858 |   934559    30436    585 | 27.367 % |
c [minisat220] |     31565 |  262839  1914446  5526858 |  1028015    30942    588 | 27.367 % |
c [minisat220] |     32324 |  262839  1914446  5526858 |  1130817    31701    590 | 27.367 % |
c [minisat220] |     33463 |  262802  1914446  5526858 |  1243899    32839    602 | 27.377 % |
c [minisat220] |     35171 |  262668  1913499  5524731 |  1368289    34511    609 | 27.414 % |
c [minisat220] |     37733 |  262648  1912927  5523461 |  1505118    37045    609 | 27.419 % |
c [minisat220] |     41577 |  262502  1911665  5520676 |  1655629    40868    616 | 27.460 % |
c [minisat220] ===============================================================================
[54.623] Found a plan. New actual length: 111.
111
[54.624] Attempting to find a plan shorter than 111.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |     41926 |  262497  1911665  5520676 |   700943    41216    615 | 27.461 % |
c [minisat220] |     42076 |  262497  1911665  5520676 |   771038    41366    615 | 27.461 % |
c [minisat220] |     42301 |  262497  1911665  5520676 |   848142    41591    616 | 27.461 % |
c [minisat220] |     42638 |  262461  1911665  5520676 |   932956    41926    617 | 27.471 % |
c [minisat220] |     43144 |  262460  1911665  5520676 |  1026251    42431    616 | 27.471 % |
c [minisat220] |     43903 |  262455  1911665  5520676 |  1128877    43186    619 | 27.473 % |
c [minisat220] |     45042 |  262454  1911665  5520676 |  1241764    44324    622 | 27.473 % |
c [minisat220] |     46750 |  262451  1911256  5519775 |  1365941    46021    624 | 27.474 % |
c [minisat220] |     49312 |  262448  1911256  5519775 |  1502535    48581    646 | 27.475 % |
c [minisat220] |     53156 |  262444  1911154  5519569 |  1652788    52421    646 | 27.476 % |
c [minisat220] ===============================================================================
[77.585] Found a plan. New actual length: 109.
109
[77.585] Attempting to find a plan shorter than 109.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |     53451 |  262443  1911154  5519569 |   700756    52715    646 | 27.476 % |
c [minisat220] |     53601 |  262441  1911122  5519503 |   770832    52864    646 | 27.477 % |
c [minisat220] |     53826 |  262441  1911122  5519503 |   847915    53089    646 | 27.477 % |
c [minisat220] |     54163 |  262441  1911122  5519503 |   932706    53426    647 | 27.477 % |
c [minisat220] |     54669 |  262441  1911122  5519503 |  1025977    53932    650 | 27.477 % |
c [minisat220] |     55428 |  262441  1911122  5519503 |  1128575    54691    648 | 27.477 % |
c [minisat220] |     56567 |  262441  1911122  5519503 |  1241432    55830    644 | 27.477 % |
c [minisat220] |     58275 |  262441  1911107  5519472 |  1365576    57537    641 | 27.477 % |
c [minisat220] |     60837 |  262441  1911107  5519472 |  1502133    60099    639 | 27.477 % |
c [minisat220] |     64681 |  262435  1911083  5519423 |  1652347    63940    636 | 27.478 % |
c [minisat220] |     70447 |  262412  1911020  5519295 |  1817581    69703    631 | 27.485 % |
c [minisat220] |     79096 |  262181  1910770  5518748 |  1999339    78322    645 | 27.548 % |
c [minisat220] |     92070 |  262118  1908288  5513236 |  2199273    91256    650 | 27.566 % |
c [minisat220] |    111531 |  262095  1908097  5512839 |  2419201   110704    667 | 27.572 % |
Interrupt signal (15) received.
109
solution 2032 1
21 433 22 512 47 458 48 841 63 267 64 481 857 478 497 861 71 212 21 238 22 72 475 515 69 276 21 237 22 70 719 41 183 42 504 37 177 38 638 71 213 72 475 500 33 245 34 692 41 182 42 474 490 25 168 26 612 71 211 72 475 486 13 232 14 666 51 328 52 867 49 321 51 322 52 50 907 17 360 25 373 26 18 872 67 403 25 371 26 68 943 17 362 63 399 64 18 477 864 977 1999 1283 1807 1878 1062 1731 2032 1630 1866 
[286.933] Exiting.
