> Preprocessed problem (16960 ms)
Before elimination: 13040 tasks
After elimination: 13040 tasks
  13040/13041 tasks are reachable.
Initial task network before elimination: 24 tasks
Initial task network after elimination: 24 tasks
  312/848 facts are reachable.
> Created HtnProblemData instance (1995 ms)
> Encoded problem into SAT (3610 ms)
> Wrote formula to file (1421 ms)
> Executed SAT solver (5963 ms)
> Decoded result (3 ms)
> Reported result (3 ms)
Total execution time: 29955ms. Exiting.
