> Preprocessed problem (674 ms)
Before elimination: 1133 tasks
After elimination: 1133 tasks
  1133/1134 tasks are reachable.
Initial task network before elimination: 10 tasks
Initial task network after elimination: 10 tasks
  120/340 facts are reachable.
> Created HtnProblemData instance (135 ms)
> Encoded problem into SAT (172 ms)
> Wrote formula to file (73 ms)
> Executed SAT solver (95 ms)
> Decoded result (3 ms)
> Reported result (2 ms)
Total execution time: 1154ms. Exiting.
