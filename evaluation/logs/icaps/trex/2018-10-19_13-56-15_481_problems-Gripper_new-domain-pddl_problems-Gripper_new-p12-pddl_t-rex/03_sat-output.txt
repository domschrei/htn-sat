Command : ./interpreter_t-rex_minisat220 -P -b8192 -o -l-1 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.001] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.001] Processed problem encoding.
[0.001] Calculated possible fact changes of composite elements.
[0.001] Initialized instantiation procedure.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     0   * * *
[0.001] *************************************
[0.001] Instantiated 679 initial clauses.
[0.001] The encoding contains a total of 403 distinct variables.
[0.001] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.001] Executed solver; result: UNSAT.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     1   * * *
[0.001] *************************************
[0.001] Computed next depth properties: array size of 79.
[0.001] Instantiated 351 transitional clauses.
[0.002] Instantiated 2,792 universal clauses.
[0.002] Instantiated and added clauses for a total of 3,822 clauses.
[0.002] The encoding contains a total of 1,426 distinct variables.
[0.002] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.002] Executed solver; result: SAT.
[0.002] Solver returned SAT; a solution has been found at makespan 1.
77
solution 105 1
104 102 5 105 103 2 98 92 5 99 93 2 90 84 5 91 85 2 82 76 5 83 77 2 74 68 5 75 69 2 66 60 5 67 61 2 58 52 5 59 53 2 50 44 5 51 45 2 42 36 5 43 37 2 34 28 5 35 29 2 26 20 5 27 21 2 18 12 5 19 13 2 9 8 5 11 10 
[0.003] The initially found plan has an effective length of 77.
[0.003] Added 79 permanent goal conditions.
[0.004] Added 6,321 clauses counting the effective plan length.
[0.004] Attempting to find a plan shorter than 77.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.005] No such plan exists.
[0.005] Final plan of length 77 identified.
77
solution 105 1
104 102 5 105 103 2 98 92 5 99 93 2 90 84 5 91 85 2 82 76 5 83 77 2 74 68 5 75 69 2 66 60 5 67 61 2 58 52 5 59 53 2 50 44 5 51 45 2 42 36 5 43 37 2 34 28 5 35 29 2 26 20 5 27 21 2 18 12 5 19 13 2 9 8 5 11 10 
[0.006] Exiting.
