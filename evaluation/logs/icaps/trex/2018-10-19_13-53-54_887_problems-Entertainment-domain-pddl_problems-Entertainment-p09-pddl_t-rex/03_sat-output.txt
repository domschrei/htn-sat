Command : ./interpreter_t-rex_minisat220 -P -b8192 -o -l-1 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.001] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.002] Processed problem encoding.
[0.003] Calculated possible fact changes of composite elements.
[0.003] Initialized instantiation procedure.
[0.003] 
[0.003] *************************************
[0.003] * * *   M a k e s p a n     0   * * *
[0.003] *************************************
[0.003] Instantiated 814 initial clauses.
[0.003] The encoding contains a total of 525 distinct variables.
[0.003] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.003] Executed solver; result: UNSAT.
[0.003] 
[0.003] *************************************
[0.003] * * *   M a k e s p a n     1   * * *
[0.003] *************************************
[0.003] Computed next depth properties: array size of 5.
[0.004] Instantiated 218 transitional clauses.
[0.004] Instantiated 1,132 universal clauses.
[0.004] Instantiated and added clauses for a total of 2,164 clauses.
[0.004] The encoding contains a total of 914 distinct variables.
[0.004] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.004] Executed solver; result: UNSAT.
[0.004] 
[0.004] *************************************
[0.004] * * *   M a k e s p a n     2   * * *
[0.004] *************************************
[0.004] Computed next depth properties: array size of 7.
[0.004] Instantiated 541 transitional clauses.
[0.005] Instantiated 2,435 universal clauses.
[0.005] Instantiated and added clauses for a total of 5,140 clauses.
[0.005] The encoding contains a total of 1,344 distinct variables.
[0.005] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.005] Executed solver; result: UNSAT.
[0.005] 
[0.005] *************************************
[0.005] * * *   M a k e s p a n     3   * * *
[0.005] *************************************
[0.005] Computed next depth properties: array size of 9.
[0.006] Instantiated 677 transitional clauses.
[0.007] Instantiated 2,876 universal clauses.
[0.007] Instantiated and added clauses for a total of 8,693 clauses.
[0.007] The encoding contains a total of 1,893 distinct variables.
[0.007] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.008] Executed solver; result: SAT.
[0.008] Solver returned SAT; a solution has been found at makespan 3.
4
solution 146 1
97 120 146 126 
[0.008] The initially found plan has an effective length of 4.
[0.008] Added 9 permanent goal conditions.
[0.008] Added 91 clauses counting the effective plan length.
[0.008] Attempting to find a plan shorter than 4.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.009] No such plan exists.
[0.009] Final plan of length 4 identified.
4
solution 146 1
97 120 146 126 
[0.009] Exiting.
