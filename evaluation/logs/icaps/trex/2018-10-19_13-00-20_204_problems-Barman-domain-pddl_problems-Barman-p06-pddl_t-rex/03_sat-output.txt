Command : ./interpreter_t-rex_minisat220 -P -b8192 -o -l-1 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.001] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.012] Processed problem encoding.
[0.015] Calculated possible fact changes of composite elements.
[0.015] Initialized instantiation procedure.
[0.015] 
[0.015] *************************************
[0.015] * * *   M a k e s p a n     0   * * *
[0.015] *************************************
[0.017] Instantiated 5,955 initial clauses.
[0.017] The encoding contains a total of 3,128 distinct variables.
[0.017] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.017] Executed solver; result: UNSAT.
[0.017] 
[0.017] *************************************
[0.017] * * *   M a k e s p a n     1   * * *
[0.017] *************************************
[0.018] Computed next depth properties: array size of 43.
[0.019] Instantiated 2,128 transitional clauses.
[0.024] Instantiated 17,334 universal clauses.
[0.024] Instantiated and added clauses for a total of 25,417 clauses.
[0.024] The encoding contains a total of 9,679 distinct variables.
[0.024] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.024] Executed solver; result: UNSAT.
[0.024] 
[0.024] *************************************
[0.024] * * *   M a k e s p a n     2   * * *
[0.024] *************************************
[0.028] Computed next depth properties: array size of 183.
[0.032] Instantiated 13,678 transitional clauses.
[0.050] Instantiated 96,154 universal clauses.
[0.050] Instantiated and added clauses for a total of 135,249 clauses.
[0.050] The encoding contains a total of 21,721 distinct variables.
[0.050] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.050] Executed solver; result: UNSAT.
[0.050] 
[0.050] *************************************
[0.050] * * *   M a k e s p a n     3   * * *
[0.050] *************************************
[0.058] Computed next depth properties: array size of 239.
[0.062] Instantiated 11,382 transitional clauses.
[0.080] Instantiated 92,402 universal clauses.
[0.080] Instantiated and added clauses for a total of 239,033 clauses.
[0.080] The encoding contains a total of 28,583 distinct variables.
[0.080] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |       103 |   27903   231571   586500 |    84909       99     28 |  2.376 % |
c [minisat220] |       253 |   27901   230103   582800 |    93400      247     49 |  2.383 % |
c [minisat220] ===============================================================================
[0.152] Executed solver; result: SAT.
[0.152] Solver returned SAT; a solution has been found at makespan 3.
206
solution 1187 1
1030 556 284 285 736 735 21 937 949 950 951 939 940 1040 839 389 390 751 750 31 937 978 985 979 939 940 1023 862 410 411 592 591 14 937 982 987 983 939 940 1027 776 326 327 868 867 18 937 966 989 967 939 940 1031 514 242 243 874 873 22 932 972 990 973 934 935 1029 150 511 239 240 779 781 20 932 956 992 957 934 935 1039 1187 164 165 794 796 30 1024 96 15 937 954 995 955 939 940 1034 742 200 201 652 651 25 932 942 996 943 934 935 1040 390 661 119 120 571 570 31 1028 462 19 937 938 999 941 939 940 1040 300 661 119 120 931 930 31 1030 375 21 937 970 1001 971 939 940 1033 66 697 155 156 785 787 24 1031 423 22 932 960 1002 961 934 935 1038 658 116 117 928 927 29 1033 336 24 932 968 1004 969 934 935 1038 477 658 116 117 568 567 29 937 938 1007 941 939 940 1039 435 794 344 345 706 705 30 1037 252 28 932 976 1008 977 934 935 
[0.154] The initially found plan has an effective length of 206.
[0.154] Added 239 permanent goal conditions.
[0.159] Added 57,361 clauses counting the effective plan length.
[0.159] Attempting to find a plan shorter than 206.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |       545 |   27744   248403   620932 |    91081      536     63 | 51.751 % |
c [minisat220] |       695 |   27422   248403   620932 |   100189      675     59 | 52.311 % |
c [minisat220] |       920 |   27209   149815   395082 |   110208      809     62 | 52.682 % |
c [minisat220] ===============================================================================
[0.243] Found a plan. New actual length: 201.
201
[0.244] Attempting to find a plan shorter than 201.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |      1056 |   27207   149815   395082 |    54932      944     59 | 52.685 % |
c [minisat220] |      1206 |   26711   148068   391280 |    60425     1079     57 | 53.548 % |
c [minisat220] ===============================================================================
[0.283] Found a plan. New actual length: 200.
200
[0.284] Attempting to find a plan shorter than 200.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |      1438 |   26681   148068   391280 |    54291     1310     56 | 53.600 % |
c [minisat220] |      1588 |   26681   141790   377766 |    59720     1453     57 | 53.600 % |
c [minisat220] |      1813 |   26678   141790   377766 |    65692     1676     61 | 53.605 % |
c [minisat220] |      2150 |   26449   139448   372752 |    72262     1993     61 | 54.003 % |
c [minisat220] |      2656 |   26397   138669   371081 |    79488     2483     60 | 54.094 % |
c [minisat220] ===============================================================================
[0.449] Found a plan. New actual length: 199.
199
[0.451] Attempting to find a plan shorter than 199.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |      2992 |   26231   137142   367640 |    50845     2767     59 | 54.383 % |
c [minisat220] |      3142 |   26173   137142   367640 |    55929     2913     58 | 54.483 % |
c [minisat220] |      3367 |   26109   137142   367640 |    61522     3136     59 | 54.595 % |
c [minisat220] |      3704 |   26011   135571   364270 |    67675     3456     58 | 54.765 % |
c [minisat220] |      4210 |   25845   134152   361107 |    74442     3915     59 | 55.054 % |
c [minisat220] |      4969 |   25644   132772   358002 |    81886     4572     57 | 55.403 % |
c [minisat220] |      6108 |   25533   131305   354831 |    90075     5606     60 | 55.596 % |
c [minisat220] |      7816 |   25360   129777   351500 |    99083     7212     58 | 55.897 % |
c [minisat220] |     10378 |   25235   128517   348794 |   108991     9623     64 | 56.115 % |
c [minisat220] ===============================================================================
[1.587] Found a plan. New actual length: 198.
198
[1.587] Attempting to find a plan shorter than 198.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |     14178 |   24991   126609   344670 |    46423    13232     69 | 56.539 % |
c [minisat220] |     14328 |   24991   126609   344670 |    51065    13382     69 | 56.539 % |
c [minisat220] |     14553 |   24989   126270   343939 |    56172    13564     69 | 56.542 % |
c [minisat220] ===============================================================================
[1.713] Found a plan. New actual length: 197.
197
[1.714] Attempting to find a plan shorter than 197.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |     14839 |   24978   126270   343939 |    46299    13845     69 | 56.562 % |
c [minisat220] |     14989 |   24974   126270   343939 |    50928    13993     69 | 56.569 % |
c [minisat220] |     15214 |   24974   126270   343939 |    56021    14218     69 | 56.569 % |
c [minisat220] |     15551 |   24960   126270   343939 |    61623    14548     69 | 56.593 % |
c [minisat220] |     16057 |   24944   125730   342708 |    67786    14927     69 | 56.621 % |
c [minisat220] |     16816 |   24907   125730   342708 |    74565    15670     69 | 56.685 % |
c [minisat220] ===============================================================================
[2.048] Found a plan. New actual length: 196.
196
[2.049] Attempting to find a plan shorter than 196.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |     17699 |   24878   125344   341837 |    45959    16394     69 | 56.736 % |
c [minisat220] |     17849 |   24876   125344   341837 |    50555    16543     70 | 56.739 % |
c [minisat220] |     18074 |   24855   125344   341837 |    55610    16765     69 | 56.776 % |
c [minisat220] |     18411 |   24855   124988   341034 |    61172    16810     71 | 56.776 % |
c [minisat220] |     18917 |   24843   124988   341034 |    67289    17311     70 | 56.796 % |
c [minisat220] |     19676 |   24809   124988   341034 |    74018    18059     71 | 56.856 % |
c [minisat220] |     20815 |   24710   123811   338445 |    81419    18774     73 | 57.028 % |
c [minisat220] |     22523 |   24684   123566   337913 |    89561    20302     76 | 57.073 % |
c [minisat220] |     25085 |   24544   123098   336803 |    98518    21945     79 | 57.316 % |
c [minisat220] ===============================================================================
[3.463] Found a plan. New actual length: 195.
195
[3.464] Attempting to find a plan shorter than 195.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |     25835 |   24456   122651   335795 |    44972    22687     80 | 57.469 % |
c [minisat220] |     25985 |   24454   122651   335795 |    49469    22836     80 | 57.473 % |
c [minisat220] |     26210 |   24446   122651   335795 |    54416    23058     80 | 57.487 % |
c [minisat220] |     26547 |   24446   122129   334583 |    59857    23229     80 | 57.487 % |
c [minisat220] |     27053 |   24446   122129   334583 |    65843    23735     80 | 57.487 % |
c [minisat220] |     27812 |   24443   122118   334555 |    72427    24474     81 | 57.492 % |
c [minisat220] |     28951 |   24438   122118   334555 |    79670    25611     81 | 57.501 % |
c [minisat220] |     30659 |   24426   121988   334261 |    87637    27311     80 | 57.522 % |
c [minisat220] |     33221 |   24411   121793   333862 |    96401    29859     80 | 57.548 % |
c [minisat220] |     37065 |   24393   121642   333544 |   106041    33554     81 | 57.579 % |
c [minisat220] |     42831 |   24375   121444   333112 |   116645    38912     82 | 57.610 % |
c [minisat220] |     51480 |   24300   120423   331039 |   128310    47110     82 | 57.741 % |
c [minisat220] ===============================================================================
[16.186] Found a plan. New actual length: 194.
194
[16.187] Attempting to find a plan shorter than 194.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |     55990 |   24248   119765   329677 |    43913    50444     82 | 57.831 % |
c [minisat220] |     56140 |   24248   119765   329677 |    48305    50594     81 | 57.831 % |
c [minisat220] |     56365 |   24248   119765   329677 |    53135    50819     81 | 57.831 % |
c [minisat220] |     56702 |   24244   119737   329621 |    58449    51152     81 | 57.838 % |
c [minisat220] |     57208 |   24244   119737   329621 |    64294    51658     81 | 57.838 % |
c [minisat220] |     57967 |   24242   119737   329621 |    70723    52416     81 | 57.842 % |
c [minisat220] |     59106 |   24242   119685   329517 |    77796    53513     80 | 57.842 % |
c [minisat220] |     60814 |   24228   119622   329363 |    85575    55216     80 | 57.866 % |
c [minisat220] |     63376 |   24224   119568   329255 |    94133    57776     79 | 57.873 % |
c [minisat220] |     67220 |   24218   119488   329095 |   103546    61582     78 | 57.883 % |
c [minisat220] |     72986 |   24215   119436   328991 |   113901    67326     78 | 57.889 % |
c [minisat220] ===============================================================================
[27.131] No such plan exists.
[27.131] Final plan of length 194 identified.
194
solution 1181 1
1040 571 299 300 751 750 31 937 949 950 951 939 940 1038 836 386 387 748 747 29 932 976 984 977 934 935 1039 165 886 434 435 616 615 30 932 980 986 981 934 935 1039 75 794 344 345 886 885 30 932 964 988 965 934 935 1039 435 526 254 255 886 885 30 937 974 991 975 939 940 1040 480 571 299 300 839 841 31 932 956 992 957 934 935 1039 345 706 164 165 794 796 30 937 954 995 955 939 940 1038 1178 206 207 658 657 29 937 944 997 945 939 940 1040 390 661 119 120 571 570 31 932 933 998 936 934 935 1039 255 616 74 75 886 885 30 932 968 1000 969 934 935 1039 435 706 164 165 794 796 30 937 962 1003 963 939 940 1038 1176 116 117 928 927 29 932 968 1004 969 934 935 1037 432 613 71 72 523 522 28 937 938 1007 941 939 940 1039 1181 344 345 706 705 30 1037 252 28 932 976 1008 977 934 935 
[27.132] Exiting.
