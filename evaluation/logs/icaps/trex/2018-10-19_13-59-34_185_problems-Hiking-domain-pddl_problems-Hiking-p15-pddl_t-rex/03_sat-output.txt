Command : ./interpreter_t-rex_minisat220 -P -b8192 -o -l-1 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.001] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.038] Processed problem encoding.
[0.045] Calculated possible fact changes of composite elements.
[0.045] Initialized instantiation procedure.
[0.045] 
[0.045] *************************************
[0.045] * * *   M a k e s p a n     0   * * *
[0.045] *************************************
[0.046] Instantiated 282 initial clauses.
[0.046] The encoding contains a total of 191 distinct variables.
[0.046] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.046] Executed solver; result: UNSAT.
[0.046] 
[0.046] *************************************
[0.046] * * *   M a k e s p a n     1   * * *
[0.046] *************************************
[0.046] Computed next depth properties: array size of 3.
[0.046] Instantiated 32 transitional clauses.
[0.047] Instantiated 354 universal clauses.
[0.047] Instantiated and added clauses for a total of 668 clauses.
[0.047] The encoding contains a total of 313 distinct variables.
[0.047] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.047] Executed solver; result: UNSAT.
[0.047] 
[0.047] *************************************
[0.047] * * *   M a k e s p a n     2   * * *
[0.047] *************************************
[0.047] Computed next depth properties: array size of 4.
[0.050] Instantiated 8,751 transitional clauses.
[0.054] Instantiated 39,817 universal clauses.
[0.054] Instantiated and added clauses for a total of 49,236 clauses.
[0.054] The encoding contains a total of 3,899 distinct variables.
[0.054] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.055] Executed solver; result: UNSAT.
[0.055] 
[0.055] *************************************
[0.055] * * *   M a k e s p a n     3   * * *
[0.055] *************************************
[0.056] Computed next depth properties: array size of 14.
[0.064] Instantiated 29,093 transitional clauses.
[0.070] Instantiated 51,489 universal clauses.
[0.070] Instantiated and added clauses for a total of 129,818 clauses.
[0.070] The encoding contains a total of 8,137 distinct variables.
[0.070] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.070] Executed solver; result: UNSAT.
[0.070] 
[0.070] *************************************
[0.070] * * *   M a k e s p a n     4   * * *
[0.070] *************************************
[0.072] Computed next depth properties: array size of 32.
[0.081] Instantiated 31,176 transitional clauses.
[0.086] Instantiated 22,394 universal clauses.
[0.086] Instantiated and added clauses for a total of 183,388 clauses.
[0.086] The encoding contains a total of 10,154 distinct variables.
[0.086] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.087] Executed solver; result: UNSAT.
[0.087] 
[0.087] *************************************
[0.087] * * *   M a k e s p a n     5   * * *
[0.087] *************************************
[0.089] Computed next depth properties: array size of 36.
[0.095] Instantiated 15,676 transitional clauses.
[0.100] Instantiated 9,592 universal clauses.
[0.100] Instantiated and added clauses for a total of 208,656 clauses.
[0.100] The encoding contains a total of 11,011 distinct variables.
[0.100] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.102] Executed solver; result: UNSAT.
[0.102] 
[0.102] *************************************
[0.102] * * *   M a k e s p a n     6   * * *
[0.102] *************************************
[0.104] Computed next depth properties: array size of 40.
[0.110] Instantiated 15,676 transitional clauses.
[0.114] Instantiated 9,868 universal clauses.
[0.114] Instantiated and added clauses for a total of 234,200 clauses.
[0.114] The encoding contains a total of 11,872 distinct variables.
[0.114] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.119] Executed solver; result: UNSAT.
[0.119] 
[0.119] *************************************
[0.119] * * *   M a k e s p a n     7   * * *
[0.119] *************************************
[0.121] Computed next depth properties: array size of 44.
[0.127] Instantiated 15,676 transitional clauses.
[0.132] Instantiated 10,144 universal clauses.
[0.132] Instantiated and added clauses for a total of 260,020 clauses.
[0.132] The encoding contains a total of 12,737 distinct variables.
[0.132] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |       148 |    9240   189548   851716 |    69500      144     31 | 27.450 % |
c [minisat220] ===============================================================================
[0.139] Executed solver; result: UNSAT.
[0.139] 
[0.139] *************************************
[0.139] * * *   M a k e s p a n     8   * * *
[0.139] *************************************
[0.142] Computed next depth properties: array size of 48.
[0.149] Instantiated 15,676 transitional clauses.
[0.154] Instantiated 10,420 universal clauses.
[0.154] Instantiated and added clauses for a total of 286,116 clauses.
[0.154] The encoding contains a total of 13,606 distinct variables.
[0.154] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |       322 |   10109   215541   994340 |    79031      318     30 | 25.696 % |
c [minisat220] |       472 |   10109   215541   994340 |    86934      468     28 | 25.696 % |
c [minisat220] ===============================================================================
[0.173] Executed solver; result: UNSAT.
[0.173] 
[0.173] *************************************
[0.173] * * *   M a k e s p a n     9   * * *
[0.173] *************************************
[0.176] Computed next depth properties: array size of 52.
[0.183] Instantiated 15,676 transitional clauses.
[0.190] Instantiated 10,696 universal clauses.
[0.190] Instantiated and added clauses for a total of 312,488 clauses.
[0.190] The encoding contains a total of 14,479 distinct variables.
[0.190] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |       769 |   10982   241810  1139202 |    88663      765     31 | 24.147 % |
c [minisat220] |       919 |   10982   241810  1139202 |    97530      915     61 | 24.147 % |
c [minisat220] |      1144 |   10982   241810  1139202 |   107283     1140     78 | 24.147 % |
c [minisat220] ===============================================================================
[0.235] Executed solver; result: SAT.
[0.235] Solver returned SAT; a solution has been found at makespan 9.
27
solution 1801 1
1801 426 416 427 417 667 665 664 666 663 428 391 392 416 490 480 491 481 670 668 669 671 672 492 455 456 480 
[0.236] The initially found plan has an effective length of 27.
[0.236] Added 52 permanent goal conditions.
[0.237] Added 2,757 clauses counting the effective plan length.
[0.237] Attempting to find a plan shorter than 27.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |      1415 |    7814   243465  1143113 |    89270     1411     86 | 50.883 % |
c [minisat220] |      1565 |    7735   125403   580932 |    98197     1438     83 | 51.380 % |
c [minisat220] |      1790 |    7572   125403   580932 |   108017     1661     76 | 52.404 % |
c [minisat220] |      2127 |    7448   125403   580932 |   118819     1997     69 | 53.184 % |
c [minisat220] ===============================================================================
[0.299] No such plan exists.
[0.299] Final plan of length 27 identified.
27
solution 1801 1
1801 426 416 427 417 667 665 664 666 663 428 391 392 416 490 480 491 481 670 668 669 671 672 492 455 456 480 
[0.300] Exiting.
