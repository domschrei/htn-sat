Command : ./interpreter_t-rex_minisat220 -P -b8192 -o -l-1 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.001] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.001] Processed problem encoding.
[0.001] Calculated possible fact changes of composite elements.
[0.001] Initialized instantiation procedure.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     0   * * *
[0.001] *************************************
[0.001] Instantiated 122 initial clauses.
[0.001] The encoding contains a total of 76 distinct variables.
[0.001] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.001] Executed solver; result: UNSAT.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     1   * * *
[0.001] *************************************
[0.001] Computed next depth properties: array size of 13.
[0.001] Instantiated 68 transitional clauses.
[0.001] Instantiated 352 universal clauses.
[0.001] Instantiated and added clauses for a total of 542 clauses.
[0.001] The encoding contains a total of 249 distinct variables.
[0.001] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.001] Executed solver; result: UNSAT.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     2   * * *
[0.001] *************************************
[0.001] Computed next depth properties: array size of 23.
[0.002] Instantiated 174 transitional clauses.
[0.002] Instantiated 810 universal clauses.
[0.002] Instantiated and added clauses for a total of 1,526 clauses.
[0.002] The encoding contains a total of 471 distinct variables.
[0.002] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.002] Executed solver; result: SAT.
[0.002] Solver returned SAT; a solution has been found at makespan 2.
11
solution 32 1
3 14 4 18 21 17 22 25 32 26 29 
[0.003] The initially found plan has an effective length of 11.
[0.003] Added 23 permanent goal conditions.
[0.003] Added 553 clauses counting the effective plan length.
[0.003] Attempting to find a plan shorter than 11.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.003] No such plan exists.
[0.003] Final plan of length 11 identified.
11
solution 32 1
3 14 4 18 21 17 22 25 32 26 29 
[0.004] Exiting.
