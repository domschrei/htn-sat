> Preprocessed problem (1528 ms)
Before elimination: 1453 tasks
After elimination: 1453 tasks
  1453/2427 tasks are reachable.
Initial task network before elimination: 14 tasks
Initial task network after elimination: 14 tasks
  234/854 facts are reachable.
> Created HtnProblemData instance (64 ms)
> Encoded problem into SAT (109 ms)
> Wrote formula to file (32 ms)
> Executed SAT solver (2862 ms)
> Decoded result (4 ms)
> Reported result (3 ms)
Total execution time: 4603ms. Exiting.
