> Preprocessed problem (252 ms)
Before elimination: 1872 tasks
After elimination: 1872 tasks
  1872/5259 tasks are reachable.
Initial task network before elimination: 19 tasks
Initial task network after elimination: 19 tasks
  169/640 facts are reachable.
> Created HtnProblemData instance (38 ms)
> Encoded problem into SAT (94 ms)
> Wrote formula to file (25 ms)
> Executed SAT solver (237 ms)
> Decoded result (2 ms)
> Reported result (2 ms)
Total execution time: 650ms. Exiting.
