Command : ./interpreter_t-rex_minisat220 -P -b8192 -o -l-1 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.001] Reading problem encoding from file "f.cnf".
[0.002] Parsed head comment information.
[0.079] Processed problem encoding.
[0.177] Calculated possible fact changes of composite elements.
[0.179] Initialized instantiation procedure.
[0.179] 
[0.179] *************************************
[0.179] * * *   M a k e s p a n     0   * * *
[0.179] *************************************
[0.181] Instantiated 10,577 initial clauses.
[0.181] The encoding contains a total of 5,870 distinct variables.
[0.181] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.181] Executed solver; result: UNSAT.
[0.181] 
[0.181] *************************************
[0.181] * * *   M a k e s p a n     1   * * *
[0.181] *************************************
[0.195] Computed next depth properties: array size of 81.
[0.208] Instantiated 21,239 transitional clauses.
[0.228] Instantiated 66,164 universal clauses.
[0.228] Instantiated and added clauses for a total of 97,980 clauses.
[0.228] The encoding contains a total of 30,559 distinct variables.
[0.228] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.228] Executed solver; result: UNSAT.
[0.228] 
[0.228] *************************************
[0.228] * * *   M a k e s p a n     2   * * *
[0.228] *************************************
[0.267] Computed next depth properties: array size of 145.
[0.327] Instantiated 125,585 transitional clauses.
[0.373] Instantiated 193,001 universal clauses.
[0.373] Instantiated and added clauses for a total of 416,566 clauses.
[0.373] The encoding contains a total of 98,052 distinct variables.
[0.373] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.373] Executed solver; result: UNSAT.
[0.373] 
[0.373] *************************************
[0.373] * * *   M a k e s p a n     3   * * *
[0.373] *************************************
[0.446] Computed next depth properties: array size of 253.
[0.582] Instantiated 327,809 transitional clauses.
[0.662] Instantiated 375,645 universal clauses.
[0.662] Instantiated and added clauses for a total of 1,120,020 clauses.
[0.662] The encoding contains a total of 195,223 distinct variables.
[0.662] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.745] Executed solver; result: UNSAT.
[0.745] 
[0.745] *************************************
[0.745] * * *   M a k e s p a n     4   * * *
[0.745] *************************************
[0.854] Computed next depth properties: array size of 385.
[1.032] Instantiated 446,020 transitional clauses.
[1.145] Instantiated 519,357 universal clauses.
[1.145] Instantiated and added clauses for a total of 2,085,397 clauses.
[1.145] The encoding contains a total of 302,579 distinct variables.
[1.145] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |       133 |  298969  2047852  5706574 |   751600      131     85 |  1.193 % |
c [minisat220] ===============================================================================
[1.378] Executed solver; result: SAT.
[1.378] Solver returned SAT; a solution has been found at makespan 4.
244
solution 1959 1
13 569 65 610 66 14 799 51 594 52 1023 35 307 5 282 49 317 50 6 36 766 7 286 49 318 50 8 885 47 383 31 365 32 48 785 1038 65 274 61 267 62 66 789 43 253 61 269 62 44 870 756 805 1045 45 120 17 94 9 84 10 18 46 776 21 98 22 821 65 607 13 566 9 558 10 14 66 756 775 17 571 18 1001 9 83 17 92 18 10 749 758 1028 59 672 7 618 49 662 50 8 60 1069 1189 39 183 25 171 26 40 1064 1182 65 678 49 658 7 619 8 50 66 757 1063 57 671 7 620 8 58 1171 59 672 7 618 49 660 50 8 60 757 1057 23 635 49 662 50 24 1162 65 678 49 658 7 617 8 50 66 757 1054 21 633 7 620 8 22 1156 41 449 3 411 4 42 1050 19 427 3 413 4 20 1113 59 673 25 636 26 60 757 1048 1187 9 622 25 642 59 674 60 26 10 757 1060 53 667 54 1166 41 723 42 1959 3 689 4 1679 41 728 42 1837 1901 29 363 30 1519 23 360 24 1770 29 364 30 1902 31 367 47 381 48 32 1339 1763 1838 71 74 17 94 18 72 1227 1709 
[1.381] The initially found plan has an effective length of 244.
[1.383] Added 385 permanent goal conditions.
[1.403] Added 148,611 clauses counting the effective plan length.
[1.403] Attempting to find a plan shorter than 244.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[1.467] Found a plan. New actual length: 241.
241
[1.468] Attempting to find a plan shorter than 241.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |       299 |  290003  2187407  6065352 |   802049      294    327 | 23.131 % |
c [minisat220] |       449 |  287933  2187407  6065352 |   882254      433    247 | 23.680 % |
c [minisat220] |       674 |  287932  1872771  5244187 |   970479      611    318 | 23.680 % |
c [minisat220] ===============================================================================
[2.348] Found a plan. New actual length: 222.
222
[2.348] Attempting to find a plan shorter than 222.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |       970 |  287728  1872771  5244187 |   686682      899    279 | 23.734 % |
c [minisat220] ===============================================================================
[2.693] Found a plan. New actual length: 219.
219
[2.694] Attempting to find a plan shorter than 219.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |      1136 |  285908  1869036  5235777 |   686682     1049    274 | 24.216 % |
c [minisat220] |      1286 |  285796  1869036  5235777 |   755350     1182    259 | 24.246 % |
c [minisat220] ===============================================================================
[3.156] Found a plan. New actual length: 208.
208
[3.157] Attempting to find a plan shorter than 208.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |      1414 |  284242  1869036  5235777 |   685313     1307    254 | 24.658 % |
c [minisat220] ===============================================================================
[3.370] Found a plan. New actual length: 199.
199
[3.371] Attempting to find a plan shorter than 199.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |      1552 |  282871  1869036  5235777 |   685313     1438    250 | 25.021 % |
c [minisat220] |      1702 |  281522  1830664  5147985 |   753844     1513    247 | 25.379 % |
c [minisat220] ===============================================================================
[3.746] Found a plan. New actual length: 196.
196
[3.747] Attempting to find a plan shorter than 196.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |      1823 |  278828  1830664  5147985 |   671243     1633    240 | 26.093 % |
c [minisat220] ===============================================================================
[4.030] Found a plan. New actual length: 194.
194
[4.030] Attempting to find a plan shorter than 194.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |      2002 |  277793  1830664  5147985 |   671243     1804    231 | 26.367 % |
c [minisat220] |      2152 |  275679  1791864  5057835 |   738367     1901    231 | 26.928 % |
c [minisat220] |      2377 |  275502  1791864  5057835 |   812204     2124    228 | 26.975 % |
c [minisat220] |      2714 |  275167  1787591  5048151 |   893425     2395    221 | 27.063 % |
c [minisat220] ===============================================================================
[5.732] Found a plan. New actual length: 190.
190
[5.733] Attempting to find a plan shorter than 190.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |      3198 |  273572  1778972  5028512 |   655450     2800    220 | 27.486 % |
c [minisat220] |      3348 |  273538  1778972  5028512 |   720995     2949    222 | 27.495 % |
c [minisat220] |      3573 |  273537  1778972  5028512 |   793094     3173    223 | 27.496 % |
c [minisat220] ===============================================================================
[6.643] Found a plan. New actual length: 187.
187
[6.644] Attempting to find a plan shorter than 187.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[6.734] Found a plan. New actual length: 184.
184
[6.735] Attempting to find a plan shorter than 184.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |      3970 |  272816  1773870  5017171 |   650419     3534    229 | 27.687 % |
c [minisat220] |      4120 |  272446  1773870  5017171 |   715460     3681    227 | 27.785 % |
c [minisat220] |      4345 |  272445  1773870  5017171 |   787006     3905    224 | 27.785 % |
c [minisat220] ===============================================================================
[7.588] Found a plan. New actual length: 183.
183
[7.589] Attempting to find a plan shorter than 183.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |      4693 |  271658  1763627  4994012 |   646663     4203    219 | 27.994 % |
c [minisat220] |      4843 |  271598  1763627  4994012 |   711329     4347    215 | 28.009 % |
c [minisat220] |      5068 |  271544  1763627  4994012 |   782462     4569    218 | 28.024 % |
c [minisat220] |      5405 |  271543  1762110  4990252 |   860708     4888    222 | 28.024 % |
c [minisat220] |      5911 |  271488  1762110  4990252 |   946779     5392    224 | 28.039 % |
c [minisat220] ===============================================================================
[9.185] Found a plan. New actual length: 181.
181
[9.186] Attempting to find a plan shorter than 181.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |      6453 |  271458  1761375  4988641 |   646107     5930    218 | 28.047 % |
c [minisat220] |      6603 |  271458  1761375  4988641 |   710717     6080    216 | 28.047 % |
c [minisat220] |      6828 |  271445  1761375  4988641 |   781789     6304    213 | 28.050 % |
c [minisat220] |      7165 |  271445  1761274  4988411 |   859968     6639    210 | 28.050 % |
c [minisat220] |      7671 |  271427  1761274  4988411 |   945965     7144    204 | 28.055 % |
c [minisat220] ===============================================================================
[11.004] Found a plan. New actual length: 179.
179
[11.005] Attempting to find a plan shorter than 179.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |      7911 |  271425  1761099  4988037 |   645800     7383    207 | 28.055 % |
c [minisat220] |      8061 |  271425  1761099  4988037 |   710380     7533    206 | 28.055 % |
c [minisat220] |      8286 |  271425  1761099  4988037 |   781418     7758    206 | 28.055 % |
c [minisat220] |      8623 |  271425  1761099  4988037 |   859560     8095    201 | 28.055 % |
c [minisat220] |      9129 |  271294  1759851  4985357 |   945516     8595    198 | 28.090 % |
c [minisat220] ===============================================================================
[12.770] No such plan exists.
[12.770] Final plan of length 179 identified.
179
solution 1887 1
45 121 27 103 28 46 794 51 127 27 101 28 52 836 13 568 59 599 60 14 769 7 555 8 998 749 782 1029 47 381 71 342 72 48 791 43 375 44 940 801 1033 59 600 13 566 9 559 10 14 60 756 781 21 575 22 1006 9 558 10 756 775 17 571 18 1001 753 761 1037 39 186 41 190 42 40 1067 1183 1066 1188 59 672 7 619 8 60 757 1063 57 671 7 620 8 58 1171 59 672 7 618 49 660 50 8 60 757 1057 23 635 49 662 50 24 1162 65 678 49 658 7 617 8 50 66 757 1054 21 633 7 620 8 22 1156 41 449 3 411 4 42 1050 19 427 3 413 4 20 1113 59 673 25 636 26 60 757 1048 1187 9 622 25 642 59 674 60 26 10 757 1060 53 667 54 1166 1887 1527 1773 1887 1433 1768 1887 1339 1763 1887 1197 1758 
[12.771] Exiting.
