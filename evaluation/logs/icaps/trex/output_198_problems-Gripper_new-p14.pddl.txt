> Preprocessed problem (135 ms)
Before elimination: 139 tasks
After elimination: 139 tasks
  139/261 tasks are reachable.
Initial task network before elimination: 15 tasks
Initial task network after elimination: 15 tasks
  124/316 facts are reachable.
> Created HtnProblemData instance (10 ms)
> Encoded problem into SAT (26 ms)
> Wrote formula to file (6 ms)
> Executed SAT solver (17 ms)
> Decoded result (2 ms)
> Reported result (2 ms)
Total execution time: 198ms. Exiting.
