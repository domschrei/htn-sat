Command : ./interpreter_t-rex_minisat220 -P -b8192 -o -l-1 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.001] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.046] Processed problem encoding.
[0.058] Calculated possible fact changes of composite elements.
[0.060] Initialized instantiation procedure.
[0.060] 
[0.060] *************************************
[0.060] * * *   M a k e s p a n     0   * * *
[0.060] *************************************
[0.071] Instantiated 21,758 initial clauses.
[0.071] The encoding contains a total of 11,239 distinct variables.
[0.071] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.071] Executed solver; result: UNSAT.
[0.071] 
[0.071] *************************************
[0.071] * * *   M a k e s p a n     1   * * *
[0.071] *************************************
[0.082] Computed next depth properties: array size of 89.
[0.088] Instantiated 1,312 transitional clauses.
[0.117] Instantiated 48,448 universal clauses.
[0.117] Instantiated and added clauses for a total of 71,518 clauses.
[0.117] The encoding contains a total of 23,278 distinct variables.
[0.117] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.117] Executed solver; result: UNSAT.
[0.117] 
[0.117] *************************************
[0.117] * * *   M a k e s p a n     2   * * *
[0.117] *************************************
[0.139] Computed next depth properties: array size of 132.
[0.171] Instantiated 73,231 transitional clauses.
[0.236] Instantiated 294,088 universal clauses.
[0.236] Instantiated and added clauses for a total of 438,837 clauses.
[0.236] The encoding contains a total of 77,591 distinct variables.
[0.236] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.236] Executed solver; result: UNSAT.
[0.236] 
[0.236] *************************************
[0.236] * * *   M a k e s p a n     3   * * *
[0.236] *************************************
[0.269] Computed next depth properties: array size of 218.
[0.323] Instantiated 178,835 transitional clauses.
[0.385] Instantiated 101,649 universal clauses.
[0.385] Instantiated and added clauses for a total of 719,321 clauses.
[0.385] The encoding contains a total of 91,403 distinct variables.
[0.385] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.386] Executed solver; result: UNSAT.
[0.386] 
[0.386] *************************************
[0.386] * * *   M a k e s p a n     4   * * *
[0.386] *************************************
[0.431] Computed next depth properties: array size of 304.
[0.459] Instantiated 2,658 transitional clauses.
[0.548] Instantiated 132,847 universal clauses.
[0.548] Instantiated and added clauses for a total of 854,826 clauses.
[0.548] The encoding contains a total of 113,322 distinct variables.
[0.548] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.548] Executed solver; result: UNSAT.
[0.548] 
[0.548] *************************************
[0.548] * * *   M a k e s p a n     5   * * *
[0.548] *************************************
[0.617] Computed next depth properties: array size of 390.
[0.697] Instantiated 150,945 transitional clauses.
[0.870] Instantiated 748,112 universal clauses.
[0.870] Instantiated and added clauses for a total of 1,753,883 clauses.
[0.870] The encoding contains a total of 227,336 distinct variables.
[0.870] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |       106 |  225438  1738128  4504227 |   637313      103    616 |  0.834 % |
c [minisat220] |       256 |  225438  1736658  4501282 |   701044      253    489 |  0.834 % |
c [minisat220] ===============================================================================
[1.021] Executed solver; result: SAT.
[1.021] Solver returned SAT; a solution has been found at makespan 5.
189
solution 6287 1
280 3538 292 3584 5 3638 11 3691 18 277 1197 289 1401 22 3796 31 1509 34 3902 43 282 5830 294 6287 50 4009 53 301 281 3543 293 4052 59 302 280 3548 292 4104 64 301 279 3497 291 4155 70 298 278 1147 290 1867 77 4280 84 4334 89 4387 96 4440 102 300 280 3556 292 4468 109 4546 114 4599 121 4652 129 301 281 3560 293 4676 138 302 280 3561 292 4728 145 2461 150 4863 157 299 277 1223 289 2545 161 301 281 3564 293 4936 171 302 280 3566 292 4988 175 298 278 1173 290 2699 179 5128 187 5182 194 301 279 3518 291 5195 201 299 277 1228 289 2909 206 300 280 3571 292 5300 215 301 281 3573 293 5352 223 302 280 3574 292 5404 230 301 281 3575 293 5456 239 302 280 3576 292 5508 245 301 281 3577 293 5560 251 298 278 1180 290 3271 253 302 280 3578 292 5664 261 301 281 3580 293 5716 269 302 279 3529 291 5767 273 3163 5610 
[1.032] The initially found plan has an effective length of 189.
[1.036] Added 390 permanent goal conditions.
[1.056] Added 152,491 clauses counting the effective plan length.
[1.056] Attempting to find a plan shorter than 189.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |       429 |  162577  1878690  4894419 |   688853      423    409 | 46.516 % |
c [minisat220] |       579 |  162576  1878690  4894419 |   757738      572    355 | 46.516 % |
c [minisat220] |       804 |  162565  1878690  4894419 |   833512      792    300 | 46.520 % |
c [minisat220] |      1141 |  162460  1878690  4894419 |   916863     1122    244 | 46.554 % |
c [minisat220] |      1647 |  162100   959050  2687789 |  1008549     1604    218 | 46.673 % |
c [minisat220] |      2406 |  161910   957829  2684737 |  1109404     2355    218 | 46.735 % |
c [minisat220] |      3545 |  161896   957766  2684596 |  1220345     3480    241 | 46.740 % |
c [minisat220] ===============================================================================
[2.858] Found a plan. New actual length: 179.
179
[2.859] Attempting to find a plan shorter than 179.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |      3813 |  161822   957766  2684596 |   351180     3744    241 | 46.764 % |
c [minisat220] ===============================================================================
[3.136] Found a plan. New actual length: 166.
166
[3.136] Attempting to find a plan shorter than 166.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[3.196] Found a plan. New actual length: 162.
162
[3.197] Attempting to find a plan shorter than 162.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |      4055 |  161821   957445  2683858 |   351063     3983    233 | 46.764 % |
c [minisat220] |      4205 |  161669   957445  2683858 |   386169     4130    229 | 46.814 % |
c [minisat220] |      4430 |  161669   957445  2683858 |   424786     4355    240 | 46.814 % |
c [minisat220] |      4767 |  161669   957445  2683858 |   467265     4692    243 | 46.814 % |
c [minisat220] ===============================================================================
[3.808] Found a plan. New actual length: 158.
158
[3.809] Attempting to find a plan shorter than 158.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[3.853] Found a plan. New actual length: 154.
154
[3.853] Attempting to find a plan shorter than 154.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[3.899] Found a plan. New actual length: 150.
150
[3.900] Attempting to find a plan shorter than 150.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[4.007] Found a plan. New actual length: 147.
147
[4.007] Attempting to find a plan shorter than 147.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |      5261 |  161189   956817  2682300 |   350832     5176    236 | 46.972 % |
c [minisat220] |      5411 |  160699   956817  2682300 |   385916     5323    235 | 47.133 % |
c [minisat220] |      5636 |  160699   954985  2677656 |   424507     5538    233 | 47.133 % |
c [minisat220] |      5973 |  160640   954985  2677656 |   466958     5873    239 | 47.153 % |
c [minisat220] |      6479 |  160496   954985  2677656 |   513654     6375    244 | 47.200 % |
c [minisat220] |      7238 |  160320   954057  2675468 |   565019     7122    250 | 47.258 % |
c [minisat220] |      8377 |  160320   953652  2674537 |   621521     8259    278 | 47.258 % |
c [minisat220] ===============================================================================
[6.031] Found a plan. New actual length: 142.
142
[6.032] Attempting to find a plan shorter than 142.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[6.085] Found a plan. New actual length: 138.
138
[6.086] Attempting to find a plan shorter than 138.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |      9854 |  159955   952466  2671875 |   349672     9707    276 | 47.378 % |
c [minisat220] ===============================================================================
[6.233] Found a plan. New actual length: 127.
127
[6.233] Attempting to find a plan shorter than 127.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[6.280] Found a plan. New actual length: 126.
126
[6.281] Attempting to find a plan shorter than 126.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |     10034 |  159693   952466  2671875 |   349237     9882    276 | 47.464 % |
c [minisat220] |     10184 |  159445   952466  2671875 |   384161    10024    275 | 47.546 % |
c [minisat220] |     10409 |  158684   952466  2671875 |   422577    10244    274 | 47.796 % |
c [minisat220] |     10746 |  158684   952466  2671875 |   464835    10581    272 | 47.796 % |
c [minisat220] |     11252 |  157447   945110  2653356 |   511318    11021    275 | 48.203 % |
c [minisat220] ===============================================================================
[7.382] Found a plan. New actual length: 118.
118
[7.382] Attempting to find a plan shorter than 118.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |     12085 |  157064   945110  2653356 |   346540    11850    277 | 48.329 % |
c [minisat220] ===============================================================================
[7.493] Found a plan. New actual length: 114.
114
[7.493] Attempting to find a plan shorter than 114.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |     12186 |  156777   945110  2653356 |   346540    11948    276 | 48.424 % |
c [minisat220] ===============================================================================
[7.636] Found a plan. New actual length: 110.
110
[7.636] Attempting to find a plan shorter than 110.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[7.690] Found a plan. New actual length: 106.
106
[7.690] Attempting to find a plan shorter than 106.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[7.763] Found a plan. New actual length: 104.
104
[7.763] Attempting to find a plan shorter than 104.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |     12406 |  156776   945110  2653356 |   346540    12167    278 | 48.424 % |
c [minisat220] |     12556 |  156776   943677  2649736 |   381194    12307    278 | 48.424 % |
c [minisat220] |     12781 |  156481   943677  2649736 |   419313    12530    276 | 48.521 % |
c [minisat220] ===============================================================================
[8.247] Found a plan. New actual length: 101.
101
[8.248] Attempting to find a plan shorter than 101.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |     13130 |  156480   943677  2649736 |   346014    12878    278 | 48.521 % |
c [minisat220] |     13280 |  155342   943677  2649736 |   380616    13027    277 | 48.896 % |
c [minisat220] |     13505 |  155342   943677  2649736 |   418678    13252    282 | 48.896 % |
c [minisat220] |     13842 |  153978   943677  2649736 |   460545    13577    281 | 49.345 % |
c [minisat220] |     14348 |  152865   935526  2628022 |   506600    13980    281 | 49.711 % |
c [minisat220] |     15107 |  152849   935526  2628022 |   557260    14723    282 | 49.716 % |
c [minisat220] |     16246 |  152844   935526  2628022 |   612986    15860    290 | 49.718 % |
c [minisat220] |     17954 |  152832   935188  2627237 |   674285    17539    296 | 49.722 % |
c [minisat220] ===============================================================================
[11.143] Found a plan. New actual length: 100.
100
[11.144] Attempting to find a plan shorter than 100.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[11.228] Found a plan. New actual length: 97.
97
[11.228] Attempting to find a plan shorter than 97.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |     19326 |  152515   935076  2627000 |   342861    18896    302 | 49.826 % |
c [minisat220] |     19476 |  152513   935076  2627000 |   377147    19044    302 | 49.826 % |
c [minisat220] |     19701 |  152512   935076  2627000 |   414862    19268    303 | 49.827 % |
c [minisat220] ===============================================================================
[11.741] Found a plan. New actual length: 96.
96
[11.742] Attempting to find a plan shorter than 96.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |     20075 |  152512   935076  2627000 |   342861    19642    304 | 49.827 % |
c [minisat220] ===============================================================================
[11.925] Found a plan. New actual length: 95.
95
[11.926] Attempting to find a plan shorter than 95.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[11.972] Found a plan. New actual length: 94.
94
[11.973] Attempting to find a plan shorter than 94.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |     20235 |  152512   935076  2627000 |   342861    19802    304 | 49.827 % |
c [minisat220] |     20385 |  152512   935076  2627000 |   377147    19952    305 | 49.827 % |
c [minisat220] |     20610 |  152512   932821  2621844 |   414862    20153    306 | 49.827 % |
c [minisat220] |     20947 |  152184   932821  2621844 |   456348    20489    304 | 49.935 % |
c [minisat220] ===============================================================================
[12.804] Found a plan. New actual length: 93.
93
[12.805] Attempting to find a plan shorter than 93.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |     21529 |  152184   932821  2621844 |   342034    21071    308 | 49.935 % |
c [minisat220] |     21679 |  152184   932821  2621844 |   376237    21221    307 | 49.935 % |
c [minisat220] |     21904 |  152184   932821  2621844 |   413861    21446    305 | 49.935 % |
c [minisat220] |     22241 |  152184   932821  2621844 |   455247    21783    306 | 49.935 % |
c [minisat220] |     22747 |  152184   932821  2621844 |   500772    22289    308 | 49.935 % |
c [minisat220] |     23506 |  152183   931475  2618826 |   550849    23045    312 | 49.935 % |
c [minisat220] |     24645 |  152183   931475  2618826 |   605934    24184    317 | 49.935 % |
c [minisat220] |     26353 |  152183   931475  2618826 |   666528    25892    324 | 49.935 % |
c [minisat220] ===============================================================================
[15.916] Found a plan. New actual length: 91.
91
[15.917] Attempting to find a plan shorter than 91.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[16.099] No such plan exists.
[16.099] Final plan of length 91 identified.
91
solution 5818 1
279 3486 291 3583 4 3638 10 3691 17 3744 24 3797 30 3850 36 3904 42 3957 47 4010 52 4064 57 4121 63 4174 70 4228 78 4281 84 4334 89 4387 96 4440 102 4493 108 4546 113 4599 120 4652 128 4705 136 4758 144 4811 151 4864 156 4917 163 4970 169 5023 174 5076 180 5129 186 5182 193 5235 201 5288 208 5341 214 5394 221 5447 229 5500 237 5553 244 5606 249 5659 254 5712 260 5765 267 5818 273 3122 5610 
[16.100] Exiting.
