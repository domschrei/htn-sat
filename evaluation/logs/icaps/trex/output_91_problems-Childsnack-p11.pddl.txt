> Preprocessed problem (3348 ms)
Before elimination: 3961 tasks
After elimination: 3961 tasks
  3961/3962 tasks are reachable.
Initial task network before elimination: 16 tasks
Initial task network after elimination: 16 tasks
  186/524 facts are reachable.
> Created HtnProblemData instance (523 ms)
> Encoded problem into SAT (508 ms)
> Wrote formula to file (333 ms)
> Executed SAT solver (1073 ms)
> Decoded result (3 ms)
> Reported result (2 ms)
Total execution time: 5790ms. Exiting.
