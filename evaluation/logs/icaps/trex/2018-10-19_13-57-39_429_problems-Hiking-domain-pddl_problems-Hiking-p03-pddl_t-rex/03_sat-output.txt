Command : ./interpreter_t-rex_minisat220 -P -b8192 -o -l-1 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.001] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.030] Processed problem encoding.
[0.034] Calculated possible fact changes of composite elements.
[0.034] Initialized instantiation procedure.
[0.034] 
[0.034] *************************************
[0.034] * * *   M a k e s p a n     0   * * *
[0.034] *************************************
[0.034] Instantiated 276 initial clauses.
[0.034] The encoding contains a total of 189 distinct variables.
[0.034] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.034] Executed solver; result: UNSAT.
[0.034] 
[0.034] *************************************
[0.034] * * *   M a k e s p a n     1   * * *
[0.034] *************************************
[0.035] Computed next depth properties: array size of 3.
[0.035] Instantiated 40 transitional clauses.
[0.035] Instantiated 354 universal clauses.
[0.035] Instantiated and added clauses for a total of 670 clauses.
[0.035] The encoding contains a total of 313 distinct variables.
[0.035] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.035] Executed solver; result: UNSAT.
[0.035] 
[0.035] *************************************
[0.035] * * *   M a k e s p a n     2   * * *
[0.035] *************************************
[0.035] Computed next depth properties: array size of 4.
[0.037] Instantiated 7,843 transitional clauses.
[0.039] Instantiated 34,617 universal clauses.
[0.039] Instantiated and added clauses for a total of 43,130 clauses.
[0.039] The encoding contains a total of 3,279 distinct variables.
[0.039] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.040] Executed solver; result: UNSAT.
[0.040] 
[0.040] *************************************
[0.040] * * *   M a k e s p a n     3   * * *
[0.040] *************************************
[0.041] Computed next depth properties: array size of 14.
[0.046] Instantiated 17,149 transitional clauses.
[0.050] Instantiated 41,111 universal clauses.
[0.050] Instantiated and added clauses for a total of 101,390 clauses.
[0.050] The encoding contains a total of 6,341 distinct variables.
[0.050] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.050] Executed solver; result: UNSAT.
[0.050] 
[0.050] *************************************
[0.050] * * *   M a k e s p a n     4   * * *
[0.050] *************************************
[0.052] Computed next depth properties: array size of 32.
[0.058] Instantiated 15,959 transitional clauses.
[0.063] Instantiated 21,797 universal clauses.
[0.063] Instantiated and added clauses for a total of 139,146 clauses.
[0.063] The encoding contains a total of 8,706 distinct variables.
[0.063] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.063] Executed solver; result: UNSAT.
[0.063] 
[0.063] *************************************
[0.063] * * *   M a k e s p a n     5   * * *
[0.063] *************************************
[0.066] Computed next depth properties: array size of 52.
[0.073] Instantiated 14,886 transitional clauses.
[0.078] Instantiated 20,817 universal clauses.
[0.078] Instantiated and added clauses for a total of 174,849 clauses.
[0.078] The encoding contains a total of 10,930 distinct variables.
[0.078] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.079] Executed solver; result: UNSAT.
[0.079] 
[0.079] *************************************
[0.079] * * *   M a k e s p a n     6   * * *
[0.079] *************************************
[0.082] Computed next depth properties: array size of 74.
[0.089] Instantiated 13,664 transitional clauses.
[0.096] Instantiated 18,844 universal clauses.
[0.096] Instantiated and added clauses for a total of 207,357 clauses.
[0.096] The encoding contains a total of 12,860 distinct variables.
[0.096] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.096] Executed solver; result: UNSAT.
[0.096] 
[0.096] *************************************
[0.096] * * *   M a k e s p a n     7   * * *
[0.096] *************************************
[0.099] Computed next depth properties: array size of 82.
[0.106] Instantiated 11,144 transitional clauses.
[0.113] Instantiated 14,996 universal clauses.
[0.113] Instantiated and added clauses for a total of 233,497 clauses.
[0.113] The encoding contains a total of 14,198 distinct variables.
[0.113] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.118] Executed solver; result: UNSAT.
[0.118] 
[0.118] *************************************
[0.118] * * *   M a k e s p a n     8   * * *
[0.118] *************************************
[0.123] Computed next depth properties: array size of 90.
[0.130] Instantiated 11,144 transitional clauses.
[0.139] Instantiated 15,402 universal clauses.
[0.139] Instantiated and added clauses for a total of 260,043 clauses.
[0.139] The encoding contains a total of 15,544 distinct variables.
[0.139] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.141] Executed solver; result: UNSAT.
[0.141] 
[0.141] *************************************
[0.141] * * *   M a k e s p a n     9   * * *
[0.141] *************************************
[0.146] Computed next depth properties: array size of 98.
[0.154] Instantiated 11,144 transitional clauses.
[0.161] Instantiated 15,808 universal clauses.
[0.161] Instantiated and added clauses for a total of 286,995 clauses.
[0.161] The encoding contains a total of 16,898 distinct variables.
[0.161] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |       147 |   13282   216398  1146327 |    79345      142    110 | 21.394 % |
c [minisat220] |       297 |   13282   216398  1146327 |    87280      292    248 | 21.394 % |
c [minisat220] ===============================================================================
[0.210] Executed solver; result: SAT.
[0.210] Solver returned SAT; a solution has been found at makespan 9.
45
solution 1533 1
1533 9 3 4 5 413 411 412 6 10 11 3 43 37 38 39 414 415 416 40 44 45 37 83 88 72 89 418 417 419 74 86 87 88 117 122 106 123 421 422 420 108 120 121 122 
[0.211] The initially found plan has an effective length of 45.
[0.211] Added 98 permanent goal conditions.
[0.213] Added 9,703 clauses counting the effective plan length.
[0.213] Attempting to find a plan shorter than 45.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |       550 |   15812   225739  1171604 |    82770      545    319 | 27.624 % |
c [minisat220] |       700 |   15713   225739  1171604 |    91048      694    291 | 28.077 % |
c [minisat220] |       925 |   15713   225739  1171604 |   100152      919    240 | 28.077 % |
c [minisat220] |      1262 |   15713   187102   914609 |   110168     1254    202 | 28.077 % |
c [minisat220] ===============================================================================
[0.285] No such plan exists.
[0.285] Final plan of length 45 identified.
45
solution 1533 1
1533 9 3 4 5 413 411 412 6 10 11 3 43 37 38 39 414 415 416 40 44 45 37 83 88 72 89 418 417 419 74 86 87 88 117 122 106 123 421 422 420 108 120 121 122 
[0.286] Exiting.
