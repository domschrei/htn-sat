Command : ./interpreter_t-rex_minisat220 -P -b8192 -o -l-1 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.001] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.007] Processed problem encoding.
[0.009] Calculated possible fact changes of composite elements.
[0.009] Initialized instantiation procedure.
[0.009] 
[0.009] *************************************
[0.009] * * *   M a k e s p a n     0   * * *
[0.009] *************************************
[0.009] Instantiated 1,597 initial clauses.
[0.009] The encoding contains a total of 907 distinct variables.
[0.009] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.010] Executed solver; result: UNSAT.
[0.010] 
[0.010] *************************************
[0.010] * * *   M a k e s p a n     1   * * *
[0.010] *************************************
[0.011] Computed next depth properties: array size of 45.
[0.012] Instantiated 1,683 transitional clauses.
[0.014] Instantiated 6,688 universal clauses.
[0.014] Instantiated and added clauses for a total of 9,968 clauses.
[0.014] The encoding contains a total of 3,303 distinct variables.
[0.014] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.014] Executed solver; result: UNSAT.
[0.014] 
[0.014] *************************************
[0.014] * * *   M a k e s p a n     2   * * *
[0.014] *************************************
[0.017] Computed next depth properties: array size of 81.
[0.022] Instantiated 7,643 transitional clauses.
[0.028] Instantiated 18,321 universal clauses.
[0.028] Instantiated and added clauses for a total of 35,932 clauses.
[0.028] The encoding contains a total of 8,391 distinct variables.
[0.028] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.028] Executed solver; result: UNSAT.
[0.028] 
[0.028] *************************************
[0.028] * * *   M a k e s p a n     3   * * *
[0.028] *************************************
[0.033] Computed next depth properties: array size of 142.
[0.042] Instantiated 18,061 transitional clauses.
[0.052] Instantiated 34,464 universal clauses.
[0.053] Instantiated and added clauses for a total of 88,457 clauses.
[0.053] The encoding contains a total of 15,539 distinct variables.
[0.053] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.063] Executed solver; result: SAT.
[0.063] Solver returned SAT; a solution has been found at makespan 3.
108
solution 334 1
5 53 6 122 9 61 10 145 13 37 3 31 4 14 125 19 46 20 137 115 119 155 3 30 13 39 14 4 115 123 15 42 16 136 5 52 6 116 164 7 58 8 193 17 45 15 43 16 18 115 171 21 47 22 188 19 88 5 77 6 20 169 11 82 12 213 5 79 19 87 20 6 117 161 237 334 3 76 19 88 20 4 244 312 15 42 16 331 17 45 18 291 309 15 41 16 329 13 37 3 28 4 14 257 25 27 26 303 
[0.065] The initially found plan has an effective length of 108.
[0.065] Added 142 permanent goal conditions.
[0.068] Added 20,307 clauses counting the effective plan length.
[0.068] Attempting to find a plan shorter than 108.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.072] Found a plan. New actual length: 102.
102
[0.073] Attempting to find a plan shorter than 102.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.075] Found a plan. New actual length: 96.
96
[0.076] Attempting to find a plan shorter than 96.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.085] Found a plan. New actual length: 90.
90
[0.085] Attempting to find a plan shorter than 90.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.088] Found a plan. New actual length: 87.
87
[0.089] Attempting to find a plan shorter than 87.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.093] Found a plan. New actual length: 84.
84
[0.094] Attempting to find a plan shorter than 84.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.097] Found a plan. New actual length: 81.
81
[0.098] Attempting to find a plan shorter than 81.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |       206 |   19189    82173   216087 |    30360      191     30 | 25.722 % |
c [minisat220] ===============================================================================
[0.125] Found a plan. New actual length: 77.
77
[0.126] Attempting to find a plan shorter than 77.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.128] Found a plan. New actual length: 74.
74
[0.129] Attempting to find a plan shorter than 74.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.131] Found a plan. New actual length: 71.
71
[0.132] Attempting to find a plan shorter than 71.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |       387 |   18423    79003   209018 |    28967      355     32 | 28.687 % |
c [minisat220] ===============================================================================
[0.154] Found a plan. New actual length: 65.
65
[0.154] Attempting to find a plan shorter than 65.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.159] No such plan exists.
[0.159] Final plan of length 65 identified.
65
solution 324 1
5 53 6 122 9 62 10 146 13 37 3 31 4 14 125 19 46 20 137 115 119 155 116 124 158 3 29 4 115 163 7 34 8 178 25 96 26 174 21 111 22 232 19 88 5 77 6 20 169 11 82 12 213 115 159 235 324 241 299 324 279 307 324 259 303 
[0.160] Exiting.
