Command : ./interpreter_t-rex_minisat220 -P -b8192 -o -l-1 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.001] Reading problem encoding from file "f.cnf".
[0.002] Parsed head comment information.
[0.287] Processed problem encoding.
[0.758] Calculated possible fact changes of composite elements.
[0.814] Initialized instantiation procedure.
[0.814] 
[0.814] *************************************
[0.814] * * *   M a k e s p a n     0   * * *
[0.814] *************************************
[0.826] Instantiated 43,263 initial clauses.
[0.826] The encoding contains a total of 34,826 distinct variables.
[0.826] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.826] Executed solver; result: UNSAT.
[0.826] 
[0.826] *************************************
[0.826] * * *   M a k e s p a n     1   * * *
[0.826] *************************************
[0.848] Computed next depth properties: array size of 57.
[0.906] Instantiated 126,827 transitional clauses.
[0.945] Instantiated 198,718 universal clauses.
[0.945] Instantiated and added clauses for a total of 368,808 clauses.
[0.945] The encoding contains a total of 75,532 distinct variables.
[0.945] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.946] Executed solver; result: UNSAT.
[0.946] 
[0.946] *************************************
[0.946] * * *   M a k e s p a n     2   * * *
[0.946] *************************************
[1.004] Computed next depth properties: array size of 169.
[1.230] Instantiated 331,039 transitional clauses.
[1.381] Instantiated 908,555 universal clauses.
[1.381] Instantiated and added clauses for a total of 1,608,402 clauses.
[1.381] The encoding contains a total of 236,854 distinct variables.
[1.381] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[1.381] Executed solver; result: UNSAT.
[1.381] 
[1.381] *************************************
[1.381] * * *   M a k e s p a n     3   * * *
[1.381] *************************************
[1.558] Computed next depth properties: array size of 337.
[2.697] Instantiated 1,828,141 transitional clauses.
[3.048] Instantiated 2,229,621 universal clauses.
[3.048] Instantiated and added clauses for a total of 5,666,164 clauses.
[3.048] The encoding contains a total of 574,456 distinct variables.
[3.049] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[3.050] Executed solver; result: UNSAT.
[3.050] 
[3.050] *************************************
[3.050] * * *   M a k e s p a n     4   * * *
[3.050] *************************************
[3.369] Computed next depth properties: array size of 505.
[5.674] Instantiated 3,663,982 transitional clauses.
[6.187] Instantiated 3,121,814 universal clauses.
[6.187] Instantiated and added clauses for a total of 12,451,960 clauses.
[6.187] The encoding contains a total of 960,015 distinct variables.
[6.187] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[6.767] Executed solver; result: SAT.
[6.767] Solver returned SAT; a solution has been found at makespan 4.
80
solution 2540 1
480 2540 690 1866 487 677 646 647 317 65 327 1439 793 410 179 30 360 1629 346 607 1508 565 332 155 22 372 2044 323 1032 1026 1030 509 209 38 492 1276 401 1120 1066 1067 517 776 777 66 3 402 557 558 320 109 16 405 202 37 254 53 431 1151 672 350 1359 364 964 921 922 246 48 75 9 463 102 12 288 63 492 210 39 478 237 47 
[6.774] The initially found plan has an effective length of 80.
[6.869] Added 505 permanent goal conditions.
[6.908] Added 255,531 clauses counting the effective plan length.
[6.908] Attempting to find a plan shorter than 80.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[7.249] No such plan exists.
[7.249] Final plan of length 80 identified.
80
solution 2540 1
480 2540 690 1866 487 677 646 647 317 65 327 1439 793 410 179 30 360 1629 346 607 1508 565 332 155 22 372 2044 323 1032 1026 1030 509 209 38 492 1276 401 1120 1066 1067 517 776 777 66 3 402 557 558 320 109 16 405 202 37 254 53 431 1151 672 350 1359 364 964 921 922 246 48 75 9 463 102 12 288 63 492 210 39 478 237 47 
[7.251] Exiting.
