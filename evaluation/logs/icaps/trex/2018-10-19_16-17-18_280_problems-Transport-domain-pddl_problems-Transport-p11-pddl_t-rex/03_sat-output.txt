Command : ./interpreter_t-rex_minisat220 -P -b8192 -o -l-1 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.001] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.001] Processed problem encoding.
[0.001] Calculated possible fact changes of composite elements.
[0.001] Initialized instantiation procedure.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     0   * * *
[0.001] *************************************
[0.001] Instantiated 272 initial clauses.
[0.001] The encoding contains a total of 176 distinct variables.
[0.001] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.001] Executed solver; result: UNSAT.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     1   * * *
[0.001] *************************************
[0.001] Computed next depth properties: array size of 17.
[0.001] Instantiated 396 transitional clauses.
[0.002] Instantiated 2,380 universal clauses.
[0.002] Instantiated and added clauses for a total of 3,048 clauses.
[0.002] The encoding contains a total of 629 distinct variables.
[0.002] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.002] Executed solver; result: UNSAT.
[0.002] 
[0.002] *************************************
[0.002] * * *   M a k e s p a n     2   * * *
[0.002] *************************************
[0.002] Computed next depth properties: array size of 25.
[0.002] Instantiated 446 transitional clauses.
[0.002] Instantiated 2,106 universal clauses.
[0.002] Instantiated and added clauses for a total of 5,600 clauses.
[0.002] The encoding contains a total of 981 distinct variables.
[0.002] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.002] Executed solver; result: UNSAT.
[0.002] 
[0.002] *************************************
[0.002] * * *   M a k e s p a n     3   * * *
[0.002] *************************************
[0.002] Computed next depth properties: array size of 33.
[0.003] Instantiated 498 transitional clauses.
[0.003] Instantiated 2,394 universal clauses.
[0.003] Instantiated and added clauses for a total of 8,492 clauses.
[0.003] The encoding contains a total of 1,361 distinct variables.
[0.003] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.003] Executed solver; result: SAT.
[0.003] Solver returned SAT; a solution has been found at makespan 3.
20
solution 94 1
93 67 92 11 85 87 37 86 83 88 7 89 55 94 17 84 25 85 87 5 
[0.004] The initially found plan has an effective length of 20.
[0.004] Added 33 permanent goal conditions.
[0.004] Added 1,123 clauses counting the effective plan length.
[0.004] Attempting to find a plan shorter than 20.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |       114 |    1307     6169    16040 |     2261      109     13 | 33.147 % |
c [minisat220] ===============================================================================
[0.009] Found a plan. New actual length: 18.
18
[0.010] Attempting to find a plan shorter than 18.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.011] No such plan exists.
[0.011] Final plan of length 18 identified.
18
solution 94 1
93 67 92 11 93 69 92 89 94 15 23 88 9 90 57 91 93 13 
[0.011] Exiting.
