Command : ./interpreter_t-rex_minisat220 -P -b8192 -o -l-1 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.001] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.009] Processed problem encoding.
[0.010] Calculated possible fact changes of composite elements.
[0.011] Initialized instantiation procedure.
[0.011] 
[0.011] *************************************
[0.011] * * *   M a k e s p a n     0   * * *
[0.011] *************************************
[0.011] Instantiated 1,973 initial clauses.
[0.011] The encoding contains a total of 1,332 distinct variables.
[0.011] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.011] Executed solver; result: UNSAT.
[0.011] 
[0.011] *************************************
[0.011] * * *   M a k e s p a n     1   * * *
[0.011] *************************************
[0.012] Computed next depth properties: array size of 17.
[0.013] Instantiated 3,111 transitional clauses.
[0.014] Instantiated 9,806 universal clauses.
[0.014] Instantiated and added clauses for a total of 14,890 clauses.
[0.014] The encoding contains a total of 3,608 distinct variables.
[0.014] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.014] Executed solver; result: UNSAT.
[0.014] 
[0.014] *************************************
[0.014] * * *   M a k e s p a n     2   * * *
[0.014] *************************************
[0.015] Computed next depth properties: array size of 49.
[0.018] Instantiated 8,326 transitional clauses.
[0.022] Instantiated 27,994 universal clauses.
[0.022] Instantiated and added clauses for a total of 51,210 clauses.
[0.022] The encoding contains a total of 8,260 distinct variables.
[0.022] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.023] Executed solver; result: UNSAT.
[0.023] 
[0.023] *************************************
[0.023] * * *   M a k e s p a n     3   * * *
[0.023] *************************************
[0.025] Computed next depth properties: array size of 97.
[0.031] Instantiated 20,237 transitional clauses.
[0.039] Instantiated 51,328 universal clauses.
[0.039] Instantiated and added clauses for a total of 122,775 clauses.
[0.039] The encoding contains a total of 15,854 distinct variables.
[0.039] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.042] Executed solver; result: SAT.
[0.042] Solver returned SAT; a solution has been found at makespan 3.
29
solution 279 1
44 279 184 80 93 94 26 10 34 180 181 83 155 57 157 24 9 67 135 137 59 29 11 61 108 109 44 17 8 
[0.043] The initially found plan has an effective length of 29.
[0.043] Added 97 permanent goal conditions.
[0.044] Added 9,507 clauses counting the effective plan length.
[0.044] Attempting to find a plan shorter than 29.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.045] Found a plan. New actual length: 27.
27
[0.046] Attempting to find a plan shorter than 27.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.047] No such plan exists.
[0.047] Final plan of length 27 identified.
27
solution 279 1
44 279 184 80 93 94 26 10 34 180 181 83 155 156 24 9 135 137 59 29 11 61 108 109 44 17 8 
[0.047] Exiting.
