> Preprocessed problem (340 ms)
Before elimination: 976 tasks
After elimination: 976 tasks
  976/2575 tasks are reachable.
Initial task network before elimination: 24 tasks
Initial task network after elimination: 24 tasks
  900/1858 facts are reachable.
> Created HtnProblemData instance (56 ms)
> Encoded problem into SAT (127 ms)
> Wrote formula to file (32 ms)
> Executed SAT solver (1764 ms)
> Decoded result (2 ms)
> Reported result (1 ms)
Total execution time: 2322ms. Exiting.
