Command : ./interpreter_t-rex_minisat220 -P -b8192 -o -l-1 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.001] Reading problem encoding from file "f.cnf".
[0.030] Parsed head comment information.
[2.621] Processed problem encoding.
[7.100] Calculated possible fact changes of composite elements.
[7.587] Initialized instantiation procedure.
[7.587] 
[7.587] *************************************
[7.587] * * *   M a k e s p a n     0   * * *
[7.587] *************************************
[8.960] Instantiated 1,058,281 initial clauses.
[8.960] The encoding contains a total of 533,377 distinct variables.
[8.960] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[8.973] Executed solver; result: UNSAT.
[8.973] 
[8.973] *************************************
[8.973] * * *   M a k e s p a n     1   * * *
[8.973] *************************************
Interrupt signal (15) received.
0
