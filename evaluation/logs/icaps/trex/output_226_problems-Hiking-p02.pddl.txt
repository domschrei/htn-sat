> Preprocessed problem (413 ms)
Before elimination: 1160 tasks
After elimination: 1160 tasks
  1160/1413 tasks are reachable.
Initial task network before elimination: 1 tasks
Initial task network after elimination: 1 tasks
  70/180 facts are reachable.
> Created HtnProblemData instance (53 ms)
> Encoded problem into SAT (162 ms)
> Wrote formula to file (43 ms)
> Executed SAT solver (107 ms)
> Decoded result (3 ms)
> Reported result (2 ms)
Total execution time: 783ms. Exiting.
