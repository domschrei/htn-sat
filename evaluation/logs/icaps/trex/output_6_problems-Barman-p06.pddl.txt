> Preprocessed problem (2325 ms)
Before elimination: 1775 tasks
After elimination: 1775 tasks
  1775/2697 tasks are reachable.
Initial task network before elimination: 14 tasks
Initial task network after elimination: 14 tasks
  267/920 facts are reachable.
> Created HtnProblemData instance (58 ms)
> Encoded problem into SAT (86 ms)
> Wrote formula to file (31 ms)
> Executed SAT solver (27144 ms)
> Decoded result (3 ms)
> Reported result (24 ms)
Total execution time: 29671ms. Exiting.
