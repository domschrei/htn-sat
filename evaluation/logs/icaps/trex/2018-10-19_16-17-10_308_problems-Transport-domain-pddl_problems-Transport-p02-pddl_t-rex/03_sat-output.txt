Command : ./interpreter_t-rex_minisat220 -P -b8192 -o -l-1 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.001] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.001] Processed problem encoding.
[0.001] Calculated possible fact changes of composite elements.
[0.001] Initialized instantiation procedure.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     0   * * *
[0.001] *************************************
[0.001] Instantiated 132 initial clauses.
[0.001] The encoding contains a total of 87 distinct variables.
[0.001] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.001] Executed solver; result: UNSAT.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     1   * * *
[0.001] *************************************
[0.001] Computed next depth properties: array size of 13.
[0.002] Instantiated 152 transitional clauses.
[0.002] Instantiated 892 universal clauses.
[0.002] Instantiated and added clauses for a total of 1,176 clauses.
[0.002] The encoding contains a total of 307 distinct variables.
[0.002] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.002] Executed solver; result: UNSAT.
[0.002] 
[0.002] *************************************
[0.002] * * *   M a k e s p a n     2   * * *
[0.002] *************************************
[0.002] Computed next depth properties: array size of 19.
[0.002] Instantiated 178 transitional clauses.
[0.002] Instantiated 815 universal clauses.
[0.002] Instantiated and added clauses for a total of 2,169 clauses.
[0.002] The encoding contains a total of 471 distinct variables.
[0.002] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.002] Executed solver; result: UNSAT.
[0.002] 
[0.002] *************************************
[0.002] * * *   M a k e s p a n     3   * * *
[0.002] *************************************
[0.003] Computed next depth properties: array size of 25.
[0.003] Instantiated 197 transitional clauses.
[0.003] Instantiated 915 universal clauses.
[0.003] Instantiated and added clauses for a total of 3,281 clauses.
[0.003] The encoding contains a total of 648 distinct variables.
[0.003] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.003] Executed solver; result: SAT.
[0.003] Solver returned SAT; a solution has been found at makespan 3.
21
solution 38 1
35 38 27 35 7 36 34 36 23 34 38 33 3 37 35 36 25 34 38 33 5 
[0.004] The initially found plan has an effective length of 21.
[0.004] Added 25 permanent goal conditions.
[0.004] Added 651 clauses counting the effective plan length.
[0.004] Attempting to find a plan shorter than 21.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.004] Found a plan. New actual length: 19.
19
[0.005] Attempting to find a plan shorter than 19.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.006] Found a plan. New actual length: 17.
17
[0.006] Attempting to find a plan shorter than 17.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.007] No such plan exists.
[0.007] Final plan of length 17 identified.
17
solution 38 1
27 35 7 36 23 34 38 33 3 37 35 36 25 34 38 33 5 
[0.007] Exiting.
