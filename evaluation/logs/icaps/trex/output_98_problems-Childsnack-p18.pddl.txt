> Preprocessed problem (10158 ms)
Before elimination: 9033 tasks
After elimination: 9033 tasks
  9033/9034 tasks are reachable.
Initial task network before elimination: 21 tasks
Initial task network after elimination: 21 tasks
  275/748 facts are reachable.
> Created HtnProblemData instance (1336 ms)
> Encoded problem into SAT (1996 ms)
> Wrote formula to file (909 ms)
> Executed SAT solver (3292 ms)
> Decoded result (3 ms)
> Reported result (2 ms)
Total execution time: 17696ms. Exiting.
