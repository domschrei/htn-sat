> Preprocessed problem (56 ms)
Before elimination: 153 tasks
After elimination: 153 tasks
  153/317 tasks are reachable.
Initial task network before elimination: 5 tasks
Initial task network after elimination: 5 tasks
  29/134 facts are reachable.
> Created HtnProblemData instance (13 ms)
> Encoded problem into SAT (21 ms)
> Wrote formula to file (7 ms)
> Executed SAT solver (19 ms)
> Decoded result (2 ms)
> Reported result (1 ms)
Total execution time: 119ms. Exiting.
