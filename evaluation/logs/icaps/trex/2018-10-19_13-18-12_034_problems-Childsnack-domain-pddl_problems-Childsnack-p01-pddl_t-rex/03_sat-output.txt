Command : ./interpreter_t-rex_minisat220 -P -b8192 -o -l-1 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.001] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.029] Processed problem encoding.
[0.030] Calculated possible fact changes of composite elements.
[0.033] Initialized instantiation procedure.
[0.033] 
[0.033] *************************************
[0.033] * * *   M a k e s p a n     0   * * *
[0.033] *************************************
[0.037] Instantiated 12,868 initial clauses.
[0.037] The encoding contains a total of 11,957 distinct variables.
[0.037] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.037] Executed solver; result: UNSAT.
[0.037] 
[0.037] *************************************
[0.037] * * *   M a k e s p a n     1   * * *
[0.037] *************************************
[0.041] Computed next depth properties: array size of 51.
[0.054] Instantiated 59,080 transitional clauses.
[0.064] Instantiated 88,126 universal clauses.
[0.064] Instantiated and added clauses for a total of 160,074 clauses.
[0.064] The encoding contains a total of 18,446 distinct variables.
[0.064] Attempting solve with solver <minisat220> ...
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] |       101 |   18311   157227   398212 |    57649      100   1689 |  0.726 % |
c [minisat220] ===============================================================================
[0.083] Executed solver; result: SAT.
[0.083] Solver returned SAT; a solution has been found at makespan 1.
50
solution 1100 1
271 32 8 33 10 683 3 294 424 296 160 72 294 327 296 231 25 8 346 10 753 44 378 945 380 825 97 384 1007 386 872 62 384 1031 386 548 65 4 1071 6 492 41 300 1100 302 50 55 384 404 386 
[0.084] The initially found plan has an effective length of 50.
[0.084] Added 51 permanent goal conditions.
[0.085] Added 2,653 clauses counting the effective plan length.
[0.085] Attempting to find a plan shorter than 50.
c [minisat220] ============================[ Search Statistics ]==============================
c [minisat220] | Conflicts |          ORIGINAL         |          LEARNT          | Progress |
c [minisat220] |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
c [minisat220] ===============================================================================
c [minisat220] ===============================================================================
[0.085] No such plan exists.
[0.085] Final plan of length 50 identified.
50
solution 1100 1
271 32 8 33 10 683 3 294 424 296 160 72 294 327 296 231 25 8 346 10 753 44 378 945 380 825 97 384 1007 386 872 62 384 1031 386 548 65 4 1071 6 492 41 300 1100 302 50 55 384 404 386 
[0.085] Exiting.
