> Preprocessed problem (1608 ms)
Before elimination: 1480 tasks
After elimination: 1480 tasks
  1480/2427 tasks are reachable.
Initial task network before elimination: 14 tasks
Initial task network after elimination: 14 tasks
  235/854 facts are reachable.
> Created HtnProblemData instance (68 ms)
> Encoded problem into SAT (89 ms)
> Wrote formula to file (30 ms)
> Executed SAT solver (1724 ms)
> Decoded result (3 ms)
> Reported result (4 ms)
Total execution time: 3526ms. Exiting.
