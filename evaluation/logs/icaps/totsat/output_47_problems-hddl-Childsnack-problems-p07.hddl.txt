PANDA - Planning and Acting in a Network Decomposition Architecture
Believe us: It's great, it's fantastic!

PANDA Copyright (C) 2014-2018 Gregor Behnke, Pascal Bercher, Thomas Geier, Kadir
Dede, Daniel Höller, Kristof Mickeleit, Matthias Englert
This program comes with ABSOLUTELY NO WARRANTY
This is free software, and you are welcome to redistribute it under certain
conditions; run PANDA with -license for details.

Main Developers:
- Gregor Behnke, http://www.uni-ulm.de/in/ki/behnke
- Daniel Höller, http://www.uni-ulm.de/in/ki/hoeller

With many thanks to various further contributors.
Run PANDA with the command line argument -contributors for an extensive list.

Run it with -help for more information like available options.


PANDA was called with: "-systemConfig AAAI-2018-totSAT(minisat) -programPath minisat=./minisat problems/hddl/Childsnack/domains/domain.hddl problems/hddl/Childsnack/problems/p07.hddl"


Planner Configuration
=====================
Domain: problems/hddl/Childsnack/domains/domain.hddl
Problem: problems/hddl/Childsnack/problems/p07.hddl
Output: none

Planning Configuration
======================
	printGeneralInformation : true
	printAdditionalData     : true
	random seed             : 42
	time limit (in seconds) : none

	external programs:
		minisat : ./minisat

	Parsing Configuration
	---------------------
	Parser                : autodetect file-type
	Expand Sort Hierarchy : true
	ClosedWordAssumption  : true
	CompileSHOPMethods    : true
	Eliminate Equality    : true
	Strip Hybridity       : true
	Reduce General Tasks  : true
	
	Preprocessing Configuration
	---------------------------
	Compile negative preconditions    : true
	Compile unit methods              : false
	Compile order in methods          : false
	Compile initial plan              : true
	Ensure Methods Have Last Task     : false
	Split independent parameters      : true
	Remove unnecessary predicates     : true
	Expand choiceless abstract tasks  : true
	Domain Cleanup                    : true
	Convert to SAS+                   : false
	Grounded Reachability Analysis    : Planning Graph (mutex-free)
	Grounded Task Decomposition Graph : Two Way TDG
	Iterate reachability analysis     : true
	Ground domain                     : true
	Iterate reachability analysis     : true
	Stop directly after grounding     : false
	
	SAT-Planning Configuration
	--------------------------
	solver           : minisat
	full planner run : true
	reduction method : only normalise 
	check result     : true
	
	Post-processing Configuration
	-----------------------------
	search status
	search result
	timings
	statistics
#0 "00 global:01:problem"="p07.hddl";"00 global:00:domain"="domain.hddl"
Parsing domain ... using HDDL parser ... done
Preparing internal domain representation ... done.
Initial domain
	number of abstract tasks = 1
	number of tasks = 10
	number of decomposition methods = 2
	number of predicates = 13
	number of sorts = 7
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 9
	number of constants = 68
Compiling negative preconditions ... done.
	number of abstract tasks = 1
	number of tasks = 10
	number of decomposition methods = 2
	number of predicates = 26
	number of sorts = 7
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 9
	number of constants = 68
Compiling split parameters ... done.
	number of abstract tasks = 1
	number of tasks = 10
	number of decomposition methods = 2
	number of predicates = 26
	number of sorts = 7
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 9
	number of constants = 68
Lifted reachability analysis and domain cleanup ... done.
	number of abstract tasks = 1
	number of tasks = 9
	number of decomposition methods = 2
	number of predicates = 15
	number of sorts = 7
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 8
	number of constants = 68
Grounded planning graph ... done.
	Number of Grounded Actions 21301
	Number of Grounded Literals 359
	number of abstract tasks = 1
	number of tasks = 9
	number of decomposition methods = 2
	number of predicates = 15
	number of sorts = 7
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 8
	number of constants = 68
Two Way TDG ... done.
	number of abstract tasks = 1
	number of tasks = 9
	number of decomposition methods = 2
	number of predicates = 15
	number of sorts = 7
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 8
	number of constants = 68
Grounding ... done.
	number of abstract tasks = 14
	number of tasks = 19127
	number of decomposition methods = 48678
	number of predicates = 252
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 19113
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 14
	number of tasks = 19127
	number of decomposition methods = 48678
	number of predicates = 168
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 19113
	number of constants = 0
Omitting lifted reachability analysis ... 
Grounded planning graph ... done.
	Number of Grounded Actions 19113
	Number of Grounded Literals 303
	number of abstract tasks = 14
	number of tasks = 19127
	number of decomposition methods = 48678
	number of predicates = 168
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 19113
	number of constants = 0
Two Way TDG ... done.
	number of abstract tasks = 14
	number of tasks = 19127
	number of decomposition methods = 48678
	number of predicates = 168
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 19113
	number of constants = 0
Compiling expand choiceless abstract tasks ... done.
	number of abstract tasks = 14
	number of tasks = 19127
	number of decomposition methods = 48678
	number of predicates = 168
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 19113
	number of constants = 0
Compiling abstract tasks without methods ... done.
	number of abstract tasks = 14
	number of tasks = 19127
	number of decomposition methods = 48678
	number of predicates = 168
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 19113
	number of constants = 0
Compiling initial plan ... done.
	number of abstract tasks = 15
	number of tasks = 19128
	number of decomposition methods = 48679
	number of predicates = 168
	number of sorts = 0
	number of tasks in largest method = 14
	number of epsilon methods = 0
	number of primitive tasks = 19113
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 15
	number of tasks = 19128
	number of decomposition methods = 48679
	number of predicates = 168
	number of sorts = 0
	number of tasks in largest method = 14
	number of epsilon methods = 0
	number of primitive tasks = 19113
	number of constants = 0
Omitting lifted reachability analysis ... 
Grounded planning graph ... done.
	Number of Grounded Actions 19113
	Number of Grounded Literals 303
	number of abstract tasks = 15
	number of tasks = 19128
	number of decomposition methods = 48679
	number of predicates = 168
	number of sorts = 0
	number of tasks in largest method = 14
	number of epsilon methods = 0
	number of primitive tasks = 19113
	number of constants = 0
Two Way TDG ... done.
	number of abstract tasks = 15
	number of tasks = 19128
	number of decomposition methods = 48679
	number of predicates = 168
	number of sorts = 0
	number of tasks in largest method = 14
	number of epsilon methods = 0
	number of primitive tasks = 19113
	number of constants = 0
Compiling expand choiceless abstract tasks ... done.
	number of abstract tasks = 14
	number of tasks = 19127
	number of decomposition methods = 48678
	number of predicates = 168
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 19113
	number of constants = 0
Compiling abstract tasks without methods ... done.
	number of abstract tasks = 14
	number of tasks = 19127
	number of decomposition methods = 48678
	number of predicates = 168
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 19113
	number of constants = 0
Compiling initial plan ... done.
	number of abstract tasks = 15
	number of tasks = 19128
	number of decomposition methods = 48679
	number of predicates = 168
	number of sorts = 0
	number of tasks in largest method = 14
	number of epsilon methods = 0
	number of primitive tasks = 19113
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 15
	number of tasks = 19128
	number of decomposition methods = 48679
	number of predicates = 168
	number of sorts = 0
	number of tasks in largest method = 14
	number of epsilon methods = 0
	number of primitive tasks = 19113
	number of constants = 0
Tasks 19113 - 0
Domain is acyclic: true
Domain is mostly acyclic: true
Domain is regular: false
Domain is tail recursive: true
Domain is totally ordered: true
Domain has last task in all methods: true
Time remaining for planner 9223372036854728186ms

Running SAT search with K = 0
Time remaining for SAT search 9223372036854728173ms
Time used for this run 9223372036854728173ms


Still waiting ... running for 102 will abort at 9223372036854728173
Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 183
NUMBER OF STATE CLAUSES 182
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 99.45% 0.55% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID c6558c72-07a2-4cbf-9647-ab7ae38b5e04
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
ERROR false

Running SAT search with K = 1
Time remaining for SAT search 9223372036854727868ms
Time used for this run 9223372036854727868ms


Still waiting ... running for 100 will abort at 9223372036854727868
Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 183
NUMBER OF STATE CLAUSES 182
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 99.45% 0.55% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 02c27a7c-5452-4a03-aa10-535215bff3ff
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
ERROR false

Running SAT search with K = 2
Time remaining for SAT search 9223372036854727666ms
Time used for this run 9223372036854727666ms


Still waiting ... running for 100 will abort at 9223372036854727666
Generating initial PDT ... done
Checking whether the PDT can grow any more ... no ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 1566906
NUMBER OF STATE CLAUSES 167510
NUMBER OF DECOMPOSITION CLAUSES 1399396
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 10.69% 89.31% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 637f7fd1-9cc7-4553-b8f1-7cfb55c2211d
FLUSH
CLOSE
NUMBER OF PATHS 84
Starting minisat
Setting starttime of solver to 1542618735660
Command exited with non-zero status 10
0.42 0.03

Time command gave the following runtime for the solver: 450
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... done
SAT-Solver says: SAT

extracting solution


CHECKING primitive solution of length 84 ...
true 27 SHOP_methodm0_serve_0_precondition[child1,sandw1,table2,bread3,content12]
true 4 make_sandwich_no_gluten[sandw1,bread3,content12]
true 41 put_on_tray[sandw1,tray1,kitchen]
true 36 move_tray[tray1,kitchen,table2]
true 8 serve_sandwich_no_gluten[sandw1,child1,tray1,table2]
true 21 move_tray[tray1,table2,kitchen]
true 93 SHOP_methodm1_serve_1_precondition[sandw14,table3,content3,bread8,child2]
true 96 make_sandwich[sandw14,bread8,content3]
true 28 put_on_tray[sandw14,tray1,kitchen]
true 31 move_tray[tray1,kitchen,table3]
true 26 serve_sandwich[sandw14,child2,tray1,table3]
true 75 move_tray[tray1,table3,kitchen]
true 97 SHOP_methodm1_serve_1_precondition[sandw5,table3,content14,bread4,child3]
true 98 make_sandwich[sandw5,bread4,content14]
true 57 put_on_tray[sandw5,tray3,kitchen]
true 44 move_tray[tray3,kitchen,table3]
true 63 serve_sandwich[sandw5,child3,tray3,table3]
true 16 move_tray[tray3,table3,kitchen]
true 12 SHOP_methodm0_serve_0_precondition[child4,sandw13,table3,bread5,content11]
true 78 make_sandwich_no_gluten[sandw13,bread5,content11]
true 30 put_on_tray[sandw13,tray2,kitchen]
true 53 move_tray[tray2,kitchen,table3]
true 59 serve_sandwich_no_gluten[sandw13,child4,tray2,table3]
true 20 move_tray[tray2,table3,kitchen]
true 37 SHOP_methodm0_serve_0_precondition[child5,sandw6,table2,bread2,content7]
true 38 make_sandwich_no_gluten[sandw6,bread2,content7]
true 29 put_on_tray[sandw6,tray3,kitchen]
true 61 move_tray[tray3,kitchen,table2]
true 7 serve_sandwich_no_gluten[sandw6,child5,tray3,table2]
true 43 move_tray[tray3,table2,kitchen]
true 89 SHOP_methodm1_serve_1_precondition[sandw18,table1,content10,bread14,child6]
true 49 make_sandwich[sandw18,bread14,content10]
true 33 put_on_tray[sandw18,tray2,kitchen]
true 17 move_tray[tray2,kitchen,table1]
true 77 serve_sandwich[sandw18,child6,tray2,table1]
true 86 move_tray[tray2,table1,kitchen]
true 3 SHOP_methodm1_serve_1_precondition[sandw10,table3,content13,bread11,child7]
true 46 make_sandwich[sandw10,bread11,content13]
true 81 put_on_tray[sandw10,tray3,kitchen]
true 40 move_tray[tray3,kitchen,table3]
true 54 serve_sandwich[sandw10,child7,tray3,table3]
true 25 move_tray[tray3,table3,kitchen]
true 22 SHOP_methodm0_serve_0_precondition[child8,sandw16,table1,bread13,content2]
true 42 make_sandwich_no_gluten[sandw16,bread13,content2]
true 79 put_on_tray[sandw16,tray1,kitchen]
true 50 move_tray[tray1,kitchen,table1]
true 19 serve_sandwich_no_gluten[sandw16,child8,tray1,table1]
true 45 move_tray[tray1,table1,kitchen]
true 69 SHOP_methodm1_serve_1_precondition[sandw9,table1,content8,bread10,child9]
true 15 make_sandwich[sandw9,bread10,content8]
true 14 put_on_tray[sandw9,tray2,kitchen]
true 68 move_tray[tray2,kitchen,table1]
true 13 serve_sandwich[sandw9,child9,tray2,table1]
true 91 move_tray[tray2,table1,kitchen]
true 80 SHOP_methodm1_serve_1_precondition[sandw7,table3,content6,bread9,child10]
true 82 make_sandwich[sandw7,bread9,content6]
true 67 put_on_tray[sandw7,tray3,kitchen]
true 11 move_tray[tray3,kitchen,table3]
true 9 serve_sandwich[sandw7,child10,tray3,table3]
true 90 move_tray[tray3,table3,kitchen]
true 35 SHOP_methodm1_serve_1_precondition[sandw2,table1,content1,bread7,child11]
true 76 make_sandwich[sandw2,bread7,content1]
true 32 put_on_tray[sandw2,tray3,kitchen]
true 64 move_tray[tray3,kitchen,table1]
true 39 serve_sandwich[sandw2,child11,tray3,table1]
true 34 move_tray[tray3,table1,kitchen]
true 51 SHOP_methodm1_serve_1_precondition[sandw8,table1,content9,bread12,child12]
true 0 make_sandwich[sandw8,bread12,content9]
true 6 put_on_tray[sandw8,tray1,kitchen]
true 72 move_tray[tray1,kitchen,table1]
true 1 serve_sandwich[sandw8,child12,tray1,table1]
true 55 move_tray[tray1,table1,kitchen]
true 62 SHOP_methodm1_serve_1_precondition[sandw3,table1,content4,bread1,child13]
true 65 make_sandwich[sandw3,bread1,content4]
true 66 put_on_tray[sandw3,tray1,kitchen]
true 47 move_tray[tray1,kitchen,table1]
true 85 serve_sandwich[sandw3,child13,tray1,table1]
true 52 move_tray[tray1,table1,kitchen]
true 84 SHOP_methodm0_serve_0_precondition[child14,sandw17,table2,bread6,content5]
true 88 make_sandwich_no_gluten[sandw17,bread6,content5]
true 56 put_on_tray[sandw17,tray3,kitchen]
true 48 move_tray[tray3,kitchen,table2]
true 10 serve_sandwich_no_gluten[sandw17,child14,tray3,table2]
true 92 move_tray[tray3,table2,kitchen]
 done.
ERROR false
Panda says: SOLUTION
============ global ============
randomseed     = 42
peak memory    = 4691319352
planner result = SOLUTION
============ properties ============
acyclic                  = true
mostly acyclic           = true
regular                  = false
tail recursive           = true
totally ordered          = true
last task in all methods = true
============ problem ============
number of constants         = 0
number of predicates        = 168
number of actions           = 19128
number of abstract actions  = 15
number of primitive actions = 19113
number of methods           = 48679
============ sat ============
plan length                     = -1
number of variables             = 97790
number of clauses               = 1566906
average size of clauses         = 2.076022428914051
number of assert                = 65
number of horn                  = 1564009
K offset                        = 0
K chosen value                  = 2
state formula                   = 167510
method children clauses         = 0
number of paths                 = 84
maximum plan length             = 84
number of decomposition clauses = 1399396
number of ordering clauses      = 0
number of state clauses         = 167510
solved                          = true
timeout                         = false

----------------- TIMINGS -----------------
============ total ============
total = 58151
============ parsing ============
total                         = 668
file parser                   = 423
sort expansion                = 97
closed world assumption       = 61
shop methods                  = 7
eliminate identical variables = 20
strip domain of hybridity     = 34
flatten formula               = 23
============ preprocessing ============
total                                      = 43287
compile negative preconditions             = 17
compile unit methods                       = 0
split parameter                            = 21
expand choiceless abstract tasks           = 664
expand choiceless abstract tasks           = 714
compile methods with identical tasks       = 1
removing unnecessary predicates            = 9956
lifted reachabiltiy analysis               = 35
grounded planning graph analysis           = 7319
grounded task decomposition graph analysis = 22809
grounding                                  = 1640
create artificial top task                 = 68
============ sat ============
total                                        = 7251
generate formula                             = 6505
generate path decomposition tree             = 2671
normalise path decomposition tree            = 1074
translate path decomposition tree to clauses = 1699
transform to DIMACS                          = 265
SAT solver                                   = 450
SAT solver for K=0002                        = 450

#1 "40 sat:90:solved"="true";"30 problem:05:number of primitive actions"="19113";"30 problem:01:number of constants"="0";"30 problem:04:number of abstract actions"="15";"02 properties:04:tail recursive"="true";"00 global:80:peak memory"="4691319352";"40 sat:20:state formula"="167510";"40 sat:01:number of variables"="97790";"40 sat:14:K offset"="0";"40 sat:30:number of paths"="84";"40 sat:00:plan length"="-1";"40 sat:50:number of ordering clauses"="0";"02 properties:02:mostly acyclic"="true";"30 problem:06:number of methods"="48679";"02 properties:05:totally ordered"="true";"02 properties:06:last task in all methods"="true";"30 problem:03:number of actions"="19128";"30 problem:02:number of predicates"="168";"40 sat:03:number of horn"="1564009";"40 sat:15:K chosen value"="2";"02 properties:03:regular"="false";"40 sat:03:average size of clauses"="2.076022428914051";"40 sat:02:number of clauses"="1566906";"40 sat:50:number of state clauses"="167510";"40 sat:03:number of assert"="65";"40 sat:22:method children clauses"="0";"00 global:90:planner result"="SOLUTION";"02 properties:01:acyclic"="true";"40 sat:31:maximum plan length"="84";"40 sat:50:number of decomposition clauses"="1399396";"00 global:02:randomseed"="42";"40 sat:91:timeout"="false";"01 parsing:01:file parser"="423";"40 sat:00:total"="7251";"40 sat:20:transform to DIMACS"="265";"40 sat:11:generate path decomposition tree"="2671";"02 preprocessing:07:compile methods with identical tasks"="1";"01 parsing:04:shop methods"="7";"02 preprocessing:08:removing unnecessary predicates"="9956";"01 parsing:03:closed world assumption"="61";"02 preprocessing:11:lifted reachabiltiy analysis"="35";"01 parsing:02:sort expansion"="97";"40 sat:12:normalise path decomposition tree"="1074";"40 sat:40:SAT solver"="450";"01 parsing:00:total"="668";"02 preprocessing:06:expand choiceless abstract tasks"="714";"01 parsing:06:strip domain of hybridity"="34";"40 sat:13:translate path decomposition tree to clauses"="1699";"40 sat:10:generate formula"="6505";"40 sat:41:SAT solver for K=0002"="450";"02 preprocessing:01:compile negative preconditions"="17";"00 total:00:total"="58151";"02 preprocessing:12:grounded planning graph analysis"="7319";"02 preprocessing:02:compile unit methods"="0";"02 preprocessing:23:grounded task decomposition graph analysis"="22809";"02 preprocessing:04:split parameter"="21";"01 parsing:07:flatten formula"="23";"02 preprocessing:05:expand choiceless abstract tasks"="664";"02 preprocessing:00:total"="43287";"02 preprocessing:99:create artificial top task"="68";"01 parsing:05:eliminate identical variables"="20";"02 preprocessing:84:grounding"="1640"
SOLUTION SEQUENCE
0: SHOP_methodm0_serve_0_precondition(child1,sandw1,table2,bread3,content12)
1: make_sandwich_no_gluten(sandw1,bread3,content12)
2: put_on_tray(sandw1,tray1,kitchen)
3: move_tray(tray1,kitchen,table2)
4: serve_sandwich_no_gluten(sandw1,child1,tray1,table2)
5: move_tray(tray1,table2,kitchen)
6: SHOP_methodm1_serve_1_precondition(sandw14,table3,content3,bread8,child2)
7: make_sandwich(sandw14,bread8,content3)
8: put_on_tray(sandw14,tray1,kitchen)
9: move_tray(tray1,kitchen,table3)
10: serve_sandwich(sandw14,child2,tray1,table3)
11: move_tray(tray1,table3,kitchen)
12: SHOP_methodm1_serve_1_precondition(sandw5,table3,content14,bread4,child3)
13: make_sandwich(sandw5,bread4,content14)
14: put_on_tray(sandw5,tray3,kitchen)
15: move_tray(tray3,kitchen,table3)
16: serve_sandwich(sandw5,child3,tray3,table3)
17: move_tray(tray3,table3,kitchen)
18: SHOP_methodm0_serve_0_precondition(child4,sandw13,table3,bread5,content11)
19: make_sandwich_no_gluten(sandw13,bread5,content11)
20: put_on_tray(sandw13,tray2,kitchen)
21: move_tray(tray2,kitchen,table3)
22: serve_sandwich_no_gluten(sandw13,child4,tray2,table3)
23: move_tray(tray2,table3,kitchen)
24: SHOP_methodm0_serve_0_precondition(child5,sandw6,table2,bread2,content7)
25: make_sandwich_no_gluten(sandw6,bread2,content7)
26: put_on_tray(sandw6,tray3,kitchen)
27: move_tray(tray3,kitchen,table2)
28: serve_sandwich_no_gluten(sandw6,child5,tray3,table2)
29: move_tray(tray3,table2,kitchen)
30: SHOP_methodm1_serve_1_precondition(sandw18,table1,content10,bread14,child6)
31: make_sandwich(sandw18,bread14,content10)
32: put_on_tray(sandw18,tray2,kitchen)
33: move_tray(tray2,kitchen,table1)
34: serve_sandwich(sandw18,child6,tray2,table1)
35: move_tray(tray2,table1,kitchen)
36: SHOP_methodm1_serve_1_precondition(sandw10,table3,content13,bread11,child7)
37: make_sandwich(sandw10,bread11,content13)
38: put_on_tray(sandw10,tray3,kitchen)
39: move_tray(tray3,kitchen,table3)
40: serve_sandwich(sandw10,child7,tray3,table3)
41: move_tray(tray3,table3,kitchen)
42: SHOP_methodm0_serve_0_precondition(child8,sandw16,table1,bread13,content2)
43: make_sandwich_no_gluten(sandw16,bread13,content2)
44: put_on_tray(sandw16,tray1,kitchen)
45: move_tray(tray1,kitchen,table1)
46: serve_sandwich_no_gluten(sandw16,child8,tray1,table1)
47: move_tray(tray1,table1,kitchen)
48: SHOP_methodm1_serve_1_precondition(sandw9,table1,content8,bread10,child9)
49: make_sandwich(sandw9,bread10,content8)
50: put_on_tray(sandw9,tray2,kitchen)
51: move_tray(tray2,kitchen,table1)
52: serve_sandwich(sandw9,child9,tray2,table1)
53: move_tray(tray2,table1,kitchen)
54: SHOP_methodm1_serve_1_precondition(sandw7,table3,content6,bread9,child10)
55: make_sandwich(sandw7,bread9,content6)
56: put_on_tray(sandw7,tray3,kitchen)
57: move_tray(tray3,kitchen,table3)
58: serve_sandwich(sandw7,child10,tray3,table3)
59: move_tray(tray3,table3,kitchen)
60: SHOP_methodm1_serve_1_precondition(sandw2,table1,content1,bread7,child11)
61: make_sandwich(sandw2,bread7,content1)
62: put_on_tray(sandw2,tray3,kitchen)
63: move_tray(tray3,kitchen,table1)
64: serve_sandwich(sandw2,child11,tray3,table1)
65: move_tray(tray3,table1,kitchen)
66: SHOP_methodm1_serve_1_precondition(sandw8,table1,content9,bread12,child12)
67: make_sandwich(sandw8,bread12,content9)
68: put_on_tray(sandw8,tray1,kitchen)
69: move_tray(tray1,kitchen,table1)
70: serve_sandwich(sandw8,child12,tray1,table1)
71: move_tray(tray1,table1,kitchen)
72: SHOP_methodm1_serve_1_precondition(sandw3,table1,content4,bread1,child13)
73: make_sandwich(sandw3,bread1,content4)
74: put_on_tray(sandw3,tray1,kitchen)
75: move_tray(tray1,kitchen,table1)
76: serve_sandwich(sandw3,child13,tray1,table1)
77: move_tray(tray1,table1,kitchen)
78: SHOP_methodm0_serve_0_precondition(child14,sandw17,table2,bread6,content5)
79: make_sandwich_no_gluten(sandw17,bread6,content5)
80: put_on_tray(sandw17,tray3,kitchen)
81: move_tray(tray3,kitchen,table2)
82: serve_sandwich_no_gluten(sandw17,child14,tray3,table2)
83: move_tray(tray3,table2,kitchen)
