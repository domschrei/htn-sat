PANDA - Planning and Acting in a Network Decomposition Architecture
Believe us: It's great, it's fantastic!

PANDA Copyright (C) 2014-2018 Gregor Behnke, Pascal Bercher, Thomas Geier, Kadir
Dede, Daniel Höller, Kristof Mickeleit, Matthias Englert
This program comes with ABSOLUTELY NO WARRANTY
This is free software, and you are welcome to redistribute it under certain
conditions; run PANDA with -license for details.

Main Developers:
- Gregor Behnke, http://www.uni-ulm.de/in/ki/behnke
- Daniel Höller, http://www.uni-ulm.de/in/ki/hoeller

With many thanks to various further contributors.
Run PANDA with the command line argument -contributors for an extensive list.

Run it with -help for more information like available options.


PANDA was called with: "-systemConfig AAAI-2018-totSAT(minisat) -programPath minisat=./minisat problems/hddl/Transport/domains/domain.hddl problems/hddl/Transport/problems/pfile07.hddl"


Planner Configuration
=====================
Domain: problems/hddl/Transport/domains/domain.hddl
Problem: problems/hddl/Transport/problems/pfile07.hddl
Output: none

Planning Configuration
======================
	printGeneralInformation : true
	printAdditionalData     : true
	random seed             : 42
	time limit (in seconds) : none

	external programs:
		minisat : ./minisat

	Parsing Configuration
	---------------------
	Parser                : autodetect file-type
	Expand Sort Hierarchy : true
	ClosedWordAssumption  : true
	CompileSHOPMethods    : true
	Eliminate Equality    : true
	Strip Hybridity       : true
	Reduce General Tasks  : true
	
	Preprocessing Configuration
	---------------------------
	Compile negative preconditions    : true
	Compile unit methods              : false
	Compile order in methods          : false
	Compile initial plan              : true
	Ensure Methods Have Last Task     : false
	Split independent parameters      : true
	Remove unnecessary predicates     : true
	Expand choiceless abstract tasks  : true
	Domain Cleanup                    : true
	Convert to SAS+                   : false
	Grounded Reachability Analysis    : Planning Graph (mutex-free)
	Grounded Task Decomposition Graph : Two Way TDG
	Iterate reachability analysis     : true
	Ground domain                     : true
	Iterate reachability analysis     : true
	Stop directly after grounding     : false
	
	SAT-Planning Configuration
	--------------------------
	solver           : minisat
	full planner run : true
	reduction method : only normalise 
	check result     : true
	
	Post-processing Configuration
	-----------------------------
	search status
	search result
	timings
	statistics
#0 "00 global:01:problem"="pfile07.hddl";"00 global:00:domain"="domain.hddl"
Parsing domain ... using HDDL parser ... done
Preparing internal domain representation ... done.
Initial domain
	number of abstract tasks = 4
	number of tasks = 8
	number of decomposition methods = 6
	number of predicates = 5
	number of sorts = 7
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 4
	number of constants = 15
Compiling negative preconditions ... done.
	number of abstract tasks = 4
	number of tasks = 8
	number of decomposition methods = 6
	number of predicates = 10
	number of sorts = 7
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 4
	number of constants = 15
Compiling split parameters ... done.
	number of abstract tasks = 4
	number of tasks = 8
	number of decomposition methods = 6
	number of predicates = 10
	number of sorts = 7
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 4
	number of constants = 15
Lifted reachability analysis and domain cleanup ... done.
	number of abstract tasks = 4
	number of tasks = 8
	number of decomposition methods = 6
	number of predicates = 5
	number of sorts = 7
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 4
	number of constants = 15
Grounded planning graph ... done.
	Number of Grounded Actions 49
	Number of Grounded Literals 62
	number of abstract tasks = 4
	number of tasks = 8
	number of decomposition methods = 6
	number of predicates = 5
	number of sorts = 7
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 4
	number of constants = 15
Two Way TDG ... done.
	number of abstract tasks = 4
	number of tasks = 8
	number of decomposition methods = 6
	number of predicates = 5
	number of sorts = 7
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 4
	number of constants = 15
Grounding ... done.
	number of abstract tasks = 29
	number of tasks = 78
	number of decomposition methods = 69
	number of predicates = 36
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 49
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 29
	number of tasks = 78
	number of decomposition methods = 69
	number of predicates = 26
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 49
	number of constants = 0
Omitting lifted reachability analysis ... 
Grounded planning graph ... done.
	Number of Grounded Actions 49
	Number of Grounded Literals 52
	number of abstract tasks = 29
	number of tasks = 78
	number of decomposition methods = 69
	number of predicates = 26
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 49
	number of constants = 0
Two Way TDG ... done.
	number of abstract tasks = 29
	number of tasks = 78
	number of decomposition methods = 69
	number of predicates = 26
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 49
	number of constants = 0
Compiling expand choiceless abstract tasks ... done.
	number of abstract tasks = 29
	number of tasks = 78
	number of decomposition methods = 69
	number of predicates = 26
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 49
	number of constants = 0
Compiling abstract tasks without methods ... done.
	number of abstract tasks = 29
	number of tasks = 78
	number of decomposition methods = 69
	number of predicates = 26
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 49
	number of constants = 0
Compiling initial plan ... done.
	number of abstract tasks = 30
	number of tasks = 79
	number of decomposition methods = 70
	number of predicates = 26
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 49
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 30
	number of tasks = 79
	number of decomposition methods = 70
	number of predicates = 26
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 49
	number of constants = 0
Omitting lifted reachability analysis ... 
Grounded planning graph ... done.
	Number of Grounded Actions 49
	Number of Grounded Literals 52
	number of abstract tasks = 30
	number of tasks = 79
	number of decomposition methods = 70
	number of predicates = 26
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 49
	number of constants = 0
Two Way TDG ... done.
	number of abstract tasks = 30
	number of tasks = 79
	number of decomposition methods = 70
	number of predicates = 26
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 49
	number of constants = 0
Compiling expand choiceless abstract tasks ... done.
	number of abstract tasks = 29
	number of tasks = 78
	number of decomposition methods = 69
	number of predicates = 26
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 49
	number of constants = 0
Compiling abstract tasks without methods ... done.
	number of abstract tasks = 29
	number of tasks = 78
	number of decomposition methods = 69
	number of predicates = 26
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 49
	number of constants = 0
Compiling initial plan ... done.
	number of abstract tasks = 30
	number of tasks = 79
	number of decomposition methods = 70
	number of predicates = 26
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 49
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 30
	number of tasks = 79
	number of decomposition methods = 70
	number of predicates = 26
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 49
	number of constants = 0
Tasks 49 - 0
Domain is acyclic: false
Domain is mostly acyclic: false
Domain is regular: false
Domain is tail recursive: false
Domain is totally ordered: true
Domain has last task in all methods: true
Time remaining for planner 9223372036854774669ms

Running SAT search with K = 0
Time remaining for SAT search 9223372036854774655ms
Time used for this run 9223372036854774655ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 27
NUMBER OF STATE CLAUSES 26
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 96.3% 3.7% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 00521079-2285-4adb-949e-e570a135e40e
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
Still waiting ... running for 102 will abort at 9223372036854774655
ERROR false

Running SAT search with K = 1
Time remaining for SAT search 9223372036854774550ms
Time used for this run 9223372036854774550ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 27
NUMBER OF STATE CLAUSES 26
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 96.3% 3.7% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID fb77a47f-ec98-458c-9ea6-daab5f189919
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
Still waiting ... running for 101 will abort at 9223372036854774550
ERROR false

Running SAT search with K = 2
Time remaining for SAT search 9223372036854774449ms
Time used for this run 9223372036854774449ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 27
NUMBER OF STATE CLAUSES 26
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 96.3% 3.7% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID f872dda2-acf5-400b-93b2-2dd7eeb62ba3
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
Still waiting ... running for 101 will abort at 9223372036854774449
ERROR false

Running SAT search with K = 3
Time remaining for SAT search 9223372036854774347ms
Time used for this run 9223372036854774347ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
Still waiting ... running for 100 will abort at 9223372036854774347
NUMBER OF CLAUSES 2524
NUMBER OF STATE CLAUSES 1625
NUMBER OF DECOMPOSITION CLAUSES 899
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 64.38% 35.62% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID e8c14294-7bb3-4da0-bc35-3a62d20231da
FLUSH
CLOSE
NUMBER OF PATHS 24
Starting minisat
Setting starttime of solver to 1539959297157
Command exited with non-zero status 20
0.00 0.00

Time command gave the following runtime for the solver: 0
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... done
SAT-Solver says: UNSAT
ERROR false

Running SAT search with K = 4
Time remaining for SAT search 9223372036854774124ms
Time used for this run 9223372036854774124ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 4416
NUMBER OF STATE CLAUSES 2414
NUMBER OF DECOMPOSITION CLAUSES 2002
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 54.660000000000004% 45.34% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID d64203ed-c51d-4c52-af67-8e618d97ff76
FLUSH
CLOSE
NUMBER OF PATHS 36
Starting minisat
Setting starttime of solver to 1539959297324
Command exited with non-zero status 20
0.00 0.00

Time command gave the following runtime for the solver: 0
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... done
SAT-Solver says: UNSAT
Still waiting ... running for 100 will abort at 9223372036854774124
ERROR false

Running SAT search with K = 5
Time remaining for SAT search 9223372036854774007ms
Time used for this run 9223372036854774007ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 7017
NUMBER OF STATE CLAUSES 3256
NUMBER OF DECOMPOSITION CLAUSES 3761
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 46.4% 53.6% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 7ed55728-f2f3-4bad-a09b-85962d518331
FLUSH
CLOSE
NUMBER OF PATHS 48
Starting minisat
Setting starttime of solver to 1539959297451
Command exited with non-zero status 10
0.00 0.00

Time command gave the following runtime for the solver: 0
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... Still waiting ... running for 100 will abort at 9223372036854774007
done
SAT-Solver says: SAT

extracting solution


CHECKING primitive solution of length 42 ...
true 44 drive[truck-0,city-loc-3,city-loc-4]
true 82 drive[truck-0,city-loc-4,city-loc-0]
true 38 pick-up[truck-0,city-loc-0,package-0,capacity-1,capacity-2]
true 88 drive[truck-0,city-loc-0,city-loc-4]
true 37 drive[truck-0,city-loc-4,city-loc-0]
true 67 drive[truck-0,city-loc-0,city-loc-2]
true 55 drop[truck-0,city-loc-2,package-0,capacity-1,capacity-2]
true 6 drive[truck-0,city-loc-2,city-loc-0]
true 56 drive[truck-0,city-loc-0,city-loc-4]
true 9 drive[truck-0,city-loc-4,city-loc-3]
true 24 pick-up[truck-0,city-loc-3,package-1,capacity-1,capacity-2]
true 34 drive[truck-0,city-loc-3,city-loc-4]
true 42 drive[truck-0,city-loc-4,city-loc-0]
true 4 drive[truck-0,city-loc-0,city-loc-2]
true 26 drop[truck-0,city-loc-2,package-1,capacity-1,capacity-2]
true 59 drive[truck-0,city-loc-2,city-loc-0]
true 3 drive[truck-0,city-loc-0,city-loc-2]
true 74 pick-up[truck-0,city-loc-2,package-2,capacity-1,capacity-2]
true 90 drive[truck-0,city-loc-2,city-loc-0]
true 57 drive[truck-0,city-loc-0,city-loc-4]
true 60 drop[truck-0,city-loc-4,package-2,capacity-1,capacity-2]
true 32 drive[truck-0,city-loc-4,city-loc-0]
true 80 drive[truck-0,city-loc-0,city-loc-4]
true 21 drive[truck-0,city-loc-4,city-loc-1]
true 65 pick-up[truck-0,city-loc-1,package-3,capacity-1,capacity-2]
true 77 drive[truck-0,city-loc-1,city-loc-4]
true 11 drive[truck-0,city-loc-4,city-loc-3]
true 75 drop[truck-0,city-loc-3,package-3,capacity-1,capacity-2]
true 47 drive[truck-0,city-loc-3,city-loc-4]
true 15 drive[truck-0,city-loc-4,city-loc-0]
true 64 drive[truck-0,city-loc-0,city-loc-4]
true 71 pick-up[truck-0,city-loc-4,package-4,capacity-1,capacity-2]
true 49 drive[truck-0,city-loc-4,city-loc-0]
true 84 drive[truck-0,city-loc-0,city-loc-4]
true 31 drive[truck-0,city-loc-4,city-loc-0]
true 51 drop[truck-0,city-loc-0,package-4,capacity-1,capacity-2]
true 27 drive[truck-0,city-loc-0,city-loc-4]
true 45 drive[truck-0,city-loc-4,city-loc-1]
true 20 pick-up[truck-0,city-loc-1,package-5,capacity-1,capacity-2]
true 19 drive[truck-0,city-loc-1,city-loc-4]
true 23 drive[truck-0,city-loc-4,city-loc-3]
true 39 drop[truck-0,city-loc-3,package-5,capacity-1,capacity-2]
 done.
ERROR false
Panda says: SOLUTION
============ global ============
randomseed     = 42
peak memory    = 251658240
planner result = SOLUTION
============ properties ============
acyclic                  = false
mostly acyclic           = false
regular                  = false
tail recursive           = false
totally ordered          = true
last task in all methods = true
============ problem ============
number of constants         = 0
number of predicates        = 26
number of actions           = 79
number of abstract actions  = 30
number of primitive actions = 49
number of methods           = 70
============ sat ============
plan length                     = -1
number of variables             = 2306
number of clauses               = 7017
average size of clauses         = 2.227305116146501
number of assert                = 9
number of horn                  = 6168
K offset                        = 0
K chosen value                  = 5
state formula                   = 3256
method children clauses         = 0
number of paths                 = 48
maximum plan length             = 48
number of decomposition clauses = 3761
number of ordering clauses      = 0
number of state clauses         = 3256
solved                          = true
timeout                         = false

----------------- TIMINGS -----------------
============ total ============
total = 2020
============ parsing ============
total                         = 575
file parser                   = 407
sort expansion                = 71
closed world assumption       = 38
shop methods                  = 2
eliminate identical variables = 16
strip domain of hybridity     = 25
flatten formula               = 16
============ preprocessing ============
total                                      = 525
compile negative preconditions             = 12
compile unit methods                       = 0
split parameter                            = 16
expand choiceless abstract tasks           = 18
expand choiceless abstract tasks           = 1
compile methods with identical tasks       = 1
removing unnecessary predicates            = 29
lifted reachabiltiy analysis               = 25
grounded planning graph analysis           = 169
grounded task decomposition graph analysis = 195
grounding                                  = 48
create artificial top task                 = 2
============ sat ============
total                                        = 389
generate formula                             = 331
generate path decomposition tree             = 89
normalise path decomposition tree            = 38
translate path decomposition tree to clauses = 108
transform to DIMACS                          = 22
SAT solver                                   = 0
SAT solver for K=0003                        = 0
SAT solver for K=0004                        = 0
SAT solver for K=0005                        = 0

#1 "40 sat:90:solved"="true";"30 problem:05:number of primitive actions"="49";"30 problem:01:number of constants"="0";"30 problem:04:number of abstract actions"="30";"02 properties:04:tail recursive"="false";"00 global:80:peak memory"="251658240";"40 sat:20:state formula"="3256";"40 sat:01:number of variables"="2306";"40 sat:14:K offset"="0";"40 sat:30:number of paths"="48";"40 sat:00:plan length"="-1";"40 sat:50:number of ordering clauses"="0";"02 properties:02:mostly acyclic"="false";"30 problem:06:number of methods"="70";"02 properties:05:totally ordered"="true";"02 properties:06:last task in all methods"="true";"30 problem:03:number of actions"="79";"30 problem:02:number of predicates"="26";"40 sat:03:number of horn"="6168";"40 sat:15:K chosen value"="5";"02 properties:03:regular"="false";"40 sat:03:average size of clauses"="2.227305116146501";"40 sat:02:number of clauses"="7017";"40 sat:50:number of state clauses"="3256";"40 sat:03:number of assert"="9";"40 sat:22:method children clauses"="0";"00 global:90:planner result"="SOLUTION";"02 properties:01:acyclic"="false";"40 sat:31:maximum plan length"="48";"40 sat:50:number of decomposition clauses"="3761";"00 global:02:randomseed"="42";"40 sat:91:timeout"="false";"01 parsing:01:file parser"="407";"40 sat:41:SAT solver for K=0004"="0";"40 sat:00:total"="389";"40 sat:20:transform to DIMACS"="22";"40 sat:11:generate path decomposition tree"="89";"02 preprocessing:07:compile methods with identical tasks"="1";"01 parsing:04:shop methods"="2";"02 preprocessing:08:removing unnecessary predicates"="29";"01 parsing:03:closed world assumption"="38";"02 preprocessing:11:lifted reachabiltiy analysis"="25";"01 parsing:02:sort expansion"="71";"40 sat:12:normalise path decomposition tree"="38";"40 sat:40:SAT solver"="0";"01 parsing:00:total"="575";"02 preprocessing:06:expand choiceless abstract tasks"="1";"01 parsing:06:strip domain of hybridity"="25";"40 sat:13:translate path decomposition tree to clauses"="108";"40 sat:10:generate formula"="331";"40 sat:41:SAT solver for K=0005"="0";"02 preprocessing:01:compile negative preconditions"="12";"00 total:00:total"="2020";"02 preprocessing:12:grounded planning graph analysis"="169";"02 preprocessing:02:compile unit methods"="0";"02 preprocessing:23:grounded task decomposition graph analysis"="195";"02 preprocessing:04:split parameter"="16";"01 parsing:07:flatten formula"="16";"02 preprocessing:05:expand choiceless abstract tasks"="18";"40 sat:41:SAT solver for K=0003"="0";"02 preprocessing:00:total"="525";"02 preprocessing:99:create artificial top task"="2";"01 parsing:05:eliminate identical variables"="16";"02 preprocessing:84:grounding"="48"
SOLUTION SEQUENCE
0: drive(truck-0,city-loc-3,city-loc-4)
1: drive(truck-0,city-loc-4,city-loc-0)
2: pick-up(truck-0,city-loc-0,package-0,capacity-1,capacity-2)
3: drive(truck-0,city-loc-0,city-loc-4)
4: drive(truck-0,city-loc-4,city-loc-0)
5: drive(truck-0,city-loc-0,city-loc-2)
6: drop(truck-0,city-loc-2,package-0,capacity-1,capacity-2)
7: drive(truck-0,city-loc-2,city-loc-0)
8: drive(truck-0,city-loc-0,city-loc-4)
9: drive(truck-0,city-loc-4,city-loc-3)
10: pick-up(truck-0,city-loc-3,package-1,capacity-1,capacity-2)
11: drive(truck-0,city-loc-3,city-loc-4)
12: drive(truck-0,city-loc-4,city-loc-0)
13: drive(truck-0,city-loc-0,city-loc-2)
14: drop(truck-0,city-loc-2,package-1,capacity-1,capacity-2)
15: drive(truck-0,city-loc-2,city-loc-0)
16: drive(truck-0,city-loc-0,city-loc-2)
17: pick-up(truck-0,city-loc-2,package-2,capacity-1,capacity-2)
18: drive(truck-0,city-loc-2,city-loc-0)
19: drive(truck-0,city-loc-0,city-loc-4)
20: drop(truck-0,city-loc-4,package-2,capacity-1,capacity-2)
21: drive(truck-0,city-loc-4,city-loc-0)
22: drive(truck-0,city-loc-0,city-loc-4)
23: drive(truck-0,city-loc-4,city-loc-1)
24: pick-up(truck-0,city-loc-1,package-3,capacity-1,capacity-2)
25: drive(truck-0,city-loc-1,city-loc-4)
26: drive(truck-0,city-loc-4,city-loc-3)
27: drop(truck-0,city-loc-3,package-3,capacity-1,capacity-2)
28: drive(truck-0,city-loc-3,city-loc-4)
29: drive(truck-0,city-loc-4,city-loc-0)
30: drive(truck-0,city-loc-0,city-loc-4)
31: pick-up(truck-0,city-loc-4,package-4,capacity-1,capacity-2)
32: drive(truck-0,city-loc-4,city-loc-0)
33: drive(truck-0,city-loc-0,city-loc-4)
34: drive(truck-0,city-loc-4,city-loc-0)
35: drop(truck-0,city-loc-0,package-4,capacity-1,capacity-2)
36: drive(truck-0,city-loc-0,city-loc-4)
37: drive(truck-0,city-loc-4,city-loc-1)
38: pick-up(truck-0,city-loc-1,package-5,capacity-1,capacity-2)
39: drive(truck-0,city-loc-1,city-loc-4)
40: drive(truck-0,city-loc-4,city-loc-3)
41: drop(truck-0,city-loc-3,package-5,capacity-1,capacity-2)
