PANDA - Planning and Acting in a Network Decomposition Architecture
Believe us: It's great, it's fantastic!

PANDA Copyright (C) 2014-2018 Gregor Behnke, Pascal Bercher, Thomas Geier, Kadir
Dede, Daniel Höller, Kristof Mickeleit, Matthias Englert
This program comes with ABSOLUTELY NO WARRANTY
This is free software, and you are welcome to redistribute it under certain
conditions; run PANDA with -license for details.

Main Developers:
- Gregor Behnke, http://www.uni-ulm.de/in/ki/behnke
- Daniel Höller, http://www.uni-ulm.de/in/ki/hoeller

With many thanks to various further contributors.
Run PANDA with the command line argument -contributors for an extensive list.

Run it with -help for more information like available options.


PANDA was called with: "-systemConfig AAAI-2018-totSAT(minisat) -programPath minisat=./minisat problems/hddl/Depots/domains/domain.hddl problems/hddl/Depots/problems/p03.hddl"


Planner Configuration
=====================
Domain: problems/hddl/Depots/domains/domain.hddl
Problem: problems/hddl/Depots/problems/p03.hddl
Output: none

Planning Configuration
======================
	printGeneralInformation : true
	printAdditionalData     : true
	random seed             : 42
	time limit (in seconds) : none

	external programs:
		minisat : ./minisat

	Parsing Configuration
	---------------------
	Parser                : autodetect file-type
	Expand Sort Hierarchy : true
	ClosedWordAssumption  : true
	CompileSHOPMethods    : true
	Eliminate Equality    : true
	Strip Hybridity       : true
	Reduce General Tasks  : true
	
	Preprocessing Configuration
	---------------------------
	Compile negative preconditions    : true
	Compile unit methods              : false
	Compile order in methods          : false
	Compile initial plan              : true
	Ensure Methods Have Last Task     : false
	Split independent parameters      : true
	Remove unnecessary predicates     : true
	Expand choiceless abstract tasks  : true
	Domain Cleanup                    : true
	Convert to SAS+                   : false
	Grounded Reachability Analysis    : Planning Graph (mutex-free)
	Grounded Task Decomposition Graph : Two Way TDG
	Iterate reachability analysis     : true
	Ground domain                     : true
	Iterate reachability analysis     : true
	Stop directly after grounding     : false
	
	SAT-Planning Configuration
	--------------------------
	solver           : minisat
	full planner run : true
	reduction method : only normalise 
	check result     : true
	
	Post-processing Configuration
	-----------------------------
	search status
	search result
	timings
	statistics
#0 "00 global:01:problem"="p03.hddl";"00 global:00:domain"="domain.hddl"
Parsing domain ... using HDDL parser ... done
Preparing internal domain representation ... done.
Initial domain
	number of abstract tasks = 6
	number of tasks = 23
	number of decomposition methods = 12
	number of predicates = 6
	number of sorts = 10
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 17
	number of constants = 17
Compiling negative preconditions ... done.
	number of abstract tasks = 6
	number of tasks = 23
	number of decomposition methods = 12
	number of predicates = 12
	number of sorts = 10
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 17
	number of constants = 17
Compiling split parameters ... done.
	number of abstract tasks = 7
	number of tasks = 24
	number of decomposition methods = 13
	number of predicates = 12
	number of sorts = 10
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 17
	number of constants = 17
Lifted reachability analysis and domain cleanup ... done.
	number of abstract tasks = 7
	number of tasks = 24
	number of decomposition methods = 13
	number of predicates = 8
	number of sorts = 10
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 17
	number of constants = 17
Grounded planning graph ... done.
	Number of Grounded Actions 305
	Number of Grounded Literals 208
	number of abstract tasks = 7
	number of tasks = 24
	number of decomposition methods = 13
	number of predicates = 8
	number of sorts = 10
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 17
	number of constants = 17
Two Way TDG ... done.
	number of abstract tasks = 7
	number of tasks = 24
	number of decomposition methods = 13
	number of predicates = 8
	number of sorts = 10
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 17
	number of constants = 17
Grounding ... done.
	number of abstract tasks = 125
	number of tasks = 371
	number of decomposition methods = 298
	number of predicates = 115
	number of sorts = 0
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 246
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 125
	number of tasks = 371
	number of decomposition methods = 298
	number of predicates = 99
	number of sorts = 0
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 246
	number of constants = 0
Omitting lifted reachability analysis ... 
Grounded planning graph ... done.
	Number of Grounded Actions 246
	Number of Grounded Literals 194
	number of abstract tasks = 125
	number of tasks = 371
	number of decomposition methods = 298
	number of predicates = 99
	number of sorts = 0
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 246
	number of constants = 0
Two Way TDG ... done.
	number of abstract tasks = 125
	number of tasks = 371
	number of decomposition methods = 298
	number of predicates = 99
	number of sorts = 0
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 246
	number of constants = 0
Compiling expand choiceless abstract tasks ... done.
	number of abstract tasks = 43
	number of tasks = 289
	number of decomposition methods = 216
	number of predicates = 99
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 246
	number of constants = 0
Compiling abstract tasks without methods ... done.
	number of abstract tasks = 43
	number of tasks = 289
	number of decomposition methods = 216
	number of predicates = 99
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 246
	number of constants = 0
Compiling initial plan ... done.
	number of abstract tasks = 44
	number of tasks = 290
	number of decomposition methods = 217
	number of predicates = 99
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 246
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 44
	number of tasks = 290
	number of decomposition methods = 217
	number of predicates = 99
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 246
	number of constants = 0
Omitting lifted reachability analysis ... 
Grounded planning graph ... done.
	Number of Grounded Actions 246
	Number of Grounded Literals 194
	number of abstract tasks = 44
	number of tasks = 290
	number of decomposition methods = 217
	number of predicates = 99
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 246
	number of constants = 0
Two Way TDG ... done.
	number of abstract tasks = 44
	number of tasks = 290
	number of decomposition methods = 217
	number of predicates = 99
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 246
	number of constants = 0
Compiling expand choiceless abstract tasks ... done.
	number of abstract tasks = 43
	number of tasks = 289
	number of decomposition methods = 216
	number of predicates = 99
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 246
	number of constants = 0
Compiling abstract tasks without methods ... done.
	number of abstract tasks = 43
	number of tasks = 289
	number of decomposition methods = 216
	number of predicates = 99
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 246
	number of constants = 0
Compiling initial plan ... done.
	number of abstract tasks = 44
	number of tasks = 290
	number of decomposition methods = 217
	number of predicates = 99
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 246
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 44
	number of tasks = 290
	number of decomposition methods = 217
	number of predicates = 99
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 246
	number of constants = 0
Tasks 246 - 1
Domain is acyclic: true
Domain is mostly acyclic: true
Domain is regular: false
Domain is tail recursive: true
Domain is totally ordered: true
Domain has last task in all methods: true
Time remaining for planner 9223372036854773642ms

Running SAT search with K = 0
Time remaining for SAT search 9223372036854773629ms
Time used for this run 9223372036854773629ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 106
NUMBER OF STATE CLAUSES 105
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 99.06% 0.9400000000000001% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID ed3769fd-6251-4182-9f9b-df2c0cefcbd0
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
Still waiting ... running for 102 will abort at 9223372036854773629
ERROR false

Running SAT search with K = 1
Time remaining for SAT search 9223372036854773523ms
Time used for this run 9223372036854773523ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 106
NUMBER OF STATE CLAUSES 105
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 99.06% 0.9400000000000001% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID cced0578-8e82-4311-b8c8-876d745435b6
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
Still waiting ... running for 101 will abort at 9223372036854773523
ERROR false

Running SAT search with K = 2
Time remaining for SAT search 9223372036854773422ms
Time used for this run 9223372036854773422ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 2563
NUMBER OF STATE CLAUSES 2487
NUMBER OF DECOMPOSITION CLAUSES 76
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 97.03% 2.97% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 6d3c2dfa-52e6-4acf-89a2-8eeffab5bc9b
FLUSH
CLOSE
NUMBER OF PATHS 12
Still waiting ... running for 100 will abort at 9223372036854773422
Starting minisat
Setting starttime of solver to 1542620201465
Command exited with non-zero status 20
0.00 0.00

Time command gave the following runtime for the solver: 0
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... done
SAT-Solver says: UNSAT
ERROR false

Running SAT search with K = 3
Time remaining for SAT search 9223372036854773201ms
Time used for this run 9223372036854773201ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... Still waiting ... running for 100 will abort at 9223372036854773201
done
NUMBER OF CLAUSES 23736
NUMBER OF STATE CLAUSES 18273
NUMBER OF DECOMPOSITION CLAUSES 5463
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 76.98% 23.02% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 050cc248-1b85-4c62-8c1e-c9e06fb7ac29
FLUSH
CLOSE
NUMBER OF PATHS 84
Starting minisat
Setting starttime of solver to 1542620201811
Command exited with non-zero status 20
0.00 0.00

Time command gave the following runtime for the solver: 0
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... done
SAT-Solver says: UNSAT
ERROR false

Running SAT search with K = 4
Time remaining for SAT search 9223372036854772880ms
Time used for this run 9223372036854772880ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
Still waiting ... running for 100 will abort at 9223372036854772880
NUMBER OF CLAUSES 55426
NUMBER OF STATE CLAUSES 40756
NUMBER OF DECOMPOSITION CLAUSES 14670
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 73.53% 26.47% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID aafd33d5-a321-49d5-96d2-39e46082724f
FLUSH
CLOSE
NUMBER OF PATHS 185
Starting minisat
Setting starttime of solver to 1542620202126
Command exited with non-zero status 20
0.01 0.00

Time command gave the following runtime for the solver: 10
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... done
SAT-Solver says: UNSAT
ERROR false

Running SAT search with K = 5
Time remaining for SAT search 9223372036854772557ms
Time used for this run 9223372036854772557ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... Still waiting ... running for 101 will abort at 9223372036854772557
done
NUMBER OF CLAUSES 67088
NUMBER OF STATE CLAUSES 50130
NUMBER OF DECOMPOSITION CLAUSES 16958
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 74.72% 25.28% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 7e12a241-63cf-426a-8335-5655636edc6b
FLUSH
CLOSE
NUMBER OF PATHS 228
Starting minisat
Setting starttime of solver to 1542620202472
Command exited with non-zero status 10
0.02 0.00

Time command gave the following runtime for the solver: 20
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... done
SAT-Solver says: SAT

extracting solution


CHECKING primitive solution of length 74 ...
true 35 SHOP_methodm10_do_load_truck_10_precondition[crate1,depot0,pallet0,hoist0]
true 74 SHOP_methodm6_do_get_truck_6_precondition[truck0,depot0]
true 82 nop[]
true 46 SHOP_methodm4_do_clear_4_precondition[crate1,depot0]
true 79 nop[]
true 14 Lift[hoist0,crate1,pallet0,depot0]
true 85 Load[hoist0,crate1,truck0,depot0]
true 16 Drive[truck0,depot0,distributor1]
true 1 SHOP_methodm11_do_unload_truck_11_precondition[truck0,crate1,pallet2,distributor1,hoist2]
true 99 SHOP_methodm5_do_clear_5_precondition[pallet2,crate2,distributor1,hoist2]
true 25 SHOP_methodm5_do_clear_5_precondition[crate2,crate5,distributor1,hoist2]
true 66 SHOP_methodm4_do_clear_4_precondition[crate5,distributor1]
true 111 nop[]
true 26 Lift[hoist2,crate5,crate2,distributor1]
true 75 SHOP_methodm6_do_get_truck_6_precondition[truck0,distributor1]
true 63 nop[]
true 21 Load[hoist2,crate5,truck0,distributor1]
true 53 Lift[hoist2,crate2,pallet2,distributor1]
true 37 SHOP_methodm7_do_get_truck_7_precondition[truck1,distributor1]
true 104 Drive[truck1,distributor0,distributor1]
true 50 Load[hoist2,crate2,truck1,distributor1]
true 58 Unload[hoist2,crate1,truck0,distributor1]
true 39 Drop[hoist2,crate1,pallet2,distributor1]
true 36 SHOP_methodm2_do_put_on_2_precondition[crate2,truck1]
true 87 SHOP_methodm7_do_get_truck_7_precondition[truck1,depot0]
true 110 Drive[truck1,distributor1,depot0]
true 73 SHOP_methodm4_do_clear_4_precondition[pallet0,depot0]
true 114 nop[]
true 49 Unload[hoist0,crate2,truck1,depot0]
true 62 Drop[hoist0,crate2,pallet0,depot0]
true 92 SHOP_methodm10_do_load_truck_10_precondition[crate4,distributor0,crate3,hoist1]
true 68 SHOP_methodm7_do_get_truck_7_precondition[truck1,distributor0]
true 12 Drive[truck1,depot0,distributor0]
true 30 SHOP_methodm4_do_clear_4_precondition[crate4,distributor0]
true 113 nop[]
true 102 Lift[hoist1,crate4,crate3,distributor0]
true 83 Load[hoist1,crate4,truck1,distributor0]
true 7 Drive[truck1,distributor0,distributor0]
true 76 SHOP_methodm11_do_unload_truck_11_precondition[truck1,crate4,pallet1,distributor0,hoist1]
true 10 SHOP_methodm5_do_clear_5_precondition[pallet1,crate0,distributor0,hoist1]
true 40 SHOP_methodm5_do_clear_5_precondition[crate0,crate3,distributor0,hoist1]
true 22 SHOP_methodm4_do_clear_4_precondition[crate3,distributor0]
true 72 nop[]
true 91 Lift[hoist1,crate3,crate0,distributor0]
true 9 SHOP_methodm6_do_get_truck_6_precondition[truck1,distributor0]
true 29 nop[]
true 34 Load[hoist1,crate3,truck1,distributor0]
true 93 Lift[hoist1,crate0,pallet1,distributor0]
true 60 SHOP_methodm7_do_get_truck_7_precondition[truck0,distributor0]
true 8 Drive[truck0,distributor1,distributor0]
true 13 Load[hoist1,crate0,truck0,distributor0]
true 81 Unload[hoist1,crate4,truck1,distributor0]
true 45 Drop[hoist1,crate4,pallet1,distributor0]
true 103 SHOP_methodm2_do_put_on_2_precondition[crate0,truck0]
true 89 SHOP_methodm7_do_get_truck_7_precondition[truck0,distributor1]
true 86 Drive[truck0,distributor0,distributor1]
true 112 SHOP_methodm4_do_clear_4_precondition[crate1,distributor1]
true 100 nop[]
true 84 Unload[hoist2,crate0,truck0,distributor1]
true 116 Drop[hoist2,crate0,crate1,distributor1]
true 11 SHOP_methodm2_do_put_on_2_precondition[crate3,truck1]
true 94 SHOP_methodm7_do_get_truck_7_precondition[truck1,depot0]
true 69 Drive[truck1,distributor0,depot0]
true 5 SHOP_methodm4_do_clear_4_precondition[crate2,depot0]
true 19 nop[]
true 33 Unload[hoist0,crate3,truck1,depot0]
true 27 Drop[hoist0,crate3,crate2,depot0]
true 15 SHOP_methodm2_do_put_on_2_precondition[crate5,truck0]
true 97 SHOP_methodm6_do_get_truck_6_precondition[truck0,distributor1]
true 42 nop[]
true 88 SHOP_methodm4_do_clear_4_precondition[crate0,distributor1]
true 2 nop[]
true 0 Unload[hoist2,crate5,truck0,distributor1]
true 117 Drop[hoist2,crate5,crate0,distributor1]
 done.
ERROR false
Panda says: SOLUTION
============ global ============
randomseed     = 42
peak memory    = 645922816
planner result = SOLUTION
============ properties ============
acyclic                  = true
mostly acyclic           = true
regular                  = false
tail recursive           = true
totally ordered          = true
last task in all methods = true
============ problem ============
number of constants         = 0
number of predicates        = 99
number of actions           = 290
number of abstract actions  = 44
number of primitive actions = 246
number of methods           = 217
============ sat ============
plan length                     = -1
number of variables             = 26166
number of clauses               = 67088
average size of clauses         = 2.1889607679465777
number of assert                = 37
number of horn                  = 63247
K offset                        = 0
K chosen value                  = 5
state formula                   = 50130
method children clauses         = 0
number of paths                 = 228
maximum plan length             = 228
number of decomposition clauses = 16958
number of ordering clauses      = 0
number of state clauses         = 50130
solved                          = true
timeout                         = false

----------------- TIMINGS -----------------
============ total ============
total = 3770
============ parsing ============
total                         = 645
file parser                   = 411
sort expansion                = 99
closed world assumption       = 43
shop methods                  = 9
eliminate identical variables = 19
strip domain of hybridity     = 40
flatten formula               = 24
============ preprocessing ============
total                                      = 1430
compile negative preconditions             = 17
compile unit methods                       = 0
split parameter                            = 17
expand choiceless abstract tasks           = 167
expand choiceless abstract tasks           = 2
compile methods with identical tasks       = 0
removing unnecessary predicates            = 114
lifted reachabiltiy analysis               = 47
grounded planning graph analysis           = 405
grounded task decomposition graph analysis = 562
grounding                                  = 86
create artificial top task                 = 2
============ sat ============
total                                        = 920
generate formula                             = 751
generate path decomposition tree             = 136
normalise path decomposition tree            = 57
translate path decomposition tree to clauses = 191
transform to DIMACS                          = 95
SAT solver                                   = 30
SAT solver for K=0002                        = 0
SAT solver for K=0003                        = 0
SAT solver for K=0004                        = 10
SAT solver for K=0005                        = 20

#1 "40 sat:90:solved"="true";"30 problem:05:number of primitive actions"="246";"30 problem:01:number of constants"="0";"30 problem:04:number of abstract actions"="44";"02 properties:04:tail recursive"="true";"00 global:80:peak memory"="645922816";"40 sat:20:state formula"="50130";"40 sat:01:number of variables"="26166";"40 sat:14:K offset"="0";"40 sat:30:number of paths"="228";"40 sat:00:plan length"="-1";"40 sat:50:number of ordering clauses"="0";"02 properties:02:mostly acyclic"="true";"30 problem:06:number of methods"="217";"02 properties:05:totally ordered"="true";"02 properties:06:last task in all methods"="true";"30 problem:03:number of actions"="290";"30 problem:02:number of predicates"="99";"40 sat:03:number of horn"="63247";"40 sat:15:K chosen value"="5";"02 properties:03:regular"="false";"40 sat:03:average size of clauses"="2.1889607679465777";"40 sat:02:number of clauses"="67088";"40 sat:50:number of state clauses"="50130";"40 sat:03:number of assert"="37";"40 sat:22:method children clauses"="0";"00 global:90:planner result"="SOLUTION";"02 properties:01:acyclic"="true";"40 sat:31:maximum plan length"="228";"40 sat:50:number of decomposition clauses"="16958";"00 global:02:randomseed"="42";"40 sat:91:timeout"="false";"01 parsing:01:file parser"="411";"40 sat:41:SAT solver for K=0004"="10";"40 sat:00:total"="920";"40 sat:20:transform to DIMACS"="95";"40 sat:11:generate path decomposition tree"="136";"02 preprocessing:07:compile methods with identical tasks"="0";"01 parsing:04:shop methods"="9";"02 preprocessing:08:removing unnecessary predicates"="114";"01 parsing:03:closed world assumption"="43";"02 preprocessing:11:lifted reachabiltiy analysis"="47";"01 parsing:02:sort expansion"="99";"40 sat:12:normalise path decomposition tree"="57";"40 sat:40:SAT solver"="30";"01 parsing:00:total"="645";"02 preprocessing:06:expand choiceless abstract tasks"="2";"01 parsing:06:strip domain of hybridity"="40";"40 sat:13:translate path decomposition tree to clauses"="191";"40 sat:10:generate formula"="751";"40 sat:41:SAT solver for K=0005"="20";"40 sat:41:SAT solver for K=0002"="0";"02 preprocessing:01:compile negative preconditions"="17";"00 total:00:total"="3770";"02 preprocessing:12:grounded planning graph analysis"="405";"02 preprocessing:02:compile unit methods"="0";"02 preprocessing:23:grounded task decomposition graph analysis"="562";"02 preprocessing:04:split parameter"="17";"01 parsing:07:flatten formula"="24";"02 preprocessing:05:expand choiceless abstract tasks"="167";"40 sat:41:SAT solver for K=0003"="0";"02 preprocessing:00:total"="1430";"02 preprocessing:99:create artificial top task"="2";"01 parsing:05:eliminate identical variables"="19";"02 preprocessing:84:grounding"="86"
SOLUTION SEQUENCE
0: SHOP_methodm10_do_load_truck_10_precondition(crate1,depot0,pallet0,hoist0)
1: SHOP_methodm6_do_get_truck_6_precondition(truck0,depot0)
2: nop()
3: SHOP_methodm4_do_clear_4_precondition(crate1,depot0)
4: nop()
5: Lift(hoist0,crate1,pallet0,depot0)
6: Load(hoist0,crate1,truck0,depot0)
7: Drive(truck0,depot0,distributor1)
8: SHOP_methodm11_do_unload_truck_11_precondition(truck0,crate1,pallet2,distributor1,hoist2)
9: SHOP_methodm5_do_clear_5_precondition(pallet2,crate2,distributor1,hoist2)
10: SHOP_methodm5_do_clear_5_precondition(crate2,crate5,distributor1,hoist2)
11: SHOP_methodm4_do_clear_4_precondition(crate5,distributor1)
12: nop()
13: Lift(hoist2,crate5,crate2,distributor1)
14: SHOP_methodm6_do_get_truck_6_precondition(truck0,distributor1)
15: nop()
16: Load(hoist2,crate5,truck0,distributor1)
17: Lift(hoist2,crate2,pallet2,distributor1)
18: SHOP_methodm7_do_get_truck_7_precondition(truck1,distributor1)
19: Drive(truck1,distributor0,distributor1)
20: Load(hoist2,crate2,truck1,distributor1)
21: Unload(hoist2,crate1,truck0,distributor1)
22: Drop(hoist2,crate1,pallet2,distributor1)
23: SHOP_methodm2_do_put_on_2_precondition(crate2,truck1)
24: SHOP_methodm7_do_get_truck_7_precondition(truck1,depot0)
25: Drive(truck1,distributor1,depot0)
26: SHOP_methodm4_do_clear_4_precondition(pallet0,depot0)
27: nop()
28: Unload(hoist0,crate2,truck1,depot0)
29: Drop(hoist0,crate2,pallet0,depot0)
30: SHOP_methodm10_do_load_truck_10_precondition(crate4,distributor0,crate3,hoist1)
31: SHOP_methodm7_do_get_truck_7_precondition(truck1,distributor0)
32: Drive(truck1,depot0,distributor0)
33: SHOP_methodm4_do_clear_4_precondition(crate4,distributor0)
34: nop()
35: Lift(hoist1,crate4,crate3,distributor0)
36: Load(hoist1,crate4,truck1,distributor0)
37: Drive(truck1,distributor0,distributor0)
38: SHOP_methodm11_do_unload_truck_11_precondition(truck1,crate4,pallet1,distributor0,hoist1)
39: SHOP_methodm5_do_clear_5_precondition(pallet1,crate0,distributor0,hoist1)
40: SHOP_methodm5_do_clear_5_precondition(crate0,crate3,distributor0,hoist1)
41: SHOP_methodm4_do_clear_4_precondition(crate3,distributor0)
42: nop()
43: Lift(hoist1,crate3,crate0,distributor0)
44: SHOP_methodm6_do_get_truck_6_precondition(truck1,distributor0)
45: nop()
46: Load(hoist1,crate3,truck1,distributor0)
47: Lift(hoist1,crate0,pallet1,distributor0)
48: SHOP_methodm7_do_get_truck_7_precondition(truck0,distributor0)
49: Drive(truck0,distributor1,distributor0)
50: Load(hoist1,crate0,truck0,distributor0)
51: Unload(hoist1,crate4,truck1,distributor0)
52: Drop(hoist1,crate4,pallet1,distributor0)
53: SHOP_methodm2_do_put_on_2_precondition(crate0,truck0)
54: SHOP_methodm7_do_get_truck_7_precondition(truck0,distributor1)
55: Drive(truck0,distributor0,distributor1)
56: SHOP_methodm4_do_clear_4_precondition(crate1,distributor1)
57: nop()
58: Unload(hoist2,crate0,truck0,distributor1)
59: Drop(hoist2,crate0,crate1,distributor1)
60: SHOP_methodm2_do_put_on_2_precondition(crate3,truck1)
61: SHOP_methodm7_do_get_truck_7_precondition(truck1,depot0)
62: Drive(truck1,distributor0,depot0)
63: SHOP_methodm4_do_clear_4_precondition(crate2,depot0)
64: nop()
65: Unload(hoist0,crate3,truck1,depot0)
66: Drop(hoist0,crate3,crate2,depot0)
67: SHOP_methodm2_do_put_on_2_precondition(crate5,truck0)
68: SHOP_methodm6_do_get_truck_6_precondition(truck0,distributor1)
69: nop()
70: SHOP_methodm4_do_clear_4_precondition(crate0,distributor1)
71: nop()
72: Unload(hoist2,crate5,truck0,distributor1)
73: Drop(hoist2,crate5,crate0,distributor1)
