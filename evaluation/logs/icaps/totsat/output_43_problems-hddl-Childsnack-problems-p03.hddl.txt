PANDA - Planning and Acting in a Network Decomposition Architecture
Believe us: It's great, it's fantastic!

PANDA Copyright (C) 2014-2018 Gregor Behnke, Pascal Bercher, Thomas Geier, Kadir
Dede, Daniel Höller, Kristof Mickeleit, Matthias Englert
This program comes with ABSOLUTELY NO WARRANTY
This is free software, and you are welcome to redistribute it under certain
conditions; run PANDA with -license for details.

Main Developers:
- Gregor Behnke, http://www.uni-ulm.de/in/ki/behnke
- Daniel Höller, http://www.uni-ulm.de/in/ki/hoeller

With many thanks to various further contributors.
Run PANDA with the command line argument -contributors for an extensive list.

Run it with -help for more information like available options.


PANDA was called with: "-systemConfig AAAI-2018-totSAT(minisat) -programPath minisat=./minisat problems/hddl/Childsnack/domains/domain.hddl problems/hddl/Childsnack/problems/p03.hddl"


Planner Configuration
=====================
Domain: problems/hddl/Childsnack/domains/domain.hddl
Problem: problems/hddl/Childsnack/problems/p03.hddl
Output: none

Planning Configuration
======================
	printGeneralInformation : true
	printAdditionalData     : true
	random seed             : 42
	time limit (in seconds) : none

	external programs:
		minisat : ./minisat

	Parsing Configuration
	---------------------
	Parser                : autodetect file-type
	Expand Sort Hierarchy : true
	ClosedWordAssumption  : true
	CompileSHOPMethods    : true
	Eliminate Equality    : true
	Strip Hybridity       : true
	Reduce General Tasks  : true
	
	Preprocessing Configuration
	---------------------------
	Compile negative preconditions    : true
	Compile unit methods              : false
	Compile order in methods          : false
	Compile initial plan              : true
	Ensure Methods Have Last Task     : false
	Split independent parameters      : true
	Remove unnecessary predicates     : true
	Expand choiceless abstract tasks  : true
	Domain Cleanup                    : true
	Convert to SAS+                   : false
	Grounded Reachability Analysis    : Planning Graph (mutex-free)
	Grounded Task Decomposition Graph : Two Way TDG
	Iterate reachability analysis     : true
	Ground domain                     : true
	Iterate reachability analysis     : true
	Stop directly after grounding     : false
	
	SAT-Planning Configuration
	--------------------------
	solver           : minisat
	full planner run : true
	reduction method : only normalise 
	check result     : true
	
	Post-processing Configuration
	-----------------------------
	search status
	search result
	timings
	statistics
#0 "00 global:01:problem"="p03.hddl";"00 global:00:domain"="domain.hddl"
Parsing domain ... using HDDL parser ... done
Preparing internal domain representation ... done.
Initial domain
	number of abstract tasks = 1
	number of tasks = 10
	number of decomposition methods = 2
	number of predicates = 13
	number of sorts = 7
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 9
	number of constants = 55
Compiling negative preconditions ... done.
	number of abstract tasks = 1
	number of tasks = 10
	number of decomposition methods = 2
	number of predicates = 26
	number of sorts = 7
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 9
	number of constants = 55
Compiling split parameters ... done.
	number of abstract tasks = 1
	number of tasks = 10
	number of decomposition methods = 2
	number of predicates = 26
	number of sorts = 7
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 9
	number of constants = 55
Lifted reachability analysis and domain cleanup ... done.
	number of abstract tasks = 1
	number of tasks = 9
	number of decomposition methods = 2
	number of predicates = 15
	number of sorts = 7
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 8
	number of constants = 55
Grounded planning graph ... done.
	Number of Grounded Actions 8721
	Number of Grounded Literals 288
	number of abstract tasks = 1
	number of tasks = 9
	number of decomposition methods = 2
	number of predicates = 15
	number of sorts = 7
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 8
	number of constants = 55
Two Way TDG ... done.
	number of abstract tasks = 1
	number of tasks = 9
	number of decomposition methods = 2
	number of predicates = 15
	number of sorts = 7
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 8
	number of constants = 55
Grounding ... done.
	number of abstract tasks = 11
	number of tasks = 7649
	number of decomposition methods = 18315
	number of predicates = 201
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 7638
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 11
	number of tasks = 7649
	number of decomposition methods = 18315
	number of predicates = 135
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 7638
	number of constants = 0
Omitting lifted reachability analysis ... 
Grounded planning graph ... done.
	Number of Grounded Actions 7638
	Number of Grounded Literals 244
	number of abstract tasks = 11
	number of tasks = 7649
	number of decomposition methods = 18315
	number of predicates = 135
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 7638
	number of constants = 0
Two Way TDG ... done.
	number of abstract tasks = 11
	number of tasks = 7649
	number of decomposition methods = 18315
	number of predicates = 135
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 7638
	number of constants = 0
Compiling expand choiceless abstract tasks ... done.
	number of abstract tasks = 11
	number of tasks = 7649
	number of decomposition methods = 18315
	number of predicates = 135
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 7638
	number of constants = 0
Compiling abstract tasks without methods ... done.
	number of abstract tasks = 11
	number of tasks = 7649
	number of decomposition methods = 18315
	number of predicates = 135
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 7638
	number of constants = 0
Compiling initial plan ... done.
	number of abstract tasks = 12
	number of tasks = 7650
	number of decomposition methods = 18316
	number of predicates = 135
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 7638
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 12
	number of tasks = 7650
	number of decomposition methods = 18316
	number of predicates = 135
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 7638
	number of constants = 0
Omitting lifted reachability analysis ... 
Grounded planning graph ... done.
	Number of Grounded Actions 7638
	Number of Grounded Literals 244
	number of abstract tasks = 12
	number of tasks = 7650
	number of decomposition methods = 18316
	number of predicates = 135
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 7638
	number of constants = 0
Two Way TDG ... done.
	number of abstract tasks = 12
	number of tasks = 7650
	number of decomposition methods = 18316
	number of predicates = 135
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 7638
	number of constants = 0
Compiling expand choiceless abstract tasks ... done.
	number of abstract tasks = 11
	number of tasks = 7649
	number of decomposition methods = 18315
	number of predicates = 135
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 7638
	number of constants = 0
Compiling abstract tasks without methods ... done.
	number of abstract tasks = 11
	number of tasks = 7649
	number of decomposition methods = 18315
	number of predicates = 135
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 7638
	number of constants = 0
Compiling initial plan ... done.
	number of abstract tasks = 12
	number of tasks = 7650
	number of decomposition methods = 18316
	number of predicates = 135
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 7638
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 12
	number of tasks = 7650
	number of decomposition methods = 18316
	number of predicates = 135
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 7638
	number of constants = 0
Tasks 7638 - 0
Domain is acyclic: true
Domain is mostly acyclic: true
Domain is regular: false
Domain is tail recursive: true
Domain is totally ordered: true
Domain has last task in all methods: true
Time remaining for planner 9223372036854757824ms

Running SAT search with K = 0
Time remaining for SAT search 9223372036854757811ms
Time used for this run 9223372036854757811ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 147
NUMBER OF STATE CLAUSES 146
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 99.32000000000001% 0.68% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
Still waiting ... running for 102 will abort at 9223372036854757811
UUID a77d7c4f-7a2c-4cf0-aa16-94c86dcfb44b
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
ERROR false

Running SAT search with K = 1
Time remaining for SAT search 9223372036854757605ms
Time used for this run 9223372036854757605ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 147
NUMBER OF STATE CLAUSES 146
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 99.32000000000001% 0.68% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 4a3d222b-8ef8-4890-8bb2-5ac5fadf0725
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
Still waiting ... running for 100 will abort at 9223372036854757605
ERROR false

Running SAT search with K = 2
Time remaining for SAT search 9223372036854757504ms
Time used for this run 9223372036854757504ms


Generating initial PDT ... Still waiting ... running for 100 will abort at 9223372036854757504
done
Checking whether the PDT can grow any more ... no ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 570290
NUMBER OF STATE CLAUSES 72104
NUMBER OF DECOMPOSITION CLAUSES 498186
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 12.64% 87.36% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 28fe8507-b455-4120-810e-f78f1087cb02
FLUSH
CLOSE
NUMBER OF PATHS 66
Starting minisat
Setting starttime of solver to 1542618563998
Command exited with non-zero status 10
0.16 0.01

Time command gave the following runtime for the solver: 170
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... done
SAT-Solver says: SAT

extracting solution


CHECKING primitive solution of length 66 ...
true 29 SHOP_methodm0_serve_0_precondition[content5,table2,child1,bread10,sandw15]
true 42 make_sandwich_no_gluten[sandw15,bread10,content5]
true 36 put_on_tray[sandw15,tray2,kitchen]
true 30 move_tray[tray2,kitchen,table2]
true 15 serve_sandwich_no_gluten[sandw15,child1,tray2,table2]
true 43 move_tray[tray2,table2,kitchen]
true 6 SHOP_methodm1_serve_1_precondition[table1,bread11,sandw13,child2,content4]
true 40 make_sandwich[sandw13,bread11,content4]
true 54 put_on_tray[sandw13,tray1,kitchen]
true 63 move_tray[tray1,kitchen,table1]
true 74 serve_sandwich[sandw13,child2,tray1,table1]
true 31 move_tray[tray1,table1,kitchen]
true 62 SHOP_methodm0_serve_0_precondition[content2,table1,child3,bread9,sandw6]
true 49 make_sandwich_no_gluten[sandw6,bread9,content2]
true 23 put_on_tray[sandw6,tray3,kitchen]
true 41 move_tray[tray3,kitchen,table1]
true 25 serve_sandwich_no_gluten[sandw6,child3,tray3,table1]
true 21 move_tray[tray3,table1,kitchen]
true 13 SHOP_methodm1_serve_1_precondition[table2,bread7,sandw2,child4,content10]
true 16 make_sandwich[sandw2,bread7,content10]
true 73 put_on_tray[sandw2,tray3,kitchen]
true 71 move_tray[tray3,kitchen,table2]
true 3 serve_sandwich[sandw2,child4,tray3,table2]
true 58 move_tray[tray3,table2,kitchen]
true 64 SHOP_methodm0_serve_0_precondition[content9,table3,child5,bread3,sandw3]
true 66 make_sandwich_no_gluten[sandw3,bread3,content9]
true 50 put_on_tray[sandw3,tray3,kitchen]
true 14 move_tray[tray3,kitchen,table3]
true 47 serve_sandwich_no_gluten[sandw3,child5,tray3,table3]
true 69 move_tray[tray3,table3,kitchen]
true 76 SHOP_methodm1_serve_1_precondition[table3,bread2,sandw14,child6,content3]
true 45 make_sandwich[sandw14,bread2,content3]
true 46 put_on_tray[sandw14,tray3,kitchen]
true 65 move_tray[tray3,kitchen,table3]
true 56 serve_sandwich[sandw14,child6,tray3,table3]
true 33 move_tray[tray3,table3,kitchen]
true 39 SHOP_methodm1_serve_1_precondition[table3,bread1,sandw10,child7,content1]
true 55 make_sandwich[sandw10,bread1,content1]
true 22 put_on_tray[sandw10,tray3,kitchen]
true 44 move_tray[tray3,kitchen,table3]
true 34 serve_sandwich[sandw10,child7,tray3,table3]
true 12 move_tray[tray3,table3,kitchen]
true 4 SHOP_methodm1_serve_1_precondition[table2,bread5,sandw7,child8,content6]
true 67 make_sandwich[sandw7,bread5,content6]
true 75 put_on_tray[sandw7,tray3,kitchen]
true 9 move_tray[tray3,kitchen,table2]
true 72 serve_sandwich[sandw7,child8,tray3,table2]
true 0 move_tray[tray3,table2,kitchen]
true 77 SHOP_methodm1_serve_1_precondition[table1,bread8,sandw5,child9,content7]
true 59 make_sandwich[sandw5,bread8,content7]
true 48 put_on_tray[sandw5,tray3,kitchen]
true 24 move_tray[tray3,kitchen,table1]
true 61 serve_sandwich[sandw5,child9,tray3,table1]
true 17 move_tray[tray3,table1,kitchen]
true 1 SHOP_methodm1_serve_1_precondition[table3,bread6,sandw1,child10,content8]
true 10 make_sandwich[sandw1,bread6,content8]
true 51 put_on_tray[sandw1,tray3,kitchen]
true 38 move_tray[tray3,kitchen,table3]
true 7 serve_sandwich[sandw1,child10,tray3,table3]
true 8 move_tray[tray3,table3,kitchen]
true 11 SHOP_methodm0_serve_0_precondition[content11,table1,child11,bread4,sandw9]
true 2 make_sandwich_no_gluten[sandw9,bread4,content11]
true 5 put_on_tray[sandw9,tray1,kitchen]
true 26 move_tray[tray1,kitchen,table1]
true 37 serve_sandwich_no_gluten[sandw9,child11,tray1,table1]
true 35 move_tray[tray1,table1,kitchen]
 done.
ERROR false
Panda says: SOLUTION
============ global ============
randomseed     = 42
peak memory    = 2679612416
planner result = SOLUTION
============ properties ============
acyclic                  = true
mostly acyclic           = true
regular                  = false
tail recursive           = true
totally ordered          = true
last task in all methods = true
============ problem ============
number of constants         = 0
number of predicates        = 135
number of actions           = 7650
number of abstract actions  = 12
number of primitive actions = 7638
number of methods           = 18316
============ sat ============
plan length                     = -1
number of variables             = 41143
number of clauses               = 570290
average size of clauses         = 2.080045240140981
number of assert                = 52
number of horn                  = 568461
K offset                        = 0
K chosen value                  = 2
state formula                   = 72104
method children clauses         = 0
number of paths                 = 66
maximum plan length             = 66
number of decomposition clauses = 498186
number of ordering clauses      = 0
number of state clauses         = 72104
solved                          = true
timeout                         = false

----------------- TIMINGS -----------------
============ total ============
total = 22063
============ parsing ============
total                         = 673
file parser                   = 450
sort expansion                = 93
closed world assumption       = 52
shop methods                  = 7
eliminate identical variables = 19
strip domain of hybridity     = 36
flatten formula               = 16
============ preprocessing ============
total                                      = 15649
compile negative preconditions             = 16
compile unit methods                       = 0
split parameter                            = 18
expand choiceless abstract tasks           = 245
expand choiceless abstract tasks           = 230
compile methods with identical tasks       = 0
removing unnecessary predicates            = 3198
lifted reachabiltiy analysis               = 34
grounded planning graph analysis           = 2820
grounded task decomposition graph analysis = 8219
grounding                                  = 819
create artificial top task                 = 26
============ sat ============
total                                        = 2963
generate formula                             = 2634
generate path decomposition tree             = 1004
normalise path decomposition tree            = 322
translate path decomposition tree to clauses = 832
transform to DIMACS                          = 125
SAT solver                                   = 170
SAT solver for K=0002                        = 170

#1 "40 sat:90:solved"="true";"30 problem:05:number of primitive actions"="7638";"30 problem:01:number of constants"="0";"30 problem:04:number of abstract actions"="12";"02 properties:04:tail recursive"="true";"00 global:80:peak memory"="2679612416";"40 sat:20:state formula"="72104";"40 sat:01:number of variables"="41143";"40 sat:14:K offset"="0";"40 sat:30:number of paths"="66";"40 sat:00:plan length"="-1";"40 sat:50:number of ordering clauses"="0";"02 properties:02:mostly acyclic"="true";"30 problem:06:number of methods"="18316";"02 properties:05:totally ordered"="true";"02 properties:06:last task in all methods"="true";"30 problem:03:number of actions"="7650";"30 problem:02:number of predicates"="135";"40 sat:03:number of horn"="568461";"40 sat:15:K chosen value"="2";"02 properties:03:regular"="false";"40 sat:03:average size of clauses"="2.080045240140981";"40 sat:02:number of clauses"="570290";"40 sat:50:number of state clauses"="72104";"40 sat:03:number of assert"="52";"40 sat:22:method children clauses"="0";"00 global:90:planner result"="SOLUTION";"02 properties:01:acyclic"="true";"40 sat:31:maximum plan length"="66";"40 sat:50:number of decomposition clauses"="498186";"00 global:02:randomseed"="42";"40 sat:91:timeout"="false";"01 parsing:01:file parser"="450";"40 sat:00:total"="2963";"40 sat:20:transform to DIMACS"="125";"40 sat:11:generate path decomposition tree"="1004";"02 preprocessing:07:compile methods with identical tasks"="0";"01 parsing:04:shop methods"="7";"02 preprocessing:08:removing unnecessary predicates"="3198";"01 parsing:03:closed world assumption"="52";"02 preprocessing:11:lifted reachabiltiy analysis"="34";"01 parsing:02:sort expansion"="93";"40 sat:12:normalise path decomposition tree"="322";"40 sat:40:SAT solver"="170";"01 parsing:00:total"="673";"02 preprocessing:06:expand choiceless abstract tasks"="230";"01 parsing:06:strip domain of hybridity"="36";"40 sat:13:translate path decomposition tree to clauses"="832";"40 sat:10:generate formula"="2634";"40 sat:41:SAT solver for K=0002"="170";"02 preprocessing:01:compile negative preconditions"="16";"00 total:00:total"="22063";"02 preprocessing:12:grounded planning graph analysis"="2820";"02 preprocessing:02:compile unit methods"="0";"02 preprocessing:23:grounded task decomposition graph analysis"="8219";"02 preprocessing:04:split parameter"="18";"01 parsing:07:flatten formula"="16";"02 preprocessing:05:expand choiceless abstract tasks"="245";"02 preprocessing:00:total"="15649";"02 preprocessing:99:create artificial top task"="26";"01 parsing:05:eliminate identical variables"="19";"02 preprocessing:84:grounding"="819"
SOLUTION SEQUENCE
0: SHOP_methodm0_serve_0_precondition(content5,table2,child1,bread10,sandw15)
1: make_sandwich_no_gluten(sandw15,bread10,content5)
2: put_on_tray(sandw15,tray2,kitchen)
3: move_tray(tray2,kitchen,table2)
4: serve_sandwich_no_gluten(sandw15,child1,tray2,table2)
5: move_tray(tray2,table2,kitchen)
6: SHOP_methodm1_serve_1_precondition(table1,bread11,sandw13,child2,content4)
7: make_sandwich(sandw13,bread11,content4)
8: put_on_tray(sandw13,tray1,kitchen)
9: move_tray(tray1,kitchen,table1)
10: serve_sandwich(sandw13,child2,tray1,table1)
11: move_tray(tray1,table1,kitchen)
12: SHOP_methodm0_serve_0_precondition(content2,table1,child3,bread9,sandw6)
13: make_sandwich_no_gluten(sandw6,bread9,content2)
14: put_on_tray(sandw6,tray3,kitchen)
15: move_tray(tray3,kitchen,table1)
16: serve_sandwich_no_gluten(sandw6,child3,tray3,table1)
17: move_tray(tray3,table1,kitchen)
18: SHOP_methodm1_serve_1_precondition(table2,bread7,sandw2,child4,content10)
19: make_sandwich(sandw2,bread7,content10)
20: put_on_tray(sandw2,tray3,kitchen)
21: move_tray(tray3,kitchen,table2)
22: serve_sandwich(sandw2,child4,tray3,table2)
23: move_tray(tray3,table2,kitchen)
24: SHOP_methodm0_serve_0_precondition(content9,table3,child5,bread3,sandw3)
25: make_sandwich_no_gluten(sandw3,bread3,content9)
26: put_on_tray(sandw3,tray3,kitchen)
27: move_tray(tray3,kitchen,table3)
28: serve_sandwich_no_gluten(sandw3,child5,tray3,table3)
29: move_tray(tray3,table3,kitchen)
30: SHOP_methodm1_serve_1_precondition(table3,bread2,sandw14,child6,content3)
31: make_sandwich(sandw14,bread2,content3)
32: put_on_tray(sandw14,tray3,kitchen)
33: move_tray(tray3,kitchen,table3)
34: serve_sandwich(sandw14,child6,tray3,table3)
35: move_tray(tray3,table3,kitchen)
36: SHOP_methodm1_serve_1_precondition(table3,bread1,sandw10,child7,content1)
37: make_sandwich(sandw10,bread1,content1)
38: put_on_tray(sandw10,tray3,kitchen)
39: move_tray(tray3,kitchen,table3)
40: serve_sandwich(sandw10,child7,tray3,table3)
41: move_tray(tray3,table3,kitchen)
42: SHOP_methodm1_serve_1_precondition(table2,bread5,sandw7,child8,content6)
43: make_sandwich(sandw7,bread5,content6)
44: put_on_tray(sandw7,tray3,kitchen)
45: move_tray(tray3,kitchen,table2)
46: serve_sandwich(sandw7,child8,tray3,table2)
47: move_tray(tray3,table2,kitchen)
48: SHOP_methodm1_serve_1_precondition(table1,bread8,sandw5,child9,content7)
49: make_sandwich(sandw5,bread8,content7)
50: put_on_tray(sandw5,tray3,kitchen)
51: move_tray(tray3,kitchen,table1)
52: serve_sandwich(sandw5,child9,tray3,table1)
53: move_tray(tray3,table1,kitchen)
54: SHOP_methodm1_serve_1_precondition(table3,bread6,sandw1,child10,content8)
55: make_sandwich(sandw1,bread6,content8)
56: put_on_tray(sandw1,tray3,kitchen)
57: move_tray(tray3,kitchen,table3)
58: serve_sandwich(sandw1,child10,tray3,table3)
59: move_tray(tray3,table3,kitchen)
60: SHOP_methodm0_serve_0_precondition(content11,table1,child11,bread4,sandw9)
61: make_sandwich_no_gluten(sandw9,bread4,content11)
62: put_on_tray(sandw9,tray1,kitchen)
63: move_tray(tray1,kitchen,table1)
64: serve_sandwich_no_gluten(sandw9,child11,tray1,table1)
65: move_tray(tray1,table1,kitchen)
