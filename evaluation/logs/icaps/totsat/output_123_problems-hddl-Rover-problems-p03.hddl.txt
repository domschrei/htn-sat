PANDA - Planning and Acting in a Network Decomposition Architecture
Believe us: It's great, it's fantastic!

PANDA Copyright (C) 2014-2018 Gregor Behnke, Pascal Bercher, Thomas Geier, Kadir
Dede, Daniel Höller, Kristof Mickeleit, Matthias Englert
This program comes with ABSOLUTELY NO WARRANTY
This is free software, and you are welcome to redistribute it under certain
conditions; run PANDA with -license for details.

Main Developers:
- Gregor Behnke, http://www.uni-ulm.de/in/ki/behnke
- Daniel Höller, http://www.uni-ulm.de/in/ki/hoeller

With many thanks to various further contributors.
Run PANDA with the command line argument -contributors for an extensive list.

Run it with -help for more information like available options.


PANDA was called with: "-systemConfig AAAI-2018-totSAT(minisat) -programPath minisat=./minisat problems/hddl/Rover/domains/domain.hddl problems/hddl/Rover/problems/p03.hddl"


Planner Configuration
=====================
Domain: problems/hddl/Rover/domains/domain.hddl
Problem: problems/hddl/Rover/problems/p03.hddl
Output: none

Planning Configuration
======================
	printGeneralInformation : true
	printAdditionalData     : true
	random seed             : 42
	time limit (in seconds) : none

	external programs:
		minisat : ./minisat

	Parsing Configuration
	---------------------
	Parser                : autodetect file-type
	Expand Sort Hierarchy : true
	ClosedWordAssumption  : true
	CompileSHOPMethods    : true
	Eliminate Equality    : true
	Strip Hybridity       : true
	Reduce General Tasks  : true
	
	Preprocessing Configuration
	---------------------------
	Compile negative preconditions    : true
	Compile unit methods              : false
	Compile order in methods          : false
	Compile initial plan              : true
	Ensure Methods Have Last Task     : false
	Split independent parameters      : true
	Remove unnecessary predicates     : true
	Expand choiceless abstract tasks  : true
	Domain Cleanup                    : true
	Convert to SAS+                   : false
	Grounded Reachability Analysis    : Planning Graph (mutex-free)
	Grounded Task Decomposition Graph : Two Way TDG
	Iterate reachability analysis     : true
	Ground domain                     : true
	Iterate reachability analysis     : true
	Stop directly after grounding     : false
	
	SAT-Planning Configuration
	--------------------------
	solver           : minisat
	full planner run : true
	reduction method : only normalise 
	check result     : true
	
	Post-processing Configuration
	-----------------------------
	search status
	search result
	timings
	statistics
#0 "00 global:01:problem"="p03.hddl";"00 global:00:domain"="domain.hddl"
Parsing domain ... using HDDL parser ... done
Preparing internal domain representation ... done.
Initial domain
	number of abstract tasks = 10
	number of tasks = 40
	number of decomposition methods = 16
	number of predicates = 26
	number of sorts = 8
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 30
	number of constants = 18
Compiling negative preconditions ... done.
	number of abstract tasks = 10
	number of tasks = 40
	number of decomposition methods = 16
	number of predicates = 52
	number of sorts = 8
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 30
	number of constants = 18
Compiling split parameters ... done.
	number of abstract tasks = 10
	number of tasks = 40
	number of decomposition methods = 16
	number of predicates = 52
	number of sorts = 8
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 30
	number of constants = 18
Lifted reachability analysis and domain cleanup ... done.
	number of abstract tasks = 10
	number of tasks = 40
	number of decomposition methods = 16
	number of predicates = 29
	number of sorts = 8
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 30
	number of constants = 18
Grounded planning graph ... done.
	Number of Grounded Actions 121
	Number of Grounded Literals 128
	number of abstract tasks = 10
	number of tasks = 40
	number of decomposition methods = 16
	number of predicates = 29
	number of sorts = 8
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 30
	number of constants = 18
Two Way TDG ... done.
	number of abstract tasks = 10
	number of tasks = 40
	number of decomposition methods = 16
	number of predicates = 29
	number of sorts = 8
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 30
	number of constants = 18
Grounding ... done.
	number of abstract tasks = 36
	number of tasks = 143
	number of decomposition methods = 96
	number of predicates = 137
	number of sorts = 0
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 107
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 36
	number of tasks = 143
	number of decomposition methods = 96
	number of predicates = 30
	number of sorts = 0
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 107
	number of constants = 0
Omitting lifted reachability analysis ... 
Grounded planning graph ... done.
	Number of Grounded Actions 107
	Number of Grounded Literals 53
	number of abstract tasks = 36
	number of tasks = 143
	number of decomposition methods = 96
	number of predicates = 30
	number of sorts = 0
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 107
	number of constants = 0
Two Way TDG ... done.
	number of abstract tasks = 36
	number of tasks = 143
	number of decomposition methods = 96
	number of predicates = 30
	number of sorts = 0
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 107
	number of constants = 0
Compiling expand choiceless abstract tasks ... done.
	number of abstract tasks = 35
	number of tasks = 142
	number of decomposition methods = 95
	number of predicates = 30
	number of sorts = 0
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 107
	number of constants = 0
Compiling abstract tasks without methods ... done.
	number of abstract tasks = 35
	number of tasks = 142
	number of decomposition methods = 95
	number of predicates = 30
	number of sorts = 0
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 107
	number of constants = 0
Compiling initial plan ... done.
	number of abstract tasks = 36
	number of tasks = 143
	number of decomposition methods = 96
	number of predicates = 30
	number of sorts = 0
	number of tasks in largest method = 7
	number of epsilon methods = 0
	number of primitive tasks = 107
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 36
	number of tasks = 143
	number of decomposition methods = 96
	number of predicates = 30
	number of sorts = 0
	number of tasks in largest method = 7
	number of epsilon methods = 0
	number of primitive tasks = 107
	number of constants = 0
Tasks 107 - 26
Domain is acyclic: false
Domain is mostly acyclic: false
Domain is regular: false
Domain is tail recursive: false
Domain is totally ordered: true
Domain has last task in all methods: true
Time remaining for planner 9223372036854774483ms

Running SAT search with K = 0
Time remaining for SAT search 9223372036854774467ms
Time used for this run 9223372036854774467ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 34
NUMBER OF STATE CLAUSES 33
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 97.06% 2.94% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 9de546a0-1658-48c5-affe-62fd04b1f09f
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
Still waiting ... running for 102 will abort at 9223372036854774467
ERROR false

Running SAT search with K = 1
Time remaining for SAT search 9223372036854774361ms
Time used for this run 9223372036854774361ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 34
NUMBER OF STATE CLAUSES 33
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 97.06% 2.94% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID e08f4113-d5ec-4262-a082-74a406a5057a
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
Still waiting ... running for 101 will abort at 9223372036854774361
ERROR false

Running SAT search with K = 2
Time remaining for SAT search 9223372036854774260ms
Time used for this run 9223372036854774260ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 34
NUMBER OF STATE CLAUSES 33
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 97.06% 2.94% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 07659f2d-8a6b-421e-9bda-96f3e06a095d
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
Still waiting ... running for 101 will abort at 9223372036854774260
ERROR false

Running SAT search with K = 3
Time remaining for SAT search 9223372036854774159ms
Time used for this run 9223372036854774159ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 34
NUMBER OF STATE CLAUSES 33
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 97.06% 2.94% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 7572abfc-73e2-47e8-b2ef-f16a9dfd1900
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
Still waiting ... running for 100 will abort at 9223372036854774159
ERROR false

Running SAT search with K = 4
Time remaining for SAT search 9223372036854774057ms
Time used for this run 9223372036854774057ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
Still waiting ... running for 100 will abort at 9223372036854774057
NUMBER OF CLAUSES 5510
NUMBER OF STATE CLAUSES 3208
NUMBER OF DECOMPOSITION CLAUSES 2302
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 58.22% 41.78% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 23315000-384a-4221-8094-e4dbb5b7fd15
FLUSH
CLOSE
NUMBER OF PATHS 48
Starting minisat
Setting starttime of solver to 1542621581667
Command exited with non-zero status 10
0.00 0.00

Time command gave the following runtime for the solver: 0
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... done
SAT-Solver says: SAT

extracting solution


CHECKING primitive solution of length 36 ...
true 15 SHOP_methodm7_get_soil_data_7_precondition[rover0store,rover0]
true 23 SHOP_methodm0_do_navigate1_0_precondition[rover0,waypoint3]
true 22 nop[]
true 4 SHOP_methodm5_empty_store_5_precondition[rover0store]
true 21 nop[]
true 13 sample_soil[rover0,rover0store,waypoint3]
true 39 SHOP_methodm9_send_soil_data_9_precondition[general,waypoint2,waypoint3,rover0]
true 28 communicate_soil_data2[rover0,general,waypoint3,waypoint2]
true 32 SHOP_methodm10_get_rock_data_10_precondition[rover1store,rover1]
true 52 SHOP_methodm1_do_navigate1_1_precondition[rover1,waypoint2]
true 34 visit[waypoint2]
true 33 SHOP_methodm3_do_navigate2_3_precondition[rover1,waypoint2,waypoint1]
true 48 navigate[rover1,waypoint2,waypoint1]
true 20 unvisit[waypoint2]
true 54 SHOP_methodm5_empty_store_5_precondition[rover1store]
true 16 nop[]
true 56 sample_rock[rover1,rover1store,waypoint1]
true 45 SHOP_methodm11_send_rock_data_11_precondition[general,waypoint2,waypoint1]
true 5 SHOP_methodm1_do_navigate1_1_precondition[rover1,waypoint1]
true 43 visit[waypoint1]
true 19 SHOP_methodm2_do_navigate2_2_precondition[rover1,waypoint1]
true 11 nop[]
true 2 unvisit[waypoint1]
true 26 communicate_rock_data1[rover1,general,waypoint1,waypoint1,waypoint2]
true 53 SHOP_methodm13_get_image_data_13_precondition[waypoint1,rover1,high_res,camera0,objective0]
true 10 SHOP_methodm15_do_calibrate_15_precondition[camera0,objective0,waypoint1]
true 36 SHOP_methodm0_do_navigate1_0_precondition[rover1,waypoint1]
true 42 nop[]
true 27 calibrate[rover1,camera0,objective0,waypoint1]
true 50 SHOP_methodm0_do_navigate1_0_precondition[rover1,waypoint1]
true 35 nop[]
true 14 take_image[rover1,waypoint1,objective0,camera0,high_res]
true 3 SHOP_methodm14_send_image_data_14_precondition[general,waypoint2,waypoint1]
true 41 SHOP_methodm0_do_navigate1_0_precondition[rover1,waypoint1]
true 12 nop[]
true 49 communicate_image_data[rover1,general,objective0,high_res,waypoint1,waypoint2]
 done.
ERROR false
Panda says: SOLUTION
============ global ============
randomseed     = 42
peak memory    = 218103808
planner result = SOLUTION
============ properties ============
acyclic                  = false
mostly acyclic           = false
regular                  = false
tail recursive           = false
totally ordered          = true
last task in all methods = true
============ problem ============
number of constants         = 0
number of predicates        = 30
number of actions           = 143
number of abstract actions  = 36
number of primitive actions = 107
number of methods           = 96
============ sat ============
plan length                     = -1
number of variables             = 2049
number of clauses               = 5510
average size of clauses         = 2.336116152450091
number of assert                = 14
number of horn                  = 5053
K offset                        = 0
K chosen value                  = 4
state formula                   = 3208
method children clauses         = 0
number of paths                 = 48
maximum plan length             = 48
number of decomposition clauses = 2302
number of ordering clauses      = 0
number of state clauses         = 3208
solved                          = true
timeout                         = false

----------------- TIMINGS -----------------
============ total ============
total = 2075
============ parsing ============
total                         = 647
file parser                   = 442
sort expansion                = 87
closed world assumption       = 37
shop methods                  = 10
eliminate identical variables = 18
strip domain of hybridity     = 27
flatten formula               = 25
============ preprocessing ============
total                                      = 615
compile negative preconditions             = 20
compile unit methods                       = 0
split parameter                            = 14
expand choiceless abstract tasks           = 20
expand choiceless abstract tasks           = 1
compile methods with identical tasks       = 1
removing unnecessary predicates            = 38
lifted reachabiltiy analysis               = 34
grounded planning graph analysis           = 182
grounded task decomposition graph analysis = 239
grounding                                  = 56
create artificial top task                 = 2
============ sat ============
total                                        = 262
generate formula                             = 224
generate path decomposition tree             = 70
normalise path decomposition tree            = 27
translate path decomposition tree to clauses = 67
transform to DIMACS                          = 16
SAT solver                                   = 0
SAT solver for K=0004                        = 0

#1 "40 sat:90:solved"="true";"30 problem:05:number of primitive actions"="107";"30 problem:01:number of constants"="0";"30 problem:04:number of abstract actions"="36";"02 properties:04:tail recursive"="false";"00 global:80:peak memory"="218103808";"40 sat:20:state formula"="3208";"40 sat:01:number of variables"="2049";"40 sat:14:K offset"="0";"40 sat:30:number of paths"="48";"40 sat:00:plan length"="-1";"40 sat:50:number of ordering clauses"="0";"02 properties:02:mostly acyclic"="false";"30 problem:06:number of methods"="96";"02 properties:05:totally ordered"="true";"02 properties:06:last task in all methods"="true";"30 problem:03:number of actions"="143";"30 problem:02:number of predicates"="30";"40 sat:03:number of horn"="5053";"40 sat:15:K chosen value"="4";"02 properties:03:regular"="false";"40 sat:03:average size of clauses"="2.336116152450091";"40 sat:02:number of clauses"="5510";"40 sat:50:number of state clauses"="3208";"40 sat:03:number of assert"="14";"40 sat:22:method children clauses"="0";"00 global:90:planner result"="SOLUTION";"02 properties:01:acyclic"="false";"40 sat:31:maximum plan length"="48";"40 sat:50:number of decomposition clauses"="2302";"00 global:02:randomseed"="42";"40 sat:91:timeout"="false";"01 parsing:01:file parser"="442";"40 sat:41:SAT solver for K=0004"="0";"40 sat:00:total"="262";"40 sat:20:transform to DIMACS"="16";"40 sat:11:generate path decomposition tree"="70";"02 preprocessing:07:compile methods with identical tasks"="1";"01 parsing:04:shop methods"="10";"02 preprocessing:08:removing unnecessary predicates"="38";"01 parsing:03:closed world assumption"="37";"02 preprocessing:11:lifted reachabiltiy analysis"="34";"01 parsing:02:sort expansion"="87";"40 sat:12:normalise path decomposition tree"="27";"40 sat:40:SAT solver"="0";"01 parsing:00:total"="647";"02 preprocessing:06:expand choiceless abstract tasks"="1";"01 parsing:06:strip domain of hybridity"="27";"40 sat:13:translate path decomposition tree to clauses"="67";"40 sat:10:generate formula"="224";"02 preprocessing:01:compile negative preconditions"="20";"00 total:00:total"="2075";"02 preprocessing:12:grounded planning graph analysis"="182";"02 preprocessing:02:compile unit methods"="0";"02 preprocessing:23:grounded task decomposition graph analysis"="239";"02 preprocessing:04:split parameter"="14";"01 parsing:07:flatten formula"="25";"02 preprocessing:05:expand choiceless abstract tasks"="20";"02 preprocessing:00:total"="615";"02 preprocessing:99:create artificial top task"="2";"01 parsing:05:eliminate identical variables"="18";"02 preprocessing:84:grounding"="56"
SOLUTION SEQUENCE
0: SHOP_methodm7_get_soil_data_7_precondition(rover0store,rover0)
1: SHOP_methodm0_do_navigate1_0_precondition(rover0,waypoint3)
2: nop()
3: SHOP_methodm5_empty_store_5_precondition(rover0store)
4: nop()
5: sample_soil(rover0,rover0store,waypoint3)
6: SHOP_methodm9_send_soil_data_9_precondition(general,waypoint2,waypoint3,rover0)
7: communicate_soil_data2(rover0,general,waypoint3,waypoint2)
8: SHOP_methodm10_get_rock_data_10_precondition(rover1store,rover1)
9: SHOP_methodm1_do_navigate1_1_precondition(rover1,waypoint2)
10: visit(waypoint2)
11: SHOP_methodm3_do_navigate2_3_precondition(rover1,waypoint2,waypoint1)
12: navigate(rover1,waypoint2,waypoint1)
13: unvisit(waypoint2)
14: SHOP_methodm5_empty_store_5_precondition(rover1store)
15: nop()
16: sample_rock(rover1,rover1store,waypoint1)
17: SHOP_methodm11_send_rock_data_11_precondition(general,waypoint2,waypoint1)
18: SHOP_methodm1_do_navigate1_1_precondition(rover1,waypoint1)
19: visit(waypoint1)
20: SHOP_methodm2_do_navigate2_2_precondition(rover1,waypoint1)
21: nop()
22: unvisit(waypoint1)
23: communicate_rock_data1(rover1,general,waypoint1,waypoint1,waypoint2)
24: SHOP_methodm13_get_image_data_13_precondition(waypoint1,rover1,high_res,camera0,objective0)
25: SHOP_methodm15_do_calibrate_15_precondition(camera0,objective0,waypoint1)
26: SHOP_methodm0_do_navigate1_0_precondition(rover1,waypoint1)
27: nop()
28: calibrate(rover1,camera0,objective0,waypoint1)
29: SHOP_methodm0_do_navigate1_0_precondition(rover1,waypoint1)
30: nop()
31: take_image(rover1,waypoint1,objective0,camera0,high_res)
32: SHOP_methodm14_send_image_data_14_precondition(general,waypoint2,waypoint1)
33: SHOP_methodm0_do_navigate1_0_precondition(rover1,waypoint1)
34: nop()
35: communicate_image_data(rover1,general,objective0,high_res,waypoint1,waypoint2)
