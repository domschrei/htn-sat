PANDA - Planning and Acting in a Network Decomposition Architecture
Believe us: It's great, it's fantastic!

PANDA Copyright (C) 2014-2018 Gregor Behnke, Pascal Bercher, Thomas Geier, Kadir
Dede, Daniel Höller, Kristof Mickeleit, Matthias Englert
This program comes with ABSOLUTELY NO WARRANTY
This is free software, and you are welcome to redistribute it under certain
conditions; run PANDA with -license for details.

Main Developers:
- Gregor Behnke, http://www.uni-ulm.de/in/ki/behnke
- Daniel Höller, http://www.uni-ulm.de/in/ki/hoeller

With many thanks to various further contributors.
Run PANDA with the command line argument -contributors for an extensive list.

Run it with -help for more information like available options.


PANDA was called with: "-systemConfig AAAI-2018-totSAT(minisat) -programPath minisat=./minisat problems/hddl/Entertainment/domains/domain.hddl problems/hddl/Entertainment/problems/p06-use-twice.hddl"


Planner Configuration
=====================
Domain: problems/hddl/Entertainment/domains/domain.hddl
Problem: problems/hddl/Entertainment/problems/p06-use-twice.hddl
Output: none

Planning Configuration
======================
	printGeneralInformation : true
	printAdditionalData     : true
	random seed             : 42
	time limit (in seconds) : none

	external programs:
		minisat : ./minisat

	Parsing Configuration
	---------------------
	Parser                : autodetect file-type
	Expand Sort Hierarchy : true
	ClosedWordAssumption  : true
	CompileSHOPMethods    : true
	Eliminate Equality    : true
	Strip Hybridity       : true
	Reduce General Tasks  : true
	
	Preprocessing Configuration
	---------------------------
	Compile negative preconditions    : true
	Compile unit methods              : false
	Compile order in methods          : false
	Compile initial plan              : true
	Ensure Methods Have Last Task     : false
	Split independent parameters      : true
	Remove unnecessary predicates     : true
	Expand choiceless abstract tasks  : true
	Domain Cleanup                    : true
	Convert to SAS+                   : false
	Grounded Reachability Analysis    : Planning Graph (mutex-free)
	Grounded Task Decomposition Graph : Two Way TDG
	Iterate reachability analysis     : true
	Ground domain                     : true
	Iterate reachability analysis     : true
	Stop directly after grounding     : false
	
	SAT-Planning Configuration
	--------------------------
	solver           : minisat
	full planner run : true
	reduction method : only normalise 
	check result     : true
	
	Post-processing Configuration
	-----------------------------
	search status
	search result
	timings
	statistics
#0 "00 global:01:problem"="p06-use-twice.hddl";"00 global:00:domain"="domain.hddl"
Parsing domain ... using HDDL parser ... done
Preparing internal domain representation ... done.
Initial domain
	number of abstract tasks = 14
	number of tasks = 29
	number of decomposition methods = 30
	number of predicates = 9
	number of sorts = 3
	number of tasks in largest method = 3
	number of epsilon methods = 0
	number of primitive tasks = 15
	number of constants = 26
Compiling negative preconditions ... done.
	number of abstract tasks = 14
	number of tasks = 29
	number of decomposition methods = 30
	number of predicates = 18
	number of sorts = 3
	number of tasks in largest method = 3
	number of epsilon methods = 0
	number of primitive tasks = 15
	number of constants = 26
Compiling split parameters ... done.
	number of abstract tasks = 14
	number of tasks = 29
	number of decomposition methods = 30
	number of predicates = 18
	number of sorts = 3
	number of tasks in largest method = 3
	number of epsilon methods = 0
	number of primitive tasks = 15
	number of constants = 26
Lifted reachability analysis and domain cleanup ... done.
	number of abstract tasks = 9
	number of tasks = 18
	number of decomposition methods = 19
	number of predicates = 11
	number of sorts = 3
	number of tasks in largest method = 3
	number of epsilon methods = 0
	number of primitive tasks = 9
	number of constants = 26
Grounded planning graph ... done.
	Number of Grounded Actions 448
	Number of Grounded Literals 309
	number of abstract tasks = 8
	number of tasks = 15
	number of decomposition methods = 16
	number of predicates = 11
	number of sorts = 3
	number of tasks in largest method = 3
	number of epsilon methods = 0
	number of primitive tasks = 7
	number of constants = 26
Two Way TDG ... done.
	number of abstract tasks = 8
	number of tasks = 15
	number of decomposition methods = 16
	number of predicates = 11
	number of sorts = 3
	number of tasks in largest method = 3
	number of epsilon methods = 0
	number of primitive tasks = 7
	number of constants = 26
Grounding ... done.
	number of abstract tasks = 316
	number of tasks = 560
	number of decomposition methods = 944
	number of predicates = 327
	number of sorts = 0
	number of tasks in largest method = 3
	number of epsilon methods = 0
	number of primitive tasks = 244
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 316
	number of tasks = 560
	number of decomposition methods = 944
	number of predicates = 96
	number of sorts = 0
	number of tasks in largest method = 3
	number of epsilon methods = 0
	number of primitive tasks = 244
	number of constants = 0
Omitting lifted reachability analysis ... 
Grounded planning graph ... done.
	Number of Grounded Actions 200
	Number of Grounded Literals 80
	number of abstract tasks = 230
	number of tasks = 430
	number of decomposition methods = 499
	number of predicates = 96
	number of sorts = 0
	number of tasks in largest method = 3
	number of epsilon methods = 0
	number of primitive tasks = 200
	number of constants = 0
Two Way TDG ... done.
	number of abstract tasks = 230
	number of tasks = 430
	number of decomposition methods = 499
	number of predicates = 96
	number of sorts = 0
	number of tasks in largest method = 3
	number of epsilon methods = 0
	number of primitive tasks = 200
	number of constants = 0
Compiling expand choiceless abstract tasks ... done.
	number of abstract tasks = 134
	number of tasks = 334
	number of decomposition methods = 403
	number of predicates = 96
	number of sorts = 0
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 200
	number of constants = 0
Compiling abstract tasks without methods ... done.
	number of abstract tasks = 134
	number of tasks = 334
	number of decomposition methods = 403
	number of predicates = 96
	number of sorts = 0
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 200
	number of constants = 0
Compiling initial plan ... done.
	number of abstract tasks = 135
	number of tasks = 335
	number of decomposition methods = 404
	number of predicates = 96
	number of sorts = 0
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 200
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 135
	number of tasks = 335
	number of decomposition methods = 404
	number of predicates = 64
	number of sorts = 0
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 200
	number of constants = 0
Omitting lifted reachability analysis ... 
Grounded planning graph ... done.
	Number of Grounded Actions 200
	Number of Grounded Literals 80
	number of abstract tasks = 135
	number of tasks = 335
	number of decomposition methods = 404
	number of predicates = 64
	number of sorts = 0
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 200
	number of constants = 0
Two Way TDG ... done.
	number of abstract tasks = 135
	number of tasks = 335
	number of decomposition methods = 404
	number of predicates = 64
	number of sorts = 0
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 200
	number of constants = 0
Compiling expand choiceless abstract tasks ... done.
	number of abstract tasks = 134
	number of tasks = 334
	number of decomposition methods = 403
	number of predicates = 64
	number of sorts = 0
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 200
	number of constants = 0
Compiling abstract tasks without methods ... done.
	number of abstract tasks = 134
	number of tasks = 334
	number of decomposition methods = 403
	number of predicates = 64
	number of sorts = 0
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 200
	number of constants = 0
Compiling initial plan ... done.
	number of abstract tasks = 135
	number of tasks = 335
	number of decomposition methods = 404
	number of predicates = 64
	number of sorts = 0
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 200
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 135
	number of tasks = 335
	number of decomposition methods = 404
	number of predicates = 64
	number of sorts = 0
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 200
	number of constants = 0
Tasks 200 - 96
Domain is acyclic: false
Domain is mostly acyclic: false
Domain is regular: false
Domain is tail recursive: false
Domain is totally ordered: true
Domain has last task in all methods: true
Time remaining for planner 9223372036854773465ms

Running SAT search with K = 0
Time remaining for SAT search 9223372036854773451ms
Time used for this run 9223372036854773451ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 65
NUMBER OF STATE CLAUSES 64
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 98.46000000000001% 1.54% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 72586301-2eb8-4b04-bbe9-eea59e5b2a3f
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
Still waiting ... running for 102 will abort at 9223372036854773451
ERROR false

Running SAT search with K = 1
Time remaining for SAT search 9223372036854773346ms
Time used for this run 9223372036854773346ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 65
NUMBER OF STATE CLAUSES 64
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 98.46000000000001% 1.54% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID e11421ac-c1fe-4499-bf0e-9811931de717
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
Still waiting ... running for 100 will abort at 9223372036854773346
ERROR false

Running SAT search with K = 2
Time remaining for SAT search 9223372036854773244ms
Time used for this run 9223372036854773244ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 65
NUMBER OF STATE CLAUSES 64
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 98.46000000000001% 1.54% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 315dfe89-d8dd-4ec6-8cb8-d67dd3b143d5
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
Still waiting ... running for 100 will abort at 9223372036854773244
ERROR false

Running SAT search with K = 3
Time remaining for SAT search 9223372036854773143ms
Time used for this run 9223372036854773143ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 65
NUMBER OF STATE CLAUSES 64
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 98.46000000000001% 1.54% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID e380e463-d33a-422a-ad36-8fe2fe246cdb
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
Still waiting ... running for 100 will abort at 9223372036854773143
ERROR false

Running SAT search with K = 4
Time remaining for SAT search 9223372036854773042ms
Time used for this run 9223372036854773042ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... Still waiting ... running for 100 will abort at 9223372036854773042
done
NUMBER OF CLAUSES 5456
NUMBER OF STATE CLAUSES 3040
NUMBER OF DECOMPOSITION CLAUSES 2416
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 55.72% 44.28% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 2ee6512b-210f-49d3-94de-bc0b23c9c0e7
FLUSH
CLOSE
NUMBER OF PATHS 21
Starting minisat
Setting starttime of solver to 1539950129922
Command exited with non-zero status 20
0.00 0.00

Time command gave the following runtime for the solver: 0
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... done
SAT-Solver says: UNSAT
ERROR false

Running SAT search with K = 5
Time remaining for SAT search 9223372036854772822ms
Time used for this run 9223372036854772822ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... Still waiting ... running for 100 will abort at 9223372036854772822
done
NUMBER OF CLAUSES 30866
NUMBER OF STATE CLAUSES 10516
NUMBER OF DECOMPOSITION CLAUSES 20350
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 34.07% 65.93% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 691305e0-4fc7-4766-b2d6-d1dd8a485d76
FLUSH
CLOSE
NUMBER OF PATHS 66
Starting minisat
Setting starttime of solver to 1539950130217
Command exited with non-zero status 10
0.01 0.00

Time command gave the following runtime for the solver: 10
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... done
SAT-Solver says: SAT

extracting solution


CHECKING primitive solution of length 30 ...
true 74 SHOP_methodm-dconnect-v_10_precondition[blu-ray-1-scart-1,scart-cable-3-scart-1]
true 40 CONSEQUENT__CONSEQUENT__plug[blu-ray-1,blu-ray-1-scart-1,scart-cable-3,scart-cable-3-scart-1]
true 71 SHOP_methodm-dconnect-v_10_precondition[scart-cable-3-scart-2,multi-scart-1-scart-2]
true 51 CONSEQUENT__CONSEQUENT__plug[scart-cable-3,scart-cable-3-scart-2,multi-scart-1,multi-scart-1-scart-2]
true 68 SHOP_methodm-dconnect-a-empty_12_precondition[blu-ray-1,scart-cable-3]
true 47 SHOP_methodm-dconnect-a-empty_12_precondition[scart-cable-3,multi-scart-1]
true 44 SHOP_methodm-dconnect-v_10_precondition[multi-scart-1-scart-1,scart-cable-1-scart-1]
true 58 CONSEQUENT__CONSEQUENT__plug[multi-scart-1,multi-scart-1-scart-1,scart-cable-1,scart-cable-1-scart-1]
true 67 SHOP_methodm-dconnect-v_10_precondition[scart-cable-1-scart-2,tv1-1-scart-1]
true 34 CONSEQUENT__CONSEQUENT__plug[scart-cable-1,scart-cable-1-scart-2,tv1-1,tv1-1-scart-1]
true 43 SHOP_methodm-dconnect-a-empty_12_precondition[multi-scart-1,scart-cable-1]
true 65 SHOP_methodm-dconnect-a-empty_12_precondition[scart-cable-1,tv1-1]
true 16 SHOP_methodm-dconnect-v_10_precondition[dvd-1-scart-1,scart-cable-2-scart-1]
true 46 CONSEQUENT__CONSEQUENT__plug[dvd-1,dvd-1-scart-1,scart-cable-2,scart-cable-2-scart-1]
true 8 SHOP_methodm-dconnect-a-empty_12_precondition[dvd-1,scart-cable-2]
true 56 SHOP_methodm-dconnect-v_10_precondition[scart-cable-2-scart-2,multi-scart-1-scart-4]
true 61 CONSEQUENT__CONSEQUENT__plug[scart-cable-2,scart-cable-2-scart-2,multi-scart-1,multi-scart-1-scart-4]
true 54 SHOP_methodm-dconnect-v-empty_13_precondition[multi-scart-1,scart-cable-1]
true 15 SHOP_methodm-dconnect-a-empty_12_precondition[scart-cable-2,multi-scart-1]
true 0 SHOP_methodm-dconnect-a-empty_12_precondition[multi-scart-1,scart-cable-1]
true 26 SHOP_methodm-dconnect-av-empty_11_precondition[scart-cable-1,tv1-1]
true 62 SHOP_methodm-dconnect-v_10_precondition[game-console-1-scart-1,scart-cable-4-scart-1]
true 19 CONSEQUENT__CONSEQUENT__plug[game-console-1,game-console-1-scart-1,scart-cable-4,scart-cable-4-scart-1]
true 23 SHOP_methodm-dconnect-a-empty_12_precondition[game-console-1,scart-cable-4]
true 38 SHOP_methodm-dconnect-v_10_precondition[scart-cable-4-scart-2,multi-scart-1-scart-3]
true 12 CONSEQUENT__CONSEQUENT__plug[scart-cable-4,scart-cable-4-scart-2,multi-scart-1,multi-scart-1-scart-3]
true 29 SHOP_methodm-dconnect-v-empty_13_precondition[multi-scart-1,scart-cable-1]
true 17 SHOP_methodm-dconnect-a-empty_12_precondition[scart-cable-4,multi-scart-1]
true 11 SHOP_methodm-dconnect-a-empty_12_precondition[multi-scart-1,scart-cable-1]
true 6 SHOP_methodm-dconnect-av-empty_11_precondition[scart-cable-1,tv1-1]
 done.
ERROR false
Panda says: SOLUTION
============ global ============
randomseed     = 42
peak memory    = 645922816
planner result = SOLUTION
============ properties ============
acyclic                  = false
mostly acyclic           = false
regular                  = false
tail recursive           = false
totally ordered          = true
last task in all methods = true
============ problem ============
number of constants         = 0
number of predicates        = 64
number of actions           = 335
number of abstract actions  = 135
number of primitive actions = 200
number of methods           = 404
============ sat ============
plan length                     = -1
number of variables             = 7575
number of clauses               = 30866
average size of clauses         = 2.8670057668632154
number of assert                = 17
number of horn                  = 27605
K offset                        = 0
K chosen value                  = 5
state formula                   = 10516
method children clauses         = 0
number of paths                 = 66
maximum plan length             = 66
number of decomposition clauses = 20350
number of ordering clauses      = 0
number of state clauses         = 10516
solved                          = true
timeout                         = false

----------------- TIMINGS -----------------
============ total ============
total = 3405
============ parsing ============
total                         = 675
file parser                   = 438
sort expansion                = 84
closed world assumption       = 43
shop methods                  = 11
eliminate identical variables = 22
strip domain of hybridity     = 32
flatten formula               = 45
============ preprocessing ============
total                                      = 1596
compile negative preconditions             = 18
compile unit methods                       = 0
split parameter                            = 14
expand choiceless abstract tasks           = 168
expand choiceless abstract tasks           = 4
compile methods with identical tasks       = 0
removing unnecessary predicates            = 183
lifted reachabiltiy analysis               = 32
grounded planning graph analysis           = 536
grounded task decomposition graph analysis = 521
grounding                                  = 104
create artificial top task                 = 3
============ sat ============
total                                        = 522
generate formula                             = 441
generate path decomposition tree             = 125
normalise path decomposition tree            = 55
translate path decomposition tree to clauses = 127
transform to DIMACS                          = 34
SAT solver                                   = 10
SAT solver for K=0004                        = 0
SAT solver for K=0005                        = 10

#1 "40 sat:90:solved"="true";"30 problem:05:number of primitive actions"="200";"30 problem:01:number of constants"="0";"30 problem:04:number of abstract actions"="135";"02 properties:04:tail recursive"="false";"00 global:80:peak memory"="645922816";"40 sat:20:state formula"="10516";"40 sat:01:number of variables"="7575";"40 sat:14:K offset"="0";"40 sat:30:number of paths"="66";"40 sat:00:plan length"="-1";"40 sat:50:number of ordering clauses"="0";"02 properties:02:mostly acyclic"="false";"30 problem:06:number of methods"="404";"02 properties:05:totally ordered"="true";"02 properties:06:last task in all methods"="true";"30 problem:03:number of actions"="335";"30 problem:02:number of predicates"="64";"40 sat:03:number of horn"="27605";"40 sat:15:K chosen value"="5";"02 properties:03:regular"="false";"40 sat:03:average size of clauses"="2.8670057668632154";"40 sat:02:number of clauses"="30866";"40 sat:50:number of state clauses"="10516";"40 sat:03:number of assert"="17";"40 sat:22:method children clauses"="0";"00 global:90:planner result"="SOLUTION";"02 properties:01:acyclic"="false";"40 sat:31:maximum plan length"="66";"40 sat:50:number of decomposition clauses"="20350";"00 global:02:randomseed"="42";"40 sat:91:timeout"="false";"01 parsing:01:file parser"="438";"40 sat:41:SAT solver for K=0004"="0";"40 sat:00:total"="522";"40 sat:20:transform to DIMACS"="34";"40 sat:11:generate path decomposition tree"="125";"02 preprocessing:07:compile methods with identical tasks"="0";"01 parsing:04:shop methods"="11";"02 preprocessing:08:removing unnecessary predicates"="183";"01 parsing:03:closed world assumption"="43";"02 preprocessing:11:lifted reachabiltiy analysis"="32";"01 parsing:02:sort expansion"="84";"40 sat:12:normalise path decomposition tree"="55";"40 sat:40:SAT solver"="10";"01 parsing:00:total"="675";"02 preprocessing:06:expand choiceless abstract tasks"="4";"01 parsing:06:strip domain of hybridity"="32";"40 sat:13:translate path decomposition tree to clauses"="127";"40 sat:10:generate formula"="441";"40 sat:41:SAT solver for K=0005"="10";"02 preprocessing:01:compile negative preconditions"="18";"00 total:00:total"="3405";"02 preprocessing:12:grounded planning graph analysis"="536";"02 preprocessing:02:compile unit methods"="0";"02 preprocessing:23:grounded task decomposition graph analysis"="521";"02 preprocessing:04:split parameter"="14";"01 parsing:07:flatten formula"="45";"02 preprocessing:05:expand choiceless abstract tasks"="168";"02 preprocessing:00:total"="1596";"02 preprocessing:99:create artificial top task"="3";"01 parsing:05:eliminate identical variables"="22";"02 preprocessing:84:grounding"="104"
SOLUTION SEQUENCE
0: SHOP_methodm-dconnect-v_10_precondition(blu-ray-1-scart-1,scart-cable-3-scart-1)
1: CONSEQUENT__CONSEQUENT__plug(blu-ray-1,blu-ray-1-scart-1,scart-cable-3,scart-cable-3-scart-1)
2: SHOP_methodm-dconnect-v_10_precondition(scart-cable-3-scart-2,multi-scart-1-scart-2)
3: CONSEQUENT__CONSEQUENT__plug(scart-cable-3,scart-cable-3-scart-2,multi-scart-1,multi-scart-1-scart-2)
4: SHOP_methodm-dconnect-a-empty_12_precondition(blu-ray-1,scart-cable-3)
5: SHOP_methodm-dconnect-a-empty_12_precondition(scart-cable-3,multi-scart-1)
6: SHOP_methodm-dconnect-v_10_precondition(multi-scart-1-scart-1,scart-cable-1-scart-1)
7: CONSEQUENT__CONSEQUENT__plug(multi-scart-1,multi-scart-1-scart-1,scart-cable-1,scart-cable-1-scart-1)
8: SHOP_methodm-dconnect-v_10_precondition(scart-cable-1-scart-2,tv1-1-scart-1)
9: CONSEQUENT__CONSEQUENT__plug(scart-cable-1,scart-cable-1-scart-2,tv1-1,tv1-1-scart-1)
10: SHOP_methodm-dconnect-a-empty_12_precondition(multi-scart-1,scart-cable-1)
11: SHOP_methodm-dconnect-a-empty_12_precondition(scart-cable-1,tv1-1)
12: SHOP_methodm-dconnect-v_10_precondition(dvd-1-scart-1,scart-cable-2-scart-1)
13: CONSEQUENT__CONSEQUENT__plug(dvd-1,dvd-1-scart-1,scart-cable-2,scart-cable-2-scart-1)
14: SHOP_methodm-dconnect-a-empty_12_precondition(dvd-1,scart-cable-2)
15: SHOP_methodm-dconnect-v_10_precondition(scart-cable-2-scart-2,multi-scart-1-scart-4)
16: CONSEQUENT__CONSEQUENT__plug(scart-cable-2,scart-cable-2-scart-2,multi-scart-1,multi-scart-1-scart-4)
17: SHOP_methodm-dconnect-v-empty_13_precondition(multi-scart-1,scart-cable-1)
18: SHOP_methodm-dconnect-a-empty_12_precondition(scart-cable-2,multi-scart-1)
19: SHOP_methodm-dconnect-a-empty_12_precondition(multi-scart-1,scart-cable-1)
20: SHOP_methodm-dconnect-av-empty_11_precondition(scart-cable-1,tv1-1)
21: SHOP_methodm-dconnect-v_10_precondition(game-console-1-scart-1,scart-cable-4-scart-1)
22: CONSEQUENT__CONSEQUENT__plug(game-console-1,game-console-1-scart-1,scart-cable-4,scart-cable-4-scart-1)
23: SHOP_methodm-dconnect-a-empty_12_precondition(game-console-1,scart-cable-4)
24: SHOP_methodm-dconnect-v_10_precondition(scart-cable-4-scart-2,multi-scart-1-scart-3)
25: CONSEQUENT__CONSEQUENT__plug(scart-cable-4,scart-cable-4-scart-2,multi-scart-1,multi-scart-1-scart-3)
26: SHOP_methodm-dconnect-v-empty_13_precondition(multi-scart-1,scart-cable-1)
27: SHOP_methodm-dconnect-a-empty_12_precondition(scart-cable-4,multi-scart-1)
28: SHOP_methodm-dconnect-a-empty_12_precondition(multi-scart-1,scart-cable-1)
29: SHOP_methodm-dconnect-av-empty_11_precondition(scart-cable-1,tv1-1)
