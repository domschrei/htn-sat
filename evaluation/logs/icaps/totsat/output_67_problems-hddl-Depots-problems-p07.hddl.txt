PANDA - Planning and Acting in a Network Decomposition Architecture
Believe us: It's great, it's fantastic!

PANDA Copyright (C) 2014-2018 Gregor Behnke, Pascal Bercher, Thomas Geier, Kadir
Dede, Daniel Höller, Kristof Mickeleit, Matthias Englert
This program comes with ABSOLUTELY NO WARRANTY
This is free software, and you are welcome to redistribute it under certain
conditions; run PANDA with -license for details.

Main Developers:
- Gregor Behnke, http://www.uni-ulm.de/in/ki/behnke
- Daniel Höller, http://www.uni-ulm.de/in/ki/hoeller

With many thanks to various further contributors.
Run PANDA with the command line argument -contributors for an extensive list.

Run it with -help for more information like available options.


PANDA was called with: "-systemConfig AAAI-2018-totSAT(minisat) -programPath minisat=./minisat problems/hddl/Depots/domains/domain.hddl problems/hddl/Depots/problems/p07.hddl"


Planner Configuration
=====================
Domain: problems/hddl/Depots/domains/domain.hddl
Problem: problems/hddl/Depots/problems/p07.hddl
Output: none

Planning Configuration
======================
	printGeneralInformation : true
	printAdditionalData     : true
	random seed             : 42
	time limit (in seconds) : none

	external programs:
		minisat : ./minisat

	Parsing Configuration
	---------------------
	Parser                : autodetect file-type
	Expand Sort Hierarchy : true
	ClosedWordAssumption  : true
	CompileSHOPMethods    : true
	Eliminate Equality    : true
	Strip Hybridity       : true
	Reduce General Tasks  : true
	
	Preprocessing Configuration
	---------------------------
	Compile negative preconditions    : true
	Compile unit methods              : false
	Compile order in methods          : false
	Compile initial plan              : true
	Ensure Methods Have Last Task     : false
	Split independent parameters      : true
	Remove unnecessary predicates     : true
	Expand choiceless abstract tasks  : true
	Domain Cleanup                    : true
	Convert to SAS+                   : false
	Grounded Reachability Analysis    : Planning Graph (mutex-free)
	Grounded Task Decomposition Graph : Two Way TDG
	Iterate reachability analysis     : true
	Ground domain                     : true
	Iterate reachability analysis     : true
	Stop directly after grounding     : false
	
	SAT-Planning Configuration
	--------------------------
	solver           : minisat
	full planner run : true
	reduction method : only normalise 
	check result     : true
	
	Post-processing Configuration
	-----------------------------
	search status
	search result
	timings
	statistics
#0 "00 global:01:problem"="p07.hddl";"00 global:00:domain"="domain.hddl"
Parsing domain ... using HDDL parser ... done
Preparing internal domain representation ... done.
Initial domain
	number of abstract tasks = 6
	number of tasks = 23
	number of decomposition methods = 12
	number of predicates = 6
	number of sorts = 10
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 17
	number of constants = 20
Compiling negative preconditions ... done.
	number of abstract tasks = 6
	number of tasks = 23
	number of decomposition methods = 12
	number of predicates = 12
	number of sorts = 10
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 17
	number of constants = 20
Compiling split parameters ... done.
	number of abstract tasks = 7
	number of tasks = 24
	number of decomposition methods = 13
	number of predicates = 12
	number of sorts = 10
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 17
	number of constants = 20
Lifted reachability analysis and domain cleanup ... done.
	number of abstract tasks = 7
	number of tasks = 24
	number of decomposition methods = 13
	number of predicates = 8
	number of sorts = 10
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 17
	number of constants = 20
Grounded planning graph ... done.
	Number of Grounded Actions 244
	Number of Grounded Literals 200
	number of abstract tasks = 7
	number of tasks = 24
	number of decomposition methods = 13
	number of predicates = 8
	number of sorts = 10
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 17
	number of constants = 20
Two Way TDG ... done.
	number of abstract tasks = 7
	number of tasks = 24
	number of decomposition methods = 13
	number of predicates = 8
	number of sorts = 10
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 17
	number of constants = 20
Grounding ... done.
	number of abstract tasks = 89
	number of tasks = 266
	number of decomposition methods = 196
	number of predicates = 118
	number of sorts = 0
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 177
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 89
	number of tasks = 266
	number of decomposition methods = 196
	number of predicates = 87
	number of sorts = 0
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 177
	number of constants = 0
Omitting lifted reachability analysis ... 
Grounded planning graph ... done.
	Number of Grounded Actions 177
	Number of Grounded Literals 168
	number of abstract tasks = 89
	number of tasks = 266
	number of decomposition methods = 196
	number of predicates = 87
	number of sorts = 0
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 177
	number of constants = 0
Two Way TDG ... done.
	number of abstract tasks = 89
	number of tasks = 266
	number of decomposition methods = 196
	number of predicates = 87
	number of sorts = 0
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 177
	number of constants = 0
Compiling expand choiceless abstract tasks ... done.
	number of abstract tasks = 30
	number of tasks = 207
	number of decomposition methods = 137
	number of predicates = 87
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 177
	number of constants = 0
Compiling abstract tasks without methods ... done.
	number of abstract tasks = 30
	number of tasks = 207
	number of decomposition methods = 137
	number of predicates = 87
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 177
	number of constants = 0
Compiling initial plan ... done.
	number of abstract tasks = 31
	number of tasks = 208
	number of decomposition methods = 138
	number of predicates = 87
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 177
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 31
	number of tasks = 208
	number of decomposition methods = 138
	number of predicates = 87
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 177
	number of constants = 0
Omitting lifted reachability analysis ... 
Grounded planning graph ... done.
	Number of Grounded Actions 177
	Number of Grounded Literals 168
	number of abstract tasks = 31
	number of tasks = 208
	number of decomposition methods = 138
	number of predicates = 87
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 177
	number of constants = 0
Two Way TDG ... done.
	number of abstract tasks = 31
	number of tasks = 208
	number of decomposition methods = 138
	number of predicates = 87
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 177
	number of constants = 0
Compiling expand choiceless abstract tasks ... done.
	number of abstract tasks = 30
	number of tasks = 207
	number of decomposition methods = 137
	number of predicates = 87
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 177
	number of constants = 0
Compiling abstract tasks without methods ... done.
	number of abstract tasks = 30
	number of tasks = 207
	number of decomposition methods = 137
	number of predicates = 87
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 177
	number of constants = 0
Compiling initial plan ... done.
	number of abstract tasks = 31
	number of tasks = 208
	number of decomposition methods = 138
	number of predicates = 87
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 177
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 31
	number of tasks = 208
	number of decomposition methods = 138
	number of predicates = 87
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 177
	number of constants = 0
Tasks 177 - 1
Domain is acyclic: true
Domain is mostly acyclic: true
Domain is regular: false
Domain is tail recursive: true
Domain is totally ordered: true
Domain has last task in all methods: true
Time remaining for planner 9223372036854773880ms

Running SAT search with K = 0
Time remaining for SAT search 9223372036854773863ms
Time used for this run 9223372036854773863ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 93
NUMBER OF STATE CLAUSES 92
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 98.92% 1.08% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 2b94d4a1-7a5d-4915-bc77-b9f00883b8ab
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
Still waiting ... running for 102 will abort at 9223372036854773863
ERROR false

Running SAT search with K = 1
Time remaining for SAT search 9223372036854773759ms
Time used for this run 9223372036854773759ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 93
NUMBER OF STATE CLAUSES 92
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 98.92% 1.08% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 1aef5309-2f9a-487f-ba69-04328b817634
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
Still waiting ... running for 100 will abort at 9223372036854773759
ERROR false

Running SAT search with K = 2
Time remaining for SAT search 9223372036854773657ms
Time used for this run 9223372036854773657ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 1901
NUMBER OF STATE CLAUSES 1837
NUMBER OF DECOMPOSITION CLAUSES 64
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 96.63% 3.37% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 34174d38-fc44-416e-b6f0-24ab9ee81263
FLUSH
CLOSE
NUMBER OF PATHS 10
Starting minisat
Setting starttime of solver to 1542620228908
Command exited with non-zero status 20
0.00 0.00

Still waiting ... running for 100 will abort at 9223372036854773657
Time command gave the following runtime for the solver: 0
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... done
SAT-Solver says: UNSAT
ERROR false

Running SAT search with K = 3
Time remaining for SAT search 9223372036854773433ms
Time used for this run 9223372036854773433ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
Still waiting ... running for 100 will abort at 9223372036854773433
NUMBER OF CLAUSES 17105
NUMBER OF STATE CLAUSES 13077
NUMBER OF DECOMPOSITION CLAUSES 4028
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 76.45% 23.55% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 90e1d6fb-a67c-4f07-9e34-d55f628800aa
FLUSH
CLOSE
NUMBER OF PATHS 68
Starting minisat
Setting starttime of solver to 1542620229205
Command exited with non-zero status 20
0.00 0.00

Time command gave the following runtime for the solver: 0
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... done
SAT-Solver says: UNSAT
ERROR false

Running SAT search with K = 4
Time remaining for SAT search 9223372036854773217ms
Time used for this run 9223372036854773217ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
Still waiting ... running for 100 will abort at 9223372036854773217
NUMBER OF CLAUSES 35690
NUMBER OF STATE CLAUSES 26994
NUMBER OF DECOMPOSITION CLAUSES 8696
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 75.63% 24.37% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 9e2c7ef8-a711-4372-bd0b-cf1e87b44a27
FLUSH
CLOSE
NUMBER OF PATHS 141
Starting minisat
Setting starttime of solver to 1542620229438
Command exited with non-zero status 10
0.01 0.00

Time command gave the following runtime for the solver: 10
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... done
SAT-Solver says: SAT

extracting solution


CHECKING primitive solution of length 58 ...
true 68 SHOP_methodm10_do_load_truck_10_precondition[crate3,distributor1,crate2,hoist2]
true 39 SHOP_methodm6_do_get_truck_6_precondition[truck0,distributor1]
true 67 nop[]
true 44 SHOP_methodm4_do_clear_4_precondition[crate3,distributor1]
true 34 nop[]
true 20 Lift[hoist2,crate3,crate2,distributor1]
true 30 Load[hoist2,crate3,truck0,distributor1]
true 45 Drive[truck0,distributor1,distributor0]
true 60 SHOP_methodm11_do_unload_truck_11_precondition[truck0,pallet1,hoist1,crate3,distributor0]
true 32 SHOP_methodm4_do_clear_4_precondition[pallet1,distributor0]
true 79 nop[]
true 87 Unload[hoist1,crate3,truck0,distributor0]
true 84 Drop[hoist1,crate3,pallet1,distributor0]
true 22 SHOP_methodm1_do_put_on_1_precondition[crate0,distributor0]
true 38 SHOP_methodm5_do_clear_5_precondition[crate0,crate4,distributor0,hoist1]
true 88 SHOP_methodm4_do_clear_4_precondition[crate4,distributor0]
true 10 nop[]
true 21 Lift[hoist1,crate4,crate0,distributor0]
true 3 SHOP_methodm7_do_get_truck_7_precondition[truck1,distributor0]
true 89 Drive[truck1,depot0,distributor0]
true 36 Load[hoist1,crate4,truck1,distributor0]
true 57 SHOP_methodm4_do_clear_4_precondition[pallet3,distributor0]
true 37 nop[]
true 42 SHOP_methodm9_do_lift_crate_9_precondition[crate0,pallet4,distributor0,hoist1]
true 40 Lift[hoist1,crate0,pallet4,distributor0]
true 82 Drop[hoist1,crate0,pallet3,distributor0]
true 4 SHOP_methodm2_do_put_on_2_precondition[crate4,truck1]
true 29 SHOP_methodm7_do_get_truck_7_precondition[truck1,distributor1]
true 53 Drive[truck1,distributor0,distributor1]
true 28 SHOP_methodm5_do_clear_5_precondition[pallet5,crate1,distributor1,hoist2]
true 16 SHOP_methodm4_do_clear_4_precondition[crate1,distributor1]
true 26 nop[]
true 7 Lift[hoist2,crate1,pallet5,distributor1]
true 48 SHOP_methodm7_do_get_truck_7_precondition[truck0,distributor1]
true 72 Drive[truck0,distributor0,distributor1]
true 66 Load[hoist2,crate1,truck0,distributor1]
true 17 Unload[hoist2,crate4,truck1,distributor1]
true 58 Drop[hoist2,crate4,pallet5,distributor1]
true 71 SHOP_methodm2_do_put_on_2_precondition[crate1,truck0]
true 56 SHOP_methodm6_do_get_truck_6_precondition[truck0,distributor1]
true 35 nop[]
true 46 SHOP_methodm4_do_clear_4_precondition[crate4,distributor1]
true 0 nop[]
true 65 Unload[hoist2,crate1,truck0,distributor1]
true 80 Drop[hoist2,crate1,crate4,distributor1]
true 49 SHOP_methodm10_do_load_truck_10_precondition[crate5,depot0,pallet0,hoist0]
true 74 SHOP_methodm7_do_get_truck_7_precondition[truck0,depot0]
true 59 Drive[truck0,distributor1,depot0]
true 1 SHOP_methodm4_do_clear_4_precondition[crate5,depot0]
true 63 nop[]
true 15 Lift[hoist0,crate5,pallet0,depot0]
true 33 Load[hoist0,crate5,truck0,depot0]
true 83 Drive[truck0,depot0,distributor1]
true 81 SHOP_methodm11_do_unload_truck_11_precondition[truck0,crate1,hoist2,crate5,distributor1]
true 70 SHOP_methodm4_do_clear_4_precondition[crate1,distributor1]
true 13 nop[]
true 5 Unload[hoist2,crate5,truck0,distributor1]
true 50 Drop[hoist2,crate5,crate1,distributor1]
 done.
ERROR false
Panda says: SOLUTION
============ global ============
randomseed     = 42
peak memory    = 511705088
planner result = SOLUTION
============ properties ============
acyclic                  = true
mostly acyclic           = true
regular                  = false
tail recursive           = true
totally ordered          = true
last task in all methods = true
============ problem ============
number of constants         = 0
number of predicates        = 87
number of actions           = 208
number of abstract actions  = 31
number of primitive actions = 177
number of methods           = 138
============ sat ============
plan length                     = -1
number of variables             = 14233
number of clauses               = 35690
average size of clauses         = 2.1706080134491454
number of assert                = 36
number of horn                  = 33697
K offset                        = 0
K chosen value                  = 4
state formula                   = 26994
method children clauses         = 0
number of paths                 = 141
maximum plan length             = 141
number of decomposition clauses = 8696
number of ordering clauses      = 0
number of state clauses         = 26994
solved                          = true
timeout                         = false

----------------- TIMINGS -----------------
============ total ============
total = 3009
============ parsing ============
total                         = 656
file parser                   = 439
sort expansion                = 90
closed world assumption       = 40
shop methods                  = 9
eliminate identical variables = 18
strip domain of hybridity     = 35
flatten formula               = 24
============ preprocessing ============
total                                      = 1198
compile negative preconditions             = 17
compile unit methods                       = 0
split parameter                            = 17
expand choiceless abstract tasks           = 151
expand choiceless abstract tasks           = 2
compile methods with identical tasks       = 0
removing unnecessary predicates            = 76
lifted reachabiltiy analysis               = 37
grounded planning graph analysis           = 337
grounded task decomposition graph analysis = 478
grounding                                  = 71
create artificial top task                 = 2
============ sat ============
total                                        = 531
generate formula                             = 430
generate path decomposition tree             = 94
normalise path decomposition tree            = 33
translate path decomposition tree to clauses = 106
transform to DIMACS                          = 52
SAT solver                                   = 10
SAT solver for K=0002                        = 0
SAT solver for K=0003                        = 0
SAT solver for K=0004                        = 10

#1 "40 sat:90:solved"="true";"30 problem:05:number of primitive actions"="177";"30 problem:01:number of constants"="0";"30 problem:04:number of abstract actions"="31";"02 properties:04:tail recursive"="true";"00 global:80:peak memory"="511705088";"40 sat:20:state formula"="26994";"40 sat:01:number of variables"="14233";"40 sat:14:K offset"="0";"40 sat:30:number of paths"="141";"40 sat:00:plan length"="-1";"40 sat:50:number of ordering clauses"="0";"02 properties:02:mostly acyclic"="true";"30 problem:06:number of methods"="138";"02 properties:05:totally ordered"="true";"02 properties:06:last task in all methods"="true";"30 problem:03:number of actions"="208";"30 problem:02:number of predicates"="87";"40 sat:03:number of horn"="33697";"40 sat:15:K chosen value"="4";"02 properties:03:regular"="false";"40 sat:03:average size of clauses"="2.1706080134491454";"40 sat:02:number of clauses"="35690";"40 sat:50:number of state clauses"="26994";"40 sat:03:number of assert"="36";"40 sat:22:method children clauses"="0";"00 global:90:planner result"="SOLUTION";"02 properties:01:acyclic"="true";"40 sat:31:maximum plan length"="141";"40 sat:50:number of decomposition clauses"="8696";"00 global:02:randomseed"="42";"40 sat:91:timeout"="false";"01 parsing:01:file parser"="439";"40 sat:41:SAT solver for K=0004"="10";"40 sat:00:total"="531";"40 sat:20:transform to DIMACS"="52";"40 sat:11:generate path decomposition tree"="94";"02 preprocessing:07:compile methods with identical tasks"="0";"01 parsing:04:shop methods"="9";"02 preprocessing:08:removing unnecessary predicates"="76";"01 parsing:03:closed world assumption"="40";"02 preprocessing:11:lifted reachabiltiy analysis"="37";"01 parsing:02:sort expansion"="90";"40 sat:12:normalise path decomposition tree"="33";"40 sat:40:SAT solver"="10";"01 parsing:00:total"="656";"02 preprocessing:06:expand choiceless abstract tasks"="2";"01 parsing:06:strip domain of hybridity"="35";"40 sat:13:translate path decomposition tree to clauses"="106";"40 sat:10:generate formula"="430";"40 sat:41:SAT solver for K=0002"="0";"02 preprocessing:01:compile negative preconditions"="17";"00 total:00:total"="3009";"02 preprocessing:12:grounded planning graph analysis"="337";"02 preprocessing:02:compile unit methods"="0";"02 preprocessing:23:grounded task decomposition graph analysis"="478";"02 preprocessing:04:split parameter"="17";"01 parsing:07:flatten formula"="24";"02 preprocessing:05:expand choiceless abstract tasks"="151";"40 sat:41:SAT solver for K=0003"="0";"02 preprocessing:00:total"="1198";"02 preprocessing:99:create artificial top task"="2";"01 parsing:05:eliminate identical variables"="18";"02 preprocessing:84:grounding"="71"
SOLUTION SEQUENCE
0: SHOP_methodm10_do_load_truck_10_precondition(crate3,distributor1,crate2,hoist2)
1: SHOP_methodm6_do_get_truck_6_precondition(truck0,distributor1)
2: nop()
3: SHOP_methodm4_do_clear_4_precondition(crate3,distributor1)
4: nop()
5: Lift(hoist2,crate3,crate2,distributor1)
6: Load(hoist2,crate3,truck0,distributor1)
7: Drive(truck0,distributor1,distributor0)
8: SHOP_methodm11_do_unload_truck_11_precondition(truck0,pallet1,hoist1,crate3,distributor0)
9: SHOP_methodm4_do_clear_4_precondition(pallet1,distributor0)
10: nop()
11: Unload(hoist1,crate3,truck0,distributor0)
12: Drop(hoist1,crate3,pallet1,distributor0)
13: SHOP_methodm1_do_put_on_1_precondition(crate0,distributor0)
14: SHOP_methodm5_do_clear_5_precondition(crate0,crate4,distributor0,hoist1)
15: SHOP_methodm4_do_clear_4_precondition(crate4,distributor0)
16: nop()
17: Lift(hoist1,crate4,crate0,distributor0)
18: SHOP_methodm7_do_get_truck_7_precondition(truck1,distributor0)
19: Drive(truck1,depot0,distributor0)
20: Load(hoist1,crate4,truck1,distributor0)
21: SHOP_methodm4_do_clear_4_precondition(pallet3,distributor0)
22: nop()
23: SHOP_methodm9_do_lift_crate_9_precondition(crate0,pallet4,distributor0,hoist1)
24: Lift(hoist1,crate0,pallet4,distributor0)
25: Drop(hoist1,crate0,pallet3,distributor0)
26: SHOP_methodm2_do_put_on_2_precondition(crate4,truck1)
27: SHOP_methodm7_do_get_truck_7_precondition(truck1,distributor1)
28: Drive(truck1,distributor0,distributor1)
29: SHOP_methodm5_do_clear_5_precondition(pallet5,crate1,distributor1,hoist2)
30: SHOP_methodm4_do_clear_4_precondition(crate1,distributor1)
31: nop()
32: Lift(hoist2,crate1,pallet5,distributor1)
33: SHOP_methodm7_do_get_truck_7_precondition(truck0,distributor1)
34: Drive(truck0,distributor0,distributor1)
35: Load(hoist2,crate1,truck0,distributor1)
36: Unload(hoist2,crate4,truck1,distributor1)
37: Drop(hoist2,crate4,pallet5,distributor1)
38: SHOP_methodm2_do_put_on_2_precondition(crate1,truck0)
39: SHOP_methodm6_do_get_truck_6_precondition(truck0,distributor1)
40: nop()
41: SHOP_methodm4_do_clear_4_precondition(crate4,distributor1)
42: nop()
43: Unload(hoist2,crate1,truck0,distributor1)
44: Drop(hoist2,crate1,crate4,distributor1)
45: SHOP_methodm10_do_load_truck_10_precondition(crate5,depot0,pallet0,hoist0)
46: SHOP_methodm7_do_get_truck_7_precondition(truck0,depot0)
47: Drive(truck0,distributor1,depot0)
48: SHOP_methodm4_do_clear_4_precondition(crate5,depot0)
49: nop()
50: Lift(hoist0,crate5,pallet0,depot0)
51: Load(hoist0,crate5,truck0,depot0)
52: Drive(truck0,depot0,distributor1)
53: SHOP_methodm11_do_unload_truck_11_precondition(truck0,crate1,hoist2,crate5,distributor1)
54: SHOP_methodm4_do_clear_4_precondition(crate1,distributor1)
55: nop()
56: Unload(hoist2,crate5,truck0,distributor1)
57: Drop(hoist2,crate5,crate1,distributor1)
