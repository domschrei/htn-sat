PANDA - Planning and Acting in a Network Decomposition Architecture
Believe us: It's great, it's fantastic!

PANDA Copyright (C) 2014-2018 Gregor Behnke, Pascal Bercher, Thomas Geier, Kadir
Dede, Daniel Höller, Kristof Mickeleit, Matthias Englert
This program comes with ABSOLUTELY NO WARRANTY
This is free software, and you are welcome to redistribute it under certain
conditions; run PANDA with -license for details.

Main Developers:
- Gregor Behnke, http://www.uni-ulm.de/in/ki/behnke
- Daniel Höller, http://www.uni-ulm.de/in/ki/hoeller

With many thanks to various further contributors.
Run PANDA with the command line argument -contributors for an extensive list.

Run it with -help for more information like available options.


PANDA was called with: "-systemConfig AAAI-2018-totSAT(minisat) -programPath minisat=./minisat problems/hddl/Childsnack/domains/domain.hddl problems/hddl/Childsnack/problems/p16.hddl"


Planner Configuration
=====================
Domain: problems/hddl/Childsnack/domains/domain.hddl
Problem: problems/hddl/Childsnack/problems/p16.hddl
Output: none

Planning Configuration
======================
	printGeneralInformation : true
	printAdditionalData     : true
	random seed             : 42
	time limit (in seconds) : none

	external programs:
		minisat : ./minisat

	Parsing Configuration
	---------------------
	Parser                : autodetect file-type
	Expand Sort Hierarchy : true
	ClosedWordAssumption  : true
	CompileSHOPMethods    : true
	Eliminate Equality    : true
	Strip Hybridity       : true
	Reduce General Tasks  : true
	
	Preprocessing Configuration
	---------------------------
	Compile negative preconditions    : true
	Compile unit methods              : false
	Compile order in methods          : false
	Compile initial plan              : true
	Ensure Methods Have Last Task     : false
	Split independent parameters      : true
	Remove unnecessary predicates     : true
	Expand choiceless abstract tasks  : true
	Domain Cleanup                    : true
	Convert to SAS+                   : false
	Grounded Reachability Analysis    : Planning Graph (mutex-free)
	Grounded Task Decomposition Graph : Two Way TDG
	Iterate reachability analysis     : true
	Ground domain                     : true
	Iterate reachability analysis     : true
	Stop directly after grounding     : false
	
	SAT-Planning Configuration
	--------------------------
	solver           : minisat
	full planner run : true
	reduction method : only normalise 
	check result     : true
	
	Post-processing Configuration
	-----------------------------
	search status
	search result
	timings
	statistics
#0 "00 global:01:problem"="p16.hddl";"00 global:00:domain"="domain.hddl"
Parsing domain ... using HDDL parser ... done
Preparing internal domain representation ... done.
Initial domain
	number of abstract tasks = 1
	number of tasks = 10
	number of decomposition methods = 2
	number of predicates = 13
	number of sorts = 7
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 9
	number of constants = 90
Compiling negative preconditions ... done.
	number of abstract tasks = 1
	number of tasks = 10
	number of decomposition methods = 2
	number of predicates = 26
	number of sorts = 7
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 9
	number of constants = 90
Compiling split parameters ... done.
	number of abstract tasks = 1
	number of tasks = 10
	number of decomposition methods = 2
	number of predicates = 26
	number of sorts = 7
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 9
	number of constants = 90
Lifted reachability analysis and domain cleanup ... done.
	number of abstract tasks = 1
	number of tasks = 9
	number of decomposition methods = 2
	number of predicates = 15
	number of sorts = 7
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 8
	number of constants = 90
Grounded planning graph ... done.
	Number of Grounded Actions 64053
	Number of Grounded Literals 528
	number of abstract tasks = 1
	number of tasks = 9
	number of decomposition methods = 2
	number of predicates = 15
	number of sorts = 7
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 8
	number of constants = 90
Two Way TDG ... done.
	number of abstract tasks = 1
	number of tasks = 9
	number of decomposition methods = 2
	number of predicates = 15
	number of sorts = 7
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 8
	number of constants = 90
Grounding ... done.
	number of abstract tasks = 19
	number of tasks = 58643
	number of decomposition methods = 207100
	number of predicates = 362
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 58624
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 19
	number of tasks = 58643
	number of decomposition methods = 207100
	number of predicates = 248
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 58624
	number of constants = 0
Omitting lifted reachability analysis ... 
Grounded planning graph ... done.
	Number of Grounded Actions 58624
	Number of Grounded Literals 452
	number of abstract tasks = 19
	number of tasks = 58643
	number of decomposition methods = 207100
	number of predicates = 248
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 58624
	number of constants = 0
Two Way TDG ... 