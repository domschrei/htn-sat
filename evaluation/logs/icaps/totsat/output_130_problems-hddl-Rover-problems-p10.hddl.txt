PANDA - Planning and Acting in a Network Decomposition Architecture
Believe us: It's great, it's fantastic!

PANDA Copyright (C) 2014-2018 Gregor Behnke, Pascal Bercher, Thomas Geier, Kadir
Dede, Daniel Höller, Kristof Mickeleit, Matthias Englert
This program comes with ABSOLUTELY NO WARRANTY
This is free software, and you are welcome to redistribute it under certain
conditions; run PANDA with -license for details.

Main Developers:
- Gregor Behnke, http://www.uni-ulm.de/in/ki/behnke
- Daniel Höller, http://www.uni-ulm.de/in/ki/hoeller

With many thanks to various further contributors.
Run PANDA with the command line argument -contributors for an extensive list.

Run it with -help for more information like available options.


PANDA was called with: "-systemConfig AAAI-2018-totSAT(minisat) -programPath minisat=./minisat problems/hddl/Rover/domains/domain.hddl problems/hddl/Rover/problems/p10.hddl"


Planner Configuration
=====================
Domain: problems/hddl/Rover/domains/domain.hddl
Problem: problems/hddl/Rover/problems/p10.hddl
Output: none

Planning Configuration
======================
	printGeneralInformation : true
	printAdditionalData     : true
	random seed             : 42
	time limit (in seconds) : none

	external programs:
		minisat : ./minisat

	Parsing Configuration
	---------------------
	Parser                : autodetect file-type
	Expand Sort Hierarchy : true
	ClosedWordAssumption  : true
	CompileSHOPMethods    : true
	Eliminate Equality    : true
	Strip Hybridity       : true
	Reduce General Tasks  : true
	
	Preprocessing Configuration
	---------------------------
	Compile negative preconditions    : true
	Compile unit methods              : false
	Compile order in methods          : false
	Compile initial plan              : true
	Ensure Methods Have Last Task     : false
	Split independent parameters      : true
	Remove unnecessary predicates     : true
	Expand choiceless abstract tasks  : true
	Domain Cleanup                    : true
	Convert to SAS+                   : false
	Grounded Reachability Analysis    : Planning Graph (mutex-free)
	Grounded Task Decomposition Graph : Two Way TDG
	Iterate reachability analysis     : true
	Ground domain                     : true
	Iterate reachability analysis     : true
	Stop directly after grounding     : false
	
	SAT-Planning Configuration
	--------------------------
	solver           : minisat
	full planner run : true
	reduction method : only normalise 
	check result     : true
	
	Post-processing Configuration
	-----------------------------
	search status
	search result
	timings
	statistics
#0 "00 global:01:problem"="p10.hddl";"00 global:00:domain"="domain.hddl"
Parsing domain ... using HDDL parser ... done
Preparing internal domain representation ... done.
Initial domain
	number of abstract tasks = 10
	number of tasks = 40
	number of decomposition methods = 16
	number of predicates = 26
	number of sorts = 8
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 30
	number of constants = 60
Compiling negative preconditions ... done.
	number of abstract tasks = 10
	number of tasks = 40
	number of decomposition methods = 16
	number of predicates = 52
	number of sorts = 8
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 30
	number of constants = 60
Compiling split parameters ... done.
	number of abstract tasks = 10
	number of tasks = 40
	number of decomposition methods = 16
	number of predicates = 52
	number of sorts = 8
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 30
	number of constants = 60
Lifted reachability analysis and domain cleanup ... done.
	number of abstract tasks = 10
	number of tasks = 40
	number of decomposition methods = 16
	number of predicates = 29
	number of sorts = 8
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 30
	number of constants = 60
Grounded planning graph ... done.
	Number of Grounded Actions 11839
	Number of Grounded Literals 6083
	number of abstract tasks = 10
	number of tasks = 40
	number of decomposition methods = 16
	number of predicates = 29
	number of sorts = 8
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 30
	number of constants = 60
Two Way TDG ... done.
	number of abstract tasks = 10
	number of tasks = 40
	number of decomposition methods = 16
	number of predicates = 29
	number of sorts = 8
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 30
	number of constants = 60
Grounding ... done.
	number of abstract tasks = 4544
	number of tasks = 15041
	number of decomposition methods = 17500
	number of predicates = 10796
	number of sorts = 0
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 10497
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 4544
	number of tasks = 15041
	number of decomposition methods = 17500
	number of predicates = 388
	number of sorts = 0
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 10497
	number of constants = 0
Omitting lifted reachability analysis ... 
Grounded planning graph ... done.
	Number of Grounded Actions 10497
	Number of Grounded Literals 677
	number of abstract tasks = 4544
	number of tasks = 15041
	number of decomposition methods = 17500
	number of predicates = 388
	number of sorts = 0
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 10497
	number of constants = 0
Two Way TDG ... done.
	number of abstract tasks = 4544
	number of tasks = 15041
	number of decomposition methods = 17500
	number of predicates = 388
	number of sorts = 0
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 10497
	number of constants = 0
Compiling expand choiceless abstract tasks ... done.
	number of abstract tasks = 4544
	number of tasks = 15041
	number of decomposition methods = 17500
	number of predicates = 388
	number of sorts = 0
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 10497
	number of constants = 0
Compiling abstract tasks without methods ... done.
	number of abstract tasks = 4544
	number of tasks = 15041
	number of decomposition methods = 17500
	number of predicates = 388
	number of sorts = 0
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 10497
	number of constants = 0
Compiling initial plan ... done.
	number of abstract tasks = 4545
	number of tasks = 15042
	number of decomposition methods = 17501
	number of predicates = 388
	number of sorts = 0
	number of tasks in largest method = 20
	number of epsilon methods = 0
	number of primitive tasks = 10497
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 4545
	number of tasks = 15042
	number of decomposition methods = 17501
	number of predicates = 388
	number of sorts = 0
	number of tasks in largest method = 20
	number of epsilon methods = 0
	number of primitive tasks = 10497
	number of constants = 0
Omitting lifted reachability analysis ... 
Grounded planning graph ... done.
	Number of Grounded Actions 10497
	Number of Grounded Literals 677
	number of abstract tasks = 4545
	number of tasks = 15042
	number of decomposition methods = 17501
	number of predicates = 388
	number of sorts = 0
	number of tasks in largest method = 20
	number of epsilon methods = 0
	number of primitive tasks = 10497
	number of constants = 0
Two Way TDG ... done.
	number of abstract tasks = 4545
	number of tasks = 15042
	number of decomposition methods = 17501
	number of predicates = 388
	number of sorts = 0
	number of tasks in largest method = 20
	number of epsilon methods = 0
	number of primitive tasks = 10497
	number of constants = 0
Compiling expand choiceless abstract tasks ... done.
	number of abstract tasks = 4544
	number of tasks = 15041
	number of decomposition methods = 17500
	number of predicates = 388
	number of sorts = 0
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 10497
	number of constants = 0
Compiling abstract tasks without methods ... done.
	number of abstract tasks = 4544
	number of tasks = 15041
	number of decomposition methods = 17500
	number of predicates = 388
	number of sorts = 0
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 10497
	number of constants = 0
Compiling initial plan ... done.
	number of abstract tasks = 4545
	number of tasks = 15042
	number of decomposition methods = 17501
	number of predicates = 388
	number of sorts = 0
	number of tasks in largest method = 20
	number of epsilon methods = 0
	number of primitive tasks = 10497
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 4545
	number of tasks = 15042
	number of decomposition methods = 17501
	number of predicates = 388
	number of sorts = 0
	number of tasks in largest method = 20
	number of epsilon methods = 0
	number of primitive tasks = 10497
	number of constants = 0
Tasks 10497 - 1033
Domain is acyclic: false
Domain is mostly acyclic: false
Domain is regular: false
Domain is tail recursive: false
Domain is totally ordered: true
Domain has last task in all methods: true
Time remaining for planner 9223372036854757292ms

Running SAT search with K = 0
Time remaining for SAT search 9223372036854757280ms
Time used for this run 9223372036854757280ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 409
NUMBER OF STATE CLAUSES 408
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 99.76% 0.24% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID b8271b12-5e81-4b8b-ac0c-37d1fbab72df
FLUSH
Still waiting ... running for 102 will abort at 9223372036854757280
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
ERROR false

Running SAT search with K = 1
Time remaining for SAT search 9223372036854757073ms
Time used for this run 9223372036854757073ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 409
NUMBER OF STATE CLAUSES 408
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 99.76% 0.24% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 928d8448-b7cd-4b1b-b00f-854107d3a291
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
Still waiting ... running for 100 will abort at 9223372036854757073
ERROR false

Running SAT search with K = 2
Time remaining for SAT search 9223372036854756972ms
Time used for this run 9223372036854756972ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... Still waiting ... running for 100 will abort at 9223372036854756972
done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 409
NUMBER OF STATE CLAUSES 408
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 99.76% 0.24% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID a35a5f81-2a1e-4fa4-8d8f-2896f36e97d8
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
ERROR false

Running SAT search with K = 3
Time remaining for SAT search 9223372036854756771ms
Time used for this run 9223372036854756771ms


Generating initial PDT ... Still waiting ... running for 100 will abort at 9223372036854756771
done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 409
NUMBER OF STATE CLAUSES 408
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 99.76% 0.24% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID a30732bb-1cd7-4f93-9998-25bab9159fce
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
ERROR false

Running SAT search with K = 4
Time remaining for SAT search 9223372036854756369ms
Time used for this run 9223372036854756369ms


Generating initial PDT ... Still waiting ... running for 100 will abort at 9223372036854756369
done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 964338
NUMBER OF STATE CLAUSES 225506
NUMBER OF DECOMPOSITION CLAUSES 738832
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 23.38% 76.62% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID fafad5e1-54c8-4fa1-8e0c-fd38b85f60c9
FLUSH
CLOSE
NUMBER OF PATHS 270
Starting minisat
Setting starttime of solver to 1542621721533
Command exited with non-zero status 20
2.04 0.08

Time command gave the following runtime for the solver: 2120
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... done
SAT-Solver says: UNSAT
ERROR false

Running SAT search with K = 5
Time remaining for SAT search 9223372036854744523ms
Time used for this run 9223372036854744523ms


Generating initial PDT ... Still waiting ... running for 101 will abort at 9223372036854744523
done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... Still waiting ... running for 61484 will abort at 9223372036854744523
done
