PANDA - Planning and Acting in a Network Decomposition Architecture
Believe us: It's great, it's fantastic!

PANDA Copyright (C) 2014-2018 Gregor Behnke, Pascal Bercher, Thomas Geier, Kadir
Dede, Daniel Höller, Kristof Mickeleit, Matthias Englert
This program comes with ABSOLUTELY NO WARRANTY
This is free software, and you are welcome to redistribute it under certain
conditions; run PANDA with -license for details.

Main Developers:
- Gregor Behnke, http://www.uni-ulm.de/in/ki/behnke
- Daniel Höller, http://www.uni-ulm.de/in/ki/hoeller

With many thanks to various further contributors.
Run PANDA with the command line argument -contributors for an extensive list.

Run it with -help for more information like available options.


PANDA was called with: "-systemConfig AAAI-2018-totSAT(minisat) -programPath minisat=./minisat problems/hddl/Childsnack/domains/domain.hddl problems/hddl/Childsnack/problems/p05.hddl"


Planner Configuration
=====================
Domain: problems/hddl/Childsnack/domains/domain.hddl
Problem: problems/hddl/Childsnack/problems/p05.hddl
Output: none

Planning Configuration
======================
	printGeneralInformation : true
	printAdditionalData     : true
	random seed             : 42
	time limit (in seconds) : none

	external programs:
		minisat : ./minisat

	Parsing Configuration
	---------------------
	Parser                : autodetect file-type
	Expand Sort Hierarchy : true
	ClosedWordAssumption  : true
	CompileSHOPMethods    : true
	Eliminate Equality    : true
	Strip Hybridity       : true
	Reduce General Tasks  : true
	
	Preprocessing Configuration
	---------------------------
	Compile negative preconditions    : true
	Compile unit methods              : false
	Compile order in methods          : false
	Compile initial plan              : true
	Ensure Methods Have Last Task     : false
	Split independent parameters      : true
	Remove unnecessary predicates     : true
	Expand choiceless abstract tasks  : true
	Domain Cleanup                    : true
	Convert to SAS+                   : false
	Grounded Reachability Analysis    : Planning Graph (mutex-free)
	Grounded Task Decomposition Graph : Two Way TDG
	Iterate reachability analysis     : true
	Ground domain                     : true
	Iterate reachability analysis     : true
	Stop directly after grounding     : false
	
	SAT-Planning Configuration
	--------------------------
	solver           : minisat
	full planner run : true
	reduction method : only normalise 
	check result     : true
	
	Post-processing Configuration
	-----------------------------
	search status
	search result
	timings
	statistics
#0 "00 global:01:problem"="p05.hddl";"00 global:00:domain"="domain.hddl"
Parsing domain ... using HDDL parser ... done
Preparing internal domain representation ... done.
Initial domain
	number of abstract tasks = 1
	number of tasks = 10
	number of decomposition methods = 2
	number of predicates = 13
	number of sorts = 7
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 9
	number of constants = 63
Compiling negative preconditions ... done.
	number of abstract tasks = 1
	number of tasks = 10
	number of decomposition methods = 2
	number of predicates = 26
	number of sorts = 7
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 9
	number of constants = 63
Compiling split parameters ... done.
	number of abstract tasks = 1
	number of tasks = 10
	number of decomposition methods = 2
	number of predicates = 26
	number of sorts = 7
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 9
	number of constants = 63
Lifted reachability analysis and domain cleanup ... done.
	number of abstract tasks = 1
	number of tasks = 9
	number of decomposition methods = 2
	number of predicates = 15
	number of sorts = 7
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 8
	number of constants = 63
Grounded planning graph ... done.
	Number of Grounded Actions 14862
	Number of Grounded Literals 328
	number of abstract tasks = 1
	number of tasks = 9
	number of decomposition methods = 2
	number of predicates = 15
	number of sorts = 7
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 8
	number of constants = 63
Two Way TDG ... done.
	number of abstract tasks = 1
	number of tasks = 9
	number of decomposition methods = 2
	number of predicates = 15
	number of sorts = 7
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 8
	number of constants = 63
Grounding ... done.
	number of abstract tasks = 13
	number of tasks = 13087
	number of decomposition methods = 32487
	number of predicates = 231
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 13074
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 13
	number of tasks = 13087
	number of decomposition methods = 32487
	number of predicates = 153
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 13074
	number of constants = 0
Omitting lifted reachability analysis ... 
Grounded planning graph ... done.
	Number of Grounded Actions 13074
	Number of Grounded Literals 276
	number of abstract tasks = 13
	number of tasks = 13087
	number of decomposition methods = 32487
	number of predicates = 153
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 13074
	number of constants = 0
Two Way TDG ... done.
	number of abstract tasks = 13
	number of tasks = 13087
	number of decomposition methods = 32487
	number of predicates = 153
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 13074
	number of constants = 0
Compiling expand choiceless abstract tasks ... done.
	number of abstract tasks = 13
	number of tasks = 13087
	number of decomposition methods = 32487
	number of predicates = 153
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 13074
	number of constants = 0
Compiling abstract tasks without methods ... done.
	number of abstract tasks = 13
	number of tasks = 13087
	number of decomposition methods = 32487
	number of predicates = 153
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 13074
	number of constants = 0
Compiling initial plan ... done.
	number of abstract tasks = 14
	number of tasks = 13088
	number of decomposition methods = 32488
	number of predicates = 153
	number of sorts = 0
	number of tasks in largest method = 13
	number of epsilon methods = 0
	number of primitive tasks = 13074
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 14
	number of tasks = 13088
	number of decomposition methods = 32488
	number of predicates = 153
	number of sorts = 0
	number of tasks in largest method = 13
	number of epsilon methods = 0
	number of primitive tasks = 13074
	number of constants = 0
Omitting lifted reachability analysis ... 
Grounded planning graph ... done.
	Number of Grounded Actions 13074
	Number of Grounded Literals 276
	number of abstract tasks = 14
	number of tasks = 13088
	number of decomposition methods = 32488
	number of predicates = 153
	number of sorts = 0
	number of tasks in largest method = 13
	number of epsilon methods = 0
	number of primitive tasks = 13074
	number of constants = 0
Two Way TDG ... done.
	number of abstract tasks = 14
	number of tasks = 13088
	number of decomposition methods = 32488
	number of predicates = 153
	number of sorts = 0
	number of tasks in largest method = 13
	number of epsilon methods = 0
	number of primitive tasks = 13074
	number of constants = 0
Compiling expand choiceless abstract tasks ... done.
	number of abstract tasks = 13
	number of tasks = 13087
	number of decomposition methods = 32487
	number of predicates = 153
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 13074
	number of constants = 0
Compiling abstract tasks without methods ... done.
	number of abstract tasks = 13
	number of tasks = 13087
	number of decomposition methods = 32487
	number of predicates = 153
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 13074
	number of constants = 0
Compiling initial plan ... done.
	number of abstract tasks = 14
	number of tasks = 13088
	number of decomposition methods = 32488
	number of predicates = 153
	number of sorts = 0
	number of tasks in largest method = 13
	number of epsilon methods = 0
	number of primitive tasks = 13074
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 14
	number of tasks = 13088
	number of decomposition methods = 32488
	number of predicates = 153
	number of sorts = 0
	number of tasks in largest method = 13
	number of epsilon methods = 0
	number of primitive tasks = 13074
	number of constants = 0
Tasks 13074 - 0
Domain is acyclic: true
Domain is mostly acyclic: true
Domain is regular: false
Domain is tail recursive: true
Domain is totally ordered: true
Domain has last task in all methods: true
Time remaining for planner 9223372036854744360ms

Running SAT search with K = 0
Time remaining for SAT search 9223372036854744347ms
Time used for this run 9223372036854744347ms


Still waiting ... running for 101 will abort at 9223372036854744347
Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 167
NUMBER OF STATE CLAUSES 166
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 99.4% 0.6% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 86c54862-ae86-44ce-9ed8-61855a0260cc
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
ERROR false

Running SAT search with K = 1
Time remaining for SAT search 9223372036854744142ms
Time used for this run 9223372036854744142ms


Generating initial PDT ... Still waiting ... running for 100 will abort at 9223372036854744142
done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 167
NUMBER OF STATE CLAUSES 166
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 99.4% 0.6% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 3f16afe9-ad2a-4058-b4a2-33e40df0841f
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
ERROR false

Running SAT search with K = 2
Time remaining for SAT search 9223372036854743941ms
Time used for this run 9223372036854743941ms


Generating initial PDT ... Still waiting ... running for 100 will abort at 9223372036854743941
done
Checking whether the PDT can grow any more ... no ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 1022917
NUMBER OF STATE CLAUSES 118584
NUMBER OF DECOMPOSITION CLAUSES 904333
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 11.59% 88.41% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID f69a4100-b2d0-44f6-aa06-4d29198c7296
FLUSH
CLOSE
NUMBER OF PATHS 78
Starting minisat
Setting starttime of solver to 1542618636791
Command exited with non-zero status 10
0.63 0.01

Time command gave the following runtime for the solver: 640
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... done
SAT-Solver says: SAT

extracting solution


CHECKING primitive solution of length 78 ...
true 69 SHOP_methodm0_serve_0_precondition[content11,table2,child1,sandw13,bread3]
true 13 make_sandwich_no_gluten[sandw13,bread3,content11]
true 79 put_on_tray[sandw13,tray3,kitchen]
true 24 move_tray[tray3,kitchen,table2]
true 85 serve_sandwich_no_gluten[sandw13,child1,tray3,table2]
true 80 move_tray[tray3,table2,kitchen]
true 44 SHOP_methodm1_serve_1_precondition[table3,child2,bread8,sandw11,content7]
true 28 make_sandwich[sandw11,bread8,content7]
true 9 put_on_tray[sandw11,tray1,kitchen]
true 68 move_tray[tray1,kitchen,table3]
true 38 serve_sandwich[sandw11,child2,tray1,table3]
true 64 move_tray[tray1,table3,kitchen]
true 6 SHOP_methodm1_serve_1_precondition[table3,child3,bread9,sandw10,content13]
true 51 make_sandwich[sandw10,bread9,content13]
true 32 put_on_tray[sandw10,tray1,kitchen]
true 40 move_tray[tray1,kitchen,table3]
true 22 serve_sandwich[sandw10,child3,tray1,table3]
true 57 move_tray[tray1,table3,kitchen]
true 23 SHOP_methodm0_serve_0_precondition[content6,table3,child4,sandw5,bread11]
true 83 make_sandwich_no_gluten[sandw5,bread11,content6]
true 34 put_on_tray[sandw5,tray2,kitchen]
true 70 move_tray[tray2,kitchen,table3]
true 77 serve_sandwich_no_gluten[sandw5,child4,tray2,table3]
true 21 move_tray[tray2,table3,kitchen]
true 50 SHOP_methodm1_serve_1_precondition[table2,child5,bread6,sandw4,content5]
true 3 make_sandwich[sandw4,bread6,content5]
true 72 put_on_tray[sandw4,tray2,kitchen]
true 63 move_tray[tray2,kitchen,table2]
true 61 serve_sandwich[sandw4,child5,tray2,table2]
true 87 move_tray[tray2,table2,kitchen]
true 62 SHOP_methodm1_serve_1_precondition[table1,child6,bread7,sandw14,content9]
true 60 make_sandwich[sandw14,bread7,content9]
true 88 put_on_tray[sandw14,tray3,kitchen]
true 47 move_tray[tray3,kitchen,table1]
true 78 serve_sandwich[sandw14,child6,tray3,table1]
true 42 move_tray[tray3,table1,kitchen]
true 41 SHOP_methodm1_serve_1_precondition[table3,child7,bread10,sandw6,content12]
true 45 make_sandwich[sandw6,bread10,content12]
true 48 put_on_tray[sandw6,tray2,kitchen]
true 56 move_tray[tray2,kitchen,table3]
true 66 serve_sandwich[sandw6,child7,tray2,table3]
true 12 move_tray[tray2,table3,kitchen]
true 11 SHOP_methodm0_serve_0_precondition[content4,table1,child8,sandw3,bread12]
true 46 make_sandwich_no_gluten[sandw3,bread12,content4]
true 82 put_on_tray[sandw3,tray2,kitchen]
true 75 move_tray[tray2,kitchen,table1]
true 18 serve_sandwich_no_gluten[sandw3,child8,tray2,table1]
true 89 move_tray[tray2,table1,kitchen]
true 14 SHOP_methodm1_serve_1_precondition[table1,child9,bread4,sandw15,content3]
true 84 make_sandwich[sandw15,bread4,content3]
true 35 put_on_tray[sandw15,tray1,kitchen]
true 59 move_tray[tray1,kitchen,table1]
true 27 serve_sandwich[sandw15,child9,tray1,table1]
true 31 move_tray[tray1,table1,kitchen]
true 54 SHOP_methodm1_serve_1_precondition[table3,child10,bread13,sandw8,content1]
true 19 make_sandwich[sandw8,bread13,content1]
true 17 put_on_tray[sandw8,tray3,kitchen]
true 30 move_tray[tray3,kitchen,table3]
true 0 serve_sandwich[sandw8,child10,tray3,table3]
true 91 move_tray[tray3,table3,kitchen]
true 36 SHOP_methodm1_serve_1_precondition[table1,child11,bread2,sandw9,content8]
true 65 make_sandwich[sandw9,bread2,content8]
true 10 put_on_tray[sandw9,tray1,kitchen]
true 71 move_tray[tray1,kitchen,table1]
true 39 serve_sandwich[sandw9,child11,tray1,table1]
true 2 move_tray[tray1,table1,kitchen]
true 76 SHOP_methodm0_serve_0_precondition[content10,table1,child12,sandw16,bread1]
true 33 make_sandwich_no_gluten[sandw16,bread1,content10]
true 37 put_on_tray[sandw16,tray3,kitchen]
true 67 move_tray[tray3,kitchen,table1]
true 43 serve_sandwich_no_gluten[sandw16,child12,tray3,table1]
true 20 move_tray[tray3,table1,kitchen]
true 8 SHOP_methodm0_serve_0_precondition[content2,table1,child13,sandw1,bread5]
true 55 make_sandwich_no_gluten[sandw1,bread5,content2]
true 81 put_on_tray[sandw1,tray1,kitchen]
true 5 move_tray[tray1,kitchen,table1]
true 26 serve_sandwich_no_gluten[sandw1,child13,tray1,table1]
true 90 move_tray[tray1,table1,kitchen]
 done.
ERROR false
Panda says: SOLUTION
============ global ============
randomseed     = 42
peak memory    = 3969043376
planner result = SOLUTION
============ properties ============
acyclic                  = true
mostly acyclic           = true
regular                  = false
tail recursive           = true
totally ordered          = true
last task in all methods = true
============ problem ============
number of constants         = 0
number of predicates        = 153
number of actions           = 13088
number of abstract actions  = 14
number of primitive actions = 13074
number of methods           = 32488
============ sat ============
plan length                     = -1
number of variables             = 68276
number of clauses               = 1022917
average size of clauses         = 2.0787512574333986
number of assert                = 60
number of horn                  = 1020483
K offset                        = 0
K chosen value                  = 2
state formula                   = 118584
method children clauses         = 0
number of paths                 = 78
maximum plan length             = 78
number of decomposition clauses = 904333
number of ordering clauses      = 0
number of state clauses         = 118584
solved                          = true
timeout                         = false

----------------- TIMINGS -----------------
============ total ============
total = 38879
============ parsing ============
total                         = 650
file parser                   = 416
sort expansion                = 102
closed world assumption       = 56
shop methods                  = 8
eliminate identical variables = 18
strip domain of hybridity     = 33
flatten formula               = 17
============ preprocessing ============
total                                      = 28329
compile negative preconditions             = 22
compile unit methods                       = 0
split parameter                            = 28
expand choiceless abstract tasks           = 387
expand choiceless abstract tasks           = 410
compile methods with identical tasks       = 0
removing unnecessary predicates            = 6116
lifted reachabiltiy analysis               = 39
grounded planning graph analysis           = 4723
grounded task decomposition graph analysis = 15273
grounding                                  = 1253
create artificial top task                 = 46
============ sat ============
total                                        = 5613
generate formula                             = 4785
generate path decomposition tree             = 2068
normalise path decomposition tree            = 670
translate path decomposition tree to clauses = 1077
transform to DIMACS                          = 156
SAT solver                                   = 640
SAT solver for K=0002                        = 640

#1 "40 sat:90:solved"="true";"30 problem:05:number of primitive actions"="13074";"30 problem:01:number of constants"="0";"30 problem:04:number of abstract actions"="14";"02 properties:04:tail recursive"="true";"00 global:80:peak memory"="3969043376";"40 sat:20:state formula"="118584";"40 sat:01:number of variables"="68276";"40 sat:14:K offset"="0";"40 sat:30:number of paths"="78";"40 sat:00:plan length"="-1";"40 sat:50:number of ordering clauses"="0";"02 properties:02:mostly acyclic"="true";"30 problem:06:number of methods"="32488";"02 properties:05:totally ordered"="true";"02 properties:06:last task in all methods"="true";"30 problem:03:number of actions"="13088";"30 problem:02:number of predicates"="153";"40 sat:03:number of horn"="1020483";"40 sat:15:K chosen value"="2";"02 properties:03:regular"="false";"40 sat:03:average size of clauses"="2.0787512574333986";"40 sat:02:number of clauses"="1022917";"40 sat:50:number of state clauses"="118584";"40 sat:03:number of assert"="60";"40 sat:22:method children clauses"="0";"00 global:90:planner result"="SOLUTION";"02 properties:01:acyclic"="true";"40 sat:31:maximum plan length"="78";"40 sat:50:number of decomposition clauses"="904333";"00 global:02:randomseed"="42";"40 sat:91:timeout"="false";"01 parsing:01:file parser"="416";"40 sat:00:total"="5613";"40 sat:20:transform to DIMACS"="156";"40 sat:11:generate path decomposition tree"="2068";"02 preprocessing:07:compile methods with identical tasks"="0";"01 parsing:04:shop methods"="8";"02 preprocessing:08:removing unnecessary predicates"="6116";"01 parsing:03:closed world assumption"="56";"02 preprocessing:11:lifted reachabiltiy analysis"="39";"01 parsing:02:sort expansion"="102";"40 sat:12:normalise path decomposition tree"="670";"40 sat:40:SAT solver"="640";"01 parsing:00:total"="650";"02 preprocessing:06:expand choiceless abstract tasks"="410";"01 parsing:06:strip domain of hybridity"="33";"40 sat:13:translate path decomposition tree to clauses"="1077";"40 sat:10:generate formula"="4785";"40 sat:41:SAT solver for K=0002"="640";"02 preprocessing:01:compile negative preconditions"="22";"00 total:00:total"="38879";"02 preprocessing:12:grounded planning graph analysis"="4723";"02 preprocessing:02:compile unit methods"="0";"02 preprocessing:23:grounded task decomposition graph analysis"="15273";"02 preprocessing:04:split parameter"="28";"01 parsing:07:flatten formula"="17";"02 preprocessing:05:expand choiceless abstract tasks"="387";"02 preprocessing:00:total"="28329";"02 preprocessing:99:create artificial top task"="46";"01 parsing:05:eliminate identical variables"="18";"02 preprocessing:84:grounding"="1253"
SOLUTION SEQUENCE
0: SHOP_methodm0_serve_0_precondition(content11,table2,child1,sandw13,bread3)
1: make_sandwich_no_gluten(sandw13,bread3,content11)
2: put_on_tray(sandw13,tray3,kitchen)
3: move_tray(tray3,kitchen,table2)
4: serve_sandwich_no_gluten(sandw13,child1,tray3,table2)
5: move_tray(tray3,table2,kitchen)
6: SHOP_methodm1_serve_1_precondition(table3,child2,bread8,sandw11,content7)
7: make_sandwich(sandw11,bread8,content7)
8: put_on_tray(sandw11,tray1,kitchen)
9: move_tray(tray1,kitchen,table3)
10: serve_sandwich(sandw11,child2,tray1,table3)
11: move_tray(tray1,table3,kitchen)
12: SHOP_methodm1_serve_1_precondition(table3,child3,bread9,sandw10,content13)
13: make_sandwich(sandw10,bread9,content13)
14: put_on_tray(sandw10,tray1,kitchen)
15: move_tray(tray1,kitchen,table3)
16: serve_sandwich(sandw10,child3,tray1,table3)
17: move_tray(tray1,table3,kitchen)
18: SHOP_methodm0_serve_0_precondition(content6,table3,child4,sandw5,bread11)
19: make_sandwich_no_gluten(sandw5,bread11,content6)
20: put_on_tray(sandw5,tray2,kitchen)
21: move_tray(tray2,kitchen,table3)
22: serve_sandwich_no_gluten(sandw5,child4,tray2,table3)
23: move_tray(tray2,table3,kitchen)
24: SHOP_methodm1_serve_1_precondition(table2,child5,bread6,sandw4,content5)
25: make_sandwich(sandw4,bread6,content5)
26: put_on_tray(sandw4,tray2,kitchen)
27: move_tray(tray2,kitchen,table2)
28: serve_sandwich(sandw4,child5,tray2,table2)
29: move_tray(tray2,table2,kitchen)
30: SHOP_methodm1_serve_1_precondition(table1,child6,bread7,sandw14,content9)
31: make_sandwich(sandw14,bread7,content9)
32: put_on_tray(sandw14,tray3,kitchen)
33: move_tray(tray3,kitchen,table1)
34: serve_sandwich(sandw14,child6,tray3,table1)
35: move_tray(tray3,table1,kitchen)
36: SHOP_methodm1_serve_1_precondition(table3,child7,bread10,sandw6,content12)
37: make_sandwich(sandw6,bread10,content12)
38: put_on_tray(sandw6,tray2,kitchen)
39: move_tray(tray2,kitchen,table3)
40: serve_sandwich(sandw6,child7,tray2,table3)
41: move_tray(tray2,table3,kitchen)
42: SHOP_methodm0_serve_0_precondition(content4,table1,child8,sandw3,bread12)
43: make_sandwich_no_gluten(sandw3,bread12,content4)
44: put_on_tray(sandw3,tray2,kitchen)
45: move_tray(tray2,kitchen,table1)
46: serve_sandwich_no_gluten(sandw3,child8,tray2,table1)
47: move_tray(tray2,table1,kitchen)
48: SHOP_methodm1_serve_1_precondition(table1,child9,bread4,sandw15,content3)
49: make_sandwich(sandw15,bread4,content3)
50: put_on_tray(sandw15,tray1,kitchen)
51: move_tray(tray1,kitchen,table1)
52: serve_sandwich(sandw15,child9,tray1,table1)
53: move_tray(tray1,table1,kitchen)
54: SHOP_methodm1_serve_1_precondition(table3,child10,bread13,sandw8,content1)
55: make_sandwich(sandw8,bread13,content1)
56: put_on_tray(sandw8,tray3,kitchen)
57: move_tray(tray3,kitchen,table3)
58: serve_sandwich(sandw8,child10,tray3,table3)
59: move_tray(tray3,table3,kitchen)
60: SHOP_methodm1_serve_1_precondition(table1,child11,bread2,sandw9,content8)
61: make_sandwich(sandw9,bread2,content8)
62: put_on_tray(sandw9,tray1,kitchen)
63: move_tray(tray1,kitchen,table1)
64: serve_sandwich(sandw9,child11,tray1,table1)
65: move_tray(tray1,table1,kitchen)
66: SHOP_methodm0_serve_0_precondition(content10,table1,child12,sandw16,bread1)
67: make_sandwich_no_gluten(sandw16,bread1,content10)
68: put_on_tray(sandw16,tray3,kitchen)
69: move_tray(tray3,kitchen,table1)
70: serve_sandwich_no_gluten(sandw16,child12,tray3,table1)
71: move_tray(tray3,table1,kitchen)
72: SHOP_methodm0_serve_0_precondition(content2,table1,child13,sandw1,bread5)
73: make_sandwich_no_gluten(sandw1,bread5,content2)
74: put_on_tray(sandw1,tray1,kitchen)
75: move_tray(tray1,kitchen,table1)
76: serve_sandwich_no_gluten(sandw1,child13,tray1,table1)
77: move_tray(tray1,table1,kitchen)
