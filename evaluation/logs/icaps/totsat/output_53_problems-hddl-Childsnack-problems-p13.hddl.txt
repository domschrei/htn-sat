PANDA - Planning and Acting in a Network Decomposition Architecture
Believe us: It's great, it's fantastic!

PANDA Copyright (C) 2014-2018 Gregor Behnke, Pascal Bercher, Thomas Geier, Kadir
Dede, Daniel Höller, Kristof Mickeleit, Matthias Englert
This program comes with ABSOLUTELY NO WARRANTY
This is free software, and you are welcome to redistribute it under certain
conditions; run PANDA with -license for details.

Main Developers:
- Gregor Behnke, http://www.uni-ulm.de/in/ki/behnke
- Daniel Höller, http://www.uni-ulm.de/in/ki/hoeller

With many thanks to various further contributors.
Run PANDA with the command line argument -contributors for an extensive list.

Run it with -help for more information like available options.


PANDA was called with: "-systemConfig AAAI-2018-totSAT(minisat) -programPath minisat=./minisat problems/hddl/Childsnack/domains/domain.hddl problems/hddl/Childsnack/problems/p13.hddl"


Planner Configuration
=====================
Domain: problems/hddl/Childsnack/domains/domain.hddl
Problem: problems/hddl/Childsnack/problems/p13.hddl
Output: none

Planning Configuration
======================
	printGeneralInformation : true
	printAdditionalData     : true
	random seed             : 42
	time limit (in seconds) : none

	external programs:
		minisat : ./minisat

	Parsing Configuration
	---------------------
	Parser                : autodetect file-type
	Expand Sort Hierarchy : true
	ClosedWordAssumption  : true
	CompileSHOPMethods    : true
	Eliminate Equality    : true
	Strip Hybridity       : true
	Reduce General Tasks  : true
	
	Preprocessing Configuration
	---------------------------
	Compile negative preconditions    : true
	Compile unit methods              : false
	Compile order in methods          : false
	Compile initial plan              : true
	Ensure Methods Have Last Task     : false
	Split independent parameters      : true
	Remove unnecessary predicates     : true
	Expand choiceless abstract tasks  : true
	Domain Cleanup                    : true
	Convert to SAS+                   : false
	Grounded Reachability Analysis    : Planning Graph (mutex-free)
	Grounded Task Decomposition Graph : Two Way TDG
	Iterate reachability analysis     : true
	Ground domain                     : true
	Iterate reachability analysis     : true
	Stop directly after grounding     : false
	
	SAT-Planning Configuration
	--------------------------
	solver           : minisat
	full planner run : true
	reduction method : only normalise 
	check result     : true
	
	Post-processing Configuration
	-----------------------------
	search status
	search result
	timings
	statistics
#0 "00 global:01:problem"="p13.hddl";"00 global:00:domain"="domain.hddl"
Parsing domain ... using HDDL parser ... done
Preparing internal domain representation ... done.
Initial domain
	number of abstract tasks = 1
	number of tasks = 10
	number of decomposition methods = 2
	number of predicates = 13
	number of sorts = 7
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 9
	number of constants = 82
Compiling negative preconditions ... done.
	number of abstract tasks = 1
	number of tasks = 10
	number of decomposition methods = 2
	number of predicates = 26
	number of sorts = 7
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 9
	number of constants = 82
Compiling split parameters ... done.
	number of abstract tasks = 1
	number of tasks = 10
	number of decomposition methods = 2
	number of predicates = 26
	number of sorts = 7
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 9
	number of constants = 82
Lifted reachability analysis and domain cleanup ... done.
	number of abstract tasks = 1
	number of tasks = 9
	number of decomposition methods = 2
	number of predicates = 15
	number of sorts = 7
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 8
	number of constants = 82
Grounded planning graph ... done.
	Number of Grounded Actions 44740
	Number of Grounded Literals 484
	number of abstract tasks = 1
	number of tasks = 9
	number of decomposition methods = 2
	number of predicates = 15
	number of sorts = 7
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 8
	number of constants = 82
Two Way TDG ... done.
	number of abstract tasks = 1
	number of tasks = 9
	number of decomposition methods = 2
	number of predicates = 15
	number of sorts = 7
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 8
	number of constants = 82
Grounding ... done.
	number of abstract tasks = 17
	number of tasks = 40889
	number of decomposition methods = 142324
	number of predicates = 330
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 40872
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 17
	number of tasks = 40889
	number of decomposition methods = 142324
	number of predicates = 228
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 40872
	number of constants = 0
Omitting lifted reachability analysis ... 
Grounded planning graph ... done.
	Number of Grounded Actions 40872
	Number of Grounded Literals 416
	number of abstract tasks = 17
	number of tasks = 40889
	number of decomposition methods = 142324
	number of predicates = 228
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 40872
	number of constants = 0
Two Way TDG ... done.
	number of abstract tasks = 17
	number of tasks = 40889
	number of decomposition methods = 142324
	number of predicates = 228
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 40872
	number of constants = 0
Compiling expand choiceless abstract tasks ... done.
	number of abstract tasks = 17
	number of tasks = 40889
	number of decomposition methods = 142324
	number of predicates = 228
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 40872
	number of constants = 0
Compiling abstract tasks without methods ... done.
	number of abstract tasks = 17
	number of tasks = 40889
	number of decomposition methods = 142324
	number of predicates = 228
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 40872
	number of constants = 0
Compiling initial plan ... done.
	number of abstract tasks = 18
	number of tasks = 40890
	number of decomposition methods = 142325
	number of predicates = 228
	number of sorts = 0
	number of tasks in largest method = 17
	number of epsilon methods = 0
	number of primitive tasks = 40872
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 18
	number of tasks = 40890
	number of decomposition methods = 142325
	number of predicates = 228
	number of sorts = 0
	number of tasks in largest method = 17
	number of epsilon methods = 0
	number of primitive tasks = 40872
	number of constants = 0
Omitting lifted reachability analysis ... 
Grounded planning graph ... done.
	Number of Grounded Actions 40872
	Number of Grounded Literals 416
	number of abstract tasks = 18
	number of tasks = 40890
	number of decomposition methods = 142325
	number of predicates = 228
	number of sorts = 0
	number of tasks in largest method = 17
	number of epsilon methods = 0
	number of primitive tasks = 40872
	number of constants = 0
Two Way TDG ... done.
	number of abstract tasks = 18
	number of tasks = 40890
	number of decomposition methods = 142325
	number of predicates = 228
	number of sorts = 0
	number of tasks in largest method = 17
	number of epsilon methods = 0
	number of primitive tasks = 40872
	number of constants = 0
Compiling expand choiceless abstract tasks ... done.
	number of abstract tasks = 17
	number of tasks = 40889
	number of decomposition methods = 142324
	number of predicates = 228
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 40872
	number of constants = 0
Compiling abstract tasks without methods ... done.
	number of abstract tasks = 17
	number of tasks = 40889
	number of decomposition methods = 142324
	number of predicates = 228
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 40872
	number of constants = 0
Compiling initial plan ... done.
	number of abstract tasks = 18
	number of tasks = 40890
	number of decomposition methods = 142325
	number of predicates = 228
	number of sorts = 0
	number of tasks in largest method = 17
	number of epsilon methods = 0
	number of primitive tasks = 40872
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 18
	number of tasks = 40890
	number of decomposition methods = 142325
	number of predicates = 228
	number of sorts = 0
	number of tasks in largest method = 17
	number of epsilon methods = 0
	number of primitive tasks = 40872
	number of constants = 0
Tasks 40872 - 0
Domain is acyclic: true
Domain is mostly acyclic: true
Domain is regular: false
Domain is tail recursive: true
Domain is totally ordered: true
Domain has last task in all methods: true
Time remaining for planner 9223372036854647801ms

Running SAT search with K = 0
Time remaining for SAT search 9223372036854647786ms
Time used for this run 9223372036854647786ms


Still waiting ... running for 102 will abort at 9223372036854647786
Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 246
NUMBER OF STATE CLAUSES 245
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 99.59% 0.41000000000000003% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 0c19f8e0-4def-488c-b666-ce2a1b82d067
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
ERROR false

Running SAT search with K = 1
Time remaining for SAT search 9223372036854647180ms
Time used for this run 9223372036854647180ms


Still waiting ... running for 100 will abort at 9223372036854647180
Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 246
NUMBER OF STATE CLAUSES 245
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 99.59% 0.41000000000000003% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 77fd5ad8-de41-4175-9e7f-e2904bc5db96
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
ERROR false

Running SAT search with K = 2
Time remaining for SAT search 9223372036854646469ms
Time used for this run 9223372036854646469ms


Still waiting ... running for 101 will abort at 9223372036854646469
Generating initial PDT ... done
Checking whether the PDT can grow any more ... no ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... 