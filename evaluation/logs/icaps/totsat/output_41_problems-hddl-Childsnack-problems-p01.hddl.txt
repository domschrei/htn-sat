PANDA - Planning and Acting in a Network Decomposition Architecture
Believe us: It's great, it's fantastic!

PANDA Copyright (C) 2014-2018 Gregor Behnke, Pascal Bercher, Thomas Geier, Kadir
Dede, Daniel Höller, Kristof Mickeleit, Matthias Englert
This program comes with ABSOLUTELY NO WARRANTY
This is free software, and you are welcome to redistribute it under certain
conditions; run PANDA with -license for details.

Main Developers:
- Gregor Behnke, http://www.uni-ulm.de/in/ki/behnke
- Daniel Höller, http://www.uni-ulm.de/in/ki/hoeller

With many thanks to various further contributors.
Run PANDA with the command line argument -contributors for an extensive list.

Run it with -help for more information like available options.


PANDA was called with: "-systemConfig AAAI-2018-totSAT(minisat) -programPath minisat=./minisat problems/hddl/Childsnack/domains/domain.hddl problems/hddl/Childsnack/problems/p01.hddl"


Planner Configuration
=====================
Domain: problems/hddl/Childsnack/domains/domain.hddl
Problem: problems/hddl/Childsnack/problems/p01.hddl
Output: none

Planning Configuration
======================
	printGeneralInformation : true
	printAdditionalData     : true
	random seed             : 42
	time limit (in seconds) : none

	external programs:
		minisat : ./minisat

	Parsing Configuration
	---------------------
	Parser                : autodetect file-type
	Expand Sort Hierarchy : true
	ClosedWordAssumption  : true
	CompileSHOPMethods    : true
	Eliminate Equality    : true
	Strip Hybridity       : true
	Reduce General Tasks  : true
	
	Preprocessing Configuration
	---------------------------
	Compile negative preconditions    : true
	Compile unit methods              : false
	Compile order in methods          : false
	Compile initial plan              : true
	Ensure Methods Have Last Task     : false
	Split independent parameters      : true
	Remove unnecessary predicates     : true
	Expand choiceless abstract tasks  : true
	Domain Cleanup                    : true
	Convert to SAS+                   : false
	Grounded Reachability Analysis    : Planning Graph (mutex-free)
	Grounded Task Decomposition Graph : Two Way TDG
	Iterate reachability analysis     : true
	Ground domain                     : true
	Iterate reachability analysis     : true
	Stop directly after grounding     : false
	
	SAT-Planning Configuration
	--------------------------
	solver           : minisat
	full planner run : true
	reduction method : only normalise 
	check result     : true
	
	Post-processing Configuration
	-----------------------------
	search status
	search result
	timings
	statistics
#0 "00 global:01:problem"="p01.hddl";"00 global:00:domain"="domain.hddl"
Parsing domain ... using HDDL parser ... done
Preparing internal domain representation ... done.
Initial domain
	number of abstract tasks = 1
	number of tasks = 10
	number of decomposition methods = 2
	number of predicates = 13
	number of sorts = 7
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 9
	number of constants = 50
Compiling negative preconditions ... done.
	number of abstract tasks = 1
	number of tasks = 10
	number of decomposition methods = 2
	number of predicates = 26
	number of sorts = 7
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 9
	number of constants = 50
Compiling split parameters ... done.
	number of abstract tasks = 1
	number of tasks = 10
	number of decomposition methods = 2
	number of predicates = 26
	number of sorts = 7
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 9
	number of constants = 50
Lifted reachability analysis and domain cleanup ... done.
	number of abstract tasks = 1
	number of tasks = 9
	number of decomposition methods = 2
	number of predicates = 15
	number of sorts = 7
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 8
	number of constants = 50
Grounded planning graph ... done.
	Number of Grounded Actions 5598
	Number of Grounded Literals 257
	number of abstract tasks = 1
	number of tasks = 9
	number of decomposition methods = 2
	number of predicates = 15
	number of sorts = 7
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 8
	number of constants = 50
Two Way TDG ... done.
	number of abstract tasks = 1
	number of tasks = 9
	number of decomposition methods = 2
	number of predicates = 15
	number of sorts = 7
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 8
	number of constants = 50
Grounding ... done.
	number of abstract tasks = 10
	number of tasks = 4773
	number of decomposition methods = 10920
	number of predicates = 180
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 4763
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 10
	number of tasks = 4773
	number of decomposition methods = 10920
	number of predicates = 120
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 4763
	number of constants = 0
Omitting lifted reachability analysis ... 
Grounded planning graph ... done.
	Number of Grounded Actions 4763
	Number of Grounded Literals 217
	number of abstract tasks = 10
	number of tasks = 4773
	number of decomposition methods = 10920
	number of predicates = 120
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 4763
	number of constants = 0
Two Way TDG ... done.
	number of abstract tasks = 10
	number of tasks = 4773
	number of decomposition methods = 10920
	number of predicates = 120
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 4763
	number of constants = 0
Compiling expand choiceless abstract tasks ... done.
	number of abstract tasks = 10
	number of tasks = 4773
	number of decomposition methods = 10920
	number of predicates = 120
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 4763
	number of constants = 0
Compiling abstract tasks without methods ... done.
	number of abstract tasks = 10
	number of tasks = 4773
	number of decomposition methods = 10920
	number of predicates = 120
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 4763
	number of constants = 0
Compiling initial plan ... done.
	number of abstract tasks = 11
	number of tasks = 4774
	number of decomposition methods = 10921
	number of predicates = 120
	number of sorts = 0
	number of tasks in largest method = 10
	number of epsilon methods = 0
	number of primitive tasks = 4763
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 11
	number of tasks = 4774
	number of decomposition methods = 10921
	number of predicates = 120
	number of sorts = 0
	number of tasks in largest method = 10
	number of epsilon methods = 0
	number of primitive tasks = 4763
	number of constants = 0
Omitting lifted reachability analysis ... 
Grounded planning graph ... done.
	Number of Grounded Actions 4763
	Number of Grounded Literals 217
	number of abstract tasks = 11
	number of tasks = 4774
	number of decomposition methods = 10921
	number of predicates = 120
	number of sorts = 0
	number of tasks in largest method = 10
	number of epsilon methods = 0
	number of primitive tasks = 4763
	number of constants = 0
Two Way TDG ... done.
	number of abstract tasks = 11
	number of tasks = 4774
	number of decomposition methods = 10921
	number of predicates = 120
	number of sorts = 0
	number of tasks in largest method = 10
	number of epsilon methods = 0
	number of primitive tasks = 4763
	number of constants = 0
Compiling expand choiceless abstract tasks ... done.
	number of abstract tasks = 10
	number of tasks = 4773
	number of decomposition methods = 10920
	number of predicates = 120
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 4763
	number of constants = 0
Compiling abstract tasks without methods ... done.
	number of abstract tasks = 10
	number of tasks = 4773
	number of decomposition methods = 10920
	number of predicates = 120
	number of sorts = 0
	number of tasks in largest method = 6
	number of epsilon methods = 0
	number of primitive tasks = 4763
	number of constants = 0
Compiling initial plan ... done.
	number of abstract tasks = 11
	number of tasks = 4774
	number of decomposition methods = 10921
	number of predicates = 120
	number of sorts = 0
	number of tasks in largest method = 10
	number of epsilon methods = 0
	number of primitive tasks = 4763
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 11
	number of tasks = 4774
	number of decomposition methods = 10921
	number of predicates = 120
	number of sorts = 0
	number of tasks in largest method = 10
	number of epsilon methods = 0
	number of primitive tasks = 4763
	number of constants = 0
Tasks 4763 - 0
Domain is acyclic: true
Domain is mostly acyclic: true
Domain is regular: false
Domain is tail recursive: true
Domain is totally ordered: true
Domain has last task in all methods: true
Time remaining for planner 9223372036854763274ms

Running SAT search with K = 0
Time remaining for SAT search 9223372036854763260ms
Time used for this run 9223372036854763260ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 131
NUMBER OF STATE CLAUSES 130
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 99.24000000000001% 0.76% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 8a9abca0-a8dd-4aad-b035-5d03353808b7
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
Still waiting ... running for 102 will abort at 9223372036854763260
ERROR false

Running SAT search with K = 1
Time remaining for SAT search 9223372036854763155ms
Time used for this run 9223372036854763155ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 131
NUMBER OF STATE CLAUSES 130
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 99.24000000000001% 0.76% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID cf61d77f-0d8d-41a2-a571-63d64ba86d39
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
Still waiting ... running for 100 will abort at 9223372036854763155
ERROR false

Running SAT search with K = 2
Time remaining for SAT search 9223372036854763054ms
Time used for this run 9223372036854763054ms


Generating initial PDT ... Still waiting ... running for 101 will abort at 9223372036854763054
done
Checking whether the PDT can grow any more ... no ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 329742
NUMBER OF STATE CLAUSES 47938
NUMBER OF DECOMPOSITION CLAUSES 281804
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 14.540000000000001% 85.46000000000001% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 203f53fe-9620-4dbf-8de8-c28bbf44527c
FLUSH
CLOSE
NUMBER OF PATHS 60
Starting minisat
Setting starttime of solver to 1542618524659
Command exited with non-zero status 10
0.07 0.00

Time command gave the following runtime for the solver: 70
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... done
SAT-Solver says: SAT

extracting solution


CHECKING primitive solution of length 60 ...
true 69 SHOP_methodm0_serve_0_precondition[table2,bread4,content4,child1,sandw1]
true 23 make_sandwich_no_gluten[sandw1,bread4,content4]
true 12 put_on_tray[sandw1,tray3,kitchen]
true 55 move_tray[tray3,kitchen,table2]
true 64 serve_sandwich_no_gluten[sandw1,child1,tray3,table2]
true 59 move_tray[tray3,table2,kitchen]
true 6 SHOP_methodm1_serve_1_precondition[table1,bread6,content3,sandw8,child2]
true 50 make_sandwich[sandw8,bread6,content3]
true 40 put_on_tray[sandw8,tray2,kitchen]
true 35 move_tray[tray2,kitchen,table1]
true 63 serve_sandwich[sandw8,child2,tray2,table1]
true 14 move_tray[tray2,table1,kitchen]
true 7 SHOP_methodm0_serve_0_precondition[table1,bread2,content8,child3,sandw11]
true 29 make_sandwich_no_gluten[sandw11,bread2,content8]
true 26 put_on_tray[sandw11,tray3,kitchen]
true 2 move_tray[tray3,kitchen,table1]
true 57 serve_sandwich_no_gluten[sandw11,child3,tray3,table1]
true 66 move_tray[tray3,table1,kitchen]
true 47 SHOP_methodm0_serve_0_precondition[table2,bread9,content2,child4,sandw3]
true 36 make_sandwich_no_gluten[sandw3,bread9,content2]
true 4 put_on_tray[sandw3,tray2,kitchen]
true 32 move_tray[tray2,kitchen,table2]
true 39 serve_sandwich_no_gluten[sandw3,child4,tray2,table2]
true 46 move_tray[tray2,table2,kitchen]
true 31 SHOP_methodm1_serve_1_precondition[table3,bread5,content7,sandw7,child5]
true 22 make_sandwich[sandw7,bread5,content7]
true 44 put_on_tray[sandw7,tray3,kitchen]
true 34 move_tray[tray3,kitchen,table3]
true 21 serve_sandwich[sandw7,child5,tray3,table3]
true 52 move_tray[tray3,table3,kitchen]
true 13 SHOP_methodm1_serve_1_precondition[table3,bread3,content6,sandw2,child6]
true 60 make_sandwich[sandw2,bread3,content6]
true 70 put_on_tray[sandw2,tray3,kitchen]
true 18 move_tray[tray3,kitchen,table3]
true 24 serve_sandwich[sandw2,child6,tray3,table3]
true 10 move_tray[tray3,table3,kitchen]
true 1 SHOP_methodm1_serve_1_precondition[table3,bread1,content9,sandw12,child7]
true 61 make_sandwich[sandw12,bread1,content9]
true 49 put_on_tray[sandw12,tray1,kitchen]
true 5 move_tray[tray1,kitchen,table3]
true 33 serve_sandwich[sandw12,child7,tray1,table3]
true 19 move_tray[tray1,table3,kitchen]
true 51 SHOP_methodm1_serve_1_precondition[table2,bread10,content5,sandw13,child8]
true 20 make_sandwich[sandw13,bread10,content5]
true 54 put_on_tray[sandw13,tray3,kitchen]
true 0 move_tray[tray3,kitchen,table2]
true 30 serve_sandwich[sandw13,child8,tray3,table2]
true 62 move_tray[tray3,table2,kitchen]
true 11 SHOP_methodm1_serve_1_precondition[table1,bread7,content10,sandw4,child9]
true 15 make_sandwich[sandw4,bread7,content10]
true 45 put_on_tray[sandw4,tray1,kitchen]
true 17 move_tray[tray1,kitchen,table1]
true 68 serve_sandwich[sandw4,child9,tray1,table1]
true 38 move_tray[tray1,table1,kitchen]
true 25 SHOP_methodm0_serve_0_precondition[table3,bread8,content1,child10,sandw6]
true 9 make_sandwich_no_gluten[sandw6,bread8,content1]
true 43 put_on_tray[sandw6,tray1,kitchen]
true 42 move_tray[tray1,kitchen,table3]
true 27 serve_sandwich_no_gluten[sandw6,child10,tray1,table3]
true 28 move_tray[tray1,table3,kitchen]
 done.
ERROR false
Panda says: SOLUTION
============ global ============
randomseed     = 42
peak memory    = 1813773824
planner result = SOLUTION
============ properties ============
acyclic                  = true
mostly acyclic           = true
regular                  = false
tail recursive           = true
totally ordered          = true
last task in all methods = true
============ problem ============
number of constants         = 0
number of predicates        = 120
number of actions           = 4774
number of abstract actions  = 11
number of primitive actions = 4763
number of methods           = 10921
============ sat ============
plan length                     = -1
number of variables             = 26810
number of clauses               = 329742
average size of clauses         = 2.084463004409508
number of assert                = 47
number of horn                  = 328276
K offset                        = 0
K chosen value                  = 2
state formula                   = 47938
method children clauses         = 0
number of paths                 = 60
maximum plan length             = 60
number of decomposition clauses = 281804
number of ordering clauses      = 0
number of state clauses         = 47938
solved                          = true
timeout                         = false

----------------- TIMINGS -----------------
============ total ============
total = 15096
============ parsing ============
total                         = 673
file parser                   = 469
sort expansion                = 83
closed world assumption       = 54
shop methods                  = 6
eliminate identical variables = 16
strip domain of hybridity     = 26
flatten formula               = 17
============ preprocessing ============
total                                      = 10820
compile negative preconditions             = 16
compile unit methods                       = 0
split parameter                            = 17
expand choiceless abstract tasks           = 170
expand choiceless abstract tasks           = 131
compile methods with identical tasks       = 0
removing unnecessary predicates            = 2313
lifted reachabiltiy analysis               = 30
grounded planning graph analysis           = 1937
grounded task decomposition graph analysis = 5621
grounding                                  = 545
create artificial top task                 = 16
============ sat ============
total                                        = 1835
generate formula                             = 1477
generate path decomposition tree             = 611
normalise path decomposition tree            = 192
translate path decomposition tree to clauses = 386
transform to DIMACS                          = 197
SAT solver                                   = 70
SAT solver for K=0002                        = 70

#1 "40 sat:90:solved"="true";"30 problem:05:number of primitive actions"="4763";"30 problem:01:number of constants"="0";"30 problem:04:number of abstract actions"="11";"02 properties:04:tail recursive"="true";"00 global:80:peak memory"="1813773824";"40 sat:20:state formula"="47938";"40 sat:01:number of variables"="26810";"40 sat:14:K offset"="0";"40 sat:30:number of paths"="60";"40 sat:00:plan length"="-1";"40 sat:50:number of ordering clauses"="0";"02 properties:02:mostly acyclic"="true";"30 problem:06:number of methods"="10921";"02 properties:05:totally ordered"="true";"02 properties:06:last task in all methods"="true";"30 problem:03:number of actions"="4774";"30 problem:02:number of predicates"="120";"40 sat:03:number of horn"="328276";"40 sat:15:K chosen value"="2";"02 properties:03:regular"="false";"40 sat:03:average size of clauses"="2.084463004409508";"40 sat:02:number of clauses"="329742";"40 sat:50:number of state clauses"="47938";"40 sat:03:number of assert"="47";"40 sat:22:method children clauses"="0";"00 global:90:planner result"="SOLUTION";"02 properties:01:acyclic"="true";"40 sat:31:maximum plan length"="60";"40 sat:50:number of decomposition clauses"="281804";"00 global:02:randomseed"="42";"40 sat:91:timeout"="false";"01 parsing:01:file parser"="469";"40 sat:00:total"="1835";"40 sat:20:transform to DIMACS"="197";"40 sat:11:generate path decomposition tree"="611";"02 preprocessing:07:compile methods with identical tasks"="0";"01 parsing:04:shop methods"="6";"02 preprocessing:08:removing unnecessary predicates"="2313";"01 parsing:03:closed world assumption"="54";"02 preprocessing:11:lifted reachabiltiy analysis"="30";"01 parsing:02:sort expansion"="83";"40 sat:12:normalise path decomposition tree"="192";"40 sat:40:SAT solver"="70";"01 parsing:00:total"="673";"02 preprocessing:06:expand choiceless abstract tasks"="131";"01 parsing:06:strip domain of hybridity"="26";"40 sat:13:translate path decomposition tree to clauses"="386";"40 sat:10:generate formula"="1477";"40 sat:41:SAT solver for K=0002"="70";"02 preprocessing:01:compile negative preconditions"="16";"00 total:00:total"="15096";"02 preprocessing:12:grounded planning graph analysis"="1937";"02 preprocessing:02:compile unit methods"="0";"02 preprocessing:23:grounded task decomposition graph analysis"="5621";"02 preprocessing:04:split parameter"="17";"01 parsing:07:flatten formula"="17";"02 preprocessing:05:expand choiceless abstract tasks"="170";"02 preprocessing:00:total"="10820";"02 preprocessing:99:create artificial top task"="16";"01 parsing:05:eliminate identical variables"="16";"02 preprocessing:84:grounding"="545"
SOLUTION SEQUENCE
0: SHOP_methodm0_serve_0_precondition(table2,bread4,content4,child1,sandw1)
1: make_sandwich_no_gluten(sandw1,bread4,content4)
2: put_on_tray(sandw1,tray3,kitchen)
3: move_tray(tray3,kitchen,table2)
4: serve_sandwich_no_gluten(sandw1,child1,tray3,table2)
5: move_tray(tray3,table2,kitchen)
6: SHOP_methodm1_serve_1_precondition(table1,bread6,content3,sandw8,child2)
7: make_sandwich(sandw8,bread6,content3)
8: put_on_tray(sandw8,tray2,kitchen)
9: move_tray(tray2,kitchen,table1)
10: serve_sandwich(sandw8,child2,tray2,table1)
11: move_tray(tray2,table1,kitchen)
12: SHOP_methodm0_serve_0_precondition(table1,bread2,content8,child3,sandw11)
13: make_sandwich_no_gluten(sandw11,bread2,content8)
14: put_on_tray(sandw11,tray3,kitchen)
15: move_tray(tray3,kitchen,table1)
16: serve_sandwich_no_gluten(sandw11,child3,tray3,table1)
17: move_tray(tray3,table1,kitchen)
18: SHOP_methodm0_serve_0_precondition(table2,bread9,content2,child4,sandw3)
19: make_sandwich_no_gluten(sandw3,bread9,content2)
20: put_on_tray(sandw3,tray2,kitchen)
21: move_tray(tray2,kitchen,table2)
22: serve_sandwich_no_gluten(sandw3,child4,tray2,table2)
23: move_tray(tray2,table2,kitchen)
24: SHOP_methodm1_serve_1_precondition(table3,bread5,content7,sandw7,child5)
25: make_sandwich(sandw7,bread5,content7)
26: put_on_tray(sandw7,tray3,kitchen)
27: move_tray(tray3,kitchen,table3)
28: serve_sandwich(sandw7,child5,tray3,table3)
29: move_tray(tray3,table3,kitchen)
30: SHOP_methodm1_serve_1_precondition(table3,bread3,content6,sandw2,child6)
31: make_sandwich(sandw2,bread3,content6)
32: put_on_tray(sandw2,tray3,kitchen)
33: move_tray(tray3,kitchen,table3)
34: serve_sandwich(sandw2,child6,tray3,table3)
35: move_tray(tray3,table3,kitchen)
36: SHOP_methodm1_serve_1_precondition(table3,bread1,content9,sandw12,child7)
37: make_sandwich(sandw12,bread1,content9)
38: put_on_tray(sandw12,tray1,kitchen)
39: move_tray(tray1,kitchen,table3)
40: serve_sandwich(sandw12,child7,tray1,table3)
41: move_tray(tray1,table3,kitchen)
42: SHOP_methodm1_serve_1_precondition(table2,bread10,content5,sandw13,child8)
43: make_sandwich(sandw13,bread10,content5)
44: put_on_tray(sandw13,tray3,kitchen)
45: move_tray(tray3,kitchen,table2)
46: serve_sandwich(sandw13,child8,tray3,table2)
47: move_tray(tray3,table2,kitchen)
48: SHOP_methodm1_serve_1_precondition(table1,bread7,content10,sandw4,child9)
49: make_sandwich(sandw4,bread7,content10)
50: put_on_tray(sandw4,tray1,kitchen)
51: move_tray(tray1,kitchen,table1)
52: serve_sandwich(sandw4,child9,tray1,table1)
53: move_tray(tray1,table1,kitchen)
54: SHOP_methodm0_serve_0_precondition(table3,bread8,content1,child10,sandw6)
55: make_sandwich_no_gluten(sandw6,bread8,content1)
56: put_on_tray(sandw6,tray1,kitchen)
57: move_tray(tray1,kitchen,table3)
58: serve_sandwich_no_gluten(sandw6,child10,tray1,table3)
59: move_tray(tray1,table3,kitchen)
