PANDA - Planning and Acting in a Network Decomposition Architecture
Believe us: It's great, it's fantastic!

PANDA Copyright (C) 2014-2018 Gregor Behnke, Pascal Bercher, Thomas Geier, Kadir
Dede, Daniel Höller, Kristof Mickeleit, Matthias Englert
This program comes with ABSOLUTELY NO WARRANTY
This is free software, and you are welcome to redistribute it under certain
conditions; run PANDA with -license for details.

Main Developers:
- Gregor Behnke, http://www.uni-ulm.de/in/ki/behnke
- Daniel Höller, http://www.uni-ulm.de/in/ki/hoeller

With many thanks to various further contributors.
Run PANDA with the command line argument -contributors for an extensive list.

Run it with -help for more information like available options.


PANDA was called with: "-systemConfig AAAI-2018-totSAT(minisat) -programPath minisat=./minisat problems/hddl/Transport/domains/domain.hddl problems/hddl/Transport/problems/pfile11.hddl"


Planner Configuration
=====================
Domain: problems/hddl/Transport/domains/domain.hddl
Problem: problems/hddl/Transport/problems/pfile11.hddl
Output: none

Planning Configuration
======================
	printGeneralInformation : true
	printAdditionalData     : true
	random seed             : 42
	time limit (in seconds) : none

	external programs:
		minisat : ./minisat

	Parsing Configuration
	---------------------
	Parser                : autodetect file-type
	Expand Sort Hierarchy : true
	ClosedWordAssumption  : true
	CompileSHOPMethods    : true
	Eliminate Equality    : true
	Strip Hybridity       : true
	Reduce General Tasks  : true
	
	Preprocessing Configuration
	---------------------------
	Compile negative preconditions    : true
	Compile unit methods              : false
	Compile order in methods          : false
	Compile initial plan              : true
	Ensure Methods Have Last Task     : false
	Split independent parameters      : true
	Remove unnecessary predicates     : true
	Expand choiceless abstract tasks  : true
	Domain Cleanup                    : true
	Convert to SAS+                   : false
	Grounded Reachability Analysis    : Planning Graph (mutex-free)
	Grounded Task Decomposition Graph : Two Way TDG
	Iterate reachability analysis     : true
	Ground domain                     : true
	Iterate reachability analysis     : true
	Stop directly after grounding     : false
	
	SAT-Planning Configuration
	--------------------------
	solver           : minisat
	full planner run : true
	reduction method : only normalise 
	check result     : true
	
	Post-processing Configuration
	-----------------------------
	search status
	search result
	timings
	statistics
#0 "00 global:01:problem"="pfile11.hddl";"00 global:00:domain"="domain.hddl"
Parsing domain ... using HDDL parser ... done
Preparing internal domain representation ... done.
Initial domain
	number of abstract tasks = 4
	number of tasks = 8
	number of decomposition methods = 6
	number of predicates = 5
	number of sorts = 7
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 4
	number of constants = 13
Compiling negative preconditions ... done.
	number of abstract tasks = 4
	number of tasks = 8
	number of decomposition methods = 6
	number of predicates = 10
	number of sorts = 7
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 4
	number of constants = 13
Compiling split parameters ... done.
	number of abstract tasks = 4
	number of tasks = 8
	number of decomposition methods = 6
	number of predicates = 10
	number of sorts = 7
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 4
	number of constants = 13
Lifted reachability analysis and domain cleanup ... done.
	number of abstract tasks = 4
	number of tasks = 8
	number of decomposition methods = 6
	number of predicates = 5
	number of sorts = 7
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 4
	number of constants = 13
Grounded planning graph ... done.
	Number of Grounded Actions 70
	Number of Grounded Literals 69
	number of abstract tasks = 4
	number of tasks = 8
	number of decomposition methods = 6
	number of predicates = 5
	number of sorts = 7
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 4
	number of constants = 13
Two Way TDG ... done.
	number of abstract tasks = 4
	number of tasks = 8
	number of decomposition methods = 6
	number of predicates = 5
	number of sorts = 7
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 4
	number of constants = 13
Grounding ... done.
	number of abstract tasks = 36
	number of tasks = 106
	number of decomposition methods = 100
	number of predicates = 39
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 70
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 36
	number of tasks = 106
	number of decomposition methods = 100
	number of predicates = 30
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 70
	number of constants = 0
Omitting lifted reachability analysis ... 
Grounded planning graph ... done.
	Number of Grounded Actions 70
	Number of Grounded Literals 60
	number of abstract tasks = 36
	number of tasks = 106
	number of decomposition methods = 100
	number of predicates = 30
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 70
	number of constants = 0
Two Way TDG ... done.
	number of abstract tasks = 36
	number of tasks = 106
	number of decomposition methods = 100
	number of predicates = 30
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 70
	number of constants = 0
Compiling expand choiceless abstract tasks ... done.
	number of abstract tasks = 36
	number of tasks = 106
	number of decomposition methods = 100
	number of predicates = 30
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 70
	number of constants = 0
Compiling abstract tasks without methods ... done.
	number of abstract tasks = 36
	number of tasks = 106
	number of decomposition methods = 100
	number of predicates = 30
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 70
	number of constants = 0
Compiling initial plan ... done.
	number of abstract tasks = 37
	number of tasks = 107
	number of decomposition methods = 101
	number of predicates = 30
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 70
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 37
	number of tasks = 107
	number of decomposition methods = 101
	number of predicates = 30
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 70
	number of constants = 0
Omitting lifted reachability analysis ... 
Grounded planning graph ... done.
	Number of Grounded Actions 70
	Number of Grounded Literals 60
	number of abstract tasks = 37
	number of tasks = 107
	number of decomposition methods = 101
	number of predicates = 30
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 70
	number of constants = 0
Two Way TDG ... done.
	number of abstract tasks = 37
	number of tasks = 107
	number of decomposition methods = 101
	number of predicates = 30
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 70
	number of constants = 0
Compiling expand choiceless abstract tasks ... done.
	number of abstract tasks = 36
	number of tasks = 106
	number of decomposition methods = 100
	number of predicates = 30
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 70
	number of constants = 0
Compiling abstract tasks without methods ... done.
	number of abstract tasks = 36
	number of tasks = 106
	number of decomposition methods = 100
	number of predicates = 30
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 70
	number of constants = 0
Compiling initial plan ... done.
	number of abstract tasks = 37
	number of tasks = 107
	number of decomposition methods = 101
	number of predicates = 30
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 70
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 37
	number of tasks = 107
	number of decomposition methods = 101
	number of predicates = 30
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 70
	number of constants = 0
Tasks 70 - 0
Domain is acyclic: false
Domain is mostly acyclic: false
Domain is regular: false
Domain is tail recursive: false
Domain is totally ordered: true
Domain has last task in all methods: true
Time remaining for planner 9223372036854774684ms

Running SAT search with K = 0
Time remaining for SAT search 9223372036854774671ms
Time used for this run 9223372036854774671ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 31
NUMBER OF STATE CLAUSES 30
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 96.77% 3.23% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID ce4da042-09ca-497e-8faf-adf800ebe9b5
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
Still waiting ... running for 102 will abort at 9223372036854774671
ERROR false

Running SAT search with K = 1
Time remaining for SAT search 9223372036854774565ms
Time used for this run 9223372036854774565ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 31
NUMBER OF STATE CLAUSES 30
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 96.77% 3.23% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 710f4338-5502-41e5-a0ed-6076ba9571af
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
Still waiting ... running for 100 will abort at 9223372036854774565
ERROR false

Running SAT search with K = 2
Time remaining for SAT search 9223372036854774465ms
Time used for this run 9223372036854774465ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 31
NUMBER OF STATE CLAUSES 30
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 96.77% 3.23% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID c31388db-4b7d-440e-ad02-b8f6055da917
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
Still waiting ... running for 101 will abort at 9223372036854774465
ERROR false

Running SAT search with K = 3
Time remaining for SAT search 9223372036854774363ms
Time used for this run 9223372036854774363ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
Still waiting ... running for 100 will abort at 9223372036854774363
NUMBER OF CLAUSES 2894
NUMBER OF STATE CLAUSES 1462
NUMBER OF DECOMPOSITION CLAUSES 1432
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 50.52% 49.480000000000004% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID d728d3d9-fa75-48c2-b39e-3a54da613bf3
FLUSH
CLOSE
NUMBER OF PATHS 16
Starting minisat
Setting starttime of solver to 1539959310628
Command exited with non-zero status 20
0.00 0.00

Time command gave the following runtime for the solver: 0
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... done
SAT-Solver says: UNSAT
ERROR false

Running SAT search with K = 4
Time remaining for SAT search 9223372036854774140ms
Time used for this run 9223372036854774140ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 5584
NUMBER OF STATE CLAUSES 2172
NUMBER OF DECOMPOSITION CLAUSES 3412
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 38.9% 61.1% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 32e0f25a-394f-4133-aeb9-a21abaea097b
FLUSH
CLOSE
NUMBER OF PATHS 24
Starting minisat
Setting starttime of solver to 1539959310811
Command exited with non-zero status 20
0.00 0.00

Time command gave the following runtime for the solver: 0
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... done
SAT-Solver says: UNSAT
Still waiting ... running for 100 will abort at 9223372036854774140
ERROR false

Running SAT search with K = 5
Time remaining for SAT search 9223372036854774023ms
Time used for this run 9223372036854774023ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 9448
NUMBER OF STATE CLAUSES 2954
NUMBER OF DECOMPOSITION CLAUSES 6494
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 31.27% 68.73% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID c63605e4-334c-48e8-b0e3-e8a1e770ae3d
FLUSH
CLOSE
NUMBER OF PATHS 32
Starting minisat
Setting starttime of solver to 1539959310957
Still waiting ... running for 100 will abort at 9223372036854774023
Command exited with non-zero status 10
0.00 0.00

Time command gave the following runtime for the solver: 0
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... done
SAT-Solver says: SAT

extracting solution


CHECKING primitive solution of length 30 ...
true 6 drive[truck-0,city-loc-0,city-loc-1]
true 23 drive[truck-0,city-loc-1,city-loc-2]
true 56 drive[truck-0,city-loc-2,city-loc-2]
true 17 pick-up[truck-0,city-loc-2,package-0,capacity-1,capacity-2]
true 31 drive[truck-0,city-loc-2,city-loc-1]
true 44 drive[truck-0,city-loc-1,city-loc-2]
true 62 drive[truck-0,city-loc-2,city-loc-1]
true 39 drop[truck-0,city-loc-1,package-0,capacity-1,capacity-2]
true 58 drive[truck-1,city-loc-1,city-loc-0]
true 14 drive[truck-1,city-loc-0,city-loc-1]
true 59 drive[truck-1,city-loc-1,city-loc-2]
true 46 pick-up[truck-1,city-loc-2,package-1,capacity-1,capacity-2]
true 30 drive[truck-1,city-loc-2,city-loc-1]
true 0 drive[truck-1,city-loc-1,city-loc-0]
true 40 drive[truck-1,city-loc-0,city-loc-3]
true 37 drop[truck-1,city-loc-3,package-1,capacity-1,capacity-2]
true 36 drive[truck-0,city-loc-1,city-loc-0]
true 16 pick-up[truck-0,city-loc-0,package-2,capacity-1,capacity-2]
true 64 drive[truck-0,city-loc-0,city-loc-1]
true 20 drive[truck-0,city-loc-1,city-loc-0]
true 21 drive[truck-0,city-loc-0,city-loc-3]
true 15 drop[truck-0,city-loc-3,package-2,capacity-1,capacity-2]
true 47 drive[truck-0,city-loc-3,city-loc-0]
true 22 drive[truck-0,city-loc-0,city-loc-1]
true 9 drive[truck-0,city-loc-1,city-loc-0]
true 25 pick-up[truck-0,city-loc-0,package-3,capacity-1,capacity-2]
true 55 drive[truck-0,city-loc-0,city-loc-1]
true 2 drive[truck-0,city-loc-1,city-loc-2]
true 12 drive[truck-0,city-loc-2,city-loc-2]
true 18 drop[truck-0,city-loc-2,package-3,capacity-1,capacity-2]
 done.
ERROR false
Panda says: SOLUTION
============ global ============
randomseed     = 42
peak memory    = 251658240
planner result = SOLUTION
============ properties ============
acyclic                  = false
mostly acyclic           = false
regular                  = false
tail recursive           = false
totally ordered          = true
last task in all methods = true
============ problem ============
number of constants         = 0
number of predicates        = 30
number of actions           = 107
number of abstract actions  = 37
number of primitive actions = 70
number of methods           = 101
============ sat ============
plan length                     = -1
number of variables             = 2429
number of clauses               = 9448
average size of clauses         = 2.471316680779001
number of assert                = 9
number of horn                  = 8074
K offset                        = 0
K chosen value                  = 5
state formula                   = 2954
method children clauses         = 0
number of paths                 = 32
maximum plan length             = 32
number of decomposition clauses = 6494
number of ordering clauses      = 0
number of state clauses         = 2954
solved                          = true
timeout                         = false

----------------- TIMINGS -----------------
============ total ============
total = 2003
============ parsing ============
total                         = 548
file parser                   = 389
sort expansion                = 60
closed world assumption       = 39
shop methods                  = 2
eliminate identical variables = 15
strip domain of hybridity     = 26
flatten formula               = 17
============ preprocessing ============
total                                      = 534
compile negative preconditions             = 10
compile unit methods                       = 0
split parameter                            = 13
expand choiceless abstract tasks           = 18
expand choiceless abstract tasks           = 1
compile methods with identical tasks       = 0
removing unnecessary predicates            = 37
lifted reachabiltiy analysis               = 24
grounded planning graph analysis           = 170
grounded task decomposition graph analysis = 206
grounding                                  = 46
create artificial top task                 = 2
============ sat ============
total                                        = 385
generate formula                             = 321
generate path decomposition tree             = 85
normalise path decomposition tree            = 36
translate path decomposition tree to clauses = 110
transform to DIMACS                          = 27
SAT solver                                   = 0
SAT solver for K=0003                        = 0
SAT solver for K=0004                        = 0
SAT solver for K=0005                        = 0

#1 "40 sat:90:solved"="true";"30 problem:05:number of primitive actions"="70";"30 problem:01:number of constants"="0";"30 problem:04:number of abstract actions"="37";"02 properties:04:tail recursive"="false";"00 global:80:peak memory"="251658240";"40 sat:20:state formula"="2954";"40 sat:01:number of variables"="2429";"40 sat:14:K offset"="0";"40 sat:30:number of paths"="32";"40 sat:00:plan length"="-1";"40 sat:50:number of ordering clauses"="0";"02 properties:02:mostly acyclic"="false";"30 problem:06:number of methods"="101";"02 properties:05:totally ordered"="true";"02 properties:06:last task in all methods"="true";"30 problem:03:number of actions"="107";"30 problem:02:number of predicates"="30";"40 sat:03:number of horn"="8074";"40 sat:15:K chosen value"="5";"02 properties:03:regular"="false";"40 sat:03:average size of clauses"="2.471316680779001";"40 sat:02:number of clauses"="9448";"40 sat:50:number of state clauses"="2954";"40 sat:03:number of assert"="9";"40 sat:22:method children clauses"="0";"00 global:90:planner result"="SOLUTION";"02 properties:01:acyclic"="false";"40 sat:31:maximum plan length"="32";"40 sat:50:number of decomposition clauses"="6494";"00 global:02:randomseed"="42";"40 sat:91:timeout"="false";"01 parsing:01:file parser"="389";"40 sat:41:SAT solver for K=0004"="0";"40 sat:00:total"="385";"40 sat:20:transform to DIMACS"="27";"40 sat:11:generate path decomposition tree"="85";"02 preprocessing:07:compile methods with identical tasks"="0";"01 parsing:04:shop methods"="2";"02 preprocessing:08:removing unnecessary predicates"="37";"01 parsing:03:closed world assumption"="39";"02 preprocessing:11:lifted reachabiltiy analysis"="24";"01 parsing:02:sort expansion"="60";"40 sat:12:normalise path decomposition tree"="36";"40 sat:40:SAT solver"="0";"01 parsing:00:total"="548";"02 preprocessing:06:expand choiceless abstract tasks"="1";"01 parsing:06:strip domain of hybridity"="26";"40 sat:13:translate path decomposition tree to clauses"="110";"40 sat:10:generate formula"="321";"40 sat:41:SAT solver for K=0005"="0";"02 preprocessing:01:compile negative preconditions"="10";"00 total:00:total"="2003";"02 preprocessing:12:grounded planning graph analysis"="170";"02 preprocessing:02:compile unit methods"="0";"02 preprocessing:23:grounded task decomposition graph analysis"="206";"02 preprocessing:04:split parameter"="13";"01 parsing:07:flatten formula"="17";"02 preprocessing:05:expand choiceless abstract tasks"="18";"40 sat:41:SAT solver for K=0003"="0";"02 preprocessing:00:total"="534";"02 preprocessing:99:create artificial top task"="2";"01 parsing:05:eliminate identical variables"="15";"02 preprocessing:84:grounding"="46"
SOLUTION SEQUENCE
0: drive(truck-0,city-loc-0,city-loc-1)
1: drive(truck-0,city-loc-1,city-loc-2)
2: drive(truck-0,city-loc-2,city-loc-2)
3: pick-up(truck-0,city-loc-2,package-0,capacity-1,capacity-2)
4: drive(truck-0,city-loc-2,city-loc-1)
5: drive(truck-0,city-loc-1,city-loc-2)
6: drive(truck-0,city-loc-2,city-loc-1)
7: drop(truck-0,city-loc-1,package-0,capacity-1,capacity-2)
8: drive(truck-1,city-loc-1,city-loc-0)
9: drive(truck-1,city-loc-0,city-loc-1)
10: drive(truck-1,city-loc-1,city-loc-2)
11: pick-up(truck-1,city-loc-2,package-1,capacity-1,capacity-2)
12: drive(truck-1,city-loc-2,city-loc-1)
13: drive(truck-1,city-loc-1,city-loc-0)
14: drive(truck-1,city-loc-0,city-loc-3)
15: drop(truck-1,city-loc-3,package-1,capacity-1,capacity-2)
16: drive(truck-0,city-loc-1,city-loc-0)
17: pick-up(truck-0,city-loc-0,package-2,capacity-1,capacity-2)
18: drive(truck-0,city-loc-0,city-loc-1)
19: drive(truck-0,city-loc-1,city-loc-0)
20: drive(truck-0,city-loc-0,city-loc-3)
21: drop(truck-0,city-loc-3,package-2,capacity-1,capacity-2)
22: drive(truck-0,city-loc-3,city-loc-0)
23: drive(truck-0,city-loc-0,city-loc-1)
24: drive(truck-0,city-loc-1,city-loc-0)
25: pick-up(truck-0,city-loc-0,package-3,capacity-1,capacity-2)
26: drive(truck-0,city-loc-0,city-loc-1)
27: drive(truck-0,city-loc-1,city-loc-2)
28: drive(truck-0,city-loc-2,city-loc-2)
29: drop(truck-0,city-loc-2,package-3,capacity-1,capacity-2)
