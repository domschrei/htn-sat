PANDA - Planning and Acting in a Network Decomposition Architecture
Believe us: It's great, it's fantastic!

PANDA Copyright (C) 2014-2018 Gregor Behnke, Pascal Bercher, Thomas Geier, Kadir
Dede, Daniel Höller, Kristof Mickeleit, Matthias Englert
This program comes with ABSOLUTELY NO WARRANTY
This is free software, and you are welcome to redistribute it under certain
conditions; run PANDA with -license for details.

Main Developers:
- Gregor Behnke, http://www.uni-ulm.de/in/ki/behnke
- Daniel Höller, http://www.uni-ulm.de/in/ki/hoeller

With many thanks to various further contributors.
Run PANDA with the command line argument -contributors for an extensive list.

Run it with -help for more information like available options.


PANDA was called with: "-systemConfig AAAI-2018-totSAT(minisat) -programPath minisat=./minisat problems/hddl/Transport/domains/domain.hddl problems/hddl/Transport/problems/pfile19.hddl"


Planner Configuration
=====================
Domain: problems/hddl/Transport/domains/domain.hddl
Problem: problems/hddl/Transport/problems/pfile19.hddl
Output: none

Planning Configuration
======================
	printGeneralInformation : true
	printAdditionalData     : true
	random seed             : 42
	time limit (in seconds) : none

	external programs:
		minisat : ./minisat

	Parsing Configuration
	---------------------
	Parser                : autodetect file-type
	Expand Sort Hierarchy : true
	ClosedWordAssumption  : true
	CompileSHOPMethods    : true
	Eliminate Equality    : true
	Strip Hybridity       : true
	Reduce General Tasks  : true
	
	Preprocessing Configuration
	---------------------------
	Compile negative preconditions    : true
	Compile unit methods              : false
	Compile order in methods          : false
	Compile initial plan              : true
	Ensure Methods Have Last Task     : false
	Split independent parameters      : true
	Remove unnecessary predicates     : true
	Expand choiceless abstract tasks  : true
	Domain Cleanup                    : true
	Convert to SAS+                   : false
	Grounded Reachability Analysis    : Planning Graph (mutex-free)
	Grounded Task Decomposition Graph : Two Way TDG
	Iterate reachability analysis     : true
	Ground domain                     : true
	Iterate reachability analysis     : true
	Stop directly after grounding     : false
	
	SAT-Planning Configuration
	--------------------------
	solver           : minisat
	full planner run : true
	reduction method : only normalise 
	check result     : true
	
	Post-processing Configuration
	-----------------------------
	search status
	search result
	timings
	statistics
#0 "00 global:01:problem"="pfile19.hddl";"00 global:00:domain"="domain.hddl"
Parsing domain ... using HDDL parser ... done
Preparing internal domain representation ... done.
Initial domain
	number of abstract tasks = 4
	number of tasks = 8
	number of decomposition methods = 6
	number of predicates = 5
	number of sorts = 7
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 4
	number of constants = 29
Compiling negative preconditions ... done.
	number of abstract tasks = 4
	number of tasks = 8
	number of decomposition methods = 6
	number of predicates = 10
	number of sorts = 7
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 4
	number of constants = 29
Compiling split parameters ... done.
	number of abstract tasks = 4
	number of tasks = 8
	number of decomposition methods = 6
	number of predicates = 10
	number of sorts = 7
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 4
	number of constants = 29
Lifted reachability analysis and domain cleanup ... done.
	number of abstract tasks = 4
	number of tasks = 8
	number of decomposition methods = 6
	number of predicates = 5
	number of sorts = 7
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 4
	number of constants = 29
Grounded planning graph ... done.
	Number of Grounded Actions 278
	Number of Grounded Literals 183
	number of abstract tasks = 4
	number of tasks = 8
	number of decomposition methods = 6
	number of predicates = 5
	number of sorts = 7
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 4
	number of constants = 29
Two Way TDG ... done.
	number of abstract tasks = 4
	number of tasks = 8
	number of decomposition methods = 6
	number of predicates = 5
	number of sorts = 7
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 4
	number of constants = 29
Grounding ... done.
	number of abstract tasks = 101
	number of tasks = 379
	number of decomposition methods = 378
	number of predicates = 107
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 278
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 101
	number of tasks = 379
	number of decomposition methods = 378
	number of predicates = 76
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 278
	number of constants = 0
Omitting lifted reachability analysis ... 
Grounded planning graph ... done.
	Number of Grounded Actions 278
	Number of Grounded Literals 152
	number of abstract tasks = 101
	number of tasks = 379
	number of decomposition methods = 378
	number of predicates = 76
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 278
	number of constants = 0
Two Way TDG ... done.
	number of abstract tasks = 101
	number of tasks = 379
	number of decomposition methods = 378
	number of predicates = 76
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 278
	number of constants = 0
Compiling expand choiceless abstract tasks ... done.
	number of abstract tasks = 101
	number of tasks = 379
	number of decomposition methods = 378
	number of predicates = 76
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 278
	number of constants = 0
Compiling abstract tasks without methods ... done.
	number of abstract tasks = 101
	number of tasks = 379
	number of decomposition methods = 378
	number of predicates = 76
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 278
	number of constants = 0
Compiling initial plan ... done.
	number of abstract tasks = 102
	number of tasks = 380
	number of decomposition methods = 379
	number of predicates = 76
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 278
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 102
	number of tasks = 380
	number of decomposition methods = 379
	number of predicates = 76
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 278
	number of constants = 0
Omitting lifted reachability analysis ... 
Grounded planning graph ... done.
	Number of Grounded Actions 278
	Number of Grounded Literals 152
	number of abstract tasks = 102
	number of tasks = 380
	number of decomposition methods = 379
	number of predicates = 76
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 278
	number of constants = 0
Two Way TDG ... done.
	number of abstract tasks = 102
	number of tasks = 380
	number of decomposition methods = 379
	number of predicates = 76
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 278
	number of constants = 0
Compiling expand choiceless abstract tasks ... done.
	number of abstract tasks = 101
	number of tasks = 379
	number of decomposition methods = 378
	number of predicates = 76
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 278
	number of constants = 0
Compiling abstract tasks without methods ... done.
	number of abstract tasks = 101
	number of tasks = 379
	number of decomposition methods = 378
	number of predicates = 76
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 278
	number of constants = 0
Compiling initial plan ... done.
	number of abstract tasks = 102
	number of tasks = 380
	number of decomposition methods = 379
	number of predicates = 76
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 278
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 102
	number of tasks = 380
	number of decomposition methods = 379
	number of predicates = 76
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 278
	number of constants = 0
Tasks 278 - 0
Domain is acyclic: false
Domain is mostly acyclic: false
Domain is regular: false
Domain is tail recursive: false
Domain is totally ordered: true
Domain has last task in all methods: true
Time remaining for planner 9223372036854774215ms

Running SAT search with K = 0
Time remaining for SAT search 9223372036854774202ms
Time used for this run 9223372036854774202ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 77
NUMBER OF STATE CLAUSES 76
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 98.7% 1.3% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 299a98c2-f4d0-4e4d-a22f-f143c6e12240
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
Still waiting ... running for 102 will abort at 9223372036854774202
ERROR false

Running SAT search with K = 1
Time remaining for SAT search 9223372036854774096ms
Time used for this run 9223372036854774096ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 77
NUMBER OF STATE CLAUSES 76
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 98.7% 1.3% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 0a1ebcaa-f1fa-4ea2-92c0-8ae175a238ba
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
Still waiting ... running for 101 will abort at 9223372036854774096
ERROR false

Running SAT search with K = 2
Time remaining for SAT search 9223372036854773995ms
Time used for this run 9223372036854773995ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 77
NUMBER OF STATE CLAUSES 76
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 98.7% 1.3% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 96c6b01c-fbb2-4574-8dac-d5a828422eb0
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
Still waiting ... running for 100 will abort at 9223372036854773995
ERROR false

Running SAT search with K = 3
Time remaining for SAT search 9223372036854773893ms
Time used for this run 9223372036854773893ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... Still waiting ... running for 100 will abort at 9223372036854773893
done
NUMBER OF CLAUSES 14150
NUMBER OF STATE CLAUSES 8678
NUMBER OF DECOMPOSITION CLAUSES 5472
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 61.33% 38.67% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 940ec250-b873-43a2-ab31-aba377fc06bf
FLUSH
CLOSE
NUMBER OF PATHS 44
Starting minisat
Setting starttime of solver to 1539959341227
Command exited with non-zero status 20
0.00 0.00

Time command gave the following runtime for the solver: 0
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... done
SAT-Solver says: UNSAT
ERROR false

Running SAT search with K = 4
Time remaining for SAT search 9223372036854773567ms
Time used for this run 9223372036854773567ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
Still waiting ... running for 100 will abort at 9223372036854773567
NUMBER OF CLAUSES 30004
NUMBER OF STATE CLAUSES 13442
NUMBER OF DECOMPOSITION CLAUSES 16562
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 44.800000000000004% 55.2% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID fc49a754-8de3-43aa-91ea-03627475f604
FLUSH
CLOSE
NUMBER OF PATHS 66
Starting minisat
Setting starttime of solver to 1539959341485
Command exited with non-zero status 20
0.00 0.00

Time command gave the following runtime for the solver: 0
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... done
SAT-Solver says: UNSAT
ERROR false

Running SAT search with K = 5
Time remaining for SAT search 9223372036854773348ms
Time used for this run 9223372036854773348ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... Still waiting ... running for 100 will abort at 9223372036854773348
done
NUMBER OF CLAUSES 62200
NUMBER OF STATE CLAUSES 19194
NUMBER OF DECOMPOSITION CLAUSES 43006
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 30.86% 69.14% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID a1a24ec1-ee44-4c4a-ba5f-5e8eae4f8a21
FLUSH
CLOSE
NUMBER OF PATHS 88
Starting minisat
Setting starttime of solver to 1539959341813
Command exited with non-zero status 20
0.02 0.00

Time command gave the following runtime for the solver: 20
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... done
SAT-Solver says: UNSAT
ERROR false

Running SAT search with K = 6
Time remaining for SAT search 9223372036854772928ms
Time used for this run 9223372036854772928ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... Still waiting ... running for 100 will abort at 9223372036854772928
done
NUMBER OF CLAUSES 112244
NUMBER OF STATE CLAUSES 25810
NUMBER OF DECOMPOSITION CLAUSES 86434
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 22.990000000000002% 77.01% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 1f3339c1-8607-44ea-85f1-ae4a536506e7
FLUSH
CLOSE
NUMBER OF PATHS 110
Starting minisat
Setting starttime of solver to 1539959342280
Command exited with non-zero status 10
0.05 0.00

Time command gave the following runtime for the solver: 50
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... done
SAT-Solver says: SAT

extracting solution


CHECKING primitive solution of length 87 ...
true 117 drive[truck-0,city-loc-11,city-loc-10]
true 184 drive[truck-0,city-loc-10,city-loc-8]
true 93 pick-up[truck-0,city-loc-8,package-0,capacity-2,capacity-3]
true 120 drive[truck-0,city-loc-8,city-loc-10]
true 26 drive[truck-0,city-loc-10,city-loc-11]
true 54 drop[truck-0,city-loc-11,package-0,capacity-2,capacity-3]
true 81 drive[truck-1,city-loc-1,city-loc-4]
true 142 drive[truck-1,city-loc-4,city-loc-0]
true 46 drive[truck-1,city-loc-0,city-loc-9]
true 119 drive[truck-1,city-loc-9,city-loc-10]
true 151 pick-up[truck-1,city-loc-10,package-1,capacity-2,capacity-3]
true 8 drive[truck-1,city-loc-10,city-loc-9]
true 125 drive[truck-1,city-loc-9,city-loc-0]
true 176 drive[truck-1,city-loc-0,city-loc-4]
true 56 drive[truck-1,city-loc-4,city-loc-3]
true 36 drop[truck-1,city-loc-3,package-1,capacity-2,capacity-3]
true 65 noop[truck-0,city-loc-11]
true 84 pick-up[truck-0,city-loc-11,package-2,capacity-2,capacity-3]
true 42 drive[truck-0,city-loc-11,city-loc-10]
true 29 drop[truck-0,city-loc-10,package-2,capacity-2,capacity-3]
true 182 drive[truck-1,city-loc-3,city-loc-4]
true 74 drive[truck-1,city-loc-4,city-loc-1]
true 17 pick-up[truck-1,city-loc-1,package-3,capacity-2,capacity-3]
true 102 drive[truck-1,city-loc-1,city-loc-4]
true 122 drive[truck-1,city-loc-4,city-loc-0]
true 58 drive[truck-1,city-loc-0,city-loc-8]
true 112 drive[truck-1,city-loc-8,city-loc-5]
true 141 drop[truck-1,city-loc-5,package-3,capacity-2,capacity-3]
true 24 drive[truck-1,city-loc-5,city-loc-8]
true 5 drive[truck-1,city-loc-8,city-loc-0]
true 137 drive[truck-1,city-loc-0,city-loc-9]
true 130 drive[truck-1,city-loc-9,city-loc-0]
true 181 pick-up[truck-1,city-loc-0,package-4,capacity-2,capacity-3]
true 16 drive[truck-1,city-loc-0,city-loc-8]
true 177 drive[truck-1,city-loc-8,city-loc-10]
true 144 drive[truck-1,city-loc-10,city-loc-8]
true 75 drive[truck-1,city-loc-8,city-loc-5]
true 20 drop[truck-1,city-loc-5,package-4,capacity-2,capacity-3]
true 165 drive[truck-1,city-loc-5,city-loc-8]
true 168 drive[truck-1,city-loc-8,city-loc-0]
true 51 drive[truck-1,city-loc-0,city-loc-4]
true 98 drive[truck-1,city-loc-4,city-loc-3]
true 1 pick-up[truck-1,city-loc-3,package-5,capacity-2,capacity-3]
true 132 drive[truck-1,city-loc-3,city-loc-4]
true 149 drive[truck-1,city-loc-4,city-loc-0]
true 38 drive[truck-1,city-loc-0,city-loc-8]
true 71 drive[truck-1,city-loc-8,city-loc-5]
true 100 drop[truck-1,city-loc-5,package-5,capacity-2,capacity-3]
true 68 drive[truck-0,city-loc-10,city-loc-2]
true 70 pick-up[truck-0,city-loc-2,package-6,capacity-2,capacity-3]
true 18 drive[truck-0,city-loc-2,city-loc-4]
true 0 drive[truck-0,city-loc-4,city-loc-2]
true 34 drive[truck-0,city-loc-2,city-loc-10]
true 60 drive[truck-0,city-loc-10,city-loc-11]
true 104 drop[truck-0,city-loc-11,package-6,capacity-2,capacity-3]
true 47 drive[truck-0,city-loc-11,city-loc-10]
true 185 drive[truck-0,city-loc-10,city-loc-9]
true 161 drive[truck-0,city-loc-9,city-loc-0]
true 52 drive[truck-0,city-loc-0,city-loc-4]
true 121 pick-up[truck-0,city-loc-4,package-7,capacity-2,capacity-3]
true 171 drive[truck-0,city-loc-4,city-loc-3]
true 55 drive[truck-0,city-loc-3,city-loc-4]
true 30 drive[truck-0,city-loc-4,city-loc-2]
true 180 drive[truck-0,city-loc-2,city-loc-10]
true 178 drop[truck-0,city-loc-10,package-7,capacity-2,capacity-3]
true 72 drive[truck-1,city-loc-5,city-loc-8]
true 140 drive[truck-1,city-loc-8,city-loc-10]
true 145 drive[truck-1,city-loc-10,city-loc-9]
true 92 drive[truck-1,city-loc-9,city-loc-2]
true 150 pick-up[truck-1,city-loc-2,package-8,capacity-2,capacity-3]
true 114 drive[truck-1,city-loc-2,city-loc-4]
true 129 drive[truck-1,city-loc-4,city-loc-7]
true 164 drive[truck-1,city-loc-7,city-loc-4]
true 82 drive[truck-1,city-loc-4,city-loc-7]
true 174 drop[truck-1,city-loc-7,package-8,capacity-2,capacity-3]
true 77 noop[truck-0,city-loc-10]
true 94 pick-up[truck-0,city-loc-10,package-9,capacity-2,capacity-3]
true 83 drive[truck-0,city-loc-10,city-loc-8]
true 14 drop[truck-0,city-loc-8,package-9,capacity-2,capacity-3]
true 67 drive[truck-1,city-loc-7,city-loc-4]
true 13 drive[truck-1,city-loc-4,city-loc-1]
true 73 pick-up[truck-1,city-loc-1,package-10,capacity-2,capacity-3]
true 134 drive[truck-1,city-loc-1,city-loc-4]
true 136 drive[truck-1,city-loc-4,city-loc-0]
true 158 drive[truck-1,city-loc-0,city-loc-8]
true 95 drive[truck-1,city-loc-8,city-loc-0]
true 61 drop[truck-1,city-loc-0,package-10,capacity-2,capacity-3]
 done.
ERROR false
Panda says: SOLUTION
============ global ============
randomseed     = 42
peak memory    = 650117120
planner result = SOLUTION
============ properties ============
acyclic                  = false
mostly acyclic           = false
regular                  = false
tail recursive           = false
totally ordered          = true
last task in all methods = true
============ problem ============
number of constants         = 0
number of predicates        = 76
number of actions           = 380
number of abstract actions  = 102
number of primitive actions = 278
number of methods           = 379
============ sat ============
plan length                     = -1
number of variables             = 20336
number of clauses               = 112244
average size of clauses         = 2.9668400983571503
number of assert                = 16
number of horn                  = 100168
K offset                        = 0
K chosen value                  = 6
state formula                   = 25810
method children clauses         = 0
number of paths                 = 110
maximum plan length             = 110
number of decomposition clauses = 86434
number of ordering clauses      = 0
number of state clauses         = 25810
solved                          = true
timeout                         = false

----------------- TIMINGS -----------------
============ total ============
total = 3499
============ parsing ============
total                         = 608
file parser                   = 424
sort expansion                = 71
closed world assumption       = 46
shop methods                  = 1
eliminate identical variables = 15
strip domain of hybridity     = 33
flatten formula               = 17
============ preprocessing ============
total                                      = 916
compile negative preconditions             = 11
compile unit methods                       = 1
split parameter                            = 19
expand choiceless abstract tasks           = 24
expand choiceless abstract tasks           = 3
compile methods with identical tasks       = 0
removing unnecessary predicates            = 80
lifted reachabiltiy analysis               = 32
grounded planning graph analysis           = 308
grounded task decomposition graph analysis = 344
grounding                                  = 82
create artificial top task                 = 3
============ sat ============
total                                        = 1174
generate formula                             = 954
generate path decomposition tree             = 178
normalise path decomposition tree            = 104
translate path decomposition tree to clauses = 349
transform to DIMACS                          = 96
SAT solver                                   = 70
SAT solver for K=0003                        = 0
SAT solver for K=0004                        = 0
SAT solver for K=0005                        = 20
SAT solver for K=0006                        = 50

#1 "40 sat:90:solved"="true";"30 problem:05:number of primitive actions"="278";"30 problem:01:number of constants"="0";"30 problem:04:number of abstract actions"="102";"02 properties:04:tail recursive"="false";"00 global:80:peak memory"="650117120";"40 sat:20:state formula"="25810";"40 sat:01:number of variables"="20336";"40 sat:14:K offset"="0";"40 sat:30:number of paths"="110";"40 sat:00:plan length"="-1";"40 sat:50:number of ordering clauses"="0";"02 properties:02:mostly acyclic"="false";"30 problem:06:number of methods"="379";"02 properties:05:totally ordered"="true";"02 properties:06:last task in all methods"="true";"30 problem:03:number of actions"="380";"30 problem:02:number of predicates"="76";"40 sat:03:number of horn"="100168";"40 sat:15:K chosen value"="6";"02 properties:03:regular"="false";"40 sat:03:average size of clauses"="2.9668400983571503";"40 sat:02:number of clauses"="112244";"40 sat:50:number of state clauses"="25810";"40 sat:03:number of assert"="16";"40 sat:22:method children clauses"="0";"00 global:90:planner result"="SOLUTION";"02 properties:01:acyclic"="false";"40 sat:31:maximum plan length"="110";"40 sat:50:number of decomposition clauses"="86434";"00 global:02:randomseed"="42";"40 sat:91:timeout"="false";"01 parsing:01:file parser"="424";"40 sat:41:SAT solver for K=0004"="0";"40 sat:00:total"="1174";"40 sat:20:transform to DIMACS"="96";"40 sat:11:generate path decomposition tree"="178";"02 preprocessing:07:compile methods with identical tasks"="0";"01 parsing:04:shop methods"="1";"02 preprocessing:08:removing unnecessary predicates"="80";"01 parsing:03:closed world assumption"="46";"02 preprocessing:11:lifted reachabiltiy analysis"="32";"01 parsing:02:sort expansion"="71";"40 sat:12:normalise path decomposition tree"="104";"40 sat:40:SAT solver"="70";"01 parsing:00:total"="608";"02 preprocessing:06:expand choiceless abstract tasks"="3";"01 parsing:06:strip domain of hybridity"="33";"40 sat:13:translate path decomposition tree to clauses"="349";"40 sat:10:generate formula"="954";"40 sat:41:SAT solver for K=0005"="20";"02 preprocessing:01:compile negative preconditions"="11";"00 total:00:total"="3499";"02 preprocessing:12:grounded planning graph analysis"="308";"02 preprocessing:02:compile unit methods"="1";"02 preprocessing:23:grounded task decomposition graph analysis"="344";"40 sat:41:SAT solver for K=0006"="50";"02 preprocessing:04:split parameter"="19";"01 parsing:07:flatten formula"="17";"02 preprocessing:05:expand choiceless abstract tasks"="24";"40 sat:41:SAT solver for K=0003"="0";"02 preprocessing:00:total"="916";"02 preprocessing:99:create artificial top task"="3";"01 parsing:05:eliminate identical variables"="15";"02 preprocessing:84:grounding"="82"
SOLUTION SEQUENCE
0: drive(truck-0,city-loc-11,city-loc-10)
1: drive(truck-0,city-loc-10,city-loc-8)
2: pick-up(truck-0,city-loc-8,package-0,capacity-2,capacity-3)
3: drive(truck-0,city-loc-8,city-loc-10)
4: drive(truck-0,city-loc-10,city-loc-11)
5: drop(truck-0,city-loc-11,package-0,capacity-2,capacity-3)
6: drive(truck-1,city-loc-1,city-loc-4)
7: drive(truck-1,city-loc-4,city-loc-0)
8: drive(truck-1,city-loc-0,city-loc-9)
9: drive(truck-1,city-loc-9,city-loc-10)
10: pick-up(truck-1,city-loc-10,package-1,capacity-2,capacity-3)
11: drive(truck-1,city-loc-10,city-loc-9)
12: drive(truck-1,city-loc-9,city-loc-0)
13: drive(truck-1,city-loc-0,city-loc-4)
14: drive(truck-1,city-loc-4,city-loc-3)
15: drop(truck-1,city-loc-3,package-1,capacity-2,capacity-3)
16: noop(truck-0,city-loc-11)
17: pick-up(truck-0,city-loc-11,package-2,capacity-2,capacity-3)
18: drive(truck-0,city-loc-11,city-loc-10)
19: drop(truck-0,city-loc-10,package-2,capacity-2,capacity-3)
20: drive(truck-1,city-loc-3,city-loc-4)
21: drive(truck-1,city-loc-4,city-loc-1)
22: pick-up(truck-1,city-loc-1,package-3,capacity-2,capacity-3)
23: drive(truck-1,city-loc-1,city-loc-4)
24: drive(truck-1,city-loc-4,city-loc-0)
25: drive(truck-1,city-loc-0,city-loc-8)
26: drive(truck-1,city-loc-8,city-loc-5)
27: drop(truck-1,city-loc-5,package-3,capacity-2,capacity-3)
28: drive(truck-1,city-loc-5,city-loc-8)
29: drive(truck-1,city-loc-8,city-loc-0)
30: drive(truck-1,city-loc-0,city-loc-9)
31: drive(truck-1,city-loc-9,city-loc-0)
32: pick-up(truck-1,city-loc-0,package-4,capacity-2,capacity-3)
33: drive(truck-1,city-loc-0,city-loc-8)
34: drive(truck-1,city-loc-8,city-loc-10)
35: drive(truck-1,city-loc-10,city-loc-8)
36: drive(truck-1,city-loc-8,city-loc-5)
37: drop(truck-1,city-loc-5,package-4,capacity-2,capacity-3)
38: drive(truck-1,city-loc-5,city-loc-8)
39: drive(truck-1,city-loc-8,city-loc-0)
40: drive(truck-1,city-loc-0,city-loc-4)
41: drive(truck-1,city-loc-4,city-loc-3)
42: pick-up(truck-1,city-loc-3,package-5,capacity-2,capacity-3)
43: drive(truck-1,city-loc-3,city-loc-4)
44: drive(truck-1,city-loc-4,city-loc-0)
45: drive(truck-1,city-loc-0,city-loc-8)
46: drive(truck-1,city-loc-8,city-loc-5)
47: drop(truck-1,city-loc-5,package-5,capacity-2,capacity-3)
48: drive(truck-0,city-loc-10,city-loc-2)
49: pick-up(truck-0,city-loc-2,package-6,capacity-2,capacity-3)
50: drive(truck-0,city-loc-2,city-loc-4)
51: drive(truck-0,city-loc-4,city-loc-2)
52: drive(truck-0,city-loc-2,city-loc-10)
53: drive(truck-0,city-loc-10,city-loc-11)
54: drop(truck-0,city-loc-11,package-6,capacity-2,capacity-3)
55: drive(truck-0,city-loc-11,city-loc-10)
56: drive(truck-0,city-loc-10,city-loc-9)
57: drive(truck-0,city-loc-9,city-loc-0)
58: drive(truck-0,city-loc-0,city-loc-4)
59: pick-up(truck-0,city-loc-4,package-7,capacity-2,capacity-3)
60: drive(truck-0,city-loc-4,city-loc-3)
61: drive(truck-0,city-loc-3,city-loc-4)
62: drive(truck-0,city-loc-4,city-loc-2)
63: drive(truck-0,city-loc-2,city-loc-10)
64: drop(truck-0,city-loc-10,package-7,capacity-2,capacity-3)
65: drive(truck-1,city-loc-5,city-loc-8)
66: drive(truck-1,city-loc-8,city-loc-10)
67: drive(truck-1,city-loc-10,city-loc-9)
68: drive(truck-1,city-loc-9,city-loc-2)
69: pick-up(truck-1,city-loc-2,package-8,capacity-2,capacity-3)
70: drive(truck-1,city-loc-2,city-loc-4)
71: drive(truck-1,city-loc-4,city-loc-7)
72: drive(truck-1,city-loc-7,city-loc-4)
73: drive(truck-1,city-loc-4,city-loc-7)
74: drop(truck-1,city-loc-7,package-8,capacity-2,capacity-3)
75: noop(truck-0,city-loc-10)
76: pick-up(truck-0,city-loc-10,package-9,capacity-2,capacity-3)
77: drive(truck-0,city-loc-10,city-loc-8)
78: drop(truck-0,city-loc-8,package-9,capacity-2,capacity-3)
79: drive(truck-1,city-loc-7,city-loc-4)
80: drive(truck-1,city-loc-4,city-loc-1)
81: pick-up(truck-1,city-loc-1,package-10,capacity-2,capacity-3)
82: drive(truck-1,city-loc-1,city-loc-4)
83: drive(truck-1,city-loc-4,city-loc-0)
84: drive(truck-1,city-loc-0,city-loc-8)
85: drive(truck-1,city-loc-8,city-loc-0)
86: drop(truck-1,city-loc-0,package-10,capacity-2,capacity-3)
