PANDA - Planning and Acting in a Network Decomposition Architecture
Believe us: It's great, it's fantastic!

PANDA Copyright (C) 2014-2018 Gregor Behnke, Pascal Bercher, Thomas Geier, Kadir
Dede, Daniel Höller, Kristof Mickeleit, Matthias Englert
This program comes with ABSOLUTELY NO WARRANTY
This is free software, and you are welcome to redistribute it under certain
conditions; run PANDA with -license for details.

Main Developers:
- Gregor Behnke, http://www.uni-ulm.de/in/ki/behnke
- Daniel Höller, http://www.uni-ulm.de/in/ki/hoeller

With many thanks to various further contributors.
Run PANDA with the command line argument -contributors for an extensive list.

Run it with -help for more information like available options.


PANDA was called with: "-systemConfig AAAI-2018-totSAT(minisat) -programPath minisat=./minisat problems/hddl/Transport/domains/domain.hddl problems/hddl/Transport/problems/pfile16.hddl"


Planner Configuration
=====================
Domain: problems/hddl/Transport/domains/domain.hddl
Problem: problems/hddl/Transport/problems/pfile16.hddl
Output: none

Planning Configuration
======================
	printGeneralInformation : true
	printAdditionalData     : true
	random seed             : 42
	time limit (in seconds) : none

	external programs:
		minisat : ./minisat

	Parsing Configuration
	---------------------
	Parser                : autodetect file-type
	Expand Sort Hierarchy : true
	ClosedWordAssumption  : true
	CompileSHOPMethods    : true
	Eliminate Equality    : true
	Strip Hybridity       : true
	Reduce General Tasks  : true
	
	Preprocessing Configuration
	---------------------------
	Compile negative preconditions    : true
	Compile unit methods              : false
	Compile order in methods          : false
	Compile initial plan              : true
	Ensure Methods Have Last Task     : false
	Split independent parameters      : true
	Remove unnecessary predicates     : true
	Expand choiceless abstract tasks  : true
	Domain Cleanup                    : true
	Convert to SAS+                   : false
	Grounded Reachability Analysis    : Planning Graph (mutex-free)
	Grounded Task Decomposition Graph : Two Way TDG
	Iterate reachability analysis     : true
	Ground domain                     : true
	Iterate reachability analysis     : true
	Stop directly after grounding     : false
	
	SAT-Planning Configuration
	--------------------------
	solver           : minisat
	full planner run : true
	reduction method : only normalise 
	check result     : true
	
	Post-processing Configuration
	-----------------------------
	search status
	search result
	timings
	statistics
#0 "00 global:01:problem"="pfile16.hddl";"00 global:00:domain"="domain.hddl"
Parsing domain ... using HDDL parser ... done
Preparing internal domain representation ... done.
Initial domain
	number of abstract tasks = 4
	number of tasks = 8
	number of decomposition methods = 6
	number of predicates = 5
	number of sorts = 7
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 4
	number of constants = 21
Compiling negative preconditions ... done.
	number of abstract tasks = 4
	number of tasks = 8
	number of decomposition methods = 6
	number of predicates = 10
	number of sorts = 7
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 4
	number of constants = 21
Compiling split parameters ... done.
	number of abstract tasks = 4
	number of tasks = 8
	number of decomposition methods = 6
	number of predicates = 10
	number of sorts = 7
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 4
	number of constants = 21
Lifted reachability analysis and domain cleanup ... done.
	number of abstract tasks = 4
	number of tasks = 8
	number of decomposition methods = 6
	number of predicates = 5
	number of sorts = 7
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 4
	number of constants = 21
Grounded planning graph ... done.
	Number of Grounded Actions 146
	Number of Grounded Literals 127
	number of abstract tasks = 4
	number of tasks = 8
	number of decomposition methods = 6
	number of predicates = 5
	number of sorts = 7
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 4
	number of constants = 21
Two Way TDG ... done.
	number of abstract tasks = 4
	number of tasks = 8
	number of decomposition methods = 6
	number of predicates = 5
	number of sorts = 7
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 4
	number of constants = 21
Grounding ... done.
	number of abstract tasks = 72
	number of tasks = 218
	number of decomposition methods = 212
	number of predicates = 73
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 146
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 72
	number of tasks = 218
	number of decomposition methods = 212
	number of predicates = 54
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 146
	number of constants = 0
Omitting lifted reachability analysis ... 
Grounded planning graph ... done.
	Number of Grounded Actions 146
	Number of Grounded Literals 108
	number of abstract tasks = 72
	number of tasks = 218
	number of decomposition methods = 212
	number of predicates = 54
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 146
	number of constants = 0
Two Way TDG ... done.
	number of abstract tasks = 72
	number of tasks = 218
	number of decomposition methods = 212
	number of predicates = 54
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 146
	number of constants = 0
Compiling expand choiceless abstract tasks ... done.
	number of abstract tasks = 72
	number of tasks = 218
	number of decomposition methods = 212
	number of predicates = 54
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 146
	number of constants = 0
Compiling abstract tasks without methods ... done.
	number of abstract tasks = 72
	number of tasks = 218
	number of decomposition methods = 212
	number of predicates = 54
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 146
	number of constants = 0
Compiling initial plan ... done.
	number of abstract tasks = 73
	number of tasks = 219
	number of decomposition methods = 213
	number of predicates = 54
	number of sorts = 0
	number of tasks in largest method = 8
	number of epsilon methods = 0
	number of primitive tasks = 146
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 73
	number of tasks = 219
	number of decomposition methods = 213
	number of predicates = 54
	number of sorts = 0
	number of tasks in largest method = 8
	number of epsilon methods = 0
	number of primitive tasks = 146
	number of constants = 0
Omitting lifted reachability analysis ... 
Grounded planning graph ... done.
	Number of Grounded Actions 146
	Number of Grounded Literals 108
	number of abstract tasks = 73
	number of tasks = 219
	number of decomposition methods = 213
	number of predicates = 54
	number of sorts = 0
	number of tasks in largest method = 8
	number of epsilon methods = 0
	number of primitive tasks = 146
	number of constants = 0
Two Way TDG ... done.
	number of abstract tasks = 73
	number of tasks = 219
	number of decomposition methods = 213
	number of predicates = 54
	number of sorts = 0
	number of tasks in largest method = 8
	number of epsilon methods = 0
	number of primitive tasks = 146
	number of constants = 0
Compiling expand choiceless abstract tasks ... done.
	number of abstract tasks = 72
	number of tasks = 218
	number of decomposition methods = 212
	number of predicates = 54
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 146
	number of constants = 0
Compiling abstract tasks without methods ... done.
	number of abstract tasks = 72
	number of tasks = 218
	number of decomposition methods = 212
	number of predicates = 54
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 146
	number of constants = 0
Compiling initial plan ... done.
	number of abstract tasks = 73
	number of tasks = 219
	number of decomposition methods = 213
	number of predicates = 54
	number of sorts = 0
	number of tasks in largest method = 8
	number of epsilon methods = 0
	number of primitive tasks = 146
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 73
	number of tasks = 219
	number of decomposition methods = 213
	number of predicates = 54
	number of sorts = 0
	number of tasks in largest method = 8
	number of epsilon methods = 0
	number of primitive tasks = 146
	number of constants = 0
Tasks 146 - 0
Domain is acyclic: false
Domain is mostly acyclic: false
Domain is regular: false
Domain is tail recursive: false
Domain is totally ordered: true
Domain has last task in all methods: true
Time remaining for planner 9223372036854774435ms

Running SAT search with K = 0
Time remaining for SAT search 9223372036854774420ms
Time used for this run 9223372036854774420ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 55
NUMBER OF STATE CLAUSES 54
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 98.18% 1.82% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID e8889f8a-38e0-495f-a545-254227fc0ca3
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
Still waiting ... running for 103 will abort at 9223372036854774420
ERROR false

Running SAT search with K = 1
Time remaining for SAT search 9223372036854774315ms
Time used for this run 9223372036854774315ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 55
NUMBER OF STATE CLAUSES 54
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 98.18% 1.82% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID d4b0839a-b452-4315-b137-a8c85e72c03b
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
Still waiting ... running for 100 will abort at 9223372036854774315
ERROR false

Running SAT search with K = 2
Time remaining for SAT search 9223372036854774214ms
Time used for this run 9223372036854774214ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 55
NUMBER OF STATE CLAUSES 54
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 98.18% 1.82% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID cb39c2d0-e9dc-4b72-b410-afecaaef9004
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
Still waiting ... running for 100 will abort at 9223372036854774214
ERROR false

Running SAT search with K = 3
Time remaining for SAT search 9223372036854774113ms
Time used for this run 9223372036854774113ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... Still waiting ... running for 100 will abort at 9223372036854774113
done
NUMBER OF CLAUSES 7496
NUMBER OF STATE CLAUSES 4500
NUMBER OF DECOMPOSITION CLAUSES 2996
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 60.03% 39.97% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 9bf45896-214e-4650-ace9-bb659f066e7f
FLUSH
CLOSE
NUMBER OF PATHS 32
Starting minisat
Setting starttime of solver to 1539959328092
Command exited with non-zero status 20
0.00 0.00

Time command gave the following runtime for the solver: 0
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... done
SAT-Solver says: UNSAT
ERROR false

Running SAT search with K = 4
Time remaining for SAT search 9223372036854773888ms
Time used for this run 9223372036854773888ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
Still waiting ... running for 100 will abort at 9223372036854773888
NUMBER OF CLAUSES 15802
NUMBER OF STATE CLAUSES 6970
NUMBER OF DECOMPOSITION CLAUSES 8832
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 44.11% 55.89% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 99f2304c-1516-4b8d-989f-3d9c823acd14
FLUSH
CLOSE
NUMBER OF PATHS 48
Starting minisat
Setting starttime of solver to 1539959328272
Command exited with non-zero status 20
0.00 0.00

Time command gave the following runtime for the solver: 0
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... done
SAT-Solver says: UNSAT
ERROR false

Running SAT search with K = 5
Time remaining for SAT search 9223372036854773668ms
Time used for this run 9223372036854773668ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
Still waiting ... running for 100 will abort at 9223372036854773668
NUMBER OF CLAUSES 31366
NUMBER OF STATE CLAUSES 9902
NUMBER OF DECOMPOSITION CLAUSES 21464
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 31.57% 68.43% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 8ce0688c-832b-41e3-a936-36401626ffe5
FLUSH
CLOSE
NUMBER OF PATHS 64
Starting minisat
Setting starttime of solver to 1539959328520
Command exited with non-zero status 20
0.00 0.00

Time command gave the following runtime for the solver: 0
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... done
SAT-Solver says: UNSAT
ERROR false

Running SAT search with K = 6
Time remaining for SAT search 9223372036854773453ms
Time used for this run 9223372036854773453ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... Still waiting ... running for 100 will abort at 9223372036854773453
done
NUMBER OF CLAUSES 53586
NUMBER OF STATE CLAUSES 13172
NUMBER OF DECOMPOSITION CLAUSES 40414
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 24.580000000000002% 75.42% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 2f1d3d73-e415-488d-8f06-128056da4140
FLUSH
CLOSE
NUMBER OF PATHS 80
Starting minisat
Setting starttime of solver to 1539959328797
Command exited with non-zero status 10
0.02 0.00

Time command gave the following runtime for the solver: 20
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... done
SAT-Solver says: SAT

extracting solution


CHECKING primitive solution of length 64 ...
true 86 drive[truck-0,city-loc-5,city-loc-4]
true 120 drive[truck-0,city-loc-4,city-loc-6]
true 131 drive[truck-0,city-loc-6,city-loc-1]
true 82 drive[truck-0,city-loc-1,city-loc-0]
true 5 pick-up[truck-0,city-loc-0,package-0,capacity-1,capacity-2]
true 60 drive[truck-0,city-loc-0,city-loc-1]
true 103 drive[truck-0,city-loc-1,city-loc-6]
true 123 drive[truck-0,city-loc-6,city-loc-5]
true 91 drive[truck-0,city-loc-5,city-loc-2]
true 133 drop[truck-0,city-loc-2,package-0,capacity-1,capacity-2]
true 26 drive[truck-1,city-loc-5,city-loc-6]
true 61 drive[truck-1,city-loc-6,city-loc-1]
true 127 drive[truck-1,city-loc-1,city-loc-0]
true 104 drive[truck-1,city-loc-0,city-loc-3]
true 101 pick-up[truck-1,city-loc-3,package-1,capacity-1,capacity-2]
true 136 drive[truck-1,city-loc-3,city-loc-0]
true 24 drive[truck-1,city-loc-0,city-loc-1]
true 118 drive[truck-1,city-loc-1,city-loc-6]
true 72 drive[truck-1,city-loc-6,city-loc-4]
true 62 drop[truck-1,city-loc-4,package-1,capacity-1,capacity-2]
true 14 noop[truck-1,city-loc-4]
true 89 pick-up[truck-1,city-loc-4,package-2,capacity-1,capacity-2]
true 117 drive[truck-1,city-loc-4,city-loc-5]
true 77 drive[truck-1,city-loc-5,city-loc-6]
true 74 drive[truck-1,city-loc-6,city-loc-1]
true 51 drive[truck-1,city-loc-1,city-loc-0]
true 112 drop[truck-1,city-loc-0,package-2,capacity-1,capacity-2]
true 95 drive[truck-0,city-loc-2,city-loc-5]
true 20 drive[truck-0,city-loc-5,city-loc-7]
true 128 pick-up[truck-0,city-loc-7,package-3,capacity-1,capacity-2]
true 108 drive[truck-0,city-loc-7,city-loc-5]
true 8 drive[truck-0,city-loc-5,city-loc-7]
true 81 drive[truck-0,city-loc-7,city-loc-5]
true 58 drive[truck-0,city-loc-5,city-loc-2]
true 65 drop[truck-0,city-loc-2,package-3,capacity-1,capacity-2]
true 28 drive[truck-0,city-loc-2,city-loc-5]
true 66 drive[truck-0,city-loc-5,city-loc-4]
true 116 pick-up[truck-0,city-loc-4,package-4,capacity-1,capacity-2]
true 69 drive[truck-0,city-loc-4,city-loc-5]
true 40 drop[truck-0,city-loc-5,package-4,capacity-1,capacity-2]
true 48 drive[truck-1,city-loc-0,city-loc-3]
true 71 pick-up[truck-1,city-loc-3,package-5,capacity-1,capacity-2]
true 30 drive[truck-1,city-loc-3,city-loc-0]
true 100 drive[truck-1,city-loc-0,city-loc-1]
true 38 drive[truck-1,city-loc-1,city-loc-6]
true 46 drive[truck-1,city-loc-6,city-loc-5]
true 96 drop[truck-1,city-loc-5,package-5,capacity-1,capacity-2]
true 2 drive[truck-1,city-loc-5,city-loc-2]
true 57 drive[truck-1,city-loc-2,city-loc-5]
true 37 drive[truck-1,city-loc-5,city-loc-2]
true 22 pick-up[truck-1,city-loc-2,package-6,capacity-1,capacity-2]
true 90 drive[truck-1,city-loc-2,city-loc-5]
true 129 drive[truck-1,city-loc-5,city-loc-7]
true 78 drop[truck-1,city-loc-7,package-6,capacity-1,capacity-2]
true 54 drive[truck-1,city-loc-7,city-loc-5]
true 18 drive[truck-1,city-loc-5,city-loc-7]
true 132 drive[truck-1,city-loc-7,city-loc-5]
true 36 drive[truck-1,city-loc-5,city-loc-7]
true 134 pick-up[truck-1,city-loc-7,package-7,capacity-1,capacity-2]
true 17 drive[truck-1,city-loc-7,city-loc-5]
true 67 drive[truck-1,city-loc-5,city-loc-6]
true 126 drive[truck-1,city-loc-6,city-loc-5]
true 9 drive[truck-1,city-loc-5,city-loc-4]
true 52 drop[truck-1,city-loc-4,package-7,capacity-1,capacity-2]
 done.
ERROR false
Panda says: SOLUTION
============ global ============
randomseed     = 42
peak memory    = 541065216
planner result = SOLUTION
============ properties ============
acyclic                  = false
mostly acyclic           = false
regular                  = false
tail recursive           = false
totally ordered          = true
last task in all methods = true
============ problem ============
number of constants         = 0
number of predicates        = 54
number of actions           = 219
number of abstract actions  = 73
number of primitive actions = 146
number of methods           = 213
============ sat ============
plan length                     = -1
number of variables             = 10897
number of clauses               = 53586
average size of clauses         = 2.7531631396260217
number of assert                = 13
number of horn                  = 47214
K offset                        = 0
K chosen value                  = 6
state formula                   = 13172
method children clauses         = 0
number of paths                 = 80
maximum plan length             = 80
number of decomposition clauses = 40414
number of ordering clauses      = 0
number of state clauses         = 13172
solved                          = true
timeout                         = false

----------------- TIMINGS -----------------
============ total ============
total = 2869
============ parsing ============
total                         = 596
file parser                   = 408
sort expansion                = 79
closed world assumption       = 42
shop methods                  = 2
eliminate identical variables = 17
strip domain of hybridity     = 29
flatten formula               = 17
============ preprocessing ============
total                                      = 723
compile negative preconditions             = 12
compile unit methods                       = 0
split parameter                            = 18
expand choiceless abstract tasks           = 19
expand choiceless abstract tasks           = 2
compile methods with identical tasks       = 0
removing unnecessary predicates            = 53
lifted reachabiltiy analysis               = 27
grounded planning graph analysis           = 236
grounded task decomposition graph analysis = 280
grounding                                  = 64
create artificial top task                 = 3
============ sat ============
total                                        = 781
generate formula                             = 638
generate path decomposition tree             = 146
normalise path decomposition tree            = 66
translate path decomposition tree to clauses = 208
transform to DIMACS                          = 65
SAT solver                                   = 20
SAT solver for K=0003                        = 0
SAT solver for K=0004                        = 0
SAT solver for K=0005                        = 0
SAT solver for K=0006                        = 20

#1 "40 sat:90:solved"="true";"30 problem:05:number of primitive actions"="146";"30 problem:01:number of constants"="0";"30 problem:04:number of abstract actions"="73";"02 properties:04:tail recursive"="false";"00 global:80:peak memory"="541065216";"40 sat:20:state formula"="13172";"40 sat:01:number of variables"="10897";"40 sat:14:K offset"="0";"40 sat:30:number of paths"="80";"40 sat:00:plan length"="-1";"40 sat:50:number of ordering clauses"="0";"02 properties:02:mostly acyclic"="false";"30 problem:06:number of methods"="213";"02 properties:05:totally ordered"="true";"02 properties:06:last task in all methods"="true";"30 problem:03:number of actions"="219";"30 problem:02:number of predicates"="54";"40 sat:03:number of horn"="47214";"40 sat:15:K chosen value"="6";"02 properties:03:regular"="false";"40 sat:03:average size of clauses"="2.7531631396260217";"40 sat:02:number of clauses"="53586";"40 sat:50:number of state clauses"="13172";"40 sat:03:number of assert"="13";"40 sat:22:method children clauses"="0";"00 global:90:planner result"="SOLUTION";"02 properties:01:acyclic"="false";"40 sat:31:maximum plan length"="80";"40 sat:50:number of decomposition clauses"="40414";"00 global:02:randomseed"="42";"40 sat:91:timeout"="false";"01 parsing:01:file parser"="408";"40 sat:41:SAT solver for K=0004"="0";"40 sat:00:total"="781";"40 sat:20:transform to DIMACS"="65";"40 sat:11:generate path decomposition tree"="146";"02 preprocessing:07:compile methods with identical tasks"="0";"01 parsing:04:shop methods"="2";"02 preprocessing:08:removing unnecessary predicates"="53";"01 parsing:03:closed world assumption"="42";"02 preprocessing:11:lifted reachabiltiy analysis"="27";"01 parsing:02:sort expansion"="79";"40 sat:12:normalise path decomposition tree"="66";"40 sat:40:SAT solver"="20";"01 parsing:00:total"="596";"02 preprocessing:06:expand choiceless abstract tasks"="2";"01 parsing:06:strip domain of hybridity"="29";"40 sat:13:translate path decomposition tree to clauses"="208";"40 sat:10:generate formula"="638";"40 sat:41:SAT solver for K=0005"="0";"02 preprocessing:01:compile negative preconditions"="12";"00 total:00:total"="2869";"02 preprocessing:12:grounded planning graph analysis"="236";"02 preprocessing:02:compile unit methods"="0";"02 preprocessing:23:grounded task decomposition graph analysis"="280";"40 sat:41:SAT solver for K=0006"="20";"02 preprocessing:04:split parameter"="18";"01 parsing:07:flatten formula"="17";"02 preprocessing:05:expand choiceless abstract tasks"="19";"40 sat:41:SAT solver for K=0003"="0";"02 preprocessing:00:total"="723";"02 preprocessing:99:create artificial top task"="3";"01 parsing:05:eliminate identical variables"="17";"02 preprocessing:84:grounding"="64"
SOLUTION SEQUENCE
0: drive(truck-0,city-loc-5,city-loc-4)
1: drive(truck-0,city-loc-4,city-loc-6)
2: drive(truck-0,city-loc-6,city-loc-1)
3: drive(truck-0,city-loc-1,city-loc-0)
4: pick-up(truck-0,city-loc-0,package-0,capacity-1,capacity-2)
5: drive(truck-0,city-loc-0,city-loc-1)
6: drive(truck-0,city-loc-1,city-loc-6)
7: drive(truck-0,city-loc-6,city-loc-5)
8: drive(truck-0,city-loc-5,city-loc-2)
9: drop(truck-0,city-loc-2,package-0,capacity-1,capacity-2)
10: drive(truck-1,city-loc-5,city-loc-6)
11: drive(truck-1,city-loc-6,city-loc-1)
12: drive(truck-1,city-loc-1,city-loc-0)
13: drive(truck-1,city-loc-0,city-loc-3)
14: pick-up(truck-1,city-loc-3,package-1,capacity-1,capacity-2)
15: drive(truck-1,city-loc-3,city-loc-0)
16: drive(truck-1,city-loc-0,city-loc-1)
17: drive(truck-1,city-loc-1,city-loc-6)
18: drive(truck-1,city-loc-6,city-loc-4)
19: drop(truck-1,city-loc-4,package-1,capacity-1,capacity-2)
20: noop(truck-1,city-loc-4)
21: pick-up(truck-1,city-loc-4,package-2,capacity-1,capacity-2)
22: drive(truck-1,city-loc-4,city-loc-5)
23: drive(truck-1,city-loc-5,city-loc-6)
24: drive(truck-1,city-loc-6,city-loc-1)
25: drive(truck-1,city-loc-1,city-loc-0)
26: drop(truck-1,city-loc-0,package-2,capacity-1,capacity-2)
27: drive(truck-0,city-loc-2,city-loc-5)
28: drive(truck-0,city-loc-5,city-loc-7)
29: pick-up(truck-0,city-loc-7,package-3,capacity-1,capacity-2)
30: drive(truck-0,city-loc-7,city-loc-5)
31: drive(truck-0,city-loc-5,city-loc-7)
32: drive(truck-0,city-loc-7,city-loc-5)
33: drive(truck-0,city-loc-5,city-loc-2)
34: drop(truck-0,city-loc-2,package-3,capacity-1,capacity-2)
35: drive(truck-0,city-loc-2,city-loc-5)
36: drive(truck-0,city-loc-5,city-loc-4)
37: pick-up(truck-0,city-loc-4,package-4,capacity-1,capacity-2)
38: drive(truck-0,city-loc-4,city-loc-5)
39: drop(truck-0,city-loc-5,package-4,capacity-1,capacity-2)
40: drive(truck-1,city-loc-0,city-loc-3)
41: pick-up(truck-1,city-loc-3,package-5,capacity-1,capacity-2)
42: drive(truck-1,city-loc-3,city-loc-0)
43: drive(truck-1,city-loc-0,city-loc-1)
44: drive(truck-1,city-loc-1,city-loc-6)
45: drive(truck-1,city-loc-6,city-loc-5)
46: drop(truck-1,city-loc-5,package-5,capacity-1,capacity-2)
47: drive(truck-1,city-loc-5,city-loc-2)
48: drive(truck-1,city-loc-2,city-loc-5)
49: drive(truck-1,city-loc-5,city-loc-2)
50: pick-up(truck-1,city-loc-2,package-6,capacity-1,capacity-2)
51: drive(truck-1,city-loc-2,city-loc-5)
52: drive(truck-1,city-loc-5,city-loc-7)
53: drop(truck-1,city-loc-7,package-6,capacity-1,capacity-2)
54: drive(truck-1,city-loc-7,city-loc-5)
55: drive(truck-1,city-loc-5,city-loc-7)
56: drive(truck-1,city-loc-7,city-loc-5)
57: drive(truck-1,city-loc-5,city-loc-7)
58: pick-up(truck-1,city-loc-7,package-7,capacity-1,capacity-2)
59: drive(truck-1,city-loc-7,city-loc-5)
60: drive(truck-1,city-loc-5,city-loc-6)
61: drive(truck-1,city-loc-6,city-loc-5)
62: drive(truck-1,city-loc-5,city-loc-4)
63: drop(truck-1,city-loc-4,package-7,capacity-1,capacity-2)
