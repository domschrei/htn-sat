PANDA - Planning and Acting in a Network Decomposition Architecture
Believe us: It's great, it's fantastic!

PANDA Copyright (C) 2014-2018 Gregor Behnke, Pascal Bercher, Thomas Geier, Kadir
Dede, Daniel Höller, Kristof Mickeleit, Matthias Englert
This program comes with ABSOLUTELY NO WARRANTY
This is free software, and you are welcome to redistribute it under certain
conditions; run PANDA with -license for details.

Main Developers:
- Gregor Behnke, http://www.uni-ulm.de/in/ki/behnke
- Daniel Höller, http://www.uni-ulm.de/in/ki/hoeller

With many thanks to various further contributors.
Run PANDA with the command line argument -contributors for an extensive list.

Run it with -help for more information like available options.


PANDA was called with: "-systemConfig AAAI-2018-totSAT(minisat) -programPath minisat=./minisat problems/hddl/Transport/domains/domain.hddl problems/hddl/Transport/problems/pfile27.hddl"


Planner Configuration
=====================
Domain: problems/hddl/Transport/domains/domain.hddl
Problem: problems/hddl/Transport/problems/pfile27.hddl
Output: none

Planning Configuration
======================
	printGeneralInformation : true
	printAdditionalData     : true
	random seed             : 42
	time limit (in seconds) : none

	external programs:
		minisat : ./minisat

	Parsing Configuration
	---------------------
	Parser                : autodetect file-type
	Expand Sort Hierarchy : true
	ClosedWordAssumption  : true
	CompileSHOPMethods    : true
	Eliminate Equality    : true
	Strip Hybridity       : true
	Reduce General Tasks  : true
	
	Preprocessing Configuration
	---------------------------
	Compile negative preconditions    : true
	Compile unit methods              : false
	Compile order in methods          : false
	Compile initial plan              : true
	Ensure Methods Have Last Task     : false
	Split independent parameters      : true
	Remove unnecessary predicates     : true
	Expand choiceless abstract tasks  : true
	Domain Cleanup                    : true
	Convert to SAS+                   : false
	Grounded Reachability Analysis    : Planning Graph (mutex-free)
	Grounded Task Decomposition Graph : Two Way TDG
	Iterate reachability analysis     : true
	Ground domain                     : true
	Iterate reachability analysis     : true
	Stop directly after grounding     : false
	
	SAT-Planning Configuration
	--------------------------
	solver           : minisat
	full planner run : true
	reduction method : only normalise 
	check result     : true
	
	Post-processing Configuration
	-----------------------------
	search status
	search result
	timings
	statistics
#0 "00 global:01:problem"="pfile27.hddl";"00 global:00:domain"="domain.hddl"
Parsing domain ... using HDDL parser ... done
Preparing internal domain representation ... done.
Initial domain
	number of abstract tasks = 4
	number of tasks = 8
	number of decomposition methods = 6
	number of predicates = 5
	number of sorts = 7
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 4
	number of constants = 38
Compiling negative preconditions ... done.
	number of abstract tasks = 4
	number of tasks = 8
	number of decomposition methods = 6
	number of predicates = 10
	number of sorts = 7
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 4
	number of constants = 38
Compiling split parameters ... done.
	number of abstract tasks = 4
	number of tasks = 8
	number of decomposition methods = 6
	number of predicates = 10
	number of sorts = 7
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 4
	number of constants = 38
Lifted reachability analysis and domain cleanup ... done.
	number of abstract tasks = 4
	number of tasks = 8
	number of decomposition methods = 6
	number of predicates = 5
	number of sorts = 7
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 4
	number of constants = 38
Grounded planning graph ... done.
	Number of Grounded Actions 770
	Number of Grounded Literals 441
	number of abstract tasks = 4
	number of tasks = 8
	number of decomposition methods = 6
	number of predicates = 5
	number of sorts = 7
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 4
	number of constants = 38
Two Way TDG ... done.
	number of abstract tasks = 4
	number of tasks = 8
	number of decomposition methods = 6
	number of predicates = 5
	number of sorts = 7
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 4
	number of constants = 38
Grounding ... done.
	number of abstract tasks = 315
	number of tasks = 1085
	number of decomposition methods = 1165
	number of predicates = 246
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 770
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 315
	number of tasks = 1085
	number of decomposition methods = 1165
	number of predicates = 195
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 770
	number of constants = 0
Omitting lifted reachability analysis ... 
Grounded planning graph ... done.
	Number of Grounded Actions 770
	Number of Grounded Literals 390
	number of abstract tasks = 315
	number of tasks = 1085
	number of decomposition methods = 1165
	number of predicates = 195
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 770
	number of constants = 0
Two Way TDG ... done.
	number of abstract tasks = 315
	number of tasks = 1085
	number of decomposition methods = 1165
	number of predicates = 195
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 770
	number of constants = 0
Compiling expand choiceless abstract tasks ... done.
	number of abstract tasks = 315
	number of tasks = 1085
	number of decomposition methods = 1165
	number of predicates = 195
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 770
	number of constants = 0
Compiling abstract tasks without methods ... done.
	number of abstract tasks = 315
	number of tasks = 1085
	number of decomposition methods = 1165
	number of predicates = 195
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 770
	number of constants = 0
Compiling initial plan ... done.
	number of abstract tasks = 316
	number of tasks = 1086
	number of decomposition methods = 1166
	number of predicates = 195
	number of sorts = 0
	number of tasks in largest method = 15
	number of epsilon methods = 0
	number of primitive tasks = 770
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 316
	number of tasks = 1086
	number of decomposition methods = 1166
	number of predicates = 195
	number of sorts = 0
	number of tasks in largest method = 15
	number of epsilon methods = 0
	number of primitive tasks = 770
	number of constants = 0
Omitting lifted reachability analysis ... 
Grounded planning graph ... done.
	Number of Grounded Actions 770
	Number of Grounded Literals 390
	number of abstract tasks = 316
	number of tasks = 1086
	number of decomposition methods = 1166
	number of predicates = 195
	number of sorts = 0
	number of tasks in largest method = 15
	number of epsilon methods = 0
	number of primitive tasks = 770
	number of constants = 0
Two Way TDG ... done.
	number of abstract tasks = 316
	number of tasks = 1086
	number of decomposition methods = 1166
	number of predicates = 195
	number of sorts = 0
	number of tasks in largest method = 15
	number of epsilon methods = 0
	number of primitive tasks = 770
	number of constants = 0
Compiling expand choiceless abstract tasks ... done.
	number of abstract tasks = 315
	number of tasks = 1085
	number of decomposition methods = 1165
	number of predicates = 195
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 770
	number of constants = 0
Compiling abstract tasks without methods ... done.
	number of abstract tasks = 315
	number of tasks = 1085
	number of decomposition methods = 1165
	number of predicates = 195
	number of sorts = 0
	number of tasks in largest method = 4
	number of epsilon methods = 0
	number of primitive tasks = 770
	number of constants = 0
Compiling initial plan ... done.
	number of abstract tasks = 316
	number of tasks = 1086
	number of decomposition methods = 1166
	number of predicates = 195
	number of sorts = 0
	number of tasks in largest method = 15
	number of epsilon methods = 0
	number of primitive tasks = 770
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 316
	number of tasks = 1086
	number of decomposition methods = 1166
	number of predicates = 195
	number of sorts = 0
	number of tasks in largest method = 15
	number of epsilon methods = 0
	number of primitive tasks = 770
	number of constants = 0
Tasks 770 - 0
Domain is acyclic: false
Domain is mostly acyclic: false
Domain is regular: false
Domain is tail recursive: false
Domain is totally ordered: true
Domain has last task in all methods: true
Time remaining for planner 9223372036854773179ms

Running SAT search with K = 0
Time remaining for SAT search 9223372036854773164ms
Time used for this run 9223372036854773164ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 196
NUMBER OF STATE CLAUSES 195
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 99.49000000000001% 0.51% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID fb984890-346a-46e5-8e26-94b59d81322f
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
Still waiting ... running for 103 will abort at 9223372036854773164
ERROR false

Running SAT search with K = 1
Time remaining for SAT search 9223372036854773058ms
Time used for this run 9223372036854773058ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 196
NUMBER OF STATE CLAUSES 195
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 99.49000000000001% 0.51% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID edfb439c-3815-4869-9a3c-62591a3ede1a
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
Still waiting ... running for 101 will abort at 9223372036854773058
ERROR false

Running SAT search with K = 2
Time remaining for SAT search 9223372036854772957ms
Time used for this run 9223372036854772957ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 196
NUMBER OF STATE CLAUSES 195
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 99.49000000000001% 0.51% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID e9415ef7-4bf4-4b9e-aed6-4689f86e6140
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
Still waiting ... running for 100 will abort at 9223372036854772957
ERROR false

Running SAT search with K = 3
Time remaining for SAT search 9223372036854772855ms
Time used for this run 9223372036854772855ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... Still waiting ... running for 100 will abort at 9223372036854772855
done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 50379
NUMBER OF STATE CLAUSES 29000
NUMBER OF DECOMPOSITION CLAUSES 21379
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 57.56% 42.44% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID f411685e-603b-4534-84a4-e44f1d5efd98
FLUSH
CLOSE
NUMBER OF PATHS 60
Starting minisat
Setting starttime of solver to 1539959611080
Command exited with non-zero status 20
0.01 0.00

Time command gave the following runtime for the solver: 10
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... done
SAT-Solver says: UNSAT
ERROR false

Running SAT search with K = 4
Time remaining for SAT search 9223372036854772332ms
Time used for this run 9223372036854772332ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... Still waiting ... running for 100 will abort at 9223372036854772332
done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 161999
NUMBER OF STATE CLAUSES 49630
NUMBER OF DECOMPOSITION CLAUSES 112369
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 30.64% 69.36% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 70164740-c728-4869-acdc-0e4f89773597
FLUSH
CLOSE
NUMBER OF PATHS 90
Starting minisat
Setting starttime of solver to 1539959611649
Command exited with non-zero status 20
0.03 0.00

Time command gave the following runtime for the solver: 30
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... done
SAT-Solver says: UNSAT
ERROR false

Running SAT search with K = 5
Time remaining for SAT search 9223372036854771711ms
Time used for this run 9223372036854771711ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... Still waiting ... running for 101 will abort at 9223372036854771711
done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 563919
NUMBER OF STATE CLAUSES 80130
NUMBER OF DECOMPOSITION CLAUSES 483789
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 14.21% 85.79% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 5c160a9b-d66f-4673-b965-d7077484cf81
FLUSH
CLOSE
NUMBER OF PATHS 120
Starting minisat
Setting starttime of solver to 1539959613075
Command exited with non-zero status 10
0.32 0.02

Time command gave the following runtime for the solver: 340
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... done
SAT-Solver says: SAT

extracting solution


CHECKING primitive solution of length 108 ...
true 119 drive[truck-1,city-loc-3,city-loc-13]
true 179 drive[truck-1,city-loc-13,city-loc-8]
true 230 drive[truck-1,city-loc-8,city-loc-5]
true 92 pick-up[truck-1,city-loc-5,package-0,capacity-1,capacity-2]
true 171 drive[truck-1,city-loc-5,city-loc-8]
true 207 drive[truck-1,city-loc-8,city-loc-4]
true 139 drive[truck-1,city-loc-4,city-loc-9]
true 43 drop[truck-1,city-loc-9,package-0,capacity-1,capacity-2]
true 22 drive[truck-1,city-loc-9,city-loc-11]
true 138 drive[truck-1,city-loc-11,city-loc-2]
true 167 pick-up[truck-1,city-loc-2,package-1,capacity-1,capacity-2]
true 220 drive[truck-1,city-loc-2,city-loc-8]
true 151 drive[truck-1,city-loc-8,city-loc-2]
true 215 drive[truck-1,city-loc-2,city-loc-0]
true 216 drop[truck-1,city-loc-0,package-1,capacity-1,capacity-2]
true 176 drive[truck-3,city-loc-3,city-loc-11]
true 208 pick-up[truck-3,city-loc-11,package-2,capacity-1,capacity-2]
true 54 drive[truck-3,city-loc-11,city-loc-2]
true 99 drive[truck-3,city-loc-2,city-loc-8]
true 170 drive[truck-3,city-loc-8,city-loc-8]
true 50 drop[truck-3,city-loc-8,package-2,capacity-1,capacity-2]
true 62 drive[truck-4,city-loc-12,city-loc-10]
true 223 drive[truck-4,city-loc-10,city-loc-8]
true 154 pick-up[truck-4,city-loc-8,package-3,capacity-1,capacity-2]
true 123 drive[truck-4,city-loc-8,city-loc-2]
true 152 drive[truck-4,city-loc-2,city-loc-6]
true 221 drop[truck-4,city-loc-6,package-3,capacity-1,capacity-2]
true 197 drive[truck-3,city-loc-8,city-loc-4]
true 51 drive[truck-3,city-loc-4,city-loc-7]
true 227 pick-up[truck-3,city-loc-7,package-4,capacity-1,capacity-2]
true 48 drive[truck-3,city-loc-7,city-loc-4]
true 17 drive[truck-3,city-loc-4,city-loc-8]
true 10 drive[truck-3,city-loc-8,city-loc-13]
true 7 drop[truck-3,city-loc-13,package-4,capacity-1,capacity-2]
true 68 drive[truck-3,city-loc-13,city-loc-8]
true 93 drive[truck-3,city-loc-8,city-loc-14]
true 71 drive[truck-3,city-loc-14,city-loc-0]
true 213 pick-up[truck-3,city-loc-0,package-5,capacity-1,capacity-2]
true 217 drive[truck-3,city-loc-0,city-loc-2]
true 131 drive[truck-3,city-loc-2,city-loc-11]
true 1 drive[truck-3,city-loc-11,city-loc-9]
true 212 drop[truck-3,city-loc-9,package-5,capacity-1,capacity-2]
true 231 drive[truck-0,city-loc-12,city-loc-10]
true 155 drive[truck-0,city-loc-10,city-loc-3]
true 158 drive[truck-0,city-loc-3,city-loc-13]
true 69 pick-up[truck-0,city-loc-13,package-6,capacity-1,capacity-2]
true 121 drive[truck-0,city-loc-13,city-loc-3]
true 173 drive[truck-0,city-loc-3,city-loc-11]
true 58 drive[truck-0,city-loc-11,city-loc-1]
true 222 drop[truck-0,city-loc-1,package-6,capacity-1,capacity-2]
true 174 noop[truck-4,city-loc-6]
true 105 pick-up[truck-4,city-loc-6,package-7,capacity-1,capacity-2]
true 156 drive[truck-4,city-loc-6,city-loc-2]
true 59 drive[truck-4,city-loc-2,city-loc-8]
true 28 drop[truck-4,city-loc-8,package-7,capacity-1,capacity-2]
true 14 drive[truck-2,city-loc-7,city-loc-4]
true 168 drive[truck-2,city-loc-4,city-loc-8]
true 183 drive[truck-2,city-loc-8,city-loc-5]
true 137 pick-up[truck-2,city-loc-5,package-8,capacity-1,capacity-2]
true 3 drive[truck-2,city-loc-5,city-loc-8]
true 39 drive[truck-2,city-loc-8,city-loc-13]
true 85 drive[truck-2,city-loc-13,city-loc-11]
true 86 drop[truck-2,city-loc-11,package-8,capacity-1,capacity-2]
true 24 drive[truck-2,city-loc-11,city-loc-2]
true 94 drive[truck-2,city-loc-2,city-loc-0]
true 205 pick-up[truck-2,city-loc-0,package-9,capacity-1,capacity-2]
true 169 drive[truck-2,city-loc-0,city-loc-14]
true 140 drive[truck-2,city-loc-14,city-loc-12]
true 214 drive[truck-2,city-loc-12,city-loc-12]
true 206 drop[truck-2,city-loc-12,package-9,capacity-1,capacity-2]
true 182 drive[truck-4,city-loc-8,city-loc-13]
true 23 drive[truck-4,city-loc-13,city-loc-11]
true 198 drive[truck-4,city-loc-11,city-loc-3]
true 162 pick-up[truck-4,city-loc-3,package-10,capacity-1,capacity-2]
true 81 drive[truck-4,city-loc-3,city-loc-8]
true 145 drive[truck-4,city-loc-8,city-loc-4]
true 201 drop[truck-4,city-loc-4,package-10,capacity-1,capacity-2]
true 0 drive[truck-4,city-loc-4,city-loc-8]
true 87 drive[truck-4,city-loc-8,city-loc-2]
true 159 drive[truck-4,city-loc-2,city-loc-6]
true 164 pick-up[truck-4,city-loc-6,package-11,capacity-1,capacity-2]
true 202 drive[truck-4,city-loc-6,city-loc-2]
true 44 drive[truck-4,city-loc-2,city-loc-11]
true 76 drive[truck-4,city-loc-11,city-loc-1]
true 109 drop[truck-4,city-loc-1,package-11,capacity-1,capacity-2]
true 150 drive[truck-3,city-loc-9,city-loc-11]
true 113 drive[truck-3,city-loc-11,city-loc-2]
true 189 drive[truck-3,city-loc-2,city-loc-8]
true 49 pick-up[truck-3,city-loc-8,package-12,capacity-1,capacity-2]
true 29 drive[truck-3,city-loc-8,city-loc-2]
true 188 drive[truck-3,city-loc-2,city-loc-6]
true 200 drop[truck-3,city-loc-6,package-12,capacity-1,capacity-2]
true 190 drive[truck-0,city-loc-1,city-loc-3]
true 104 drive[truck-0,city-loc-3,city-loc-8]
true 61 drive[truck-0,city-loc-8,city-loc-5]
true 111 pick-up[truck-0,city-loc-5,package-13,capacity-1,capacity-2]
true 32 drive[truck-0,city-loc-5,city-loc-8]
true 181 drive[truck-0,city-loc-8,city-loc-14]
true 79 drive[truck-0,city-loc-14,city-loc-0]
true 88 drop[truck-0,city-loc-0,package-13,capacity-1,capacity-2]
true 122 drive[truck-3,city-loc-6,city-loc-2]
true 77 drive[truck-3,city-loc-2,city-loc-11]
true 195 drive[truck-3,city-loc-11,city-loc-9]
true 106 pick-up[truck-3,city-loc-9,package-14,capacity-1,capacity-2]
true 53 drive[truck-3,city-loc-9,city-loc-4]
true 84 drive[truck-3,city-loc-4,city-loc-8]
true 166 drive[truck-3,city-loc-8,city-loc-5]
true 38 drop[truck-3,city-loc-5,package-14,capacity-1,capacity-2]
 done.
ERROR false
Panda says: SOLUTION
============ global ============
randomseed     = 42
peak memory    = 771024896
planner result = SOLUTION
============ properties ============
acyclic                  = false
mostly acyclic           = false
regular                  = false
tail recursive           = false
totally ordered          = true
last task in all methods = true
============ problem ============
number of constants         = 0
number of predicates        = 195
number of actions           = 1086
number of abstract actions  = 316
number of primitive actions = 770
number of methods           = 1166
============ sat ============
plan length                     = -1
number of variables             = 62959
number of clauses               = 563919
average size of clauses         = 4.132662669638725
number of assert                = 26
number of horn                  = 523359
K offset                        = 0
K chosen value                  = 5
state formula                   = 80130
method children clauses         = 0
number of paths                 = 120
maximum plan length             = 120
number of decomposition clauses = 483789
number of ordering clauses      = 0
number of state clauses         = 80130
solved                          = true
timeout                         = false

----------------- TIMINGS -----------------
============ total ============
total = 6231
============ parsing ============
total                         = 660
file parser                   = 439
sort expansion                = 99
closed world assumption       = 47
shop methods                  = 2
eliminate identical variables = 17
strip domain of hybridity     = 37
flatten formula               = 16
============ preprocessing ============
total                                      = 1831
compile negative preconditions             = 14
compile unit methods                       = 0
split parameter                            = 19
expand choiceless abstract tasks           = 34
expand choiceless abstract tasks           = 10
compile methods with identical tasks       = 1
removing unnecessary predicates            = 161
lifted reachabiltiy analysis               = 31
grounded planning graph analysis           = 533
grounded task decomposition graph analysis = 868
grounding                                  = 144
create artificial top task                 = 5
============ sat ============
total                                        = 2704
generate formula                             = 1780
generate path decomposition tree             = 283
normalise path decomposition tree            = 149
translate path decomposition tree to clauses = 677
transform to DIMACS                          = 487
SAT solver                                   = 380
SAT solver for K=0003                        = 10
SAT solver for K=0004                        = 30
SAT solver for K=0005                        = 340

#1 "40 sat:90:solved"="true";"30 problem:05:number of primitive actions"="770";"30 problem:01:number of constants"="0";"30 problem:04:number of abstract actions"="316";"02 properties:04:tail recursive"="false";"00 global:80:peak memory"="771024896";"40 sat:20:state formula"="80130";"40 sat:01:number of variables"="62959";"40 sat:14:K offset"="0";"40 sat:30:number of paths"="120";"40 sat:00:plan length"="-1";"40 sat:50:number of ordering clauses"="0";"02 properties:02:mostly acyclic"="false";"30 problem:06:number of methods"="1166";"02 properties:05:totally ordered"="true";"02 properties:06:last task in all methods"="true";"30 problem:03:number of actions"="1086";"30 problem:02:number of predicates"="195";"40 sat:03:number of horn"="523359";"40 sat:15:K chosen value"="5";"02 properties:03:regular"="false";"40 sat:03:average size of clauses"="4.132662669638725";"40 sat:02:number of clauses"="563919";"40 sat:50:number of state clauses"="80130";"40 sat:03:number of assert"="26";"40 sat:22:method children clauses"="0";"00 global:90:planner result"="SOLUTION";"02 properties:01:acyclic"="false";"40 sat:31:maximum plan length"="120";"40 sat:50:number of decomposition clauses"="483789";"00 global:02:randomseed"="42";"40 sat:91:timeout"="false";"01 parsing:01:file parser"="439";"40 sat:41:SAT solver for K=0004"="30";"40 sat:00:total"="2704";"40 sat:20:transform to DIMACS"="487";"40 sat:11:generate path decomposition tree"="283";"02 preprocessing:07:compile methods with identical tasks"="1";"01 parsing:04:shop methods"="2";"02 preprocessing:08:removing unnecessary predicates"="161";"01 parsing:03:closed world assumption"="47";"02 preprocessing:11:lifted reachabiltiy analysis"="31";"01 parsing:02:sort expansion"="99";"40 sat:12:normalise path decomposition tree"="149";"40 sat:40:SAT solver"="380";"01 parsing:00:total"="660";"02 preprocessing:06:expand choiceless abstract tasks"="10";"01 parsing:06:strip domain of hybridity"="37";"40 sat:13:translate path decomposition tree to clauses"="677";"40 sat:10:generate formula"="1780";"40 sat:41:SAT solver for K=0005"="340";"02 preprocessing:01:compile negative preconditions"="14";"00 total:00:total"="6231";"02 preprocessing:12:grounded planning graph analysis"="533";"02 preprocessing:02:compile unit methods"="0";"02 preprocessing:23:grounded task decomposition graph analysis"="868";"02 preprocessing:04:split parameter"="19";"01 parsing:07:flatten formula"="16";"02 preprocessing:05:expand choiceless abstract tasks"="34";"40 sat:41:SAT solver for K=0003"="10";"02 preprocessing:00:total"="1831";"02 preprocessing:99:create artificial top task"="5";"01 parsing:05:eliminate identical variables"="17";"02 preprocessing:84:grounding"="144"
SOLUTION SEQUENCE
0: drive(truck-1,city-loc-3,city-loc-13)
1: drive(truck-1,city-loc-13,city-loc-8)
2: drive(truck-1,city-loc-8,city-loc-5)
3: pick-up(truck-1,city-loc-5,package-0,capacity-1,capacity-2)
4: drive(truck-1,city-loc-5,city-loc-8)
5: drive(truck-1,city-loc-8,city-loc-4)
6: drive(truck-1,city-loc-4,city-loc-9)
7: drop(truck-1,city-loc-9,package-0,capacity-1,capacity-2)
8: drive(truck-1,city-loc-9,city-loc-11)
9: drive(truck-1,city-loc-11,city-loc-2)
10: pick-up(truck-1,city-loc-2,package-1,capacity-1,capacity-2)
11: drive(truck-1,city-loc-2,city-loc-8)
12: drive(truck-1,city-loc-8,city-loc-2)
13: drive(truck-1,city-loc-2,city-loc-0)
14: drop(truck-1,city-loc-0,package-1,capacity-1,capacity-2)
15: drive(truck-3,city-loc-3,city-loc-11)
16: pick-up(truck-3,city-loc-11,package-2,capacity-1,capacity-2)
17: drive(truck-3,city-loc-11,city-loc-2)
18: drive(truck-3,city-loc-2,city-loc-8)
19: drive(truck-3,city-loc-8,city-loc-8)
20: drop(truck-3,city-loc-8,package-2,capacity-1,capacity-2)
21: drive(truck-4,city-loc-12,city-loc-10)
22: drive(truck-4,city-loc-10,city-loc-8)
23: pick-up(truck-4,city-loc-8,package-3,capacity-1,capacity-2)
24: drive(truck-4,city-loc-8,city-loc-2)
25: drive(truck-4,city-loc-2,city-loc-6)
26: drop(truck-4,city-loc-6,package-3,capacity-1,capacity-2)
27: drive(truck-3,city-loc-8,city-loc-4)
28: drive(truck-3,city-loc-4,city-loc-7)
29: pick-up(truck-3,city-loc-7,package-4,capacity-1,capacity-2)
30: drive(truck-3,city-loc-7,city-loc-4)
31: drive(truck-3,city-loc-4,city-loc-8)
32: drive(truck-3,city-loc-8,city-loc-13)
33: drop(truck-3,city-loc-13,package-4,capacity-1,capacity-2)
34: drive(truck-3,city-loc-13,city-loc-8)
35: drive(truck-3,city-loc-8,city-loc-14)
36: drive(truck-3,city-loc-14,city-loc-0)
37: pick-up(truck-3,city-loc-0,package-5,capacity-1,capacity-2)
38: drive(truck-3,city-loc-0,city-loc-2)
39: drive(truck-3,city-loc-2,city-loc-11)
40: drive(truck-3,city-loc-11,city-loc-9)
41: drop(truck-3,city-loc-9,package-5,capacity-1,capacity-2)
42: drive(truck-0,city-loc-12,city-loc-10)
43: drive(truck-0,city-loc-10,city-loc-3)
44: drive(truck-0,city-loc-3,city-loc-13)
45: pick-up(truck-0,city-loc-13,package-6,capacity-1,capacity-2)
46: drive(truck-0,city-loc-13,city-loc-3)
47: drive(truck-0,city-loc-3,city-loc-11)
48: drive(truck-0,city-loc-11,city-loc-1)
49: drop(truck-0,city-loc-1,package-6,capacity-1,capacity-2)
50: noop(truck-4,city-loc-6)
51: pick-up(truck-4,city-loc-6,package-7,capacity-1,capacity-2)
52: drive(truck-4,city-loc-6,city-loc-2)
53: drive(truck-4,city-loc-2,city-loc-8)
54: drop(truck-4,city-loc-8,package-7,capacity-1,capacity-2)
55: drive(truck-2,city-loc-7,city-loc-4)
56: drive(truck-2,city-loc-4,city-loc-8)
57: drive(truck-2,city-loc-8,city-loc-5)
58: pick-up(truck-2,city-loc-5,package-8,capacity-1,capacity-2)
59: drive(truck-2,city-loc-5,city-loc-8)
60: drive(truck-2,city-loc-8,city-loc-13)
61: drive(truck-2,city-loc-13,city-loc-11)
62: drop(truck-2,city-loc-11,package-8,capacity-1,capacity-2)
63: drive(truck-2,city-loc-11,city-loc-2)
64: drive(truck-2,city-loc-2,city-loc-0)
65: pick-up(truck-2,city-loc-0,package-9,capacity-1,capacity-2)
66: drive(truck-2,city-loc-0,city-loc-14)
67: drive(truck-2,city-loc-14,city-loc-12)
68: drive(truck-2,city-loc-12,city-loc-12)
69: drop(truck-2,city-loc-12,package-9,capacity-1,capacity-2)
70: drive(truck-4,city-loc-8,city-loc-13)
71: drive(truck-4,city-loc-13,city-loc-11)
72: drive(truck-4,city-loc-11,city-loc-3)
73: pick-up(truck-4,city-loc-3,package-10,capacity-1,capacity-2)
74: drive(truck-4,city-loc-3,city-loc-8)
75: drive(truck-4,city-loc-8,city-loc-4)
76: drop(truck-4,city-loc-4,package-10,capacity-1,capacity-2)
77: drive(truck-4,city-loc-4,city-loc-8)
78: drive(truck-4,city-loc-8,city-loc-2)
79: drive(truck-4,city-loc-2,city-loc-6)
80: pick-up(truck-4,city-loc-6,package-11,capacity-1,capacity-2)
81: drive(truck-4,city-loc-6,city-loc-2)
82: drive(truck-4,city-loc-2,city-loc-11)
83: drive(truck-4,city-loc-11,city-loc-1)
84: drop(truck-4,city-loc-1,package-11,capacity-1,capacity-2)
85: drive(truck-3,city-loc-9,city-loc-11)
86: drive(truck-3,city-loc-11,city-loc-2)
87: drive(truck-3,city-loc-2,city-loc-8)
88: pick-up(truck-3,city-loc-8,package-12,capacity-1,capacity-2)
89: drive(truck-3,city-loc-8,city-loc-2)
90: drive(truck-3,city-loc-2,city-loc-6)
91: drop(truck-3,city-loc-6,package-12,capacity-1,capacity-2)
92: drive(truck-0,city-loc-1,city-loc-3)
93: drive(truck-0,city-loc-3,city-loc-8)
94: drive(truck-0,city-loc-8,city-loc-5)
95: pick-up(truck-0,city-loc-5,package-13,capacity-1,capacity-2)
96: drive(truck-0,city-loc-5,city-loc-8)
97: drive(truck-0,city-loc-8,city-loc-14)
98: drive(truck-0,city-loc-14,city-loc-0)
99: drop(truck-0,city-loc-0,package-13,capacity-1,capacity-2)
100: drive(truck-3,city-loc-6,city-loc-2)
101: drive(truck-3,city-loc-2,city-loc-11)
102: drive(truck-3,city-loc-11,city-loc-9)
103: pick-up(truck-3,city-loc-9,package-14,capacity-1,capacity-2)
104: drive(truck-3,city-loc-9,city-loc-4)
105: drive(truck-3,city-loc-4,city-loc-8)
106: drive(truck-3,city-loc-8,city-loc-5)
107: drop(truck-3,city-loc-5,package-14,capacity-1,capacity-2)
