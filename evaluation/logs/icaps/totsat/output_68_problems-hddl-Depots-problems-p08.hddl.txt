PANDA - Planning and Acting in a Network Decomposition Architecture
Believe us: It's great, it's fantastic!

PANDA Copyright (C) 2014-2018 Gregor Behnke, Pascal Bercher, Thomas Geier, Kadir
Dede, Daniel Höller, Kristof Mickeleit, Matthias Englert
This program comes with ABSOLUTELY NO WARRANTY
This is free software, and you are welcome to redistribute it under certain
conditions; run PANDA with -license for details.

Main Developers:
- Gregor Behnke, http://www.uni-ulm.de/in/ki/behnke
- Daniel Höller, http://www.uni-ulm.de/in/ki/hoeller

With many thanks to various further contributors.
Run PANDA with the command line argument -contributors for an extensive list.

Run it with -help for more information like available options.


PANDA was called with: "-systemConfig AAAI-2018-totSAT(minisat) -programPath minisat=./minisat problems/hddl/Depots/domains/domain.hddl problems/hddl/Depots/problems/p08.hddl"


Planner Configuration
=====================
Domain: problems/hddl/Depots/domains/domain.hddl
Problem: problems/hddl/Depots/problems/p08.hddl
Output: none

Planning Configuration
======================
	printGeneralInformation : true
	printAdditionalData     : true
	random seed             : 42
	time limit (in seconds) : none

	external programs:
		minisat : ./minisat

	Parsing Configuration
	---------------------
	Parser                : autodetect file-type
	Expand Sort Hierarchy : true
	ClosedWordAssumption  : true
	CompileSHOPMethods    : true
	Eliminate Equality    : true
	Strip Hybridity       : true
	Reduce General Tasks  : true
	
	Preprocessing Configuration
	---------------------------
	Compile negative preconditions    : true
	Compile unit methods              : false
	Compile order in methods          : false
	Compile initial plan              : true
	Ensure Methods Have Last Task     : false
	Split independent parameters      : true
	Remove unnecessary predicates     : true
	Expand choiceless abstract tasks  : true
	Domain Cleanup                    : true
	Convert to SAS+                   : false
	Grounded Reachability Analysis    : Planning Graph (mutex-free)
	Grounded Task Decomposition Graph : Two Way TDG
	Iterate reachability analysis     : true
	Ground domain                     : true
	Iterate reachability analysis     : true
	Stop directly after grounding     : false
	
	SAT-Planning Configuration
	--------------------------
	solver           : minisat
	full planner run : true
	reduction method : only normalise 
	check result     : true
	
	Post-processing Configuration
	-----------------------------
	search status
	search result
	timings
	statistics
#0 "00 global:01:problem"="p08.hddl";"00 global:00:domain"="domain.hddl"
Parsing domain ... using HDDL parser ... done
Preparing internal domain representation ... done.
Initial domain
	number of abstract tasks = 6
	number of tasks = 23
	number of decomposition methods = 12
	number of predicates = 6
	number of sorts = 10
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 17
	number of constants = 24
Compiling negative preconditions ... done.
	number of abstract tasks = 6
	number of tasks = 23
	number of decomposition methods = 12
	number of predicates = 12
	number of sorts = 10
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 17
	number of constants = 24
Compiling split parameters ... done.
	number of abstract tasks = 7
	number of tasks = 24
	number of decomposition methods = 13
	number of predicates = 12
	number of sorts = 10
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 17
	number of constants = 24
Lifted reachability analysis and domain cleanup ... done.
	number of abstract tasks = 7
	number of tasks = 24
	number of decomposition methods = 13
	number of predicates = 8
	number of sorts = 10
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 17
	number of constants = 24
Grounded planning graph ... done.
	Number of Grounded Actions 317
	Number of Grounded Literals 270
	number of abstract tasks = 7
	number of tasks = 24
	number of decomposition methods = 13
	number of predicates = 8
	number of sorts = 10
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 17
	number of constants = 24
Two Way TDG ... done.
	number of abstract tasks = 7
	number of tasks = 24
	number of decomposition methods = 13
	number of predicates = 8
	number of sorts = 10
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 17
	number of constants = 24
Grounding ... done.
	number of abstract tasks = 111
	number of tasks = 338
	number of decomposition methods = 240
	number of predicates = 156
	number of sorts = 0
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 227
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 111
	number of tasks = 338
	number of decomposition methods = 240
	number of predicates = 129
	number of sorts = 0
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 227
	number of constants = 0
Omitting lifted reachability analysis ... 
Grounded planning graph ... done.
	Number of Grounded Actions 227
	Number of Grounded Literals 243
	number of abstract tasks = 111
	number of tasks = 338
	number of decomposition methods = 240
	number of predicates = 129
	number of sorts = 0
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 227
	number of constants = 0
Two Way TDG ... done.
	number of abstract tasks = 111
	number of tasks = 338
	number of decomposition methods = 240
	number of predicates = 129
	number of sorts = 0
	number of tasks in largest method = 5
	number of epsilon methods = 0
	number of primitive tasks = 227
	number of constants = 0
Compiling expand choiceless abstract tasks ... done.
	number of abstract tasks = 39
	number of tasks = 266
	number of decomposition methods = 168
	number of predicates = 129
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 227
	number of constants = 0
Compiling abstract tasks without methods ... done.
	number of abstract tasks = 39
	number of tasks = 266
	number of decomposition methods = 168
	number of predicates = 129
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 227
	number of constants = 0
Compiling initial plan ... done.
	number of abstract tasks = 40
	number of tasks = 267
	number of decomposition methods = 169
	number of predicates = 129
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 227
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 40
	number of tasks = 267
	number of decomposition methods = 169
	number of predicates = 129
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 227
	number of constants = 0
Omitting lifted reachability analysis ... 
Grounded planning graph ... done.
	Number of Grounded Actions 227
	Number of Grounded Literals 243
	number of abstract tasks = 40
	number of tasks = 267
	number of decomposition methods = 169
	number of predicates = 129
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 227
	number of constants = 0
Two Way TDG ... done.
	number of abstract tasks = 40
	number of tasks = 267
	number of decomposition methods = 169
	number of predicates = 129
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 227
	number of constants = 0
Compiling expand choiceless abstract tasks ... done.
	number of abstract tasks = 39
	number of tasks = 266
	number of decomposition methods = 168
	number of predicates = 129
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 227
	number of constants = 0
Compiling abstract tasks without methods ... done.
	number of abstract tasks = 39
	number of tasks = 266
	number of decomposition methods = 168
	number of predicates = 129
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 227
	number of constants = 0
Compiling initial plan ... done.
	number of abstract tasks = 40
	number of tasks = 267
	number of decomposition methods = 169
	number of predicates = 129
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 227
	number of constants = 0
Removing unnecessary predicates ... done.
	number of abstract tasks = 40
	number of tasks = 267
	number of decomposition methods = 169
	number of predicates = 129
	number of sorts = 0
	number of tasks in largest method = 11
	number of epsilon methods = 0
	number of primitive tasks = 227
	number of constants = 0
Tasks 227 - 1
Domain is acyclic: false
Domain is mostly acyclic: false
Domain is regular: false
Domain is tail recursive: false
Domain is totally ordered: true
Domain has last task in all methods: true
Time remaining for planner 9223372036854773755ms

Running SAT search with K = 0
Time remaining for SAT search 9223372036854773739ms
Time used for this run 9223372036854773739ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 137
NUMBER OF STATE CLAUSES 136
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 99.27% 0.73% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID dc0e008b-685f-4546-a8e6-75334d941fa0
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
Still waiting ... running for 103 will abort at 9223372036854773739
ERROR false

Running SAT search with K = 1
Time remaining for SAT search 9223372036854773632ms
Time used for this run 9223372036854773632ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 137
NUMBER OF STATE CLAUSES 136
NUMBER OF DECOMPOSITION CLAUSES 1
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 99.27% 0.73% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 4db9b97b-3562-4e66-92df-836b8e16f6d9
FLUSH
CLOSE
NUMBER OF PATHS 0
Problem is trivially unsatisfiable ... exiting
Removing files ... 
Still waiting ... running for 100 will abort at 9223372036854773632
ERROR false

Running SAT search with K = 2
Time remaining for SAT search 9223372036854773531ms
Time used for this run 9223372036854773531ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
NUMBER OF CLAUSES 3843
NUMBER OF STATE CLAUSES 3755
NUMBER OF DECOMPOSITION CLAUSES 88
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 97.71000000000001% 2.29% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 7614d896-99b7-4edf-b6bf-985749308cdc
FLUSH
CLOSE
NUMBER OF PATHS 14
Starting minisat
Setting starttime of solver to 1542620233336
Still waiting ... running for 100 will abort at 9223372036854773531
Command exited with non-zero status 20
0.00 0.00

Time command gave the following runtime for the solver: 0
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... done
SAT-Solver says: UNSAT
ERROR false

Running SAT search with K = 3
Time remaining for SAT search 9223372036854773307ms
Time used for this run 9223372036854773307ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
Still waiting ... running for 100 will abort at 9223372036854773307
NUMBER OF CLAUSES 29091
NUMBER OF STATE CLAUSES 24890
NUMBER OF DECOMPOSITION CLAUSES 4201
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 85.56% 14.44% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 851e1925-b298-4430-b1d0-e70481cca14e
FLUSH
CLOSE
NUMBER OF PATHS 91
Starting minisat
Setting starttime of solver to 1542620233654
Command exited with non-zero status 20
0.00 0.00

Time command gave the following runtime for the solver: 0
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... done
SAT-Solver says: UNSAT
ERROR false

Running SAT search with K = 4
Time remaining for SAT search 9223372036854772986ms
Time used for this run 9223372036854772986ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
Still waiting ... running for 100 will abort at 9223372036854772986
NUMBER OF CLAUSES 60712
NUMBER OF STATE CLAUSES 51355
NUMBER OF DECOMPOSITION CLAUSES 9357
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 84.59% 15.41% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID fc9889d0-b098-481a-9a23-39c93fcfb0a8
FLUSH
CLOSE
NUMBER OF PATHS 188
Starting minisat
Setting starttime of solver to 1542620234006
Command exited with non-zero status 20
0.01 0.00

Time command gave the following runtime for the solver: 10
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... done
SAT-Solver says: UNSAT
ERROR false

Running SAT search with K = 5
Time remaining for SAT search 9223372036854772665ms
Time used for this run 9223372036854772665ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
Still waiting ... running for 100 will abort at 9223372036854772665
NUMBER OF CLAUSES 79038
NUMBER OF STATE CLAUSES 67587
NUMBER OF DECOMPOSITION CLAUSES 11451
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 85.51% 14.49% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 2846aff1-b73d-448c-9f8d-36c0e632bc2c
FLUSH
CLOSE
NUMBER OF PATHS 248
Starting minisat
Setting starttime of solver to 1542620234349
Command exited with non-zero status 20
0.01 0.00

Time command gave the following runtime for the solver: 10
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... done
SAT-Solver says: UNSAT
ERROR false

Running SAT search with K = 6
Time remaining for SAT search 9223372036854772347ms
Time used for this run 9223372036854772347ms


Generating initial PDT ... done
Checking whether the PDT can grow any more ... yes ... done
Normalising and optimising PDT ... done
Assignment implications
Generating clauses representing decomposition ... done
Still waiting ... running for 100 will abort at 9223372036854772347
NUMBER OF CLAUSES 94221
NUMBER OF STATE CLAUSES 81095
NUMBER OF DECOMPOSITION CLAUSES 13126
NUMBER OF ORDER CLAUSES 0
PERCENTAGES 86.07000000000001% 13.93% 0.0% 
					Running with plan length -1 lo 1 -1
READY TO WRITE
UUID 10ce3fcb-06f2-4c16-8394-00c73899888b
FLUSH
CLOSE
NUMBER OF PATHS 298
Starting minisat
Setting starttime of solver to 1542620234685
Command exited with non-zero status 10
0.03 0.00

Time command gave the following runtime for the solver: 30
Logging statistical information about the run ... done
Reading solver output ... done
Preparing solver output ... done
SAT-Solver says: SAT

extracting solution


CHECKING primitive solution of length 99 ...
true 39 SHOP_methodm10_do_load_truck_10_precondition[crate9,distributor1,pallet2,hoist2]
true 147 SHOP_methodm7_do_get_truck_7_precondition[truck1,distributor1]
true 35 Drive[truck1,distributor0,distributor1]
true 137 SHOP_methodm4_do_clear_4_precondition[crate9,distributor1]
true 22 nop[]
true 127 Lift[hoist2,crate9,pallet2,distributor1]
true 73 Load[hoist2,crate9,truck1,distributor1]
true 93 Drive[truck1,distributor1,depot0]
true 43 SHOP_methodm11_do_unload_truck_11_precondition[crate9,depot0,truck1,hoist0,pallet0]
true 46 SHOP_methodm5_do_clear_5_precondition[pallet0,crate2,depot0,hoist0]
true 57 SHOP_methodm4_do_clear_4_precondition[crate2,depot0]
true 110 nop[]
true 95 Lift[hoist0,crate2,pallet0,depot0]
true 130 SHOP_methodm7_do_get_truck_7_precondition[truck0,depot0]
true 11 Drive[truck0,distributor0,depot0]
true 71 Load[hoist0,crate2,truck0,depot0]
true 134 Unload[hoist0,crate9,truck1,depot0]
true 109 Drop[hoist0,crate9,pallet0,depot0]
true 59 SHOP_methodm10_do_load_truck_10_precondition[crate7,distributor1,crate4,hoist2]
true 89 SHOP_methodm7_do_get_truck_7_precondition[truck1,distributor1]
true 55 Drive[truck1,depot0,distributor1]
true 77 SHOP_methodm4_do_clear_4_precondition[crate7,distributor1]
true 51 nop[]
true 6 Lift[hoist2,crate7,crate4,distributor1]
true 31 Load[hoist2,crate7,truck1,distributor1]
true 32 Drive[truck1,distributor1,distributor0]
true 45 SHOP_methodm11_do_unload_truck_11_precondition[crate7,distributor0,truck1,hoist1,pallet1]
true 84 SHOP_methodm5_do_clear_5_precondition[pallet1,crate1,distributor0,hoist1]
true 60 SHOP_methodm5_do_clear_5_precondition[crate1,crate5,distributor0,hoist1]
true 2 SHOP_methodm5_do_clear_5_precondition[crate5,crate6,distributor0,hoist1]
true 94 SHOP_methodm4_do_clear_4_precondition[crate6,distributor0]
true 142 nop[]
true 87 Lift[hoist1,crate6,crate5,distributor0]
true 66 SHOP_methodm6_do_get_truck_6_precondition[truck1,distributor0]
true 74 nop[]
true 24 Load[hoist1,crate6,truck1,distributor0]
true 124 Lift[hoist1,crate5,crate1,distributor0]
true 150 SHOP_methodm6_do_get_truck_6_precondition[truck1,distributor0]
true 152 nop[]
true 103 Load[hoist1,crate5,truck1,distributor0]
true 53 Lift[hoist1,crate1,pallet1,distributor0]
true 17 SHOP_methodm6_do_get_truck_6_precondition[truck1,distributor0]
true 65 nop[]
true 69 Load[hoist1,crate1,truck1,distributor0]
true 108 Unload[hoist1,crate7,truck1,distributor0]
true 145 Drop[hoist1,crate7,pallet1,distributor0]
true 138 SHOP_methodm2_do_put_on_2_precondition[crate6,truck1]
true 112 SHOP_methodm7_do_get_truck_7_precondition[truck1,distributor1]
true 28 Drive[truck1,distributor0,distributor1]
true 105 SHOP_methodm4_do_clear_4_precondition[pallet2,distributor1]
true 67 nop[]
true 0 Unload[hoist2,crate6,truck1,distributor1]
true 126 Drop[hoist2,crate6,pallet2,distributor1]
true 79 SHOP_methodm10_do_load_truck_10_precondition[crate0,distributor0,pallet4,hoist1]
true 121 SHOP_methodm7_do_get_truck_7_precondition[truck1,distributor0]
true 115 Drive[truck1,distributor1,distributor0]
true 68 SHOP_methodm4_do_clear_4_precondition[crate0,distributor0]
true 36 nop[]
true 97 Lift[hoist1,crate0,pallet4,distributor0]
true 85 Load[hoist1,crate0,truck1,distributor0]
true 82 Drive[truck1,distributor0,distributor1]
true 83 SHOP_methodm11_do_unload_truck_11_precondition[crate0,distributor1,truck1,hoist2,pallet3]
true 49 SHOP_methodm5_do_clear_5_precondition[pallet3,crate4,distributor1,hoist2]
true 10 SHOP_methodm4_do_clear_4_precondition[crate4,distributor1]
true 63 nop[]
true 107 Lift[hoist2,crate4,pallet3,distributor1]
true 128 SHOP_methodm7_do_get_truck_7_precondition[truck0,distributor1]
true 50 Drive[truck0,depot0,distributor1]
true 80 Load[hoist2,crate4,truck0,distributor1]
true 58 Unload[hoist2,crate0,truck1,distributor1]
true 42 Drop[hoist2,crate0,pallet3,distributor1]
true 98 SHOP_methodm1_do_put_on_1_precondition[crate8,distributor0]
true 141 SHOP_methodm4_do_clear_4_precondition[crate8,distributor0]
true 33 nop[]
true 34 SHOP_methodm4_do_clear_4_precondition[pallet4,distributor0]
true 101 nop[]
true 75 SHOP_methodm9_do_lift_crate_9_precondition[crate8,crate3,distributor0,hoist1]
true 146 Lift[hoist1,crate8,crate3,distributor0]
true 56 Drop[hoist1,crate8,pallet4,distributor0]
true 21 SHOP_methodm10_do_load_truck_10_precondition[crate3,distributor0,pallet5,hoist1]
true 131 SHOP_methodm7_do_get_truck_7_precondition[truck0,distributor0]
true 27 Drive[truck0,distributor1,distributor0]
true 90 SHOP_methodm4_do_clear_4_precondition[crate3,distributor0]
true 18 nop[]
true 9 Lift[hoist1,crate3,pallet5,distributor0]
true 88 Load[hoist1,crate3,truck0,distributor0]
true 3 Drive[truck0,distributor0,distributor0]
true 72 SHOP_methodm11_do_unload_truck_11_precondition[crate3,distributor0,truck0,hoist1,crate8]
true 116 SHOP_methodm4_do_clear_4_precondition[crate8,distributor0]
true 37 nop[]
true 13 Unload[hoist1,crate3,truck0,distributor0]
true 64 Drop[hoist1,crate3,crate8,distributor0]
true 104 SHOP_methodm2_do_put_on_2_precondition[crate1,truck1]
true 144 SHOP_methodm6_do_get_truck_6_precondition[truck1,distributor1]
true 118 nop[]
true 120 SHOP_methodm4_do_clear_4_precondition[crate0,distributor1]
true 102 nop[]
true 81 Unload[hoist2,crate1,truck1,distributor1]
true 47 Drop[hoist2,crate1,crate0,distributor1]
 done.
ERROR false
Panda says: SOLUTION
============ global ============
randomseed     = 42
peak memory    = 641728512
planner result = SOLUTION
============ properties ============
acyclic                  = false
mostly acyclic           = false
regular                  = false
tail recursive           = false
totally ordered          = true
last task in all methods = true
============ problem ============
number of constants         = 0
number of predicates        = 129
number of actions           = 267
number of abstract actions  = 40
number of primitive actions = 227
number of methods           = 169
============ sat ============
plan length                     = -1
number of variables             = 41822
number of clauses               = 94221
average size of clauses         = 2.0830388130034705
number of assert                = 53
number of horn                  = 90679
K offset                        = 0
K chosen value                  = 6
state formula                   = 81095
method children clauses         = 0
number of paths                 = 298
maximum plan length             = 298
number of decomposition clauses = 13126
number of ordering clauses      = 0
number of state clauses         = 81095
solved                          = true
timeout                         = false

----------------- TIMINGS -----------------
============ total ============
total = 4080
============ parsing ============
total                         = 640
file parser                   = 414
sort expansion                = 92
closed world assumption       = 45
shop methods                  = 9
eliminate identical variables = 20
strip domain of hybridity     = 36
flatten formula               = 24
============ preprocessing ============
total                                      = 1353
compile negative preconditions             = 18
compile unit methods                       = 0
split parameter                            = 20
expand choiceless abstract tasks           = 124
expand choiceless abstract tasks           = 3
compile methods with identical tasks       = 0
removing unnecessary predicates            = 94
lifted reachabiltiy analysis               = 40
grounded planning graph analysis           = 355
grounded task decomposition graph analysis = 587
grounding                                  = 99
create artificial top task                 = 1
============ sat ============
total                                        = 1194
generate formula                             = 929
generate path decomposition tree             = 139
normalise path decomposition tree            = 66
translate path decomposition tree to clauses = 175
transform to DIMACS                          = 151
SAT solver                                   = 50
SAT solver for K=0002                        = 0
SAT solver for K=0003                        = 0
SAT solver for K=0004                        = 10
SAT solver for K=0005                        = 10
SAT solver for K=0006                        = 30

#1 "40 sat:90:solved"="true";"30 problem:05:number of primitive actions"="227";"30 problem:01:number of constants"="0";"30 problem:04:number of abstract actions"="40";"02 properties:04:tail recursive"="false";"00 global:80:peak memory"="641728512";"40 sat:20:state formula"="81095";"40 sat:01:number of variables"="41822";"40 sat:14:K offset"="0";"40 sat:30:number of paths"="298";"40 sat:00:plan length"="-1";"40 sat:50:number of ordering clauses"="0";"02 properties:02:mostly acyclic"="false";"30 problem:06:number of methods"="169";"02 properties:05:totally ordered"="true";"02 properties:06:last task in all methods"="true";"30 problem:03:number of actions"="267";"30 problem:02:number of predicates"="129";"40 sat:03:number of horn"="90679";"40 sat:15:K chosen value"="6";"02 properties:03:regular"="false";"40 sat:03:average size of clauses"="2.0830388130034705";"40 sat:02:number of clauses"="94221";"40 sat:50:number of state clauses"="81095";"40 sat:03:number of assert"="53";"40 sat:22:method children clauses"="0";"00 global:90:planner result"="SOLUTION";"02 properties:01:acyclic"="false";"40 sat:31:maximum plan length"="298";"40 sat:50:number of decomposition clauses"="13126";"00 global:02:randomseed"="42";"40 sat:91:timeout"="false";"01 parsing:01:file parser"="414";"40 sat:41:SAT solver for K=0004"="10";"40 sat:00:total"="1194";"40 sat:20:transform to DIMACS"="151";"40 sat:11:generate path decomposition tree"="139";"02 preprocessing:07:compile methods with identical tasks"="0";"01 parsing:04:shop methods"="9";"02 preprocessing:08:removing unnecessary predicates"="94";"01 parsing:03:closed world assumption"="45";"02 preprocessing:11:lifted reachabiltiy analysis"="40";"01 parsing:02:sort expansion"="92";"40 sat:12:normalise path decomposition tree"="66";"40 sat:40:SAT solver"="50";"01 parsing:00:total"="640";"02 preprocessing:06:expand choiceless abstract tasks"="3";"01 parsing:06:strip domain of hybridity"="36";"40 sat:13:translate path decomposition tree to clauses"="175";"40 sat:10:generate formula"="929";"40 sat:41:SAT solver for K=0005"="10";"40 sat:41:SAT solver for K=0002"="0";"02 preprocessing:01:compile negative preconditions"="18";"00 total:00:total"="4080";"02 preprocessing:12:grounded planning graph analysis"="355";"02 preprocessing:02:compile unit methods"="0";"02 preprocessing:23:grounded task decomposition graph analysis"="587";"40 sat:41:SAT solver for K=0006"="30";"02 preprocessing:04:split parameter"="20";"01 parsing:07:flatten formula"="24";"02 preprocessing:05:expand choiceless abstract tasks"="124";"40 sat:41:SAT solver for K=0003"="0";"02 preprocessing:00:total"="1353";"02 preprocessing:99:create artificial top task"="1";"01 parsing:05:eliminate identical variables"="20";"02 preprocessing:84:grounding"="99"
SOLUTION SEQUENCE
0: SHOP_methodm10_do_load_truck_10_precondition(crate9,distributor1,pallet2,hoist2)
1: SHOP_methodm7_do_get_truck_7_precondition(truck1,distributor1)
2: Drive(truck1,distributor0,distributor1)
3: SHOP_methodm4_do_clear_4_precondition(crate9,distributor1)
4: nop()
5: Lift(hoist2,crate9,pallet2,distributor1)
6: Load(hoist2,crate9,truck1,distributor1)
7: Drive(truck1,distributor1,depot0)
8: SHOP_methodm11_do_unload_truck_11_precondition(crate9,depot0,truck1,hoist0,pallet0)
9: SHOP_methodm5_do_clear_5_precondition(pallet0,crate2,depot0,hoist0)
10: SHOP_methodm4_do_clear_4_precondition(crate2,depot0)
11: nop()
12: Lift(hoist0,crate2,pallet0,depot0)
13: SHOP_methodm7_do_get_truck_7_precondition(truck0,depot0)
14: Drive(truck0,distributor0,depot0)
15: Load(hoist0,crate2,truck0,depot0)
16: Unload(hoist0,crate9,truck1,depot0)
17: Drop(hoist0,crate9,pallet0,depot0)
18: SHOP_methodm10_do_load_truck_10_precondition(crate7,distributor1,crate4,hoist2)
19: SHOP_methodm7_do_get_truck_7_precondition(truck1,distributor1)
20: Drive(truck1,depot0,distributor1)
21: SHOP_methodm4_do_clear_4_precondition(crate7,distributor1)
22: nop()
23: Lift(hoist2,crate7,crate4,distributor1)
24: Load(hoist2,crate7,truck1,distributor1)
25: Drive(truck1,distributor1,distributor0)
26: SHOP_methodm11_do_unload_truck_11_precondition(crate7,distributor0,truck1,hoist1,pallet1)
27: SHOP_methodm5_do_clear_5_precondition(pallet1,crate1,distributor0,hoist1)
28: SHOP_methodm5_do_clear_5_precondition(crate1,crate5,distributor0,hoist1)
29: SHOP_methodm5_do_clear_5_precondition(crate5,crate6,distributor0,hoist1)
30: SHOP_methodm4_do_clear_4_precondition(crate6,distributor0)
31: nop()
32: Lift(hoist1,crate6,crate5,distributor0)
33: SHOP_methodm6_do_get_truck_6_precondition(truck1,distributor0)
34: nop()
35: Load(hoist1,crate6,truck1,distributor0)
36: Lift(hoist1,crate5,crate1,distributor0)
37: SHOP_methodm6_do_get_truck_6_precondition(truck1,distributor0)
38: nop()
39: Load(hoist1,crate5,truck1,distributor0)
40: Lift(hoist1,crate1,pallet1,distributor0)
41: SHOP_methodm6_do_get_truck_6_precondition(truck1,distributor0)
42: nop()
43: Load(hoist1,crate1,truck1,distributor0)
44: Unload(hoist1,crate7,truck1,distributor0)
45: Drop(hoist1,crate7,pallet1,distributor0)
46: SHOP_methodm2_do_put_on_2_precondition(crate6,truck1)
47: SHOP_methodm7_do_get_truck_7_precondition(truck1,distributor1)
48: Drive(truck1,distributor0,distributor1)
49: SHOP_methodm4_do_clear_4_precondition(pallet2,distributor1)
50: nop()
51: Unload(hoist2,crate6,truck1,distributor1)
52: Drop(hoist2,crate6,pallet2,distributor1)
53: SHOP_methodm10_do_load_truck_10_precondition(crate0,distributor0,pallet4,hoist1)
54: SHOP_methodm7_do_get_truck_7_precondition(truck1,distributor0)
55: Drive(truck1,distributor1,distributor0)
56: SHOP_methodm4_do_clear_4_precondition(crate0,distributor0)
57: nop()
58: Lift(hoist1,crate0,pallet4,distributor0)
59: Load(hoist1,crate0,truck1,distributor0)
60: Drive(truck1,distributor0,distributor1)
61: SHOP_methodm11_do_unload_truck_11_precondition(crate0,distributor1,truck1,hoist2,pallet3)
62: SHOP_methodm5_do_clear_5_precondition(pallet3,crate4,distributor1,hoist2)
63: SHOP_methodm4_do_clear_4_precondition(crate4,distributor1)
64: nop()
65: Lift(hoist2,crate4,pallet3,distributor1)
66: SHOP_methodm7_do_get_truck_7_precondition(truck0,distributor1)
67: Drive(truck0,depot0,distributor1)
68: Load(hoist2,crate4,truck0,distributor1)
69: Unload(hoist2,crate0,truck1,distributor1)
70: Drop(hoist2,crate0,pallet3,distributor1)
71: SHOP_methodm1_do_put_on_1_precondition(crate8,distributor0)
72: SHOP_methodm4_do_clear_4_precondition(crate8,distributor0)
73: nop()
74: SHOP_methodm4_do_clear_4_precondition(pallet4,distributor0)
75: nop()
76: SHOP_methodm9_do_lift_crate_9_precondition(crate8,crate3,distributor0,hoist1)
77: Lift(hoist1,crate8,crate3,distributor0)
78: Drop(hoist1,crate8,pallet4,distributor0)
79: SHOP_methodm10_do_load_truck_10_precondition(crate3,distributor0,pallet5,hoist1)
80: SHOP_methodm7_do_get_truck_7_precondition(truck0,distributor0)
81: Drive(truck0,distributor1,distributor0)
82: SHOP_methodm4_do_clear_4_precondition(crate3,distributor0)
83: nop()
84: Lift(hoist1,crate3,pallet5,distributor0)
85: Load(hoist1,crate3,truck0,distributor0)
86: Drive(truck0,distributor0,distributor0)
87: SHOP_methodm11_do_unload_truck_11_precondition(crate3,distributor0,truck0,hoist1,crate8)
88: SHOP_methodm4_do_clear_4_precondition(crate8,distributor0)
89: nop()
90: Unload(hoist1,crate3,truck0,distributor0)
91: Drop(hoist1,crate3,crate8,distributor0)
92: SHOP_methodm2_do_put_on_2_precondition(crate1,truck1)
93: SHOP_methodm6_do_get_truck_6_precondition(truck1,distributor1)
94: nop()
95: SHOP_methodm4_do_clear_4_precondition(crate0,distributor1)
96: nop()
97: Unload(hoist2,crate1,truck1,distributor1)
98: Drop(hoist2,crate1,crate0,distributor1)
