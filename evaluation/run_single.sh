#!/bin/bash

memlimit=`cat memlimit_kb` # in kilobytes
timeout_seconds=`cat timeout_seconds`
simulate=false

function create_temp_dir() {
    i=$1
    mkdir -p proc_$i
    cd proc_$i
    mkdir -p logs
    for f in `cat ../necessary_files|tr '\n' ' '` ; do
        ln -s ../$f `basename $f`
    done
    cd ..
}

function run_instance() {
    command="$1"

    # Execute the command, write time info in file
    timeout_command="./timeout -t $timeout_seconds -s $memlimit $command"
    /usr/bin/time -o output_time $timeout_command #| awk '{print "'$line_idx':",$0}' #> /dev/null
}

# Retrieve the instance to execute
line_idx=$1
line=`sed -n ${line_idx}p full_eval_instances_randomized.csv`

# Create and switch to this processor's directory
create_temp_dir $line_idx
cd proc_$line_idx

# Parse line
line_words=( $line )    
problem_type=${line_words[0]}
problem_index=${line_words[1]}
solving_type=${line_words[2]}
other_params=${line_words[@]:3}

# Set application arguments
madagascar=true
non_htn="non_htn/"
if [ "$solving_type" == "M" ] ; then
        param_executable="./M"
elif [ "$solving_type" == "Mp" ]; then
    param_executable="./Mp"
elif [ "$solving_type" == "MpC" ]; then
    param_executable="./MpC"
elif [ "$solving_type" == "MpC_Incplan" ]; then
    param_executable="./MpC_Incplan"
else
    non_htn=""
    madagascar=false
    if [ "$solving_type" == "T-REX_isolated" ]; then
        param_formulafile=../../paramils/instances/$problem_type/${problem_index}.cnf
    else
        param_encoding=$solving_type
    fi
fi
param_domain=../../problems/${non_htn}$problem_type/domain.pddl
param_problem=../../problems/${non_htn}$problem_type/p${problem_index}.pddl

# Assemble command to execute
if [[ "$solving_type" == "MpC_Incplan" ]]; then
    command="./solve_madagascar_incremental.sh $param_executable $param_domain $param_problem $problem_type $problem_index $solving_type $other_params"
elif $madagascar; then
    command="./solve_madagascar.sh $param_executable $param_domain $param_problem $problem_type $problem_index $solving_type $other_params"
elif [[ "$solving_type" == "T-REX_isolated" ]]; then
    directory="logs/`date +%Y-%m-%d_%H-%M-%S_%N`_$solving_type"
    mkdir $directory
    command="./interpreter_t-rex -i $param_formulafile $other_params"
else
    command="java -Xmx10g -jar htnsat.jar -d $param_domain -p $param_problem -e $param_encoding $other_params"
fi

if ! $simulate ; then

    # Execute
    run_instance "$command" > general_output
    
    # Move logs
    dest_dir=logs/`ls -t logs/|head -1`
    echo $line > $dest_dir/args
    if [ -f general_output ]; then
        mv general_output $dest_dir/
    fi
    if [ -f output_time ]; then
        mv output_time $dest_dir/time
    fi
    if [ -f 04_solution.txt ]; then
        mv 04_solution.txt $dest_dir/
    fi
fi
