#!/bin/bash

command="./paramils -scenariofile trex_scenario.txt -userunlog 1 -init 0 -validN 100"
num_runs=1

seq 1 $num_runs | parallel $command -numRun {}
