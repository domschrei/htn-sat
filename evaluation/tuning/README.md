# T-REX parameter tuning 

In order to perform a T-REX parameter tuning with ParamILS, follow these steps:

* Get [ParamILS v2.3.5](http://www.cs.ubc.ca/labs/beta/Projects/ParamILS/paramils2.3.5-source.zip).
* Drop the executable `./paramils` in this directory.
* Generate the instances to test by executing `bash generate_instances.sh` (requiring the HTN-SAT application in `../../bin/htnsat.jar` and the problem files in `../../problems/`). The domains to test on can be changed by editing the file `domains`.
* Execute `bash run_tuning.sh` to launch the tuning procedure.
