#!/bin/bash

domains=`cat domains`
problem_dir="../../problems/"

> trex_instances.txt

for domain in $domains ; do

    mkdir -p instances/$domain
    domain_file=$problem_dir/$domain/domain.pddl
    echo $domain_file
    count=1
    
    for problem_file in $problem_dir/$domain/p*.pddl ; do
    
        java -jar ../../bin/htnsat.jar -Xmx10g -d $domain_file -p $problem_file -0 -e t-rex
        if [ -f f.cnf ]; then
            filename=instances/$domain/${count}.cnf
            mv f.cnf $filename
            echo $filename >> trex_instances.txt
        fi
        count=$((count+1))
    
    done

done
