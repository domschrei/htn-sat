#!/bin/bash

# input format: <algo_exec> <instance_name> <instance-specific info> <cutoff_time> <cutoff_length> <seed> <params>
instance_name="$1"
instance_info="$2"
cutoff_time="$3"
cutoff_length="$4"
seed="$5"
shift 5
params="$@"

# <params> e.g. like 
# -cumulative_amo 0 -full_prim_elem_encoding 0 ...

# ------------------------------------------
# Convert parameters to T-REX argument style
# ------------------------------------------

# Delete all "zero" (unset) parameters
params=`echo $params|sed 's#-[0-9a-z_]\+ 0##g'`

# Adapt all set parameters
params=`echo $params|sed 's#-init_layer_amo 1#-a#'`
params=`echo $params|sed 's#-binary_amo_threshold#-b#'`
params=`echo $params|sed 's#-encode_predecessors 1#-P#'`
params=`echo $params|sed 's#-full_fact_encoding 1#-F#'`
params=`echo $params|sed 's#-full_prim_elem_encoding 1#-e#'`
params=`echo $params|sed 's#-cumulative_amo 1#-c#'`
params=`echo $params|sed 's#-full_amo 1#-A#'`

echo $params > PARAMS_OUT

# ------------------------------------------
# Call t-rex
# ------------------------------------------

command="timeout $cutoff_time ../../bin/t-rex -i $instance_name $params"
$command > output_trex

# ------------------------------------------
# Evaluate the results
# ------------------------------------------

if grep -q '] Exiting.' output_trex ; then
    solved="SAT"
    runtime=`cat output_trex|grep '] Exiting.'|grep -oE "[0-9]+\.[0-9]+"`
    runlength=`cat output_trex|grep -oE "a solution has been found at makespan [0-9]+."|grep -oE "[0-9]+"`
    best_sol=0
else
    solved="TIMEOUT"
    runtime=$cutoff_time
    runlength=`cat output_trex|grep -oE "M a k e s p a n"|tail -1|grep -oE "[0-9]+"`
    best_sol=""
fi

# Output
echo "Result for ParamILS: $solved, $runtime, $runlength, $best_sol, $seed"

cat output_trex >> full_output
