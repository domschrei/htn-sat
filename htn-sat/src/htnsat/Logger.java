package htnsat;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Logs information into a given text file.
 * 
 * @author Dominik Schreiber
 */
public class Logger {

	/**
	 * Global property where to place all log files 
	 * during the execution of this program.
	 */
	private static String directory;
	
	/**
	 * Global initialization method to create the directory
	 * for all logging during the program's execution.
	 */
	public static void init(String... args) {
		SimpleDateFormat sdf = new SimpleDateFormat(
				"yyyy-MM-dd_HH-mm-ss_SSS");
		directory = "logs/" + sdf.format(new Date());
		for (String arg : args) {
			arg = arg.replaceAll("\\.\\.\\/", "");
			arg = arg.replaceAll("\\.", "-");
			arg = arg.replaceAll("\\/", "-");
			directory += "_" + arg;
		}
		directory += "/";
		new File(directory).mkdirs();
	}
	
	/**
	 * Name of the log file
	 */
	private String filename;
	/**
	 * FileWriter used to write into the log file
	 */
	private FileWriter writer;
	/**
	 * True, iff the log content should also be output to stdout
	 */
	private boolean verbose;
	/**
	 * Time stamp to output elapsed time
	 */
	private long time;
	
	private long roundTime;

	/**
	 * @param verbose if the log content should also be output 
	 * to stdout
	 */
	public Logger(boolean verbose) {
		
		this.verbose = verbose;
		startTimer();
		
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			@Override
			public void run() {
				close();
			}
		}));
	}

	/**
	 * Sets the name of the file (without directory specification)
	 * to write into. Must be provided before logging.
	 */
	public void setFilename(String filename) {
		
		this.filename = filename;
		try {				
			if (writer != null) {
				writer.close();
			}
			writer = new FileWriter(directory + filename);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Logs the specified string into the log file
	 * (and also to stdout, if <code>verbose</code>.
	 */
	public void print(String str) {
		
		if (verbose) {			
			System.out.println(str);
		}
		if (writer != null) {
			try {				
				writer.write(str + "\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Logs the specified string together with the elapsed 
	 * time since the creation of this object or the last 
	 * call of this method.
	 */
	public void printTimestamped(String str) {
		
		String reportStr = "> " + str 
				+ " (" + passedTime() + " ms)";
		print(reportStr);
	}
	
	/**
	 * Returns the path to the logfile 
	 * (i.e. directory and file name).
	 */
	public String getFile() {
		
		return directory + filename;
	}
	
	/**
	 * Returns the path of the directory
	 * where the logfile resides.
	 */
	public String getDirectory() {
		
		return directory;
	}
	
	/**
	 * Closes the FileWriter associated with this Logger.
	 * Should be called when the logging process has ended.
	 */
	public void close() {
		
		try {
			if (writer != null)
				writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {			
			writer = null;
		}
	}

	
	// Time measurement
	
	private void startTimer() {
		time = System.currentTimeMillis();
		roundTime = time;
	}
	private long passedTime() {
		long newTime = System.currentTimeMillis();
		long passedTime = newTime - roundTime;
		roundTime = newTime;
		return passedTime;
	}
	
	public long totalPassedTime() {
		long newTime = System.currentTimeMillis();
		long passedTime = newTime - time;
		return passedTime;
	}
}
