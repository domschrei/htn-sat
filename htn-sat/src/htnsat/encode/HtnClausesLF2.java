package htnsat.encode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import htnsat.htn.HtnAction;
import htnsat.htn.HtnMethod;
import htnsat.htn.HtnProblemData;
import htnsat.htn.HtnSimplifier;
import htnsat.sat.AtomDomain;
import htnsat.sat.AtomType;
import htnsat.sat.SatClause;
import htnsat.sat.SatDomain;
import htnsat.sat.SatFormula;
import htnsat.sat.SatUtils;
import htnsat.sat.SatUtils.AtMostOneMode;

/**
 * <b>L</b>inear <b>F</b>orward encoding
 * <br/><br/>
 * 
 * Complexity notation:
 * 
 * <ul>
 * <li>s: maximum steps</li>
 * <li>p: facts</li>
 * <li>a: primitive tasks</li>
 * <li>t: non-primitive tasks</li>
 * <li>c: constraints per network task / method</li>
 * <li>i: tasks of initial network</li>
 * </ul>
 * 
 * @author Dominik Schreiber
 *
 */
public class HtnClausesLF2 extends AbstractHtnClauses {
	
	private int numSteps;
	private AtomDomain domainNonprimTasks;
	private AtomDomain domainAllTasks;
	private AtomDomain domainSteps;
	private AtomDomain domainStepsButLast;
	private AtomDomain domainFacts;
	private AtomDomain domainReductions;
	private AtomDomain domainActions;
	
	private AtomType actionAtStep;
	private AtomType factAtStep;
	private AtomType taskStartsAtStep;
	private AtomType taskEndsAtStep;
	private AtomType taskReductionStartsAtStep;
	private AtomType taskReductionEndsAtStep;
	private AtomType subtaskOfReductionStartsAtStep;
	private AtomType subtaskOfReductionEndsAtStep;
	
	private SatFormula f;
	
	private SatClause currentClause;
	
	private HtnProblemData data;
	private int[] maxTaskLengths;
	
	public HtnClausesLF2(HtnProblemData data, int numSteps) {
		
		super(null, false);
		this.data = data;
		maxTaskLengths = HtnSimplifier.calculateMaxTaskLengths(data, numSteps);
		
		f = new SatFormula();
		
		this.numSteps = numSteps;
		
		domainNonprimTasks = new AtomDomain(0, data.getNonprimitiveTasks().size());
		domainAllTasks = new AtomDomain(0, data.getTasks().size());
		domainSteps = new AtomDomain(0, numSteps + 1);
		domainStepsButLast = new AtomDomain(0, numSteps);
		domainFacts = new AtomDomain(0, data.getFacts().size());
		domainReductions = new AtomDomain(0, data.getMaxReductionsPerTask());
		domainActions = new AtomDomain(0, data.getPrimitiveTasks().size());
		
		actionAtStep = new AtomType(AtomTag.ACTION_AT_STEP_AS_ACTION_IDX, 
				domainActions, domainSteps);
		factAtStep = new AtomType(AtomTag.FACT_AT_STEP, 
				domainFacts, domainSteps);
		taskStartsAtStep = new AtomType(AtomTag.TASK_STARTS_AT_STEP, 
				domainNonprimTasks, domainSteps);
		taskEndsAtStep = new AtomType(AtomTag.TASK_ENDS_AT_STEP,
				domainNonprimTasks, domainSteps);
		taskReductionStartsAtStep = new AtomType(domainNonprimTasks, 
				domainReductions, domainSteps);
		taskReductionEndsAtStep = new AtomType(domainNonprimTasks, 
				domainReductions, domainSteps);
		subtaskOfReductionStartsAtStep = new AtomType(domainNonprimTasks,
				domainReductions, domainSteps, domainAllTasks, domainSteps);
		subtaskOfReductionEndsAtStep = new AtomType(domainNonprimTasks,
				domainReductions, domainSteps, domainAllTasks, domainSteps);
		
		List<AtomType> atomTypes = Arrays.asList(actionAtStep, 
				factAtStep, 
				taskStartsAtStep,
				taskEndsAtStep,
				taskReductionStartsAtStep, 
				taskReductionEndsAtStep, 
				subtaskOfReductionStartsAtStep, 
				subtaskOfReductionEndsAtStep);
		d = new SatDomain(atomTypes);
	}
	
	/**
	 * Adds clauses to specify all facts that hold and 
	 * do not hold in the initial state.
	 * <br/><br/>O(f) clauses, O(1) literals each
	 */
	public void addInitialState() {
		
		List<Integer> initFacts = new ArrayList<>();
		// Each fact from the initial state holds at t=0
		for (int p : data.getInitFacts()) {
			add(uncompressedFactAtStep(p, 0));
			initFacts.add(data.compressFact(p));
		}
		// Each non-specified fact does NOT hold at t=0
		for (int p : domainFacts) {
			if (!initFacts.contains(p)) {
				add(-d.atom(factAtStep, p, 0));
			}
		}
	}
	
	/**
	 * Adds clauses that ensure the <i>before</i>, <i>after</i> and
	 * <i>between</i> temporal constraints between tasks from the 
	 * initial network.
	 * <br/><br/>O(c*s) clauses, O(1) literals each
	 */
	public void addGoalFacts() {
		
		// Goal state needs to hold at the end
		// (extracted from the initial task network)
		for (int p : data.getGoalFacts()) {
			add(uncompressedFactAtStep(p, numSteps));
		}
	}
	
	/**
	 * Adds clauses that require all primitive actions
	 * to follow their preconditions and to trigger their effects.
	 * <br/><br/>O(a*s) clauses, O(1) literals each
	 */
	public void addActionPreconditionsAndEffects() {
		
		for (int a : domainActions) {
			int task = data.actionFlatIdxToTaskIdx(a);
			HtnAction action = data.getActionOfTask(task);
			for (int t : domainSteps) {
				int atomAction = d.atom(actionAtStep, a, t);
				// ... all preconditions (now)
				for (int p : action.getPreconditionsPos()) {
					add(-atomAction, uncompressedFactAtStep(p, t));
				}
				for (int p : action.getPreconditionsNeg()) {
					add(-atomAction, -uncompressedFactAtStep(p, t));
				}
			}
			for (int t : domainStepsButLast) {
				int atomAction = d.atom(actionAtStep, a, t);
				// ... all effects (at the next timestep)
				for (int p : action.getEffectsPos()) {
					add(-atomAction, uncompressedFactAtStep(p, t+1));
				}
				for (int p : action.getEffectsNeg()) {
					add(-atomAction, -uncompressedFactAtStep(p, t+1));
				}
			}
		}
	}
	
	/**
	 * Adds clauses to ensure that at least one action
	 * is executed at each step.
	 * <br/><br/>O(a*s) clauses, O(1) literals each
	 */
	public void addAtLeastOneActionPerStep() {
		
		// any of the actions must hold at timestep t
		for (int t : domainStepsButLast) {
			for (int a : domainActions) {
				addToClause(d.atom(actionAtStep, a, t));
			}
			endClause();
		}
		// no action at the final step
		for (int a : domainActions) {
			add(-d.atom(actionAtStep, a, numSteps));
		}
	}
	
	/**
	 * Adds clauses to ensure that not more than one action
	 * is executed at each step.
	 * <br/><br/>O(s*a*log(a)) clauses, O(1) literals each
	 */
	public void addAtMostOneActionPerStep() {
		
		for (int t : domainSteps) {
			List<Integer> actionsAtStep = new ArrayList<>();
			for (int a : domainActions) {
				actionsAtStep.add(d.atom(actionAtStep, a, t));
			}
			SatFormula amo = SatUtils.atMostOne(AtMostOneMode.quadratic, 
					actionsAtStep, d);
			add(amo);
		}
	}
	
	/**
	 * Adds clauses to ensure that, if any fact changes,
	 * some action has been executed and has this fact
	 * as one of its effects.
	 * <br/><br/>O(s*p) clauses, O(a) literals each
	 */
	public void addActionFrameAxioms() {
		
		for (int t : domainStepsButLast) {
			for (int p : domainFacts) {
				int pUncompr = data.uncompressFact(p);
				// if p is false @t and true @t+1, then
				// any of the supporting actions is active @t
				addToClause(d.atom(factAtStep, p, t), -d.atom(factAtStep, p, t+1));
				for (int a : data.getActionTasksWithEffect(pUncompr, true)) {
					int action = data.actionTaskIdxToFlatIdx(a);
					addToClause(d.atom(actionAtStep, action, t));
				}
				endClause();
				// if p is true @t and false @t+1, then
				// any of the supporting actions is active @t
				addToClause(-d.atom(factAtStep, p, t), d.atom(factAtStep, p, t+1));
				for (int a : data.getActionTasksWithEffect(pUncompr, false)) {
					int action = data.actionTaskIdxToFlatIdx(a);
					addToClause(d.atom(actionAtStep, action, t));
				}
				endClause();
			}
		}
	}
	
	public void addInitialTaskNetworkConstraints() {
		
		List<Integer> initTasks = data.getInitTasks();
		
		int firstTask = initTasks.get(0);
		int lastTask = initTasks.get(initTasks.size()-1);
		
		// First task starts right away
		add(d.atom(taskStartsAtStep, 
			data.getNonprimitiveIndexOfTask(firstTask),
			0));
		
		// Last task ends at the final step
		add(d.atom(taskEndsAtStep, 
			data.getNonprimitiveIndexOfTask(lastTask), 
			numSteps-1));
		
		// For each pair in the sequence of initial tasks
		for (int i = 0; i < initTasks.size()-1; i++) {
			int task1 = data.getNonprimitiveIndexOfTask(initTasks.get(i));
			int task2 = data.getNonprimitiveIndexOfTask(initTasks.get(i+1));
			
			for (int t : domainStepsButLast) {
				
				add(-d.atom(taskEndsAtStep, task1, t), 
						d.atom(taskStartsAtStep, task2, t+1));
					
				add(d.atom(taskEndsAtStep, task1, t), 
						-d.atom(taskStartsAtStep, task2, t+1));
			}
			
			add(-d.atom(taskEndsAtStep, task1, numSteps));
		}
	}
	
	public void addNonprimTaskReductionChoice() {
		
		for (int t : domainSteps) {
			
			for (int nonprimTask : domainNonprimTasks) {
				int task = data.getNonprimitiveTasks().get(nonprimTask);

				// No task starts or ends at the final step
				if (t == numSteps) {
					add(-d.atom(taskStartsAtStep, nonprimTask, t));
					add(-d.atom(taskEndsAtStep, nonprimTask, t));
				}
				
				for (int r = 0; r < data.getNumMethodsOfTask(task); r++) {
					add(-d.atom(taskReductionStartsAtStep, nonprimTask, r, t), 
							d.atom(taskStartsAtStep, nonprimTask, t));
					add(-d.atom(taskReductionEndsAtStep, nonprimTask, r, t), 
							d.atom(taskEndsAtStep, nonprimTask, t));
				}
				
				addToClause(-d.atom(taskStartsAtStep, nonprimTask, t));
				for (int r = 0; r < data.getNumMethodsOfTask(task); r++) {
					addToClause(d.atom(taskReductionStartsAtStep, 
							nonprimTask, r, t));
				}
				endClause();

				addToClause(-d.atom(taskEndsAtStep, nonprimTask, t));
				for (int r = 0; r < data.getNumMethodsOfTask(task); r++) {
					addToClause(d.atom(taskReductionEndsAtStep, 
							nonprimTask, r, t));
				}
				endClause();
			}
		}
	}
	
	/**
	 * Adds clauses that ensure that 
	 * <ul><li>a started task will end,</li>
	 * <li>a finished task has started before,</li>
	 * <li>a started task does not start again until
	 * it finishes, and</li>
	 * <li>a finished task did not finish another time 
	 * before it started.</li>
	 * </ul> 
	 * <br/><br/>O(s*t) clauses, O(s) literals each
	 */
	public void addValidGrammarForTaskStartAndEnd() {
		
		for (int t : domainSteps) {
			for (int nonprimTask : domainNonprimTasks) {
				for (int r = 0; r < 
						data.getNumMethodsOfTask(
								data.getNonprimitiveTasks().get(nonprimTask)
						); r++) {
					
					int litTaskStarts = -d.atom(taskReductionStartsAtStep, 
							nonprimTask, r, t);
					int litTaskEnds = -d.atom(taskReductionEndsAtStep, 
							nonprimTask, r, t);
					
					// When a task starts, it must finish at some later step
					// (allow concurrence as well)
					addToClause(litTaskStarts);
					for (int tAfter = t; tAfter <= Math.min(t+maxSteps(nonprimTask), 
							numSteps); tAfter++) {
						addToClause(d.atom(taskReductionEndsAtStep, 
								nonprimTask, r, tAfter));
					}
					endClause();
					
					// When a task finishes, it must have started before
					addToClause(litTaskEnds);
					for (int tBefore = Math.max(t-maxSteps(nonprimTask), 0); 
							tBefore <= t; tBefore++) {
						addToClause(d.atom(taskReductionStartsAtStep, 
								nonprimTask, r, tBefore));
					}
					endClause();
					
					/*
					// When a task starts @t and it hasn't finished in [t,tAfter),
					// then it doesn't start (again) @tAfter
					for (int tAfter = t+1; tAfter <= Math.min(t+maxSteps(nonprimTask),
							numSteps); tAfter++) {
						addToClause(litTaskStarts);
						for (int tBetween = t; tBetween < tAfter; tBetween++) {
							addToClause(d.atom(taskReductionEndsAtStep, 
									nonprimTask, r, tBetween));
						}
						addToClause(-d.atom(taskReductionStartsAtStep, 
								nonprimTask, r, tAfter));
						endClause();
					}
					
					// When a task finishes @t and it hasn't started in (tBefore,t],
					// then it doesn't finish (again) @tBefore
					for (int tBefore = Math.max(t-maxSteps(nonprimTask), 0); 
							tBefore <= t-1; tBefore++) {
						addToClause(litTaskEnds);
						for (int tBetween = tBefore+1; tBetween <= t; tBetween++) {
							addToClause(d.atom(taskReductionStartsAtStep, 
									nonprimTask, r, tBetween));
						}
						addToClause(-d.atom(taskReductionEndsAtStep, 
								nonprimTask, r, tBefore));
						endClause();
					}*/
				}
								
			}
		}
	}
	
	/**
	 * Adds clauses ensuring that a reduction's end
	 * implies the previous end of all of its subtasks
	 * and that a reduction's start implies the following
	 * start of all of its subtasks.
	 * <br/><br/>O(s*t*r*e) clauses, O(s) literals each
	 */
	public void addReductionSubtaskCompletion() {
		
		for (int tStart : domainSteps) {
			for (int tEnd = tStart; tEnd <= numSteps; tEnd++) {
				
				for (int nonprimTask : domainNonprimTasks) {
					
					int task = data.getNonprimitiveTasks().get(nonprimTask);
					
					List<HtnMethod> reductions = data.getMethodsOfTask(task);
					for (int r = 0; r < data.getNumMethodsOfTask(task); r++) {
						
						List<Integer> expansion = reductions.get(r).getExpansion();
						for (int subtask : expansion) {
							
							if (data.isTaskPrimitive(subtask)) {
								
								addToClause(-d.atom(taskReductionStartsAtStep, 
										nonprimTask, r, tStart));
								addToClause(-d.atom(taskReductionEndsAtStep, 
										nonprimTask, r, tEnd));
								for (int tBetween = tStart; tBetween <= tEnd; tBetween++) {									
									addToClause(d.atom(actionAtStep, 
											data.getPrimitiveIndexOfTask(subtask), 
											tBetween));
								}
								endClause();
								
							} else {
								addToClause(-d.atom(taskReductionStartsAtStep, 
										nonprimTask, r, tStart));
								addToClause(-d.atom(taskReductionEndsAtStep, 
										nonprimTask, r, tEnd));
								for (int tBetween = tStart; tBetween <= tEnd; tBetween++) {							
									addToClause(d.atom(taskEndsAtStep, 
											data.getNonprimitiveIndexOfTask(subtask), 
											tBetween));
								}
								endClause();
								
								addToClause(-d.atom(taskReductionStartsAtStep, 
										nonprimTask, r, tStart));
								addToClause(-d.atom(taskReductionEndsAtStep, 
										nonprimTask, r, tEnd));
								for (int tBetween = tStart; tBetween <= tEnd; tBetween++) {							
									addToClause(d.atom(taskStartsAtStep, 
											data.getNonprimitiveIndexOfTask(subtask), 
											tBetween));
								}
								endClause();
							}
						}
					}
				}
				
			}
		}
	}
	
	public void addBorderSubtaskExecution() {
		
		for (int t : domainSteps) {
			for (int nonprimTask : domainNonprimTasks) {
				
				int task = data.getNonprimitiveTasks().get(nonprimTask);
				int numReductions = data.getNumMethodsOfTask(task);
				
				for (int r = 0; r < numReductions; r++) {
					HtnMethod method = data.getMethodsOfTask(task).get(r);
					
					int firstSubtask = method.getExpansion().get(0);
					int lastSubtask = method.getExpansion().get(method.getExpansion().size()-1);

					addToClause(-d.atom(taskReductionStartsAtStep, nonprimTask, r, t));
					if (data.isTaskPrimitive(firstSubtask)) {
						addToClause(d.atom(actionAtStep, 
								data.getPrimitiveIndexOfTask(firstSubtask), 
								t));
					} else {
						addToClause(d.atom(taskStartsAtStep, 
								data.getNonprimitiveIndexOfTask(firstSubtask), 
								t));
					}
					endClause();

					addToClause(-d.atom(taskReductionEndsAtStep, nonprimTask, r, t));
					if (data.isTaskPrimitive(lastSubtask)) {
						addToClause(d.atom(actionAtStep, 
								data.getPrimitiveIndexOfTask(lastSubtask), 
								t));
					} else {
						addToClause(d.atom(taskEndsAtStep, 
								data.getNonprimitiveIndexOfTask(lastSubtask), 
								t));
					}
					endClause();
					
				}				
			}
		}
	}
	
	/**
	 * Adds clauses ensuring that the start of a reduction
	 * implies that all of its preconditions hold.
	 * <br/><br/>O(s*t*r*c) clauses, O(1) literals each
	 */
	public void addMethodPreconditions() {
		
		for (int t : domainSteps) {
			for (int nonprimTask : domainNonprimTasks) {
				
				int task = data.getNonprimitiveTasks().get(nonprimTask);
				int numReductions = data.getNumMethodsOfTask(task);
				
				for (int r = 0; r < numReductions; r++) {
					HtnMethod method = data.getMethodsOfTask(task).get(r);
					// If a task reduction starts @t,
					// then its pos. and neg. preconditions must hold @t
					List<Integer> preconds = method.getBeforeConstraints();
					for (int p : preconds) {
						int lit = uncompressedFactAtStep(p, t);
						add(-d.atom(taskReductionStartsAtStep, nonprimTask, r, t), 
								lit);
					}
				}				
			}
		}
	}	
	
	public void addMethodOrdering() {
		
		for (int t : domainSteps) {
			for (int nonprimTask : domainNonprimTasks) {
				
				int task = data.getNonprimitiveTasks().get(nonprimTask);
				int numReductions = data.getNumMethodsOfTask(task);
				
				for (int r = 0; r < numReductions; r++) {
					List<Integer> expansion = data.getMethodsOfTask(task).get(r)
							.getExpansion();
					
					for (int expIdx = 0; expIdx < expansion.size(); expIdx++) {
						int subtask = expansion.get(expIdx);
												
						// If a task reduction starts @t,
						// then at some point each subtask is applied
						// (stated with the taskReductionStartsAtStep
						// helper atoms)
						SatClause cStart = new SatClause(
								-d.atom(taskReductionStartsAtStep, nonprimTask, r, t));
						
						for (int tAfter = t; tAfter <= Math.min(
								t+maxSteps(nonprimTask), numSteps); tAfter++) {

							cStart.add(d.atom(subtaskOfReductionStartsAtStep, 
									nonprimTask, r, t, subtask, tAfter));
							
							// If a subtask @tAfter is applied as part of a reduction @t,
							// then the subtask starts @tAfter
							if (data.isTaskPrimitive(subtask)) {
								add(-d.atom(subtaskOfReductionStartsAtStep, 
										nonprimTask, r, t, subtask, tAfter), 
									d.atom(actionAtStep, 
										data.getPrimitiveIndexOfTask(subtask), 
										tAfter));							
							} else {
								add(-d.atom(subtaskOfReductionStartsAtStep, 
										nonprimTask, r, t, subtask, tAfter), 
									d.atom(taskStartsAtStep, 
										data.getNonprimitiveIndexOfTask(subtask),
										tAfter));	
							}
							
							// A subtask @tAfter is applied as part of a reduction @t
							// iff the _previous_ subtask ended @tAfter-1
							if (expIdx > 0 && tAfter > 0) {
								int prevSubtask = expansion.get(expIdx-1);

								add(-d.atom(subtaskOfReductionStartsAtStep, 
										nonprimTask, r, t, subtask, tAfter), 
									d.atom(subtaskOfReductionEndsAtStep, 
										nonprimTask, r, t, prevSubtask, tAfter-1));
								add(d.atom(subtaskOfReductionStartsAtStep, 
										nonprimTask, r, t, subtask, tAfter), 
									-d.atom(subtaskOfReductionEndsAtStep, 
										nonprimTask, r, t, prevSubtask, tAfter-1));
							}
						}
						f.addClause(cStart);
						
						SatClause cEnd = new SatClause(
								-d.atom(taskReductionEndsAtStep, nonprimTask, r, t));
						
						for (int tBefore = Math.max(t-maxSteps(nonprimTask), 0); 
								tBefore <= t; tBefore++) {

							cEnd.add(d.atom(subtaskOfReductionEndsAtStep, 
									nonprimTask, r, t, subtask, tBefore));
							
							// If a subtask @tAfter is applied as part of a reduction @t,
							// then the subtask ends @tAfter
							if (data.isTaskPrimitive(subtask)) {
								add(-d.atom(subtaskOfReductionEndsAtStep, 
										nonprimTask, r, t, subtask, tBefore), 
									d.atom(actionAtStep, 
										data.getPrimitiveIndexOfTask(subtask), 
										tBefore));							
							} else {
								add(-d.atom(subtaskOfReductionEndsAtStep, 
										nonprimTask, r, t, subtask, tBefore), 
									d.atom(taskEndsAtStep, 
										data.getNonprimitiveIndexOfTask(subtask),
										tBefore));	
							}
						}
						f.addClause(cEnd);
					}
					/*
					int tMin = t;
					int tMax = Math.min(t+maxSteps(expansion.get(0)), numSteps);
					for (int expIdx = 0; expIdx < expansion.size()-1; expIdx++) {
						int subtask1 = expansion.get(expIdx);
						int subtask2 = expansion.get(expIdx+1);
						
						for (int t2 = tMin; t2 <= tMax; t2++) {
							// If a given task reduction starts @t
							// and a subtask @t2 realizes it,
							// then the next subtask realizes it _later_
							addToClause(-d.atom(taskReductionStartsAtStep, 
									nonprimTask, r, t));
							addToClause(-d.atom(subtaskOfReductionStartsAtStep, 
									nonprimTask, r, t, subtask1, t2));
							for (int t3 = t2+1; t3 <= Math.min(t2+maxSteps(subtask1), 
									numSteps); t3++) {
								addToClause(d.atom(subtaskOfReductionStartsAtStep, 
									nonprimTask, r, t, subtask2, t3));
							}
							endClause();
						}
						tMin = Math.min(tMin + maxSteps(expansion.get(expIdx)), 
								numSteps);
						tMax = Math.min(tMax + maxSteps(expansion.get(expIdx+1)), 
								numSteps);
					}*/
				}
			}
		}
	}
	
	/**
	 * @return the SatFormula object created
	 * by all previous add*() calls
	 */
	public SatFormula getFormula() {
		return f;
	}
	
	/**
	 * @return the SatDomain collection of different
	 * atom types and their organization
	 */
	public SatDomain getDomain() {
		return d;
	}
	
	private void add(Integer... literals) {
		
		f.addClause(new SatClause(literals));
	}
		
	private void add(SatFormula f) {
		
		this.f.addAllClauses(f);
	}
	
	/**
	 * Adds all provided literals to the currently
	 * active clause, or to a new clause if there is
	 * no current active one.<br/>
	 * The active clause can then be concluded
	 * with endClause().
	 */
	private void addToClause(Integer... literals) {
		
		if (currentClause == null)
			currentClause = new SatClause();
		
		for (int lit : literals)
			currentClause.add(lit);
	}
	
	/**
	 * Concludes the clause that has been added to
	 * by previous addToClause(*) calls, and adds it
	 * to the formula.
	 */
	private void endClause() {
		
		if (currentClause != null) {
			f.addClause(currentClause);
			currentClause = null;
		}
	}
	
	/**
	 * @param fact an uncompressed fact as it occurs in
	 * the problem definition; a positive number which
	 * can represent a fact or a negated fact
	 * @param t the timestep where the fact holds
	 * @return a SAT literal (positive or negative) 
	 * corresponding to the truth value of the fact at step t
	 */
	private int uncompressedFactAtStep(int fact, int t) {
		
		int factCompressed = data.compressFact(fact);
		boolean factHolds = data.isFactPositive(fact);
		return (factHolds ? 1 : -1) * d.atom(factAtStep, factCompressed, t);
	}
	
	private int maxSteps(int nonprimTask) {
		return maxTaskLengths[data.getNonprimitiveTasks().get(nonprimTask)];
	}
}
