package htnsat.encode;

import java.util.ArrayList;
import java.util.List;

import htnsat.sat.ISatFormula;
import htnsat.decode.StackDecoder;
import htnsat.htn.HtnAction;
import htnsat.htn.HtnMethod;
import htnsat.htn.HtnProblem;
import htnsat.htn.HtnProblemData;
import htnsat.sat.ActionConflicts;
import htnsat.sat.AtomDomain;
import htnsat.sat.AtomType;
import htnsat.sat.BinaryEncoding;
import htnsat.sat.IncSatFormula;
import htnsat.sat.SatDomain;
import htnsat.sat.SatFormula;
import htnsat.sat.SatUtils;
import htnsat.sat.SatUtils.AtMostOneMode;
import htnsat.util.Assert;
import htnsat.sat.IncSatFormula.FormulaType;
import pddl4j.util.OrderedMethod;
import pddl4j.util.Signature;
import htnsat.sat.SatClause;

/**
 * <b>M</b>ultiple (Stack) <b>M</b>achine <b>S</b>imulation encoding.
 * 
 * @author Dominik Schreiber
 */
public class HtnClausesMMS extends AbstractHtnClauses {

	/**
	 * The value representing the stack's bottom.
	 */
	private List<Integer> stackBottoms;
	
	// AtomDomains which are used for encoding
	private AtomDomain domFacts;
	private AtomDomain domActions;
	private AtomDomain domMachines;
	private AtomDomain domReductions;
	private AtomDomain domPushSize;
	private AtomDomain domStack;
		
	// Helper AtomDomains to easily iterate 
	// over possible values
	private List<List<BinaryEncoding>> taskStacks;
	private List<AtomDomain> domsTasks;
	private List<AtomDomain> domsTaskBits;
	private List<AtomDomain> domsActions;
	
	// Global AtomTypes
	private AtomType holds;
	private AtomType execute;
	
	// AtomTypes per machine
	private List<AtomType> pop;
	private List<AtomType> skip;
	private List<AtomType> push;
	private List<AtomType> executes;
	private List<AtomType> reduction;
	//private AtomType stackLoadedAt;
	
	// Resulting incremental formula
	private IncSatFormula f;
	
	// Helpers to conveniently create clauses
	private FormulaType currentFormulaMode;
	private SatClause currentClause;
	
	private HtnProblemData data;
			
	public HtnClausesMMS(HtnProblemData data, 
			int stackSize) {
		
		super(null, true); // incremental = true
		this.data = data;
		
		f = new IncSatFormula();
		
		// Domains
		
		int maxReductionsPerTask = data.getMaxReductionsPerTask();
		int maxSubtasksPerRed = data.getMaxExpansionSize();
		domFacts = new AtomDomain(0, data.getFacts().size());
		domActions = new AtomDomain(0, 
				data.getPrimitiveTasks().size());
		
		domStack = new AtomDomain(0, stackSize);
		domReductions = new AtomDomain(0, maxReductionsPerTask);
		domMachines = new AtomDomain(0, 
				data.getInitTasks().size());
		domsTasks = new ArrayList<>();
		domsActions = new ArrayList<>();
		stackBottoms = new ArrayList<>();
		for (int machine : domMachines) {
			/*int numTasksForInitTask = data.getReachableTasksFor(machine).size();
			int numPrimTasksForInitTask = data.getReachablePrimitiveTasksFor(
					machine).size();
			AtomDomain domTasks = new AtomDomain(0, 
					numTasksForInitTask+1);
			AtomDomain domAction = new AtomDomain(0, 
					numPrimTasksForInitTask);
			domsTasks.add(domTasks);
			domsActions.add(domAction);
			stackBottoms.add(numTasksForInitTask);*/
		}
		
		//domTasks = new AtomDomain(0, numTasks);
		domPushSize = new AtomDomain(0, Math.min(stackSize, maxSubtasksPerRed));
		
		// Atom types
		
		holds = new AtomType(AtomTag.FACT_AT_STEP, domFacts);
		execute = new AtomType(AtomTag.ACTION_AT_STEP_AS_ACTION_IDX, domActions);
		
		pop = new ArrayList<>();
		skip = new ArrayList<>();
		push = new ArrayList<>();
		executes = new ArrayList<>();
		reduction = new ArrayList<>();
		for (int machine : domMachines) {
			pop.add(new AtomType());
			skip.add(new AtomType());
			push.add(new AtomType(domPushSize));
			executes.add(new AtomType(domsActions.get(machine)));
			reduction.add(new AtomType(domReductions));
		}
		
		List<AtomType> allAtomTypes = new ArrayList<>();
		allAtomTypes.add(holds);
		allAtomTypes.add(execute);
		allAtomTypes.addAll(pop);
		allAtomTypes.addAll(skip);
		allAtomTypes.addAll(push);
		allAtomTypes.addAll(executes);
		allAtomTypes.addAll(reduction);
		d = new SatDomain(allAtomTypes);
		
		// Initialize binary-encoded task stacks
		taskStacks = new ArrayList<>();
		domsTaskBits = new ArrayList<>();
		for (int machine : domMachines) {
			List<BinaryEncoding> stack = new ArrayList<>();
			for (int pos = 0; pos < stackSize; pos++) {
				stack.add(new BinaryEncoding(AtomTag.BINARY_ENCODING, 
						d, domsTasks.get(machine).getSize()));
			}
			taskStacks.add(stack);
			domsTaskBits.add(new AtomDomain(0, stack.get(0).getBitLength()));	
		}
	}
	
	/**
	 * Completely defines all of the relevant facts
	 * for the initial state, according to the problem 
	 * definition.
	 * 
	 * <br/><br/>Type: <b>initial</b>
	 */
	public void addInitialFacts() {
		
		mode(FormulaType.initial);
		
		// Facts
		List<Integer> initFacts = new ArrayList<>();
		// Each fact from the initial state holds at t=0
		for (int p : data.getInitFacts()) {
			add(fact(p));
			initFacts.add(data.compressFact(p));
		}
		// Each non-specified fact does NOT hold at t=0
		for (int p : domFacts) {
			if (!initFacts.contains(p)) {
				add(-d.atom(holds, p));
			}
		}
	}
	
	/**
	 * Adds clauses for the initial state that ensure that 
	 * the initial task is on top of the stack and the bottom
	 * symbol is on the following position.
	 * 
	 * <br/>All other stack positions are undefined at the initial 
	 * state as they will never be reached in a consistent assignment.
	 * 
	 * <br/><br/>Type: <b>initial</b>
	 */
	public void addInitialStackState() {
		
		mode(FormulaType.initial);
		
		// Initial elements to process are the initial tasks
		for (int machine : domMachines) {
			
			List<BinaryEncoding> stack = taskStacks.get(machine);
			int initTask = data.getInitTasks().get(machine);
			
			Integer[] literals = stack.get(0).posLiterals(
					taskIdxToTaskDomainIdx(machine, initTask));
			for (int lit : literals) {
				add(lit);
			}
			
			// The next element is the bottom symbol
			literals = stack.get(1).posLiterals(
					stackBottoms.get(machine));
			for (int lit : literals) {
				add(lit);
			}
		}
	}
	
	/**
	 * Adds the single unit clause stating that the bottom symbol
	 * must be on top of the stack in the final state, i.e.
	 * the stack is empty.
	 * 
	 * <br/><br/>Type: <b>goal</b>
	 */
	public void addGoalCondition() {
		
		mode(FormulaType.goal);
		
		for (int machine : domMachines) {
			
			// To be a valid plan, the stacks must be empty in the end,
			// i.e. the bottom symbol is on top of each stack
			Integer[] literals = taskStacks.get(machine)
					.get(0).posLiterals(stackBottoms.get(machine));
			for (int lit : literals) {
				add(lit);
			}
		}
	}
	
	/**
	 * Adds goal conditions in a classical planning fashion,
	 * i.e. all goal predicates must hold in the final state.
	 * 
	 * <br/>Unnecessary, if the full encoding is used. Convenient
	 * for debugging (only add clauses corresponding to classical 
	 * planning, test if a plan is found that way).
	 * 
	 * <br/>The goal conditions are extracted from the after 
	 * constraints of the initial task.
	 * 
	 * <br/><br/>Type: <b>goal</b>
	 */
	public void addClassicFactGoal() {
		
		mode(FormulaType.goal);
		
		for (int p : data.getGoalFacts()) {
			add(fact(p));
		}
	}
	
	/**
	 * Adds clauses ensuring that at most one action is
	 * executed for any given step.
	 * 
	 * <br/>Used encoding: tree-like at-most-one
	 * 
	 * <br/><br/>Type: <b>universal</b>
	 */
	public void addAtMostOneAction() {
		
		mode(FormulaType.universal);
		
		List<Integer> atoms = new ArrayList<>();
		for (int a : domActions) {
			atoms.add(d.atom(execute, a));
		}
		add(SatUtils.atMostOne(AtMostOneMode.tree, atoms, d));
	}
	
	/**
	 * Adds clauses that require an action's preconditions to hold
	 * whenever it is executed.
	 * 
	 * <br/><br/>Type: <b>universal</b>
	 */
	public void addActionPreconditions() {
		
		mode(FormulaType.universal);
		
		for (int a : domActions) {
			int task = data.actionFlatIdxToTaskIdx(a);
			HtnAction action = data.getActionOfTask(task);
			
			int litAction = -d.atom(execute, a);
			for (int p : action.getPreconditionsPos()) {
				add(litAction, fact(p));
			}
			for (int p : action.getPreconditionsNeg()) {
				add(litAction, -fact(p));
			}
		}
	}
	
	/**
	 * Adds clauses ensuring that an action's effects hold
	 * whenever the action is executed.
	 * 
	 * <br/><br/>Type: <b>transitional</b>
	 */
	public void addActionEffects() {
		
		mode(FormulaType.transitional);
		
		for (int a : domActions) {
			int task = data.actionFlatIdxToTaskIdx(a);
			HtnAction action = data.getActionOfTask(task);
			
			int litAction = -d.atom(execute, a);
			for (int p : action.getEffectsPos()) {
				add(litAction, nextStep(fact(p)));
			}
			for (int p : action.getEffectsNeg()) {
				add(litAction, -nextStep(fact(p)));
			}
		}
	}
	
	/**
	 * Adds clauses ensuring that the change of any fact
	 * implies the execution of a supporting action.
	 * 
	 * <br/><br/>Type: <b>transitional</b>
	 */
	public void addActionFrameAxioms() {
		
		mode(FormulaType.transitional);
		
		for (int p : domFacts) {
			
			int pUncompr = data.uncompressFact(p);
			// if p is false @t and true @t+1, then
			// any of the supporting actions is active @t
			addToClause(d.atom(holds, p), -nextStep(d.atom(holds, p)));
			for (int task : data.getActionTasksWithEffect(pUncompr, true)) {
				int a = data.actionTaskIdxToFlatIdx(task);
				addToClause(d.atom(execute, a));
			}
			endClause();
			// if p is true @t and false @t+1, then
			// any of the supporting actions is active @t
			addToClause(-d.atom(holds, p), nextStep(d.atom(holds, p)));
			for (int task : data.getActionTasksWithEffect(pUncompr, false)) {
				int a = data.actionTaskIdxToFlatIdx(task);
				addToClause(d.atom(execute, a));
			}
			endClause();
		}
	}
	
	/**
	 * Ensures that pop and push are mutually exclusive
	 * and that no action is executed whenever a pop or a push
	 * is done.
	 * 
	 * <br/><br/>Type: <b>universal</b>
	 */
	public void addPushPopActionExclusion() {
		
		mode(FormulaType.universal);
		
		for (int machine : domMachines) {
			
			// List of operations on a stack
			List<Integer> atoms = new ArrayList<>();
			for (int k : domPushSize) {
				atoms.add(d.atom(push.get(machine), k));
			}
			atoms.add(d.atom(pop.get(machine)));
			atoms.add(d.atom(skip.get(machine)));
			
			// At least one pop/push/skip action is executed
			for (int atom : atoms) {
				addToClause(atom);
			}
			endClause();
			
			// At most one pop/push/skip action is executed
			add(SatUtils.atMostOne(AtMostOneMode.quadratic, atoms, d));
		}
	}
	
	public void concludeDomain() {
		
		// After this method call, all necessary helper
		// variables must have been added
		d.concludeDomain();
		f.setEncodedNumVars(d.getTotalSize());
	}
	
	/**
	 * Adds clauses that ensure that the stack's content
	 * will shift accordingly to pop() and push() operations
	 * and remain unchanged if no pop() and no push() is done. 
	 * 
	 * <br/><br/>Type: <b>transitional</b>
	 */
	public void addStackMovements() {
		
		mode(FormulaType.transitional);
		
		for (int machine : domMachines) {
			
			List<BinaryEncoding> stack = taskStacks.get(machine);

			Integer[] literals = taskStacks.get(machine).get(0)
					.negLiterals(stackBottoms.get(machine));
			addToClause(literals);
			addToClause(d.atom(skip.get(machine)));
			endClause();
			
			// Movement of task bits
			for (int k : domStack) {		
				for (int i : domsTaskBits.get(machine)) {
					int atomAtK = stack.get(k).getAtomForBit(i);
					
					// Skip() operation: everything stays the same
					add(-atomAtK, -d.atom(skip.get(machine)), nextStep(atomAtK));
					add(atomAtK, -d.atom(skip.get(machine)), -nextStep(atomAtK));
					
					// Push operations
					for (int numPush : domPushSize) {
						
						if (k > 0 && k+numPush < stack.size()) {
							
							int atomAtKPlus = stack.get(k+numPush).getAtomForBit(i);
							
							// If push() is done, all bits are shifted +numPush
							add(-atomAtK, -d.atom(push.get(machine), numPush), 
									nextStep(atomAtKPlus));
							add(atomAtK, -d.atom(push.get(machine), numPush), 
									-nextStep(atomAtKPlus));
						}
					}
					
					// Pop() operation
					if (k > 0) {
						int atomAtKMinus1 = stack.get(k-1).getAtomForBit(i);
						
						// If pop() is done, all bits are shifted -1
						add(-atomAtK, -d.atom(pop.get(machine)), 
								nextStep(atomAtKMinus1));
						add(atomAtK, -d.atom(pop.get(machine)), 
								-nextStep(atomAtKMinus1));
					}
				}
			}
		}
	}
	
	/**
	 * Adds clauses defining the necessary stack operation
	 * or the action to execute depending on what method and 
	 * what counter value is on top of the stack.
	 * <br/>Also checks <i>after</i> constraints of tasks before
	 * their pop() and <i>between</i> constraints between
	 * the two corresponding subtasks.
	 * 
	 * <br/><br/>Type: <b>universal</b>
	 */
	public void addStackTopImplications() {
			
		for (int machine : domMachines) {
			
			// Remember all actions which correspond to some
			// task belonging to the machine's task domain
			List<Integer> possibleActions = new ArrayList<>();
			
			for (int t : domsTasks.get(machine)) {
				
				if (t == stackBottoms.get(machine)) {
					continue;
				}
				
				List<BinaryEncoding> stack = taskStacks.get(machine);
				Integer[] litTaskNeg = stack.get(0).negLiterals(t);
				
				mode(FormulaType.universal);
				int task = taskDomainIdxToTaskIdx(machine, t);
				
				if (data.isTaskPrimitive(task)) {
					
					// This action CAN occur on this machine
					int actionDomIdx = actionTaskIdxToDomainIdx(machine, task);
					possibleActions.add(actionDomIdx);
					
					// Execute the action (implying a pop()) or skip
					addToClause(litTaskNeg);
					addToClause(d.atom(executes.get(machine), 
							actionDomIdx));
					addToClause(d.atom(skip.get(machine)));
					endClause();

					addToClause(litTaskNeg);
					addToClause(-d.atom(executes.get(machine), 
							actionDomIdx));
					addToClause(d.atom(pop.get(machine)));
					endClause();
					
					// Back direction: if the action is executed,
					// then the task needs to be on the stack's top
					for (int lit : litTaskNeg) {
						add(-d.atom(executes.get(machine), 
								actionDomIdx), -lit);
					}
					
				} else {
					
					// Some reduction must be applied, or skip()
					int numReductions = data.getNumMethodsOfTask(task);
					addToClause(litTaskNeg);
					for (int red : domReductions) {
						if (red >= numReductions)
							break;
						addToClause(d.atom(reduction.get(machine), red));
					}
					/*
					 *  TODO This literal allows a machine to skip a step 
					 *  even if a method is on top of the stack. This can be
					 *  useful to wait for another machine to enable a
					 *  precondition that leads to an overall shorter computation
					 *  of this machine (because a shorter expansion can be
					 *  chosen). However, this also greatly increases the
					 *  possible space to be explored by the solver.
					 */
					//addToClause(d.atom(skip.get(machine)));
					endClause();
					
					// For each reduction: if applied, then push all subtasks
					// and check precondition
					for (int red : domReductions) {
						if (red >= numReductions)
							break;
						
						List<Integer> subtasks = 
								data.getMethodsOfTask(task).get(red).getExpansion();
						for (int subtaskIdx = 0; subtaskIdx < subtasks.size(); 
								subtaskIdx++) {
							int subtask = subtasks.get(subtaskIdx);
							for (int lit : stack.get(subtaskIdx)
									.posLiterals(
										taskIdxToTaskDomainIdx(machine, subtask))) {
								// If task on top and reduction applied,
								// then each respective subtask bit on the task stack is set
								mode(FormulaType.transitional);
								addToClause(litTaskNeg);
								addToClause(-d.atom(reduction.get(machine), red), 
										nextStep(lit));
								endClause();
							}
						}
						mode(FormulaType.universal);
						
						// All subtasks are pushed, but the current task vanishes 
						// => push(numSubtasks -1)
						addToClause(litTaskNeg);
						addToClause(-d.atom(reduction.get(machine), red), 
								d.atom(push.get(machine), subtasks.size()-1));
						endClause();
						
						// Before constraints must hold
						HtnMethod m = data.getMethodsOfTask(task).get(red);
						for (int p : m.getBeforeConstraints()) {
							addToClause(litTaskNeg);
							addToClause(-d.atom(reduction.get(machine), red), fact(p));
							endClause();
						}
					}
				}
			}
			
			// Forbid actions which cannot occur in this machine
			mode(FormulaType.universal);
			for (int a : domsActions.get(machine)) {
				if (!possibleActions.contains(a)) {
					// This action cannot occur in this machine
					add(-d.atom(executes.get(machine), a));
				}
			}
		}
	}	
	
	public void addMachineActionSemantics() {
		
		mode(FormulaType.universal);
		
		for (int a : domActions) {
			int task = data.actionFlatIdxToTaskIdx(a);
			
			addToClause(-d.atom(execute, a));
			for (int machine : domMachines) {
				
				// Check if the action is contained 
				// in the stack's domain
				int actionDomIdx = actionTaskIdxToDomainIdx(machine, task);
				if (actionDomIdx >= 0) {					
					addToClause(d.atom(executes.get(machine), 
							actionDomIdx));
					
					add(-d.atom(executes.get(machine), actionDomIdx), 
							d.atom(execute, a));
				}
			}
			endClause();
		}
	}
	
	public void addCompleteParallelActionExclusion() {
		
		mode(FormulaType.universal);
		
		List<Integer> atoms = new ArrayList<>();
		for (int machine : domMachines) {
			atoms.add(d.atom(pop.get(machine)));
		}
		add(SatUtils.atMostOne(AtMostOneMode.quadratic, atoms, d));
	}
	
	public void addConflictingActionExclusion() {
		
		mode(FormulaType.universal);
		
		ActionConflicts conflicts = new ActionConflicts(problem);
		
		for (int action : domActions) {
			List<Integer> c = conflicts.getConflicts(action);
			for (int action2 : c) {
				add(-d.atom(execute, action), -d.atom(execute, action2));
			}
		}
	}
	
	/**
	 * Changes the formula to which the following clauses 
	 * will be added to. Must be called at the start of
	 * each clause-adding method as well as before any
	 * mode switches inbetween a method.
	 * @param type <i>initial</i>, <i>goal</i>, 
	 * <i>universal</i> or <i>transitional</i>
	 */
	private void mode(FormulaType type) {
		
		currentFormulaMode = type;
	}
	
	/**
	 * Adds the literals as a new clause to the
	 * currently active SatFormula.
	 */
	private void add(Integer... literals) {
		
		f.addClause(currentFormulaMode, new SatClause(literals));
	}
	
	/**
	 * Adds all clauses of the provided formula 
	 * to the currently active SatFormula.
	 */
	private void add(SatFormula f) {
		
		this.f.addAllClauses(currentFormulaMode, f);
	}
	
	/**
	 * Adds all provided literals to the currently
	 * active clause, or to a new clause if there is
	 * no current active one.<br/>
	 * The active clause must then be concluded
	 * with endClause().
	 */
	private void addToClause(Integer... literals) {
		
		if (currentClause == null)
			currentClause = new SatClause();
		
		for (int lit : literals)
			currentClause.add(lit);
	}
	
	/**
	 * Concludes the clause that has been added to
	 * by previous addToClause(*) calls, and adds it
	 * to the currently active formula.
	 */
	private void endClause() {
		
		if (currentClause != null) {
			f.addClause(currentFormulaMode, currentClause);
			currentClause = null;
		}
	}
	
	/**
	 * @param fact a fact represented by an index
	 * of data.getRelevantFacts().
	 * @return an atom representing this fact in the
	 * valid AtomDomain
	 */
	private int fact(int fact) {
		
		int factCompressed = data.compressFact(fact);
		boolean factHolds = data.isFactPositive(fact);
		return (factHolds ? 1 : -1) * d.atom(holds, factCompressed);
	}
	
	/**
	 * @param lit a literal for the current state
	 * @return the equivalent literal for the <i>next</i> state
	 * in the incremental definition
	 */
	private int nextStep(int lit) {
		
		Assert.that(!d.isModifiable(), "The domain is still modifiable; "
				+ "any \"next step\" variables could become erroneous.");
		
		boolean pos = lit > 0;
		lit = Math.abs(lit);
		
		return (pos ? 1 : -1) * (lit + d.getTotalSize());
	}
	
	private int taskDomainIdxToTaskIdx(int machineIdx, int domainIdx) {
		return -1;
//		return data.getReachableTasksFor(machineIdx).get(domainIdx);
	}
	
	private int taskIdxToTaskDomainIdx(int machineIdx, int taskIdx) {
		return -1;
//		return data.getReachableTasksFor(machineIdx).indexOf(taskIdx);
	}
	
	/**
	 * @param taskIdx the index of an action in
	 * data.getReachablePrimitiveTasks()
	 * @return the index of the 
	 * corresponding action in the encoded domain 
	 * (i.e. a valid value in the action AtomDomain)
	 */
	private int actionTaskIdxToDomainIdx(int machineIdx, int taskIdx) {
		return -1;
//		return data.getReachablePrimitiveTasksFor(machineIdx).indexOf(taskIdx);
	}

	/**
	 * Returns the created IncSatFormula object.
	 */
	@Override
	public ISatFormula getFormula() {
		
		return f;
	}
	
	/**
	 * Creates a StackDecoder object which can be used
	 * to convert atom values from a result to the
	 * corresponding stack content.
	 */
	public StackDecoder[] getStackDecoders() {
		
		StackDecoder[] decoders = new StackDecoder[domMachines.getSize()];
		for (int m = 0; m < decoders.length; m++) {			
			StackDecoder decoder = new StackDecoder(taskStacks.get(m), 
					d, stackBottoms.get(m));
			decoders[m] = decoder;
		}
		return decoders;
	}
}
