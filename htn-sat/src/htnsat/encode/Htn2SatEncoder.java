package htnsat.encode;

import htnsat.decode.Sat2HtnDecoder;
import htnsat.htn.HtnProblemData;
import htnsat.sat.ISatFormula;

/**
 * Takes an HTN planning problem and offers a conversion
 * into a propositional logic formula in CNF, or
 * respectively a set of CNF formulae for an incremental 
 * solving process.
 * 
 * @author Dominik Schreiber
 */
public class Htn2SatEncoder {
	
	/**
	 * The mode of an HTN to SAT encoding.
	 * 
	 * @author Dominik Schreiber
	 */
	public enum Mode {
		
		// Discontinued in favor of using Madagascar instead
		// primitiveSequential, 
		// primitiveParallel, 
		
		// Initial approach (based on 1998)
		linearForward,
		
		// SMS implementations with convenient configurations
		// (unary method encoding and binary task encoding
		// both "work well")
		smsUnaryMethods, 
		smsBinaryTasks, 
		
		// Currently not working
		// TODO fix and implement this?
		smsUnaryTasks,

		// Discontinued as the method expansions become very ugly
		// with binary encoding of methods, and as such will
		// certainly propagate comparably slowly
		// smsBinaryMethods,
		
		// Discontinued due to time constraints
		// and the encoding not being significantly faster
		// than its "single-threaded" siblings (as it is)
		// mms,
		
		// Discontinued 
		// (DIMSPEC-compliant, but far inferior prototype to T-REX)
		// sir,
		
		// Best approach (by many orders of magnitude);
		// outputs abstract, non-DIMACS encoding file
		// to be interpreted by T-REX application
		tRex,
		
		tRexTasks,
		
		gtohp;
	};
	
	/**
	 * The used mode of encoding
	 */
	private Mode mode;
	/**
	 * The HtnProblem instance to encode
	 */
	private HtnProblemData data;
	/**
	 * The encoding engine that is used
	 */
	private AbstractHtnClauses htnClauses;

	/**
	 * Encodes an HTN planning problem into a propositional logic formula.
	 * 
	 * @param domain
	 * @param problem
	 */
	public ISatFormula encode(HtnProblemData data, Mode mode, int numSteps) {
		
		this.mode = mode;
		this.data = data;
		
		// Encode the problem
		addTerms(numSteps);
		
		return htnClauses.getFormula();		
	}
	
	/**
	 * Encodes the problem.
	 * @param maxDepth amount of maximal steps the problem 
	 * should be solved for; only relevant for non-incremental 
	 * encodings
	 */
	public void addTerms(int maxDepth) {
		
		if (mode == Mode.linearForward) {
			
			// TODO implement this for HtnProblemData, too
			//problem.computeMaxPrimitiveStepsOfTasks(numSteps);
			/*
			HtnClausesLF htnClauses = new HtnClausesLF(data, maxDepth);
			
			// Basic planning clauses
			htnClauses.addInitialState();
			htnClauses.addActionPreconditionsAndEffects();
			htnClauses.addAtLeastOneActionPerStep();
			htnClauses.addAtMostOneActionPerStep();
			htnClauses.addActionFrameAxioms();
			htnClauses.addInitialTaskNetworkExplicitConstraints();
			
			// HTN planning constraints
			htnClauses.addMethodPreconditions();
			htnClauses.addInitialTaskNetworkCompletion();
			htnClauses.addInitialTaskNetworkEarliestStart();
			htnClauses.addReductionAxioms();
			htnClauses.addValidGrammarForTaskStartAndEnd();
			htnClauses.addReductionSubtaskCompletion();
			htnClauses.addInitialTaskNetworkTotalSuccession();
			htnClauses.addMethodOrdering();
			htnClauses.addReductionBorderSubtasks();
			*/
			
			HtnClausesLF2 htnClauses = new HtnClausesLF2(data, maxDepth);
			
			// Basic planning clauses
			htnClauses.addInitialState();
			htnClauses.addActionPreconditionsAndEffects();
			htnClauses.addAtLeastOneActionPerStep();
			htnClauses.addAtMostOneActionPerStep();
			htnClauses.addActionFrameAxioms();
			htnClauses.addGoalFacts();
			
			// HTN constraints
			htnClauses.addInitialTaskNetworkConstraints();
			htnClauses.addMethodPreconditions();
			htnClauses.addNonprimTaskReductionChoice();
			htnClauses.addReductionSubtaskCompletion();
			htnClauses.addValidGrammarForTaskStartAndEnd();
			htnClauses.addBorderSubtaskExecution();
			htnClauses.addMethodOrdering();
			System.out.println("clauses added");
			
			this.htnClauses = htnClauses;
			
		} else if (mode == Mode.smsBinaryTasks) {
			
			HtnClausesSMSBinaryTasks htnClauses = 
					new HtnClausesSMSBinaryTasks(data, maxDepth);
			
			// AMO encodings
			htnClauses.addAtMostOneAction();
			htnClauses.addPushPopActionExclusion();
			
			htnClauses.concludeDomain();
			
			// Classic planning constraints
			htnClauses.addInitialFacts();
			htnClauses.addActionPreconditions();
			htnClauses.addActionEffects(); // !
			htnClauses.addActionFrameAxioms(); // !
			htnClauses.addClassicFactGoal();

			// Stack-based HTN constraints
			htnClauses.addInitialStackState(); // !
			htnClauses.addGoalCondition();
			htnClauses.addStackMovements(); // !
			htnClauses.addStackTopImplications();
			htnClauses.addFactConstancyWhenPush();
			
			this.htnClauses = htnClauses;
			
		} else if (mode == Mode.smsUnaryTasks) {

			// FIXME test
			
			HtnClausesSMSUnaryTasks 
			htnClauses = new HtnClausesSMSUnaryTasks
					(data, maxDepth);
			
			// AMO encodings
			htnClauses.addAtMostOneAction();
			htnClauses.addPushPopActionExclusion();
			
			htnClauses.concludeDomain();
			
			// Classic planning constraints
			htnClauses.addInitialFacts();
			htnClauses.addActionPreconditions();
			htnClauses.addActionEffects();
			htnClauses.addActionFrameAxioms();
			htnClauses.addClassicFactGoal();

			// Stack-based HTN constraints
			htnClauses.addInitialStackState();
			htnClauses.addGoalCondition();
			htnClauses.addStackMovements();
			htnClauses.addStackTopImplications();
			htnClauses.addFactConstancyWhenPush();
			// TODO correct without these?
			//htnClauses.addTaskLinkWithBinaryStack();
			
			this.htnClauses = htnClauses;
		
		} else if (mode == Mode.smsUnaryMethods) {
			
			HtnClausesSMSUnaryMethods htnClauses 
			= new HtnClausesSMSUnaryMethods(data, maxDepth);
			
			// At-most-one clauses modifying the domain
			htnClauses.addAtMostOneAction();
			
			htnClauses.concludeDomain();
			
			// Classic planning constraints
			htnClauses.addInitialFacts();
			htnClauses.addActionPreconditions();
			htnClauses.addActionEffects();
			htnClauses.addActionFrameAxioms();
			htnClauses.addClassicFactGoal();
			
			// Stack-based HTN constraints
			htnClauses.addInitialStackState();
			htnClauses.addGoalCondition();
			htnClauses.addStackMovements();
			htnClauses.addFactConstancyWhenPush();
			htnClauses.addPushPopActionExclusion();
			htnClauses.addStackTopImplications();
			htnClauses.addAtMostOneMethodAndAction();
			
			this.htnClauses = htnClauses;
		
		} else if (mode == Mode.tRexTasks) {
			
				HtnClausesTRexTaskBased htnClauses = new HtnClausesTRexTaskBased(data);
				
				// The domain remains unmodified, as all
				// AMO constraints are added dynamically
				// later within the T-REX application
				htnClauses.concludeDomain();
				
				//htnClauses.addInitialMethods();			
				htnClauses.addInitialTasks();
				htnClauses.addInitialFacts();
				htnClauses.addActionPreconditions();
				htnClauses.addActionEffects();
				htnClauses.addTaskReductions();
				htnClauses.addMethodPreconditions();
				htnClauses.addNonprimitiveTransitions();
				
				htnClauses.addGoalCondition();
				
				this.htnClauses = htnClauses;
				
		} else if (mode == Mode.tRex) {
			
			HtnClausesTRex htnClauses = new HtnClausesTRex(data);
			
			// The domain remains unmodified, as all
			// AMO constraints are added dynamically
			// later within the T-REX application
			htnClauses.concludeDomain();
			
			htnClauses.addInitialMethods();
			htnClauses.addInitialFacts();
			htnClauses.addActionPreconditions();
			htnClauses.addActionEffects();
			htnClauses.addMethodPreconditions();
			htnClauses.addNonprimitiveTransitions();
			
			htnClauses.addGoalCondition();
			
			this.htnClauses = htnClauses;
			
		} else {
			throw new IllegalArgumentException(
					"No valid encoding mode provided.");
		}
	}		

	/**
	 * Returns a Sat2HtnDecoder instance which can read,
	 * interpret and output the plan provided by a SatSolver
	 * which solves the problem encoded by this object.
	 */
	public Sat2HtnDecoder getDecoder() {
		
		Sat2HtnDecoder decoder;
		decoder = new Sat2HtnDecoder(data, htnClauses.getDomain(), mode);			

		// Add the binary encoding information of the stack
		/*if (mode == Mode.smsBinaryMethods) {
			HtnClausesSMSMethodBased htnClauses = 
					(HtnClausesSMSMethodBased) this.htnClauses;
			decoder.setStackDecoder(htnClauses.getStackDecoder());
		} else */ if (mode == Mode.smsBinaryTasks) {
			HtnClausesSMSBinaryTasks htnClauses = 
				(HtnClausesSMSBinaryTasks) this.htnClauses;
			decoder.setStackDecoder(htnClauses.getStackDecoder());
		}/* else if (mode == Mode.smsUnaryTasks) {
			HtnClausesSMSUnaryTasks htnClauses = 
				(HtnClausesSMSUnaryTasks) this.htnClauses;
			decoder.setStackDecoder(htnClauses.getStackDecoder());
		} else if (mode == Mode.mms) {
			HtnClausesMMS htnClauses = (HtnClausesMMS) this.htnClauses;
			decoder.setStackDecoder(htnClauses.getStackDecoders());
		}*/
		
		return decoder;
	}
}