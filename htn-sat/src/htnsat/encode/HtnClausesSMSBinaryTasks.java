package htnsat.encode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import htnsat.sat.ISatFormula;
import htnsat.decode.StackDecoder;
import htnsat.htn.HtnAction;
import htnsat.htn.HtnMethod;
import htnsat.htn.HtnProblemData;
import htnsat.sat.AtomDomain;
import htnsat.sat.AtomType;
import htnsat.sat.BinaryEncoding;
import htnsat.sat.IncSatFormula;
import htnsat.sat.SatDomain;
import htnsat.sat.SatFormula;
import htnsat.sat.SatUtils;
import htnsat.sat.SatUtils.AtMostOneMode;
import htnsat.util.Assert;
import htnsat.sat.IncSatFormula.FormulaType;
import htnsat.sat.SatClause;

/**
 * <b>S</b>tack <b>M</b>achine <b>S</b>imulation encoding.
 * 
 * @author Dominik Schreiber
 */
public class HtnClausesSMSBinaryTasks extends AbstractHtnClauses {

	/**
	 * The value representing the stack's bottom.
	 */
	private int stackBottom;
	
	// AtomDomains which are used for encoding
	private AtomDomain domFacts;
	private AtomDomain domActions;
	private AtomDomain domStack;
	private AtomDomain domReductions;
	private AtomDomain domPushSize;
	
	// Helper AtomDomains to easily iterate 
	// over possible values
	private List<BinaryEncoding> taskStack;
	private AtomDomain domTasks;
	private AtomDomain domTaskBits;
	private AtomDomain domRealActions;
	
	// Used AtomTypes
	private AtomType holds;
	private AtomType execute;
	private AtomType pop;
	private AtomType push;
	private AtomType reduction;
	//private AtomType stackLoadedAt;
	
	// Resulting incremental formula
	private IncSatFormula f;
	
	// Helpers to conveniently create clauses
	private FormulaType currentFormulaMode;
	private SatClause currentClause;
	
	private int maxStackSize;
	
	private HtnProblemData data;
		
	public HtnClausesSMSBinaryTasks(HtnProblemData data, 
			int stackSize) {
		
		super(null, true); // incremental = true
		
		this.data = data;
		
		f = new IncSatFormula();
		this.maxStackSize = stackSize;
		
		// Domains
		
		int numTasks = data.getTasks().size();
		int maxReductionsPerTask = data.getMaxReductionsPerTask();
		int maxSubtasksPerRed = data.getMaxExpansionSize();
		domFacts = new AtomDomain(0, data.getCompressedFacts().size());
		domActions = new AtomDomain(0, 
				data.getPrimitiveTasks().size()+1);
		domRealActions = new AtomDomain(0, data.getPrimitiveTasks().size());
		//noAction = data.getReachablePrimitiveTasks().size();
		domStack = new AtomDomain(0, stackSize);
		domReductions = new AtomDomain(0, maxReductionsPerTask);
		domTasks = new AtomDomain(0, numTasks);
		domPushSize = new AtomDomain(0, Math.min(stackSize, maxSubtasksPerRed));
		stackBottom = numTasks;
		
		// Atom types
		
		holds = new AtomType(AtomTag.FACT_AT_STEP, domFacts);
		execute = new AtomType(AtomTag.ACTION_AT_STEP_AS_ACTION_IDX, domActions);
		pop = new AtomType(AtomTag.POP);
		push = new AtomType(AtomTag.PUSH, domPushSize);
		reduction = new AtomType(domReductions);
		//stackLoadedAt = new AtomType(domStack);
		
		List<AtomType> allAtomTypes = Arrays.asList(holds, execute, pop, 
				push, reduction //, stackLoadedAt
				);
		d = new SatDomain(allAtomTypes);
		
		// Initialize binary-encoded stack of methods
		
		taskStack = new ArrayList<>();
		for (int pos = 0; pos < stackSize; pos++) {
			taskStack.add(new BinaryEncoding(AtomTag.BINARY_ENCODING, 
					d, numTasks+1));
		}
		domTaskBits = new AtomDomain(0, taskStack.get(0).getBitLength());	
	}
	
	/**
	 * Adds clauses ensuring that the binary encodings of the stack
	 * will not take any values out of the method domain.
	 * 
	 * <br/>Usually unnecessary to add, as the stack content is 
	 * well-defined for each step on any relevant position.
	 * 
	 * <br/><br/>Type: <b>universal</b>
	 */
	public void addForbiddenBinaryValues() {
		
		mode(FormulaType.universal);
		
		for (int pos = 0; pos < maxStackSize; pos++) {
			add(taskStack.get(pos).getForbiddenValues());
		}
	}
	
	/**
	 * Completely defines all of the relevant facts
	 * for the initial state, according to the problem 
	 * definition.
	 * 
	 * <br/><br/>Type: <b>initial</b>
	 */
	public void addInitialFacts() {
		
		mode(FormulaType.initial);
		
		// Facts
		List<Integer> initFacts = new ArrayList<>();
		// Each fact from the initial state holds at t=0
		for (int p : data.getInitFacts()) {
			add(fact(p));
			initFacts.add(data.compressFact(p));
		}
		// Each non-specified fact does NOT hold at t=0
		for (int p : domFacts) {
			if (!initFacts.contains(p)) {
				add(-d.atom(holds, p));
			}
		}
	}
	
	/**
	 * Adds clauses for the initial state that ensure that 
	 * the initial task is on top of the stack and the bottom
	 * symbol is on the following position.
	 * 
	 * <br/>All other stack positions are undefined at the initial 
	 * state as they will never be reached in a consistent assignment.
	 * 
	 * <br/><br/>Type: <b>initial</b>
	 */
	public void addInitialStackState() {
		
		mode(FormulaType.initial);
		
		// Initial elements to process are the initial tasks
		int stackPos = 0;
		for (int task : data.getInitTasks()) {
			
			Integer[] literals = taskStack.get(stackPos)
					.posLiterals(task);
			for (int lit : literals) {
				add(lit);
			}
			stackPos++;
		}
		
		// The next element is the bottom symbol
		Integer[] literals = taskStack.get(stackPos).posLiterals(stackBottom);
		for (int lit : literals) {
			add(lit);
		}
		
		// The stack size
//		for (int i = 0; i < stackPos; i++) {			
//			add(d.atom(stackLoadedAt, i));
//		}
	}
	
	/**
	 * Adds the single unit clause stating that the bottom symbol
	 * must be on top of the stack in the final state, i.e.
	 * the stack is empty.
	 * 
	 * <br/><br/>Type: <b>goal</b>
	 */
	public void addGoalCondition() {
		
		mode(FormulaType.goal);
		
		// To be a valid plan, the stack must be empty in the end,
		// i.e. the bottom symbol is on top of the stack
		Integer[] literals = taskStack.get(0).posLiterals(stackBottom);
		for (int lit : literals) {
			add(lit);
		}
		
		// Alternatively: the stack size must be 0
		//add(-d.atom(stackLoadedAt, 0));
	}
	
	/**
	 * Adds goal conditions in a classical planning fashion,
	 * i.e. all goal predicates must hold in the final state.
	 * 
	 * <br/>Unnecessary, if the full encoding is used. Convenient
	 * for debugging (only add clauses corresponding to classical 
	 * planning, test if a plan is found that way).
	 * 
	 * <br/>The goal conditions are extracted from the after 
	 * constraints of the initial task.
	 * 
	 * <br/><br/>Type: <b>goal</b>
	 */
	public void addClassicFactGoal() {
		
		mode(FormulaType.goal);
		
		for (int p : data.getGoalFacts()) {
			add(fact(p));
		}
	}
	
	/**
	 * Adds clauses ensuring that at most one action is
	 * executed for any given step.
	 * 
	 * <br/>Used encoding: tree-like at-most-one
	 * 
	 * <br/><br/>Type: <b>universal</b>
	 */
	public void addAtMostOneAction() {
		
		mode(FormulaType.universal);
		
		List<Integer> atoms = new ArrayList<>();
		for (int a : domActions) {
			atoms.add(d.atom(execute, a));
		}
		add(SatUtils.atMostOne(AtMostOneMode.tree, atoms, d));
	}
	
	/**
	 * Adds clauses that require an action's preconditions to hold
	 * whenever it is executed.
	 * 
	 * <br/><br/>Type: <b>universal</b>
	 */
	public void addActionPreconditions() {
		
		mode(FormulaType.universal);
		
		for (int a : domRealActions) {
			int task = data.actionFlatIdxToTaskIdx(a);
			HtnAction action = data.getActionOfTask(task);
			
			int litAction = -d.atom(execute, a);
			for (int p : action.getPreconditionsPos()) {
				add(litAction, fact(p));
			}
			for (int p : action.getPreconditionsNeg()) {
				add(litAction, -fact(p));
			}
		}
	}
	
	/**
	 * Adds clauses ensuring that an action's effects hold
	 * whenever the action is executed.
	 * 
	 * <br/><br/>Type: <b>transitional</b>
	 */
	public void addActionEffects() {
		
		mode(FormulaType.transitional);
		
		for (int a : domRealActions) {
			int task = data.actionFlatIdxToTaskIdx(a);
			HtnAction action = data.getActionOfTask(task);
			
			int litAction = -d.atom(execute, a);
			for (int p : action.getEffectsPos()) {
				add(litAction, nextStep(fact(p)));
			}
			for (int p : action.getEffectsNeg()) {
				add(litAction, -nextStep(fact(p)));
			}
		}
	}
	
	/**
	 * Adds clauses ensuring that the change of any fact
	 * implies the execution of a supporting action.
	 * 
	 * <br/><br/>Type: <b>transitional</b>
	 */
	public void addActionFrameAxioms() {
		
		mode(FormulaType.transitional);
		
		for (int p : domFacts) {
			
			int pUncompr = data.uncompressFact(p);
			// if p is false @t and true @t+1, then
			// any of the supporting actions is active @t
			addToClause(d.atom(holds, p), -nextStep(d.atom(holds, p)));
			for (int task : data.getActionTasksWithEffect(pUncompr, true)) {
				int a = data.actionTaskIdxToFlatIdx(task);
				addToClause(d.atom(execute, a));
			}
			endClause();
			// if p is true @t and false @t+1, then
			// any of the supporting actions is active @t
			addToClause(-d.atom(holds, p), nextStep(d.atom(holds, p)));
			for (int task : data.getActionTasksWithEffect(pUncompr, false)) {
				int a = data.actionTaskIdxToFlatIdx(task);
				addToClause(d.atom(execute, a));
			}
			endClause();
		}
	}
	
	/*
	public void addStackSizePropagation() {
		
		mode(FormulaType.transitional);
		
		// An empty stack does not support pop()
		add(-d.atom(stackLoadedAt, 0), -d.atom(pop));

		for (int s : domStack) {
			
			if (s+1 < maxStackSize) {
				
				// If stack is loaded @s but unloaded @s+1,
				// position s will become unloaded after a pop
				add(-d.atom(stackLoadedAt, s), 
					d.atom(stackLoadedAt, s+1), 
					-d.atom(pop), 
					-nextStep(d.atom(stackLoadedAt, s)));
				// If stack is loaded @s and also @s+1,
				// position s will stay loaded after a pop
				add(-d.atom(stackLoadedAt, s), 
					-d.atom(stackLoadedAt, s+1), 
					-d.atom(pop), 
					nextStep(d.atom(stackLoadedAt, s)));
				
			} else {
				// The last position on the stack
				// will unload if a pop() is done
				add(-d.atom(pop), 
					-nextStep(d.atom(stackLoadedAt, s)));
			}
			
			// If no pop() is done, a loaded position 
			// will stay loaded
			add(-d.atom(stackLoadedAt, s), 
					d.atom(pop), nextStep(d.atom(stackLoadedAt, s)));
			
			for (int k : domPushSize) {
				if (s+k < maxStackSize) {	
			
					// If the stack is loaded at position s,
					// then after a k-push it will be loaded at s+k
					add(-d.atom(stackLoadedAt, s), -d.atom(push, k), 
							nextStep(d.atom(stackLoadedAt, s+k)));
				} else {
					// Stack overflow:
					// prohibit this action
//					add(-d.atom(stackLoadedAt, s), -d.atom(push, k));
				}
			}
		}
	}*/
	
	/**
	 * Ensures that pop and push are mutually exclusive
	 * and that no action is executed whenever a pop or a push
	 * is done.
	 * 
	 * <br/><br/>Type: <b>universal</b>
	 */
	public void addPushPopActionExclusion() {
		
		mode(FormulaType.universal);
		
		List<Integer> atoms = new ArrayList<>();
		for (int k : domPushSize) {
			atoms.add(d.atom(push, k));
		}
		atoms.add(d.atom(pop));

		// At least one pop/push action is executed
		for (int atom : atoms) {
			addToClause(atom);
		}
		endClause();
		
		// At most one pop/push action is executed
		add(SatUtils.atMostOne(AtMostOneMode.quadratic, atoms, d));

		// pop() is done iff an action is executed
		// (i.e. the no-action is NOT executed)
		//add(-d.atom(pop), -d.atom(execute, noAction));
		//add(d.atom(pop), d.atom(execute, noAction));
		
	}
	
	public void concludeDomain() {
		
		// After this method call, all necessary helper
		// variables must have been added
		d.concludeDomain();
		f.setEncodedNumVars(d.getTotalSize());
	}
	
	/**
	 * Redundant clauses that specify that a fact will not change its value
	 * whenever no pop() is done.
	 */
	public void addFactConstancyWhenPush() {
		
		mode(FormulaType.transitional);
		
		for (int p : domFacts) {			
			add(d.atom(pop), -d.atom(holds, p), nextStep(d.atom(holds, p)));
			add(d.atom(pop), d.atom(holds, p), -nextStep(d.atom(holds, p)));
		}
	}

	/**
	 * Adds clauses that ensure that the stack's content
	 * will shift accordingly to pop() and push() operations
	 * and remain unchanged if no pop() and no push() is done. 
	 * 
	 * <br/><br/>Type: <b>transitional</b>
	 */
	public void addStackMovements() {
		
		mode(FormulaType.transitional);
		
		// Movement of task bits
		for (int k : domStack) {			
			for (int i : domTaskBits) {
				int atomAtK = taskStack.get(k).getAtomForBit(i);
				
				for (int numPush : domPushSize) {
						
					if (k > 0 && k+numPush < taskStack.size()) {
						
						int atomAtKPlus = taskStack.get(k+numPush).getAtomForBit(i);
						
						// If push() is done, all bits are shifted +numPush
						add(-atomAtK, -d.atom(push, numPush), 
//								-d.atom(stackLoadedAt, k), 
								nextStep(atomAtKPlus));
						add(atomAtK, -d.atom(push, numPush), 
//								-d.atom(stackLoadedAt, k), 
								-nextStep(atomAtKPlus));
					}
				}
				
				if (k > 0) {
					int atomAtKMinus1 = taskStack.get(k-1).getAtomForBit(i);

					// If pop() is done, all bits are shifted -1
					add(-atomAtK, -d.atom(pop), 
//							-d.atom(stackLoadedAt, k), 
							nextStep(atomAtKMinus1));
					add(atomAtK, -d.atom(pop), 
//							-d.atom(stackLoadedAt, k), 
							-nextStep(atomAtKMinus1));
				}
			}
		}
	}
	
	/**
	 * Adds clauses defining the necessary stack operation
	 * or the action to execute depending on what method and 
	 * what counter value is on top of the stack.
	 * <br/>Also checks <i>after</i> constraints of tasks before
	 * their pop() and <i>between</i> constraints between
	 * the two corresponding subtasks.
	 * 
	 * <br/><br/>Type: <b>universal</b>
	 */
	public void addStackTopImplications() {
			
		for (int t : domTasks) {
			
			Integer[] litTaskNeg = taskStack.get(0).negLiterals(t);

			mode(FormulaType.universal);
			int task = t;
			
			if (data.isTaskPrimitive(task)) {

				// Execute the action (implying a pop())
				addToClause(litTaskNeg);
				addToClause(d.atom(execute, data.actionTaskIdxToFlatIdx(task)));
				endClause();
				
				addToClause(litTaskNeg);
				addToClause(d.atom(pop));
				endClause();
				
				// Back direction: if the action is executed,
				// then the task needs to be on the stack's top
				for (int lit : litTaskNeg) {
					add(-d.atom(execute, data.actionTaskIdxToFlatIdx(task)), -lit);
				}
				
			} else {
				
				// Some reduction must be applied
				int numReductions = data.getMethodsOfTask(task).size();
				addToClause(litTaskNeg);
				for (int red : domReductions) {
					if (red >= numReductions)
						break;
					addToClause(d.atom(reduction, red));
				}
				endClause();
				
				// For each reduction: if applied, then push all subtasks
				// and check precondition
				for (int red : domReductions) {
					if (red >= numReductions)
						break;
					
					List<Integer> subtasks = 
							data.getMethodsOfTask(task).get(red).getExpansion();
					for (int subtaskIdx = 0; subtaskIdx < subtasks.size(); 
							subtaskIdx++) {
						int subtask = subtasks.get(subtaskIdx);
						for (int lit : taskStack.get(subtaskIdx)
								.posLiterals(subtask)) {
							// If task on top and reduction applied,
							// then each respective subtask bit on the task stack is set
							mode(FormulaType.transitional);
							addToClause(litTaskNeg);
							addToClause(-d.atom(reduction, red), nextStep(lit));
							endClause();
						}
					}
					mode(FormulaType.universal);
					
					// All subtasks are pushed, but the current task vanishes 
					// => push(numSubtasks -1)
					addToClause(litTaskNeg);
					addToClause(-d.atom(reduction, red), 
							d.atom(push, subtasks.size()-1));
					endClause();
					
					// Before constraints must hold, now and at the next step
					HtnMethod method = data.getMethodsOfTask(task).get(red);
					for (int p : method.getBeforeConstraints()) {
						mode(FormulaType.universal);
						addToClause(litTaskNeg);
						addToClause(-d.atom(reduction, red), fact(p));
						endClause();
						mode(FormulaType.transitional);
						addToClause(litTaskNeg);
						addToClause(-d.atom(reduction, red), nextStep(fact(p)));
						endClause();
					}
				}
			}
		}
	}	
	
	/**
	 * Changes the formula to which the following clauses 
	 * will be added to. Must be called at the start of
	 * each clause-adding method as well as before any
	 * mode switches inbetween a method.
	 * @param type <i>initial</i>, <i>goal</i>, 
	 * <i>universal</i> or <i>transitional</i>
	 */
	private void mode(FormulaType type) {
		
		currentFormulaMode = type;
	}
	
	/**
	 * Adds the literals as a new clause to the
	 * currently active SatFormula.
	 */
	private void add(Integer... literals) {
		
		f.addClause(currentFormulaMode, new SatClause(literals));
	}
	
	/**
	 * Adds all clauses of the provided formula 
	 * to the currently active SatFormula.
	 */
	private void add(SatFormula f) {
		
		this.f.addAllClauses(currentFormulaMode, f);
	}
	
	/**
	 * Adds all provided literals to the currently
	 * active clause, or to a new clause if there is
	 * no current active one.<br/>
	 * The active clause must then be concluded
	 * with endClause().
	 */
	private void addToClause(Integer... literals) {
		
		if (currentClause == null)
			currentClause = new SatClause();
		
		for (int lit : literals)
			currentClause.add(lit);
	}
	
	/**
	 * Concludes the clause that has been added to
	 * by previous addToClause(*) calls, and adds it
	 * to the currently active formula.
	 */
	private void endClause() {
		
		if (currentClause != null) {
			f.addClause(currentFormulaMode, currentClause);
			currentClause = null;
		}
	}
	
	/**
	 * @param fact a fact represented by an index
	 * of data.getRelevantFacts().
	 * @return an atom representing this fact in the
	 * valid AtomDomain
	 */
	private int fact(int fact) {
		
		int factCompressed = data.compressFact(fact);
		boolean factHolds = data.isFactPositive(fact);
		return (factHolds ? 1 : -1) * d.atom(holds, factCompressed);
	}
	
	/**
	 * @param lit a literal for the current state
	 * @return the equivalent literal for the <i>next</i> state
	 * in the incremental definition
	 */
	private int nextStep(int lit) {
		
		Assert.that(!d.isModifiable(), "The domain is still modifiable; "
				+ "any \"next step\" variables could become erroneous.");
		
		boolean pos = lit > 0;
		lit = Math.abs(lit);
		
		return (pos ? 1 : -1) * (lit + d.getTotalSize());
	}
	
	/**
	 * Returns the created IncSatFormula object.
	 */
	@Override
	public ISatFormula getFormula() {
		
		return f;
	}
	
	/**
	 * Creates a StackDecoder object which can be used
	 * to convert atom values from a result to the
	 * corresponding stack content.
	 */
	public StackDecoder getStackDecoder() {
		
		StackDecoder decoder = new StackDecoder(taskStack, d, stackBottom);
		return decoder;
	}
}
