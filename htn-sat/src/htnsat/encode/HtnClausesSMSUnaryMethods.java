 package htnsat.encode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import htnsat.sat.ISatFormula;
import htnsat.htn.HtnAction;
import htnsat.htn.HtnMethod;
import htnsat.htn.HtnProblemData;
import htnsat.sat.AtomDomain;
import htnsat.sat.AtomType;
import htnsat.sat.IncSatFormula;
import htnsat.sat.SatDomain;
import htnsat.sat.SatFormula;
import htnsat.sat.SatUtils;
import htnsat.sat.SatUtils.AtMostOneMode;
import htnsat.util.Assert;
import htnsat.sat.IncSatFormula.FormulaType;
import htnsat.sat.SatClause;

/**
 * <b>S</b>tack <b>M</b>achine <b>S</b>imulation encoding.
 * 
 * @author Dominik Schreiber
 */
public class HtnClausesSMSUnaryMethods extends AbstractHtnClauses {

	/**
	 * The value representing the stack's bottom.
	 */
	private int bottomMethod;
	private int initMethod;
	
	// AtomDomains which are used for encoding
	private AtomDomain domFacts;
	private AtomDomain domActions;
	private AtomDomain domMethods;
	private AtomDomain domStack;
	private AtomDomain domPushSize;
	
	// Helper AtomDomains to easily iterate 
	// over possible values
	private AtomDomain domRealMethods;
	
	// Used AtomTypes
	private AtomType holds;
	private AtomType execute;
	private AtomType actionAt;
	private AtomType methodAt;
	private AtomType pop;
	private AtomType push;
	
	// Resulting incremental formula
	private IncSatFormula f;
	
	// Helpers to conveniently create clauses
	private FormulaType currentFormulaMode;
	private SatClause currentClause;
	
	private int maxStackSize;
	private int maxExpansionSize;
		
	private HtnProblemData data;
	
	public HtnClausesSMSUnaryMethods(HtnProblemData data, 
			int stackSize) {
		
		super(null, true); // incremental = true
		
		this.data = data;
		
		f = new IncSatFormula();
		this.maxStackSize = stackSize;
		
		// Domains
		
		maxExpansionSize = data.getMaxExpansionSize();
		domFacts = new AtomDomain(0, data.getCompressedFacts().size());
		
		domActions = new AtomDomain(0, data.getPrimitiveTasks().size());
		
		domMethods = new AtomDomain(0, data.getFlatMethods().size()+1);
		domRealMethods = new AtomDomain(0, data.getFlatMethods().size());
		bottomMethod = data.getFlatMethods().size();
		initMethod = 0;
		
		domStack = new AtomDomain(0, stackSize);
		domPushSize = new AtomDomain(0, Math.min(stackSize, maxExpansionSize));
		
		// Atom types
		
		holds = new AtomType(AtomTag.FACT_AT_STEP, domFacts);
		execute = new AtomType(AtomTag.ACTION_AT_STEP_AS_ACTION_IDX, domActions);
		pop = new AtomType(AtomTag.POP);
		push = new AtomType(AtomTag.PUSH_MULTI, domPushSize);
		actionAt = new AtomType(domStack, domActions);
		methodAt = new AtomType(domStack, domMethods);
		
		List<AtomType> allAtomTypes = Arrays.asList(holds, execute, pop, push, 
				actionAt, methodAt);
		d = new SatDomain(allAtomTypes);
	}
		
	/**
	 * Completely defines all of the relevant facts
	 * for the initial state, according to the problem 
	 * definition.
	 * 
	 * <br/><br/>Type: <b>initial</b>
	 */
	public void addInitialFacts() {
		
		mode(FormulaType.initial);
		
		// Facts
		List<Integer> initFacts = new ArrayList<>();
		// Each fact from the initial state holds at t=0
		for (int p : data.getInitFacts()) {
			 // TODO pos/neg distinction?
			add(fact(p));
			initFacts.add(data.compressFact(p));
		}
		// Each non-specified fact does NOT hold at t=0
		for (int p : domFacts) {
			if (!initFacts.contains(p)) {
				add(-d.atom(holds, p));
			}
		}
	}
	
	/**
	 * Adds clauses for the initial state that ensure that 
	 * the initial task is on top of the stack and the bottom
	 * symbol is on the following position.
	 * 
	 * <br/>All other stack positions are undefined at the initial 
	 * state as they will never be reached in a consistent assignment.
	 * 
	 * <br/><br/>Type: <b>initial</b>
	 */
	public void addInitialStackState() {
		
		mode(FormulaType.initial);
		
		// Initial elements to process are the initial tasks
		int stackPos = 0;
		for (int task : data.getInitTasks()) {

			List<Integer> possibleActions = new ArrayList<>();
			List<Integer> possibleMethods = new ArrayList<>();
			
			// Add clauses for possible actions / methods
			if (data.isTaskPrimitive(task)) {
				int actionId = data.actionTaskIdxToFlatIdx(task);
				add(d.atom(actionAt, stackPos, actionId));
				possibleActions.add(actionId);
			} else {
				List<HtnMethod> methods = data.getMethodsOfTask(task);
				for (HtnMethod method : methods) {
					int methodId = data.flatIdxOfMethod(method);
					addToClause(d.atom(methodAt, stackPos, methodId));
					possibleMethods.add(methodId);
				}
				endClause();
			}
			
			// Forbid everything else
			for (int action : domActions) {
				if (!possibleActions.contains(action)) {
					add(-d.atom(actionAt, stackPos, action));
				}
			}
			for (int method : domMethods) {
				if (!possibleMethods.contains(method)) {
					add(-d.atom(methodAt, stackPos, method));
				}
			}
			
			stackPos++;
		}
		
		// The next element is the bottom symbol
		add(d.atom(methodAt, stackPos, bottomMethod));
		
		// (It doesn't matter if this stack cell contains additional
		// methods / actions, as the computation ends before this cell
		// would be processed)
	}
	
	/**
	 * Adds the single unit clause stating that the bottom symbol
	 * must be on top of the stack in the final state, i.e.
	 * the stack is empty.
	 * 
	 * <br/><br/>Type: <b>goal</b>
	 */
	public void addGoalCondition() {
		
		mode(FormulaType.goal);
		
		// To be a valid plan, the stack must be empty in the end,
		// i.e. the bottom symbol is on top of the stack
		add(d.atom(methodAt, 0, bottomMethod));
	}
	
	/**
	 * Adds goal conditions in a classical planning fashion,
	 * i.e. all goal predicates must hold in the final state.
	 * 
	 * <br/>Unnecessary, if the full encoding is used. Convenient
	 * for debugging (only add clauses corresponding to classical 
	 * planning, test if a plan is found that way).
	 * 
	 * <br/>The goal conditions are extracted from the after 
	 * constraints of the initial task.
	 * 
	 * <br/><br/>Type: <b>goal</b>
	 */
	public void addClassicFactGoal() {
		
		mode(FormulaType.goal);
		
		for (int p : data.getGoalFacts()) {
			// TODO neg goals?
			add(fact(p));
		}
	}
	
	/**
	 * Adds clauses ensuring that at most one action is
	 * executed for any given step.
	 * 
	 * <br/>Used encoding: tree-like at-most-one
	 * 
	 * <br/><br/>Type: <b>universal</b>
	 */
	public void addAtMostOneAction() {
		
		mode(FormulaType.universal);
		
		List<Integer> atoms = new ArrayList<>();
		for (int a : domActions) {
			atoms.add(d.atom(execute, a));
		}
		add(SatUtils.atMostOne(AtMostOneMode.tree, atoms, d));
	}
	
	/**
	 * Adds clauses that require an action's preconditions to hold
	 * whenever it is executed.
	 * 
	 * <br/><br/>Type: <b>universal</b>
	 */
	public void addActionPreconditions() {
		
		mode(FormulaType.universal);
		
		for (int a : domActions) {
			int task = data.actionFlatIdxToTaskIdx(a);
			HtnAction action = data.getActionOfTask(task);
			
			int litAction = -d.atom(execute, a);
			for (int p : action.getPreconditionsPos()) {
				add(litAction, fact(p));
			}
			for (int p : action.getPreconditionsNeg()) {
				add(litAction, -fact(p));
			}
		}
	}
	
	/**
	 * Adds clauses ensuring that an action's effects hold
	 * whenever the action is executed.
	 * 
	 * <br/><br/>Type: <b>transitional</b>
	 */
	public void addActionEffects() {
		
		mode(FormulaType.transitional);
		
		for (int a : domActions) {
			int task = data.actionFlatIdxToTaskIdx(a);
			HtnAction action = data.getActionOfTask(task);
			
			int litAction = -d.atom(execute, a);
			for (int p : action.getEffectsPos()) {
				add(litAction, nextStep(fact(p)));
			}
			for (int p : action.getEffectsNeg()) {
				add(litAction, -nextStep(fact(p)));
			}
		}
	}
	
	/**
	 * Adds clauses ensuring that the change of any fact
	 * implies the execution of a supporting action.
	 * 
	 * <br/><br/>Type: <b>transitional</b>
	 */
	public void addActionFrameAxioms() {
		
		mode(FormulaType.transitional);
		
		for (int p : domFacts) {
			
			int pUncompr = data.uncompressFact(p);
			// if p is false @t and true @t+1, then
			// any of the supporting actions is active @t
			addToClause(d.atom(holds, p), -nextStep(d.atom(holds, p)));
			for (int task : data.getActionTasksWithEffect(pUncompr, true)) {
				int a = data.actionTaskIdxToFlatIdx(task);
				addToClause(d.atom(execute, a));
			}
			endClause();
			// if p is true @t and false @t+1, then
			// any of the supporting actions is active @t
			addToClause(-d.atom(holds, p), nextStep(d.atom(holds, p)));
			for (int task : data.getActionTasksWithEffect(pUncompr, false)) {
				int a = data.actionTaskIdxToFlatIdx(task);
				addToClause(d.atom(execute, a));
			}
			endClause();
		}
	}
	
	/**
	 * Ensures that pop and push are mutually exclusive
	 * and that no action is executed whenever a pop or a push
	 * is done.
	 * 
	 * <br/><br/>Type: <b>universal</b>
	 */
	public void addPushPopActionExclusion() {
		
		mode(FormulaType.universal);
		
		List<Integer> atoms = new ArrayList<>();
		for (int k : domPushSize) {
			atoms.add(d.atom(push, k));
		}
		atoms.add(d.atom(pop));

		// At least one pop/push action is executed
		for (int atom : atoms) {
			addToClause(atom);
		}
		endClause();
		
		// At most one pop/push action is executed
		add(SatUtils.atMostOne(AtMostOneMode.quadratic, atoms, d));

		// pop() is done iff an action is executed
		// (i.e. the no-action is NOT executed)
		//add(-d.atom(pop), -d.atom(execute, noAction));
		//add(d.atom(pop), d.atom(execute, noAction));
		
	}
	
	public void concludeDomain() {
		
		// After this method call, all necessary helper
		// variables must have been added
		d.concludeDomain();
		f.setEncodedNumVars(d.getTotalSize());
	}
	
	/**
	 * Redundant clauses that specify that a fact will not change its value
	 * whenever no pop() is done.
	 */
	public void addFactConstancyWhenPush() {
		
		mode(FormulaType.transitional);
		
		for (int p : domFacts) {			
			add(d.atom(pop), -d.atom(holds, p), nextStep(d.atom(holds, p)));
			add(d.atom(pop), d.atom(holds, p), -nextStep(d.atom(holds, p)));
		}
	}

	/**
	 * Adds clauses that ensure that the stack's content
	 * will shift accordingly to pop() and push() operations
	 * and remain unchanged if no pop() and no push() is done. 
	 * 
	 * <br/><br/>Type: <b>transitional</b>
	 */
	public void addStackMovements() {
		
		mode(FormulaType.transitional);
		
		// Movement of task bits
		for (int k : domStack) {
			
			if (k == 0)
				continue;
			
			// pop() operation for actions
			for (int action : domActions) {
				add(-d.atom(pop), -d.atom(actionAt, k, action), 
						nextStep(d.atom(actionAt, k-1, action)));
				add(-d.atom(pop), d.atom(actionAt, k, action), 
						-nextStep(d.atom(actionAt, k-1, action)));
			}
			
			// pop() operation for methods
			for (int method : domMethods) {		
				add(-d.atom(pop), -d.atom(methodAt, k, method), 
						nextStep(d.atom(methodAt, k-1, method)));
				add(-d.atom(pop), d.atom(methodAt, k, method), 
						-nextStep(d.atom(methodAt, k-1, method)));
			}
			
			for (int pushSize : domPushSize) {
				if (k+pushSize < maxStackSize) {
				
					// push(pushSize) operation for actions
					for (int action : domActions) {
						add(-d.atom(push, pushSize), -d.atom(actionAt, k, action), 
								nextStep(d.atom(actionAt, k+pushSize, action)));
						add(-d.atom(push, pushSize), d.atom(actionAt, k, action), 
								-nextStep(d.atom(actionAt, k+pushSize, action)));
					}
					
					// push(pushSize) operation for methods
					for (int method : domMethods) {
						add(-d.atom(push, pushSize), -d.atom(methodAt, k, method), 
								nextStep(d.atom(methodAt, k+pushSize, method)));
						add(-d.atom(push, pushSize), d.atom(methodAt, k, method), 
								-nextStep(d.atom(methodAt, k+pushSize, method)));
					}
				}
			}
		}
	}
	
	/**
	 * Adds clauses defining the necessary stack operation
	 * or the action to execute depending on what method and 
	 * what counter value is on top of the stack.
	 * <br/>Also checks <i>after</i> constraints of tasks before
	 * their pop() and <i>between</i> constraints between
	 * the two corresponding subtasks.
	 * 
	 * <br/><br/>Type: <b>universal</b>
	 */
	public void addStackTopImplications() {
		
		for (int action : domActions) {
			
			mode(FormulaType.universal);
			
			// Execute the action, and pop()
			add(-d.atom(actionAt, 0, action), d.atom(execute, action));
			add(-d.atom(actionAt, 0, action), d.atom(pop));
			
			// Back direction: if the action is executed,
			// then a pop() needs to be done
			add(-d.atom(execute, action), d.atom(pop));
		}
		
		for (int method : domRealMethods) {
		
			if (method == initMethod)
				continue;
			
			List<Integer> expansion = data.getFlatMethods().get(method).getExpansion();
			int expansionSize = expansion.size();
			
			// Define new stack content at the pushed indices
			if (expansionSize == 1 && data.isTaskPrimitive(expansion.get(0))) {

				mode(FormulaType.universal);
				
				// The method only contains a single task, which is primitive;
				// just execute the action, pop, and check preconditions
				add(-d.atom(methodAt, 0, method), d.atom(execute, 
						data.actionTaskIdxToFlatIdx(expansion.get(0))));
				add(-d.atom(methodAt, 0, method), d.atom(pop));
				
				// Preconditions need to hold
				HtnMethod m = data.getFlatMethods().get(method);
				for (int p : m.getBeforeConstraints()) {
					add(-d.atom(methodAt, 0, method), fact(p));
				}
				
			} else for (int expIdx = 0; expIdx < expansion.size(); expIdx++) {
				
				// Push number of subtasks, minus the disappearing current method
				mode(FormulaType.universal);
				add(-d.atom(methodAt, 0, method), d.atom(push, expansionSize-1));
				
				mode(FormulaType.transitional);
				
				int task = expansion.get(expIdx);
				if (data.isTaskPrimitive(task)) {
					
					// Action at position expIdx
					add(-d.atom(methodAt, 0, method), nextStep(d.atom(actionAt, expIdx, 
							data.actionTaskIdxToFlatIdx(task))));
				} else {
					
					// Some method at position expIdx
					List<HtnMethod> possibleMethods = data.getMethodsOfTask(task);
					addToClause(-d.atom(methodAt, 0, method));
					for (HtnMethod possibleMethod : possibleMethods) {
						int possMethodIdx = data.flatIdxOfMethod(possibleMethod);
						addToClause(nextStep(d.atom(methodAt, expIdx, possMethodIdx)));
					}
					endClause();
				}
			}
			
			// Before constraints of the method must hold
			mode(FormulaType.universal);
			HtnMethod m = data.getFlatMethods().get(method);
			for (int p : m.getBeforeConstraints()) {
				add(-d.atom(methodAt, 0, method), fact(p));
			}			
		}	
	}
	
	public void addAtMostOneMethodAndAction() {
		
		/*
		mode(FormulaType.universal);
		
		for (int stackPos = 0; stackPos < maxExpansionSize; stackPos++) {
			List<Integer> atoms = new ArrayList<>();
			for (int action : domActions) {
				atoms.add(d.atom(actionAt, stackPos, action));
			}
			for (int method : domMethods) {
				atoms.add(d.atom(methodAt, stackPos, method));
			}
			add(SatUtils.atMostOne(AtMostOneMode.tree, atoms, d));
		}*/
		
		mode(FormulaType.transitional);
		
		for (int stackPos = 0; stackPos < maxExpansionSize; stackPos++) {
			for (int pushSize : domPushSize) {
				add(d.atom(methodAt, stackPos, bottomMethod), -d.atom(push, pushSize), 
						-nextStep(d.atom(methodAt, stackPos, bottomMethod)));
			}
		}
	}
	
	/**
	 * Changes the formula to which the following clauses 
	 * will be added to. Must be called at the start of
	 * each clause-adding method as well as before any
	 * mode switches inbetween a method.
	 * @param type <i>initial</i>, <i>goal</i>, 
	 * <i>universal</i> or <i>transitional</i>
	 */
	private void mode(FormulaType type) {
		
		currentFormulaMode = type;
	}
	
	/**
	 * Adds the literals as a new clause to the
	 * currently active SatFormula.
	 */
	private void add(Integer... literals) {
		
		f.addClause(currentFormulaMode, new SatClause(literals));
	}
	
	/**
	 * Adds all clauses of the provided formula 
	 * to the currently active SatFormula.
	 */
	private void add(SatFormula f) {
		
		this.f.addAllClauses(currentFormulaMode, f);
	}
	
	/**
	 * Adds all provided literals to the currently
	 * active clause, or to a new clause if there is
	 * no current active one.<br/>
	 * The active clause must then be concluded
	 * with endClause().
	 */
	private void addToClause(Integer... literals) {
		
		if (currentClause == null)
			currentClause = new SatClause();
		
		for (int lit : literals)
			currentClause.add(lit);
	}
	
	/**
	 * Concludes the clause that has been added to
	 * by previous addToClause(*) calls, and adds it
	 * to the currently active formula.
	 */
	private void endClause() {
		
		if (currentClause != null) {
			f.addClause(currentFormulaMode, currentClause);
			currentClause = null;
		}
	}
	
	/**
	 * @param fact a fact represented by an index
	 * of problem.getRelevantFacts().
	 * @return an atom representing this fact in the
	 * valid AtomDomain
	 */
	private int fact(int fact) {
		
		int factCompressed = data.compressFact(fact);
		boolean factHolds = data.isFactPositive(fact);
		return (factHolds ? 1 : -1) * d.atom(holds, factCompressed);
	}
	
	/**
	 * @param lit a literal for the current state
	 * @return the equivalent literal for the <i>next</i> state
	 * in the incremental definition
	 */
	private int nextStep(int lit) {
		
		Assert.that(!d.isModifiable(), "The domain is still modifiable; "
				+ "any \"next step\" variables could become erroneous.");
		
		boolean pos = lit > 0;
		lit = Math.abs(lit);
		
		return (pos ? 1 : -1) * (lit + d.getTotalSize());
	}
	
	/**
	 * Returns the created IncSatFormula object.
	 */
	@Override
	public ISatFormula getFormula() {
		
		return f;
	}
}
