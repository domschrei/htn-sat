package htnsat.encode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import htnsat.htn.HtnAction;
import htnsat.htn.HtnMethod;
import htnsat.htn.HtnProblemData;
import htnsat.sat.AtomDomain;
import htnsat.sat.AtomType;
import htnsat.sat.SatClause;
import htnsat.sat.SatDomain;
import htnsat.sat.SatFormula;
import htnsat.sat.SatUtils;
import htnsat.sat.SatUtils.AtMostOneMode;

/**
 * <b>L</b>inear <b>F</b>orward encoding
 * <br/><br/>
 * 
 * Complexity notation:
 * 
 * <ul>
 * <li>s: maximum steps</li>
 * <li>p: facts</li>
 * <li>a: primitive tasks</li>
 * <li>t: non-primitive tasks</li>
 * <li>c: constraints per network task / method</li>
 * <li>i: tasks of initial network</li>
 * </ul>
 * 
 * @author Dominik Schreiber
 *
 */
public class HtnClausesLF extends AbstractHtnClauses {
	
	private int numSteps;
	private AtomDomain domainNonprimTasks;
	private AtomDomain domainAllTasks;
	private AtomDomain domainSteps;
	private AtomDomain domainStepsButLast;
	private AtomDomain domainFacts;
	private AtomDomain domainReductions;
	private AtomDomain domainActions;
	
	private AtomType actionAtStep;
	private AtomType factAtStep;
	private AtomType taskStartsAtStep;
	private AtomType taskEndsAtStep;
	private AtomType taskReductionStartsAtStep;
	private AtomType taskReductionEndsAtStep;
	private AtomType subtaskOfReductionAtStep;
	
	private SatFormula f;
	
	private SatClause currentClause;
	
	private HtnProblemData data;
	
	public HtnClausesLF(HtnProblemData data, int numSteps) {
		
		super(null, false);
		this.data = data;
		
		f = new SatFormula();
		
		this.numSteps = numSteps;
		
		domainNonprimTasks = new AtomDomain(0, data.getNonprimitiveTasks().size());
		domainAllTasks = new AtomDomain(0, data.getTasks().size());
		domainSteps = new AtomDomain(0, numSteps + 1);
		domainStepsButLast = new AtomDomain(0, numSteps);
		domainFacts = new AtomDomain(0, data.getFacts().size());
		domainReductions = new AtomDomain(0, data.getMaxReductionsPerTask());
		domainActions = new AtomDomain(0, data.getPrimitiveTasks().size());
		
		actionAtStep = new AtomType(AtomTag.ACTION_AT_STEP_AS_ACTION_IDX, 
				domainActions, domainSteps);
		factAtStep = new AtomType(AtomTag.FACT_AT_STEP, 
				domainFacts, domainSteps);
		taskStartsAtStep = new AtomType(AtomTag.TASK_STARTS_AT_STEP, 
				domainNonprimTasks, domainSteps);
		taskEndsAtStep = new AtomType(AtomTag.TASK_ENDS_AT_STEP, 
				domainNonprimTasks, domainSteps);
		taskReductionStartsAtStep = new AtomType(domainNonprimTasks, 
				domainReductions, domainSteps);
		taskReductionEndsAtStep = new AtomType(domainNonprimTasks, 
				domainReductions, domainSteps);
		subtaskOfReductionAtStep = new AtomType(domainNonprimTasks, 
				domainSteps, domainAllTasks, domainSteps);
		
		List<AtomType> atomTypes = Arrays.asList(actionAtStep, 
				factAtStep, taskStartsAtStep, 
				taskEndsAtStep, taskReductionStartsAtStep, 
				taskReductionEndsAtStep,
				subtaskOfReductionAtStep);
		d = new SatDomain(atomTypes);
	}
	
	/**
	 * Adds clauses to specify all facts that hold and 
	 * do not hold in the initial state.
	 * <br/><br/>O(f) clauses, O(1) literals each
	 */
	public void addInitialState() {
		
		List<Integer> initFacts = new ArrayList<>();
		// Each fact from the initial state holds at t=0
		for (int p : data.getInitFacts()) {
			add(uncompressedFactAtStep(p, 0));
			initFacts.add(data.compressFact(p));
		}
		// Each non-specified fact does NOT hold at t=0
		for (int p : domainFacts) {
			if (!initFacts.contains(p)) {
				add(-d.atom(factAtStep, p, 0));
			}
		}
	}
	
	/**
	 * Adds clauses that require all primitive actions
	 * to follow their preconditions and to trigger their effects.
	 * <br/><br/>O(a*s) clauses, O(1) literals each
	 */
	public void addActionPreconditionsAndEffects() {
		
		for (int a : domainActions) {
			int task = data.actionFlatIdxToTaskIdx(a);
			HtnAction action = data.getActionOfTask(task);
			for (int t : domainSteps) {
				int atomAction = d.atom(actionAtStep, a, t);
				// ... all preconditions (now)
				for (int p : action.getPreconditionsPos()) {
					add(-atomAction, uncompressedFactAtStep(p, t));
				}
				for (int p : action.getPreconditionsNeg()) {
					add(-atomAction, -uncompressedFactAtStep(p, t));
				}
			}
			for (int t : domainStepsButLast) {
				int atomAction = d.atom(actionAtStep, a, t);
				// ... all effects (at the next timestep)
				for (int p : action.getEffectsPos()) {
					add(-atomAction, uncompressedFactAtStep(p, t+1));
				}
				for (int p : action.getEffectsNeg()) {
					add(-atomAction, -uncompressedFactAtStep(p, t+1));
				}
			}
		}
	}
	
	/**
	 * Adds clauses to ensure that at least one action
	 * is executed at each step.
	 * <br/><br/>O(a*s) clauses, O(1) literals each
	 */
	public void addAtLeastOneActionPerStep() {
		
		// any of the actions must hold at timestep t
		for (int t : domainStepsButLast) {
			for (int a : domainActions) {
				addToClause(d.atom(actionAtStep, a, t));
			}
			endClause();
		}
	}
	
	/**
	 * Adds clauses to ensure that not more than one action
	 * is executed at each step.
	 * <br/><br/>O(s*a*log(a)) clauses, O(1) literals each
	 */
	public void addAtMostOneActionPerStep() {
		
		for (int t : domainSteps) {
			List<Integer> actionsAtStep = new ArrayList<>();
			for (int a : domainActions) {
				actionsAtStep.add(d.atom(actionAtStep, a, t));
			}
			SatFormula amo = SatUtils.atMostOne(AtMostOneMode.quadratic, 
					actionsAtStep, d);
			add(amo);
		}
	}
	
	/**
	 * Adds clauses to ensure that, if any fact changes,
	 * some action has been executed and has this fact
	 * as one of its effects.
	 * <br/><br/>O(s*p) clauses, O(a) literals each
	 */
	public void addActionFrameAxioms() {
		
		for (int t : domainStepsButLast) {
			for (int p : domainFacts) {
				int pUncompr = data.uncompressFact(p);
				// if p is false @t and true @t+1, then
				// any of the supporting actions is active @t
				addToClause(d.atom(factAtStep, p, t), -d.atom(factAtStep, p, t+1));
				for (int a : data.getActionTasksWithEffect(pUncompr, true)) {
					int action = data.actionTaskIdxToFlatIdx(a);
					addToClause(d.atom(actionAtStep, action, t));
				}
				endClause();
				// if p is true @t and false @t+1, then
				// any of the supporting actions is active @t
				addToClause(-d.atom(factAtStep, p, t), d.atom(factAtStep, p, t+1));
				for (int a : data.getActionTasksWithEffect(pUncompr, false)) {
					int action = data.actionTaskIdxToFlatIdx(a);
					addToClause(d.atom(actionAtStep, action, t));
				}
				endClause();
			}
		}
	}
	
	/**
	 * Adds clauses that ensure that all tasks from the
	 * initial network finish at some step.
	 * <br/><br/>O(i*s) clauses, O(1) literals each
	 */
	public void addInitialTaskNetworkCompletion() {
		
		List<Integer> initTasks = data.getInitTasks();
		for (int task : initTasks) {
			for (int t : domainSteps) {				
				addToClause(d.atom(taskEndsAtStep, 
						data.getNonprimitiveIndexOfTask(task), 
						t));
			}
			endClause();
		}
	}
	
	/**
	 * Adds clauses that ensure that the first task from
	 * the network will start at the very first step.
	 * <br/><br/>1 clause, 1 literal
	 */
	public void addInitialTaskNetworkEarliestStart() {
		
		add(d.atom(taskStartsAtStep, 
				data.getNonprimitiveIndexOfTask(data.getInitTasks().get(0)),
				0));
	}
	
	/**
	 * Adds clauses ensuring that the execution of the tasks
	 * from the initial network is ordered just as the tasks
	 * have been specified.
	 * <br/><br/>O(s*i) clauses, O(s) literals each
	 */
	public void addInitialTaskNetworkTotalSuccession() {
		
		List<Integer> initTasks = data.getInitTasks();
		for (int t : domainSteps) {	
			for (int i = 0; i < initTasks.size()-1; i++) {
				int nonprimTask1 = data.getNonprimitiveIndexOfTask(initTasks.get(i));
				int nonprimTask2 = data.getNonprimitiveIndexOfTask(initTasks.get(i+1));
				
				// The earlier task ends
				// iff the later task begins directly after
				// -- one direction
				addToClause(-d.atom(taskEndsAtStep, nonprimTask1, t));
				if (t < numSteps)
					addToClause(d.atom(taskStartsAtStep, nonprimTask2, t+1));
				endClause();
				// -- other direction
				addToClause(-d.atom(taskStartsAtStep, nonprimTask2, t));
				if (t > 0) 
					addToClause(d.atom(taskEndsAtStep, nonprimTask1, t-1));
				endClause();
				
				// the start time of the earlier task must 
				// precede the start time of the later task
				addToClause(-d.atom(taskStartsAtStep, nonprimTask1, t));
				for (int t2 = t+1; t2 <= Math.min(t+maxSteps(nonprimTask1), numSteps); t2++) {
					addToClause(d.atom(taskStartsAtStep, nonprimTask2, t2));
				}
				endClause();
				
				// the start time of the later task must
				// succeed the start time of the earlier task
				addToClause(-d.atom(taskStartsAtStep, nonprimTask2, t));
				for (int t2 = Math.max(t-maxSteps(nonprimTask1), 0); t2 < t; t2++) {
					addToClause(d.atom(taskStartsAtStep, nonprimTask1, t2));
				}
				endClause();
				
				// the end time of the earlier task must 
				// precede the end time of the later task
				addToClause(-d.atom(taskEndsAtStep, nonprimTask1, t));
				for (int t2 = t+1; t2 <= Math.min(t+maxSteps(nonprimTask2), numSteps); t2++) {
					addToClause(d.atom(taskEndsAtStep, nonprimTask2, t2));
				}
				endClause();
				
				// the end time of the later task must
				// succeed the end time of the earlier task				
				addToClause(-d.atom(taskEndsAtStep, nonprimTask2, t));
				for (int t2 = Math.max(t-maxSteps(nonprimTask2), 0); t2 < t; t2++) {
					addToClause(d.atom(taskEndsAtStep, nonprimTask1, t2));
				}
				endClause();
			}
		}
	}
	
	/**
	 * Adds clauses that ensure the <i>before</i>, <i>after</i> and
	 * <i>between</i> temporal constraints between tasks from the 
	 * initial network.
	 * <br/><br/>O(c*s) clauses, O(1) literals each
	 */
	public void addInitialTaskNetworkExplicitConstraints() {
		
		/*
		// Before constraints: when a task begins, the constraint holds
		for (OneTaskConstraint constr : data.getInitTaskNetwork()
				.getBeforeConstraints()) {
			int task = constr.getTaskList().getUniqueTask();
			int fact = constr.getGroundPredicate();
			for (int t : domainSteps) {
				add(-d.atom(taskStartsAtStep, task, t), 
						uncompressedFactAtStep(fact, t));
			}
		}
		
		// After constraints: after a task ends, the constraint holds
		for (OneTaskConstraint constr : data.getInitTaskNetwork()
				.getAfterConstraints()) {
			int task = constr.getTaskList().getUniqueTask();
			int fact = constr.getGroundPredicate();
			for (int t : domainStepsButLast) {
				add(-d.atom(taskEndsAtStep, task, t), 
						uncompressedFactAtStep(fact, t+1));
			}
		}*/
		
		// Goal state needs to hold at the end
		// (extracted from the initial task network)
		for (int p : data.getGoalFacts()) {
			add(uncompressedFactAtStep(p, numSteps));
		}
	}
	
	/**
	 * Adds clauses encoding the necessary axioms
	 * for the clauses specifying the reductions of a 
	 * nonprimitive task.
	 * <br/><br/>O(t*s) clauses, O(r) literals each
	 */
	public void addReductionAxioms() {
		
		for (int nonprimTask : domainNonprimTasks) {
			
			int numReductions = data.getNumMethodsOfTask(
					data.getNonprimitiveTasks().get(nonprimTask));		
			
			for (int t : domainSteps) {
				
				// If a task starts @t,
				// then one of its reductions starts @t
				int litTaskStarts = -d.atom(taskStartsAtStep, nonprimTask, t);
				addToClause(litTaskStarts);
				for (int r = 0; r < numReductions; r++) {
					addToClause(d.atom(taskReductionStartsAtStep, nonprimTask, r, t));
				}
				endClause();
				
				// If a task ends @t,
				// then one of its reductions ends @t
				int litTaskEnds = -d.atom(taskEndsAtStep, nonprimTask, t);
				addToClause(litTaskEnds);
				for (int r = 0; r < numReductions; r++) {
					addToClause(d.atom(taskReductionEndsAtStep, nonprimTask, r, t));
				}
				endClause();
			}
		}
	}
	
	/**
	 * Adds clauses that ensure that 
	 * <ul><li>a started task will end,</li>
	 * <li>a finished task has started before,</li>
	 * <li>a started task does not start again until
	 * it finishes, and</li>
	 * <li>a finished task did not finish another time 
	 * before it started.</li>
	 * </ul> 
	 * <br/><br/>O(s*t) clauses, O(s) literals each
	 */
	public void addValidGrammarForTaskStartAndEnd() {
		
		for (int t : domainSteps) {
			for (int nonprimTask : domainNonprimTasks) {
								
				int litTaskStarts = -d.atom(taskStartsAtStep, nonprimTask, t);
				int litTaskEnds = -d.atom(taskEndsAtStep, nonprimTask, t);
				
				// When a task starts, it must finish at some later step
				// (allow concurrence as well)
				addToClause(litTaskStarts);
				for (int tAfter = t; tAfter <= Math.min(t+maxSteps(nonprimTask), numSteps); tAfter++) {
					addToClause(d.atom(taskEndsAtStep, nonprimTask, tAfter));
				}
				endClause();
				
				// When a task finishes, it must have started before
				addToClause(litTaskEnds);
				for (int tBefore = Math.max(t-maxSteps(nonprimTask), 0); tBefore <= t; tBefore++) {
					addToClause(d.atom(taskStartsAtStep, nonprimTask, tBefore));
				}
				endClause();
				
				// When a task starts @t and it hasn't finished in [t,tAfter),
				// then it doesn't start (again) @tAfter
				for (int tAfter = t+1; tAfter <= Math.min(t+maxSteps(nonprimTask), numSteps); tAfter++) {
					addToClause(litTaskStarts);
					for (int tBetween = t; tBetween < tAfter; tBetween++) {
						addToClause(d.atom(taskEndsAtStep, nonprimTask, tBetween));
					}
					addToClause(-d.atom(taskStartsAtStep, nonprimTask, tAfter));
					endClause();
				}
				
				// When a task finishes @t and it hasn't started in (tBefore,t],
				// then it doesn't finish (again) @tBefore
				for (int tBefore = Math.max(t-maxSteps(nonprimTask), 0); tBefore <= t-1; tBefore++) {
					addToClause(litTaskEnds);
					for (int tBetween = tBefore+1; tBetween <= t; tBetween++) {
						addToClause(d.atom(taskStartsAtStep, nonprimTask, tBetween));
					}
					addToClause(-d.atom(taskEndsAtStep, nonprimTask, tBefore));
					endClause();
				}
			}
		}
	}
	
	/**
	 * Adds clauses ensuring that the first task
	 * of an active reduction will start at the reduction's start
	 * and that the last task of an active reduction will end
	 * at the reduction's end.
	 * <br/><br/>O(s*t*r) clauses, O(1) literals each
	 */
	public void addReductionBorderSubtasks() {
		
		for (int nonprimTask : domainNonprimTasks) {
			
			int task = data.getNonprimitiveTasks().get(nonprimTask);
			int numReductions = data.getNumMethodsOfTask(task);
			
			for (int t : domainSteps) {
				for (int r = 0; r < numReductions; r++) {
					
					List<Integer> expansion = data.getMethodsOfTask(task)
							.get(r).getExpansion();
					int firstTask = expansion.get(0);
					int lastTask = expansion.get(expansion.size()-1);
					
					// The first subtask must start at the same time as the task
					if (data.isTaskPrimitive(firstTask)) {
						add(-d.atom(taskReductionStartsAtStep, nonprimTask, r, t), 
								d.atom(actionAtStep, 
										data.getPrimitiveIndexOfTask(firstTask), t));
					} else {
						add(-d.atom(taskReductionStartsAtStep, nonprimTask, r, t), 
								d.atom(taskStartsAtStep, 
										data.getNonprimitiveIndexOfTask(firstTask), 
										t));
					}
					
					// The last subtask must finish at the same time as the task
					if (data.isTaskPrimitive(lastTask)) {
						add(-d.atom(taskReductionEndsAtStep, nonprimTask, r, t), 
								d.atom(actionAtStep, 
										data.getPrimitiveIndexOfTask(lastTask), t));
					} else {
						add(-d.atom(taskReductionEndsAtStep, nonprimTask, r, t), 
								d.atom(taskEndsAtStep, 
										data.getNonprimitiveIndexOfTask(lastTask),
										t));
					}
				}
			}
		}
	}
	
	/**
	 * Adds clauses ensuring that a reduction's end
	 * implies the previous end of all of its subtasks
	 * and that a reduction's start implies the following
	 * start of all of its subtasks.
	 * <br/><br/>O(s*t*r*e) clauses, O(s) literals each
	 */
	public void addReductionSubtaskCompletion() {
		
		for (int t : domainSteps) {
			for (int nonprimTask : domainNonprimTasks) {
				
				int numReductions = data.getNumMethodsOfTask(
						data.getNonprimitiveTasks().get(nonprimTask));
				int task = data.getNonprimitiveTasks().get(nonprimTask);
				
				for (int r = 0; r < numReductions; r++) {
					List<Integer> expansion = data.getMethodsOfTask(task)
							.get(r).getExpansion();
					
					// if a task reduction starts @t,
					// then all subtasks will start afterwards
					for (int subtask : expansion) {
						addToClause(-d.atom(taskReductionStartsAtStep, 
								nonprimTask, r, t));
						for (int tAfter = t; tAfter <= Math.min(
								t+maxSteps(nonprimTask), numSteps); tAfter++) {
							if (data.isTaskPrimitive(subtask)) {
								addToClause(d.atom(actionAtStep, 
										data.actionTaskIdxToFlatIdx(subtask), 
										tAfter));
							} else {
								addToClause(d.atom(taskStartsAtStep, 
										data.getNonprimitiveIndexOfTask(subtask),
										tAfter));
							}
						}
						endClause();
					}
					
					// if a task reduction ends @t,
					// then all subtasks have ended before
					for (int subtask : expansion) {
						addToClause(-d.atom(taskReductionEndsAtStep, nonprimTask, r, t));
						for (int tBefore = Math.max(t-maxSteps(nonprimTask), 0); 
								tBefore <= t; tBefore++) {
							if (data.isTaskPrimitive(subtask)) {
								addToClause(d.atom(actionAtStep, 
										data.actionTaskIdxToFlatIdx(subtask), 
										tBefore));
							} else {
								addToClause(d.atom(taskEndsAtStep, 
										data.getNonprimitiveIndexOfTask(subtask), 
										tBefore));
							}
						}
						endClause();
					}
				}
			}
		}
	}
	
	/**
	 * Adds clauses ensuring the ordering of the subtasks
	 * of a given reduction, just like the subtasks have
	 * been specified order-wise.
	 * <br/><br/>O(s*t*r*e) clauses with O(s) literals each,
	 * <br/>O(s²*t*r*e) clauses with O(1) literals each
	 */
	public void addMethodOrdering() {
		
		for (int t : domainSteps) {
			for (int nonprimTask : domainNonprimTasks) {
				
				int task = data.getNonprimitiveTasks().get(nonprimTask);
				int numReductions = data.getNumMethodsOfTask(task);
				List<Integer> atomsReduction = new ArrayList<>();
				
				for (int r = 0; r < numReductions; r++) {
					List<Integer> expansion = data.getMethodsOfTask(task).get(r)
							.getExpansion();
					atomsReduction.add(d.atom(taskReductionStartsAtStep, nonprimTask, 
							r, t));
					
					for (int subtask : expansion) {
						// If a task reduction starts @t,
						// then at some point each subtask is applied
						// (stated with the taskReductionStartsAtStep
						// helper atoms)
						addToClause(-d.atom(taskReductionStartsAtStep, nonprimTask, 
								r, t));
						for (int tAfter = t; tAfter <= Math.min(
								t+maxSteps(nonprimTask), numSteps); tAfter++) {
						
							addToClause(d.atom(subtaskOfReductionAtStep, 
									nonprimTask, t, subtask, tAfter));
							
							// If a subtask @tAfter is applied as part of a reduction @t,
							// then the subtask starts @tAfter
							if (data.isTaskPrimitive(subtask)) {
								add(-d.atom(subtaskOfReductionAtStep, 
										nonprimTask, t, subtask, tAfter), 
										d.atom(actionAtStep, 
												data.getPrimitiveIndexOfTask(subtask), 
												tAfter));								
							} else {
								add(-d.atom(subtaskOfReductionAtStep, 
										nonprimTask, t, subtask, tAfter), 
										d.atom(taskStartsAtStep, 
												data.getNonprimitiveIndexOfTask(subtask),
												tAfter));	
							}
						}
						endClause();
					}
					
					int tMin = t;
					int tMax = Math.min(t+maxSteps(expansion.get(0)), numSteps);
					for (int expIdx = 0; expIdx < expansion.size()-1; expIdx++) {
						int subtask1 = expansion.get(expIdx);
						int subtask2 = expansion.get(expIdx+1);
						
						for (int t2 = tMin; t2 <= tMax; t2++) {
							// If a given task reduction starts @t
							// and a subtask @t2 realizes it,
							// then the next subtask realizes it _later_
							addToClause(-d.atom(taskReductionStartsAtStep, 
									nonprimTask, r, t));
							addToClause(-d.atom(subtaskOfReductionAtStep, 
									nonprimTask, t, subtask1, t2));
							for (int t3 = t2+1; t3 <= Math.min(t2+maxSteps(subtask1), 
									numSteps); t3++) {
								addToClause(d.atom(subtaskOfReductionAtStep, 
									nonprimTask, t, subtask2, t3));
							}
							endClause();
						}
						tMin = Math.min(tMin + maxSteps(expansion.get(expIdx)), 
								numSteps);
						tMax = Math.min(tMax + maxSteps(expansion.get(expIdx+1)), 
								numSteps);
					}
				}
			}
		}
	}
	
	/**
	 * Adds clauses ensuring that the start of a reduction
	 * implies that all of its preconditions hold.
	 * <br/><br/>O(s*t*r*c) clauses, O(1) literals each
	 */
	public void addMethodPreconditions() {
		
		for (int t : domainSteps) {
			for (int nonprimTask : domainNonprimTasks) {
				
				int task = data.getNonprimitiveTasks().get(nonprimTask);
				int numReductions = data.getNumMethodsOfTask(task);
				
				for (int r = 0; r < numReductions; r++) {
					HtnMethod method = data.getMethodsOfTask(task).get(r);
					// If a task reduction starts @t,
					// then its pos. and neg. preconditions must hold @t
					List<Integer> preconds = method.getBeforeConstraints();
					for (int p : preconds) {
						int lit = uncompressedFactAtStep(p, t);
						add(-d.atom(taskReductionStartsAtStep, nonprimTask, r, t), 
								lit);
					}
				}				
			}
		}
	}	
	
	/**
	 * @return the SatFormula object created
	 * by all previous add*() calls
	 */
	public SatFormula getFormula() {
		return f;
	}
	
	/**
	 * @return the SatDomain collection of different
	 * atom types and their organization
	 */
	public SatDomain getDomain() {
		return d;
	}
	
	private void add(Integer... literals) {
		
		f.addClause(new SatClause(literals));
	}
		
	private void add(SatFormula f) {
		
		this.f.addAllClauses(f);
	}
	
	/**
	 * Adds all provided literals to the currently
	 * active clause, or to a new clause if there is
	 * no current active one.<br/>
	 * The active clause can then be concluded
	 * with endClause().
	 */
	private void addToClause(Integer... literals) {
		
		if (currentClause == null)
			currentClause = new SatClause();
		
		for (int lit : literals)
			currentClause.add(lit);
	}
	
	/**
	 * Concludes the clause that has been added to
	 * by previous addToClause(*) calls, and adds it
	 * to the formula.
	 */
	private void endClause() {
		
		if (currentClause != null) {
			f.addClause(currentClause);
			currentClause = null;
		}
	}
	
	/**
	 * @param fact an uncompressed fact as it occurs in
	 * the problem definition; a positive number which
	 * can represent a fact or a negated fact
	 * @param t the timestep where the fact holds
	 * @return a SAT literal (positive or negative) 
	 * corresponding to the truth value of the fact at step t
	 */
	private int uncompressedFactAtStep(int fact, int t) {
		
		int factCompressed = data.compressFact(fact);
		boolean factHolds = data.isFactPositive(fact);
		return (factHolds ? 1 : -1) * d.atom(factAtStep, factCompressed, t);
	}
	
	private int maxSteps(int task) {
		
		return numSteps;
	}
}
