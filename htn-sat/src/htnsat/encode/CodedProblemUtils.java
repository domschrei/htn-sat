package htnsat.encode;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import pddl4j.preprocessing.CodedProblem;
import pddl4j.util.BitOp;
import pddl4j.util.OneTaskConstraint;
import pddl4j.util.OrderedMethod;

public class CodedProblemUtils {

	private CodedProblem problem;
	
	// Basic problem measures
	private int numPrimitiveTasks;
	private int numNonprimitiveTasks;
	private int numTaskRealizations;
	private int maxExpansionSize;
	private int maxReductionsPerTask;

	private List<Integer> primitiveTaskIndices;
	private List<Integer> nonprimitiveTaskIndices;
	private List<Integer> totalTaskIndices;
	
	private List<Boolean> isTaskPrimitive;
	
	private List<Integer> methodStartIndicesPerNonprimTask;
	
	public CodedProblemUtils(CodedProblem problem) {
		this.problem = problem;
		
		calculateBasicTaskMeasures();
		calculateTaskReductions();
	}
	
	public int getNumPrimitiveTasks() {
		
		return numPrimitiveTasks;
	}
	
	public int getNumNonprimitiveTasks() {
		
		return numNonprimitiveTasks;
	}
	
	public int getNumTaskRealizations() {

		return numTaskRealizations;
	}
	
	public int getMaxExpansionSize() {
		
		return maxExpansionSize;
	}
	
	public int getMaxReductionsPerTask() {
		
		return maxReductionsPerTask;
	}

	public int getPrimitiveTaskIdx(int idx) {
		if (isTaskPrimitive(idx) == null || !isTaskPrimitive(idx))
			return -1;
		else return totalTaskIndices.get(idx);
	}
	public int getNonprimitiveTaskIdx(int idx) {
		if (isTaskPrimitive(idx) == null || isTaskPrimitive(idx))
			return -1;
		return totalTaskIndices.get(idx);
	}	
	public int getTotalIdxOfPrimitiveTask(int idx) {
		return primitiveTaskIndices.get(idx);
	}
	public int getTotalIdxOfNonprimitiveTask(int idx) {
		return nonprimitiveTaskIndices.get(idx);
	}	
	public int getFlatIndexOfMethod(int nonprimTaskIdx, int methodIdx) {
		return methodStartIndicesPerNonprimTask.get(nonprimTaskIdx) + methodIdx;
	}
	
	public List<Integer> getPrimitiveTaskIndicesAddingFact(int fact) {
		
		return getPrimitiveTaskIndicesChangingFact(fact, true);
	}
	
	public List<Integer> getPrimitiveTaskIndicesDeletingFact(int fact) {
		
		return getPrimitiveTaskIndicesChangingFact(fact, false);
	}
	
	public List<Integer> getGoalPredicates() {
		List<Integer> goalFacts = new ArrayList<>();
		for (OneTaskConstraint c : problem.getInitExpansion().getAfterConstraints()) {
			goalFacts.add(c.getGroundPredicate());
		}
		return goalFacts;
	}
	
	public Boolean isTaskPrimitive(int idx) {
		return isTaskPrimitive.get(idx);
	}
	
	public boolean isTaskWelldefined(int idx) {
		return isTaskPrimitive(idx) != null;
	}
	
	public Iterable<Integer> iteratorTaskIndices() {
		return new Iterable<Integer>() {
			@Override
			public Iterator<Integer> iterator() {
				return new TaskIterator();
			}
		};
	}
	
	private List<Integer> getPrimitiveTaskIndicesChangingFact(int fact, boolean adding) {
		
		List<Integer> primTaskIndices = new ArrayList<>();
		for (int taskIdx : primitiveTaskIndices) {
			BitOp op = problem.getRelevantOperators(taskIdx).get(0);
			if ((adding ? 
					op.getUnconditionalEffects().getPositive()
					: op.getUnconditionalEffects().getNegative()
				).get(2*fact)) {
				primTaskIndices.add(getPrimitiveTaskIdx(taskIdx));
			}
		}
		return primTaskIndices;
	}
	
	private void calculateBasicTaskMeasures() {

		primitiveTaskIndices = new ArrayList<>();
		nonprimitiveTaskIndices = new ArrayList<>();
		totalTaskIndices = new ArrayList<>();
		isTaskPrimitive = new ArrayList<>();
		
		for (int taskIdx = 0; taskIdx < problem.getRelevantTasks().size(); taskIdx++) {
			List<OrderedMethod> reductions = problem.getRelevantMethods(taskIdx);
			boolean hasOperators = problem.getRelevantOperators(taskIdx) != null
					&& !problem.getRelevantOperators(taskIdx).isEmpty();
			int subIdx = -1;
			if (reductions == null && hasOperators) {	
				
				numPrimitiveTasks++;
				primitiveTaskIndices.add(taskIdx);
				isTaskPrimitive.add(true);
				
				subIdx = primitiveTaskIndices.size()-1;
				
			} else if (reductions != null && !hasOperators) {
				
				numNonprimitiveTasks++;
				nonprimitiveTaskIndices.add(taskIdx);
				isTaskPrimitive.add(false);
				
				subIdx = nonprimitiveTaskIndices.size()-1;

				for (OrderedMethod method : reductions) {					
					maxExpansionSize = Math.max(maxExpansionSize, 
							method.getExpansion().size());
				}
			}
			
			totalTaskIndices.add(subIdx);
			if (subIdx == -1)
				isTaskPrimitive.add(null);
		}
	}
	
	private void calculateTaskReductions() {
		
		numTaskRealizations = 0;
		methodStartIndicesPerNonprimTask = new ArrayList<>();
		for (int taskIdx = 0; taskIdx < problem.getRelevantTasks().size(); taskIdx++) {
			List<OrderedMethod> reductions = problem.getRelevantMethods(taskIdx);
			if (reductions != null) {
				maxReductionsPerTask = Math.max(maxReductionsPerTask, reductions.size());
				methodStartIndicesPerNonprimTask.add(numTaskRealizations);
				numTaskRealizations += reductions.size();
			}
		}
	}
	
	private class TaskIterator implements Iterator<Integer> {
		int taskIdx = 0;
		public TaskIterator() {
			while (taskIdx < problem.getRelevantTasks().size() 
					&& !isTaskWelldefined(taskIdx)) {
				taskIdx++;
			}
		}
		@Override
		public boolean hasNext() {
			return taskIdx < problem.getRelevantTasks().size();
		}
		@Override
		public Integer next() {
			int taskIdxOut = taskIdx;
			do {
				taskIdx++;
			} while (taskIdx < problem.getRelevantTasks().size() 
					&& !isTaskWelldefined(taskIdx));
			return taskIdxOut;
		}
	}
}
