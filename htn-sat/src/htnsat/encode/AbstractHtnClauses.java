package htnsat.encode;

import htnsat.htn.HtnProblem;
import htnsat.sat.ISatFormula;
import htnsat.sat.IncSatFormula;
import htnsat.sat.SatClause;
import htnsat.sat.SatDomain;
import htnsat.sat.SatFormula;
import htnsat.sat.IncSatFormula.FormulaType;

/**
 * A placeholder for Htn2Sat encoding engines.
 * @author Dominik Schreiber
 */
public abstract class AbstractHtnClauses {

	// Invariantly relevant fields
	protected HtnProblem problem;
	protected SatDomain d;
	private SatClause currentClause;
	
	private boolean incremental;
	
	// For non-incremental encodings
	private SatFormula f;

	// For incremental encodings
	private IncSatFormula fInc;
	private FormulaType currentFormulaMode;
	
	public AbstractHtnClauses(HtnProblem problem, boolean incremental) {
		this.incremental = incremental;
		this.problem = problem;
	}
	
	/**
	 * @return the SatDomain collection of different
	 * atom types and their organization
	 */
	public SatDomain getDomain() {
		return d;
	}
	
	public abstract ISatFormula getFormula();
}
