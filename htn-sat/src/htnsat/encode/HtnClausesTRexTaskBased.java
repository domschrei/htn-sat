package htnsat.encode;

import java.util.ArrayList;
import java.util.List;

import htnsat.sat.ISatFormula;
import htnsat.htn.HtnAction;
import htnsat.htn.HtnMethod;
import htnsat.htn.HtnProblemData;
import htnsat.sat.AtomDomain;
import htnsat.sat.AtomType;
import htnsat.sat.IncSatFormula;
import htnsat.sat.SatDomain;
import htnsat.sat.SatFormula;
import htnsat.sat.SatUtils;
import htnsat.util.Assert;
import htnsat.sat.IncSatFormula.FormulaType;
import htnsat.sat.SatUtils.AtMostOneMode;
import htnsat.sat.SatClause;

/**
 * Default encoding for Incremental Hierarchy Exploration approach.
 * Makes use of the IncHex format to encode the problem in a more abstract way
 * in order to reduce encoding time and to let the solver executable
 * instantiate the clauses in an efficient way and just as needed.
 * 
 * The encoding itself is based on a stepwise refinement approach
 * where each computational step of the solver will represent an abstract
 * plan getting more and more concrete until only primitive actions remain.
 * 
 * @author Dominik Schreiber
 */
public class HtnClausesTRexTaskBased extends AbstractHtnClauses {

	private HtnProblemData data;
		
	// AtomDomains which are used for encoding
	private AtomDomain domFacts;
	private AtomDomain domActions;
	private AtomDomain domNonprimTasks;
	private AtomDomain domReductions;
	
	// Global AtomTypes
	private AtomType holds;
	private AtomType actionAt;
	private AtomType nonprimTaskAt;
	private AtomType reductionAt;
	private AtomType primitive;
	private AtomType isBlank;
	
	// Resulting incremental formula
	private IncSatFormula f;
	
	// Helpers to conveniently create clauses
	private FormulaType currentFormulaMode;
	private SatClause currentClause;
			
	public HtnClausesTRexTaskBased(HtnProblemData data) {
		
		super(null, true); // incremental = true
		
		this.data = data;
		
		f = new IncSatFormula();
		
		// Domains
		
		domFacts = new AtomDomain(0, data.getCompressedFacts().size());
		domActions = new AtomDomain(0, 
				data.getPrimitiveTasks().size());
		domNonprimTasks = new AtomDomain(0, data.getNonprimitiveTasks().size());
		domReductions = new AtomDomain(0, data.getMaxReductionsPerTask());
	
		// Atom types
		
		holds = new AtomType(AtomTag.FACT_AT_INDEX, domFacts);
		actionAt = new AtomType(AtomTag.ACTION_AT_INDEX, domActions);
		nonprimTaskAt = new AtomType(domNonprimTasks);
		reductionAt = new AtomType(domReductions);
		primitive = new AtomType();
		isBlank = new AtomType();
		//isBottom = new AtomType();
		
		List<AtomType> allAtomTypes = new ArrayList<>();
		// The ordering of types is relevant for this encoding:
		// The elements (actions, methods) must be first
		// and the "blank" element must be at position 1
		allAtomTypes.add(isBlank);
		allAtomTypes.add(actionAt);
		//allAtomTypes.add(isBottom);
		allAtomTypes.add(nonprimTaskAt);
		allAtomTypes.add(primitive);
		allAtomTypes.add(holds);
		allAtomTypes.add(reductionAt);
		//allAtomTypes.add(mayChange);
		d = new SatDomain(allAtomTypes);
		
		addHeadComments();
	}
	
	public void addHeadComments() {
		
		f.addHeadComment("e " + (data.getPrimitiveTasks().size()+1) 
				+ " " + (data.getNonprimitiveTasks().size())
				+ " " + domReductions.getSize());
		f.addHeadComment("f " + data.getCompressedFacts().size());

		// Add head comments specifying the possible fact changes of methods
		/*
		for (int method : domMethods) {

			StringBuilder effectorSpec = new StringBuilder("c " + d.atom(methodAt, method) + " ");
			BitVector changedFacts = data.getFactsChangedByMethods().get(method);
			for (int fact : changedFacts.getAllSetBits()) {
				effectorSpec.append(d.atom(holds, data.compressFact(fact)) + " ");
			}
			f.addHeadComment(effectorSpec.toString());
		}
		*/
		
		StringBuilder str = new StringBuilder("i ");
		boolean nopFound = false;
		for (int actionIdx : domActions) {
			
			// TODO replace this hack by something more robust,
			// i.e. checking that the action has no preconds/effects
			HtnAction action = data.getActionOfTask(data.actionFlatIdxToTaskIdx(actionIdx));
			if (action.getName().equalsIgnoreCase("nop")) {
				str.append(d.atom(actionAt, actionIdx) + " ");
				nopFound = true;
				break;
			}
		}
		if (nopFound)
			f.addHeadComment(str.toString());
	}
	
	/**
	 * Specifies that at the array's first index,
	 * the initial facts hold and all other facts
	 * do not hold.
	 * <br/><br/>Type: <b>initial</b>
	 */
	public void addInitialFacts() {
		
		mode(FormulaType.initial);
		
		// Facts
		List<Integer> initFacts = new ArrayList<>();
		String atZero = "@0";
		// Each fact from the initial state holds at t=0 
		for (int p : data.getInitFacts()) {
			addToClause(fact(p));
			annotateClause(atZero);
			endClause();
			initFacts.add(data.compressFact(p));
		}
		// Each non-specified fact does NOT hold at t=0
		for (int p : domFacts) {
			if (!initFacts.contains(p)) {
				addToClause(-d.atom(holds, p));
				annotateClause(atZero);
				endClause();
			}
		}
	}
	
	/**
	 * Specifies that in the first computational step, the array
	 * contains any of the valid initial tasks at its first
	 * positions, followed by a "bottom" symbol.
	 * <br/><br/>Type: <b>initial</b>
	 */
	public void addInitialTasks() {
		
		mode(FormulaType.initial);
		
		List<Integer> initTasks = data.getInitTasks();
		
		// Write all possible tasks to their respective pos.
		int idx = 0;
		String atIdx = "@0";
		for (int task : initTasks) {
			
			int lit;
			
			if (data.isTaskPrimitive(task)) {
				lit = d.atom(actionAt, data.actionTaskIdxToFlatIdx(task));
			} else {
				lit = d.atom(nonprimTaskAt, data.getNonprimitiveIndexOfTask(task));
			}
			
			addToClause(lit);
			annotateClause(atIdx);
			endClause();
			
			idx++;
			atIdx = "@" + idx;
		}
		
		// Bottom of the calculation at last index
		addToClause(d.atom(isBlank));
		annotateClause(atIdx);
		endClause(); 
		
		for (int p : data.getGoalFacts()) {
			// At index idx (= bottom of calculation),
			// the goal facts must hold
			// TODO negative goal facts?
			addToClause(fact(p));
			annotateClause(atIdx);
			endClause();
		}
	}
	
	/**
	 * Specifies that all elements of the array must be primitive
	 * at the final computational step.
	 * <br/><br/>Type: <b>goal</b>
	 */
	public void addGoalCondition() {
		
		mode(FormulaType.goal);
		
		// For the calculation to finish, all remaining elements
		// must be primitive
		addToClause(d.atom(primitive));
		annotateClause("@A");
		endClause();
	}
	
	/**
	 * Enforces an action's preconditions to hold at the index
	 * where the action is present.
	 * <br/><br/>Type: <b>universal</b>
	 */
	public void addActionPreconditions() {
		
		mode(FormulaType.universal);
		
		for (int a : domActions) {
			int task = data.actionFlatIdxToTaskIdx(a);
			HtnAction htnAction = data.getActionOfTask(task);
			int litAction = -d.atom(actionAt, a);
			for (int p : htnAction.getPreconditionsPos()) {
				add(litAction, fact(p));
			}
			for (int p : htnAction.getPreconditionsNeg()) {
				add(litAction, -fact(p));
			}
		}
	}
	
	/**
	 * Enforces an action's effects to hold at the index
	 * after the index where the action is present.
	 * <br/><br/>Type: <b>universal</b>
	 */
	public void addActionEffects() {
		
		mode(FormulaType.universal);
		for (int a : domActions) {
			int task = data.actionFlatIdxToTaskIdx(a);
			HtnAction htnAction = data.getActionOfTask(task);
			int litAction = -d.atom(actionAt, a);
			
			for (int p : htnAction.getEffectsPos()) {
				addToClause(litAction, fact(p));
				annotateAllClauseLiteralsButFirst("+1");
				endClause();
			}
			for (int p : htnAction.getEffectsNeg()) {
				addToClause(litAction, -fact(p));
				annotateAllClauseLiteralsButFirst("+1");
				endClause();
			}
		}
	}
	
	public void addTaskReductions() {
		
		mode(FormulaType.universal);
		
		for (int nonprimTask : domNonprimTasks) {
			
			int task = data.getNonprimitiveTasks().get(nonprimTask);
			
			addToClause(-d.atom(nonprimTaskAt, nonprimTask));
			
			for (int r = 0; r < data.getNumMethodsOfTask(task); r++) {
				addToClause(d.atom(reductionAt, r));
			}
			
			endClause();
		}
	}
	
	/**
	 * Enforces a method's preconditions to hold at the index
	 * where the method is present.
	 * <br/><br/>Type: <b>universal</b>
	 */
	public void addMethodPreconditions() {
		
		mode(FormulaType.universal);
		
		for (int nonprimTask : domNonprimTasks) {
			
			int task = data.getNonprimitiveTasks().get(nonprimTask);
						
			for (int r = 0; r < data.getNumMethodsOfTask(task); r++) {
				
				HtnMethod method = data.getMethodsOfTask(task).get(r);

				if (method.getBeforeConstraints().size() == 0)
					continue;
				
				addToClause(-d.atom(nonprimTaskAt, nonprimTask));
				addToClause(-d.atom(reductionAt, r));
				
				for (int p : method.getBeforeConstraints()) {
					addToClause(fact(p));
				}
				
				endClause();
			}
		}
	}
		
	/**
	 * Enforces that whenever a fact changes between two neighbored
	 * array indices, one of its supporting actions is at the
	 * first index. Also enforces an at-most-one constraint
	 * over the actions at any given index.
	 * <br/><br/>Type: <b>universal</b>
	 */
	public void addAtMostOneAction() {
		
		mode(FormulaType.universal);
		
		// At most one action per position
		List<Integer> actionAtoms = new ArrayList<>();
		for (int action : domActions) {
			actionAtoms.add(d.atom(actionAt, action));
		}
		actionAtoms.add(d.atom(isBlank));
		//actionAtoms.add(d.atom(isBottom));
		add(SatUtils.atMostOne(AtMostOneMode.binary, actionAtoms, d));
	}
		
	public void concludeDomain() {
		
		// After this method call, all necessary helper
		// variables must have been added
		d.concludeDomain();
		f.setEncodedNumVars(d.getTotalSize());
	}
		
	/**
	 * Specifies the propagation of methods from one computational step
	 * to the next one, depending on the shift at the current position.
	 * <br/><br/>Type: <b>universal, transitional</b> (from a nested method call)
	 */
	public void addNonprimitiveTransitions() {
		
		mode(FormulaType.transitional);

		for (int nonprimTask : domNonprimTasks) {
			
			int task = data.getNonprimitiveTasks().get(nonprimTask);
			
			for (int r = 0; r < data.getNumMethodsOfTask(task); r++) {
				
				HtnMethod method = data.getMethodsOfTask(task).get(r);
				int expSize = method.getExpansion().size();
				
				for (int i = 0; i < expSize; i++) {
					
					// Get the task at the i-th position of the method
					int subtask = method.getExpansion().get(i);
					String plusSubtaskIdx = "+" + i;
					
					addToClause(-d.atom(nonprimTaskAt, nonprimTask), 
							-d.atom(reductionAt, r));
					
					int lit;
					if (data.isTaskPrimitive(subtask)) {
						lit = d.atom(actionAt, data.actionTaskIdxToFlatIdx(subtask));
					} else {
						lit = d.atom(nonprimTaskAt, data.getNonprimitiveIndexOfTask(subtask));
					}
					
					addToClause(nextStep(lit));
					annotateClause(null, null, plusSubtaskIdx);
					endClause();
				}
			}
		}
	}

	/**
	 * Changes the formula to which the following clauses 
	 * will be added to. Must be called at the start of
	 * each clause-adding method as well as before any
	 * mode switches inbetween a method.
	 * @param type <i>initial</i>, <i>goal</i>, 
	 * <i>universal</i> or <i>transitional</i>
	 */
	private void mode(FormulaType type) {
		
		currentFormulaMode = type;
	}
	
	/**
	 * Adds the literals as a new clause to the
	 * currently active SatFormula.
	 */
	private void add(Integer... literals) {
		
		f.addClause(currentFormulaMode, new SatClause(literals));
	}
	
	private void add(SatClause c) {
		
		f.addClause(currentFormulaMode, c);
	}
	
	/**
	 * Adds all clauses of the provided formula 
	 * to the currently active SatFormula.
	 */
	private void add(SatFormula f) {
		
		this.f.addAllClauses(currentFormulaMode, f);
	}
	
	/**
	 * Adds all provided literals to the currently
	 * active clause, or to a new clause if there is
	 * no current active one.<br/>
	 * The active clause must then be concluded
	 * with endClause().
	 */
	private void addToClause(Integer... literals) {
		
		if (currentClause == null)
			currentClause = new SatClause();
		
		for (int lit : literals)
			currentClause.add(lit);
	}
	
	private void annotateClause(String... annotations) {
		
		currentClause.annotate(annotations);
	}
	
	private void annotateAllClauseLiterals(String annotation) {
		
		String[] annotations = new String[currentClause.getSize()];
		for (int i = 0; i < annotations.length; i++) {
			annotations[i] = annotation;
		}
		currentClause.annotate(annotations);
	}
	
	private void annotateAllClauseLiteralsButFirst(String annotation) {
		
		String[] annotations = new String[currentClause.getSize()];
		annotations[0] = null;
		for (int i = 1; i < annotations.length; i++) {
			annotations[i] = annotation;
		}
		currentClause.annotate(annotations);
	}
	
	/**
	 * Concludes the clause that has been added to
	 * by previous addToClause(*) calls, and adds it
	 * to the currently active formula.
	 */
	private void endClause() {
		
		if (currentClause != null) {
			f.addClause(currentFormulaMode, currentClause);
			currentClause = null;
		}
	}
	
	/**
	 * @param fact a fact represented by an index
	 * of problem.getRelevantFacts().
	 * @return an atom representing this fact in the
	 * valid AtomDomain
	 */
	private int fact(int fact) {
		
		int factCompressed = data.compressFact(fact);
		boolean factHolds = data.isFactPositive(fact);
		return (factHolds ? 1 : -1) * d.atom(holds, factCompressed);
	}
	
	/**
	 * @param lit a literal for the current state
	 * @return the equivalent literal for the <i>next</i> state
	 * in the incremental definition
	 */
	private int nextStep(int lit) {
		
		Assert.that(!d.isModifiable(), "The domain is still modifiable; "
				+ "any \"next step\" variables could become erroneous.");
		
		boolean pos = lit > 0;
		lit = Math.abs(lit);
		
		return (pos ? 1 : -1) * (lit + d.getTotalSize()); // TODO FIXME XXX
	}
	
	/**
	 * Returns the created IncSatFormula object.
	 */
	@Override
	public ISatFormula getFormula() {
		
		return f;
	}
}
