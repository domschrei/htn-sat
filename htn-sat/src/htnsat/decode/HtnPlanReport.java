package htnsat.decode;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

import htnsat.Logger;
import htnsat.htn.HtnProblem;
import htnsat.htn.HtnProblemData;
import pddl4j.preprocessing.CodedProblem;
import pddl4j.util.IntExp;

public class HtnPlanReport {

	private CodedProblem codedProblem;
	private HtnProblem problem;
	private HtnProblemData data;
	
	private HtnPlan plan;
	private String reportDirectory;
	private int numSteps;
	
	private Logger log;

	public HtnPlanReport(CodedProblem codedProblem, HtnPlan plan) {
		
		this.codedProblem = codedProblem;
		this.plan = plan;
		this.numSteps = plan.getNumSteps();
	}
	public HtnPlanReport(HtnProblem problem, HtnPlan plan) {
		
		this.problem = problem;
		this.plan = plan;
		this.numSteps = plan.getNumSteps();
	}
	public HtnPlanReport(HtnProblemData data, HtnPlan plan) {
		
		this.data = data;
		this.plan = plan;
		this.numSteps = plan.getNumSteps();
	}
	
	public void setLogger(Logger log) {
		
		this.log = log;
	}
	
	public void report() {
		
		String filename = "solution.txt";
		if (reportDirectory != null) {
			filename = reportDirectory + "/" + filename;
		}
		/*
		for (int step = 0; step <= numSteps; step++) {
			print("Step " + step);
			reportWorldState(step);
			reportActions(step);
			//reportStackStatus(step);
//			reportMethodAndActionArray(step);
			reportFactArray(step);
			// reportTasks(step);
		}*/
		if (plan.getMaxIndex() > 0) {
			reportIndexedActionsCompact();
			//reportCompleteMethodTree();
		} else {			
			reportActionsCompact();
		}
		
		//reportStackTable();
		//reportTaskActivitiyTable();
//		reportFactsTable();
	}
	
	private void reportWorldState(int step) {

		List<Integer> predicates = plan.get(HtnPlan.FACT_AT_STEP, step);
		print("  World state:");
		for (int fact : predicates) {
			String out = factToString(fact);
			print("    " + out);
		}
	}

	private void reportActions(int step) {

		List<Integer> actions = plan.get(HtnPlan.ACTION_AT_STEP, step);
		if (actions != null) {
			print("  Executed actions:");
			for (int actionTask : actions) {
				String out = taskToString(actionTask);
				print("    " + out);
			}
		}
	}

	private void reportActionsCompact() {

		String out = "\nCompact plan:\n";
		int stepIdx = 0;
		for (int step = 0; step <= numSteps; step++) {

			List<Integer> actions = plan.get(HtnPlan.ACTION_AT_STEP, step);
			if (actions != null && !actions.isEmpty()) {
				for (int actionTask : actions) {
					out += stepIdx + ": " + taskToString(actionTask) + "\n";
				}
				stepIdx++;
			}
		}
		print(out + "\n");
	}
	
	private void reportIndexedActionsCompact() {
		
		String out = "\nCompact plan:\n";
		int step = numSteps;
		planLoop : for (int index = 0; index < plan.getMaxIndex(); index++) {
			List<Integer> actions = plan.getForIndex(HtnPlan.ACTION_AT_INDEX, 
					step, index);
			for (int actionTask : actions) {
				if (actionTask == -1) {
					// bottom reached
					break planLoop;
				}
				out += index + ": " + taskToString(actionTask) + "/";
			}
			out = out.substring(0, out.length()-1) + "\n";
		}
		print(out + "\n");
	}

	private void reportStackStatus(int step) {

		List<Integer> op = plan.get(HtnPlan.STACK_OPERATION_AT_STEP, step);
		if (op != null && op.size() > 0) {
			print("Stack operation: " + (op.get(0) == -1 ? 
					"pop" : "push " + op.get(0)));
		}
	}

	private void reportMethodAndActionArray(int step) {
		
		for (int index = 0; index < plan.getMaxIndex(); index++) {
			List<Integer> methods = plan.getForIndex(HtnPlan.METHOD_AT_INDEX, 
					step, index);
			List<Integer> actions = plan.getForIndex(HtnPlan.ACTION_AT_INDEX, 
					step, index);
			String elemsAtIdx = "@" + index + ": ";
			for (int method : methods) {
				elemsAtIdx += "method " + methodToString(method) + ", ";
			}
			for (int action : actions) {
				if (action == -1) {
					elemsAtIdx += "#BOTTOM, ";
				} else {					
					elemsAtIdx += "action "
							+ taskToString(action)
							+ ", ";
				}
			}
			print(elemsAtIdx.substring(0, elemsAtIdx.length()-2));
		}
	}
	
	private void reportCompleteMethodTree() {
				
		List<List<String>> treeContent = new ArrayList<>();

		for (int step = 0; step <= numSteps; step++) {
			
			ArrayList<String> contentAtStep = new ArrayList<>();

			loopIndices : for (int index = 0; index < plan.getMaxIndex(); index++) {
				List<Integer> methods = plan.getForIndex(HtnPlan.METHOD_AT_INDEX, 
						step, index);
				List<Integer> actions = plan.getForIndex(HtnPlan.ACTION_AT_INDEX, 
						step, index);

				boolean bottomFound = false;
				String content = "";
				for (int action : actions) {
					if (action == -1) {
						// bottom found
						content += "#";
						bottomFound = true;
						contentAtStep.add(content);
						break loopIndices;
					} else {						
						content += "a" + action;
					}
				}
				if (!bottomFound) {					
					for (int method : methods) {
						content += "m" + method;
					}
				}
				
				contentAtStep.add(content);
			}
			
			treeContent.add(contentAtStep);
		}
	
		reportStepsTable(i -> "Idx. " + i, (idx, step) -> {
			List<String> contentAtStep = treeContent.get(step);
			if (idx < contentAtStep.size()) {
				return contentAtStep.get(idx);
			} else {
				return "-";
			}
		}, numSteps, plan.getMaxIndex());
	}
	
	private void reportFactArray(int step) {
		
		for (int index = 0; index < plan.getMaxIndex(); index++) {
			List<Integer> facts = plan.getForIndex(HtnPlan.FACT_AT_INDEX, 
					step, index);
			String elemsAtIdx = "@" + index + ": ";
			for (int p : facts) {
				elemsAtIdx += " " + factToString(p);
			}
			print(elemsAtIdx);
		}
	}
	
	private void reportStackTable() {

		List<Integer> initContent = plan.get(HtnPlan.STACK_CONTENT_AT_STEP, 
				0);
		if (!initContent.isEmpty()) {
			
			int stackSize = initContent.size();
			
			reportStepsTable(stackPos -> {
				return "Pos. " + stackPos + "  ";
			}, (stackPos, stepIdx) -> {
				int content = plan.get(HtnPlan.STACK_CONTENT_AT_STEP, stepIdx)
						.get(stackPos);
				if (content == -1) {
					return "-";
				} else {
					return "" + content;
				}
			}, numSteps, stackSize);
		}
	}

	private void reportStepsTable(Function<Integer, String> objectTitles,
			BiFunction<Integer, Integer, String> contentAtObjAndStep, int numSteps, int numObjs) {

		String[][] table = new String[numObjs + 1][numSteps + 2];
		int[] columnWidth = new int[numSteps + 2];

		// Headline
		table[0][0] = "Step";
		for (int i = 1; i < table[0].length; i++) {
			table[0][i] = (i - 1) + "";
		}

		// Content
		for (int row = 1; row < table.length; row++) {
			int objIdx = row - 1;
			table[row][0] = objectTitles.apply(objIdx);
			for (int col = 1; col < table[row].length; col++) {
				int step = col - 1;
				table[row][col] = contentAtObjAndStep.apply(objIdx, step);
			}
		}

		// Spacing
		for (int c = 0; c < table[0].length; c++) {
			columnWidth[c] = 0;
			for (int r = 0; r < table.length; r++) {
				columnWidth[c] = Math.max(columnWidth[c], table[r][c].length());
			}
		}

		String out = "";
		// Assembly
		for (int r = 0; r < table.length; r++) {
			for (int c = 0; c < table[r].length; c++) {
				String add = table[r][c];
				while (add.length() < columnWidth[c]) {
					add += " ";
				}
				out += add + " ";
			}
			out += "\n";
		}

		print(out);
	}

	private void reportTaskActivitiyTable() {

		reportStepsTable(taskIdx -> {
			IntExp task = data.getTasks().get(taskIdx);
			String prefix = (data.isTaskPrimitive(taskIdx) ? "[p] " : "[t] ");
			return "#" + taskIdx + " " + prefix + data.intExpToString(task);
		}, (taskIdx, step) -> {
			String out = "";
			if (data.isTaskPrimitive(taskIdx)) {
				int primTaskIdx = data.getPrimitiveIndexOfTask(taskIdx);
				out += plan.get(HtnPlan.ACTION_AT_STEP, step).contains(taskIdx)
						? "a" : "";
			} else {
				int nonprimTaskIdx = data.getNonprimitiveIndexOfTask(taskIdx);
				out += plan.get(HtnPlan.TASK_STARTS_AT_STEP, step).contains(
						nonprimTaskIdx) ? "s" : "";
				out += plan.get(HtnPlan.TASK_ENDS_AT_STEP, step).contains(
						nonprimTaskIdx) ? "f" : "";
			}
			return out;
		}, numSteps, data.getTasks().size());
	}

	private void reportFactsTable() {

		reportStepsTable(factIdx -> {
			IntExp fact = problem.getCompressedFacts().get(factIdx);
			return "#" + factIdx + " " + problem.intExpToString(fact);
		}, (factIdx, step) -> {
			List<Integer> facts = plan.get(HtnPlan.FACT_AT_STEP, step);
			return (facts.contains(factIdx) ? "✓" : "");
		}, numSteps, problem.getCompressedFacts().size());
	}

	private void reportTasks(int step) {

		List<Integer> activeTasks = plan.get(HtnPlan.TASK_STARTS_AT_STEP, step);
		List<Integer> finishedTasks = plan.get(HtnPlan.TASK_ENDS_AT_STEP, step);

		if (activeTasks != null && !activeTasks.isEmpty()) {
			print("  Active tasks:");
			for (int task : activeTasks) {
				String out = problem.intExpToString(problem.getRelevantTasks().get(task));
				print("    " + out);
			}
		}
		if (finishedTasks != null && !activeTasks.isEmpty()) {
			print("  Finished tasks:");
			for (int task : finishedTasks) {
				String out = problem.intExpToString(problem.getRelevantTasks().get(task));
				print("    " + out);
			}
		}
	}

	private String factToString(int fact) {
		String out = "";
		if (codedProblem != null) {
			out = codedProblem.toString(codedProblem.getRelevantFacts().get(fact/2));
		} else if (problem != null) {				
			out = problem.intExpToString(problem.getCompressedFacts().get(fact));
		} else if (data != null) {
			out = data.intExpToString(data.getCompressedFacts().get(fact));
		}
		return out;
	}
	private String taskToString(int task) {
		String out = "";
		if (codedProblem != null) {
			out = codedProblem.toString(codedProblem.getRelevantTasks().get(task));
		} else if (problem != null) {				
			out = problem.intExpToString(problem.getRelevantTasks().get(task));
		} else if (data != null) {
			try {
				out = data.intExpToString(data.getTasks().get(task));
			} catch (NullPointerException e) {
				out = data.getActionOfTask(task).getName();
			}
		}
		return out;
	}
	private String methodToString(int m) {
		String out = "";
		if (problem != null) {				
			out = problem.getFlatMethods().get(m).toString();
		} else if (data != null) {
			out = data.getFlatMethods().get(m).toString();
		}
		return out;
	}
	
	/**
	 * Prints the provided string to stdout
	 * as well as into the solution file.
	 */
	private void print(String str) {
		
		log.print(str);
	}
	
}
