package htnsat.decode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import htnsat.sat.AtomType;
import htnsat.sat.BinaryEncoding;
import htnsat.sat.SatDomain;
import htnsat.sat.SatDomain.Symbol;
import htnsat.util.Assert;

public class StackDecoder {

	private List<BinaryEncoding> encodings;
	private SatDomain d;
	private int maxLegalValue;
	
	public StackDecoder(List<BinaryEncoding> encodings, SatDomain d, int maxLegalValue) {
		this.encodings = encodings;
		this.d = d;
		this.maxLegalValue = maxLegalValue;
	}
	
	public int[] getStackContent(List<Integer> values) {
		
		int[] stack = new int[encodings.size()];
		for (int i = 0; i < stack.length; i++) {
			stack[i] = 0;
		}
		
		// Create a collection of atoms divided according
		// to their atom type.
		Map<AtomType, List<Integer>> atomsPerType = new HashMap<>();
		for (int value : values) {
			Symbol s = d.symbol(value);
			if (!atomsPerType.containsKey(s.type)) {
				atomsPerType.put(s.type, new ArrayList<>());
			}
			atomsPerType.get(s.type).add(value);
		}
		
		// Match the atom types with their original binary encoding
		// and decode the values into the stack
		loopType : for (AtomType type : atomsPerType.keySet()) {
			for (int i = 0; i < encodings.size(); i++) {
				BinaryEncoding enc = encodings.get(i);
				if (enc.getAtomType().equals(type)) {
					// matching atom type found
					stack[i] = enc.value(atomsPerType.get(type));
					continue loopType;
				}
			}
//			Assert.that(false, "The atom type " + type 
//					+ " does not belong to any binary encoding.");
		}
		
		return stack;
	}
	
	public int getStackSize() {
		
		return encodings.size();
	}
	
	public int getMaxLegalValue() {
		return maxLegalValue;
	}
	
}
