package htnsat.decode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import htnsat.encode.AtomTag;
import htnsat.encode.Htn2SatEncoder.Mode;
import htnsat.htn.HtnProblem;
import htnsat.htn.HtnProblemData;
import htnsat.sat.AtomDomain;
import htnsat.sat.SatDomain;
import htnsat.sat.SatDomain.Symbol;
import htnsat.sat.SatSolver.Result;
import htnsat.sat.SatSolver.Satisfiability;
import htnsat.sat.SatSolver.SolveMode;
import htnsat.util.Assert;

/**
 * Decodes and outputs a plan provided by a SatSolver.
 * 
 * @author Dominik Schreiber
 */
public class Sat2HtnDecoder {

	private Mode mode;

	private HtnPlan plan;
	
	private HtnProblem problem;
	private HtnProblemData data;
	private SatDomain domain;
	
	private List<StackDecoder> stackDecoders;
	private List<List<Integer>> rawStackValues;

	/**
	 * @param problem the original problem as an HtnProblem 
	 * instance
	 * @param domain the SatDomain instance used for the encoding
	 * @param mode the encoding mode
	 */
	public Sat2HtnDecoder(HtnProblem problem, SatDomain domain, 
			Mode mode) {
		this.mode = mode;
		this.problem = problem;
		this.domain = domain;
	}
	/**
	 * @param problem the original problem as an HtnProblem 
	 * instance
	 * @param domain the SatDomain instance used for the encoding
	 * @param mode the encoding mode
	 */
	public Sat2HtnDecoder(HtnProblemData data, SatDomain domain, 
			Mode mode) {
		this.mode = mode;
		this.data = data;
		this.domain = domain;
	}

	public void decode(Result result) {
		
		Assert.that(result.sat  != null && 
				result.sat == Satisfiability.sat, 
				"No satisfiability could be identified.");
			
		decodeStepInformation(result);
	}

	private void decodeStepInformation(Result result) {

		SolveMode solveMode = result.solveMode;
		
		plan = new HtnPlan();
		rawStackValues = new ArrayList<>();
		
		if (solveMode == SolveMode.incremental) {
			for (int i = 0; i < result.incValues.size(); i++) {
				List<Integer> stepValues = result.incValues.get(i);
				decode(stepValues, i);
			}
		} else if (solveMode == SolveMode.tRex) {
			decodeIncHexPlan(result.incValues.get(0));
		} else {
			decode(result.values, null);
		}
		
		if (stackDecoders != null) {				
			for (StackDecoder decoder : stackDecoders) {
				
				// Decode the stack values from binary
				decodeStack(decoder);
			}
		}
	}
	
	private void decode(List<Integer> atoms, Integer step) {
		
		List<AtomDomain> globalDomains = domain.getGlobalTypeDomains();
		
		List<Integer> stackValues = new ArrayList<>();
		
		boolean incremental = (step != null);
		
		atomLoop : for (int atom : atoms) {

			for (AtomDomain dom : globalDomains) {
			
				if (dom.getTag() == null) {
					// This atom is not of interest
					continue atomLoop;
				}
				
				if (dom.isInside(atom)) {

					// Insert the found atom
					// into its corresponding data structure
					Symbol symbol = domain.symbol(atom);
					
					if (dom.getTag() == AtomTag.ACTION_AT_STEP_AS_TASK_IDX) {
						int actionTaskIdx = symbol.args[0];
						if (!incremental)
							step = symbol.args[1];
						plan.add(HtnPlan.ACTION_AT_STEP, step, actionTaskIdx);
						
					} else if (dom.getTag() == AtomTag.ACTION_AT_STEP_AS_ACTION_IDX) {
						int actionIdx = symbol.args[0];
						if (!incremental)
							step = symbol.args[1];
						int actionTaskIdx = -1;
						if (problem != null) {
							actionTaskIdx = actionDomainIdxToTaskIdx(actionIdx);
						} else if (data != null) {
							actionTaskIdx = data.actionFlatIdxToTaskIdx(actionIdx);
						}
						if (actionTaskIdx >= 0)
							plan.add(HtnPlan.ACTION_AT_STEP, step, actionTaskIdx);
						
					} else if (dom.getTag() == AtomTag.FACT_AT_STEP) {
						int fact = symbol.args[0];
						if (!incremental)
							step = symbol.args[1];
						plan.add(HtnPlan.FACT_AT_STEP, step, fact);
						
					} else if (dom.getTag() == AtomTag.TASK_STARTS_AT_STEP) {
						int task = symbol.args[0];
						if (!incremental)
							step = symbol.args[1];
						plan.add(HtnPlan.TASK_STARTS_AT_STEP, step, task);
					
					} else if (dom.getTag() == AtomTag.TASK_ENDS_AT_STEP) {
						int task = symbol.args[0];
						if (!incremental)
							step = symbol.args[1];
						plan.add(HtnPlan.TASK_ENDS_AT_STEP, step, task);
					
					} else if (dom.getTag() == AtomTag.POP) {
						plan.add(HtnPlan.STACK_OPERATION_AT_STEP, step, -1);
					
					} else if (dom.getTag() == AtomTag.PUSH) {
						plan.add(HtnPlan.STACK_OPERATION_AT_STEP, step, 1);
					
					} else if (dom.getTag() == AtomTag.PUSH_MULTI) {
						int pushSize = symbol.args[0];
						plan.add(HtnPlan.STACK_OPERATION_AT_STEP, step, pushSize);
					
					} else if (dom.getTag() == AtomTag.BINARY_ENCODING) {
						stackValues.add(atom);
					
					} else if (dom.getTag() == AtomTag.ACTION_AT_INDEX) {
						int index = symbol.args[0];
						int action = symbol.args[1];
						int actionTaskIdx = -1;
						if (problem != null) {
							actionTaskIdx = actionDomainIdxToTaskIdx(action);
						} else if (data != null) {
							if (action < data.getPrimitiveTasks().size()) {
								actionTaskIdx = data.actionFlatIdxToTaskIdx(action);								
							}
						}
						//if (actionTaskIdx >= 0) {
							plan.addAtIndex(HtnPlan.ACTION_AT_INDEX, step, index, actionTaskIdx);
						//}
						
					} else if (dom.getTag() == AtomTag.METHOD_AT_INDEX) {
						int index = symbol.args[0];
						int method = symbol.args[1];
						plan.addAtIndex(HtnPlan.METHOD_AT_INDEX, step, index, method);
					
					} else if (dom.getTag() == AtomTag.FACT_AT_INDEX) {
						int index = symbol.args[0];
						int fact = symbol.args[1];
						plan.addAtIndex(HtnPlan.FACT_AT_INDEX, step, index, fact);
					}
					
					break;
				}
			}
		}
		
		if (!stackValues.isEmpty()) {
			rawStackValues.add(stackValues);
		}
	}
	
	
	private void decodeIncHexPlan(List<Integer> values) {
		
		List<AtomDomain> globalDomains = domain.getGlobalTypeDomains();
		
		int step = 0;
		for (int value : values) {
			int actionIdx = value-2;
			if (actionIdx < data.getPrimitiveTasks().size()) {				
				plan.add(HtnPlan.ACTION_AT_STEP, step, 
						data.actionFlatIdxToTaskIdx(actionIdx));
				step++;
			}
		}
	}
	
	private void decodeStack(StackDecoder stackDecoder) {
		
		// Decode the binary information into readable format
		int[][] stack = new int[rawStackValues.size()][stackDecoder.getStackSize() + 1];
		for (int step = 0; step < stack.length; step++) {
			int[] stackValues = stackDecoder.getStackContent(
					rawStackValues.get(step));
			boolean bottomReached = false;
			for (int stackPos = 0; stackPos < stack[step].length; stackPos++) {
				// When the bottom is reached, the remaining information
				// is arbitrary; add -1 at these positions
				if (bottomReached) {
					stack[step][stackPos] = -1;
				} else {
					if (stackPos < stackValues.length)
						stack[step][stackPos] = stackValues[stackPos];
					else
						stack[step][stackPos] = -1;
				}
				if (stack[step][stackPos] == stackDecoder.getMaxLegalValue()) {
					bottomReached = true;
				}
			}
		}
		
		// Add the found information to the plan
		for (int step = 0; step < stack.length; step++) {
			for (int pos = 0; pos < stack[step].length; pos++) {
				plan.add(HtnPlan.STACK_CONTENT_AT_STEP, step, 
						stack[step][pos]);
			}
		}
	}

	public HtnPlan getPlan() {
		
		return plan;
	}
	
	public HtnProblemData getProblemData() {
		
		return data;
	}
	
	public void setStackDecoder(StackDecoder... stackDecoders) {
		this.stackDecoders = Arrays.asList(stackDecoders);
	}
	
	/**
	 * @param domainIdx the index of an action in the encoded
	 * domain (i.e. a valid value in the action AtomDomain)
	 * @return the corresponding index of the action in
	 * problem.getReachablePrimitiveTasks()
	 */
	private int actionDomainIdxToTaskIdx(int domainIdx) {
		
		if (domainIdx >= problem.getReachablePrimitiveTasks().size())
			return -1;
		
		return problem.getReachablePrimitiveTasks().get(domainIdx);
	}
}
