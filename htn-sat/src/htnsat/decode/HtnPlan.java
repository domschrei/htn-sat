package htnsat.decode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import htnsat.util.Assert;

public class HtnPlan {

	public static final int ACTION_AT_STEP = 1;
	public static final int FACT_AT_STEP = 2;
	public static final int TASK_STARTS_AT_STEP = 3;
	public static final int TASK_ENDS_AT_STEP = 4;
	public static final int STACK_OPERATION_AT_STEP = 5;
	public static final int STACK_CONTENT_AT_STEP = 6;

	public static final int FACT_AT_INDEX = 7;
	public static final int ACTION_AT_INDEX = 8;
	public static final int METHOD_AT_INDEX = 9;
	
	/**
	 * Maps the steps of a plan to a certain property
	 * that hold at this point.
	 * @author Dominik Schreiber
	 */
	public class StepCollection {
		private Map<Integer, List<Integer>> steps;

		public StepCollection() {
			this.steps = new HashMap<>();
		}

		/**
		 * @param key the step where the property holds
		 * @param value a representation of the property
		 * as an integer (e.g. a fact index or an action
		 * index)
		 */
		public void add(int key, int value) {
			if (!steps.containsKey(key)) {
				steps.put(key, new ArrayList<>());
			}
			steps.get(key).add(value);
		}

		/**
		 * Returns all collected properties
		 * of the provided step.
		 */
		public List<Integer> get(int step) {
			if (steps.get(step) == null) {
				return new ArrayList<>();
			}
			return steps.get(step);
		}

		/**
		 * Returns the maximal step which is
		 * non-empty.
		 */
		public int maxStep() {
			return steps.keySet().stream().mapToInt(i -> i)
					.max().getAsInt();
		}
	}
	
	private StepCollection actionsAtStep;
	private StepCollection factsAtStep;
	private StepCollection tasksStartingAtStep;
	private StepCollection tasksFinishingAtStep;
	private StepCollection stackOperationAtStep;
	private StepCollection stackContentAtStep;
	private List<StepCollection> factAtIndexAtStep;
	private List<StepCollection> actionAtIndexAtStep;
	private List<StepCollection> methodAtIndexAtStep;
	
	private int numSteps;
	
	public HtnPlan() {
		
		actionsAtStep = new StepCollection();
		factsAtStep = new StepCollection();
		tasksStartingAtStep = new StepCollection();
		tasksFinishingAtStep = new StepCollection();
		stackOperationAtStep = new StepCollection();
		stackContentAtStep = new StepCollection();
		factAtIndexAtStep = new ArrayList<>();
		actionAtIndexAtStep = new ArrayList<>();
		methodAtIndexAtStep = new ArrayList<>();
		numSteps = 0;
	}
	
	/**
	 * Adds an information to the plan object.
	 * @param type one of the constants defined in HtnPlan
	 * @param step the step the information belongs to
	 * @param element the information, represented by an integer
	 */
	public void add(int type, int step, int element) {
		
		StepCollection c = collectionOfType(type);
		c.add(step, element);
		numSteps = Math.max(numSteps, step);
	}
	
	public void addAtIndex(int type, int step, int index, int element) {
		
		List<StepCollection> indexCollections = indexedCollectionOfType(type);
		
		while (indexCollections.size() <= index) {
			indexCollections.add(new StepCollection());
		}
		StepCollection elemAtStep = indexCollections.get(index);
		elemAtStep.add(step, element);
		numSteps = Math.max(numSteps, step);
	}
	
	/**
	 * Returns the information associated with the given
	 * type at the given step.
	 * @return a list of integers representing information
	 * of the given type at the given step
	 */
	public List<Integer> get(int type, int step) {
		
		StepCollection c = collectionOfType(type);
		return c.get(step);
	}
	
	public List<Integer> getForIndex(int type, int step, int index) {
		
		List<StepCollection> indexCollections = indexedCollectionOfType(type);
		
		if (index < indexCollections.size()) {			
			return indexCollections.get(index).get(step);
		} else {
			return new ArrayList<>();
		}
	}
	
	/**
	 * @return the maximal step index for which this plan
	 * contains any information
	 */
	public int getNumSteps() {
		
		return numSteps;
	}
	
	public int getMaxIndex() {
		
		int maxIndex = -1;
		if (factAtIndexAtStep != null)
			maxIndex = Math.max(maxIndex, factAtIndexAtStep.size());
		if (actionAtIndexAtStep != null)
			maxIndex = Math.max(maxIndex, actionAtIndexAtStep.size());
		if (methodAtIndexAtStep != null)
			maxIndex = Math.max(maxIndex, methodAtIndexAtStep.size());
		
		return maxIndex;
	}
	
	private StepCollection collectionOfType(int type) {
		
		StepCollection c = null;
		if (type == ACTION_AT_STEP) {
			c = actionsAtStep;
		} else if (type == FACT_AT_STEP) {
			c = factsAtStep;
		} else if (type == TASK_STARTS_AT_STEP) {
			c = tasksStartingAtStep;
		} else if (type == TASK_ENDS_AT_STEP) {
			c = tasksFinishingAtStep;
		} else if (type == STACK_OPERATION_AT_STEP) {
			c = stackOperationAtStep;
		} else if (type == STACK_CONTENT_AT_STEP) {
			c = stackContentAtStep;
		}
		Assert.that(c != null, "An invalid information type "
				+ "has been provided.");
		return c;
	}
	
	private List<StepCollection> indexedCollectionOfType(int type) {
		
		List<StepCollection> c = null;
		if (type == FACT_AT_INDEX) {
			c = factAtIndexAtStep;
		} else if (type == ACTION_AT_INDEX) {
			c = actionAtIndexAtStep;
		} else if (type == METHOD_AT_INDEX) {
			c = methodAtIndexAtStep;
		}
		Assert.that(c != null, "An invalid information type "
				+ "has been provided.");
		return c;
	}
	
}
