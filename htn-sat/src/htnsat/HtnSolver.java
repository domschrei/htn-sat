package htnsat;

import java.util.Arrays;
import java.util.Properties;

import htnsat.decode.HtnPlan;
import htnsat.decode.HtnPlanReport;
import htnsat.decode.Sat2HtnDecoder;
import htnsat.encode.Htn2SatEncoder;
import htnsat.encode.Htn2SatEncoder.Mode;
import htnsat.htn.HtnProblemData;
import htnsat.htn.HtnSimplifier;
import htnsat.sat.ISatFormula;
import htnsat.sat.SatSolver;
import htnsat.sat.SatSolver.Result;
import htnsat.sat.SatSolver.Satisfiability;
import htnsat.sat.SatSolver.SolveMode;
import pddl4j.examples.ISHOP.ISHOP;
import pddl4j.parser.Domain;
import pddl4j.parser.Problem;
import pddl4j.preprocessing.CodedProblem;
import pddl4j.preprocessing.Preprocessing;

public class HtnSolver {

	private HtnSolverParams params;
	
	private Logger log;
	
	/**
	 * The used HTN-to-SAT encoder.
	 */
	private Htn2SatEncoder encoder;
	
	/**
	 * @param mode the mode of the encoding
	 * @param maxDepth the maximal depth to explore 
	 * (i.e. the maximal steps for non-incremental encodings
	 * and the maximal stack size for incremental encodings)
	 * @param verbose true, iff logging should also be output
	 * to stdout
	 */
	public HtnSolver(HtnSolverParams params) {
		
		this.params = params;
	}
	
	/**
	 * Attempts to solve a parsed problem with the specified encoding.
	 */
	public void solveProblem(Problem problem, Domain domain) {
		
		try {
			// "Top-level" logger which always prints to stdout, too
			log = new Logger(true); 
			log.setFilename("00_log.txt");

			trySolveProblem(problem, domain);
		
		} catch (Exception e) {
			if (log != null) {
				log.print(e.getMessage());
				log.print(Arrays.toString(e.getStackTrace()));
			} else {
				e.printStackTrace();
			}
		}
	}
	
	private void trySolveProblem(Problem problem, Domain domain) 
			throws Exception {
		
		boolean incremental = (params.mode != Mode.linearForward);
		
		// Preprocess and encode the problem
		//Preprocessing.setLogLevel(7); // to output preprocessing stats
		CodedProblem codedProblem = Preprocessing.encode(domain, problem);
		
		log.printTimestamped("Preprocessed problem");
		
		// Simplify the problem data structures
		// and print problem info
		HtnSimplifier simplifier = new HtnSimplifier(codedProblem);
		HtnProblemData data = simplifier.simplify();
		simplifier = null;
		
		log.printTimestamped("Created HtnProblemData instance");
		
		/*
		Logger problemInfoLog = new Logger(false);
		String filename = "01_problem-info.txt";
		problemInfoLog.setFilename(filename);
		data.printInfo(problemInfoLog);

		log.printTimestamped("Wrote problem information to file \"" 
					+ filename + "\"");
		*/
		
		// Attempt to solve the problem
		Result result;
		if (incremental) {
			result = solveIncrementally(data);
		} else {
			result = solveNonincrementally(data);
		}
		
		// Positive result?
		if (result.sat  == null ||
				result.sat != Satisfiability.sat) {
			
			log.print("No satisfiability could be identified.");

		} else {
			
			
			HtnPlanReport report;
			
			/*
			boolean slim = false;
			if (slim) {
				htnsat.decode.slim.Sat2HtnDecoder decoder = encoder.getSlimDecoder();
				decoder.decode(result);
								
				HtnPlan plan = decoder.getPlan();
				report = new HtnPlanReport(codedProblem, plan);
			} else {
				 */
				
				// Decode the satisfying assignment to variables
				// back to the problem domain
				Sat2HtnDecoder decoder = encoder.getDecoder();
				decoder.decode(result);
				
				HtnPlan plan = decoder.getPlan();
				report = new HtnPlanReport(data, plan);
			//}
			
			log.printTimestamped("Decoded result");
			
			// Report the plan from the decoded information
			Logger solutionLog = new Logger(params.verbose);
			solutionLog.setFilename("04_solution.txt");
			report.setLogger(solutionLog);
			report.report();
			solutionLog.close();
			
			log.printTimestamped("Reported result");
		}
		
		log.print("Total execution time: " 
				+ log.totalPassedTime() + "ms. Exiting.");
		
		log.close();
	}
	
	private Result solveNonincrementally(HtnProblemData htnProblem) {
			
		int steps = 0;
		Result result = null;
		while (result == null || result.sat != Satisfiability.sat) {
			
			steps++;
			log.print("Solve for " + steps + " max steps");				
			
			encoder = new Htn2SatEncoder();
			ISatFormula f = encoder.encode(htnProblem, 
					params.mode, steps);
			
			log.printTimestamped("Encoded problem into SAT");

			SatSolver solver = prepareSolver(f); // non-incremental
			
			log.printTimestamped("Wrote formula to file");
			
			if (!params.onlyOutputFormula) {
			
				solver.solve();
				
				log.printTimestamped("Executed SAT solver");
				
				solver.getLogger().close();
				
				result = solver.getResult();
			}
			
			if (steps == params.maxSize ||
					(result != null && result.sat == Satisfiability.unknown)) {
				break;
			}
		}
		
		return result;
	}

	private Result solveIncrementally(HtnProblemData htnProblem) {
		
		encoder = new Htn2SatEncoder();
		ISatFormula f = encoder.encode(htnProblem, 
				params.mode, params.maxSize);
		
		log.printTimestamped("Encoded problem into SAT");
		
		SatSolver solver = prepareSolver(f); // incremental
		
		log.printTimestamped("Wrote formula to file");
		
		if (!params.onlyOutputFormula) {
		
			solver.solve();
			
			log.printTimestamped("Executed SAT solver");
			
			solver.getLogger().close();				
		}
		
		return solver.getResult();
	}
	
	private SatSolver prepareSolver(ISatFormula f) {
		
		SolveMode solveMode;
		if (params.mode == Mode.linearForward) {
			solveMode = SolveMode.single;
		} else if (params.mode == Mode.tRex) {
			solveMode = SolveMode.tRex;
		} else {
			solveMode = SolveMode.incremental;
		}
		
		SatSolver solver = new SatSolver(solveMode);
		
		Logger logSat = new Logger(false);
		//solver.setFormulaCopyPath(log.getDirectory() + "/02_formula.cnf");
		logSat.setFilename("03_sat-output.txt");
		solver.setLogger(logSat);
		if (params.interpreterParams != null) {
			solver.setInterpreterParams(params.interpreterParams);
		}
		solver.writeToFile(f);
		
		return solver;
	}
}
