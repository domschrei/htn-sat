package htnsat.sat;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IncSatFormula implements ISatFormula {
	
	public enum FormulaType {
		initial, goal, universal, transitional, reductional;
	}
	
	private SatFormula fInitial;
	private SatFormula fGoal;
	private SatFormula fUniversal;
	private SatFormula fTransitional;
	private SatFormula fReductional;
	private Map<FormulaType, SatFormula> formulae;
	
	private List<String> headComments;
	
	private int encodedNumVars;
	
	public IncSatFormula() {

		fInitial = new SatFormula();
		fGoal = new SatFormula();
		fUniversal = new SatFormula();
		fTransitional = new SatFormula();
		fReductional = new SatFormula();
		
		formulae = new HashMap<>();
		
		formulae.put(FormulaType.initial, fInitial);
		formulae.put(FormulaType.goal, fGoal);
		formulae.put(FormulaType.universal, fUniversal);
		formulae.put(FormulaType.transitional, fTransitional);
		formulae.put(FormulaType.reductional, fReductional);
		
		headComments = new ArrayList<>();
	}
	
	public void addClause(FormulaType type, Integer... literals) {
		formulae.get(type).addClause(literals);
	}
	
	public void addClause(FormulaType type, SatClause c) {
		formulae.get(type).addClause(c);
	}
	
	public void addAllClauses(FormulaType type, SatFormula f) {
		formulae.get(type).addAllClauses(f);
	}
	
	public void addHeadComment(String comment) {
		headComments.add(comment);
	}
	
	public int numVariablesSingleStep() {
		return Math.max(Math.max(Math.max(
				fTransitional.numVariables() / 2,
				fInitial.numVariables()), 
				fUniversal.numVariables()), 
				fGoal.numVariables());
	}
	
	public void output(BufferedWriter writer) throws IOException {
		int numVars = encodedNumVars;
		for (String comment : headComments) {
			writer.write("c" + comment + "\n");
			
		}
		writer.write("i cnf " + numVars + " " 
					+ fInitial.numClauses() + "\n");
		for (SatClause c : fInitial.getClauses()) {
			writer.write(c.toString() + "0\n");
		}
		writer.write("u cnf " + numVars + " " 
				+ fUniversal.numClauses() + "\n");
		for (SatClause c : fUniversal.getClauses()) {
			writer.write(c.toString() + "0\n");
		}
		writer.write("g cnf " + numVars + " " 
				+ fGoal.numClauses() + "\n");
		for (SatClause c : fGoal.getClauses()) {
			writer.write(c.toString() + "0\n");
		}
		writer.write("t cnf " + numVars + " " 
				+ fTransitional.numClauses() + "\n");
		for (SatClause c : fTransitional.getClauses()) {
			writer.write(c.toString() + "0\n");
		}
		if (fReductional.numClauses() > 0) {
			writer.write("r cnf " + numVars + " " 
					+ fReductional.numClauses() + "\n");
			for (SatClause c : fReductional.getClauses()) {
				writer.write(c.toString() + "0\n");
			}
		}
	}

	public int getEncodedNumVars() {
		return encodedNumVars;
	}

	public void setEncodedNumVars(int encodedNumVars) {
		this.encodedNumVars = encodedNumVars;
	}
}
