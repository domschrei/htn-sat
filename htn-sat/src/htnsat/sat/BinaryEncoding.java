package htnsat.sat;

import java.util.List;

import htnsat.encode.AtomTag;
import htnsat.sat.SatDomain.Symbol;
import htnsat.util.Assert;

public class BinaryEncoding {

	private int numStates;
	private int bitLength;
	
	/**
	 * The most significant bit is at the highest index
	 * and the least significant bit is at index 0.
	 */
	private int[] encodedAtoms;
	private SatDomain d;
	private AtomType atomType;
	
	/**
	 * Initializes a binary encoding inside the given
	 * domain, with values ranging from 0 inclusive to 
	 * numStates exclusive.
	 * <br/>Side effect: A new AtomType is added to the 
	 * domain.
	 * <br/>If numStates != 2^k - 1 for some k, then
	 * some values of the encoding must be explicitly
	 * forbidden by adding the clauses of the method
	 * getForbiddenValues().
	 * 
	 * @param d
	 * @param numStates
	 */
	public BinaryEncoding(SatDomain d, int numStates) {
		
		this.d = d;
		this.numStates = numStates;
		
		init();
	}
	
	public BinaryEncoding(AtomTag tag, SatDomain d, int numStates) {
		
		this.d = d;
		this.numStates = numStates;
		
		init(tag);
	}
	
	private void init() {
		
		init(null);
	}
	
	private void init(AtomTag tag) {
		
		bitLength = log2(numStates-1);
		
		AtomDomain atomDomain = new AtomDomain(0, bitLength);
		atomType = new AtomType(tag, atomDomain);
		d.addAtomType(atomType);

		encodedAtoms = new int[bitLength];
		for (int i = 0; i < bitLength; i++) {
			encodedAtoms[i] = d.atom(atomType, i);
		}
	}
	
	/**
	 * In the binary encoding, a value domain always ranges
	 * from 0 to 2^k - 1 for some k, even when the desired domain
	 * is actually 2^k - 1 - x for some positive x.
	 * <br/>This method creates a formula containing clauses
	 * that explicitly forbid these undesired values.
	 */
	public SatFormula getForbiddenValues() {
		
		SatFormula f = new SatFormula();
		
		Integer[] maxValidBits = posLiterals(numStates-1);
		for (int i = 0; i < maxValidBits.length; i++) {
			if (maxValidBits[i] < 0) {
				SatClause c = new SatClause(maxValidBits[i]);
				for (int j = i+1; j < maxValidBits.length; j++) {
					c.add(-maxValidBits[j]);
				}
				f.addClause(c);
			}
		}
		
		return f;
	}
	
	/**
	 * Returns a clause consisting of literals
	 * representing the given value in binary.
	 * <br/>Standalone semantics of this clause:
	 * at least one of the bits corresponding to the
	 * given value must be true.
	 */
	public SatClause posClause(int val) {
		
		return clause(val, true);
	}
	
	/**
	 * Returns a clause consisting of literals
	 * representing the given value in binary.
	 * <br/>Standalone semantics of this clause:
	 * The given value must not hold (i.e. at least
	 * one of the bits is different). Can be used
	 * on the left side of an implication: "If the
	 * value holds, then..."
	 */
	public SatClause negClause(int val) {
		
		return clause(val, false);
	}
	
	/**
	 * Returns literals
	 * representing the given value in binary.
	 * <br/>Standalone semantics of this clause:
	 * at least one of the bits corresponding to the
	 * given value must be true.
	 */
	public Integer[] posLiterals(int val) {
		
		return literals(val, true);
	}
	
	/**
	 * Returns literals
	 * representing the given value in binary.
	 * <br/>Standalone semantics of this clause:
	 * The given value must not hold (i.e. at least
	 * one of the bits is different). Can be used
	 * on the left side of an implication: "If the
	 * value holds, then..."
	 */
	public Integer[] negLiterals(int val) {
		
		return literals(val, false);
	}
	
	private SatClause clause(int val, boolean positive) {
		
		return new SatClause(literals(val, positive));
	}
	
	/**
	 * Calculates the corresponding literals
	 * to a given value that shall be encoded.
	 */
	private Integer[] literals(int val, boolean positive) {
		
		Assert.that(0 <= val && val < numStates, 
				"The value " + val + " is illegal for the"
					+ " binary encoding of the range "
					+ "[" + 0 + "," + (numStates-1) + "].");
		
		Integer[] lits = new Integer[bitLength];
		int remainder = val;
		for (int i = bitLength - 1; i >= 0; i--) {
			int power = (int) Math.pow(2, i);
			int atomSign;
			if (remainder >= power) {
				atomSign = 1;
				remainder -= power;
			} else {
				atomSign = -1;
			}
			lits[i] = (positive ? 1 : -1) 
					* atomSign * encodedAtoms[i];
		}
		Assert.that(remainder == 0, 
				"Binary value calculation incorrect");
		return lits;
	}
	
	/**
	 * Decodes a given clause and returns the value
	 * it represents.
	 */
	public int value(SatClause clause) {
		
		return value(clause.getLiterals());
	}

	/**
	 * Decodes a given sorted list of literals and 
	 * returns the value it represents.
	 */
	public int value(List<Integer> literals) {
		
		int value = 0;
		for (int lit : literals) {
			if (lit > 0) {	
				Symbol sym = d.symbol(lit);
				Assert.that(sym.type.equals(atomType), 
						"A wrong atom type was provided.");
				value += (int) Math.pow(2, sym.args[0]); 
			}
		}
		return value;
	}
	
	public int getAtomForBit(int bitPos) {
		
		return encodedAtoms[bitPos];
	}
	
	public int getBitLength() {
		
		return bitLength;
	}
	
	public AtomType getAtomType() {
		return atomType;
	}

	/**
	 * log2 method
	 */
	public static int log2(int n) {
		
		return (int) Math.floor(Math.log(n) / Math.log(2)) + 1;
	}
}
