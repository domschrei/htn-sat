package htnsat.sat;

import java.util.Iterator;

import htnsat.encode.AtomTag;

public class AtomDomain implements Iterable<Integer> {
	
	private int start;
	private int size;
	
	private AtomTag tag;
	
	public AtomDomain(int start, int size) {
		this.start = start;
		this.size = size;
	}
	
	public void setTag(AtomTag tag) {
		this.tag = tag;
	}
	
	public AtomTag getTag() {
		return tag;
	}

	public int getStart() {
		return start;
	}

	public int getSize() {
		return size;
	}
	
	public boolean isInside(int val) {
		return start <= val && val < start+size;
	}

	@Override
	public Iterator<Integer> iterator() {
		return new Iterator<Integer>() {

			int i = start;
			
			@Override
			public boolean hasNext() {
				return i < start+size;
			}

			@Override
			public Integer next() {
				final int next = i;
				i++;
				return next;
			}
		};
	}
	
}
