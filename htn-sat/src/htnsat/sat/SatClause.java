package htnsat.sat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Represents a propositional logic clause.
 * 
 * @author Dominik Schreiber
 */
public class SatClause {

	/**
	 * The positive and negative literals inside the clause,
	 * represented as positive or negative integers
	 */
	private List<Integer> literals;
	
	/**
	 * May contain an annotation for each literal of the clause.
	 * Used for generalized DimSpec for tree traversal.
	 */
	private String[] annotations;
	
	public SatClause(Integer... literals) {
		
		this.literals = new ArrayList<>();
		this.literals.addAll(Arrays.asList(literals));
	}
	
	public SatClause(List<Integer> literals) {
		
		this.literals = new ArrayList<>();
		this.literals.addAll(literals);
	}
	
	public SatClause() {
		this.literals = new ArrayList<>();
	}
	
	public void add(int... lit) {
		for (int i : lit) {
			literals.add(i);
		}
	}
	
	public void addAll(SatClause c) {
		for (int var : c.literals) {
			add(var);
		}
	}
	
	public void annotate(String... annotations) {
		this.annotations = annotations;
	}
	
	public int getSize() {
		return literals.size();
	}
	
	public List<Integer> getLiterals() {
		return literals;
	}

	@Override
	public String toString() {
		StringBuilder out = new StringBuilder();
		if (annotations != null) {
			// Annotated clause
			for (int i = 0; i < literals.size(); i++) {
				out.append(Integer.toString(literals.get(i)));
				if (i < annotations.length && annotations[i] != null) {
					out.append(annotations[i]);
				}
				out.append(" ");
			}
		} else {
			// Usual clause
			for (int lit : literals) {
				out.append(Integer.toString(lit) + " ");
			}
		}
		return out.toString();
	}
}