package htnsat.sat;

import java.io.BufferedWriter;
import java.io.IOException;

public interface ISatFormula {

	public void output(BufferedWriter writer) throws IOException;
}