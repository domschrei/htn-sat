package htnsat.sat;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Represents a propositional logic formula. 
 * Only meant for constructing encodings and outputting them into files.
 * Not meant for being evaluated by a solver.
 * 
 * @author Dominik Schreiber
 */
public class SatFormula implements ISatFormula {

	private List<SatClause> clauses; 
	
	public SatFormula() {
		this.clauses = new ArrayList<>();
	}
	
	public void addClause(SatClause c) {
		clauses.add(c);
	}
	
	public void addClause(Integer... literals) {
		addClause(new SatClause(literals));
	}
	
	public void addAllClauses(SatFormula f) {
		for (SatClause c : f.clauses) {
			addClause(c);
		}
	}
	
	public void annotateLastClause(String... annotations) {
		clauses.get(clauses.size()-1).annotate(annotations);
	}
	
	public int numClauses() {
		return clauses.size();
	}
	
	public int numLiterals() {
		return clauses.stream().mapToInt(c -> c.getSize()).sum();
	}
	
	public int numVariables() {
		int maxVar = 0;
		for (SatClause c : clauses) {
			for (int var : c.getLiterals()) {
				maxVar = Math.max(Math.abs(var), maxVar);
			}
		}
		return maxVar;
	}
	
	public int numDistinctVariables() {
		Set<Integer> vars = new HashSet<>();
		for (SatClause c : clauses) {
			for (int var : c.getLiterals()) {
				vars.add(var);
			}
		}
		return vars.size();
	}
	
	public List<SatClause> getClauses() {
		return clauses;
	}
	
	/**
	 * Returns a string representation of the SAT formula compliant with default
	 * cnf file standards, i.e. one clause per line denoted with space-separated integers
	 * and a "0" line at the end.
	 */
	public void output(BufferedWriter writer) throws IOException {
		
		writer.write("p cnf " + numVariables() + " " + numClauses() + "\n");
		for (int clauseIdx = 0; clauseIdx < clauses.size(); clauseIdx++) {
			String clause = clauses.get(clauseIdx).toString();
			writer.write(clause + "0\n");
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clauses == null) ? 0 : clauses.hashCode());
		return result;
	}
}
