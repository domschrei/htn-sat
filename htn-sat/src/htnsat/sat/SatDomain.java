package htnsat.sat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import htnsat.util.Assert;

/**
 * Represents a partitioning of global literal space 
 * (i.e. positive and negative integers up to a certain maximum value)
 * into sub-domains corresponding to certain types of atomic formulae.
 *  
 * @author Dominik Schreiber
 */
public class SatDomain {
	
	private List<AtomType> atomTypes;
	private List<Integer> globalStarts;
	private Map<AtomType, Integer> globalStartsMap;
	
	private boolean modifiable;
	
	public SatDomain(List<AtomType> atomTypes) {
		this.atomTypes = new ArrayList<>();
		this.atomTypes.addAll(atomTypes);
		initGlobalStarts();
	}
	
	private void initGlobalStarts() {
		globalStarts = new ArrayList<>();
		globalStartsMap = new HashMap<>();
		int globalStart = 1;
		for (int typeIdx = 0; typeIdx < atomTypes.size(); typeIdx++) {
			AtomType type = atomTypes.get(typeIdx);
			type.setEncodingIndex(typeIdx);
			globalStarts.add(globalStart);
			globalStartsMap.put(type, globalStart);
			globalStart += type.getTotalDomainSize();			
		}
		modifiable = true;
	}
	
	public void addAtomType(AtomType type) {
		
		Assert.that(modifiable, "The domain is not modifiable.");
		
		int globalStart = 1;
		if (!globalStarts.isEmpty()) {
			globalStart = globalStarts.get(globalStarts.size()-1)
				+ atomTypes.get(atomTypes.size()-1).getTotalDomainSize();
		}

		atomTypes.add(type);
		globalStarts.add(globalStart);
		globalStartsMap.put(type, globalStart);
		type.setEncodingIndex(atomTypes.size()-1);
	}
	
	public int getTotalSize() {
		
		AtomType lastType = atomTypes.get(atomTypes.size()-1);
		int[] args = new int[lastType.getArgumentDomains().size()];
		for (int i = 0; i < args.length; i++) {
			args[i] = lastType.getArgumentDomains().get(i).getStart()
					+ lastType.getArgumentDomains().get(i).getSize() - 1;
		}
		return atom(atomTypes.get(atomTypes.size()-1), args);
		
//		return globalStarts.get(globalStarts.size()-1) 
//				+ atomTypes.get(atomTypes.size()-1).getTotalDomainSize() - 1;
	}
		
	/**
	 * Encoding function.<br/>
	 * Returns an atomic formula corresponding to the provided type and arguments.
	 */
	public int atom(AtomType type, int... args) {
		
		List<AtomDomain> domains = type.getArgumentDomains();
		
		// Number of arguments must comply with number of defined domains
		Assert.that(args.length == domains.size(), 
				"The number of arguments provided for an atom type does not comply with its definition.");

		// Each argument must be within the bounds of its domain
		for (int i = 0; i < args.length; i++) {
			AtomDomain dom = domains.get(i);
			Assert.that(args[i] >= dom.getStart(), 
					"Argument #" + (i+1) + " (" + args[i] 
					+ ") is lower than its domain's start (" + dom.getStart() + ").");
			Assert.that(args[i] < dom.getStart() + dom.getSize(), 
					"Argument #" + (i+1) + " (" + args[i] 
					+ ") is greater than its domain's end (" 
					+ (dom.getStart()+dom.getSize()) + ").");
		}
		
		// Find the unique variable index
		int typeIndex = atomTypes.indexOf(type); // TODO inefficient
		Assert.that(typeIndex != -1, 
			"The specified atom type is not part of the domain.");
		int varIndex = globalStarts.get(typeIndex);
		//int varIndex = globalStartsMap.get(type);
		
		for (int i = 0; i < type.getArgumentDomains().size(); i++) {
			// Decrement each argument by its start value, 
			// as the lowest value of each argument should be zero 
			// for the following computation of the literal
			int varAdd = args[i] - domains.get(i).getStart();
			for (int j = i+1; j < type.getArgumentDomains().size(); j++) {
				varAdd *= type.getArgumentDomains().get(j).getSize();
			}
			varIndex += varAdd;
		}
		
		return varIndex;
	}
	
	/**
	 * For incremental formulae only: get an atomic variable
	 * corresponding to atom(type, args), but at the next
	 * timestep.
	 * <br/>Beware: before using this method for the first time
	 * in an encoding, the domain must be concluded (i.e. no addition
	 * of any helper variables or new atom types any more).
	 * @param type
	 * @param args
	 * @return
	 */
	public int atomNext(AtomType type, int... args) {
		
		Assert.that(!modifiable, "The domain has not been concluded yet "
				+ "and is still modifiable.");
		int atom = atom(type, args);
		return atom + getTotalSize();
	}
	
	/**
	 * Creates a list of AtomDomain objects. Each object
	 * corresponds to the domain of one particular AtomType
	 * in the final encoding. Domains in the list which
	 * correspond to an AtomType tagged with an AtomTag 
	 * will contain the equivalent tag (retrievable by getTag()).
	 */
	public List<AtomDomain> getGlobalTypeDomains() {
		
		List<AtomDomain> globalDomains = new ArrayList<>();
		for (int i = 0; i < atomTypes.size(); i++) {
			int start = globalStarts.get(i);
			int size = atomTypes.get(i).getTotalDomainSize();
			AtomDomain dom = new AtomDomain(start, size);
			if (atomTypes.get(i).getTag() != null)
				dom.setTag(atomTypes.get(i).getTag());
			globalDomains.add(dom);
		}
		return globalDomains;
	}
	
	/**
	 * Container for the decoding of a literal.<br/>
	 * Contains the AtomType, the original arguments and the truth value
	 * (true of false / positive or negative).
	 * @author Dominik Schreiber
	 */
	public class Symbol {
		public AtomType type;
		public int[] args;
		public boolean value;
	}
	
	/**
	 * Decoding function.<br/>
	 * Returns the type, the arguments and the truth value
	 * of the symbolic predicate that corresponds to the given literal.
	 */
	public Symbol symbol(int atom) {
		
		Symbol symbol = new Symbol();

		// True of False?
		symbol.value = atom > 0;
		atom = Math.abs(atom);
		
		// Find the type of atom, and express atom
		// relative to the start of its type's domain
		symbol.type = type(atom);
		atom -= globalStarts.get(atomTypes.indexOf(symbol.type));
		
		// Calculate arguments		
		List<AtomDomain> domains = symbol.type.getArgumentDomains();
		int[] args = new int[domains.size()];
		for (int argIdx = 0; argIdx < domains.size()-1; argIdx++) {
			int divisor = 1;
			for (int i = argIdx+1; i < domains.size(); i++) {
				divisor *= domains.get(i).getSize();
			}
			args[argIdx] = (atom) / divisor;
			atom -= args[argIdx] * divisor;
		}
		if (args.length > 0)
			args[args.length-1] = atom;
		symbol.args = args;
		
		// Add start values of atom domains
		for (int i = 0; i < args.length; i++) {
			args[i] += domains.get(i).getStart();
		}

		return symbol;
	}
	
	public AtomType type(int atom) {
		
		for (int typeIdx = 0; typeIdx < atomTypes.size(); typeIdx++) {
			AtomType type = atomTypes.get(typeIdx);
			if (atom >= globalStarts.get(typeIdx) && atom < 
					globalStarts.get(typeIdx) + type.getTotalDomainSize()) {
				return type;
			}
		}
		
		// No matching type found
		throw new IllegalArgumentException("No matching type found for atom " + atom);
	}

	/**
	 * Prohibits any changes made to this domain (by adding new atom types)
	 * from this point on.
	 */
	public void concludeDomain() {
		
		modifiable = false;
	}
	
	public List<AtomType> getAtomTypes() {
		return atomTypes;
	}
	
	public boolean isModifiable() {
		return modifiable;
	}
}
