package htnsat.sat;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import htnsat.htn.HtnAction;
import htnsat.htn.HtnProblem;
import htnsat.htn.HtnProblemData;
import pddl4j.util.Signature;

public class ActionConflicts {

	private List<Set<Integer>> preconditions;
	private List<Set<Integer>> effects;
	
	private List<List<Integer>> conflicts;

	public ActionConflicts(HtnProblem problem) {
		
		preconditions = new ArrayList<>();
		effects = new ArrayList<>();
		
		extractActionInfo(problem);
		calculateConflicts();
	}
	public ActionConflicts(HtnProblemData data) {
		
		preconditions = new ArrayList<>();
		effects = new ArrayList<>();
		
		extractActionInfo(data);
		calculateConflicts();
	}
	
	public List<Integer> getConflicts(int action) {
		
		return conflicts.get(action);
	}
	
	private void extractActionInfo(HtnProblem problem) {
		
		for (int task : problem.getReachablePrimitiveTasks()) {
			Signature sig = problem.getTaskSignature(task);
			
			Set<Integer> preconds = new HashSet<>();
			Set<Integer> effects = new HashSet<>();
			
			for (int p : problem.getActionPreconditionsPos(sig)) {
				preconds.add(problem.compressFact(p));
			}
			for (int p : problem.getActionPreconditionsNeg(sig)) {
				preconds.add(-problem.compressFact(p));
			}
			for (int p : problem.getActionEffectsPos(sig)) {
				effects.add(problem.compressFact(p));
			}
			for (int p : problem.getActionEffectsNeg(sig)) {
				effects.add(-problem.compressFact(p));
			}
			
			this.preconditions.add(preconds);
			this.effects.add(effects);
		}
	}
	
	private void extractActionInfo(HtnProblemData data) {
		
		for (int task : data.getPrimitiveTasks()) {
			HtnAction action = data.getActionOfTask(task);
			
			Set<Integer> preconds = new HashSet<>();
			Set<Integer> effects = new HashSet<>();
			
			for (int p : action.getPreconditionsPos()) {
				preconds.add(data.compressFact(p));
			}
			for (int p : action.getPreconditionsNeg()) {
				preconds.add(-data.compressFact(p));
			}
			for (int p : action.getEffectsPos()) {
				effects.add(data.compressFact(p));
			}
			for (int p : action.getEffectsNeg()) {
				effects.add(-data.compressFact(p));
			}
			
			this.preconditions.add(preconds);
			this.effects.add(effects);
		}
	}
	
	private void calculateConflicts() {
		
		int numConflicts = 0;
		conflicts = new ArrayList<>(preconditions.size());
		for (int a1 = 0; a1 < preconditions.size(); a1++) {
			List<Integer> c = new ArrayList<>();
			for (int a2 = 0; a2 < preconditions.size(); a2++) {
				if (a1 != a2 && conflicts(a1, a2)) {
					c.add(a2);
					numConflicts++;
				}
			}
			conflicts.add(c);
		}
		System.out.println(numConflicts + " conflicts between actions found.");
	}
	
	private boolean conflicts(int a1, int a2) {

		boolean conflictsDirection1 = false;
		for (int effect : effects.get(a1)) {
			for (int precond : preconditions.get(a2)) {
				if (precond == -effect) {
					conflictsDirection1 = true;
				}
			}
		}
		boolean conflictsDirection2 = false;
		for (int effect : effects.get(a2)) {
			for (int precond : preconditions.get(a1)) {
				if (precond == -effect) {
					conflictsDirection2 = true;
				}
			}
		}
		return conflictsDirection1 || conflictsDirection2;
	}
}
