package htnsat.sat;

import java.util.ArrayList;
import java.util.List;

import htnsat.util.Assert;
import htnsat.util.BinaryNumberTree;
import htnsat.util.BinaryNumberTree.Node;

public class SatUtils {

	public enum AtMostOneMode {
		quadratic, tree, binary;
	}
	
	public static SatFormula atMostOne(AtMostOneMode mode, List<Integer> atoms, 
			SatDomain d) {
		
		if (mode == AtMostOneMode.quadratic) {
			
			return atMostOneQuadratic(atoms);
		} else if (mode == AtMostOneMode.binary) {
			Assert.that(d.isModifiable(), "The domain is not modifiable.");
			return atMostOneBinary(atoms, d);
		} else {
			Assert.that(d.isModifiable(), "The domain is not modifiable.");
			return atMostOneTree(atoms, d);
		}
	}
	
	/**
	 * Returns an at-most-one encoding of the given atoms.
	 * It uses |atoms| helper variables and produces an amount
	 * of clauses linear w.r.t. |atoms|,
	 * with exactly two variables per clause.<br/>
	 * A new AtomDomain (where the helper variables live)
	 * will be added to the given SatDomain.
	 */
	private static SatFormula atMostOneTree(List<Integer> atoms, SatDomain d) {
		
		SatFormula f = new SatFormula();
		
		if (atoms.size() <= 1) {
			return f;
		}

		AtomDomain helperDomain = new AtomDomain(0, atoms.size());
		AtomType exclusionHelper = new AtomType(helperDomain);
		d.addAtomType(exclusionHelper);

		// Init data structure
		int[] vars = new int[atoms.size()];
		for (int i = 0; i < vars.length; i++) {
			vars[i] = atoms.get(i);
		}
		// Group variables iteratively
		int groupSize = 2;
		while (groupSize < atoms.size()) {
			for (int idx = 0; idx + groupSize/2 < atoms.size(); 
					idx += groupSize) {
				int left = idx;
				int right = idx + groupSize/2;
				// "horizontal" amo
				f.addClause(-vars[left], -vars[right]);
				// "vertical" implication
				int varHelper = d.atom(exclusionHelper, right);
				f.addClause(-vars[left], varHelper);
				f.addClause(-vars[right], varHelper);
				// save helper variable
				vars[left] = varHelper;
			}
			groupSize *= 2;
		}
		// at-most-one of final helper variable pair
		f.addClause(-vars[0], -vars[groupSize/2]);
		
		return f;
	}
	
	public static SatFormula atMostOneQuadratic(List<Integer> atoms) {
		
		SatFormula f = new SatFormula();
		
		if (atoms.size() <= 1) {
			return f;
		}
		
		for (int i = 0; i < atoms.size(); i++) {
			for (int j = i+1; j < atoms.size(); j++) {
				// NAND of all pairs of distinct actions
				f.addClause(-atoms.get(i), -atoms.get(j));				
			}
		}
		
		return f;
	}
	
	public static SatFormula atMostOneBinary(List<Integer> atoms, SatDomain d) {
		
		SatFormula f = new SatFormula();
		
		BinaryEncoding enc = new BinaryEncoding(d, atoms.size());
		for (int atomIdx = 0; atomIdx < atoms.size(); atomIdx++) {
			int atom = atoms.get(atomIdx);
			Integer[] literals = enc.posLiterals(atomIdx);
			for (int lit : literals) {
				f.addClause(-atom, lit);
			}
		}
		
		return f;
	}
	
	/**
	 * Returns a set of clauses which together ensure that
	 * the bits of the given binary encoding will take
	 * exactly one of the provided values.
	 * @param enc The binary encoding where the provided
	 * encoded values come from
	 * @param binaryAtoms a list of literal arrays whereas
	 * each literal array is the result of some posLiterals(*)
	 * of the given binary encoding.
	 * 
	 * <br/><br/>Note that this method call includes
	 * recursive methods of a maximum depth corresponding 
	 * to the bit length of the encoding.
	 */
	public static List<SatClause> anyBinaryNumber(BinaryEncoding enc, 
			List<Integer[]> binaryAtoms) {

		// Convert literals to {0,1} digits
		List<Integer[]> binaryNumbers = new ArrayList<>();
		for (Integer[] code : binaryAtoms) {
			Integer[] binNum = new Integer[code.length];
			for (int pos = 0; pos < code.length; pos++) {			
				binNum[pos] = (code[pos] > 0 ? 1 : 0);
			}
			binaryNumbers.add(binNum);
		}
		
		// Build tree representation
		BinaryNumberTree tree = new BinaryNumberTree();
		for (Integer[] num : binaryNumbers) {
			tree.addCompact(num);
		}
		
		// Convert tree to logic formula
		Node n = tree.getRoot();
		return encodingOf(n, new SatClause(), enc);
	}
	
	private static List<SatClause> encodingOf(Node n, SatClause clauseSoFar, 
			BinaryEncoding enc) {
		
		List<SatClause> clauses = new ArrayList<>();
		int pos = clauseSoFar.getSize();
		
		if (pos >= enc.getBitLength())
			return clauses;
		
		SatClause c0 = new SatClause(-enc.getAtomForBit(pos));
		c0.addAll(clauseSoFar);
		SatClause c1 = new SatClause(enc.getAtomForBit(pos));
		c1.addAll(clauseSoFar);
		if (n.child0 != null && n.child1 != null) {				
			// both children present
			clauses.addAll(encodingOf(n.child0, c1, enc));
			clauses.addAll(encodingOf(n.child1, c0, enc));
		} else if (n.child0 != null) {
			// only child at 0
			clauses.add(c0);
			clauses.addAll(encodingOf(n.child0, c1, enc));
		} else if (n.child1 != null) {
			// only chid at 1
			clauses.add(c1);
			clauses.addAll(encodingOf(n.child1, c0, enc));
		} else {
			// leaf node
		}
		
		return clauses;
	}
	
}
