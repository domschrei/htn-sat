package htnsat.sat;

import java.util.Arrays;
import java.util.List;

import htnsat.encode.AtomTag;

public class AtomType {
	
	private List<AtomDomain> argumentDomains;
	private int encodingIndex;
	
	private String description;
	private AtomTag tag;
	
	public AtomType(AtomDomain... domains) {
		this.argumentDomains = Arrays.asList(domains);
	}
	
	public AtomType(AtomTag tag, AtomDomain... domains) {
		this.tag = tag;
		this.argumentDomains = Arrays.asList(domains);
	}
	
	public List<AtomDomain> getArgumentDomains() {
		return argumentDomains;
	}
	
	public int getTotalDomainSize() {
		int totalDomainSize = 1;
		for (AtomDomain domain : argumentDomains) {
			totalDomainSize *= domain.getSize();
		}
		return totalDomainSize;
	}
	
	public int getEncodingIndex() {
		return encodingIndex;
	}
	
	public void setEncodingIndex(int encodingIndex) {
		this.encodingIndex = encodingIndex;
	}
	
	public AtomTag getTag() {
		return tag;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getDescription() {
		return description;
	}
	
	public String description(Integer... args) {
		String desc = getDescription();
		for (int i = 0; i < args.length; i++) {
			desc = desc.replaceAll("#" + i, "" + args[i]);
		}
		return desc;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((argumentDomains == null) ? 0 : argumentDomains.hashCode());
		result = prime * result + encodingIndex;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AtomType other = (AtomType) obj;
		if (argumentDomains == null) {
			if (other.argumentDomains != null)
				return false;
		} else if (!argumentDomains.equals(other.argumentDomains))
			return false;
		if (encodingIndex != other.encodingIndex)
			return false;
		return true;
	}
}
