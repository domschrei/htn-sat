package htnsat.sat;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import htnsat.Logger;

/**
 * Wrapper around external SAT solver executable files.
 * Instances of this class can be used to write a
 * given SatFormula to a certain file, to call the
 * solving process, and to possibly retrieve the raw
 * (i.e. encoded) results. 
 * 
 * @author Dominik Schreiber
 */
public class SatSolver {

	/**
	 * Path and file name of the executable for
	 * non-incremental solving.
	 */
	public static final String EXECUTABLE_SINGLE 
				= "./solve.sh";
	/**
	 * Path and file name of the executable for
	 * incremental solving.
	 */
	public static final String EXECUTABLE_INCREMENTAL 
				= "./solve_incremental.sh";
	/**
	 * Path and file name of the executable for
	 * incremental "plus" solving.
	 */
	public static final String EXECUTABLE_T_REX
				= "./interpreter_t-rex";
	/**
	 * The file which should be created
	 * and which should be used as input for solving.
	 */
	public static final String FILE_ENCODED_FORMULA
				= "f.cnf";
	
	/**
	 * Represents the satisfiability status
	 * of a result.
	 * <ul>
	 * <li><b>sat</b>: A satisfying assignment to all
	 * variables has been found and can be retrieved and
	 * decoded.</li>
	 * <li><b>unsat</b>: The encoded formula has been
	 * found to be unsatisfiable. This can also be the
	 * result of a too short maximal step size for 
	 * non-incremental solving or of an inadequate 
	 * hyperparameter.</li>
	 * <li><b>unknown</b>: The solver could not decide
	 * whether there is a satisfying assignment for this 
	 * formula, possibly due to occurred errors or 
	 * limited resources.</li>
	 * </ul>
	 */
	public enum Satisfiability {
		
        sat("SAT"), unsat("UNSAT"), unknown("unknown");
		
        private String desc;
        private Satisfiability(String desc) {
            this.desc = desc;
        }
       
        @Override
        public String toString() {
            return desc;
        }
    } 
	
	/**
	 * Data struct which contains information 
	 * concerning a SAT solving process
	 * after its termination.
	 * 
	 * @author Dominik Schreiber
	 */
	public class Result {
		/**
		 * Contains the satisfiability status of the formula
		 * (see the enum docs for its definition).
		 * <br/>Default value: <i>unknown</i> (a cancelled 
		 * solving process should always yield "unknown")
		 */
		public Satisfiability sat;
		/**
		 * The solve mode which has been used.
		 */
		public SolveMode solveMode;
		/**
		 * If an assignment has been found, this field
		 * represents the satisfying assignment of variables
		 * from a <b>non-incremental</b> solving process.
		 * <br/>Representation: list of all atoms which 
		 * are assigned to 1 / true (all the other atoms
		 * from the domain are assigned to zero)
		 */
		public List<Integer> values;
		/**
		 * If an assignment has been found, this field
		 * represents the satisfying assignment of variables
		 * from a <b>incremental</b> solving process.
		 * <br/>Representation: For each step, a list of all 
		 * atoms which are assigned to 1 / true (all the other 
		 * atoms from the domain are assigned to zero).
		 * The lists per steps are sorted ascendingly, i.e.
		 * beginning with step 0.
		 */
		public List<List<Integer>> incValues;
	}
	
	public enum SolveMode {
		single, incremental, tRex;
	}
	
	private Result result;	
	private SolveMode solveMode;
	private String[] interpreterParams = {};
	
	private Logger log;
	private String formulaCopyPath;
	
	private ISatFormula f;
	
	/**
	 * @param incremental true, if the solving process
	 * should incrementally solve a DIMSPEC-style 
	 * representation of a plan;
	 * <br/>false, if a flat DIMACS-style formula
	 * is to be solved.
	 */
	public SatSolver(SolveMode solveMode) {
				
		result = new Result();
		result.sat = Satisfiability.unknown;
		result.values = null;
		result.incValues = null;
		result.solveMode = solveMode;
		
		this.solveMode = solveMode;
	}
	
	/**
	 * Writes the provided SatFormula object
	 * to a DIMACS conformant .cnf text file.
	 */
	public void writeToFile(ISatFormula f) {
		
		this.f = f;
		
		try {
			// Write formula to file
			BufferedWriter w = new BufferedWriter(new FileWriter(FILE_ENCODED_FORMULA));
			
			f.output(w);
			w.close();
			
			// Copy formula into log directory
			if (formulaCopyPath != null) {
				Files.copy(
						new File(FILE_ENCODED_FORMULA).toPath(), 
						new File(formulaCopyPath)
							.toPath(), 
						StandardCopyOption.REPLACE_EXISTING);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Calls the SAT solver executable on the
	 * previously written problem (by writeFormulaToFile()).
	 * <br/>
	 * If successful, the result can be retrieved with
	 * getResult().
	 * @return true, if the SAT solver has been executed
	 * and read from successfully (irrespective of the result).
	 */
	public boolean solve() {

		// Identify correct solver executable
		String executable;
		if (solveMode == SolveMode.single) {
			executable = EXECUTABLE_SINGLE;
		} else if (solveMode == SolveMode.incremental) {
			executable = EXECUTABLE_INCREMENTAL;
		} else {
			executable = EXECUTABLE_T_REX;
		}
		
		// Execute external solver
		try {
			// Create process
			File output = new File(log.getFile());
			ProcessBuilder processBuilder = new ProcessBuilder(
					executable);
			
			// Have interpreter parameters been set?
			List<String> args = new ArrayList<>();
			args.add(executable);
			if (interpreterParams.length > 0) {
				args.addAll(Arrays.asList(interpreterParams));
			}
			if (executable == EXECUTABLE_T_REX) {
				// Explicitly add the input file
				args.add("-i");
				args.add("f.cnf");
			}
			processBuilder.command(args);
			
			Process process = processBuilder.redirectOutput(output)
					.redirectErrorStream(true).start();
			
			/*
			// Reads output of executable
			InputStream is = process.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			 */
			/*
			if (solveMode == SolveMode.incrementalPlus) {
				// Writes input to executable
				OutputStream os = process.getOutputStream();
				OutputStreamWriter osw = new OutputStreamWriter(os);
				BufferedWriter writer = new BufferedWriter(osw);
				
				// Provide formula to the executable
				f.output(writer);
				writer.close();
			}*/
			
			// Wait for the termination
			process.waitFor();
			
			File solutionFile;
			if (solveMode == SolveMode.tRex) {
				solutionFile = new File("SOLUTION.txt");
				if (!solutionFile.exists()) {
					return false;
				}
			} else {
				solutionFile = output;
			}
			BufferedReader br = new BufferedReader(
					new FileReader(solutionFile));
			
			// Log initialization
			if (log == null)
				log = new Logger(false); // basically /dev/null
			
			// Read results
			String line;
			boolean readValues = false;
			int numSteps = 0;
			int currentStep = 0;
			while ((line = br.readLine()) != null) {
								
				boolean incremental = (solveMode != SolveMode.single);
				if (incremental) {
										
					if (line.startsWith("solution")) {
						
						result.sat = Satisfiability.sat;
						readValues = true;
						String[] lineWords = line.split(" ");
						numSteps = Integer.parseInt(lineWords[lineWords.length-1]);
						result.incValues = new ArrayList<>();
						
					} else if (readValues) {
						
						result.incValues.add(new ArrayList<>());
						String[] lineWords = line.split(" ");
						for (String word : lineWords) {
							try {
								int val = Integer.parseInt(word);
								if (val > 0) {
									result.incValues.get(
											result.incValues.size()-1).add(val);
								}
							} catch (NumberFormatException e) {
								// do nothing
							}
						}						
						
						currentStep++;
						if (currentStep == numSteps) {
							readValues = false;
						}
					}
					
				} else {
					
					// satisfiability line
					if (line.startsWith("s")) {
						if (line.contains("UNSATISFIABLE")) {
							result.sat = Satisfiability.unsat;
						} else if (line.contains("SATISFIABLE")) {
							result.sat = Satisfiability.sat;
						} else {
							result.sat = Satisfiability.unknown;
						}
					}
					
					// values line
					if (line.startsWith("v")) {
						result.values = new ArrayList<>();
						String[] words = line.split(" ");
						for (String word : words) {
							try {
								int val = Integer.parseInt(word);
								if (val > 0) {
									result.values.add(val);
								}
							} catch (NumberFormatException e) {
								// do nothing
							}
						}
					}
				}
			}
			br.close();

			return true;
		
		} catch (IOException e1) {
			e1.printStackTrace();
			return false;
		} catch (InterruptedException e1) {
			e1.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Sets a Logger object to use for logging the solver's
	 * output. The filename must have been already specified.
	 */
	public void setLogger(Logger log) {
		
		this.log = log;
	}
	
	/**
	 * Sets the file where to copy the written SatFormula
	 * (for logging purposes).
	 */
	public void setFormulaCopyPath(String path) {
		
		this.formulaCopyPath = path;
	}
	
	public void setInterpreterParams(String[] interpreterParams) {
		
		this.interpreterParams = interpreterParams;
	}
	
	/**
	 * Returns a Result object which has a valid satisfiability
	 * value (possibly <i>unknown</i>).
	 */
	public Result getResult() {
		return result;
	}
	
	public Logger getLogger() {
		return log;
	}
}
