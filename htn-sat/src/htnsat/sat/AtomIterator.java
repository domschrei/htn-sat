package htnsat.sat;

import java.util.Iterator;

public class AtomIterator implements Iterable<Integer> {

	private SatDomain d;
	private AtomType atomType;
	private int iteratedArgIdx;
	private AtomDomain iteratedDomain;
	
	public AtomIterator(SatDomain d, AtomType type, 
			int iteratedDomainIdx, int... remainingArgs) {
		
		this.d = d;
		this.atomType = type;
		this.iteratedArgIdx = iteratedDomainIdx;
		this.iteratedDomain = type.getArgumentDomains().get(iteratedDomainIdx);
		
		init(remainingArgs);
	}
	
	private void init(int... remainingArgs) {
		
		int[] args = new int[remainingArgs.length+1];
		for (int idx = 0; idx < iteratedArgIdx; idx++) {
			args[idx] = remainingArgs[idx];
		}
		args[iteratedArgIdx] = iteratedDomain.getStart();
		for (int idx = iteratedArgIdx+1; idx < args.length; idx++) {
			args[idx] = remainingArgs[idx-1];
		}
		
		firstAtom = d.atom(atomType, args);
		args[iteratedArgIdx]++;
		int secondAtom = d.atom(atomType, args);
		step = secondAtom - firstAtom;
	}
	
	private int firstAtom;
	private int step;
	
	@Override
	public Iterator<Integer> iterator() {
		
		return new Iterator<Integer>() {

			private int idx = 0;
			private int atom = firstAtom;
			private int stepSize = step;
			
			@Override
			public boolean hasNext() {
				return idx < iteratedDomain.getSize();
			}

			@Override
			public Integer next() {
				final int atomOut = atom;
				idx++;
				atom += stepSize;
				return atomOut;
			}
		};
	}

}
