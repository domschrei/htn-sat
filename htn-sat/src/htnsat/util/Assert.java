package htnsat.util;

public class Assert {

	public static void that(boolean condition, String errorMsg) {
		if (!condition) {
			throw new IllegalArgumentException(errorMsg);
		}
	}
	
}
