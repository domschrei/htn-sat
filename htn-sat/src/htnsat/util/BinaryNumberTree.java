package htnsat.util;

import java.util.Arrays;

public class BinaryNumberTree {

	public class Node {
		public Node child0;
		public Node child1;
		public Integer[] content;
		
		@Override
		public String toString() {
			
			return toString(0);
		}
		
		private String toString(int depth) {
			
			String space = "";
			for (int i = 0; i < depth; i++) space += "  ";
			String out = "";//space + "Content: " + Arrays.toString(content) + "\n";
			if (child0 != null) {
				out += space + "child 0: {\n";
				out += child0.toString(depth+1);
				out += space + "}\n";
			}
			if (child1 != null) {
				out += space + "child 1: {\n";
				out += child1.toString(depth+1);
				out += space + "}\n";
			}
			return out;
		}
	}
	
	private Node root;
	
	public BinaryNumberTree() {
		this.root = new Node();
	}
	
	public void add(Integer[] number) {
		
		Node n = root;
		Node fatherNode = null;
		boolean leftChild = true;
		
		Integer[] remainingDigits = number;
		
		while (n.content != null) {
			
			if (remainingDigits.length >= n.content.length) {
				
				// find the 1st position where the arrays differ; split node there
				for (int pos = 0; pos < n.content.length; pos++) {
					if (remainingDigits[pos] != n.content[pos]) {
						
						Node splitNode = new Node();
						splitNode.content = Arrays.copyOf(remainingDigits, pos);
						Node node1 = new Node();
						Node node2 = n;
						splitNode.child0 = (remainingDigits[pos] < n.content[pos] ? node1 : node2);
						splitNode.child1 = (remainingDigits[pos] < n.content[pos] ? node2 : node1);
						node1.content = Arrays.copyOfRange(remainingDigits, pos, remainingDigits.length);
						node2.content = Arrays.copyOfRange(n.content, pos, n.content.length);
						
						if (fatherNode == null) {	
							root = splitNode;
						} else {
							if (leftChild) {
								fatherNode.child0 = splitNode;
							} else {
								fatherNode.child1 = splitNode;
							}
						}
						
						return;
					}
				}
				
				// all compared digits are equal, but the number to add is longer;
				// go to child node
				remainingDigits = Arrays.copyOfRange(remainingDigits, n.content.length, remainingDigits.length);
				leftChild = (remainingDigits[0] == 0);
				fatherNode = n;
				if (leftChild && n.child0 == null)
					n.child0 = new Node();
				if (!leftChild && n.child1 == null)
					n.child1 = new Node();
				n = (leftChild ? n.child0 : n.child1);
				
			} else {
				
				// element to add is shorter than current node content
				
				// should not happen, as all numbers of the tree are 
				//assumed to have equal length
			}
		}
		
		// Content of current node is empty; put new element here
		n.content = remainingDigits;
	}
	
	
	public void addCompact(Integer[] number) {
		
		Node n = root;
		int pos = 0;
		while (pos < number.length) {
			boolean zero = (number[pos] == 0);
			if ((zero ? n.child0 : n.child1) == null) {
				// insert new node
				if (zero) {
					n.child0 = new Node();
				} else {
					n.child1 = new Node();
				}
			}
			n = (zero ? n.child0 : n.child1);
			pos++;
		}
	}
	
	public Node getRoot() {
		
		return root;
	}
	
	@Override
	public String toString() {
		
		return root.toString();
	}
}
