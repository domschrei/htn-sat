package htnsat;

import java.util.ArrayList;
import java.util.List;

import com.beust.jcommander.Parameter;

import htnsat.encode.Htn2SatEncoder;
import htnsat.encode.Htn2SatEncoder.Mode;

/**
 * Bundles the set of program arguments.
 *
 * @author Dominik Schreiber
 */
public class Args {

	// See Htn2SatEncoder.Mode for comments regarding these modes
	// public static final String MODE_PRIMITIVE_SEQUENTIAL = "primitive_seq";
	// public static final String MODE_PRIMITIVE_PARALLEL = "primitive_par";
	public static final String MODE_LINEAR_FORWARD = "gct";
	public static final String MODE_SMS_BINARY_TASKS = "sms_binary_tasks";
	public static final String MODE_SMS_UNARY_METHODS = "sms_unary_decomp";
	public static final String MODE_SMS_UNARY_TASKS = "sms_unary_tasks";
	// public static final String MODE_SMS_BINARY_METHODS = "sms_binary_methods";
	// public static final String MODE_MULTI_TASK_STACK = "mms";
	// public static final String MODE_STEPWISE_REFINEMENT = "sir";
	public static final String MODE_T_REX = "t-rex";
	public static final String MODE_T_REX_TASKS = "t-rex_tasks";
	public static final String MODE_GTOHP = "gtohp";
	
	@Parameter
	protected List<String> parameters = new ArrayList<>();
	
	@Parameter(names = { "-d", "--domain" }, description = "Domain file", order=0)
	protected String domainFile;
	
	@Parameter(names = { "-p", "--problem" }, description = "Problem file", order=1)
	protected String problemFile;
	
	@Parameter(names = { "-e", "--encoding" }, description = 
			"Encoding mode: {"
			// + MODE_PRIMITIVE_SEQUENTIAL + ", "
			// + MODE_PRIMITIVE_PARALLEL  + ", "
			+ MODE_LINEAR_FORWARD  + ", "
			+ MODE_SMS_BINARY_TASKS + ", "
			+ MODE_SMS_UNARY_TASKS + ", "
			// + MODE_SMS_BINARY_METHODS + ", "
			+ MODE_SMS_UNARY_METHODS + ", "
			// + MODE_MULTI_TASK_STACK + ", "
			// + MODE_STEPWISE_REFINEMENT + ", "
			+ MODE_T_REX + ", "
			+ MODE_T_REX_TASKS + "} (NOT case-sensitive)", 
			order=2)
	protected String modeStr;	
	
	@Parameter(names = { "-m", "--max-iterations" }, description = 
			"The maximal amount of iterations to calculate"
			+ " (for non-incremental solving) or the maximal "
			+ "stack size (for incremental solving).", order=3)
	protected Integer maxSize;
	
	@Parameter(names = { "-v", "--verbose" }, description = "Also print output to stdout", 
			order=4)
	protected Boolean verbose;
	
	@Parameter(names = { "-i", "--interpreter-params" }, description = 
			"A comma-separated list of parameters, within curly braces, "
			+ "to provide to the interpreter application")
	protected String interpreterParams;
	
	@Parameter(names = { "-0", "--only-output-formula" }, description = 
			"Do not solve, but only output the encoding to \"f.cnf\"")
	protected boolean onlyOutputFormula;
	
	// Examples
	/*
	@Parameter(names = { "-log", "-verbose" }, description = "Level of verbosity")
	private Integer verbose = 1;
	
	@Parameter(names = "-groups", description = "Comma-separated list of group names to be run")
	private String groups;
	
	@Parameter(names = "-debug", description = "Debug mode")
	private boolean debug = false;
	*/
	
	public static Htn2SatEncoder.Mode getMode(String modeStr) {
		
		switch (modeStr.toLowerCase()) {
		// case MODE_PRIMITIVE_SEQUENTIAL:
		// 	return Mode.primitiveSequential;
		// case MODE_PRIMITIVE_PARALLEL:
		//  return Mode.primitiveParallel;
		case MODE_LINEAR_FORWARD:
			return Mode.linearForward;
		case MODE_SMS_BINARY_TASKS:
			return Mode.smsBinaryTasks;
		case MODE_SMS_UNARY_TASKS:
			return Mode.smsUnaryTasks;
		// case MODE_SMS_BINARY_METHODS:
		// 	return Mode.smsBinaryMethods;
		case MODE_SMS_UNARY_METHODS:
			return Mode.smsUnaryMethods;
		// case MODE_MULTI_TASK_STACK:
		//	return Mode.mms;
		// case MODE_STEPWISE_REFINEMENT:
		//	return Mode.sir;
		case MODE_T_REX:
			return Mode.tRex;
		case MODE_T_REX_TASKS:
			return Mode.tRexTasks;
		case MODE_GTOHP:
			return Mode.gtohp;
		default:
			return null;
		}
	}
	
	public String[] getInterpreterParams() {
		
		if (interpreterParams == null || interpreterParams.length() == 0)
			return new String[] {};
		
		if (interpreterParams.length() < 3 || interpreterParams.charAt(0) != '{' 
			|| interpreterParams.charAt(interpreterParams.length()-1) != '}') {
			throw new IllegalArgumentException("Error: The interpreter params are malformed.");
		}
		
		String paramList = interpreterParams.substring(1, interpreterParams.length()-1);//.replaceAll("\\+", "-");
		return paramList.split(",");
	}
}
