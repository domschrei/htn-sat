package htnsat;

import htnsat.encode.Htn2SatEncoder.Mode;

public class HtnSolverParams {

	public Mode mode;
	public int maxSize;
	public boolean verbose;
	public String[] interpreterParams;
	public boolean onlyOutputFormula;
}
