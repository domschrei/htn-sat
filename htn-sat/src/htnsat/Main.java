package htnsat;

import java.io.FileNotFoundException;

import com.beust.jcommander.JCommander;

import htnsat.encode.Htn2SatEncoder.Mode;
import pddl4j.parser.Parser;

public class Main {

	public static final int NONINC_MAX_STEPS = 100;
	public static final int INC_MAX_DEPTH = 30;
	
	public static void main(String[] argv) {
		
		// Parse arguments
		Args args = new Args();
		JCommander argCmd = JCommander.newBuilder()
					.addObject(args).build();
		argCmd.parse(argv);

		// Read argument values
		String domainFile = args.domainFile;
		String problemFile = args.problemFile;
		String modeStr = args.modeStr;
		boolean verbose = (args.verbose != null ? args.verbose : false);
		
		// Valid arguments?
		if (domainFile == null || problemFile == null) {
			System.out.println("Please provide both a domain file "
					+ "and a problem file.");
			// Print proper usage of the application
			argCmd.usage();
			System.exit(1);
		}
		
		// Which encoding mode has been chosen?
		Mode mode = Args.getMode(modeStr);
		if (mode == null) {
			System.out.println("Please provide a valid encoding mode.");
			argCmd.usage();
			System.exit(1);
		}

		// Evaluate maximal depth parameter
		int maxSize;
		if (args.maxSize != null) {
			maxSize = args.maxSize;
		} else {			
			boolean incremental = (mode != Mode.linearForward);
			if (incremental) {
				maxSize = INC_MAX_DEPTH;
			} else {
				maxSize = NONINC_MAX_STEPS;
			}
		}
		
		// Initialize logging
		Logger.init(domainFile, problemFile,
					modeStr);

		// Initialize parameters for the HtnSolver
		HtnSolverParams params = new HtnSolverParams();
		params.interpreterParams = args.getInterpreterParams();
		params.maxSize = maxSize;
		params.mode = mode;
		params.verbose = verbose;
		params.onlyOutputFormula = args.onlyOutputFormula;
		
		// Parse the problem
		Parser parser = new Parser();
 		try {
 			parser.parse(domainFile, problemFile);
 		} catch (FileNotFoundException e) {
 			System.out.println(e.getMessage());
 		}
 		if (!parser.getErrorManager().isEmpty()) {
 			parser.getErrorManager().printAll();
 		} else {
 			
 			// Attempt to solve the problem
			HtnSolver solver = new HtnSolver(params);
			solver.solveProblem(parser.getProblem(), parser.getDomain());
 		}
	}
}
