package htnsat.htn;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import pddl4j.util.BitOp;

public class HtnAction {

	private BitOp operator;
	
	private List<Integer> preconditionsPos;
	private List<Integer> preconditionsNeg;
	private List<Integer> effectsPos;
	private List<Integer> effectsNeg;
	
	public HtnAction(BitOp operator) {
		this.operator = operator;
		this.preconditionsPos = new ArrayList<>();
		this.preconditionsNeg = new ArrayList<>();
		this.effectsPos = new ArrayList<>();
		this.effectsNeg = new ArrayList<>();
	}
	
	public void addPreconditionPos(int precond) {
		preconditionsPos.add(precond);
	}
	
	public void addPreconditionNeg(int precond) {
		preconditionsNeg.add(precond);
	}

	public void addEffectPos(int effect) {
		effectsPos.add(effect);
	}

	public void addEffectNeg(int effect) {
		effectsNeg.add(effect);
	}
	
	
	
	
	public String getName() {
		return operator.getName();
	}
	
	public List<Integer> getPreconditionsPos() {
		return preconditionsPos;
	}

	public List<Integer> getPreconditionsNeg() {
		return preconditionsNeg;
	}

	public List<Integer> getEffectsPos() {
		return effectsPos;
	}

	public List<Integer> getEffectsNeg() {
		return effectsNeg;
	}

	public BitOp getOperator() {
		return operator;
	}

	@Override
	public String toString() {
		return "{" + preconditionsPos.toString() + " ; " + preconditionsNeg.toString() + "} " 
				+ operator.getName() + Arrays.toString(operator.getInstantiations())
				+ " {" + effectsPos.toString() + " ; " + effectsNeg.toString() + "}";
	}
	
	public String toString(HtnProblemData data) {
		
		StringBuilder str = new StringBuilder();
		str.append("{(");
		for (int precond : preconditionsPos) {
			str.append(data.predicateToString(precond) + " ");
		}
		str.append("),(");
		for (int precond : preconditionsNeg) {
			str.append(data.predicateToString(precond) + " ");
		}
		str.append(")} ");
		str.append(operator.getName() + "(");
		for (int constant : operator.getInstantiations()) {
			str.append(constant + ",");
		}
		str.append(") {(");
		for (int effect : effectsPos) {
			str.append(data.predicateToString(effect) + " ");
		}
		str.append("),(");
		for (int effect : effectsNeg) {
			str.append(data.predicateToString(effect) + " ");
		}
		str.append(")}");
		return str.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((effectsNeg == null) ? 0 : effectsNeg.hashCode());
		result = prime * result + ((effectsPos == null) ? 0 : effectsPos.hashCode());
		result = prime * result + ((operator == null) ? 0 : operator.hashCode());
		result = prime * result + ((preconditionsNeg == null) ? 0 : preconditionsNeg.hashCode());
		result = prime * result + ((preconditionsPos == null) ? 0 : preconditionsPos.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HtnAction other = (HtnAction) obj;
		if (effectsNeg == null) {
			if (other.effectsNeg != null)
				return false;
		} else if (!effectsNeg.equals(other.effectsNeg))
			return false;
		if (effectsPos == null) {
			if (other.effectsPos != null)
				return false;
		} else if (!effectsPos.equals(other.effectsPos))
			return false;
		if (operator == null) {
			if (other.operator != null)
				return false;
		} else if (!operator.equals(other.operator))
			return false;
		if (preconditionsNeg == null) {
			if (other.preconditionsNeg != null)
				return false;
		} else if (!preconditionsNeg.equals(other.preconditionsNeg))
			return false;
		if (preconditionsPos == null) {
			if (other.preconditionsPos != null)
				return false;
		} else if (!preconditionsPos.equals(other.preconditionsPos))
			return false;
		return true;
	}
}
