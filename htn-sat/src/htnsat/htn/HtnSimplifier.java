package htnsat.htn;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.TreeSet;
import java.util.stream.Collectors;

import htnsat.util.Assert;
import pddl4j.parser.Connective;
import pddl4j.parser.Symbol;
import pddl4j.preprocessing.CodedProblem;
import pddl4j.preprocessing.Inertia;
import pddl4j.util.BitExp;
import pddl4j.util.BitOp;
import pddl4j.util.BitVector;
import pddl4j.util.IntExp;
import pddl4j.util.OneTaskConstraint;
import pddl4j.util.OrderedMethod;
import pddl4j.util.Signature;
import pddl4j.util.TaskNetwork;

/**
 * Converts a CodedProblem instance into an instance of HtnProblemData,
 * optimizing the representation of the data for the various encodings
 * into SAT.
 * 
 * @author Dominik Schreiber
 */
public class HtnSimplifier {
	
	/**
	 * The original problem, represented as a CodedProblem instance
	 */
	private CodedProblem p;
	/**
	 * The resulting problem representation
	 */
	private HtnProblemData data;	
	
	/**
	 * @param problem the original problem, given as a CodedProblem
	 * instance
	 */
	public HtnSimplifier(CodedProblem problem) {
		this.p = problem;
		this.data = new HtnProblemData(problem);
	}
	
	/**
	 * Performs a number of restructurings and simplifications
	 * on the provided CodedProblem data and outputs a new instance
	 * of HtnProblemData.
	 */
	public HtnProblemData simplify() {
			
		// Traverse task space, setting important data fields
		// and filtering the tasks and facts which are not needed
		reduceTasksAndFacts();
		
		// Compute a flat list of methods, beginning with a
		// virtual "initial method" containing the init task network
		computeFlatMethods();

		// Computes the reduced fact set, only containing the
		// "sign-less" facts without their negative counterparts
		compressFacts();
		
		computeFlatMethodIndices();
		
		//simplifyTrivialMethods();
		
		//computeEffectorsOfFacts();
		//buildHtnTree();
		//buildHierarchyMatrix();
		
		return data;
	}
	
	/**
	 * Traverses the space of reachable tasks starting with the initial tasks
	 * and calculates various kinds of data. 
	 */
	private void reduceTasksAndFacts() {
		
		// Traverse the space of tasks beginning with the set of 
		// initial tasks and reducing the list of relevant tasks and facts
		traverseAndReduce();
		
		// Split the calculated tasks into primitive 
		// and non-primitive ones, and compute fast
		// lookup for the primitive index of a task
		data.setPrimitiveTasks(new ArrayList<>());
		data.setNonprimitiveTasks(new ArrayList<>());
		data.setPrimitiveIndicesOfTasks(new ArrayList<>(data.getTasks().size()));
		data.setNonprimitiveIndicesOfTasks(new ArrayList<>(data.getTasks().size()));
		int primitiveCount = 0;
		int nonprimitiveCount = 0;
		for (int task = 0; task < data.getTasks().size(); task++) {
			if (data.isTaskPrimitive(task)) {
				data.getPrimitiveTasks().add(task);
				data.addPrimitiveIndexOfTask(primitiveCount);
				data.addNonprimitiveIndexOfTask(-1);
				primitiveCount++;
			} else {
				data.getNonprimitiveTasks().add(task);
				data.addPrimitiveIndexOfTask(-1);
				data.addNonprimitiveIndexOfTask(nonprimitiveCount);
				nonprimitiveCount++;
			}
		}
	}
	
	/**
	 * Traverses the graph of tasks connected by their
	 * possible reductions, updates the max* integer fields
	 * in the new problem instance, and sets its sets of tasks
	 * and facts to the tasks and facts which have been reached
	 * during the traversal.
	 */
	private void traverseAndReduce() {
		
		// Retrieve tasks from initial task network
		TaskNetwork initTaskNetwork = p.getInitExpansion();
		Integer[] initTasks = new Integer[initTaskNetwork.getTasks().size()];
		for (int i = 0; i < initTasks.length; i++) {
			initTasks[i] = initTaskNetwork.getTasks().get(i);
		}
		
		Stack<Integer> unprocessedTasks = new Stack<>();
		for (int t : initTasks) {			
			unprocessedTasks.push(t); // Push all initial tasks
		}
		BitVector processedTasks = new BitVector(p.getRelevantTasks().size());
		BitVector illegalTasks = new BitVector(p.getRelevantTasks().size()); // tasks w/o operators and methods
		BitVector processedFacts = new BitVector(p.getRelevantFacts().size()/2);

		// Add facts from the initial state
		for (int p : p.getInit().getPositive().getAllSetBits()) {
			processedFacts.set(p);
		}
		for (int p : p.getInit().getNegative().getAllSetBits()) {
			processedFacts.set(p);
		}
		
		// Do traversal through the graph of tasks
		while (!unprocessedTasks.isEmpty()) {
			int task = unprocessedTasks.pop();
			
			if (!isTaskPrimitive(task)) {
				
				processedTasks.set(task);
				
				// Expand non-primitive task
				List<OrderedMethod> methods = getReductionsForTask(task);
				data.setMaxReductionsPerTask(Math.max(data.getMaxReductionsPerTask(), 
						methods.size()));
				
				for (OrderedMethod method : methods) {
					
					data.setMaxExpansionSize(Math.max(data.getMaxExpansionSize(), 
							method.getExpansion().size()));
					
					// Visit each subtask of the expansion
					for (int newTask : method.getExpansion()) {
						if (!processedTasks.get(newTask)) {
							unprocessedTasks.push(newTask);
						}
					}
					// Add pre-constraints from the methods 
					// to the reachable facts
					for (int fact : method.getPositivePreconditions()
							.getAllSetBits()) {
						processedFacts.set(fact);
					}
					for (int fact : method.getNegativePreconditions()
							.getAllSetBits()) {
						processedFacts.set(fact);
					}
				}
			} else {
				// Visit primitive action
				BitOp operator = getOperator(task);
				if (operator == null) {
					System.err.println("Warning: Task " + p.printTask(task) + " (id " + task + ") has no realizations.");
					illegalTasks.set(task);
					continue;
				}
				processedTasks.set(task);
					
				// Add all preconditions and effects 
				// to the set of reachable facts
				BitExp preconditions = operator.getPreconditions();
				for (int fact : preconditions.getPositive()
						.getAllSetBits()) {
					processedFacts.set(fact);
				}
				for (int fact : preconditions.getNegative()
						.getAllSetBits()) {
					processedFacts.set(fact);
				}
				BitExp effects = operator.getUnconditionalEffects();
				for (int fact : effects.getPositive()
						.getAllSetBits()) {
					processedFacts.set(fact);
				}
				for (int fact : effects.getNegative()
						.getAllSetBits()) {
					processedFacts.set(fact);
				}
			}
		}
		
		eliminateIllegalTasksAndMethods(processedTasks, illegalTasks);
		
		// Set the tasks and facts of the new problem data
		// to the calculated reachable subsets
		data.setTasks(reduceTaskList(p.getRelevantTasks(), processedTasks));
		data.setFacts(reduceFactList(p.getRelevantFacts(), processedFacts));		
	}
	
	private void eliminateIllegalTasksAndMethods(BitVector reachableTasks, BitVector illegalTasks) {
		
		System.out.println("Before elimination: " + reachableTasks.cardinality() + " tasks");
		
		boolean anythingRemoved = true;
		while (anythingRemoved) {
			anythingRemoved = false;
		
			for (int task : reachableTasks.getAllSetBits()) {
				
				if (!isTaskPrimitive(task)) {
					List<OrderedMethod> methods = getReductionsForTask(task);
					for (int i = 0; i < methods.size(); i++) {
						
						OrderedMethod method = methods.get(i);
						boolean keepMethod = true;
						for (int subtask : method.getExpansion()) {
													
							if (isTaskPrimitive(subtask)) {
								if (getOperator(subtask) == null) {
									// Illegal subtask found:
									// eliminate this method
									keepMethod = false;
									break;
								}
							} else if (!reachableTasks.get(subtask)) {
								// Illegal subtask (from a previous iteration/task)
								keepMethod = false;
								break;
							}
						}
						
						if (!keepMethod) {
							methods.remove(i);
							i--;
							anythingRemoved = true;
						}
					}
					
					if (methods.size() == 0) {
						// There are no legal methods for this task:
						// remove this task
						reachableTasks.set(task, false);
						anythingRemoved = true;
					}
				}
			}
		}

		System.out.println("After elimination: " + reachableTasks.cardinality() + " tasks");
	}
	
	/**
	 * Returns a filtered list of tasks.
	 * Side effects: the fields for task primitivity and for
	 * the initial tasks are created and/or modified within
	 * the new problem data instance.
	 * 
	 * @param originalTasks the original list of tasks
	 * @param reachableElemIndices the indices of elements which
	 * are reachable, i.e. they should <b>not</b> be removed
	 * @return a reduced list of tasks according to the given indices
	 */
	private List<IntExp> reduceTaskList(List<IntExp> originalTasks, 
			BitVector reachableElemIndices) {
		
		System.out.println("  " + reachableElemIndices.cardinality() 
				+ "/" + originalTasks.size() + " tasks are reachable.");
		
		List<IntExp> reducedTasks = new ArrayList<>();
		List<Integer> newIndices = new ArrayList<>();
		
		// Create a new list of allowed elements
		// and remember the transitions of indices
		for (int i = 0; i < originalTasks.size(); i++) {
			if (reachableElemIndices.get(i)) {
				reducedTasks.add(originalTasks.get(i));
				newIndices.add(reducedTasks.size()-1);
			} else {
				newIndices.add(-1);
			}
		}
		
		// Update indices in expansions, and create
		// fast task primitivity lookup
		boolean[] isTaskPrimitive = new boolean[reducedTasks.size()];
		for (int task = 0; task < originalTasks.size(); task++) {
			
			int newIdx = newIndices.get(task);
			
			if (newIdx == -1)
				continue;
			
			if (!isTaskPrimitive(task)) {
				
				isTaskPrimitive[newIdx] = false;
				
				List<OrderedMethod> methods = getReductionsForTask(task);
				for (OrderedMethod method : methods) {
					
					List<Integer> expansion = method.getExpansion();
					for (int i = 0; i < expansion.size(); i++) {
						int oldIdx = expansion.get(i);			
						if (newIndices.get(oldIdx) == -1) {
							System.err.println("Error: an illegal task avoided elimination process");
							System.exit(1);
						}
						expansion.set(i, newIndices.get(oldIdx));
					}
				}
				
			} else {
				isTaskPrimitive[newIdx] = true;
			}
		}
		data.setIsTaskPrimitive(isTaskPrimitive);
		
		// Update indices in initial task network
		List<Integer> initExpansion = p.getInitExpansion().getTasks();
		System.out.println("Initial task network before elimination: " + initExpansion.size() + " tasks");
		for (int initIdx = 0; initIdx < initExpansion.size(); initIdx++) {
			int oldTask = initExpansion.get(initIdx);
			if (newIndices.get(oldTask) == -1) {
				// this initial task has been eliminated
				initExpansion.remove(initIdx);
				initIdx--;
			} else {
				initExpansion.set(initIdx, newIndices.get(oldTask));
			}
		}
		data.setInitTasks(initExpansion);
		System.out.println("Initial task network after elimination: " + initExpansion.size() + " tasks");
		
		return reducedTasks;
	}
	
	/**
	 * Returns a filtered list of facts.
	 * Side effects: the methodsOfTasks and actionsOfTasks lookup
	 * structures are created and all occurring facts within the
	 * new problem data are updated according to their new indices.
	 * The factInertia field is created and set to a list where
	 * the element at i corresponds to the inertia of the fact
	 * with index i in getCompressedFacts().
	 * 
	 * @param originalFacts the original list of facts
	 * @param reachableElemIndices the indices of elements which
	 * are reachable, i.e. they should <b>not</b> be removed
	 * @return a reduced list of facts according to the given indices
	 */
	private List<IntExp> reduceFactList(List<IntExp> originalFacts, 
			BitVector reachableElemIndices) {
		
		System.out.println("  " + reachableElemIndices.cardinality() 
		+ "/" + originalFacts.size() + " facts are reachable.");
		
		List<IntExp> reducedFacts = new ArrayList<>();
		List<Inertia> reducedFactInertia = new ArrayList<>();
		List<Integer> newIndices = new ArrayList<>();
		
		for (int i = 0; i+1 < originalFacts.size(); i += 2) {
			if (reachableElemIndices.get(i) 
					|| reachableElemIndices.get(i+1)) {
				reducedFacts.add(originalFacts.get(i));
				newIndices.add(reducedFacts.size()-1);
				reducedFacts.add(originalFacts.get(i+1));
				newIndices.add(reducedFacts.size()-1);
				reducedFactInertia.add(
						p.getInertia().get(originalFacts.get(i).getPredicate()));
			} else {
				newIndices.add(-1);
				newIndices.add(-1);
			}
		}
		
		// Set the inertia of compressed facts
		data.setFactInertia(reducedFactInertia);
		
		// Update indices in all preconditions, effects 
		// TODO BOTTLENECK
		data.setActionsOfTasks(new HashMap<>());
		data.setMethodsOfTasks(new HashMap<>());
		for (int task = 0; task < data.getTasks().size(); task++) {
			
			boolean primitive = data.isTaskPrimitive(task);			
			Signature sig = getNewTaskSignature(task); 
			
			if (primitive) {
				BitOp operator = p.getOperators().get(sig).get(0);
				HtnAction action = new HtnAction(operator);
				for (int p : operator.getPreconditions()
						.getPositive().getAllSetBits()) {
					action.addPreconditionPos(newIndices.get(p));
				}
				for (int p : operator.getPreconditions()
						.getNegative().getAllSetBits()) {
					//Assert.that(!operator.getPreconditions().getPositive().get(p), "Precondition contained positively AND negatively!");
					action.addPreconditionNeg(newIndices.get(p));
				}
				for (int p : operator.getUnconditionalEffects()
						.getPositive().getAllSetBits()) {
					action.addEffectPos(newIndices.get(p));
				}
				for (int p : operator.getUnconditionalEffects()
						.getNegative().getAllSetBits()) {
					action.addEffectNeg(newIndices.get(p));
				}
				data.getActionsOfTasks().put(task, action);
			} else {
				List<OrderedMethod> methods = p.getMethods().get(sig);
				List<HtnMethod> htnMethods = new ArrayList<>();
				for (OrderedMethod method : methods) {
					HtnMethod htnMethod = new HtnMethod(method, 
							p -> newIndices.get(p));
					htnMethods.add(htnMethod);
				}
				data.getMethodsOfTasks().put(task, htnMethods);
			}
		}
		
		// Update constraints of initial task network
		data.setGoalFacts(new ArrayList<>());
		for (OneTaskConstraint c : p.getInitExpansion().getAfterConstraints()) {
			data.getGoalFacts().add(newIndices.get(c.getGroundPredicate()));
		}
		
		// Update initial facts
		data.setInitFacts(new ArrayList<>());
		for (int p : p.getInit().getPositive().getAllSetBits()) {
			data.getInitFacts().add(newIndices.get(p));
		}
		for (int p : p.getInit().getNegative().getAllSetBits()) {
			data.getInitFacts().add(newIndices.get(p));
		}
		
		return reducedFacts;
	}
	
	/**
	 * Computes a flat list of methods that are present within the problem
	 * and stores it in the new problem data.
	 */
	private void computeFlatMethods() {
		
		Set<HtnMethod> methodSet = new HashSet<>();
		
		// Add all methods from all tasks
		for (int task : data.getNonprimitiveTasks()) {
			List<HtnMethod> methods = data.getMethodsOfTasks().get(task);
			methodSet.addAll(methods);
		}
		
		// Create virtual initial method containing the initial task network
		OrderedMethod initMethod = new OrderedMethod("init-method", 0);
		initMethod.setExpansion(data.getInitTasks());
		List<OneTaskConstraint> c = p.getInitExpansion().getAfterConstraints();
		List<Integer> afterConstraints = new ArrayList<>();
		for (OneTaskConstraint constr : c) {
			afterConstraints.add(constr.getGroundPredicate());
		}

		// Store flat method list
		data.setFlatMethods(new ArrayList<>());
		// Put the initial method at index 0
		data.getFlatMethods().add(new HtnMethod(initMethod, afterConstraints));
		data.getFlatMethods().addAll(methodSet);
		
		// Set IDs
		for (int i = 0; i < data.getFlatMethods().size(); i++) {
			data.getFlatMethods().get(i).setId(i);
		}
	}
	
	/**
	 * Creates a list of compressed facts only containing
	 * "sign-less" facts without their negated counterparts,
	 * and stores the list in the new problem data.
	 */
	private void compressFacts() {
		
		data.setCompressedFacts(new ArrayList<>());
		for (int i = 0; i < data.getFacts().size(); i += 2) {
			IntExp expPos = data.getFacts().get(i);
			/*IntExp expNeg = data.getFacts().get(i+1);
			String posString = intExpToString(expPos);
			String negString = intExpToString(expNeg);
			String strippedNegString = 
					negString.substring(5, negString.length()-1);
			Assert.that(
					strippedNegString.equals(posString),
					"The facts are not properly alternating "
					+ "between a positive fact and its negative "
					+ "counterpart."
					+ "\n" + posString + " is not complementing " 
					+ negString + ".");
			*/
			data.getCompressedFacts().add(expPos);
		}
		Assert.that(data.getCompressedFacts().size() * 2 == data.getFacts().size(),
				"There is not the right amount of facts in the compression.");
	}
	
	private void computeFlatMethodIndices() {
		
		data.setFlatIndicesOfMethods(new HashMap<>());
		for (int method = 0; method < data.getFlatMethods().size(); method++) {
			data.setFlatIndexOfMethod(data.getFlatMethods().get(method), method);
		}
	}
	
	private void buildHierarchyMatrix() {
		
		System.out.println("init");
		List<List<Integer>> successors = new ArrayList<>();
		List<List<Integer>> predecessors = new ArrayList<>();
		for (int i = 0; i < data.getFlatMethods().size(); i++) {
			successors.add(new ArrayList<>());
			predecessors.add(new ArrayList<>());
		}
		System.out.println("main");
		System.out.println("  " + data.getFlatMethods().size());
		for (int methodId = 0; methodId < data.getFlatMethods().size(); methodId++) {
			if (methodId % 1000 == 0)
				System.out.println("    " + methodId);
			HtnMethod method = data.getFlatMethods().get(methodId);
			for (int task : method.getExpansion()) {
				if (data.isTaskPrimitive(task))
					continue;
				for (HtnMethod subMethod : data.getMethodsOfTask(task)) {
					int subMethodId = subMethod.getId();
					successors.get(methodId).add(subMethodId);
					predecessors.get(subMethodId).add(methodId);
				}
			}
		}
		System.out.println("end");
	}
	
	private void computeEffectorsOfFacts() {
		
		System.out.println("Begin computing effectors");
		
		/*
		List<BitVector> posPrimitiveEffectors = p.getPositivePrimitiveEffectors();
		List<BitVector> negPrimitiveEffectors = p.getNegativePrimitiveEffectors();
		List<BitVector> posCompositeEffectors = p.getPositiveComposedEffectors();
		List<BitVector> negCompositeEffectors = p.getNegativeComposedEffectors();
		
		data.setPosPrimitiveEffectorsOfFacts(extractEffectors(posPrimitiveEffectors));
		data.setNegPrimitiveEffectorsOfFacts(extractEffectors(negPrimitiveEffectors));
		data.setPosCompositeEffectorsOfFacts(extractEffectors(posCompositeEffectors));
		data.setNegCompositeEffectorsOfFacts(extractEffectors(negCompositeEffectors));
		*/

		// Initialize data structures
		List<BitVector> factsChangedByActions = new ArrayList<>();
		for (int i = 0; i < data.getPrimitiveTasks().size(); i++) {
			factsChangedByActions.add(new BitVector(data.getFacts().size()));
		}
		
		List<BitVector> factsChangedByMethods = new ArrayList<>();
		for (int i = 0; i < data.getFlatMethods().size(); i++) {
			factsChangedByMethods.add(new BitVector(data.getFacts().size()));
		}
		
		// Loop over methods until there are no new updates
		boolean changes = true;
		while (changes) {
			
			System.out.println("New loop");
			
			changes = false;
			
			for (int methodIdx = 0; methodIdx < data.getFlatMethods().size(); methodIdx++) {
				
				HtnMethod method = data.getFlatMethods().get(methodIdx);				
				int oldCardinality = factsChangedByMethods.get(methodIdx).cardinality();
				
				for (int taskIdx : method.getExpansion()) {
					
					if (data.isTaskPrimitive(taskIdx)) {
						
						// Both the primitive action and it's parent method
						// may change the facts which are effects of that action
						HtnAction action = data.getActionOfTask(taskIdx);
						for (int effect : action.getEffectsPos()) {
							
							factsChangedByActions.get(data.actionTaskIdxToFlatIdx(taskIdx)).set(effect);
							factsChangedByMethods.get(methodIdx).set(effect);
						}
						for (int effect : action.getEffectsNeg()) {
							
							factsChangedByActions.get(data.actionTaskIdxToFlatIdx(taskIdx)).set(effect);
							factsChangedByMethods.get(methodIdx).set(effect);
						}
						
					} else {
						
						for (HtnMethod subMethod : data.getMethodsOfTask(taskIdx)) {
							
							int subMethodIdx = subMethod.getId();
							
							// Each fact changed by a sub-method may also be changed by this method
							factsChangedByMethods.get(methodIdx)
									.or(factsChangedByMethods.get(subMethodIdx));
						}
					}
				}
				
				changes |= (oldCardinality != factsChangedByMethods.get(methodIdx).cardinality());
			}			
		}
		
		data.setFactsChangedByActions(factsChangedByActions);
		data.setFactsChangedByMethods(factsChangedByMethods);
		
		System.out.println("Computed effectors");
	}
	
	private void simplifyTrivialMethods() {
		
		Map<HtnAction, List<Integer>> replacedMethods = new HashMap<>();
		
		// For each method
		for (HtnMethod method : data.getFlatMethods()) {
			
			// Find out if all of the method's tasks are primitive
			boolean allTasksPrimitive = method.getExpansion().size() == 1;
			if (allTasksPrimitive) {
				for (int task : method.getExpansion()) {
					allTasksPrimitive &= data.isTaskPrimitive(task);
				}
			}
			
			if (allTasksPrimitive) {
				
				String name = "";
				int arity = 0;
				Set<Integer> preconditionsPos = new HashSet<>();
				Set<Integer> preconditionsNeg = new HashSet<>();
				Set<Integer> effectsPos = new HashSet<>();
				Set<Integer> effectsNeg = new HashSet<>();
				
				// Add the method's precondition(s)
				for (int p : method.getBeforeConstraints()) {
					if (data.isFactPositive(p))
						preconditionsPos.add(p);
					else
						preconditionsNeg.add(data.flipSignOfFact(p));
				}
				
				// Add the preconds and effects from the actions
				for (int task : method.getExpansion()) {
					HtnAction action = data.getActionOfTask(task);
					
					// Set name
					StringBuilder str = new StringBuilder();
					str.append("(");
					str.append(action.getOperator().getName());
					int[] args = action.getOperator().getInstantiations();
					for (int i = 0; i < args.length; i++) {
						int index = args[i];
						if (index < 0) {
							str.append(" " + Symbol.DEFAULT_VARIABLE_SYMBOL + (-index - 1));
						} else {
							str.append(" " + p.getConstants().get(index));
						}
					}
					str.append(") ");
					name = str.toString();
					
					// New positive precondition
					for (int p : action.getPreconditionsPos()) {
						// Is the precondition unfulfilled yet?
						if (!effectsPos.contains(p)) {
							preconditionsPos.add(p);
						}
					}
					
					// New negative precondition
					for (int p : action.getPreconditionsNeg()) {
						// Is the precondition unfulfilled yet?
						if (!effectsNeg.contains(p)) {
							preconditionsNeg.add(p);
						}
					}
					
					// New positive effect
					for (int p : action.getEffectsPos()) {
						effectsPos.add(p);
						// Is a previous effect overridden?
						if (effectsNeg.contains(p))
							effectsNeg.remove(p);
					}
					
					// New negative effect
					for (int p : action.getEffectsNeg()) {
						effectsNeg.add(p);
						// Is a previous effect overridden?
						if (effectsPos.contains(p))
							effectsPos.remove(p);
					}
				}
				name = name.substring(0, name.length()-1);
				
				BitOp macroOperator = new BitOp(name, arity);
				HtnAction macroAction = new HtnAction(macroOperator);
				for (int p : preconditionsPos)
					macroAction.addPreconditionPos(p);
				for (int p : preconditionsNeg)
					macroAction.addPreconditionNeg(p);
				for (int p : effectsPos)
					macroAction.addEffectPos(p);
				for (int p : effectsNeg)
					macroAction.addEffectNeg(p);
				
				if (replacedMethods.get(macroAction) == null)
					replacedMethods.put(macroAction, new ArrayList<>());
				replacedMethods.get(macroAction).add(data.getFlatIndexOfMethod(method));
			}
		}
		
		System.out.println(replacedMethods.size());
		
		// Replace all occurrences of the trivial methods 
		// with their replacing macro actions
		for (HtnAction macroAction : replacedMethods.keySet()) {
			
			// Add the action to the other actions
			int macroActionTaskIdx = data.getTasks().size();
			IntExp macroActionTask = new IntExp(Connective.TASK);
			data.getTasks().add(macroActionTask);
			data.getPrimitiveTasks().add(macroActionTaskIdx);
			data.addPrimitiveIndexOfTask(data.getPrimitiveTasks().size()-1); // TODO correct?
			data.getActionsOfTasks().put(macroActionTaskIdx, macroAction);
			
			// Update all replaced method expansions
			List<Integer> oldMethodIndices = replacedMethods.get(macroAction);
			for (int methodIdx : oldMethodIndices) {
				HtnMethod method = data.getFlatMethods().get(methodIdx);
				method.replaceExpansion(macroActionTaskIdx);
				data.setFlatIndexOfMethod(method, methodIdx);
			}
		}
	}
	
	/**
	 * @param task
	 * @return true, if the task's signature <b>within the original coded problem</b>
	 * does not correspond to any methods;<br/>
	 * false, if it does not correspond to any operators instead;<br/>
	 * IllegalArgumentException, else.
	 */
	private boolean isTaskPrimitive(int task) {
		
		Signature sig = getOldTaskSignature(task);
		
		if (p.getMethods().get(sig) == null) {
			
			/*Assert.that(p.getOperators().get(sig) != null, 
					"Neither a method nor an operator is defined on task #" 
					+ task + "'s signature.\nTask signature: " + sig.getName() 
					+ "(" + Arrays.toString(sig.getParameters()) + ")"); 
			*/
			return true;
		}
		
		/*Assert.that(p.getOperators().get(sig) == null, 
				"The given task is ambigous; there are both methods"
				+ " and operators defined on its signature."); 
		*/
		return false;
	}
	
	/**
	 * Constructs a Signature object which corresponds to the signature
	 * of the task at the given index <b>within the original coded problem</b>.
	 */
	private Signature getOldTaskSignature(int task) {
		
		IntExp taskExp = p.getRelevantTasks().get(task);
		Assert.that(taskExp.getConnective() == Connective.TASK, 
				"The retrieved IntExp is not a task.");
		// TODO Check if this is really generally correct
		String name = p.getTaskSymbols().get(taskExp.getPredicate()); 
		Signature sig = new Signature(name, taskExp.getArguments());
		return sig;
	}
	
	/**
	 * Constructs a Signature object which corresponds to the signature
	 * of the task at the given index <b>within the new problem data</b>.
	 */
	private Signature getNewTaskSignature(int task) {
		
		IntExp taskExp = data.getTasks().get(task);
		Assert.that(taskExp.getConnective() == Connective.TASK, 
				"The retrieved IntExp is not a task.");
		// TODO Check if this is really generally correct
		String name = p.getTaskSymbols().get(taskExp.getPredicate()); 
		Signature sig = new Signature(name, taskExp.getArguments());
		return sig;
	}
	
	/**
	 * Returns a list of ground OrderedMethods who fit the signature
	 * of the task at the specified index <b>within the original coded problem</b>.
	 * @param taskIndex an index e.g. given by an element of 
	 * problem.getInitTaskNetwork().getTasks()
	 */
	private List<OrderedMethod> getReductionsForTask(int task) {
		
		Signature sig = getOldTaskSignature(task);
		Assert.that(!isTaskPrimitive(task), "Task #" + task 
				+ " is primitive; it has no reductions.");
		List<OrderedMethod> reductions = p.getMethods().get(sig);
		return reductions;
	}
	
	/**
	 * Returns the operator belonging to the specified primitive task index
	 * <b>within the original coded problem</b>.
	 */
	private BitOp getOperator(int task) {
		List<BitOp> ops = p.getOperators().get(getOldTaskSignature(task));
		if (ops == null)
			return null;
		return ops.get(0);
	}
	
	/**
	 * Helper method constructing a readable string out of an IntExp object.
	 */
	private String intExpToString(IntExp exp) {
		return exp.toString(p.getConstants(), p.getTypes(), 
				p.getPredicates(), p.getFunctions(), p.getTaskSymbols());
	}
	
	private long time;
	private void startTimer() {
		time = System.nanoTime();
	}
	private float elapsedRoundTimeMillis() {
		long oldTime = time;
		time = System.nanoTime();
		return (time - oldTime) / 1000000.0f;
	}
	
	public static int[] calculateMaxTaskLengths(HtnProblemData data, int maxSteps) {
		
		int numTasks = data.getTasks().size();
		int[] taskLengths = new int[numTasks];
		for (int i = 0; i < taskLengths.length; i++) {
			taskLengths[i] = -1;
		}
		
		boolean change = true;
		while (change) {
			change = false;
			
			for (int task = 0; task < numTasks; task++) {
				
				if (taskLengths[task] == -1 && data.isTaskPrimitive(task)) {
					
					taskLengths[task] = 1;
					change = true;
					
				} else if (!data.isTaskPrimitive(task)) {
					
					if (taskLengths[task] == maxSteps) {
						continue;
					}
					
					int maxExpansionLength = 0;
					List<HtnMethod> methods = data.getMethodsOfTask(task);
					for (HtnMethod method : methods) {
						int expansionLength = 0;
						List<Integer> expansion = method.getExpansion();
						for (int subtask : expansion) {
							if (taskLengths[subtask] != -1) {
								expansionLength += taskLengths[subtask];
							}
						}
						maxExpansionLength = Math.max(maxExpansionLength, expansionLength);
					}
					if (maxExpansionLength > 0 && maxExpansionLength > taskLengths[task]) {
						taskLengths[task] = Math.min(maxSteps, maxExpansionLength);
						change = true;
					}
				}
			}
		}
		
		return taskLengths;
	}
}
