package htnsat.htn;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import htnsat.Logger;
import pddl4j.preprocessing.CodedProblem;
import pddl4j.preprocessing.Inertia;
import pddl4j.util.BitVector;
import pddl4j.util.IntExp;

public class HtnProblemData {

	private CodedProblem codedProblem;
	
	private List<IntExp> tasks;
	private List<Integer> primitiveTasks;
	private List<Integer> nonprimitiveTasks;
	private boolean[] isTaskPrimitive;
	
	private List<IntExp> facts;
	private List<IntExp> compressedFacts;
	private List<Inertia> factInertia;
	
	private Map<Integer, HtnAction> actionsOfTasks;
	private Map<Integer, List<HtnMethod>> methodsOfTasks;
	
	private List<HtnMethod> flatMethods;

	private List<Integer> initTasks;
	private List<Integer> initFacts;
	private List<Integer> goalFacts;

	private List<Integer> primitiveIndicesOfTasks;
	private List<Integer> nonprimitiveIndicesOfTasks;
	private Map<HtnMethod, Integer> flatIndicesOfMethods;
	
	private int maxReductionsPerTask = 0;
	private int maxExpansionSize = 0;

	private List<BitVector> factsChangedByActions;
	private List<BitVector> factsChangedByMethods;
	
	public HtnProblemData(CodedProblem codedProblem) {
		this.codedProblem = codedProblem;
	}
	
	public void printInfo(Logger log) {
		
		log.print("\nCompressed facts (" + compressedFacts.size() 
			+ "):");
		for (int i = 0; i < compressedFacts.size(); i++) {
			IntExp fact = compressedFacts.get(i);
			log.print("  " + i + " " + intExpToString(fact) 
				+ " (" + factInertia.get(i) + ")");
		}
		
		log.print("\nTasks (" + tasks.size() + "):");
		for (int taskIdx = 0; taskIdx < tasks.size(); taskIdx++) {
			log.print(taskIdx + " " + intExpToString(tasks.get(taskIdx)));
			if (isTaskPrimitive[taskIdx]) {
				log.print("    Action: " + actionsOfTasks.get(taskIdx).toString(this));
			} else {
				log.print("    Methods: ");
				for (HtnMethod method : methodsOfTasks.get(taskIdx)) {
					log.print("      " + flatIdxOfMethod(method) + " " + method.toString(this));
				}
			}
		}

		log.print("\nInitial facts (" + initFacts.size() + "):");
		for (int fact : initFacts) {
			log.print("  " + intExpToString(facts.get(fact)));
		}
		log.print("\nGoal facts (" + goalFacts.size() + "):");
		for (int fact : goalFacts) {
			log.print("  " + intExpToString(facts.get(fact)));
		}
		
		log.print("\nInitial tasks (" + initTasks.size() + "):");
		for (int task : initTasks) {
			log.print("  " + intExpToString(tasks.get(task)));
		}
		
		log.print("");
	}
	
	// TO STRING METHODS //

	/**
	 * Helper method constructing a readable string out of an IntExp object.
	 */
	public String intExpToString(IntExp exp) {
		return exp.toString(codedProblem.getConstants(), codedProblem.getTypes(), 
				codedProblem.getPredicates(), codedProblem.getFunctions(), 
				codedProblem.getTaskSymbols());
	}
	/**
	 * Helper method constructing a readable string out of a predicate index.
	 */
	public String predicateToString(int pred) {
		return intExpToString(facts.get(pred));
	}
	
	
	
	public boolean isTaskPrimitive(int task) {
		return task >= isTaskPrimitive.length || isTaskPrimitive[task];
	}
	
	public boolean isMethodTrivial(HtnMethod m) {
		
		List<Integer> expansion = m.getExpansion();
		if (expansion.size() == 1) {
			int task = expansion.get(0);
			return isTaskPrimitive(task);
		}
		return false;
	}
	
	public HtnAction getActionOfTask(int task) {
		return actionsOfTasks.get(task);
	}
	
	public List<HtnMethod> getMethodsOfTask(int task) {
		return methodsOfTasks.get(task);
	}
		
	public int getNumMethodsOfTask(int task) {
		return getMethodsOfTask(task).size();
	}
	
	public int actionFlatIdxToTaskIdx(int flatIdx) {
		return primitiveTasks.get(flatIdx);
	}
	
	public int actionTaskIdxToFlatIdx(int taskIdx) {
		return primitiveIndicesOfTasks.get(taskIdx);
	}
	
	public int flatIdxOfMethod(HtnMethod m) {
		return flatIndicesOfMethods.get(m);
	}

	public int compressFact(int uncompressedFact) {
		return uncompressedFact / 2;
	}
	public int uncompressFact(int compressedFact) {
		return compressedFact * 2;
	}
	public boolean isFactPositive(int uncompressedFact) {
		return uncompressedFact % 2 == 0;
	}
	public int flipSignOfFact(int uncompressedFact) {
		if (isFactPositive(uncompressedFact)) {
			return uncompressedFact+1;
		} else {
			return uncompressedFact-1;
		}
	}
	
	// TODO Inefficient. Implement effects/preconditions 
	// of actions/methods as sorted sets?
	public List<Integer> getActionTasksWithEffect(
			int uncompressedFact, boolean pos) {
		
		List<Integer> actionTasks = new ArrayList<>();
		for (int task : primitiveTasks) {
			if ((pos && getActionOfTask(task).getEffectsPos()
					.contains(uncompressedFact))
				|| (!pos && getActionOfTask(task).getEffectsNeg()
						.contains(uncompressedFact))) {
				actionTasks.add(task);
			}
		}
		return actionTasks;
	}
	
	public List<IntExp> getTasks() {
		return tasks;
	}

	public void setTasks(List<IntExp> tasks) {
		this.tasks = tasks;
	}

	public List<Integer> getPrimitiveTasks() {
		return primitiveTasks;
	}

	public void setPrimitiveTasks(List<Integer> primitiveTasks) {
		this.primitiveTasks = primitiveTasks;
	}

	public List<Integer> getNonprimitiveTasks() {
		return nonprimitiveTasks;
	}

	public void setNonprimitiveTasks(List<Integer> nonprimitiveTasks) {
		this.nonprimitiveTasks = nonprimitiveTasks;
	}

	public void setIsTaskPrimitive(boolean[] isTaskPrimitive) {
		this.isTaskPrimitive = isTaskPrimitive;
	}

	public List<IntExp> getFacts() {
		return facts;
	}

	public void setFacts(List<IntExp> facts) {
		this.facts = facts;
	}

	public List<IntExp> getCompressedFacts() {
		return compressedFacts;
	}

	public void setCompressedFacts(List<IntExp> compressedFacts) {
		this.compressedFacts = compressedFacts;
	}

	public Map<Integer, HtnAction> getActionsOfTasks() {
		return actionsOfTasks;
	}

	public void setActionsOfTasks(Map<Integer, HtnAction> actionsOfTasks) {
		this.actionsOfTasks = actionsOfTasks;
	}

	public Map<Integer, List<HtnMethod>> getMethodsOfTasks() {
		return methodsOfTasks;
	}

	public void setMethodsOfTasks(Map<Integer, List<HtnMethod>> methodsOfTasks) {
		this.methodsOfTasks = methodsOfTasks;
	}

	public List<HtnMethod> getFlatMethods() {
		return flatMethods;
	}

	public void setFlatMethods(List<HtnMethod> flatMethods) {
		this.flatMethods = flatMethods;
	}

	public List<Integer> getInitTasks() {
		return initTasks;
	}

	public void setInitTasks(List<Integer> initTasks) {
		this.initTasks = initTasks;
	}

	public List<Integer> getInitFacts() {
		return initFacts;
	}

	public void setInitFacts(List<Integer> initFacts) {
		this.initFacts = initFacts;
	}

	public List<Integer> getGoalFacts() {
		return goalFacts;
	}

	public void setGoalFacts(List<Integer> goalFacts) {
		this.goalFacts = goalFacts;
	}

	public int getMaxReductionsPerTask() {
		return maxReductionsPerTask;
	}

	public void setMaxReductionsPerTask(int maxReductionsPerTask) {
		this.maxReductionsPerTask = maxReductionsPerTask;
	}

	public int getMaxExpansionSize() {
		return maxExpansionSize;
	}

	public void setMaxExpansionSize(int maxExpansionSize) {
		this.maxExpansionSize = maxExpansionSize;
	}

	public List<Inertia> getFactInertia() {
		return factInertia;
	}

	public void setFactInertia(List<Inertia> factInertia) {
		this.factInertia = factInertia;
	}

	public void setFlatIndexOfMethod(HtnMethod method, int flatIndex) {
		this.flatIndicesOfMethods.put(method, flatIndex);
	}
	
	public void addPrimitiveIndexOfTask(int primIdx) {
		this.primitiveIndicesOfTasks.add(primIdx); // TODO correct indexing?
	}
	
	public void addNonprimitiveIndexOfTask(int nonprimIdx) {
		this.nonprimitiveIndicesOfTasks.add(nonprimIdx);
	}

	public void setPrimitiveIndicesOfTasks(List<Integer> primitiveIndicesOfTasks) {
		this.primitiveIndicesOfTasks = primitiveIndicesOfTasks;
	}
	
	public void setNonprimitiveIndicesOfTasks(List<Integer> nonprimitiveIndicesOfTasks) {
		this.nonprimitiveIndicesOfTasks = nonprimitiveIndicesOfTasks;
	}

	public void setFlatIndicesOfMethods(Map<HtnMethod, Integer> flatIndicesOfMethods) {
		this.flatIndicesOfMethods = flatIndicesOfMethods;
	}
	
	public int getPrimitiveIndexOfTask(int task) {
		return primitiveIndicesOfTasks.get(task);
	}
	
	public int getNonprimitiveIndexOfTask(int task) {
		return nonprimitiveIndicesOfTasks.get(task);
	}
	
	public int getFlatIndexOfMethod(HtnMethod method) {
		return flatIndicesOfMethods.get(method);
	}

	public List<BitVector> getFactsChangedByActions() {
		return factsChangedByActions;
	}

	public void setFactsChangedByActions(List<BitVector> factsChangedByActions) {
		this.factsChangedByActions = factsChangedByActions;
	}

	public List<BitVector> getFactsChangedByMethods() {
		return factsChangedByMethods;
	}

	public void setFactsChangedByMethods(List<BitVector> factsChangedByMethods) {
		this.factsChangedByMethods = factsChangedByMethods;
	}
	
	public CodedProblem getCodedProblem() {
		return codedProblem;
	}
}
