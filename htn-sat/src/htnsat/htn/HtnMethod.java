package htnsat.htn;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import htnsat.util.Assert;
import pddl4j.util.BitVector;
import pddl4j.util.OrderedMethod;

public class HtnMethod {

	private OrderedMethod orderedMethod;
	
	private int[] groundParams;
	private List<Integer> expansion;
	
	private List<List<Integer>> constraints;
	
	private int id;
	
	public HtnMethod(OrderedMethod orderedMethod) {
		
		this.orderedMethod = orderedMethod;
		
		groundParams = orderedMethod.getInstantiations();
		expansion = orderedMethod.getExpansion();
		
		initConstraints(orderedMethod, p -> p);
		
		// TODO add each after constraint to constraints[last]
		// TODO add each between(i-1,i) constraint to constraints[i]
	}
	
	public HtnMethod(OrderedMethod orderedMethod, 
			Function<Integer, Integer> factMapping) {
		
		this.orderedMethod = orderedMethod;
		
		groundParams = orderedMethod.getInstantiations();
		expansion = orderedMethod.getExpansion();
		
		initConstraints(orderedMethod, factMapping);
	}
	
	public HtnMethod(OrderedMethod orderedMethod, List<Integer> afterConstraints) {
		
		this.orderedMethod = orderedMethod;
		
		groundParams = orderedMethod.getInstantiations();
		expansion = orderedMethod.getExpansion();
		
		initConstraints(orderedMethod, p -> p);
		
		constraints.get(constraints.size()-1).addAll(afterConstraints);
	}
	
	public void replaceExpansion(int macroActionTaskIdx) {
		
		expansion.clear();
		expansion.add(macroActionTaskIdx);
	}
	
	private void initConstraints(OrderedMethod m, 
			Function<Integer, Integer> factMapping) {
		
		constraints = new ArrayList<>();
		for (int i = 0; i < expansion.size()+1; i++) {
			constraints.add(new ArrayList<>());
		}
		extractPreconditions(m, factMapping);
	}
	
	private void extractPreconditions(OrderedMethod m, 
			Function<Integer, Integer> factMapping) {
		
		BitVector b = m.getPositivePreconditions();
		List<Integer> posPrecond = b.getAllSetBits();
		for (int p : posPrecond) {
			int pMapped = factMapping.apply(p);
			constraints.get(0).add(pMapped);
		}
		b = m.getNegativePreconditions();
		List<Integer> negPrecond = b.getAllSetBits();
		for (int p : negPrecond) {
			//Assert.that(!posPrecond.contains(p), "Precondition contained positively AND negatively!");
			int pMapped = factMapping.apply(p);
			constraints.get(0).add(pMapped+1); // TODO validate / test
		}
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
	
	public List<Integer> getBeforeConstraints() {
		
		return constraints.get(0);
	}
	
	public List<Integer> getAfterConstraints() {
		
		return constraints.get(constraints.size()-1);
	}
	
	public List<Integer> getBetweenConstraints(int secondIdx) {
		
		return constraints.get(secondIdx);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((constraints == null) ? 0 : constraints.hashCode());
		result = prime * result + ((expansion == null) ? 0 : expansion.hashCode());
		result = prime * result + Arrays.hashCode(groundParams);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HtnMethod other = (HtnMethod) obj;
		if (constraints == null) {
			if (other.constraints != null)
				return false;
		} else if (!constraints.equals(other.constraints))
			return false;
		if (expansion == null) {
			if (other.expansion != null)
				return false;
		} else if (!expansion.equals(other.expansion))
			return false;
		if (!Arrays.equals(groundParams, other.groundParams))
			return false;
		return true;
	}

	public List<Integer> getExpansion() {
		return expansion;
	}
	
	public OrderedMethod getOrderedMethod() {
		return orderedMethod;
	}
	
	@Override
	public String toString() {
		return orderedMethod.getName() + Arrays.toString(orderedMethod.getInstantiations()) 
		+ "{" + getExpansion().stream().map(Object::toString)
                .collect(Collectors.joining(",")) + "}";
	}
	
	public String toString(HtnProblemData data) {
		return "{" 
				+ getBeforeConstraints().stream()
					.map(p -> data.predicateToString(p)).collect(Collectors.joining(",")) 
				+ "} "
				+ orderedMethod.getName() + Arrays.toString(orderedMethod.getInstantiations()) 
				+ "(" + getExpansion().stream().map(
						taskIdx -> data.intExpToString(data.getTasks().get(taskIdx))
				).collect(Collectors.joining(",")) + ")";
	}
	
}
