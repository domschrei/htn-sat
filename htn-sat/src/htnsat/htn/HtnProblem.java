package htnsat.htn;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import htnsat.Logger;
import htnsat.util.Assert;
import pddl4j.parser.Connective;
import pddl4j.preprocessing.CodedProblem;
import pddl4j.util.BitExp;
import pddl4j.util.BitOp;
import pddl4j.util.IntExp;
import pddl4j.util.OneTaskConstraint;
import pddl4j.util.OrderedMethod;
import pddl4j.util.Signature;
import pddl4j.util.TaskNetwork;

/**
 * A representation of the encoding of an HTN problem. 
 * 
 * <ul>
 * <li><b>Operators</b>: set of <b>primitive</b> actions, 
 * categorized by their signatures
 * and including their preconditions and effects.</li>
 * <li><b>Methods</b>: set of <b>non-primitive</b> task 
 * reductions, categorized by their signatures and including
 * preconditions and the corresponding expansion (into tasks) 
 * that is being made.</li>
 * <li><b>Tasks</b>: "flat" list of fully-instantiated 
 * signatures of <b>both primitive and non-primitive</b> 
 * actions.</li>
 * </ul>
 * 
 * A <i>method</i>'s expansion links to a list of <i>task</i>s,
 * whose signature can be identified either to be a 
 * <i>primitive action</i> or to be non-primitive and thus 
 * correspond to another set of methods whose signatures 
 * comply with the task's.
 * 
 * @author Dominik Schreiber
 */
public class HtnProblem {

	/**
	 * The CodedProblem instance this object is based on
	 */
	private CodedProblem codedProblem;

	/**
	 * The table of the relevant facts.
	 * All predicates that occur somewhere in the plan.
	 */
	private List<IntExp> relevantFacts;
	
	/**
	 * "flat" list of fully-instantiated signatures of 
	 * <b>both primitive and non-primitive</b> actions.
	 */
	private List<IntExp> relevantTasks;
	
	/**
	 * Contains the first expansion, i.e. a sequence of tasks,
	 * and the network's constraints that have to be fulfilled 
	 * (including the goal conditions).
	 */
	private TaskNetwork initTaskNetwork;
	
	/**
	 * Set of <b>primitive</b> actions, categorized by their signatures
	 * and including their preconditions and effects.
	 */
	private Map<Signature, List<BitOp>> operators;
	
	/**
	 * Set of <b>non-primitive</b> task reductions, categorized 
	 * by their signatures and including preconditions and the 
	 * corresponding expansion (into tasks) that is being made.
	 */
	private Map<Signature, List<OrderedMethod>> methods;

	/**
	 * Array of predicate indices that hold in the initial state.
	 */
	private List<Integer> initPredicatesPos, initPredicatesNeg;

	/**
	 * Array of predicate indices that must hold in the final state.
	 */
	private List<Integer> goalPredicatesPos, goalPredicatesNeg;

	/**
	 * A list of indices for relevantTasks, pointing to tasks
	 * which can be potentially reached from the initial task network.
	 */
	private List<Integer> reachableTasks;
	
	/**
	 * A list of indices for relevantTasks, pointing to tasks
	 * which can be identified as being primitive.
	 */
	private List<Integer> primitiveTasks;
	
	/**
	 * The intersection of reachableTasks and primitiveTasks.
	 */
	private List<Integer> reachablePrimitiveTasks;
	
	/**
	 * The complement of reachablePrimitiveTasks (given the ground
	 * set of reachableTasks).
	 */
	private List<Integer> reachableNonprimitiveTasks;
	
	/**
	 * For each of the tasks in the initial network,
	 * contains a set of indices for relevantTasks which
	 * point to the tasks which can be potentially reached
	 * from this task.
	 */
	private List<List<Integer>> reachableTasksPerInitTask;
	
	/**
	 * The subset of primitive tasks of reachableTasksPerInitTask.
	 */
	private List<List<Integer>> reachablePrimitiveTasksPerInitTask;

	/**
	 * A list of HtnMethod objects created from the `methods` 
	 * data structure, each containing their original 
	 * OrderedMethod object as well as some convenience 
	 * methods for retrieving constraints.
	 */
	private List<HtnMethod> flatMethods;
	
	private Map<HtnMethod, Integer> flatMethodIndices;
	
	/**
	 * A subset of relevantFacts, containing all facts
	 * only in a positive (or sign-less) representation.
	 */
	private List<IntExp> compressedFacts;

	/**
	 * The maximal amount of reductions that any given task may have.
	 */
	private int maxReductionsPerTask = 0;
	
	/**
	 * The maximal size of any given method's expansion, 
	 * <b>excluding the initial task network</b> whose list 
	 * of tasks may be larger.
	 */
	private int maxExpansionSize;
	
	/**
	 * For any given index of relevantTasks, contains
	 * the maximal number of primitive steps that may be needed
	 * to perform this task, or some upper threshold.
	 */
	private int[] maxPrimitiveStepsOfTask;
	
	/**
	 * The boolean at index i describes whether
	 * getRelevantTasks.get(i) is primitive or not.
	 */
	private boolean[] isTaskPrimitive;
	
	/**
	 * Creates an HTN problem from a CodedProblem instance.
	 * @param problem
	 */
	public HtnProblem(CodedProblem problem) {
		
		this.codedProblem = problem;
		this.initTaskNetwork = problem.getInitExpansion();
		this.methods = problem.getMethods();
		this.operators = problem.getOperators();
		this.relevantFacts = problem.getRelevantFacts();
		this.relevantTasks = problem.getRelevantTasks();
		
		// Extract initial state predicates
		BitExp init = problem.getInit();
		initPredicatesPos = init.getPositive().getAllSetBits();
		initPredicatesNeg = init.getNegative().getAllSetBits();
		
		// Extract goal predicates
		List<OneTaskConstraint> goals = initTaskNetwork.getAfterConstraints();
		goalPredicatesPos = new ArrayList<>();
		for (OneTaskConstraint c : goals) {
			goalPredicatesPos.add(c.getGroundPredicate());
		}
		goalPredicatesNeg = new ArrayList<>(); // TODO negative goal conditions?
		for (OneTaskConstraint c : goals) {
			goalPredicatesNeg.add(c.getGroundPredicate());
		}
		/*
		traverseTasks();
		compressFacts();
		computeFlatMethods();*/
	}
	
	/** 
	 * Builds the reachable* structures
	 * as well as the primitiveTasks structure.
	 * <br/><br/>
	 * Asserts that each reachable task is primitive (-> operators)
	 * XOR non-primitive (-> methods).
	 * <br/>Reachable means that the task occurs in one of the initial 
	 * network's tasks or any of its
	 * expansions (recursively).
	 */
	private void traverseTasks() {
		
		// Traverse the space of tasks beginning with
		// the set of initial tasks
		Integer[] initTasks = new Integer[initTaskNetwork.getTasks().size()];
		for (int i = 0; i < initTasks.length; i++) {
			initTasks[i] = initTaskNetwork.getTasks().get(i);
		}
		reachableTasks = calculateReachableTasksFor(initTasks);
		
		// Split the reachable tasks into primitive 
		// and non-primitive ones
		reachablePrimitiveTasks = new ArrayList<>();
		reachableNonprimitiveTasks = new ArrayList<>();
		for (int task : reachableTasks) {
			if (isTaskPrimitive(task)) {
				reachablePrimitiveTasks.add(task);
			} else {
				reachableNonprimitiveTasks.add(task);
			}
		}
		
		// Another additional list with _all_ primitive tasks
		// (whether reachable or not)
		primitiveTasks = new ArrayList<>();
		for (int task = 0; task < relevantTasks.size(); task++) {
			if (isTaskPrimitiveSafe(task)) {
				primitiveTasks.add(task);
			}
		}
		
		// Also traverse the space of tasks for every single
		// initial task on its own
		reachableTasksPerInitTask = new ArrayList<>();
		reachablePrimitiveTasksPerInitTask = new ArrayList<>();
		for (int initTask : initTaskNetwork.getTasks()) {
			
			reachableTasksPerInitTask.add(
					calculateReachableTasksFor(initTask));
			
			// Calculate subset of primitive tasks
			List<Integer> primitiveTasks = new ArrayList<>();
			for (int task : reachableTasksPerInitTask.get(
					reachableTasksPerInitTask.size()-1)) {
				if (isTaskPrimitive(task)) {
					primitiveTasks.add(task);
				}
			}
			reachablePrimitiveTasksPerInitTask.add(primitiveTasks);
		}
	}
	
	/**
	 * Traverses the graph of tasks connected by their
	 * possible reductions.
	 * @param initTasks the set of tasks from which
	 * the traversal should begin
	 * @return a list of tasks which have been found
	 */
	private List<Integer> calculateReachableTasksFor(Integer... initTasks) {
		
		boolean[] isTaskPrimitive = new boolean[getRelevantTasks().size()];
		
		Stack<Integer> unprocessedTasks = new Stack<>();
		Set<Integer> processedTasks = new HashSet<>();
		for (int t : initTasks) {			
			unprocessedTasks.push(t);
		}
		
		while (!unprocessedTasks.isEmpty()) {
			int task = unprocessedTasks.pop();
			processedTasks.add(task);
			// The following condition checks if the task is properly defined
			if (!isTaskPrimitive(task)) {
				isTaskPrimitive[task] = false;
				// Reduce non-primitive task
				List<OrderedMethod> methods = getReductionsForTask(task);
				maxReductionsPerTask = Math.max(maxReductionsPerTask, 
						methods.size());
				for (OrderedMethod method : methods) {
					maxExpansionSize = Math.max(maxExpansionSize, 
							method.getExpansion().size());
					for (int newTask : method.getExpansion()) {
						if (!processedTasks.contains(newTask)) {
							unprocessedTasks.push(newTask);
						}
					}
				}
			} else {
				isTaskPrimitive[task] = true;
			}
		}
		List<Integer> reachableTasks = new ArrayList<>();
		for (int i : processedTasks) {
			reachableTasks.add(i);
		}
		Collections.sort(reachableTasks);
		
		if (this.isTaskPrimitive == null) {
			this.isTaskPrimitive = isTaskPrimitive;
		}
		
		return reachableTasks;
	}
	
	/**
	 * Some various debugging information about the present HtnProblem object.
	 */
	public void printInfo(Logger log) {
		
		log.print(methods.size() + " methods");
		log.print(flatMethods.size() + " flat methods");
		log.print(operators.size() + " operators");
		log.print(relevantTasks.size() + " relevant tasks, " 
				+ reachableTasks.size() + " reachable");
		log.print(relevantFacts.size() + " relevant facts"); 
		log.print("Task network:\n" + initTaskNetwork.toString(codedProblem));
		log.print("Maximal expansion size: " + getMaxExpansionSize());
	}
	
	private void compressFacts() {
		
		compressedFacts = new ArrayList<>();
		for (int i = 0; i < getRelevantFacts().size(); i += 2) {
			IntExp expPos = getRelevantFacts().get(i);
			IntExp expNeg = getRelevantFacts().get(i+1);
			String posString = intExpToString(expPos);
			String negString = intExpToString(expNeg);
			String strippedNegString = 
					negString.substring(5, negString.length()-1);
			Assert.that(
					strippedNegString.equals(posString),
					"The facts are not properly alternating "
					+ "between a positive fact and its negative "
					+ "counterpart."
					+ "\n" + posString + " is not complementing " 
					+ negString + ".");
			compressedFacts.add(expPos);
		}
		Assert.that(compressedFacts.size() * 2 == getRelevantFacts().size(),
				"There is not the right amount of facts in the compression.");
	}
	
	public void computeMaxPrimitiveStepsOfTasks(int upperLimit) {
		
		int[] maxStepsOfTasks = new int[getRelevantTasks().size()];
		boolean[] finished = new boolean[getRelevantTasks().size()];
		
		for (int i = 0; i < maxStepsOfTasks.length; i++) {
			maxStepsOfTasks[i] = 0;
			finished[i] = false;
		}
		int progress = 1;
		while (progress > 0) {
			progress = 0;
			for (int task : getReachableTasks()) {
				if (finished[task]) {
					continue;
				} else if (isTaskPrimitive(task)) {			
					progress++;
					maxStepsOfTasks[task] = 1;
					finished[task] = true;
				} else {
					List<OrderedMethod> reductions = getReductionsForTask(task);
					int maxSteps = 0;
					for (OrderedMethod m : reductions) {
						int maxStepsInMethod = 0;
						List<Integer> expansion = m.getExpansion();
						for (int subtask : expansion) {
							maxStepsInMethod += maxStepsOfTasks[subtask];
						}
						maxSteps = Math.max(maxSteps, maxStepsInMethod);
					}
					if (maxStepsOfTasks[task] < maxSteps) {
						int oldMaxSteps = maxStepsOfTasks[task];
						maxStepsOfTasks[task] = Math.min(maxSteps, upperLimit);	
						if (oldMaxSteps < maxStepsOfTasks[task]) {							
							progress++;
						}
					}
				}
			}
		}
		
		this.maxPrimitiveStepsOfTask = maxStepsOfTasks;
	}
	
	public void computeFlatMethods() {
		
		Set<HtnMethod> methodSet = new HashSet<>();
		
		for (int task : getReachableNonprimitiveTasks()) {
			List<OrderedMethod> methods = getReductionsForTask(task);
			for (OrderedMethod method : methods) {
				methodSet.add(new HtnMethod(method));
			}
		}
		
		flatMethods = new ArrayList<>();
		OrderedMethod initMethod = new OrderedMethod("init-method", 0);
		initMethod.setExpansion(getInitTaskNetwork().getTasks());
		List<OneTaskConstraint> c = initTaskNetwork.getAfterConstraints();
		List<Integer> afterConstraints = new ArrayList<>();
		for (OneTaskConstraint constr : c) {
			afterConstraints.add(constr.getGroundPredicate());
		}
		flatMethods.add(new HtnMethod(initMethod, afterConstraints));
		flatMethods.addAll(methodSet);
		
		flatMethodIndices = new HashMap<>();
		for (int i = 0; i < flatMethods.size(); i++) {
			HtnMethod method = flatMethods.get(i);
			flatMethodIndices.put(method, i);
		}
	}

	/**
	 * @param fact the uncompressed index of the fact
	 * @return the compressed index of the fact
	 */
	public int compressFact(int fact) {
		
		return (fact/2);
	}
	/**
	 * @param fact the compressed index of the fact
	 * @return the uncompressed index of the fact
	 */
	public int uncompressFact(int fact) {
		
		return (fact*2);
	}
	/**
	 * @param fact the uncompressed index of the fact
	 * @return true iff the fact is positive
	 */
	public boolean isFactPositive(int fact) {
		
		return fact % 2 == 0;
	}
	
	/**
	 * @param task
	 * @return true, if the task's signature does not correspond to any methods;<br/>
	 * false, if it does not correspond to any operators instead;<br/>
	 * IllegalArgumentException, else.
	 */
	public boolean isTaskPrimitive(int task) {
		
		if (isTaskPrimitive != null)
			return isTaskPrimitive[task];
		
		Signature sig = getTaskSignature(task);
		
		if (getMethods().get(sig) == null) {
			
			Assert.that(operators.get(sig) != null, 
					"Neither a method nor an operator is defined on task #" 
					+ task + "'s signature.\nTask signature: " + sig.getName() 
					+ "(" + Arrays.toString(sig.getParameters()) + ")"); 
			
			return true;
		}
		
		Assert.that(operators.get(sig) == null, 
				"The given task is ambigous; there are both methods"
				+ " and operators defined on its signature."); 
		
		return false;
	}
	
	/**
	 * Same as isTaskPrimitive(task), but will just return
	 * `false` if the task cannot be identified as primitive
	 * or non-primitive.
	 */
	public boolean isTaskPrimitiveSafe(int task) {
		
		try {
			boolean prim = isTaskPrimitive(task);
			return prim;
		} catch (IllegalArgumentException e) {
			return false;
		}
	}
	
	/**
	 * Constructs a Signature object which corresponds to the signature
	 * of the task at the given index.
	 * @param task
	 */
	public Signature getTaskSignature(int task) {
		
		IntExp taskExp = getRelevantTasks().get(task);
		Assert.that(taskExp.getConnective() == Connective.TASK, 
				"The retrieved IntExp is not a task.");
		// TODO Check if this is really generally correct
		String name = codedProblem.getTaskSymbols().get(taskExp.getPredicate()); 
		Signature sig = new Signature(name, taskExp.getArguments());
		return sig;
	}
	
	/**
	 * Returns the indices of all pos. preconditions of the given action.
	 * @param sig
	 * @return
	 */
	public List<Integer> getActionPreconditionsPos(Signature sig) {

		return operators.get(sig).get(0).getPreconditions()
				.getPositive().getAllSetBits();
	}
	
	/**
	 * Returns the indices of all neg. preconditions of the given action.
	 * @param sig
	 * @return
	 */
	public List<Integer> getActionPreconditionsNeg(Signature sig) {

		return operators.get(sig).get(0).getPreconditions()
				.getNegative().getAllSetBits();
	}

	/**
	 * Returns the <b>unconditional</b> pos. effects of the given action.
	 * @param sig
	 * @return
	 */
	public List<Integer> getActionEffectsPos(Signature sig) {

		return operators.get(sig).get(0).getUnconditionalEffects()
				.getPositive().getAllSetBits();
	}

	/**
	 * Returns the <b>unconditional</b> neg. effects of the given action.
	 * @param sig
	 * @return
	 */
	public List<Integer> getActionEffectsNeg(Signature sig) {

		return operators.get(sig).get(0).getUnconditionalEffects()
				.getNegative().getAllSetBits();
	}

	/**
	 * Returns the indices of all pos. preconditions of the given method.
	 * @param sig
	 * @return
	 */
	public List<Integer> getMethodPreconditionsPos(int task, int expIdx) {
		
		Signature sig = getTaskSignature(task);
		return getMethods().get(sig).get(expIdx).getPositivePreconditions().getAllSetBits();
	}
	
	/**
	 * Returns the indices of all neg. preconditions of the given method.
	 * @param sig
	 * @return
	 */
	public List<Integer> getMethodPreconditionsNeg(int task, int expIdx) {
		
		Signature sig = getTaskSignature(task);
		return getMethods().get(sig).get(expIdx).getNegativePreconditions().getAllSetBits();
	}
	
	/**
	 * Returns a list of ground OrderedMethods who fit the signature
	 * of the task at the specified index.
	 * @param taskIndex an index e.g. given by an element of problem.getInitTaskNetwork().getTasks()
	 * @return
	 */
	public List<OrderedMethod> getReductionsForTask(int task) {
		
		Signature sig = getTaskSignature(task);
		Assert.that(!isTaskPrimitive(task), "Task #" + task 
				+ " is primitive; it has no reductions.");
		List<OrderedMethod> reductions = getMethods().get(sig);
		return reductions;
	}
	
	/**
	 * Returns the amount of possible method expansions for the task at the
	 * specified index.
	 * @param taskIndex an index e.g. given by an element of problem.getInitTaskNetwork().getTasks()
	 * @return
	 */
	public int getNumReductionsForTask(int task) {
		
		return getReductionsForTask(task).size();
	}
	
	public OrderedMethod getMethodOfTask(int task, int expIdx) {
		
		List<OrderedMethod> methods = getReductionsForTask(task);
		return methods.get(expIdx);
	}
	
	public List<Integer> getExpansionOfTask(int task, int expIdx) {
		
		return getMethodOfTask(task, expIdx).getExpansion();
	}
	
	/**
	 * 
	 * @param fact the <b>uncompressed</b> fact index
	 * @param positive
	 * @return
	 */
	public List<Integer> getActionsWithEffect(int fact, boolean positive) {
		
		List<Integer> supportingActions = new ArrayList<>();
		for (int a : getReachablePrimitiveTasks()) {
			Signature sig = getTaskSignature(a);
			List<Integer> effects;
			if (positive) {
				effects = getActionEffectsPos(sig);
			} else {
				effects = getActionEffectsNeg(sig);
			}
			// Also takes into account that the desired fact
			// might be negative
			if (effects.contains(fact)) {
				supportingActions.add(a);
			}
		}
		return supportingActions;
	}
	
	/**
	 * @return true, if the given method contains only one
	 * expansion with only a single task which is primitive.
	 */
	public boolean isMethodTrivial(OrderedMethod m) {
		
		List<Integer> expansion = m.getExpansion();
		if (expansion.size() == 1) {
			int task = expansion.get(0);
			return isTaskPrimitive(task);
		}
		return false;
	}
	
	public int indexOfMethod(HtnMethod method) {
		
		return flatMethodIndices.get(method);
		//return flatMethodIndices.get(method);
	}
	
	/**
	 * Before this method works, computeMaxPrimitiveStepsOfTasks(numSteps)
	 * has to be called.
	 * @param task the index of the task
	 * @return the maximum amount of primitive steps that this task
	 * will need to be finished, considering all possible reductions. 
	 * Returns no number higher than numSteps.
	 */
	public int getMaxPrimitiveStepsOfTask(int task) {
		return maxPrimitiveStepsOfTask[task];
	}
	
	public List<Integer> getReachableTasksFor(int initTaskIdx) {
		return reachableTasksPerInitTask.get(initTaskIdx);
	}
	
	public List<Integer> getReachablePrimitiveTasksFor(int initTaskIdx) {
		return reachablePrimitiveTasksPerInitTask.get(initTaskIdx);
	}
	
	
	
	// TRIVIAL GETTERS //
	
	public int getMaxExpansionSize() {
		return maxExpansionSize;
	}
		
	public List<Integer> getReachablePrimitiveTasks() {
		return reachablePrimitiveTasks;
	}
	public List<Integer> getReachableNonprimitiveTasks() {
		return reachableNonprimitiveTasks;
	}

	public List<Integer> getPrimitiveTasks() {
		return primitiveTasks;
	}
	
	public List<IntExp> getRelevantFacts() {
		return relevantFacts;
	}
	
	public Map<Signature, List<BitOp>> getOperators() {
		return operators;
	}

	public TaskNetwork getInitTaskNetwork() {
		return initTaskNetwork;
	}

	public Map<Signature, List<OrderedMethod>> getMethods() {
		return methods;
	}

	public List<IntExp> getRelevantTasks() {
		return relevantTasks;
	}
	
	public List<Integer> getReachableTasks() {
		return reachableTasks;
	}

	public List<Integer> getInitPredicatesPos() {
		return initPredicatesPos;
	}

	public List<Integer> getInitPredicatesNeg() {
		return initPredicatesNeg;
	}

	public List<Integer> getGoalPredicatesPos() {
		return goalPredicatesPos;
	}

	public List<Integer> getGoalPredicatesNeg() {
		return goalPredicatesNeg;
	}

	public int getMaxReductionsPerTask() {
		return maxReductionsPerTask;
	}

	public List<IntExp> getCompressedFacts() {
		return compressedFacts;
	}

	public CodedProblem getCodedProblem() {
		return codedProblem;
	}

	public List<HtnMethod> getFlatMethods() {
		return flatMethods;
	}
	
	
	// TO STRING METHODS //

	/**
	 * Helper method constructing a readable string out of an IntExp object.
	 */
	public String intExpToString(IntExp exp) {
		return exp.toString(codedProblem.getConstants(), codedProblem.getTypes(), 
				codedProblem.getPredicates(), codedProblem.getFunctions(), codedProblem.getTaskSymbols());
	}
	/**
	 * Helper method constructing a readable string out of a predicate index.
	 */
	public String predicateToString(int pred) {
		return intExpToString(relevantFacts.get(pred));
	}
	/**
	 * Helper method constructing a readable string out of a Signature object.
	 */
	public String sigToString(Signature sig) {
		return sig.toString(codedProblem.getConstants());
	}
}
