# HTN-SAT

This project is a framework designed to do SAT-based Hierarchical Task Network (HTN) planning. It is the practical contribution of the Master thesis [0] (see the document `thesis.pdf`) and serves as a repository for the publications [1] and [2].

Note that the SAT-based hierarchical planner **Tree-REX** as presented in [2] is referred to as **T-REX** in the master thesis and, generally, throughout this repository. The name has been changed retroactively to avoid name collisions with other planning-related projects.

## Structure

* `bin`: Contains all executables and general scripts.
* `evaluation`: The evaluation data gathered for [0], [1], and [2].
* `htn-sat`: The java project HTN-SAT which is used to read PDDL problem files, ground the problem via preprocessing routines [3], encode the problem into SAT, call an external solving application, and decode the solution back to a plan.
* `lib`: Contains libraries needed to build and execute the applications.
* `problems`: Contains some HTN problem benchmarks in PDDL format.
* `t-rex`: The C++ application for interpreting T-REX abstract encoding files produced by HTN-SAT. This software is designed to read the encoding and then incrementally instantiate clauses and call a SAT solver library to resolve the problem and output a solution (in encoded form).

## Building

### HTN-SAT (Java application)

A current version of HTN-SAT is provided as `bin/htnsat.jar`. If you change the code, re-build it e.g. by using an IDE as Eclipse. It requires [JCommander](https://github.com/cbeust/jcommander/releases/tag/1.71) to be dropped in the `lib/` directory, as `jcommander.jar`.

### SAT solvers and Incplan

SAT solvers for non-incremental solving (used for the GCT encoding) can be built directly from the sources provided by their developers (e.g. [Glucose](http://www.labri.fr/perso/lsimon/glucose/), [Lingeling](https://github.com/arminbiere/lingeling)).

For the incremental solving application [Incplan](https://github.com/StephanGocht/Incplan) (which is used for the SMS encodings), refer to the [IPASIR](https://github.com/biotomas/ipasir) project. Specifically, you can follow these steps:

* Download the IPASIR project.
* Edit the file `scripts/mkall.sh` to build `incplan` as an application and the SAT solvers you wish to use. 
* Execute `scripts/mkall.sh` to build Incplan and also the IPASIR libraries of the indicated SAT solvers.
* Find the Incplan executable(s) in the `bin` directory of IPASIR and the SAT libraries in `sat/<solver>/libipasir*`.

All necessary sources to build Incplan against MiniSAT (`minisat220`) or PicoSAT (`picosat961`) are included in that project. A procedure to add Glucose and Lingeling is described in the following; for additional solvers, the README files of IPASIR and the existing solver files will be helpful to set up an analogous building process.

#### Glucose

For the evaluations, I used [this Glucose-IPASIR version](https://baldur.iti.kit.edu/sat-competition-2017/solvers/incremental/glucose-ipasir.zip), which is plug-and-play: Just copy the `glucose4` directory into `ipasir/sat/`.

You can also use the official [Glucose-syrup-4.1](http://www.labri.fr/perso/lsimon/downloads/softwares/glucose-syrup-4.1.tgz) together with the Makefile and glue code included in this project in `lib/glucose4/` (updated to Glucose's current project structure and code base). Drop the contents of the Glucose-syrup-4.1 ZIP in a directory `ipasir/sat/glucose4/glucose-4/` and drop the files provided in `lib/glucose4/` in the parent directory `ipasir/sat/glucose4/`.

#### Lingeling

Download the [Lingeling Github project](https://github.com/arminbiere/lingeling) and put its contents in a directory `ipasir/sat/lingeling/lingeling`. Then, drop the provided contents of `lib/lingeling/` in the parent directory `ipasir/sat/lingeling/`. 

### T-REX Interpreter

T-REX requires the [`boost` libraries](https://www.boost.org/) `dynamic_bitset` and `program_options`, and it uses IPASIR just like Incplan. To build the application, ensure that the corresponding IPASIR library, previously built as `ipasir/sat/<solver>/libipasir*`, is present in the directory `lib/<solver>/`. Then `cd t-rex` and execute `bash install.sh`. The default build links T-REX against `glucose4`, which can be changed by setting the `IPASIRSOLVER` variable, e.g. executing `IPASIRSOLVER=minisat220 bash install.sh`.
If T-REX is built successfully, the binary is written as `bin/interpreter_t-rex`.

## Execution

Execute `java -jar htnsat.jar` and/or `./interpreter_t-rex` to get the corresponding help messages which arguments the programs take. As an example:
```
java -jar htnsat.jar -d ../problems/Rover/domain.pddl -p ../problems/Rover/p01.pddl -e t-rex -i '{-P,-b8192,-o,-l-1}'
```
This plans the problem Rover01 by using the T-REX encoding with the parameters `-P` (predecessor encoding), `-b8192` (binary At-Most-One threshold), `-o` (perform a plan optimization) and `-l-1` (linear plan optimization search with a step size of -1).

HTN-SAT is hardcoded to execute a script `solve.sh` for non-incremental solving, `solve_incremental.sh` for incremental non-T-REX solving, and `./interpreter_t-rex` for T-REX solving. You can change the two former scripts to call any executable you prepared.

HTN-SAT writes its logs into the directory `logs/*` relative to the current directory.

More information on the usage of T-REX and on the underlying problem model and input format is provided in the README in the `t-rex/` directory.

## Evaluation

All relevant evaluation results can be found in the `evaluation/logs/` directory.

In order to reproduce any evaluations that have been done or to conduct new evaluations, please consult the README in the `evaluation/` directory.
To reproduce the T-REX parameter tuning with [ParamILS](http://www.cs.ubc.ca/labs/beta/Projects/ParamILS/index.html), consult the README in `evaluation/tuning/`.

## Contact

If there are any questions or problems regarding this project, please feel free to contact me directly over Gitlab or send me a mail: `mail@dominikschreiber.de`.

## References

[0] Schreiber, Dominik (2018). "Hierarchical Task Network Planning Using SAT Techniques." Master thesis.

[1] Schreiber, Dominik et al. (2019). "Efficient SAT Encodings for Hierarchical Planning." To appear in: _Proceedings of the 11th International Conference on Agents and Artificial Intelligence_ (ICAART).

[2] Schreiber, Dominik et al. (2019). "Tree-REX: SAT-based Tree Exploration for Efficient and High-Quality HTN Planning." To appear in: _Proceedings of the 19th International Conference on Automated Planning and Scheduling_ (ICAPS).

[3] Ramoul, Abdeljalil et al. (2017). "Grounding of HTN Planning Domain." In: _International Journal on Artificial Intelligence Tools_ 26.05, p.1760021.
