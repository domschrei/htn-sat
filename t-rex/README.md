# T-REX Interpreter application

In the following, the usage of the T-REX Interpreter application is described.

## Problem model

* The central task is to find the _minimal, exhaustive, depth-first expansion_ of a tree-like hierarchical structure.
* There are _primitive_ and _composite_ elements. Primitive elements correspond to leaf nodes in the hierarchy and composite elements correspond to inner nodes.
* Each _layer_ has positions `0, ..., s_l` which each contain exactly one element. In addition, at each position at each layer, any subset of some universal set of _facts_ may hold.
* At each layer, a fact changes in between positions `i, i+1` if and only if this fact change is consistent with the element at position `i`, as defined by its _preconditions and effects_. Composite elements do not have any effects themselves, but a fact may still change there if some valid expansion of the element may lead to such a fact change.
* The content of a layer `l` is propagated to a layer `l+1` as follows: Primitive elements and facts are propagated without any change, and each composite element is replaced by some valid sequence of elements which correspond to the _child nodes_ of the element. This implies that the array generally grows with each non-trivial element expansion, and the entire content of the array _after_ each expansion is shifted accordingly.
* In the end, a sequence of primitive elements is desired which correspond to the ordered set of leaf nodes at a layer where no composite elements are left.

In the context of HTN planning, primitive elements are actions and composite elements are reductions.

## Input format

The basis for the encoding notation is Dimspec, defined [here](https://github.com/StephanGocht/Incplan).
The formula file still has four blocks `i`, `u`, `g`, `t` with the same header syntax and the same intuitive meaning.

Note that the formula notation format is highly specialized for the problem which it was designed for. For implementation and efficiency purposes, only certain kinds of clauses are expected by the application (with literals in the specified order), which are enumerated below.

A very small example formula (the encoding of problem 01 from the Elevator domain) is provided as `example.cnf` in this directory.

### Variable domains

The domain of variables is restricted to the following meaning:

* Variables `v ∈ {1, ..., A}` represent primitive elements. Hereby, variable 1 is preserved for a special `blank` primitive element which does nothing.
* Variables `v ∈ {A+1, ..., A+R}` represent composite elements.
* Variable A+R+1 represents the `isPrimitive` predicate.
* Variables v ∈ {A+R+1, ..., A+R+F+1}` represent facts.
* In the transitional specification, a second, equally sized set of variables `{A+R+F+2, ..., 2*(A+R+F+1)-1}` is used. Any variable `A+R+F+1+x` represents variable `x`, but at _the next depth_.

### Header lines

The application expects header lines as follows:
```
ce A R
cf F
ci x 
```
`A`, `R` and `F` correspond to the number of primitive elements, composite elements, and facts respectively. The third line is optional; `x` represents a variable value which corresponds to a trivial `nop` action which is not supposed to increase the effective plan length.

In the following definitions, let `p` be any fact literal (can be negative), and `i` any non-negative position. If `lit` is any literal, then let `lit'` denote the same literal at the next depth. Specifically, `lit' := sign(lit) * (A+R+F+1 + abs(lit))`.

### Initial state clauses

* **Facts at the initial state:**  
`p@i 0`
* **Elements at the initial state:**  
`v1@i v2@i ... vk@i 0`, where `{v1, ..., vk}` are element variables.

### Universal clauses

* **Preconditions of primitive elements:**  
`-v p 0`, where `v` is a primitive element variable.
* **Effects of primitive elements:**  
`-v p+1 0`, where `v` is a primitive element variable and "+1" is an annotated suffix to the literal `p`.
* **Preconditions of composite elements:**  
`-v p 0`, where `v` is a composite element variable.

### Goal state clauses

* **Goal condition:**  
`lit@A 0`, where `lit` is any variable. (Note that this is literally the letter "A", not some variable.)  
Has the meaning that `lit` must hold at _every_ position at the final layer.

### Transitional clauses

* **Expansion of composite elements:**  
`-v v1'+i v2'+i ... vk'+i 0`, where `{v1, ..., vk}` are element variables, and `+i` is an annotation with `i ≥ 0`.

### Implicitly added clauses

Note that a number of clauses is implicitly induced by the given problem. As a result, these clauses do not need to be specified explicitly, as they are added automatically just as needed:

* Frame axioms of facts
* At most one primitive element at each spot
* The definition of the `isPrimitive` predicate at each spot
* The propagation of facts and primitive elements
* The addition of `blank` elements whereever some element propagation leads to otherwise undefined positions

## Arguments

Launch the application with `-h` to get an exhaustive list of arguments which it takes.
