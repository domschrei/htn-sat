#!/bin/bash

grep "Transition,true," clause_graph.gdf > transitions
grep -v "Transition,true," clause_graph.gdf > all_but_transitions
cat all_but_transitions transitions > clause_graph.gdf
rm all_but_transitions transitions
