#!/bin/bash

set -e

if [ ! $IPASIRSOLVER ] ; then
    # Default solver
    IPASIRSOLVER=glucose4
fi
solver=$IPASIRSOLVER
echo "Linking with $solver. (Possible solvers: glucose4, lingeling, minisat220, picosat961)"

IPASIRSOLVER=$solver make $@ || exit 1
mv t-rex ../bin/interpreter_t-rex
