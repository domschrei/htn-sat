#ifndef SOLVER_INTERFACE_H
#define SOLVER_INTERFACE_H

#include <vector>
#include <functional>

#include "inc_problem.h"
#include "hierarchylayer.h"
#include "clause_graph.h"
#include "graph_renderer.h"

// Inclusion of IPASIR for SAT solver communication
extern "C" {
    #include "ipasir.h"
#include "params.h"
}

/**
 * Manages an instantiator instance and communicates with an
 * incremental SAT solver over IPASIR in order to solve
 * an IncHex problem.
 */
class SolverInterface {
    
public:
    /** 
     * Represents that the solver could not find any result,
     * neither positive nor negative.
     */
    static const int RESULT_UNKNOWN = 0;
    /**
     * Represents that the solver found a satisfying assignment.
     */
    static const int RESULT_SAT = 10;
    /**
     * Represents that the solver found the clause set to be
     * unsatisfiable.
     */
    static const int RESULT_UNSAT = 20;
    
    static const int OPTIMIZATION_MODE_LINEAR_DECREASING = 1;
    static const int OPTIMIZATION_MODE_LINEAR_INCREASING = 2;
    static const int OPTIMIZATION_MODE_BINARY = 3;
    
    static const int OPTIMIZATION_LINEAR_STEPSIZE = 5;
    /**
     * In the plan optimization step, a skewed binary search is performed
     * to find the depth-optimal plan. This value is the interpolation
     * parameter between the lower and the upper bound. 0.5 means no skew,
     * higher values mean a skew towards higher plan values to consider
     * (such that SAT is the more likely result) and lower values mean a
     * skew towards lower plan values to consider (such that UNSAT is more
     * likely).
     */
    constexpr static const double OPTIMIZATION_BINARY_NEXT_STEP_RATIO = 0.9;
    static const bool OPTIMIZATION_BINARY_ALTERNATE_SKEW = false;
    
    static const int OPTIMIZATION_MODE = OPTIMIZATION_MODE_BINARY;
    
    /**
     * Creates a SolverInterface instance and initializes
     * an Instantiator for the provided problem.
     */
    SolverInterface(IncProblem* problem, void* solver);
    
    /**
     * Attempts to incrementally solve the problem using
     * a SAT solver and the instantiator instance. Returns
     * true iff a solution has been found.
     */
    bool solve();
    
    /**
     * True, iff a solution has been found so far.
     * Also true if the solver is still optimizing the plan length.
     */
    bool has_found_solution();
    
    /**
     * Only if the solve() call has been successful, returns
     * the relevant values at the final considered depth.
     */
    std::vector<int>* get_relevant_results();
    
    /**
     * Adds additional clauses and performs a binary search on the plan lengths
     * which are possible, until a depth-optimal solution is found by the 
     * SAT solver.
     */
    void optimize_result(std::function<void(SolverInterface*, bool)> callback);
    
private:
    
    /** The problem instance. */
    IncProblem* p;
    
    HierarchyLayer* prev_layer;
    HierarchyLayer* next_layer;
    
    /** Represents the SAT solver inside IPASIR. */
    void* solver;
    
    Params* params;
    
    GraphRenderer* renderer;
    
    /**
     * True, iff a solution has been found.
     */
    bool found_solution;
    
    /**
     * The total amount of clauses that has already been added 
     * to the SAT solver.
     */
    int num_added_clauses;
    /**
     * The total amount of literals that has already been added
     * to the SAT solver.
     */
    int num_added_literals;
    /**
     * Contains the literals that have been added previously
     * (but not "published" yet by an end() call).
     */
    std::vector<int> current_clause;
    /**
     * If a solve() call has been sucessful, this contains
     * the relevant result values at the final depth.
     */
    std::vector<int> results_of_final_depth;
    
    int try_solve();
    
    /**
     * Instantiates and adds clauses for the initial depth
     * to the solver.
     */
    void add_init_clauses();
    
    /**
     * Instantiates and adds clauses for the universal state
     * of the current depth (also provided as an argument).
     */
    void add_universal_clauses();
    
    void add_fact_transitions();
    void add_prim_elems_transitions();
    void add_comp_elems_transitions();
    void add_primitivity_propagation();
    void add_expansion_conditions();
    void add_predecessor_rules();
    
    void add_preconditions_and_effects();
    void add_primitiveness_definitions();
    void add_frame_axioms(bool full_comp_elem_encoding);
    void add_reduction_alternatives();
    void add_reduction_conditions();
    
    void add_amo_elem(bool prim_elems);
        void add_quadratic_amo(std::vector<int>* vars);
        void add_binary_amo(std::vector<int>* vars);
        void extend_binary_amo(std::vector<int>* vars, int position);
    void add_amo_new_prim_elem();
    
    /**
     * Instantiates and assumes literals for the goal state
     * of the current depth (also provided as an argument).
     * These assumed literals will only be valid for a single
     * SAT solver call.
     */
    void add_goal_conditions(bool add_permanently);
    /**
     * Instantiates and adds clauses for the transition
     * from the last depth to the current depth, whereas the 
     * current depth is also provided as an argument).
     */
    void add_transitional_clauses();
    /**
     * Adds transitional clauses enforcing that the "empty"
     * successors of an element will become "blank" elements.
     */
    void add_blank_transitions();
    
    /**
     * After a sucessful solver call with a SAT result,
     * reads the relevant values from the solver and memorizes them.
     */
    void process_result();
    
    /**
     * Adds post-processing clauses which effectively count the plan length
     * at the final depth, and which can be used to optimize a found solution.
     */
    int add_blank_counter();
    
    /**
     * Adds a single "global" literal (positive or negative) 
     * to the current clause in the solver.
     */
    void add(int lit);
    /**
     * Concludes the clause that has been created by previous add(lit)
     * calls to the solver.
     */
    void end();
    /**
     * Assume a single literal to hold for (only) the following 
     * solve attempt.
     */
    void assume(int lit);
    
    /**
     * Prints a banner for the makespan with the provided index.
     */
    void log_makespan(int makespan);
    /**
     * Prints the provided result returned by a SAT solver call.
     */
    void log_result(int result);
    
    void print_assigned_vars_per_depth();
    
    /**
     * Encapsules some statistics about one particular stage 
     * of clause instantiation.
     */
    struct clause_stats {
        int added_clauses;
        int added_literals;
        int time_per_clause;
    };
    
    /**
     * Initializes a clause_stats object (its attributes may not
     * be used yet!) and returns it, such that at some later point
     * end_clause_stats can be called with this object.
     */
    clause_stats start_clause_stats();
    /**
     * Takes a clause_stats object created by start_clause_stats and 
     * sets its attributes according to what happened since then.
     */
    void end_clause_stats(clause_stats*);
};

#endif
