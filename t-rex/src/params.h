#ifndef T_REX_PARAMS_H
#define T_REX_PARAMS_H

#include <string>

class Params {

public:
    static Params* get();

    std::string input_file = "";
    long timeout_milliseconds = 0;
    
    int min_depth = 0;
    int max_depth = __INT_MAX__;

    bool reduction_choices = false;
    
    bool init_layer_amo = false;
    bool full_amo = false;
    bool predecessors_specification = false;
    
    bool full_element_encoding = false;
    bool full_fact_encoding = false;
    bool full_prim_elem_encoding = false;
    
    /**
    * Any number of primitive elements lower than this value will be
    * At-Most-One encoded in a quadratic fashion 
    * (n(n-1)/2 clauses, zero additional variables). Any number
    * of primitive elements greater or equal to this value will be 
    * encoded in a binary fashion
    * (n(log n) clauses, log(n) additional variables).
    */
    int threshold_binary_amo = 4;

    // Only if PREDECESSORS_SPECIFICATION
    bool blank_elem_predecessors_specification = false;
    // Only if PRIM_ELEMS_VARIABLE_REUSAGE
    bool cumulative_amo = true;
    
    bool optimize_plan = false;
    int optimization_mode = -1;
    int optimization_stepsize = -1;
    double optimization_skewness = 0.5;
    
    bool output_graph = false;
    
private:
    Params() {};
};

#endif
