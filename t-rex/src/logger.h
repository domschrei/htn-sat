#ifndef LOGGER_HPP_
#define LOGGER_HPP_

#include <string>
#include <chrono>

using namespace std::chrono;

typedef steady_clock::time_point timepoint;

/**
 * Measures the current time in order to log relative timestamps
 * in the following.
 */
void start_timer();
/**
 * Measures the current time in order to measure the elapsed time until
 * the next elapsed_round_nanosecs() call.
 */
void start_round_timer();

/**
 * The amount of elapsed microseconds since the last call of start_timer().
 */
long int elapsed_microsecs();
/**
 * The amount of elapsed nanoseconds since the last call of start_round_timer().
 */
long int elapsed_round_nanosecs();

/**
 * Logs the given message together with the relative timestamp.
 * A linebreak will be added at the end of the message.
 */
void log(std::string msg);

/**
 * Returns a string printf-style-formatted with an integer (%i).
 */
std::string str(const char* msg, int i);
/**
 * Returns a string printf-style-formatted with a double (%f).
 */
std::string str(const char* msg, double d);
/**
 * Returns a string printf-style-formatted with two integers (%i).
 */
std::string str(const char* msg, int i1, int i2);
/**
 * Returns a string printf-style-formatted with three integers (%i).
 */
std::string str(const char* msg, int i1, int i2, int i3);
/**
 * Returns a string printf-style-formatted with a string (%s).
 */
std::string str(const char* msg, const char* format_str);
/**
 * Returns a string printf-style-formatted with two strings (%s).
 */
std::string str(const char* msg, const char* str1, const char* str2);

const char* large_int_str(int in);

#endif
