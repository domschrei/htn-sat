#include "graph_renderer.h"
#include "params.h"

GraphRenderer::GraphRenderer(IncProblem* p)
{
    this->p = p;
    graph = new ClauseGraph();
}

void GraphRenderer::edge_mode(EdgeProperties prop)
{
    graph->set_edge_properties(prop);
}

void GraphRenderer::clear_edge_mode()
{
    graph->clear_edge_properties();
}

void GraphRenderer::add_to_edges(int id)
{
    graph->add_to_graph(id);
}

void GraphRenderer::finish(HierarchyLayer* final_layer)
{
    init_graph_nodes(final_layer);
    graph->close_files();
}

void GraphRenderer::init_graph_nodes(HierarchyLayer* final_layer)
{
    std::vector<double> y_starts = std::vector<double>();
    
    HierarchyLayer* layer = final_layer;
    
    double x, y, z;
    z = 0.0;
    
    int num_vars_per_row = 10;
    
    do {
        std::vector<double> new_y_starts = std::vector<double>();
        int x_start = 150.0 * layer->get_depth();
        int y_start = 100.0;
        
        y = y_start;
        
        for (int pos = 0; pos < layer->get_array_size(); pos++) {
            
            x = x_start;
            int vars_this_row = 0;
            
            if (layer->get_next_layer() != NULL) {
                int last_pos = layer->get_next_position(pos);
                y = y_starts[last_pos];
            }
            new_y_starts.push_back(y);
            
            bool fact_added = false;
            for (int fact = 1; fact <= p->get_num_facts(); fact++) {
                if (layer->is_fact_possible(fact, pos) && layer->is_fact_new(fact, pos)) {
                    fact_added = true;
                    add_node_at(layer->lit_at_pos(p->fact_idx_to_var(fact), pos), 
                                node_prop_fact, x, y, z);
                    
                    x += 10;
                    vars_this_row++;
                    if (vars_this_row == num_vars_per_row) {
                        x = x_start;
                        y += 10;
                        vars_this_row = 0;
                    }
                }
            }
            
            //if (fact_added) {
                y += 20;
            //}
            x = x_start;
            vars_this_row = 0;
            
            add_node_at(layer->lit_at_pos(p->get_primitivity_var(), pos), 
                        node_prop_primitivity, x, y, z);
            
            x += 10;
            vars_this_row++;
            
            int blank_elem = 1;
            if (layer->is_elem_possible(blank_elem, pos) 
                && (Params::get()->full_prim_elem_encoding
                    || layer->is_prim_elem_new(blank_elem, pos))) {
                
                add_node_at(layer->lit_at_pos(blank_elem, pos), node_prop_blank, x, y, z);
                
                x += 10;
                vars_this_row++;
                if (vars_this_row == num_vars_per_row) {
                    x = x_start;
                    y += 10;
                    vars_this_row = 0;
                }
            }
            
            for (int elem = 2; elem <= p->get_num_elems(); elem++) {
                if (layer->is_elem_possible(elem, pos)
                && (Params::get()->full_prim_elem_encoding
                    || !p->is_var_prim_elem(elem) || layer->is_prim_elem_new(elem, pos))) {
                    
                    NodeProperties prop = (p->is_var_prim_elem(elem) ? node_prop_elem_prim : node_prop_elem_comp);
                    add_node_at(layer->lit_at_pos(elem, pos), prop, x, y, z);
                    
                    x += 10;
                    vars_this_row++;
                    if (vars_this_row == num_vars_per_row) {
                        x = x_start;
                        y += 10;
                        vars_this_row = 0;
                    }
                }
            }        
            
            y += 20;
        }
        
        y_starts = new_y_starts;
        layer = layer->get_prev_layer();
        
    } while (layer != NULL);
}

void GraphRenderer::add_node_at(int id, NodeProperties prop, double x, double y, double z)
{
    prop.has_position = true;
    prop.x = x;
    prop.y = y;
    prop.z = z;
    graph->add_node(id, prop);
}
