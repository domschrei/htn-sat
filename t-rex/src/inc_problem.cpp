#include "inc_problem.h"
#include "boost/dynamic_bitset.hpp"

formula* IncProblem::f(int mode) 
{
    if (mode == MODE_INIT) {
        return &f_init;
    } else if (mode == MODE_UNIV) {
        return &f_univ;
    } else if (mode == MODE_GOAL) {
        return &f_goal;
    } else if (mode == MODE_TRAN) {
        return &f_tran;
    }
}

void IncProblem::set_num_prim_elems(int num_prim_elems)
{
    this->num_prim_elems = num_prim_elems;
    if (num_comp_elems >= 0 && num_facts >= 0)
        init();
}
void IncProblem::set_num_comp_elems(int num_comp_elems)
{
    this->num_comp_elems = num_comp_elems;
    if (num_prim_elems >= 0 && num_facts >= 0)
        init();
}
void IncProblem::set_num_reductions(int num_reductions)
{
    this->num_reductions = num_reductions;
}
void IncProblem::set_num_facts(int num_facts)
{
    this->num_facts = num_facts;
    if (num_prim_elems >= 0 && num_comp_elems >= 0)
        init();
}

void IncProblem::init()
{
    num_elems = num_prim_elems + num_comp_elems;
    primitivity_var = num_elems + 1;
    if (num_reductions > 0) {
        alt_successors = std::vector<std::vector<reduction>>(num_comp_elems);
    } else {
        successors = std::vector<std::vector<std::vector<int>>>(num_comp_elems);
    }
    predecessors = std::vector<std::vector<std::vector<int>>>(num_elems);
    preconditions_of_elems = std::vector<std::set<int>>(num_elems);
    effects_of_prim_elems = std::vector<std::set<int>>(num_prim_elems);
    supporting_prim_elems_of_pos_facts = std::vector<std::set<int>>(num_facts+1);
    supporting_prim_elems_of_neg_facts = std::vector<std::set<int>>(num_facts+1);
    if (num_reductions > 0) {
        first_reduction_var = primitivity_var + num_facts + 1;
    }
}

void IncProblem::add_successor(int comp_elem, int offset, int child_elem)
{
    // Enlarge the data structure as needed
    while (offset >= successors[comp_elem].size()) {
        successors[comp_elem].push_back(std::vector<int>());
    }
    
    // Set the new successor and update the maximal expansion size
    successors[comp_elem][offset].push_back(child_elem);
    max_expansion_size = std::max(max_expansion_size, offset+1);
    
    // Do the same for the predecessors data structure
    while (offset >= predecessors[child_elem].size()) {
        predecessors[child_elem].push_back(std::vector<int>());
    }
    predecessors[child_elem][offset].push_back(comp_elem);
}

void IncProblem::add_successor(int comp_elem, int red_idx, int offset, int child_elem)
{
    // Add empty reductions as necessary 
    // to get to the correct reduction
    while (red_idx >= alt_successors[comp_elem].size()) {
        reduction red;
        alt_successors[comp_elem].push_back(red);
    }
    reduction* red = &alt_successors[comp_elem][red_idx];
    
    // Get to the desired offset of the reduction
    while (offset >= red->size()) {
        red->push_back(std::vector<int>());
    }
    // Add successor at offset
    red->at(offset).push_back(child_elem);
    
    // Add predecessor
    while (offset >= predecessors[child_elem].size()) {
        predecessors[child_elem].push_back(std::vector<int>());
    }
    predecessors[child_elem][offset].push_back(comp_elem);
}

void IncProblem::add_precondition_of_elem(int fact, int elem)
{
    preconditions_of_elems[elem].insert(fact);
}

void IncProblem::add_effect_of_prim_elem(int fact, int prim_elem)
{
    effects_of_prim_elems[prim_elem].insert(fact);
}

void IncProblem::add_supporting_prim_elem_of_pos_fact(int prim_elem, int fact)
{
    supporting_prim_elems_of_pos_facts[fact].insert(prim_elem);
}

void IncProblem::add_supporting_prim_elem_of_neg_fact(int prim_elem, int fact)
{
    supporting_prim_elems_of_neg_facts[fact].insert(prim_elem);
}

void IncProblem::add_expansion_condition(int comp_elem, int child_elem, int offset, int fact)
{
    expansion_condition cond;
    cond.comp_elem = comp_elem;
    cond.child_elem = child_elem;
    cond.offset = offset;
    cond.fact_condition = fact;
    
    expansion_conditions.push_back(cond);
}

void IncProblem::add_reduction_condition(int comp_elem, int red_idx, int fact)
{
    reduction_condition cond;
    cond.comp_elem = comp_elem;
    cond.red_idx = red_idx;
    cond.fact_condition = fact;
    
    reduction_conditions.push_back(cond);
}

void IncProblem::add_improper_element(int elem)
{
    this->improper_element = elem;
}

void IncProblem::compute_possible_fact_changes_of_comp_elems()
{
    facts_changed_by_comp_elems = std::vector<boost::dynamic_bitset<>>(
        num_comp_elems, boost::dynamic_bitset<>(num_facts+1));
    
    // First pass: iterate over primitive elems
    for (int prim_elem = 0; prim_elem < num_prim_elems; prim_elem++) {
        
        std::set<int>* effects = get_effects_of_prim_elem(prim_elem);
        
        // For all predecessors
        std::vector<std::vector<int>>* predecessors = get_predecessors_of_elem(prim_elem);
        for (int offset = 0; offset < predecessors->size(); offset++) {
            for (int pred_idx = 0; pred_idx < predecessors->at(offset).size(); pred_idx++) {
                int comp_pred_elem = predecessors->at(offset)[pred_idx];
                
                // Add the primitive elem's actions to the predecessor elem's changes
                for (auto it = effects->begin(); it != effects->end(); ++it) {
                    int fact = std::abs(*it);
                    facts_changed_by_comp_elems[comp_pred_elem][fact] = 1;
                }
            }
        }
    }
    
    // Consecutive passes: iterate over previously changed comp. elems
    bool any_change = true;
    while (any_change) {
        any_change = false;
                
        for (int comp_elem = 0; comp_elem < num_comp_elems; comp_elem++) {
            
            std::vector<std::vector<std::vector<int>>*> successors_list;
            
            if (num_reductions == 0) {
                std::vector<std::vector<int>>* successors = get_successors_of_comp_elem(comp_elem);
                successors_list.push_back(successors);
            } else {
                std::vector<reduction>* reductions = get_alt_successors_of_comp_elem(comp_elem);
                for (int i = 0; i < reductions->size(); i++) {
                    successors_list.push_back(&reductions->at(i));
                }
            }
            
            for (auto it = successors_list.begin(); it != successors_list.end(); ++it) {
                auto successors = *it;
                
                for (int offset = 0; offset < successors->size(); offset++) {
                    for (int suc_idx = 0; suc_idx < successors->at(offset).size(); suc_idx++) {
                        
                        int child_elem = successors->at(offset)[suc_idx];
                        if (is_var_comp_elem(child_elem+1)) {
                            int comp_child_elem = comp_elem_var_to_idx(child_elem+1);
                            int count_before = facts_changed_by_comp_elems[comp_elem].count();
                            facts_changed_by_comp_elems[comp_elem] |= facts_changed_by_comp_elems[comp_child_elem];
                            any_change |= facts_changed_by_comp_elems[comp_elem].count() > count_before;
                        }
                        
                    }
                }
            }
        }
    }
}

bool IncProblem::is_var_prim_elem(int var)
{
    return var >= 1 && var <= num_prim_elems;
}
bool IncProblem::is_var_comp_elem(int var)
{
    return var > num_prim_elems && var <= num_elems;
}
bool IncProblem::is_var_elem(int var)
{
    return var >= 1 && var <= num_elems;
}
bool IncProblem::is_var_fact(int var)
{
    // The element num_elems+1 is the primitivity variable
    return var > num_elems + 1 && var <= num_elems + 1 + num_facts;
}
bool IncProblem::is_var_reduction_idx(int var)
{
    return var >= first_reduction_var && var < first_reduction_var + num_reductions;
}

int IncProblem::prim_elem_var_to_idx(int var)
{
    return var-1;
}
int IncProblem::comp_elem_var_to_idx(int var)
{
    return var - 1 - num_prim_elems;
}
int IncProblem::elem_var_to_idx(int var)
{
    return var - 1;
}
int IncProblem::fact_var_to_idx(int var)
{
    return var - (num_elems + 1);
}
int IncProblem::fact_idx_to_var(int idx)
{
    return idx + (num_elems + 1);
}
int IncProblem::reduction_var_to_idx(int var)
{
    return var - first_reduction_var;
}

std::set<int>* IncProblem::get_preconditions_of_elem(int elem)
{
    return &preconditions_of_elems[elem];
}

std::set<int>* IncProblem::get_effects_of_prim_elem(int prim_elem)
{
    return &effects_of_prim_elems[prim_elem];
}

std::set<int>* IncProblem::get_supporting_prim_elems_of_fact(int fact, bool positive)
{
    if (positive)
        return &supporting_prim_elems_of_pos_facts[fact];
    else
        return &supporting_prim_elems_of_neg_facts[fact];
}

std::vector<std::vector<int>>* IncProblem::get_successors_of_comp_elem(int comp_elem)
{
    return &successors[comp_elem];
}

std::vector<reduction>* IncProblem::get_alt_successors_of_comp_elem(int comp_elem)
{
    return &alt_successors[comp_elem];
}

std::vector<std::vector<int>>* IncProblem::get_predecessors_of_elem(int elem)
{
    return &predecessors[elem];
}

boost::dynamic_bitset<>* IncProblem::get_facts_changed_by_comp_elem(int comp_elem)
{
    return &facts_changed_by_comp_elems[comp_elem];
}

std::vector<boost::dynamic_bitset<>>* IncProblem::get_facts_changed_by_comp_elems()
{
    return &facts_changed_by_comp_elems;
}

std::vector<IncProblem::expansion_condition>* IncProblem::get_expansion_conditions()
{
    return &expansion_conditions;
}

std::vector<IncProblem::reduction_condition>* IncProblem::get_reduction_conditions()
{
    return &reduction_conditions;
}

int IncProblem::get_num_prim_elems()
{
    return num_prim_elems;
}
int IncProblem::get_num_comp_elems()
{
    return num_comp_elems;
}
int IncProblem::get_num_reductions()
{
    return num_reductions;
}
int IncProblem::get_num_elems()
{
    return num_elems;
}
int IncProblem::get_num_facts()
{
    return num_facts;
}
int IncProblem::get_num_non_elems()
{
    return f(IncProblem::MODE_UNIV)->num_vars 
        - (get_num_elems() + 1 + get_num_facts() + get_num_reductions());
}

int IncProblem::get_max_expansion_size()
{
    return max_expansion_size;
}

int IncProblem::get_primitivity_var()
{
    return primitivity_var;
}

int IncProblem::get_first_reduction_var()
{
    return first_reduction_var;
}

bool IncProblem::has_improper_element()
{
    return improper_element >= 0;
}

int IncProblem::get_improper_element()
{
    return improper_element;
}

int IncProblem::get_prim_elems_bitlength()
{
    int num_vars = num_prim_elems;
    uint bitlength = 0;
    int n = num_vars;
    do {
        ++bitlength; 
        n /= 2;
    } while(n);
    return bitlength;
}

void IncProblem::print(bool verbose)
{
    // Print data structure (for debugging)
    std::cout << "Init" << std::endl;
    std::cout << "  " << f_init.num_clauses << " clauses, " 
                      << f_init.num_vars << " variables" << std::endl;
    if (verbose) {
        for (int c = 0; c < f_init.num_clauses; c++) {
            int* clause = f_init.clauses[c];
            int clause_size = f_init.clause_sizes[c];
            for (int i = 0; i < clause_size; i++) {
                std::cout << clause[i] << " ";
            }
            std::cout << std::endl;
        }
    }
    std::cout << "Univ" << std::endl;
    std::cout << "  " << f_univ.num_clauses << " clauses, " 
                      << f_univ.num_vars << " variables" << std::endl;
    if (verbose) {
        for (int c = 0; c < f_univ.num_clauses; c++) {
            int* clause = f_univ.clauses[c];
            int clause_size = f_univ.clause_sizes[c];
            for (int i = 0; i < clause_size; i++) {
                std::cout << clause[i] << " ";
            }
            std::cout << std::endl;
        }
    }
    std::cout << "Goal" << std::endl;
    std::cout << "  " << f_goal.num_clauses << " clauses, " 
                      << f_goal.num_vars << " variables" << std::endl;
    if (verbose) {
        for (int c = 0; c < f_goal.num_clauses; c++) {
            int* clause = f_goal.clauses[c];
            int clause_size = f_goal.clause_sizes[c];
            for (int i = 0; i < clause_size; i++) {
                std::cout << clause[i] << " ";
            }
            std::cout << std::endl;
        }
    }
    std::cout << "Tran" << std::endl;
    std::cout << "  " << f_tran.num_clauses << " clauses, " 
                      << f_tran.num_vars << " variables" << std::endl;
    if (verbose) {
        for (int c = 0; c < f_tran.num_clauses; c++) {
            int* clause = f_tran.clauses[c];
            int clause_size = f_tran.clause_sizes[c];
            for (int i = 0; i < clause_size; i++) {
                std::cout << clause[i] << " ";
            }
            std::cout << std::endl;
        }
    }
}
