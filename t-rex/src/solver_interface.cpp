#include <iostream>
#include <algorithm>
#include <functional>
#include <cmath>

#include "solver_interface.h"
#include "logger.h"
#include "inc_problem.h"
#include "params.h"

bool should_terminate()
{
    long timeout_microsecs = Params::get()->timeout_milliseconds * 1000;
    return (elapsed_microsecs() >= timeout_microsecs);
}

int terminate(void * state) 
{
    return should_terminate() ? 1 : 0;
}

void check_terminate()
{
    if (Params::get()->timeout_milliseconds > 0 && should_terminate()) {
        log("Computation timed out; exiting.");
        exit(124); // timeout exit code
    }
}

SolverInterface::SolverInterface(IncProblem* problem, void* solver)
{
    this->p = problem;
    this->solver = solver;
    this->params = Params::get();
    num_added_clauses = 0;
    num_added_literals = 0;
    
    // Initialize solver
    if (params->timeout_milliseconds > 0) {
        ipasir_set_terminate(solver, 0, terminate);
    }
    
    // Initialize hierarchy
    next_layer = new HierarchyLayer(problem);
    prev_layer = NULL;
    
    if (params->output_graph) {
        renderer = new GraphRenderer(p);
    }
}

bool SolverInterface::solve()
{
    check_terminate();
    
    int depth = 0;
    
    // Print makespan banner
    log_makespan(depth);
    
    clause_stats stats = start_clause_stats();
    
    // Add the initial state variables
    add_init_clauses();
    
    add_primitiveness_definitions();
    
    if (params->init_layer_amo)
        add_amo_elem(/*prim_elems=*/false);
    
    if (params->output_graph)
        renderer->edge_mode(renderer->edge_prop_fact_related);
    
    add_frame_axioms(/*full_comp_elem_encoding=*/false); // "true" is optional
    
    if (params->reduction_choices) {
        add_reduction_alternatives();
        add_reduction_conditions();
    }
    
    if (params->output_graph)
        renderer->clear_edge_mode();
    
    end_clause_stats(&stats);
    log(str("Instantiated %s initial clauses.", 
                large_int_str(stats.added_clauses)));
    log(str("The encoding contains a total of %s distinct variables.", 
                large_int_str(next_layer->allocate_helper_variables(0))));
    
    check_terminate();
    
    // First solving attempt
    int result = try_solve();
    
    while (result == RESULT_UNSAT) {
        
        check_terminate();
        
        depth++;
        log_makespan(depth);
        
        if (depth > params->max_depth) {
            log("Maximal depth reached.");
            result = RESULT_UNKNOWN;
            break;
        }
        
        // Compute properties of next depth
        //if (prev_layer)
        //    delete prev_layer;
        prev_layer = next_layer;
        next_layer = next_layer->compute_next_layer();
        log(str("Computed next depth properties: array size of %i.", 
                next_layer->get_array_size()));
        
        check_terminate();
        
        // Transitional clauses from last to current depth
        
        stats = start_clause_stats();
        
        if (params->output_graph)
            renderer->edge_mode(renderer->edge_prop_elem_transition);
        
        add_blank_transitions();
        if (params->full_prim_elem_encoding)
            add_prim_elems_transitions();
        add_comp_elems_transitions();
        if (params->predecessors_specification)
            add_predecessor_rules();
        if (params->full_fact_encoding)
            add_fact_transitions();
        
        if (params->output_graph)
            renderer->clear_edge_mode();
        
        //add_primitivity_propagation();
        
        if (params->output_graph)
            renderer->edge_mode(renderer->edge_prop_fact_related);
        
        add_expansion_conditions();
        
        if (params->output_graph)
            renderer->clear_edge_mode();
        
        end_clause_stats(&stats);
        log(str("Instantiated %s transitional clauses.", 
                large_int_str(stats.added_clauses)));
        
        check_terminate();

        stats = start_clause_stats();
        
        if (params->output_graph)
            renderer->edge_mode(renderer->edge_prop_fact_related);
        
        add_preconditions_and_effects();
        add_frame_axioms(/*full_comp_elem_encoding=*/false); // "true" is optional
        if (params->reduction_choices) {
            add_reduction_alternatives();
            add_reduction_conditions();
        }
        
        if (params->output_graph)
            renderer->clear_edge_mode();
        
        add_primitiveness_definitions();
        if (!params->full_prim_elem_encoding) {
            add_amo_new_prim_elem();
        } else {
            add_amo_elem(/*prim_elems=*/true);
        }
        
        if (params->full_amo)
            add_amo_elem(/*prim_elems=*/false);
        
        end_clause_stats(&stats);
        log(str("Instantiated %s universal clauses.", 
                large_int_str(stats.added_clauses)));
        
        check_terminate();
        
        log(str("Instantiated and added clauses for a total of %s clauses.", large_int_str(num_added_clauses)));
        log(str("The encoding contains a total of %s distinct variables.", large_int_str(next_layer->allocate_helper_variables(0))));
        
        result = try_solve();
    }
    
    if (result == RESULT_SAT) {
        
        // A result has been found
        found_solution = true;
        log(str("Solver returned SAT; a solution has been found at makespan %i.", depth));
        
        // Query the relevant literals by ipasir_val(lit) and store them
        process_result();
        
        if (params->output_graph) {
            renderer->finish(next_layer);
        }
        
        return true;
        
    } else {
        
        // Result is UNKNOWN; an error or timeout occurred
        log("Solver timed out.");
        return false;
    }
}

int SolverInterface::try_solve()
{
    check_terminate();
    
    if (next_layer->get_depth() < params->min_depth) {
        
        log("Skipping solving attempt due to provided minimal depth.");
        return RESULT_UNSAT;
        
    } else /*if (next_layer->contains_prim_elems_everywhere())*/ {
        
        add_goal_conditions(/*add_permanently=*/false);
        
        log(str("Attempting solve with solver <%s> ...", ipasir_signature()));
                
        // Attempt solve
        int result = ipasir_solve(solver);
        log_result(result);
        return result;
        
    }/* else {
        log("Not all positions may contain primitive elements; skipping solving attempt.");
        return RESULT_UNSAT;
    }*/
}

void SolverInterface::add_init_clauses() 
{
    formula* f_init = p->f(IncProblem::MODE_INIT);
    // For each initial clause
    for (int c = 0; c < f_init->num_clauses; c++) {
        int* clause = f_init->clauses[c];
        int clause_size = f_init->clause_sizes[c];
        // For each literal
        for (int i = 0; i+1 < clause_size; i += 2) {
            int lit = clause[i];
            int pos = clause[i+1];
            add(next_layer->lit_at_pos(lit, pos));
        }
        end();
    }
}

void SolverInterface::add_fact_transitions()
{
    int added_fact_transitions = 0;
    
    // For each position
    for (int pos = 0; pos < prev_layer->get_array_size(); pos++) {
        int next_start_pos = prev_layer->get_next_position(pos);
        
        // For each fact
        for (int fact = 1; fact <= p->get_num_facts(); fact++) {

            // Is the fact possible both at the last and at the next depth?
            if (!prev_layer->is_fact_possible(fact, pos) 
                || !next_layer->is_fact_possible(fact, next_start_pos)) {
                continue;
            }
            
            // Yes -- add equivalence clauses between these variables
            int fact_var = p->fact_idx_to_var(fact);
            add(-prev_layer->lit_at_pos(fact_var, pos));
            add(next_layer->lit_at_pos(fact_var, next_start_pos));
            end();
            add(prev_layer->lit_at_pos(fact_var, pos));
            add(-next_layer->lit_at_pos(fact_var, next_start_pos));
            end();
            added_fact_transitions++;
        }
    }
    log(str("%i added fact transitions", added_fact_transitions));
}

void SolverInterface::add_prim_elems_transitions()
{
    // For each position
    for (int pos = 0; pos < prev_layer->get_array_size(); pos++) {
        int next_start_pos = prev_layer->get_next_position(pos);
        
        // For each primitive element
        for (int prim_elem = 0; prim_elem < p->get_num_prim_elems(); prim_elem++) {
            int elem_var = prim_elem + 1;
            
            // Can the element occur there?
            if (!prev_layer->is_elem_possible(elem_var, pos))
                continue;
            
            // Yes -- add implication such that the element 
            // will also occur at the next depth
            add(-prev_layer->lit_at_pos(elem_var, pos));
            add(next_layer->lit_at_pos(elem_var, next_start_pos));
            end();
        }
    }
}

void SolverInterface::add_primitivity_propagation()
{
    for (int pos = 0; pos < prev_layer->get_array_size(); pos++) {
        int next_start_pos = prev_layer->get_next_position(pos);
        add(-prev_layer->lit_at_pos(p->get_primitivity_var(), pos));
        add(next_layer->lit_at_pos(p->get_primitivity_var(), next_start_pos));
        end();
    }
}

void SolverInterface::add_comp_elems_transitions()
{
    // For each position
    for (int pos = 0; pos < prev_layer->get_array_size(); pos++) {
        int next_start_pos = prev_layer->get_next_position(pos);
        
        // For each composite element
        auto possible_elems = prev_layer->get_possible_elems(pos);
        for (int elem_var = possible_elems->find_next(p->get_num_prim_elems());
                elem_var != possible_elems->npos;
                elem_var = possible_elems->find_next(elem_var)) {
            
            int comp_elem_idx = elem_var - 1 - p->get_num_prim_elems();
            
            // Get all possible sets of successors
            std::vector<std::vector<std::vector<int>>*> successors_list;
            if (params->reduction_choices) {
                std::vector<reduction>* reductions = p->get_alt_successors_of_comp_elem(comp_elem_idx);
                for (int r = 0; r < reductions->size(); r++) {
                    successors_list.push_back(&reductions->at(r));
                }
            } else {
                successors_list.push_back(p->get_successors_of_comp_elem(comp_elem_idx));
            }
            
            // For each set of successors
            int red_idx = 0;
            for (auto it = successors_list.begin(); it != successors_list.end(); ++it) {
                
                // For each position that will contain successors of the element
                std::vector<std::vector<int>>* successors = *it;
                for (int offset = 0; offset < successors->size(); offset++) {
                    
                    // If the element occurs at the old depth ...
                    add(-prev_layer->lit_at_pos(elem_var, pos));
                    if (params->reduction_choices) {
                        add(-prev_layer->lit_at_pos(p->get_first_reduction_var() + red_idx, pos));
                    }
                    for (int next_elem_idx = 0; next_elem_idx < successors->at(offset).size(); next_elem_idx++) {
                        int next_elem_var = successors->at(offset)[next_elem_idx] + 1;
                        
                        // ... then one of its successors occurs at the next depth
                        add(next_layer->lit_at_pos(next_elem_var, next_start_pos + offset));
                    }
                    end();
                }
                
                red_idx++;
            }
        }
    }
}

void SolverInterface::add_expansion_conditions()
{
    // For each position
    for (int pos = 0; pos < prev_layer->get_array_size(); pos++) {
        int next_pos = prev_layer->get_next_position(pos);
        
        // For each expansion condition
        std::vector<IncProblem::expansion_condition>* conditions = p->get_expansion_conditions();
        std::vector<IncProblem::expansion_condition>::iterator it;
        for (it = conditions->begin(); it != conditions->end(); ++it) {
            IncProblem::expansion_condition cond = *it;
            
            int comp_elem = cond.comp_elem;
            
            // Can the parent element occur there?
            if (!prev_layer->is_elem_possible(comp_elem, pos))
                continue;
            
            // Yes -- add the fact condition as a clause
            int offset = cond.offset;
            int fact = cond.fact_condition;
            // Position at which the condition must hold
            int pos_precond = next_layer->get_last_fact_occurence_lower_equals(
                next_pos+offset, p->fact_var_to_idx(std::abs(fact)));
            
            add(-prev_layer->lit_at_pos(comp_elem, pos));
            add(-next_layer->lit_at_pos(cond.child_elem, next_pos+offset));
            add((fact>0 ? 1 : -1) * next_layer->lit_at_pos(std::abs(fact), pos_precond));
            end();
        }
    }
}

void SolverInterface::add_primitiveness_definitions()
{
    bool reuse_prim_elems = !params->full_prim_elem_encoding;
    
    // Variable expressing the "is_primitive" predicate
    int prim_var = p->get_primitivity_var();
    
    // For each position
    for (int pos = 0; pos < next_layer->get_array_size(); pos++) {
        
        auto possible_elems = next_layer->get_possible_elems(pos);
        
        // For each possible element
        for (int elem = possible_elems->find_next(0); 
                elem != possible_elems->npos; 
                elem = possible_elems->find_next(elem)) {
                        
            if (elem <= p->get_num_prim_elems()) {
                // Primitive element
                
                // Does it newly occur?
                if (reuse_prim_elems
                    && !next_layer->is_prim_elem_new(elem, pos))
                    continue;
                
                // Yes -- add an implication that this element is primitive
                add(-next_layer->lit_at_pos(elem, pos));
                add(next_layer->lit_at_pos(prim_var, pos));
                end();
                
            } else {
                // Composite element
                
                // Add an implication that this element is non-primitive
                add(-next_layer->lit_at_pos(elem, pos));
                add(-next_layer->lit_at_pos(prim_var, pos));
                end();
            }
        }
    }
}

void SolverInterface::add_amo_elem(bool prim_elems)
{
    int num_quadratic_amo = 0;
    int num_binary_amo = 0;
    
    // For each position
    for (int pos = 0; pos < next_layer->get_array_size(); pos++) {
        
        // Which primitive elements can occur there?
        std::vector<int> possible_elems;
        int lower = (prim_elems ? 0 : p->get_num_prim_elems());
        int upper = (prim_elems ? p->get_num_prim_elems() : p->get_num_elems());
        for (int elem = lower; elem < upper; elem++) {
            int elem_var = elem+1;
            
            // Add each element if it can occur
            if (next_layer->is_elem_possible(elem_var, pos))
                possible_elems.push_back(next_layer->lit_at_pos(elem_var, pos));
        }
        
        // Which AMO encoding should be done?
        if (possible_elems.size() < params->threshold_binary_amo) {
            // Do a quadratic AMO encoding
            add_quadratic_amo(&possible_elems);
            num_quadratic_amo++;
        } else {
            // Do a binary AMO encoding
            add_binary_amo(&possible_elems);
            num_binary_amo++;
        }
    }
    
    if (num_quadratic_amo + num_binary_amo > 0) {
        double quadratic_amo_ratio = ((double) num_quadratic_amo) / (num_quadratic_amo + num_binary_amo);
        log(str("%.1f%% quadratic At-Most-One encodings, the rest being binary encoded.", quadratic_amo_ratio * 100.0));
    }
}

void SolverInterface::add_amo_new_prim_elem() {
    
    int last_pos = 0;
    for (int pos = 0; pos < next_layer->get_array_size(); pos++) {
        
        // Increment the last position index as necessary
        if (next_layer->get_depth() > 0 && last_pos+1 < prev_layer->get_array_size() 
            && prev_layer->get_next_position(last_pos+1) >= pos)
            last_pos++;
        
        std::vector<int> amo_elems = std::vector<int>();
        
        // Add each newly occuring primitive element to the variable set
        for (int prim_elem = 1; prim_elem <= p->get_num_prim_elems(); prim_elem++) {
            if (next_layer->is_elem_possible(prim_elem, pos) 
                && next_layer->is_prim_elem_new(prim_elem, pos))
                amo_elems.push_back(next_layer->lit_at_pos(prim_elem, pos));
        }
        
        if (amo_elems.size() == 0)
            continue;
        
        if (params->cumulative_amo) {
            
            extend_binary_amo(&amo_elems, pos);
            
        } else {
            
            // Add the "isPrimitive" variable from the previous depth
            // to the set of variables to constrain
            if (next_layer->get_depth() > 0 && prev_layer->get_next_position(last_pos) == pos) {
                int primitive_last_depth = prev_layer->lit_at_pos(p->get_primitivity_var(), last_pos);
                amo_elems.push_back(primitive_last_depth);
            }
            
            // Which AMO encoding should be done?
            if (amo_elems.size() < params->threshold_binary_amo) {
                // Do a quadratic AMO encoding
                add_quadratic_amo(&amo_elems);
            } else {
                // Do a binary AMO encoding
                add_binary_amo(&amo_elems);
            }
        }
        
    }
}

void SolverInterface::add_quadratic_amo(std::vector<int>* vars)
{
    // Quadratic At-Most-One over all possible prim. elems
    for (int i = 0; i < vars->size(); i++) {
        int elem_var_1 = vars->at(i);
        for (int j = i+1; j < vars->size(); j++) {
            int elem_var_2 = vars->at(j);
            add(-elem_var_1);
            add(-elem_var_2);
            end();
        }
    }
}

void SolverInterface::add_binary_amo(std::vector<int>* vars)
{
    // How many variables?
    int num_vars = vars->size();
    if (num_vars <= 1)
        return;
    
    // Calculate necessary bitlength
    uint bitlength = 0;
    int n = num_vars;
    do {
        ++bitlength; 
        n /= 2;
    } while(n);
    
    // Allocate helper variables
    int binary_var_start = next_layer->allocate_helper_variables(bitlength);
    
    // For each variable
    for (uint i = 0; i < vars->size(); i++) {
        int var = vars->at(i);
        
        // For each bit of the variable's index in the set
        uint remainder = i;
        for (int bit_pos = 0; bit_pos < bitlength; bit_pos++) {
            
            // The variable implies the bit
            add(-var);
            add((remainder % 2 == 1 ? 1 : -1) * (binary_var_start + bit_pos));
            end();
            
            remainder /= 2;
        }
    }
}

void SolverInterface::extend_binary_amo(std::vector<int>* vars, int position)
{
    int bit_vars_start = next_layer->get_amo_helper_vars(position);
    int bits = p->get_prim_elems_bitlength();
    int first_number = next_layer->get_amo_first_unencoded_number(position);
    
    // For each variable
    for (uint i = 0; i < vars->size(); i++) {
        int var = vars->at(i);
        
        // For each bit of the variable's index in the set
        uint remainder = first_number + i;
        for (int bit_pos = 0; bit_pos < bits; bit_pos++) {
            
            // The variable implies the bit
            add(-var);
            add((remainder % 2 == 1 ? 1 : -1) * (bit_vars_start + bit_pos));
            end();
            
            remainder /= 2;
        }
    }
    
    //std::cout << position << " : (" << bit_vars_start << ") " << first_number << " -> " << first_number + vars->size() << std::endl;
    next_layer->set_amo_first_unencoded_number(position, first_number+vars->size());
}

void SolverInterface::add_preconditions_and_effects()
{
    // For each position
    for (int pos = 0; pos < next_layer->get_array_size(); pos++) {
        
        // For each NEW primitive element
        for (int prim_elem = 1; prim_elem <= p->get_num_prim_elems(); prim_elem++) {
            if (!next_layer->is_elem_possible(prim_elem, pos))
                continue;
            if (!params->full_prim_elem_encoding 
                && !next_layer->is_prim_elem_new(prim_elem, pos))
                continue;
            
            // Add preconditions
            std::set<int>* preconds = p->get_preconditions_of_elem(prim_elem-1);
            std::set<int> pres;
            for (auto it2 = preconds->begin(); it2 != preconds->end(); ++it2) {
                
                int fact = *it2;
                bool positive = fact>0;
                int fact_var = p->fact_idx_to_var(std::abs(fact));
                // if (pres.count(fact_var)) abort();
                pres.insert(fact_var);
                
                // The position where the precondition must be true
                int pos_precond = next_layer->get_last_fact_occurence_lower_equals(pos, std::abs(fact));
                
                add(-next_layer->lit_at_pos(prim_elem, pos));
                add((positive ? 1 : -1) * next_layer->lit_at_pos(fact_var, pos_precond));
                end();
            }
            
            // Do not add effects at last array position
            if (pos+1 >= next_layer->get_array_size()) {
                continue;
            }
            
            // Add effects
            std::set<int>* effects = p->get_effects_of_prim_elem(prim_elem-1);
            // ... first positives
            std::set<int> pos_effects;
            for (auto it2 = effects->begin(); it2 != effects->end(); ++it2) {
                int fact = *it2;
                bool positive = fact>0;
                if (!positive) continue;
                int fact_var = p->fact_idx_to_var(std::abs(fact));
                pos_effects.insert(fact_var);
                add(-next_layer->lit_at_pos(prim_elem, pos));
                add((positive ? 1 : -1) * next_layer->lit_at_pos(fact_var, pos+1));
                end();
            }
            // ... then negatives, skipping the ones already added in positive form
            for (auto it2 = effects->begin(); it2 != effects->end(); ++it2) {
                int fact = *it2;
                bool positive = fact>0;
                if (positive) continue;
                int fact_var = p->fact_idx_to_var(std::abs(fact));
                if (pos_effects.count(fact_var)) continue;
                add(-next_layer->lit_at_pos(prim_elem, pos));
                add((positive ? 1 : -1) * next_layer->lit_at_pos(fact_var, pos+1));
                end();
            }
        }
        
        // For each COMPOSITE element
        for (int i = p->get_num_prim_elems(); i < p->get_num_elems(); i++) {
            int elem = i+1;
            
            // Can the element occur there?
            if (!next_layer->is_elem_possible(elem, pos))
                continue;
            
            // Yes -- add its preconditions
            std::set<int>* preconds = p->get_preconditions_of_elem(i);
            std::set<int> pres;
            std::set<int>::iterator it;
            for (it = preconds->begin(); it != preconds->end(); ++it) {
                
                int fact = *it;
                bool positive = fact>0;
                int fact_var = p->fact_idx_to_var(std::abs(fact));
                //if (pres.count(-fact)) abort();
                pres.insert(fact);
                
                // The position where the precondition must be true
                int pos_precond = next_layer->get_last_fact_occurence_lower_equals(pos, std::abs(fact));
                
                add(-next_layer->lit_at_pos(elem, pos));
                add((positive ? 1 : -1) * next_layer->lit_at_pos(fact_var, pos_precond));
                end();
            }
        }
    }
}

void SolverInterface::add_frame_axioms(bool full_comp_elem_encoding)
{
    bool reuse_prim_elems = !params->full_prim_elem_encoding;
    
    // Variable expressing the "is_primitive" predicate
    int prim_var = p->get_primitivity_var();
    
    // For each fact
    for (int fact = 1; fact <= p->get_num_facts(); fact++) {
        
        int fact_var = p->fact_idx_to_var(fact);
        
        int pos_before = 0;
        int pos_after = 0;
        
        while (pos_after+1 < next_layer->get_array_size()) {
            
            pos_before = pos_after;
            pos_after = pos_before + 1;
            while (!next_layer->is_fact_possible(fact, pos_after))
                pos_after++;
            int pos_change = pos_after-1;
            
            if (!next_layer->is_fact_possible(fact, pos_before) 
                || !next_layer->is_fact_possible(fact, pos_after)) {
                exit(3);
            }
            
            bool variants[] = {true, false};
            
            for (int variant_idx = 0; variant_idx < 2; variant_idx++) {
                bool positive = variants[variant_idx];
                
                // A change of the fact from negative to positive ...
                add((positive ? 1 : -1) * next_layer->lit_at_pos(fact_var, pos_before));
                add((positive ? 1 : -1) * -next_layer->lit_at_pos(fact_var, pos_after));
                
                // ... is possible if the element is composite ...
                if (!full_comp_elem_encoding) {
                    add(-next_layer->lit_at_pos(prim_var, pos_change));
                } else {
                    std::vector<boost::dynamic_bitset<>>* facts_changed_by_comp_elems = p->get_facts_changed_by_comp_elems();
                    for (int comp_elem = 0; comp_elem < facts_changed_by_comp_elems->at(fact).size(); comp_elem++) {
                        if (facts_changed_by_comp_elems->at(fact)[comp_elem]) {
                            int comp_elem_var = comp_elem + 1 + p->get_num_prim_elems();
                            if (next_layer->is_elem_possible(comp_elem_var, pos_change))
                                add(next_layer->lit_at_pos(comp_elem_var, pos_change));
                        }
                    }
                }
                
                int prev_pos = -1;
                if (next_layer->get_depth() > 0)
                    prev_pos = next_layer->get_last_position(pos_change);
                bool prim_elems_before = false;
                
                // ... or if a supporting primitive element is present
                std::set<int>* supporting_prim_elems = p->get_supporting_prim_elems_of_fact(fact, positive);
                for (auto it = supporting_prim_elems->begin(); 
                     it != supporting_prim_elems->end(); ++it) {
                    int elem = *it;
                    int elem_var = elem + 1;
                    if (next_layer->is_elem_possible(elem_var, pos_change)) {
                        add(next_layer->lit_at_pos(elem_var, pos_change));
                        
                        if (reuse_prim_elems
                            && next_layer->get_depth() > 0 
                            && prev_layer->is_elem_possible(elem_var, prev_pos)) {
                            prim_elems_before = true;
                        
                            // Also add any overwritten variables
                            // corresponding to this primitive element
                            std::vector<int>* old_elem_vars = next_layer->get_overwritten_variables(pos_change, elem_var);
                            for (int i = 0; i < old_elem_vars->size(); i++) {
                                add(old_elem_vars->at(i));
                            }
                        }
                    }
                }
                
                end();
            }
        }
    }
}

void SolverInterface::add_reduction_alternatives()
{
    for (int pos = 0; pos < next_layer->get_array_size(); pos++) {
        
        for (int comp_elem = 0; comp_elem < p->get_num_comp_elems(); comp_elem++) {
            int elem_var = comp_elem + p->get_num_prim_elems() + 1;
            
            if (!next_layer->is_elem_possible(elem_var, pos))
                continue;
            
            int num_reductions = p->get_alt_successors_of_comp_elem(comp_elem)->size();
            add(-next_layer->lit_at_pos(elem_var, pos));
            for (int r = 0; r < num_reductions; r++) {
                add(next_layer->lit_at_pos(p->get_first_reduction_var()+r, pos));
            }
            end();
        }
    }
}

void SolverInterface::add_reduction_conditions()
{
    int num_conditions = 0;
    for (int pos = 0; pos < next_layer->get_array_size(); pos++) {
        
        auto conditions = p->get_reduction_conditions();
        
        for (int c = 0; c < conditions->size(); c++) {
            
            IncProblem::reduction_condition* cond = &conditions->at(c);
            int r = cond->red_idx;
            int fact = cond->fact_condition;
            int elem_var = cond->comp_elem + p->get_num_prim_elems() + 1;
            int pos_fact = next_layer->get_last_fact_occurence_lower_equals(pos, std::abs(fact));
            
            if (!next_layer->is_elem_possible(elem_var, pos))
                continue;
            
            add(-next_layer->lit_at_pos(elem_var, pos));
            add(-next_layer->lit_at_pos(p->get_first_reduction_var()+r, pos));
            add((fact > 0 ? 1 : -1) * next_layer->lit_at_pos(
                p->fact_idx_to_var(std::abs(fact)), pos_fact));
            end();
            
            num_conditions++;
        }
    }
    
    log(str("Added %i reduction conditions.", num_conditions));
}

void SolverInterface::add_blank_transitions()
{
    bool reuse_prim_elems = !params->full_prim_elem_encoding;
    
    // For each position
    for (int pos = 0; pos < prev_layer->get_array_size(); pos++) {
        int next_start_pos = prev_layer->get_next_position(pos);
        
        // Is the maximal expansion at this position non-trivial?
        int max_expansion = prev_layer->get_next_position(pos+1) - next_start_pos;
        if (max_expansion <= 1)
            continue;
 
        // For each element
        for (int i = 0; i < p->get_num_elems(); i++) {
            int elem = 1 + i;
            
            // Can the element occur there?
            if (prev_layer->is_elem_possible(elem, pos)) {
                
                // Calculate the expansion size of the element
                int exp_size = 0;
                if (p->is_var_prim_elem(elem)) {
                    
                    // Only add these clauses for NEW primitive elements
                    if (reuse_prim_elems
                        && !prev_layer->is_prim_elem_new(elem, pos))
                        continue;
                    
                    exp_size = 1;
                    
                } else if (!params->reduction_choices) {
                    exp_size = p->get_successors_of_comp_elem(i - p->get_num_prim_elems())->size();
                }
                
                if (!params->reduction_choices || p->is_var_prim_elem(elem)) {
                    
                    // Are there positions to be "blank"ed?
                    if (exp_size < max_expansion) {
                        // Define each undefined position with a blank
                        int lit = -prev_layer->lit_at_pos(elem, pos);
                        for (int offset = exp_size; offset < max_expansion; offset++) {
                            add(lit);
                            add(next_layer->lit_at_pos(LIT_BLANK_ELEMENT, next_start_pos+offset));
                            end();
                        }
                    }
                    
                } else {
                    // Composite element with reduction choices
                    std::vector<reduction>* reductions = p->get_alt_successors_of_comp_elem(i - p->get_num_prim_elems());
                    for (int r = 0; r < reductions->size(); r++) {
                        
                        int exp_size = reductions->at(r).size();
                        if (exp_size < max_expansion) {
                            
                            // Define each undefined position with a blank
                            int lit_elem = -prev_layer->lit_at_pos(elem, pos);
                            int lit_red = -prev_layer->lit_at_pos(p->get_first_reduction_var()+r, pos);
                            for (int offset = exp_size; offset < max_expansion; offset++) {
                                add(lit_elem);
                                add(lit_red);
                                add(next_layer->lit_at_pos(LIT_BLANK_ELEMENT, next_start_pos+offset));
                                end();
                            }
                        }
                    }
                }
            }
        }
    }
}

void SolverInterface::add_predecessor_rules()
{
    const bool reuse_prim_elems = !params->full_prim_elem_encoding;
    
    // For each position at the last depth
    for (int pos = 0; pos < prev_layer->get_array_size(); pos++) {
        int next_start_pos = prev_layer->get_next_position(pos);
        
        // For each next position which is defined by the last position
        for (int next_pos = next_start_pos; next_pos < prev_layer->get_next_position(pos+1); next_pos++) {
        
            // What is the offset between parent and child element, concerning the indices?
            int offset = next_pos - next_start_pos;
            
            // For each possible element at the next depth (besides "blank")
            auto possible_elems = next_layer->get_possible_elems(next_pos);
            for (int elem = possible_elems->find_next(1); 
                    elem != possible_elems->npos;
                    elem = possible_elems->find_next(elem)) {
                int elem_idx = elem - 1;
                
                // Only consider primitive element if it is newly occuring
                if (reuse_prim_elems
                    && p->is_var_prim_elem(elem) 
                    && !next_layer->is_prim_elem_new(elem, next_pos))
                    continue;
                    
                // Sanity check
                if (offset >= p->get_predecessors_of_elem(elem_idx)->size())
                    continue;
                
                // If the element is at that position at the next depth ...
                add(-next_layer->lit_at_pos(elem, next_pos));
                
                if (offset == 0 && p->is_var_prim_elem(elem) && prev_layer->is_elem_possible(elem, pos)) {
                    // ... then it might have been already there at the previous depth
                    // (only if it is a primitive element and if the indices align)
                    add(prev_layer->lit_at_pos(elem, pos));
                }
                
                // ... or one of its predecessors must be at the previous position and depth
                std::vector<int>* predecessors = &(p->get_predecessors_of_elem(elem_idx)->at(offset));
                for (auto it = predecessors->begin(); it != predecessors->end(); ++it) {
                    int parent_comp_elem_idx = *it;
                    int parent_elem = parent_comp_elem_idx + 1 + p->get_num_prim_elems();
                    if (prev_layer->is_elem_possible(parent_elem, pos))
                        add(prev_layer->lit_at_pos(parent_elem, pos));
                }
                
                end();
            }
            
            if (params->blank_elem_predecessors_specification) {
                
                // Special treatment for possible "blank" origins
                int blank_elem = 1;
                if (next_layer->is_elem_possible(blank_elem, next_pos) 
                    && (!reuse_prim_elems || next_layer->is_prim_elem_new(blank_elem, next_pos))) {
                
                    // If there is a blank at the next depth ...
                    add(-next_layer->lit_at_pos(blank_elem, next_pos));
                    
                    // ... then it might have been at the "parent" position
                    // at the previous depth
                    if (prev_layer->is_elem_possible(blank_elem, pos)) {
                        add(prev_layer->lit_at_pos(blank_elem, pos));
                    }
                    
                    if (offset > 0) {
                        // ... or some primitive element (possibly blank) 
                        // at the previous depth caused the blank transition ...
                        for (int prim_elem_idx = 0; prim_elem_idx < p->get_num_prim_elems(); prim_elem_idx++) {
                            int prim_elem = prim_elem_idx + 1;
                            if (prev_layer->is_elem_possible(prim_elem, pos)) {
                                add(prev_layer->lit_at_pos(prim_elem, pos));
                            }
                        }
                        // ... or some composite element with a smaller expansion size 
                        // than the current offset caused the blank transition
                        for (int comp_elem_idx = 0; comp_elem_idx < p->get_num_comp_elems(); comp_elem_idx++) {
                            int comp_elem = comp_elem_idx + 1 + p->get_num_prim_elems();
                            if (!prev_layer->is_elem_possible(comp_elem, pos))
                                continue;
                            
                            if (p->get_successors_of_comp_elem(comp_elem_idx)->size() <= offset) {
                                add(prev_layer->lit_at_pos(comp_elem, pos));
                            }
                        }
                    }
                    end();
                }
            }
        }
    }
}

void SolverInterface::add_goal_conditions(bool add_permanently)
{
    formula* f_goal = p->f(IncProblem::MODE_GOAL);
    // For each goal clause
    for (int c = 0; c < f_goal->num_clauses; c++) {
        int* clause = f_goal->clauses[c];
        int clause_size = f_goal->clause_sizes[c];
        // For each literal
        for (int i = 0; i+1 < clause_size; i += 2) {
            int lit = clause[i];
            int pos = clause[i+1];
            if (pos == -1) {
                // FOR-ALL annotation
                // Add the literal for all positions
                for (int p = 0; p < next_layer->get_array_size(); p++) {
                    if (add_permanently) {
                        add(next_layer->lit_at_pos(lit, p));
                        end();
                    } else {
                        assume(next_layer->lit_at_pos(lit, p));
                    }
                }
            } else {
                if (add_permanently) {
                    add(next_layer->lit_at_pos(lit, pos));
                    end();
                } else {
                    assume(next_layer->lit_at_pos(lit, pos));
                }
            }
        }
    }
}

void SolverInterface::process_result()
{
    bool reuse_prim_elems = !params->full_prim_elem_encoding;
    
    results_of_final_depth = std::vector<int>();
    
    // If present, retrieve the "improper" element 
    // not contributing to the plan length
    bool check_improper_elem = p->has_improper_element();
    int improper_elem = p->get_improper_element();
    
    // For each position
    for (int pos = 0; pos < next_layer->get_array_size(); pos++) {
        bool elem_found = false;
        
        // Search for some occuring element
        for (int i = 1; i < p->get_num_prim_elems() && !elem_found; i++) {
            
            int elem = i+1;
            
            // Is the element an "improper" element, 
            // i.e. does not contribute to the plan length?
            if (check_improper_elem && elem == improper_elem)
                continue;
            
            // Can the element occur there?
            if (!next_layer->is_elem_possible(elem, pos))
                continue;
            
            // Literal representing the element
            int literal = next_layer->lit_at_pos(elem, pos);
            if (literal != 0) {
                
                // Query value assigned to this variable
                int val = ipasir_val(solver, literal);
                if (val > 0) {
                    
                    // The value is relevant and it is positive
                    results_of_final_depth.push_back(elem);
                    elem_found = true;
                    
                } else if (reuse_prim_elems) {
             
                    std::vector<int>* overwritten_vars = next_layer->get_overwritten_variables(pos, elem);
                    for (int i = 0; i < overwritten_vars->size(); i++) {
                        int literal = overwritten_vars->at(i);
                        if (literal != 0) {
                            
                            // Query value assigned to this variable
                            int val = ipasir_val(solver, literal);
                            if (val > 0) {
                                
                                // The value is relevant and it is positive
                                results_of_final_depth.push_back(elem);
                                elem_found = true;
                            }
                        }
                    }                
                }
            }
        }
    }
}

void SolverInterface::print_assigned_vars_per_depth()
{
    int depth = next_layer->get_depth();
    // For each position
    for (int pos = 0; pos < next_layer->get_array_size(); pos++) {
        std::cout << pos << " : ";
        for (int elem_idx = 0; elem_idx < p->get_num_elems(); elem_idx++) {
            int elem = elem_idx+1;
            if (!next_layer->is_elem_possible(elem, pos)) 
                continue;
            
            int val = ipasir_val(solver, next_layer->lit_at_pos(elem, pos));
            if (val > 0)
                std::cout << elem << " ";
        }
        std::cout << std::endl;
    }
}

int SolverInterface::add_blank_counter()
{
    int var_zero_counter = next_layer->allocate_helper_variables(1);
    add(var_zero_counter);
    end();
    
    int var_counter_before = var_zero_counter;
    
    // For each position
    for (int pos = 0; pos < next_layer->get_array_size(); pos++) {
        
        std::vector<int> uncounted_elems = std::vector<int>();
        if (next_layer->is_elem_possible(LIT_BLANK_ELEMENT, pos)) {
            uncounted_elems.push_back(next_layer->lit_at_pos(LIT_BLANK_ELEMENT, pos));
            
            if (!params->full_prim_elem_encoding) {
                std::vector<int>* overwritten_vars = next_layer->get_overwritten_variables(pos, LIT_BLANK_ELEMENT);
                for (int i = 0; i < overwritten_vars->size(); i++) {
                    uncounted_elems.push_back(overwritten_vars->at(i));
                }
            }
        }
        if (p->has_improper_element() 
            && next_layer->is_elem_possible(p->get_improper_element(), pos)) {
            uncounted_elems.push_back(next_layer->lit_at_pos(p->get_improper_element(), pos));
        
            if (!params->full_prim_elem_encoding) {
                std::vector<int>* overwritten_vars = next_layer->get_overwritten_variables(pos, p->get_improper_element());
                for (int i = 0; i < overwritten_vars->size(); i++) {
                    uncounted_elems.push_back(overwritten_vars->at(i));
                }
            }
        }
        
        int var_counter_now = next_layer->allocate_helper_variables(pos+2);
        for (int i = 0; i <= pos; i++) {
            int var_counter_before_is_i = var_counter_before + i;
            int var_counter_now_is_i = var_counter_now + i;
            int var_counter_now_is_i_plus_one = var_counter_now + i + 1;
            
            // The counter value at the next position will always be
            // at least as large as the previous value
            add(-var_counter_before_is_i);
            add(var_counter_now_is_i);
            end();
            
            // If there is no "blank" and no "nop" present,
            // the counter will increment
            add(-var_counter_before_is_i);
            for (int i = 0; i < uncounted_elems.size(); i++) {
                add(uncounted_elems[i]);
            }
            add(var_counter_now_is_i_plus_one);
            end();
            
            /*
            // The counter value does not increase by more than one
            add(-var_counter_now_is_i_plus_one);
            add(var_counter_before_is_i);
            end();
            
            for (int i = 0; i < uncounted_elems.size(); i++) {
                add(-var_counter_now_is_i);
                add(-uncounted_elems[i]);
                add(var_counter_before_is_i);
                end();
            }*/
        }
        var_counter_before = var_counter_now;
    }
    
    return var_counter_before;
}

void SolverInterface::optimize_result(std::function<void(SolverInterface*, bool)> callback)
{   
    check_terminate();
    
    int depth = next_layer->get_depth();
    
    // Bounds for search
    int lower_bound = 1;
    int upper_bound = results_of_final_depth.size();
    int forbidden_plan_length = upper_bound + 1;
    if (params->optimization_mode == OPTIMIZATION_MODE_LINEAR_INCREASING)
        forbidden_plan_length = lower_bound;
    
    // The minimal forbidden length which leads to a "SAT" result
    int min_valid_forbidden_length = upper_bound + 1;
    
    log(str("The initially found plan has an effective length of %i.", upper_bound));
    log(str("PLO BEGIN %i", upper_bound));
    
    // Add goal conditions permanently
    // (such that they don't need to be assumed for each call)
    clause_stats stats = start_clause_stats();
    add_goal_conditions(true);
    end_clause_stats(&stats);
    log(str("Added %s permanent goal conditions.", large_int_str(stats.added_clauses)));
    
    // Add clauses counting the plan length,
    // and retrieve the first of the final counter variables
    stats = start_clause_stats();
    int var_counter_at_last_pos = add_blank_counter();
    end_clause_stats(&stats);
    log(str("Added %s clauses counting the effective plan length.", large_int_str(stats.added_clauses)));
    
    bool alternate_bit = false;
    while (true) {
        
        check_terminate();
        
        // Next plan length to forbid
        int new_plan_length;
        if (params->optimization_mode == OPTIMIZATION_MODE_BINARY) {
            double interpolation_param = params->optimization_skewness;
            if (OPTIMIZATION_BINARY_ALTERNATE_SKEW && alternate_bit) {
                interpolation_param = 1 - interpolation_param;
            }
            new_plan_length = std::floor((1-interpolation_param) * lower_bound + interpolation_param * upper_bound);
        } else if (params->optimization_mode == OPTIMIZATION_MODE_LINEAR_DECREASING) {
            new_plan_length = std::max(lower_bound, std::min(upper_bound, forbidden_plan_length + params->optimization_stepsize));
        } else {
            new_plan_length = std::min(upper_bound, std::max(lower_bound, forbidden_plan_length + params->optimization_stepsize));
        }
        if (new_plan_length == forbidden_plan_length)
            // No change in the length to consider -> search has finished
            break;
        forbidden_plan_length = new_plan_length;
        
        // Assume that the final plan length may not be of the forbidden length
        assume(-(var_counter_at_last_pos + forbidden_plan_length));
        
        // Attempt solve
        log(str("Attempting to find a plan shorter than %i.", forbidden_plan_length));
        int result = ipasir_solve(solver);
        if (result == RESULT_SAT) {
            
            // Read satisfying variable values and report new plan
            process_result();
            log(str("Found a plan. New actual length: %i.", (int) results_of_final_depth.size()));
            log(str("PLO UPDATE %i", (int)results_of_final_depth.size()));
            callback(this, /*print_to_stdout=*/false);
            
            // Re-set upper bound
            upper_bound = results_of_final_depth.size(); //forbidden_plan_length-1;
            min_valid_forbidden_length = results_of_final_depth.size() + 1; //forbidden_plan_length;
            
        } else {
            
            // Re-set lower bound
            log("No such plan exists.");
            lower_bound = forbidden_plan_length+1;
        }
        
        if (lower_bound > upper_bound) {
            break;
        }
        alternate_bit = !alternate_bit;
    }
    
    log(str("Final plan of length %i identified.", (int) results_of_final_depth.size()));    
    log(str("PLO END %i", (int)results_of_final_depth.size()));
}

void SolverInterface::add(int lit) 
{
    ipasir_add(solver, lit);
    //current_clause.push_back(lit);
    num_added_literals++;
    
    if (params->output_graph) {
        renderer->add_to_edges(lit);
    }
}

void SolverInterface::end()
{
    ipasir_add(solver, 0);
    num_added_clauses++;
    //printf("CNF ");
    //for (int lit : current_clause) printf("%i ", lit);
    //printf("0\n");
    //current_clause.clear();
    
    if (params->output_graph)
        renderer->add_to_edges(0);
}

void SolverInterface::assume(int lit) 
{
    ipasir_assume(solver, lit);
    //printf("CNF_ASSUME %i\n", lit);
}

std::vector<int>* SolverInterface::get_relevant_results()
{
    return &results_of_final_depth;
}

void SolverInterface::log_makespan(int makespan)
{
    log("");
    log(    "*************************************");
    log(str("* * *   M a k e s p a n   %3i   * * *", makespan));
    log(    "*************************************");
}

void SolverInterface::log_result(int result)
{
    log(str("Executed solver; result: %s.", 
            (result == RESULT_SAT ? "SAT" : 
            (result == RESULT_UNSAT ? "UNSAT" : "UNKNOWN"))
        ));
}

SolverInterface::clause_stats SolverInterface::start_clause_stats()
{
    start_round_timer();
    clause_stats stats;
    stats.added_clauses = num_added_clauses;
    stats.added_literals = num_added_literals;
    return stats;
}

void SolverInterface::end_clause_stats(clause_stats* stats)
{
    stats->added_clauses = num_added_clauses - stats->added_clauses;
    stats->added_literals = num_added_literals - stats->added_literals;
    long int duration = elapsed_round_nanosecs();
    if (stats->added_clauses > 0)
        stats->time_per_clause = duration / stats->added_clauses;
    else
        stats->time_per_clause = 0;
}

bool SolverInterface::has_found_solution()
{
    return found_solution;
}
