#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <iterator>
#include <algorithm>
#include <ctype.h>
#include <csignal>
#include <boost/program_options.hpp>

#include "inc_problem.h"
#include "solver_interface.h"
#include "logger.h"
#include "ipasir.h"
#include "params.h"

namespace po = boost::program_options;

// Fields for the processing of the encoding file
IncProblem problem;
int mode;
int clause_idx;

// Necessary for handling of interruption
SolverInterface* solver = NULL;
void output_solution(SolverInterface* solver, bool print_to_stdout);

void signalHandler( int signum ) {
   std::cout << "Interrupt signal (" << signum << ") received.\n";
   output_solution(solver, true);
   log("Exiting.");
   exit(signum);  
}

/**
 * Splits a string into a vector of strings,
 * using whitespaces as delimiters.
 */
std::vector<std::string> split(std::string const &input) 
{ 
    std::istringstream buffer(input);
    std::vector<std::string> ret((std::istream_iterator<std::string>(buffer)), 
                                 std::istream_iterator<std::string>());
    return ret;
}

bool insert_clause(int* clause, int clause_size)
{
    formula* f_current = problem.f(mode);
    
    if (mode == IncProblem::MODE_INIT || mode == IncProblem::MODE_GOAL) {
        return true;
    }
    
    // TODO what to do with unit clauses?
    if (clause_size == 2) {
        return true;
    }
    
    int lit1 = clause[0];
    int annot1 = clause[1];
    int lit2 = clause[2];
    int annot2 = clause[3];
    
    if (mode == IncProblem::MODE_UNIV) {
        // Universal clause
        
        if (lit1 < 0 && annot1 == 0 && problem.is_var_elem(-lit1)) {
            // Implication of an element
            
            if (problem.is_var_fact(std::abs(lit2)) && clause_size == 4) {
                // Precondition or effect
                
                if (annot2 == 0) {
                    // Precondition
                    
                    problem.add_precondition_of_elem(
                        (lit2>0 ? 1 : -1) * problem.fact_var_to_idx(std::abs(lit2)),
                        problem.elem_var_to_idx(-lit1)
                    );
                    return false;
                    
                } else if (annot2 == 1) {
                    
                    // Effect
                    problem.add_effect_of_prim_elem(
                        (lit2>0 ? 1 : -1) * problem.fact_var_to_idx(std::abs(lit2)),
                        problem.prim_elem_var_to_idx(-lit1)
                    );
                    
                    // Add elem to elems supporting the fact
                    if (lit2 > 0) {
                        problem.add_supporting_prim_elem_of_pos_fact(
                            problem.prim_elem_var_to_idx(-lit1),
                            problem.fact_var_to_idx(lit2)
                        );
                    } else {
                        problem.add_supporting_prim_elem_of_neg_fact(
                            problem.prim_elem_var_to_idx(-lit1),
                            problem.fact_var_to_idx(-lit2)
                        );
                    }
                    
                    return false;
                }
                
            } else if (lit2 < 0 && annot2 == 0 && problem.is_var_reduction_idx(-lit2)) {
                // Implication of some task's reduction
                
                int lit3 = clause[4];
                int annot3 = clause[5];
                
                if (clause_size == 6 && problem.is_var_fact(std::abs(lit3)) && annot3 == 0) {
                    // Reduction condition
                    
                    problem.add_reduction_condition(
                        problem.comp_elem_var_to_idx(-lit1), 
                        problem.reduction_var_to_idx(-lit2),
                        (lit3 > 0 ? 1 : -1) * problem.fact_var_to_idx(std::abs(lit3))
                    );
                    return false;
                }
                
            } else if (lit2 > 0 && annot2 == 0 && problem.is_var_reduction_idx(lit2)) {
                // Statement of reduction choices
                // no necessity to add anything 
                // (information already implied by successor specification)
                return false;
            }
        }
        
    } else if (mode == IncProblem::MODE_TRAN) {
        // Transitional clause
        
        if (lit1 < 0 && annot1 == 0 && problem.is_var_comp_elem(-lit1)) {
            // Implication of composite element
            
            // Is the clause a successor specification?
            int num_vars = f_current->num_vars;
            bool successor_clause = true;
            int eff_clause_size = clause_size / 2;
            int succ_offset = -1;
            int lit_idx = 1;
            if (problem.get_num_reductions() > 0) {
                successor_clause &= lit2 < 0 && problem.is_var_reduction_idx(-lit2) && annot2 == 0;
                lit_idx++;
            }
            for (; lit_idx < eff_clause_size; lit_idx++) {
                int lit = clause[2*lit_idx];
                int offset = clause[2*lit_idx+1];
                if (succ_offset == -1)
                    succ_offset = offset;
                successor_clause &= lit > num_vars;
                successor_clause &= problem.is_var_elem(lit - num_vars);
                successor_clause &= offset == succ_offset;
            }
            
            if (successor_clause) {
                // -- yes, it is
                
                // Clause describing the successors of an element
                int comp_elem = problem.comp_elem_var_to_idx(-lit1);
                if (problem.get_num_reductions() > 0) {
                    // Alternative-reductions successor
                    int reduction_idx = problem.reduction_var_to_idx(-lit2);
                    for (int lit_idx = 2; lit_idx < eff_clause_size; lit_idx++) {
                        int lit = clause[2*lit_idx] - num_vars;
                        problem.add_successor(
                            comp_elem,
                            reduction_idx,
                            succ_offset,
                            problem.elem_var_to_idx(lit)
                        );
                    }
                } else {
                    // Single-reduction successor
                    for (int lit_idx = 1; lit_idx < eff_clause_size; lit_idx++) {
                        int lit = clause[2*lit_idx] - num_vars;
                        problem.add_successor(
                            comp_elem, 
                            succ_offset, 
                            problem.elem_var_to_idx(lit)
                        );
                    }
                }
                return false;
                
            } else if (clause_size == 6) {
                
                // Structure for an expansion condition: -elem_now -elem_next+offset ~fact_next+offset
                
                if (lit2 < 0 && -lit2 > num_vars && problem.is_var_elem(-lit2-num_vars)) {
                    // Condition for a conditional expansion?
                    int lit3 = clause[4];
                    int annot3 = clause[5];
                    
                    if (std::abs(lit3) > num_vars) {
                        int lit3_next_step = (lit3>0 ? 1 : -1) * (std::abs(lit3) - num_vars);
                        if (problem.is_var_fact(std::abs(lit3_next_step)) && annot2 == annot3) {
                        
                            problem.add_expansion_condition(-lit1, -lit2-num_vars, annot2, lit3_next_step);
                            return false;
                        }
                    }
                }
            }
        }
    }
    
    // Add the clause to the set of ordinary clauses
    return true;
}

/**
 * Reads one line of the encoding specifying one clause
 * of the encoding, and adds the clause to the correct
 * clause set.
 */
void read_clause(std::string line) 
{
    // Calculate amount of literals in the clause
    int num_lits = 0;
    for (int i = 0; i < line.size(); i++) {
        if (line[i] == ' ')
            num_lits++;
    }
    
    int clause_size = 2 * num_lits;
    int32_t* new_clause = new int32_t[clause_size];
    bool add_clause = true;
    int max_annotation_val = 0;

    int idx = 0;
    int lit_idx = 0;
    while (idx+1 < line.size()) {
        
        // Find next special character (anything besides [0-9] and -)
        int char_idx = idx;
        while (isdigit(line[char_idx]) || line[char_idx] == '-')
            char_idx++;
        char character = line[char_idx];
        
        // Find index where the literal ends
        int end_idx = idx;
        if (character == ' ') {
            end_idx = char_idx;
        } else {
            end_idx = char_idx+1;
            while (isalnum(line[end_idx]))
                end_idx++;
        }
        
        // Fill clause values according to found annotation
        if (character == '@' && line[char_idx+1] == 'A') {
                // Mark this literal / clause to be added for all positions
                new_clause[2*lit_idx] = std::stoi(line.substr(idx, char_idx));
                new_clause[2*lit_idx+1] = -1;
        } else if (character != ' ') {
                // There is some annotation
                new_clause[2*lit_idx] = std::stoi(line.substr(idx, char_idx));
                new_clause[2*lit_idx+1] = std::stoi(line.substr(char_idx+1, end_idx));
                max_annotation_val = std::max(max_annotation_val, new_clause[2*lit_idx+1]);
        } else {
            // Plain old literal without annotation
            new_clause[2*lit_idx] = std::stoi(line.substr(idx, end_idx));
            new_clause[2*lit_idx+1] = 0;
        }
        
        idx = end_idx + 1;
        lit_idx++;
    }
    
    // Should the clause be added to a clause set?
    add_clause = insert_clause(new_clause, clause_size);
    if (add_clause && (mode == IncProblem::MODE_UNIV || mode == IncProblem::MODE_TRAN)) {
        std::cout << "Error: unidentified clause" << std::endl;
        std::cout << line << std::endl;
        std::cout << "Exiting." << std::endl;
        exit(1);
    }
    
    // Has the clause been added?
    formula* f_current = problem.f(mode);
    if (add_clause) {
        f_current->clauses[clause_idx] = new_clause;
        f_current->clause_sizes[clause_idx] = clause_size;
        clause_idx++;
    } else {
        delete[] new_clause;
        f_current->num_clauses--;
    }
}

/**
 * Processes an encoding given by an input stream
 * from a text file.
 */
void process(/*std::ifstream& file*/)
{
    // Represents the formula that is currently being added to
    formula* f_current;
    mode = 0;
    clause_idx = 0;
    
    // formulae information to be extracted
    problem = IncProblem();
    
    std::string line;
    bool headcomments_finished = false;
    
    std::string filename = Params::get()->input_file; 
    bool read_from_file = filename != "";
    std::ifstream infile;
    if (read_from_file) {
        infile = std::ifstream(filename);
        log(str("Reading problem encoding from file \"%s\".", filename.c_str()));
    } else {
        log("Reading problem encoding from stdin.");
    }

    while (read_from_file ? std::getline(infile, line) 
                            : std::getline(std::cin, line)) {
        // Comment line
        if (line[0] == 'c') {
            
            if (line.size() <= 1)
                continue;
            
            if (line[1] == 'e') {
                // Element number definition
                std::vector<std::string> words = split(line);
                int num_prim_elems = std::stoi(words[1]);
                int num_comp_elems = std::stoi(words[2]);
                if (words.size() > 3) {
                    int num_reductions = std::stoi(words[3]);
                    problem.set_num_reductions(num_reductions);
                    Params::get()->reduction_choices = true;
                }
                problem.set_num_prim_elems(num_prim_elems);
                problem.set_num_comp_elems(num_comp_elems);
                
            } else if (line[1] == 'f') {
                // Fact number definition
                int num_facts = std::stoi(split(line)[1]);
                problem.set_num_facts(num_facts);
           
            } else if (line[1] == 'c') {
                /*
                // Changes of facts definition
                std::vector<std::string> words = split(str);
                int comp_elem = stoi(words[1]);
                comp_elem = problem.comp_elem_var_to_idx(comp_elem);
                for (int i = 2; i < words.size(); i++) {
                    int fact = stoi(words[i]);
                    fact = problem.fact_var_to_idx(fact);
                    problem.add_supporting_comp_elem_of_fact(comp_elem, fact);
                }*/
            
            } else if (line[1] == 'i') {
                // Improper elements definition
                std::vector<std::string> words = split(line);
                //problem.add_improper_element(stoi(words[1]));
            }
            
            continue;
        }
        
        if (!headcomments_finished) {
            log("Parsed head comment information.");
            headcomments_finished = true;
        }
        
        // Mode changes
        bool mode_change = false;
        if (line[0] == 'i') {
            mode_change = true;
            mode = IncProblem::MODE_INIT;
        } else if (line[0] == 'u') {
            mode_change = true;
            mode = IncProblem::MODE_UNIV;
        } else if (line[0] == 'g') {
            mode_change = true;
            mode = IncProblem::MODE_GOAL;
        } else if (line[0] == 't') {
            mode_change = true;
            mode = IncProblem::MODE_TRAN;
        }
        
        if (mode_change) {
            // Split line by whitespaces
            std::vector<std::string> words = split(line);
            
            // Initialize the formula of the new mode
            f_current = problem.f(mode);
            f_current->num_vars = std::stoi( words[2] );
            f_current->num_clauses = std::stoi( words[3] );
            f_current->clause_sizes = new int[f_current->num_clauses];
            f_current->clauses = new int*[f_current->num_clauses];
            clause_idx = 0;
        } else {
            // Read the clause represented by this line
            read_clause(line);
        }
    }
}

std::string solution_string(std::vector<int>* results)
{
    std::ostringstream str;
    for (uint i = 0; i < results->size(); i++) {
        str << results->at(i) << " ";
    }
    str << std::endl;
    return str.str();
}

void output_solution(SolverInterface* solver, bool print_to_stdout)
{
    if (solver == NULL || !solver->has_found_solution()) {
        return;
    }
    
    // Retrieve relevant results
    std::vector<int>* results = solver->get_relevant_results();
    std::cout << results->size() << std::endl;
    
    std::string solution_str = solution_string(results);
    std::ofstream out("SOLUTION.txt");
    out << "solution " << *max_element(std::begin(*results), std::end(*results)) << " " << "1\n";
    out << solution_str;
    
    if (print_to_stdout) {
        std::cout << "solution " << *max_element(std::begin(*results), std::end(*results)) << " " << "1\n";
        std::cout << solution_str;
    }
}

void parse_args(int argc, char* argv[]) 
{
    po::options_description desc("Options");
    desc.add_options()
        ("help,h", "Produce help message")
        
        ("input,i", po::value<std::string>(), 
                    "Read from file (default: read from stdin)")
        
        ("optimize-plan,o", 
                    "Include a retroactive plan length optimization stage"
                    " after an initial solution is found")
        ("optimization-linear,l", po::value<int>(), "Sets the optimization mode to linear "
            "and sets the stepsize per iteration (positive or negative for an ascending "
            "or descending procedure)")
        ("optimization-skew,s", po::value<double>(), "Sets the optimization mode to bisection "
            "and sets the skewness parameter 0 < s < 1)")
        
        ("timeout,t", po::value<long>(),
                    "Terminate the application after the provided amount"
                    " of milliseconds")
        ("output-graph,g", "Create a GDF graph file from the variables and clauses "
                    "(Warning: Only for small formulae!)")
        ("min-depth,m", po::value<int>(),
                    "Start solving attempts beginning from the specified depth index (>= 0)")
        ("max-depth,M", po::value<int>(), 
                    "Give up if no solution is found at the specified depth index (> 0)")
        
        ("encode-predecessors,p",
                    "Add clauses specifying the predecessors of each "
                    "possibly occuring element")
        ("encode-all-predecessors,P", 
                    "Also add predecessor clauses for `blank` elements "
                    "(implies the \"encode-predecessors\" option)")
        
        ("full-prim-elem-encoding,e", 
                    "Switch off reusage of action variables in the encoding")
        ("full-elem-encoding,E", 
                    "Switch off sparse element encoding (implies the -e,-F options)")
        ("full-fact-encoding,F", 
                    "Switch off sparse fact encoding")
        
        ("cumulative-amo,c", 
                    "Encodes only one single, incremental At-Most-One constraint "
                    "for each position, over the entire hierarchy "
                    "(not applicable when \"full-prim-elem-encoding\" is active)")
        ("init-layer-amo,a", 
                    "Encode At-Most-One constraints over all possible elements "
                    "at each position of the initial layer (not applicable when \"full-amo\" is active")
        ("full-amo,A",
                    "Encode At-Most-One constraints over ALL elements (also composite elements)")
        ("binary-amo-threshold,b", po::value<int>(),
                    "Set the minimum amount of concerned elements which need"
                    " to be possible in order to encode At-Most-One constraints"
                    " in a binary manner instead of pairwise"
                    " (not applicable when \"cumulative-amo\" is active)")
    ;
    
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);
    
    if (vm.count("help")) {
        std::cout << desc << std::endl;
        exit(1);
    }
    
    // Print provided arguments
    std::cout << "Command : ";
    for (int arg_idx = 0; arg_idx < argc; arg_idx++) {
        std::cout << argv[arg_idx] << " ";
    }
    std::cout << std::endl;
    
    Params* params = Params::get();
    
    if (vm.count("input")) {
        params->input_file = vm["input"].as<std::string>();
    }
    if (vm.count("optimize-plan")) {
        params->optimize_plan = true;
        
        if (vm.count("optimization-linear")) {
            params->optimization_stepsize = vm["optimization-linear"].as<int>();
            if (params->optimization_stepsize > 0) {
                params->optimization_mode = SolverInterface::OPTIMIZATION_MODE_LINEAR_INCREASING;
            } else {
                params->optimization_mode = SolverInterface::OPTIMIZATION_MODE_LINEAR_DECREASING;
            }
        } else {
            params->optimization_mode = SolverInterface::OPTIMIZATION_MODE_BINARY;
            if (vm.count("optimization-skew")) {
                params->optimization_skewness = vm["optimization-skew"].as<double>();
            }
        }
    }
    if (vm.count("timeout")) {
        params->timeout_milliseconds = vm["timeout"].as<long>();
    }
    if (vm.count("output-graph")) {
        params->output_graph = true;
    }
    if (vm.count("min-depth")) {
        params->min_depth = vm["min-depth"].as<int>();
    }
    if (vm.count("max-depth")) {
        params->max_depth = vm["max-depth"].as<int>();
    }
    
    if (vm.count("encode-predecessors")) {
        params->predecessors_specification = true;
    }
    if (vm.count("encode-all-predecessors")) {
        params->predecessors_specification = true;
        params->blank_elem_predecessors_specification = true;
    }
    
    if (vm.count("full-prim-elem-encoding")) {
        params->full_prim_elem_encoding = true;
    }
    if (vm.count("full-fact-encoding")) {
        params->full_fact_encoding = true;
    }
    if (vm.count("full-elem-encoding")) {
        params->full_element_encoding = true;
        params->full_prim_elem_encoding = true;
        params->full_fact_encoding = true;
    }
    
    if (vm.count("cumulative-amo")) {
        if (params->full_prim_elem_encoding) {
            // Illegal
            std::cout << "Illegal configuration: --cumulative-amo "
            "requires a sparse primitive element encoding" << std::endl;
            exit(1);
        }
        params->cumulative_amo = true;
    }
    if (vm.count("init-layer-amo")) {
        params->init_layer_amo = true;
    }
    if (vm.count("full-amo")) {
        params->full_amo = true;
    }
    if (vm.count("binary-amo-threshold")) {
        params->threshold_binary_amo = vm["binary-amo-threshold"].as<int>();
    }
    
}

int main(int argc, char * argv[]) 
{ 
    start_timer();
    
    // Already initialize the SAT solver here
    // (to avoid memory-related IPASIR bug)
    void* ipasir = ipasir_init();
    
    // register signals and signal handler  
    signal(SIGINT, signalHandler);
    signal(SIGTERM, signalHandler);
    
    // Parse arguments
    parse_args(argc, argv);
        
    // Print hello message
    log("This is T-REX interpreter, prototype version 04/2018 patched 09/2020.");
    
    // Delete solution file from previous calls, if existing
    remove("SOLUTION.txt");
    
    // Read and process encoding
    process();
    log("Processed problem encoding.");
    
    problem.compute_possible_fact_changes_of_comp_elems();
    log("Calculated possible fact changes of composite elements.");
    
    // Initialize incremental solving procedure
    solver = new SolverInterface(&problem, ipasir);
    log("Initialized instantiation procedure.");
    
    problem.print(/*verbose=*/true);

    // Attempt to solve
    bool success = solver->solve();
    
    if (success) {
        // Print (initial) solution
        output_solution(solver, true);
        
        if (Params::get()->optimize_plan) {
            
            // Optimize solution, with the output method
            // as a callback
            solver->optimize_result(output_solution);
            
            // Print solution again, possibly optimized
            output_solution(solver, true);
        }
    }
    
    log("Exiting.");
}
