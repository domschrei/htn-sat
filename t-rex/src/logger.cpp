#include "logger.h"

timepoint begin_timepoint;
timepoint round_timepoint;

void start_timer() 
{
    begin_timepoint = steady_clock::now();
}

void start_round_timer()
{
    round_timepoint = steady_clock::now();
}

long int elapsed_microsecs() 
{
    timepoint end = steady_clock::now();
    return duration_cast<std::chrono::microseconds>(end - begin_timepoint).count();
}

long int elapsed_round_nanosecs()
{
    timepoint end = steady_clock::now();
    return duration_cast<std::chrono::nanoseconds>(end - round_timepoint).count();
}

void log(std::string msg) 
{
    double elapsed_s = elapsed_microsecs() * 0.001 * 0.001;
    printf("[%4.3f] %s\n", elapsed_s, msg.c_str());
}

std::string str(const char* msg, int i) 
{
    char buff[128];
    snprintf(buff, sizeof(buff), msg, i);
    return std::string(buff);
}

std::string str(const char* msg, int i1, int i2) 
{
    char buff[128];
    snprintf(buff, sizeof(buff), msg, i1, i2);
    return std::string(buff);
}

std::string str(const char* msg, int i1, int i2, int i3)
{
    char buff[128];
    snprintf(buff, sizeof(buff), msg, i1, i2, i3);
    return std::string(buff);
}

std::string str(const char* msg, double d) 
{
    char buff[128];
    snprintf(buff, sizeof(buff), msg, d);
    return std::string(buff);
}

std::string str(const char* msg, const char* format_str) 
{
    char buff[128];
    snprintf(buff, sizeof(buff), msg, format_str);
    return std::string(buff);
}

std::string str(const char* msg, const char* str1, const char* str2)
{
    char buff[128];
    snprintf(buff, sizeof(buff), msg, str1, str2);
    return std::string(buff);
}

const char* large_int_str(int in) 
{
    std::string out = std::to_string(in);
    int offset = 3;
    while (out.size() > offset) {
        out = out.substr(0, out.size()-offset) + "," + out.substr(out.size()-offset);
        offset += 4;
    }
    return out.c_str();
}
