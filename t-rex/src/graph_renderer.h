#ifndef CLAUSE_GRAPH_RENDERER_H
#define CLAUSE_GRAPH_RENDERER_H

#include "clause_graph.h"
#include "hierarchylayer.h"

/**
 * Renders a set of variables and clauses to a graph.
 */
class GraphRenderer {
    
public:
    GraphRenderer(IncProblem* p);
    
    /**
     * Set the edge properties for the following clauses.
     */
    void edge_mode(EdgeProperties prop);
    /**
     * Clear the edge properties for the following clauses.
     */
    void clear_edge_mode();
    /**
     * Adds a node to the current edge set.
     */
    void add_to_edges(int id);
    /**
     * Finalizes the graph creation.
     */
    void finish(HierarchyLayer* final_layer);
    
    NodeProperties node_prop_fact = {
        "Fact", //label
        true, // visible
        false, 0.0, 0.0, 0.0, // position
        true, 150,150,255, // color
        true, 5.0, 5.0 // size
    };
    NodeProperties node_prop_elem_prim = {
        "Primitive element", //label
        true, // visible
        false, 0.0, 0.0, 0.0, // position
        true, 255,255,0, // color
        true, 6.0, 6.0 // size
    };
    NodeProperties node_prop_elem_comp = {
        "Composite element", //label
        true, // visible
        false, 0.0, 0.0, 0.0, // position
        true, 255,100,0, // color
        true, 6.0, 6.0 // size
    };
    NodeProperties node_prop_blank = {
        "Blank element", //label
        true, // visible
        false, 0.0, 0.0, 0.0, // position
        true, 0,200,0, // color
        true, 6.0, 6.0 // size
    };
    NodeProperties node_prop_primitivity = {
        "Primitiveness predicate", //label
        true, // visible
        false, 0.0, 0.0, 0.0, // position
        true, 200,200,200, // color
        true, 6.0, 6.0 // size
    };
    
    EdgeProperties edge_prop_elem_transition = {
        "Transition", //label
        true, // visible
        true, 255,100,0 // color
    };
    EdgeProperties edge_prop_fact_related = {
        "Fact-related", //label
        true, // visible
        true, 200,200,255 // color
    };
    
private:
    IncProblem* p;
    HierarchyLayer* final_layer;
    
    ClauseGraph* graph;
    NodeProperties node_properties;
    
    void init_graph_nodes(HierarchyLayer* final_layer);
    void add_node_at(int id, NodeProperties prop, double x, double y, double z);
};

#endif
