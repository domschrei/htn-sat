#ifndef INC_PROBLEM_H
#define INC_PROBLEM_H

#include <iostream>
#include <vector>
#include <set>
#include <stdint.h>
#include "boost/dynamic_bitset.hpp"

const int LIT_BLANK_ELEMENT = 1;

typedef typename std::vector<std::vector<int>> reduction;

/**
 * Contains a number of annotated clauses.
 */
struct formula {
    
    /**
     * Each clause is given in an annotated manner: a clause
     * of length n will be represented by an integer array 
     * of length 2n, whereas the straight indices will contain 
     * the literal values (positive or negative) and the
     * odd indices may contain an annotation of the corresponding
     * literal, given in an integer encoding. The semantics of the
     * annotation depends on the concrete formula it is part of.
     */
    int32_t** clauses;
    /**
     * Contains the array size of each corresponding clause, 
     * so the effective clause size is that value divided by two.
     */
    int* clause_sizes;
    /**
     * The maximal literal value that occurs in any of the clauses.
     */
    int num_vars;
    /**
     * The amount of clauses in the formula.
     */
    int num_clauses;
};

/**
 * Represents a SAT-encoded problem in the IncHex style.
 */
class IncProblem {

public:
    /** Formula mode for the initial state specification */
    const static int MODE_INIT = 1;
    /** Formula mode for the universal state specification */
    const static int MODE_UNIV = 2;
    /** Formula mode for the goal state specification */
    const static int MODE_GOAL = 3;
    /** Formula mode for the transitional specification */
    const static int MODE_TRAN = 4;
    
    struct expansion_condition {
        int comp_elem;
        int child_elem;
        int offset;
        int fact_condition;        
    };
    
    struct reduction_condition {
        int comp_elem;
        int red_idx;
        int fact_condition;
    };
    
private:
    /** 
     * Contains the clauses of the initial state specification.
     * Annotations have the meaning of concrete positions 
     * ("@" annotation).
     */
    formula f_init;
    /**
     * Contains the clauses of the universal state specification.
     * Annotations have the meaning of positive position offsets
     * ("+" annotation).
     */
    formula f_univ;
    /**
     * Contains the clauses of the goal state specification.
     * Annotations have the meaning of concrete positions 
     * ("@" annotation).
     */
    formula f_goal;
    /**
     * Contains the clauses of the goal state specification.
     * Annotations have the meaning of positive position offsets
     * ("+" annotation) and the literals range up to twice their 
     * usual length, whereas the second half of that variable domain
     * corresponds to the corresponding variables at the next depth.
     */
    formula f_tran;
    
    
    // Important measures of the problem's variable domain
    
    int num_prim_elems = -1;
    int num_comp_elems = -1;
    int num_reductions = 0;
    int num_elems = -1;
    int num_facts = -1;
    int primitivity_var;
    int first_reduction_var;
    int max_expansion_size = 0;
    int improper_element = -1;
    
    /**
     * successors[elem][offset][i] corresponds to the `i`-th possible successor
     * element offset by `offset` of the previous element `elem`.
     */
    std::vector<std::vector<std::vector<int>>> successors;
    std::vector<std::vector<std::vector<int>>> predecessors;
    
    std::vector<std::vector<reduction>> alt_successors;
    /**
     * For each element, contains a set of fact indices (the preconditions).
     */
    std::vector<std::set<int>> preconditions_of_elems;
    /**
     * For each primitive element, contains a set of fact indices (the effects).
     */
    std::vector<std::set<int>> effects_of_prim_elems;
    /**
     * For each fact index >= 1, contains a set of elements
     * having the fact as a positive effect.
     */
    std::vector<std::set<int>> supporting_prim_elems_of_pos_facts;
    /**
     * For each fact index >= 1, contains a set of elements
     * having the fact as a negative effect.
     */
    std::vector<std::set<int>> supporting_prim_elems_of_neg_facts;
    /**
     * For each composite element index, contains a set of facts
     * which might be changed by that element.
     */
    std::vector<boost::dynamic_bitset<>> facts_changed_by_comp_elems;
    
    std::vector<expansion_condition> expansion_conditions;
    std::vector<reduction_condition> reduction_conditions;
    
public:
    
    /**
     * Returns the problem's formula of the given mode
     * (one of IncProblem::MODE_* , else undefined behaviour).
     */
    formula* f(int mode);
    
    
    // Initialization and building of data structures
    
    void set_num_prim_elems(int num_prim_elems);
    void set_num_comp_elems(int num_comp_elems);
    void set_num_reductions(int num_reductions);
    void set_num_facts(int num_facts);
    
    void add_successor(int comp_elem, int offset, int child_elem);
    void add_successor(int comp_elem, int red_idx, int offset, int child_elem);
    void add_precondition_of_elem(int fact, int elem);
    void add_effect_of_prim_elem(int fact, int prim_elem);
    void add_supporting_prim_elem_of_pos_fact(int prim_elem, int fact);
    void add_supporting_prim_elem_of_neg_fact(int prim_elem, int fact);
    bool add_supporting_comp_elem_of_fact(int comp_elem, int fact);
    void add_expansion_condition(int comp_elem, int child_elem, int offset, int fact);
    void add_reduction_condition(int comp_elem, int red_idx, int fact);
    void add_improper_element(int elem);
    
    void compute_possible_fact_changes_of_comp_elems();
    
    
    // Queries
    
    // Variable type queries
    bool is_var_prim_elem(int var);
    bool is_var_comp_elem(int var);
    bool is_var_elem(int var);
    bool is_var_fact(int var);
    bool is_var_reduction_idx(int var);
    
    // Index conversions
    int prim_elem_var_to_idx(int var);
    int comp_elem_var_to_idx(int var);
    int elem_var_to_idx(int var);
    int fact_var_to_idx(int var);
    int fact_idx_to_var(int idx);
    int reduction_var_to_idx(int var);
    
    // Domain queries
    int get_num_vars();
    int get_num_prim_elems();
    int get_num_comp_elems();
    int get_num_reductions();
    int get_num_elems();
    int get_num_facts();
    int get_num_non_elems();
    int get_max_expansion_size();
    int get_primitivity_var();
    int get_first_reduction_var();
    bool has_improper_element();
    int get_improper_element();
    
    /**
     * Returns a pointer to a vector which, at position i, contains a vector
     * of elements which are possible successors of the provided comp_elem 
     * at offset i.
     */
    std::vector<std::vector<int>>* get_successors_of_comp_elem(int comp_elem);
    
    std::vector<reduction>* get_alt_successors_of_comp_elem(int comp_elem);
    
    std::vector<std::vector<int>>* get_predecessors_of_elem(int elem);
    /**
     * Returns a pointer to a set of fact indices which corresponds to the
     * preconditions of the provided general element.
     */
    std::set<int>* get_preconditions_of_elem(int elem);
    /**
     * Returns a pointer to a set of fact indices which corresponds to the
     * effects of the provided primitive element.
     */
    std::set<int>* get_effects_of_prim_elem(int prim_elem);
    /**
     * Returns a pointer to a set of all primitive elements which 
     * have the provided fact as a positive or negative effect, 
     * depending on the provided `positive` bool.
     */
    std::set<int>* get_supporting_prim_elems_of_fact(int fact, bool positive);
    
    /**
     * Returns a pointer to a set of indices of all facts which 
     * _might_ change as a result of the provided composite element.
     */
    boost::dynamic_bitset<>* get_facts_changed_by_comp_elem(int comp_elem);
    std::vector<boost::dynamic_bitset<>>* get_facts_changed_by_comp_elems();
    
    std::vector<expansion_condition>* get_expansion_conditions();
    std::vector<reduction_condition>* get_reduction_conditions();
    
    int get_prim_elems_bitlength();
    
    /**
     * Prints some information about the problem data to stdout,
     * mainly for debugging purposes.
     */
    void print(bool verbose);
    
private:
    /**
     * Initializes some data structures after all important
     * domain size measures have been set.
     */
    void init();
};

#endif
