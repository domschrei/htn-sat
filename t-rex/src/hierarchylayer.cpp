#include "hierarchylayer.h"
#include "params.h"

HierarchyLayer::HierarchyLayer()
{
    // Default constructor (private)
    Params* params = Params::get();
    encode_all_elems = params->full_element_encoding;
    encode_all_facts = params->full_fact_encoding;
    reencode_prim_elems = params->full_prim_elem_encoding;
}

HierarchyLayer::HierarchyLayer(IncProblem* p)
{
    this->p = p;
    
    ALL_ELEMS_POSSIBLE = boost::dynamic_bitset<>(p->get_num_elems());
    for (int i = 0; i < ALL_ELEMS_POSSIBLE.size(); i++)
        ALL_ELEMS_POSSIBLE[i] = 1;
    
    Params* params = Params::get();
    encode_all_elems = params->full_element_encoding;
    encode_all_facts = params->full_fact_encoding;
    reencode_prim_elems = params->full_prim_elem_encoding;
    
    depth = 0;
    next_layer = this;
    has_reduction_alternatives = p->get_num_reductions() > 0;
    
    // Compute all properties for the initial 
    // hierarchy layer of the given problem
    compute_init_possible_elems_and_facts();
    array_size = possible_elems.size();
        
    compute_compact_indices();
    
    if (!encode_all_elems) {
        if (!encode_all_facts || !reencode_prim_elems) {
            compute_prim_elem_and_fact_variables();
        }
    }
}

HierarchyLayer* HierarchyLayer::compute_next_layer()
{
    next_layer = new HierarchyLayer();
    
    // Set fundamental properties
    next_layer->p = p;
    next_layer->depth = depth+1;
    next_layer->prev_layer = this;
    next_layer->has_reduction_alternatives = has_reduction_alternatives;
    next_layer->ALL_ELEMS_POSSIBLE = ALL_ELEMS_POSSIBLE;
    
    if (encode_all_elems) {
        
        next_layer->array_size = array_size + p->get_max_expansion_size() * array_size;
        
    } else {
        
        // Compute properties of the succeeding hierarchy layer
        compute_next_possible_elems();
        if (!encode_all_facts) {
            compute_next_possible_facts();
        }
        
        // Set calculated array size
        next_layer->array_size = next_layer->possible_elems.size();
    }
    
    compute_compact_indices();
    
    if (!encode_all_elems) {
        if (!reencode_prim_elems || !encode_all_facts) {
            compute_prim_elem_and_fact_variables();
        }
    }
    
    return next_layer;
}

void HierarchyLayer::compute_init_possible_elems_and_facts()
{
    // Initialize various data structures
    
    next_positions = std::vector<int>();
    
    possible_elems = std::vector<boost::dynamic_bitset<>>();
    prim_elem_variables = std::vector<std::vector<int>>();
    new_prim_elems = std::vector<boost::dynamic_bitset<>>();
    
    possible_facts = std::vector<boost::dynamic_bitset<>>();
    fact_variables = std::vector<std::vector<int>>();
    new_facts = std::vector<boost::dynamic_bitset<>>();
    
    Params* params = Params::get();
        
    // Iterate over initial clauses
    formula* f_init = p->f(IncProblem::MODE_INIT);
    for (int c = 0; c < f_init->num_clauses; c++) {
        
        int* clause = f_init->clauses[c];
        int clause_size = f_init->clause_sizes[c];

        // For each literal
        for (int i = 0; i+1 < clause_size; i += 2) {
            int lit = clause[i];
            uint pos = clause[i+1];
            
            // Is `lit` a positive element variable?
            if (lit > 0 && p->is_var_elem(lit)) {
                
                // Add the element to the possible elements    
                add_elem(&possible_elems, pos, lit);
                bool is_primitive = p->is_var_prim_elem(lit);
                
                // Add primitive elements to the "new prim elems" structure
                if (is_primitive && !params->full_prim_elem_encoding) {
                    add_prim_elem(&new_prim_elems, pos, lit);
                }
                
                // Update the number of maximal reductions at this position
                if (has_reduction_alternatives && !is_primitive) {
                    int num_reductions = p->get_alt_successors_of_comp_elem( p->comp_elem_var_to_idx(lit))->size();
                    while (pos >= max_num_reductions.size())
                        max_num_reductions.push_back(0);
                    max_num_reductions[pos] = std::max(max_num_reductions[pos], num_reductions);
                }
                
                if (params->full_fact_encoding)
                    continue;
                
                // Add the facts belonging to this element
                if (is_primitive) {
                    // Primitive element: Add effects
                    std::set<int>* effects = p->get_effects_of_prim_elem(
                        p->elem_var_to_idx(lit));
                    for (auto it = effects->begin(); it != effects->end(); ++it) {
                        add_fact(&possible_facts, pos+1, std::abs(*it));
                    }
                } else {
                    // Composite element: Add possible changes
                    boost::dynamic_bitset<>* changes = p->get_facts_changed_by_comp_elem(
                        p->comp_elem_var_to_idx(lit));                    
                    for (int idx = changes->find_first(); 
                            0 <= idx && idx < changes->size(); 
                            idx = changes->find_next(idx)) {
                        add_fact(&possible_facts, pos+1, idx);
                    }
                }
            }
            // Is `lit` a fact variable?
            else if (!params->full_fact_encoding && p->is_var_fact(std::abs(lit))) {
                // Add the fact to the possible facts
                int fact_idx = p->fact_var_to_idx(std::abs(lit));
                add_fact(&possible_facts, pos, fact_idx);
            }
        }
    }
    
    if (!params->full_fact_encoding) {
        
        // Add all facts at the final position
        int last_pos = possible_facts.size() - 1;
        for (int fact = 1; fact <= p->get_num_facts(); fact++) {
            add_fact(&possible_facts, last_pos, fact);
        }
        
        // Add all facts to the "new facts" structure
        for (int pos = 0; pos <= last_pos; pos++) {
            for (int fact = 1; fact <= p->get_num_facts(); fact++) {
                if (is_fact_possible(fact, pos)) {
                    add_fact(&new_facts, pos, fact);
                }
            }
        }
    }
    
    // Initialize the "overwritten variables" structure,
    // now that the initial array size is known
    overwritten_prim_elem_vars = std::vector<std::vector<std::vector<int>>>(
        possible_elems.size());
}

void HierarchyLayer::compute_prim_elem_and_fact_variables()
{    
    // Is this the first layer of the hierarchy?
    if (next_layer->depth == 0) {
        
        // Initialize empty data structures
        if (!encode_all_facts) {
            next_layer->fact_variables = std::vector<std::vector<int>>();
        }
        if (!reencode_prim_elems) {
            next_layer->prim_elem_variables = std::vector<std::vector<int>>();
            next_layer->amo_helper_var_starts = std::vector<int>(
                next_layer->possible_elems.size(), 0);
            next_layer->amo_first_unencoded_number = std::vector<int>(
                next_layer->possible_elems.size(), 0);
        }
        
    } else {
        
        // Propagate properties from this layer to the next layer
        
        // Initialize structures
        if (!encode_all_facts) {
            next_layer->fact_variables = std::vector<std::vector<int>>(
                next_positions[next_positions.size()-1], 
                std::vector<int>(p->get_num_facts()+1, 0)
            );
        }
        if (!reencode_prim_elems) {
            next_layer->prim_elem_variables = std::vector<std::vector<int>>(
                next_positions[next_positions.size()-1], 
                std::vector<int>(p->get_num_prim_elems()+1, 0)
            );
            next_layer->amo_helper_var_starts = std::vector<int>(
                next_positions[next_positions.size()-1], 0
            );
            next_layer->amo_first_unencoded_number = std::vector<int>(
                next_positions[next_positions.size()-1], 0
            );
        }
        
        // Convert the variable positions from the last layer 
        // to the new positions
        for (int pos = 0; pos < next_positions.size(); pos++) {
            int next_pos = next_positions[pos];
            if (!encode_all_facts && pos < fact_variables.size())
                next_layer->fact_variables[next_pos] = fact_variables[pos];
            if (!reencode_prim_elems && pos < prim_elem_variables.size()) 
                next_layer->prim_elem_variables[next_pos] = prim_elem_variables[pos];
            if (!reencode_prim_elems && pos < amo_helper_var_starts.size())
                next_layer->amo_helper_var_starts[next_pos] = amo_helper_var_starts[pos];
            if (!reencode_prim_elems && pos < amo_first_unencoded_number.size())
                next_layer->amo_first_unencoded_number[next_pos] = amo_first_unencoded_number[pos];
        }
        
        // Create new AMO helper variables where needed
        if (!reencode_prim_elems && Params::get()->cumulative_amo) {
            for (int pos = 0; pos < next_layer->amo_helper_var_starts.size(); pos++) {
                if (next_layer->amo_helper_var_starts[pos] == 0) {
                    next_layer->amo_helper_var_starts[pos] =
                        next_layer->allocate_helper_variables(
                            p->get_prim_elems_bitlength());
                }
            }
        }
    }
    
    if (!reencode_prim_elems) {
        // For each NEW possible element at each position
        for (int pos = 0; pos < next_layer->new_prim_elems.size(); pos++) {
            for (int prim_elem = 1; prim_elem <= p->get_num_prim_elems(); prim_elem++) {
                if (!next_layer->is_prim_elem_new(prim_elem, pos))
                    continue;
                
                // Assign a new variable to this new primitive element
                add(&(next_layer->prim_elem_variables), pos, prim_elem, 
                    next_layer->allocate_helper_variables(1));
            }
        }
    }
    
    if (encode_all_facts)
        return;
    
    // For each NEW possible fact at each position
    for (int pos = 0; pos < next_layer->new_facts.size(); pos++) {
        for (int i = 0; i < next_layer->new_facts[pos].size(); i++) {
            if (!next_layer->new_facts[pos][i])
                continue;
            
            int new_fact = i;
            
            // If the variable for this fact is undefined so far ...
            if (pos >= next_layer->fact_variables.size()  
                || new_fact >= next_layer->fact_variables[pos].size()
                || next_layer->fact_variables[pos][new_fact] == 0) {
                
                // ... define a new variable
                add(&(next_layer->fact_variables), pos, new_fact, 
                    next_layer->allocate_helper_variables(1));
            
            // But if the variable is already defined ...    
            } else if (pos < next_layer->fact_variables.size() 
                && new_fact < next_layer->fact_variables[pos].size()) {
                
                // ... remove the entry from the new facts
                next_layer->new_facts[pos][new_fact] = false;
            }
        }
    }  
}

void HierarchyLayer::compute_compact_indices()
{
    next_layer->compact_indices = std::vector<std::unordered_map<int, int>>();
    
    int num_non_elements = p->get_num_non_elems();
    if (encode_all_elems)
        num_non_elements += p->get_num_elems() + 1 + p->get_num_facts() + p->get_num_reductions();
    else if (encode_all_facts)
        num_non_elements += p->get_num_facts();
    
    next_layer->domain_starts = std::vector<int>();
    if (next_layer->depth == 0) {
        next_layer->domain_starts.push_back(0);
    } else {
        next_layer->domain_starts.push_back(
            // Domain start at the final position of last depth
            domain_starts[domain_starts.size()-1] 
            // Domain size at this position: amount of encoded elements ...
            + compact_indices[compact_indices.size()-1].size()
            // ... non-elements ...
            + num_non_elements
            // ... and additionally added helper variables at the last depth
            + added_helper_vars
        );
    }
    // Initialize helper variables counter for next depth
    next_layer->added_helper_vars = 0;
    
    // Create compact indices for elements and facts
    for (int pos = 0; pos < next_layer->array_size; pos++) {
        next_layer->compact_indices.push_back(std::unordered_map<int, int>());
        
        int compact_idx = 1;
        
        if (!encode_all_elems) {
            
            // Primitive elements
            if (reencode_prim_elems) {
                for (int prim_elem = 1; prim_elem <= p->get_num_prim_elems(); prim_elem++) {
                    if (next_layer->is_elem_possible(prim_elem, pos)) {
                        next_layer->compact_indices[pos].insert({{prim_elem, compact_idx}});
                        compact_idx++;
                    }
                }
            }
            
            // Composite elements
            for (int comp_elem = p->get_num_prim_elems()+1; comp_elem <= p->get_num_elems(); comp_elem++) {
                if (next_layer->is_elem_possible(comp_elem, pos)) {
                    next_layer->compact_indices[pos].insert({{comp_elem, compact_idx}});
                    compact_idx++;
                }
            }
            
            // "is primitive" variable
            next_layer->compact_indices[pos].insert({{p->get_primitivity_var(), compact_idx}});
            compact_idx++;
            
            // Reduction variables
            if (has_reduction_alternatives && pos < next_layer->max_num_reductions.size()) {
                int max_num_reductions = next_layer->max_num_reductions[pos];
                int reduction_var = p->get_first_reduction_var();
                for (int r = 0; r < max_num_reductions; r++) {
                    next_layer->compact_indices[pos].insert({{reduction_var, compact_idx}});
                    reduction_var++;
                    compact_idx++;
                }
            }
        }
        
        next_layer->domain_starts.push_back(
            next_layer->domain_starts[pos] 
            + next_layer->compact_indices[pos].size() 
            + num_non_elements
        );
    }
}

void HierarchyLayer::compute_next_possible_elems()
{
    // Initialize data structures
    next_layer->possible_elems = std::vector<boost::dynamic_bitset<>>();
    next_layer->new_prim_elems = std::vector<boost::dynamic_bitset<>>();
    next_layer->new_facts = std::vector<boost::dynamic_bitset<>>();
    
    // Initialize position mapping
    int next_pos = 0;
    next_positions = std::vector<int>();
    next_positions.push_back(next_pos);
    next_layer->last_positions = std::vector<int>();
    next_layer->last_positions.push_back(0);
    
    next_layer->overwritten_prim_elem_vars = std::vector<std::vector<std::vector<int>>>();
    
    // For all old positions of elements
    for (int pos = 0; pos < possible_elems.size(); pos++) {
        
        // Carry over variable definitions to the next layer
        while (next_pos >= next_layer->overwritten_prim_elem_vars.size())
            next_layer->overwritten_prim_elem_vars.push_back(std::vector<std::vector<int>>());
        next_layer->overwritten_prim_elem_vars[next_pos] = overwritten_prim_elem_vars[pos];
    
        // Find out the maximal shift value
        int shift = 1;
        for (int i = 0; i < p->get_num_comp_elems(); i++) {
            int elem = p->get_num_prim_elems() + i + 1;
            
            if (!is_elem_possible(elem, pos))
                continue;
            
            if (has_reduction_alternatives) {
                std::vector<reduction>* s = p->get_alt_successors_of_comp_elem(i);
                for (auto it = s->begin(); it != s->end(); ++it) {
                    reduction red = *it;
                    shift = std::max(shift, (int) red.size());
                }
            } else {
                std::vector<std::vector<int>>* s = p->get_successors_of_comp_elem(i);
                shift = std::max(shift, (int) s->size());
            }
        }
        
        // For each possible primitive element at the position
        for (int i = 0; i < p->get_num_prim_elems(); i++) {
            
            int elem = i + 1;
            
            if (!is_elem_possible(elem, pos))
                continue;
            
            // At the next start position, this element is possible again
            add_elem(&next_layer->possible_elems, next_pos, elem);
            
            // At the following positions, blank elements are possible
            for (int offset = 1; offset < shift; offset++) {
                add_elem(&next_layer->possible_elems, next_pos+offset, LIT_BLANK_ELEMENT); 
                if (!reencode_prim_elems)
                    add_elem(&next_layer->new_prim_elems, next_pos+offset, LIT_BLANK_ELEMENT);
            }
        }
        
        // For each possible composite element at the position
        for (int i = 0; i < p->get_num_comp_elems(); i++) {
            int elem = p->get_num_prim_elems() + i + 1;
            if (!is_elem_possible(elem, pos))
                continue;
            
            // Find all possible sets of successors of this element
            std::vector<std::vector<std::vector<int>>*> successors_list;
            if (has_reduction_alternatives) {
                std::vector<reduction>* reductions = p->get_alt_successors_of_comp_elem(i);
                for (int r = 0; r < reductions->size(); r++) {
                    std::vector<std::vector<int>>* successors = &reductions->at(r);
                    successors_list.push_back(successors);
                }
            } else {
                std::vector<std::vector<int>>* successors = p->get_successors_of_comp_elem(i);
                successors_list.push_back(successors);
            }
            
            // For each set of successors
            for (auto it = successors_list.begin(); it != successors_list.end(); ++it) {
                std::vector<std::vector<int>>* s = *it;
                
                // For all positions that the element will expand to
                for (int offset = 0; offset < shift; offset++) {
                
                    if (offset < s->size()) {
                        // The comp. elment has children at this position
                        
                        for (int next_elem_idx = 0; next_elem_idx < s->at(offset).size(); next_elem_idx++) {
                            int next_elem = s->at(offset)[next_elem_idx] + 1;
                            
                            // The child element occurs at the corresponding offset
                            add_elem(&next_layer->possible_elems, next_pos + offset, next_elem);
                            
                            // Primitive element?
                            if (p->is_var_prim_elem(next_elem)) {
                                
                                // Add the next elem to the set of elements
                                // which need to be newly encoded
                                if (!reencode_prim_elems) {
                                    add_prim_elem(&next_layer->new_prim_elems, next_pos + offset, next_elem);
                                
                                    if (offset == 0 
                                        && is_elem_possible(next_elem, pos)) {
                                        
                                        // If the prim. elem already occurred at this position,
                                        // then the old variable has to be remembered
                                        add(&next_layer->overwritten_prim_elem_vars, next_pos+offset, 
                                            next_elem, prim_elem_variables[pos][next_elem]);
                                    }
                                }
                                
                            } else if (has_reduction_alternatives) {
                                // Update the number of maximal reductions at this position
                                int num_reductions = p->get_alt_successors_of_comp_elem(p->comp_elem_var_to_idx(next_elem))->size();
                                while (next_pos+offset >= next_layer->max_num_reductions.size())
                                    next_layer->max_num_reductions.push_back(0);
                                next_layer->max_num_reductions[next_pos+offset] = std::max(
                                    next_layer->max_num_reductions[next_pos+offset], num_reductions);
                            }
                        }
                        
                    } else {
                        
                        // Add the blank element
                        add_elem(&next_layer->possible_elems, next_pos + offset, LIT_BLANK_ELEMENT);
                        if (!reencode_prim_elems)
                            add_prim_elem(&next_layer->new_prim_elems, next_pos + offset, LIT_BLANK_ELEMENT);
                    }
                }
            }
        }
        
        next_pos += shift;
        while (next_pos > next_layer->last_positions.size()) {
            next_layer->last_positions.push_back(pos);
        }
        next_positions.push_back(next_pos);
    }    
}

void HierarchyLayer::compute_next_possible_facts()
{
    next_layer->possible_facts = std::vector<boost::dynamic_bitset<>>(next_layer->possible_elems.size());
    next_layer->new_facts = std::vector<boost::dynamic_bitset<>>(next_layer->possible_facts.size());
    
    // Propagate occuring facts to the next depth
    for (int pos = 0; pos < possible_facts.size(); pos++) {
        int next_pos = next_positions[pos];
        next_layer->possible_facts[next_pos] = possible_facts[pos];
        next_layer->new_facts[next_pos] = boost::dynamic_bitset<>(p->get_num_facts()+1);
        for (int pos_after = next_pos+1; pos_after < next_positions[pos+1]; pos_after++) {
            next_layer->possible_facts[pos_after] = boost::dynamic_bitset<>(p->get_num_facts()+1);
            next_layer->new_facts[pos_after] = boost::dynamic_bitset<>(p->get_num_facts()+1);
        }
    }
    
    // Add facts of possible elements at the new depth
    for (int pos = 0; pos < next_layer->possible_elems.size(); pos++) {
        
        for (int elem = 1; elem < next_layer->possible_elems[pos].size(); elem++) {
        
            if (!next_layer->is_elem_possible(elem, pos)) 
                continue;
            
            std::set<int>::iterator it;
            
            if (p->is_var_prim_elem(elem)) {
                
                // Primitive element: Add effects
                if (pos+1 < next_layer->possible_elems.size()) {
                    std::set<int>* effects = p->get_effects_of_prim_elem(p->elem_var_to_idx(elem));
                    for (it = effects->begin(); it != effects->end(); ++it) {
                        int fact = std::abs(*it);
                        if (!next_layer->is_fact_possible(fact, pos+1)) {
                            add_fact(&next_layer->possible_facts, pos+1, fact);
                            add_fact(&next_layer->new_facts, pos+1, fact);
                        }
                    }
                }
            } else {
                
                // Composite element: Add possible changes
                if (pos+1 < next_layer->possible_elems.size()) {
                    
                    boost::dynamic_bitset<>* changes = p->get_facts_changed_by_comp_elem(p->comp_elem_var_to_idx(elem));
                    next_layer->possible_facts[pos+1] |= *changes;
                    next_layer->new_facts[pos+1] |= *changes;
                    /*
                    for (int idx = changes->find_first(); 0 <= idx && idx < changes->size(); idx = changes->find_next(idx)) {
                        if (!next_layer->is_fact_possible(idx, pos+1)) {
                            add_fact(&next_layer->possible_facts, pos+1, idx);
                            add_fact(&next_layer->new_facts, pos+1, idx);
                        }
                    }*/
                }
            }
        }
    }
}

int HierarchyLayer::lit_at_pos(int lit, int pos)
{
    bool positive = lit>0;
    lit = std::abs(lit);
    int l = lit;
    
    if (!reencode_prim_elems && p->is_var_prim_elem(lit)) {
        lit = prim_elem_variables[pos][lit];
        lit = (positive ? 1 : -1) * lit;
    } else if (!encode_all_facts && p->is_var_fact(lit)) {
        int fact_idx = p->fact_var_to_idx(lit);
        lit = fact_variables[pos][fact_idx];
        lit = (positive ? 1 : -1) * lit;
    } else {
        
        int num_compactified_vars = 0;
        if (!encode_all_elems)
            num_compactified_vars += p->get_num_elems() + 1 + p->get_num_reductions();
        if (!encode_all_facts)
            num_compactified_vars += p->get_num_facts();
        
        if (encode_all_elems) {
            // Do nothing
        } else if (lit > num_compactified_vars) {
            // The literal is a non-element
            lit -= num_compactified_vars;
            lit += compact_indices[pos].size();
        } else {
            // The literal is an element; compress it to its compact index
            lit = compact_indices[pos][lit];
        }
        lit = (positive ? 1 : -1) * (domain_starts[pos] + lit);
    }
    
    if (lit == 0) {
        std::cout << "error with " << l << " at pos " << pos << ", depth " << depth << std::endl;
        std::cout << 3/0 << std::endl; // make program crash for valgrind
        return 0;
    }
    return lit;
}

int HierarchyLayer::allocate_helper_variables(int amount)
{
    // Where should the new variable block start?
    int var_start = 
        // Domain start at the final position of current depth
        domain_starts[domain_starts.size()-1] 
        // Domain size at this position: amount of encoded elements ...
        + compact_indices[compact_indices.size()-1].size()
        // ... non-elements ...
        + p->get_num_non_elems()
        // ... and additionally added helper variables so far
        + added_helper_vars + 1;
    
    // Update helper variables counter
    added_helper_vars += amount;
    
    // Return the first valid new variable
    return var_start;
}

int HierarchyLayer::get_depth()
{
    return depth;
}

int HierarchyLayer::get_array_size()
{
    return array_size;
}

int HierarchyLayer::get_next_position(int pos)
{
    if (encode_all_elems)
        return pos * p->get_max_expansion_size();
    
    return next_positions[pos];
}

int HierarchyLayer::get_last_position(int pos)
{
    if (encode_all_elems)
        return pos / p->get_max_expansion_size();
    
    return last_positions[pos];
}

bool HierarchyLayer::is_elem_possible(int elem, int pos)
{
    if (encode_all_elems)
        return true;
    
    return /*pos < possible_elems.size() && elem < possible_elems[pos].size() &&*/ 
        possible_elems[pos][elem];
}

bool HierarchyLayer::is_fact_possible(int fact, int pos)
{
    if (encode_all_facts)
        return true;
    
    return /*pos < possible_facts.size() && fact < possible_facts[pos].size() &&*/ 
        possible_facts[pos][fact];
}

boost::dynamic_bitset<>* HierarchyLayer::get_possible_elems(int pos)
{
    if (encode_all_elems)
        return &ALL_ELEMS_POSSIBLE;
        
    return &possible_elems[pos];
}

int HierarchyLayer::get_prim_elem_variable(int pos, int prim_elem)
{
    return prim_elem_variables[pos][prim_elem];
}

int HierarchyLayer::get_fact_variable(int pos, int fact)
{
    return fact_variables[pos][fact];
}

std::vector<int>* HierarchyLayer::get_overwritten_variables(int pos, int prim_elem)
{
    if (pos >= overwritten_prim_elem_vars.size() 
        || prim_elem >= overwritten_prim_elem_vars[pos].size())
        return &EMPTY_VECTOR;
    return &overwritten_prim_elem_vars[pos][prim_elem];
}

int HierarchyLayer::get_last_fact_occurence_lower_equals(int pos, int fact)
{
    while (!is_fact_possible(fact, pos)) {
        pos--;
    }
    return pos;
}

int HierarchyLayer::get_first_fact_occurence_greater_equals(int pos, int fact)
{
    while (!is_fact_possible(fact, pos)) {
        pos++;
    }
    return pos;
}

bool HierarchyLayer::is_prim_elem_new(int prim_elem, int pos)
{
    return pos < new_prim_elems.size() 
            && prim_elem < new_prim_elems[pos].size() 
            && new_prim_elems[pos][prim_elem];
}

bool HierarchyLayer::is_fact_new(int fact, int pos)
{
    return pos < new_facts.size()
            && fact < new_facts[pos].size()
            && new_facts[pos][fact];
}

int HierarchyLayer::get_compact_index(int pos, int var)
{
    return compact_indices[pos][var];
}

int HierarchyLayer::get_amo_helper_vars(int pos)
{
    return amo_helper_var_starts[pos];
}

int HierarchyLayer::get_amo_first_unencoded_number(int pos)
{
    return amo_first_unencoded_number[pos];
}

void HierarchyLayer::set_amo_first_unencoded_number(int pos, int num)
{
    amo_first_unencoded_number[pos] = num;
}

bool HierarchyLayer::contains_prim_elems_everywhere()
{
    if (encode_all_elems)
        return true;
    
    for (int pos = 0; pos < array_size; pos++) {
        int first_possible_elem = possible_elems[pos].find_first();
        if (first_possible_elem == possible_elems[pos].npos 
            || first_possible_elem > p->get_num_prim_elems()) {
            return false;
        }
    }
    return true;
}

void HierarchyLayer::add_elem(std::vector<boost::dynamic_bitset<> >* data, int pos, int elem)
{
    while (pos >= data->size())
        data->push_back(boost::dynamic_bitset<>(p->get_num_elems()+1));
    data->at(pos)[elem] = true;
}

void HierarchyLayer::add_prim_elem(std::vector<boost::dynamic_bitset<>>* data, int pos, int elem)
{
    while (pos >= data->size())
        data->push_back(boost::dynamic_bitset<>(p->get_num_prim_elems()+1));
    //while (elem >= data->at(pos).size())
    //    data->at(pos).push_back(false);
    data->at(pos)[elem] = true;
}

void HierarchyLayer::add_fact(std::vector<boost::dynamic_bitset<>>* data, int pos, int fact)
{
    while (pos >= data->size())
        data->push_back(boost::dynamic_bitset<>(p->get_num_facts()+1));
    //while (elem >= data->at(pos).size())
    //    data->at(pos).push_back(false);
    data->at(pos)[fact] = true;
}

void HierarchyLayer::add(std::vector<std::vector<int> >* data, int pos, int elem, int val)
{
    while (pos >= data->size())
        data->push_back(std::vector<int>());
    while (elem >= data->at(pos).size())
        data->at(pos).push_back(0);
    data->at(pos)[elem] = val;
}

void HierarchyLayer::add(std::vector<std::vector<std::vector<int> > >* data, int pos, int elem, int val)
{
    while (pos >= data->size())
        data->push_back(std::vector<std::vector<int>>());
    while (elem >= data->at(pos).size())
        data->at(pos).push_back(std::vector<int>());
    
    // Only write the element if it is NOT contained yet
    // at the last position of the corresponding vector
    if (data->at(pos)[elem].size() == 0 ||
        data->at(pos)[elem][(data->at(pos)[elem]).size()-1] != val)
        data->at(pos)[elem].push_back(val);
}

HierarchyLayer * HierarchyLayer::get_next_layer()
{
    return next_layer;
}

HierarchyLayer * HierarchyLayer::get_prev_layer()
{
    return prev_layer;
}
