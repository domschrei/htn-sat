#ifndef GRAPH_OUTPUT_H
#define GRAPH_OUTPUT_H

#include <string>
#include <vector>
#include <iostream>
#include <fstream>

/**
 * Bundles the properties of a particular node
 * representing some variable.
 */
struct NodeProperties {
    
    const char* label;
    
    bool visible;
    
    bool has_position;
    double x;
    double y;
    double z;
    
    bool has_color;
    int r;
    int g;
    int b;
    
    bool has_size;
    double w;
    double h;
};

/**
 * Bundles the properties of a particular node
 * representing a part of some clause.
 */
struct EdgeProperties {
    
    const char* label;
    
    bool visible;
    
    bool has_color;
    int r;
    int g;
    int b;
};

/**
 * Represents a graph of the clauses which are added 
 * via ipasir_add calls. Can be output into a .gdf file
 * in order to be visualized by a tool like Gephi.
 */
class ClauseGraph {
    
public:
    ClauseGraph();
    
    /*
     * Initializes a node with the given id and without any additional properties.
     */
    void add_node(int id);
    
    /*
     * Initializes a node with the given id and some NodeProperties object.
     */
    void add_node(int id, NodeProperties prop);
    
    /**
     * Just like ipasir_add, the argument may be zero (to end the current edge set)
     * and also negative (to refer to the node with the corresponding positive id).
     */
    void add_to_graph(int id);
    
    /**
     * Sets the properties object for all edges to be added in the following.
     */
    void set_edge_properties(EdgeProperties prop);
    /**
     * Resets the properties object for all edges to be added in the following.
     */
    void clear_edge_properties();

    /**
     * Finalizes the graph creation process, merging the nodes and edges
     * into a single .gdf file.
     */
    void close_files();
    
private:
    /**
     * Initializes the file I/O necessary for the graph output.
     */
    void init_files();
    
    /**
     * Outputs the header line for the GDF node specification.
     */
    void output_node_header();
    /**
     * Outputs the header line for the GDF edge specification.
     */
    void output_edge_header();
    
    /**
     * Outputs the node of the specified id.
     */
    void output_node(int id);
    /**
     * Outputs the current set of edges.
     */
    void output_edge_set();
    
public:
    /**
     * Preliminary file for nodes
     */
    const char* FILENAME_NODES = "clause_graph_nodes.gdf.part";
    /**
     * Preliminary file for edges
     */
    const char* FILENAME_EDGES = "clause_graph_edges.gdf.part";
    /**
     * Final file for both nodes and edges
     */
    const char* FILENAME_MERGED = "clause_graph.gdf";
    /**
     * Seperator for the CSV-style gdf format
     */
    const char* SEPARATOR = ",";
    /**
     * Global position scaling factor for all nodes
     */
    const double SCALING_POS = 1.0;
    /**
     * Global size scaling factor for all nodes
     */
    const double SCALING_SIZE = 1.0;
    
private:
    /**
     * The default node properties to use when nothing else is specified
     */
    NodeProperties default_node_properties;
    /**
     * The default edge properties to use when nothing else is specified
     */
    EdgeProperties default_edge_properties;
    
    /**
     * Output for the node specification
     */
    std::ofstream file_nodes;
    /**
     * Output for the edge specification
     */
    std::ofstream file_edges;
    /**
     * The data structure for all added nodes
     */
    std::vector<NodeProperties> nodes;
    /**
     * Remembers if the node with the ID of the respective position
     * has already been output
     */
    std::vector<bool> is_node_output;
    
    /**
     * The edge properties which are currently used
     */
    EdgeProperties current_edge_properties;
    /**
     * The edge set that is currently being added to
     */
    std::vector<int> current_edge_set;
};

#endif
