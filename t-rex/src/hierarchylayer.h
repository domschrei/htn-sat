#ifndef HIERARCHY_LAYER_H
#define HIERARCHY_LAYER_H

#include "inc_problem.h"
#include "unordered_map"

/**
 * Represents one particular layer of the hierarchy of the 
 * problem at hand. Such an object contains all of the
 * layer-specific relevant data and can be used to compute
 * its succeeding hierarchical layer as well as to retrieve
 * logical variables for this layer's properties.
 */
class HierarchyLayer {
    
public:
    
    /**
     * Constructs and returns the initial hierarchical layer
     * of the given problem.
     */
    HierarchyLayer(IncProblem* p);
    
    /**
     * Constructs and returns the _next_ hierarchical layer 
     * succeeding this layer.
     */
    HierarchyLayer* compute_next_layer();
    
    /**
     * Returns a variable corresponding to the given literal
     * at the given position. The literal must be one of the
     * variables per depth and position in the abstract encoding
     * and may be negative (to retrieve the negated literal).
     */
    int lit_at_pos(int lit, int pos);
    
    /**
     * Appends `amount` variables to this layer's variable domain
     * and returns the first of these variable values.
     * When `amount` is zero, no change is being done and the
     * currently highest valid variable of the domain is returned.
     */
    int allocate_helper_variables(int amount);
    
    /**
     * Returns the depth of this layer (starting at zero 
     * for the initial layer).
     */
    int get_depth();
    /**
     * Returns the size of the element array encoded at this layer.
     */
    int get_array_size();
    /**
     * Returns the array position index of the next layer
     * which is the first successor of the given position index 
     * of this layer.
     */
    int get_next_position(int pos);
    /**
     * Returns the array position index of the previous layer
     * which is the predecessor of the given position index
     * of this layer.
     */
    int get_last_position(int pos);
    
    /**
     * Yes, iff the given element at the given position may occur.
     */
    bool is_elem_possible(int elem, int pos);
    /**
     * Yes, iff the given fact is encoded at the given position.
     */
    bool is_fact_possible(int fact, int pos);
    /**
     * Returns a pointer to the possible elements bitset at the
     * given position, in order to iterate quickly over them
     * (using find_first and find_next).
     */
    boost::dynamic_bitset<>* get_possible_elems(int pos);
    
    /**
     * Returns the variable which encodes the given primitive
     * element at the given position. Do not use combined
     * with lit_at_pos; instead, directly provide this variable
     * to a solver.
     */
    int get_prim_elem_variable(int pos, int prim_elem);
    /**
     * Returns the variable which encodes the given fact
     * at the given position. Do not use combined with 
     * lit_at_pos; instead, directly provide this variable
     * to a solver.
     */
    int get_fact_variable(int pos, int fact);
    /**
     * Returns all variables which encode the given primitive 
     * element at the given position (at some previous depth
     * of the hierarchy), but which have been overwritten by
     * the currently active variable for the element
     * (see get_prim_elem_variable).
     */
    std::vector<int>* get_overwritten_variables(int pos, int prim_elem);
    /**
     * Returns the highest array position index lower or equals `pos`
     * where the given fact occurred.
     */
    int get_last_fact_occurence_lower_equals(int pos, int fact);
    /**
     * Returns the lowest array position index greater or equals `pos`
     * where the given fact occurred.
     */
    int get_first_fact_occurence_greater_equals(int pos, int fact);
    /**
     * True, iff the given primitive element at the given position
     * is new, i.e. some composite element's expansion at the previous
     * layer defined this element to be there. 
     * (This query also returns true if the primitive element has been
     * possible before at this position.)
     */
    bool is_prim_elem_new(int prim_elem, int pos);
    /**
     * True, if the given fact at the given position has not been
     * encoded at previous layers, but is encoded now.
     */
    bool is_fact_new(int fact, int pos);
    
    /**
     * Returns the index of the given problem variable in the
     * compactified domain (omitting non-occurring elements and facts).
     */
    int get_compact_index(int pos, int var);
    /**
     * Returns the first of the variables allocated for At-Most-One
     * encodings over the primitive elements at the given position.
     * The amount of allocated variables is the bitlength of the
     * total number of primitive elements in the problem.
     */
    int get_amo_helper_vars(int pos);
    /**
     * Returns the first "number" which has not been used for
     * encoding an At-Most-One constraint at the given position.
     */
    int get_amo_first_unencoded_number(int pos);
    /**
     * Updates the first "number" which has not been used for
     * encoding an At-Most-One constraint at the given position.
     */
    void set_amo_first_unencoded_number(int pos, int num);
    /**
     * True, iff at each position of the layer, at least one 
     * primitive element may occur
     */
    bool contains_prim_elems_everywhere();
    /**
     * Returns the next layer of the hierarchy (given that it
     * has already been computed).
     */
    HierarchyLayer* get_next_layer();
    /**
     * Returns the previous layer of the hierarchy (given that
     * this is not the initial layer).
     */
    HierarchyLayer* get_prev_layer();
    
private:
    
    /** 
     * Private constructor to compute the successor layer.
     * The resulting object is completely empty.
     */
    HierarchyLayer();
    
    // Initial layer only
    
    /**
     * Computes the possible elements and facts at the
     * initial hierarchical layer. Only used for the first
     * layer object of the problem at hand.
     */
    void compute_init_possible_elems_and_facts();
    
    // For all layers
    
    /**
     * Computes the compact index mapping of problem variables
     * to logical variables for the next layer.
     */
    void compute_compact_indices();
    /**
     * Computes the primitive element and fact variables
     * for the next layer.
     */
    void compute_prim_elem_and_fact_variables();
    
    // Successive layers only
    
    /**
     * Computes the possible elements and the accompanying
     * data structures for the next layer.
     */
    void compute_next_possible_elems();
    /**
     * Computes the possible facts for the next layer.
     */
    void compute_next_possible_facts();
    
    // Internal helper methods adding elements to data structures
    
    void add_elem(std::vector<boost::dynamic_bitset<>>* data, int pos, int elem);
    void add_prim_elem(std::vector<boost::dynamic_bitset<>>* data, int pos, int elem);
    void add_fact(std::vector<boost::dynamic_bitset<>>* data, int pos, int fact);
    void add(std::vector<std::vector<int>>* data, int pos, int elem, int val);
    void add(std::vector<std::vector<std::vector<int>>>* data, int pos, int elem, int val);
    
private:
    /**
     * The problem at hand.
     */
    IncProblem* p;
    /**
     * The layer succeeding this layer.
     */
    HierarchyLayer* next_layer = NULL;
    /**
     * The layer preceeding this layer.
     */
    HierarchyLayer* prev_layer = NULL;
    /**
     * The depth of the hierarchy which this layer represents.
     */
    int depth;
    /**
     * The size of the encoded element array at this layer.
     */
    int array_size;
    /**
     * At position pos, contains the first array position index
     * at the next layer which succeeds pos. ("Index of first child")
     */
    std::vector<int> next_positions;
    /**
     * At position pos, contains the array position index at
     * the previous layer which preceeds pos. ("Parent index")
     */
    std::vector<int> last_positions;
    /**
     * At each position, contains a bitset of the possible elements
     * at that position.
     */
    std::vector<boost::dynamic_bitset<>> possible_elems;
    /**
     * At each position, contains a bitvector of the encoded facts
     * at that position.
     */
    std::vector<boost::dynamic_bitset<>> possible_facts;
    /**
     * True, if the encoding alternative is used where one composite
     * element may have multiple possible reductions, all of which
     * lead to differing possible successor elements at each next position
     */
    bool has_reduction_alternatives;
    /**
     * Only relevant if has_reduction_alternatives.
     * At each position, contains the maximal amount of reductions
     * of any composite element which may occur there.
     */
    std::vector<int> max_num_reductions;
    /**
     * At position (pos,elem), contains the variable representing
     * the primitive element elem at position pos, or zero if undefined.
     */
    std::vector<std::vector<int>> prim_elem_variables;
    /**
     * At position (pos,elem), contains a list of variables which
     * represent the primitive element elem at position pos 
     * (at some previous layer), but which have been overwritten by
     * the variable currently representing that element.
     */
    std::vector<std::vector<std::vector<int>>> overwritten_prim_elem_vars;
    /**
     * At position (pos,fact), contains the variable representing
     * the fact at position pos, or zero if undefined.
     */
    std::vector<std::vector<int>> fact_variables;
    /**
     * At position pos, contains a bitvector representing all 
     * primitive elements newly created at this layer and position
     * by some expansion of a composite element.
     */
    std::vector<boost::dynamic_bitset<>> new_prim_elems;
    /**
     * At position pos, contains a bitvector representing all
     * facts newly encoded at this layer and position.
     */
    std::vector<boost::dynamic_bitset<>> new_facts;
    /**
     * Maps a problem variable (1 <= var <= num_vars_per_pos_and_depth)
     * to its compressed index (omitting all non-occuring elements and facts).
     */
    std::vector<std::unordered_map<int, int>> compact_indices;
    /**
     * At position pos, contains the first variable belonging
     * to that position's domain.
     */
    std::vector<int> domain_starts;
    /**
     * Keeps track of the amount of helper variables allocated
     * at this layer. May not increase after the next layer
     * has been created.
     */
    int added_helper_vars;
    /**
     * Contains the first of the variables allocated for At-Most-One
     * constraint encodings at each position.
     */
    std::vector<int> amo_helper_var_starts;
    /**
     * Contains the first number which has not yet been used
     * in binary for an At-Most-One constraint at each position.
     */
    std::vector<int> amo_first_unencoded_number;
    
    /**
     * Int-Vector without any elements. 
     * Used for returning empty solutions.
     */
    std::vector<int> EMPTY_VECTOR;
    /**
     * Bitvector of length of the amount of elements
     * where each bit is set to true.
     */
    boost::dynamic_bitset<> ALL_ELEMS_POSSIBLE;
    
    /**
     * True iff a naïve encoding of ALL elements at ALL positions 
     * should be done.
     */
    bool encode_all_elems;
    /**
     * True iff the entire set of facts should be encoded at each
     * position, instead of compactifying them.
     */
    bool encode_all_facts;
    /**
     * True iff the primitive elements should be re-encoded at each
     * depth and position instead of reusing variables.
     */
    bool reencode_prim_elems;
};

#endif
