#include "clause_graph.h"

ClauseGraph::ClauseGraph()
{
    nodes = std::vector<NodeProperties>(); 
    is_node_output = std::vector<bool>();
    
    // Initialize file I/O
    init_files();
    
    // Initialize default properties of nodes and edges
    default_node_properties = {
        "Generic variable", // label
        true, // visible
        false, 0,0,0, // position
        false, 0,0,0, // color
        false, 0,0 // size
    };
    default_edge_properties = {
        "Generic clause", // label
        true, // visible
        true, 220,220,220 // color
    };
    current_edge_properties = default_edge_properties;
}

void ClauseGraph::init_files()
{
    // Open the .part files for nodes and edges, 
    // and output their headers
    file_nodes.open(FILENAME_NODES);
    file_edges.open(FILENAME_EDGES);
    output_node_header();
    output_edge_header();
}

void ClauseGraph::add_node(int id)
{
    add_node(id, default_node_properties);
}

void ClauseGraph::add_node(int id, NodeProperties prop)
{
    id = std::abs(id);
    
    // If the node is not output yet
    if (id >= is_node_output.size() || !is_node_output[id]) {
        // Enlarge data structures as needed
        while (id >= nodes.size()) {
            nodes.push_back(default_node_properties);
            is_node_output.push_back(false);
        }
        // Define specified node
        nodes[id] = prop;
        is_node_output[id] = false;
    }
    
    // Output the node, if necessary
    output_node(id);
}

void ClauseGraph::add_to_graph(int id)
{
    if (id == 0) {
        // End this edge set
        output_edge_set();
        current_edge_set.clear();
    } else {
        current_edge_set.push_back(std::abs(id));
    }
}

void ClauseGraph::set_edge_properties(EdgeProperties prop)
{
    current_edge_properties = prop;
}

void ClauseGraph::clear_edge_properties()
{
    current_edge_properties = default_edge_properties;
}

void ClauseGraph::output_node_header()
{
    file_nodes << "nodedef>name VARCHAR,label VARCHAR,x DOUBLE,y DOUBLE,z DOUBLE,"
                    "color VARCHAR,visible BOOLEAN,width DOUBLE,height DOUBLE\n";
}

void ClauseGraph::output_node(int id)
{
    if (is_node_output[id])
        return;
    
    NodeProperties prop = nodes[id];
    
    file_nodes << id << SEPARATOR;
    file_nodes << prop.label;
    file_nodes << SEPARATOR;
    
    if (prop.has_position)
        file_nodes << prop.x * SCALING_POS;
    file_nodes << SEPARATOR;
    if (prop.has_position)
        file_nodes << prop.y * SCALING_POS;
    file_nodes << SEPARATOR;
    if (prop.has_position)
        file_nodes << prop.z * SCALING_POS;
    file_nodes << SEPARATOR;
    
    if (prop.has_color)
        file_nodes << "'" << prop.r << "," << prop.g << "," << prop.b << "'";
    file_nodes << SEPARATOR;
    
    file_nodes << (prop.visible ? "true" : "false") << SEPARATOR;
    
    if (prop.has_size)
        file_nodes << prop.w * SCALING_SIZE;
    file_nodes << SEPARATOR;
    if (prop.has_size)
        file_nodes << prop.h * SCALING_SIZE;
    file_nodes << SEPARATOR;
    
    file_nodes << "\n";
    
    is_node_output[id] = true;
}

void ClauseGraph::output_edge_header()
{
    file_edges << "edgedef>node1 VARCHAR,node2 VARCHAR,directed BOOLEAN,color VARCHAR,label VARCHAR,visible BOOLEAN\n";
}

void ClauseGraph::output_edge_set()
{
    if (!current_edge_properties.visible)
        return;
    
    for (int i = 0; i < current_edge_set.size(); i++) {
        
        for (int j = i+1; j < current_edge_set.size(); j++) {
        
            int node1 = current_edge_set[i];
            int node2 = current_edge_set[j];
            
            // Node IDs
            file_edges << node1 << SEPARATOR;
            file_edges << node2 << SEPARATOR;
            
            // Directedness
            file_edges << "false" << SEPARATOR;
            
            if (current_edge_properties.has_color)
                file_edges << "'" << current_edge_properties.r 
                            << "," << current_edge_properties.g 
                            << "," << current_edge_properties.b 
                            << "'";
            file_edges << SEPARATOR;
            
            file_edges << current_edge_properties.label << SEPARATOR;
            
            // Visible
            file_edges << (current_edge_properties.visible ? "true" : "false") << SEPARATOR;
            
            file_edges << "\n";
        }
    }
}

void ClauseGraph::close_files()
{
    file_nodes.close();
    file_edges.close();
    
    std::ofstream out(FILENAME_MERGED);
    
    std::string line;
    
    std::ifstream in_nodes(FILENAME_NODES);
    while(std::getline(in_nodes, line))
        out << line << '\n';
    in_nodes.close();
    std::ifstream in_edges(FILENAME_EDGES);
    while(std::getline(in_edges, line))
        out << line << '\n';
    in_edges.close();
    
    out.close();
    
    remove(FILENAME_NODES);
    remove(FILENAME_EDGES);
}
