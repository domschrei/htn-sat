#!/bin/bash

domain="$1"

if [ ! $domain ]; then
    echo "Provide a valid domain name."
    exit 0
fi

mkdir -p hddl/$domain

for i in {01..20}; do
    echo "Converting problem $i ..."
    bash pddl2hddl_problem.sh $domain/p$i.pddl
    mv new_problem.hddl hddl/$domain/p$i.hddl
done

echo "Converting domain file ..."
bash pddl2hddl_domain.sh $domain/domain.pddl
cp new_domain.hddl hddl/$domain/domain.hddl

echo "Finished."
