
(define (problem strips-sat-x-1)
(:domain satellite) 
(:objects
satellite0 - satellite
instrument0 - instrument
satellite1 - satellite
instrument1 - instrument
instrument2 - instrument
instrument3 - instrument
satellite2 - satellite
instrument4 - instrument
instrument5 - instrument
thermograph2 - mode
thermograph0 - mode
image1 - mode
GroundStation0 - direction
GroundStation1 - direction
Phenomenon2 - direction
Star3 - direction
)
(:init
(supports instrument0 thermograph0)
(calibration_target instrument0 GroundStation0)
(on_board instrument0 satellite0)
(power_avail satellite0)
(pointing satellite0 GroundStation1)
(supports instrument1 thermograph2)
(supports instrument1 image1)
(supports instrument1 thermograph0)
(calibration_target instrument1 GroundStation1)
(supports instrument2 image1)
(calibration_target instrument2 GroundStation1)
(supports instrument3 thermograph2)
(supports instrument3 thermograph0)
(supports instrument3 image1)
(calibration_target instrument3 GroundStation1)
(on_board instrument1 satellite1)
(on_board instrument2 satellite1)
(on_board instrument3 satellite1)
(power_avail satellite1)
(pointing satellite1 Phenomenon2)
(supports instrument4 image1)
(supports instrument4 thermograph0)
(supports instrument4 thermograph2)
(calibration_target instrument4 GroundStation0)
(supports instrument5 image1)
(calibration_target instrument5 GroundStation1)
(on_board instrument4 satellite2)
(on_board instrument5 satellite2)
(power_avail satellite2)
(pointing satellite2 Phenomenon2)
)
(:goal (and
(have_image Phenomenon2 image1)
(have_image Star3 thermograph2)
) ))
