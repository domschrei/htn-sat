
(define (problem strips-sat-x-1)
(:domain satellite) 
(:objects
satellite0 - satellite
instrument0 - instrument
instrument1 - instrument
instrument2 - instrument
satellite1 - satellite
instrument3 - instrument
satellite2 - satellite
instrument4 - instrument
instrument5 - instrument
satellite3 - satellite
instrument6 - instrument
satellite4 - satellite
instrument7 - instrument
satellite5 - satellite
instrument8 - instrument
satellite6 - satellite
instrument9 - instrument
instrument10 - instrument
satellite7 - satellite
instrument11 - instrument
satellite8 - satellite
instrument12 - instrument
instrument13 - instrument
spectrograph0 - mode
spectrograph2 - mode
thermograph1 - mode
Star1 - direction
Star3 - direction
GroundStation2 - direction
GroundStation0 - direction
Planet4 - direction
Phenomenon5 - direction
Planet6 - direction
Phenomenon7 - direction
)
(:init
(supports instrument0 spectrograph2)
(calibration_target instrument0 Star3)
(supports instrument1 spectrograph0)
(calibration_target instrument1 GroundStation2)
(supports instrument2 thermograph1)
(supports instrument2 spectrograph2)
(calibration_target instrument2 Star3)
(on_board instrument0 satellite0)
(on_board instrument1 satellite0)
(on_board instrument2 satellite0)
(power_avail satellite0)
(pointing satellite0 Phenomenon5)
(supports instrument3 thermograph1)
(calibration_target instrument3 GroundStation2)
(on_board instrument3 satellite1)
(power_avail satellite1)
(pointing satellite1 GroundStation0)
(supports instrument4 thermograph1)
(calibration_target instrument4 Star1)
(supports instrument5 thermograph1)
(supports instrument5 spectrograph2)
(supports instrument5 spectrograph0)
(calibration_target instrument5 GroundStation2)
(on_board instrument4 satellite2)
(on_board instrument5 satellite2)
(power_avail satellite2)
(pointing satellite2 Planet4)
(supports instrument6 spectrograph0)
(calibration_target instrument6 GroundStation2)
(on_board instrument6 satellite3)
(power_avail satellite3)
(pointing satellite3 Star1)
(supports instrument7 thermograph1)
(supports instrument7 spectrograph0)
(calibration_target instrument7 Star3)
(on_board instrument7 satellite4)
(power_avail satellite4)
(pointing satellite4 GroundStation0)
(supports instrument8 thermograph1)
(supports instrument8 spectrograph2)
(calibration_target instrument8 GroundStation0)
(on_board instrument8 satellite5)
(power_avail satellite5)
(pointing satellite5 Phenomenon5)
(supports instrument9 spectrograph2)
(calibration_target instrument9 GroundStation0)
(supports instrument10 thermograph1)
(supports instrument10 spectrograph0)
(calibration_target instrument10 Star3)
(on_board instrument9 satellite6)
(on_board instrument10 satellite6)
(power_avail satellite6)
(pointing satellite6 Planet4)
(supports instrument11 spectrograph0)
(calibration_target instrument11 GroundStation0)
(on_board instrument11 satellite7)
(power_avail satellite7)
(pointing satellite7 Star3)
(supports instrument12 thermograph1)
(supports instrument12 spectrograph2)
(calibration_target instrument12 GroundStation2)
(supports instrument13 thermograph1)
(calibration_target instrument13 GroundStation0)
(on_board instrument12 satellite8)
(on_board instrument13 satellite8)
(power_avail satellite8)
(pointing satellite8 Planet4)
)
(:goal (and
(pointing satellite0 GroundStation2)
(pointing satellite1 GroundStation2)
(pointing satellite2 Star3)
(pointing satellite4 Planet6)
(have_image Planet4 spectrograph2)
(have_image Phenomenon5 spectrograph2)
(have_image Planet6 spectrograph0)
(have_image Phenomenon7 thermograph1)
) ))
