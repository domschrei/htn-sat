

(define (problem BW-rand-7)
(:domain blocksworld)
(:objects b1 b2 b3 b4 b5 b6 b7  - block)
(:init
(handempty)
(on b1 b7)
(ontable b2)
(on b3 b6)
(ontable b4)
(on b5 b1)
(on b6 b5)
(ontable b7)
(clear b2)
(clear b3)
(clear b4)
)
(:goal
(and
(on b1 b6)
(on b2 b1)
(on b3 b5)
(on b4 b2)
(on b6 b3)
(on b7 b4))
)
)


