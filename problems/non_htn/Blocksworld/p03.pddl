

(define (problem BW-rand-9)
(:domain blocksworld)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9  - block)
(:init
(handempty)
(on b1 b3)
(on b2 b5)
(ontable b3)
(ontable b4)
(on b5 b9)
(on b6 b4)
(on b7 b2)
(on b8 b1)
(on b9 b8)
(clear b6)
(clear b7)
)
(:goal
(and
(on b1 b2)
(on b2 b6)
(on b3 b4)
(on b5 b1)
(on b6 b3))
)
)


