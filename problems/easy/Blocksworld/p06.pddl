(define (problem BW-rand-10)
(:domain blocksworld)
(:requirements :strips :typing :negative-preconditions :htn :equality)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 - block)
(:init
(handempty)
(ontable b1)
(on b2 b6)
(on b3 b2)
(on b4 b5)
(on b5 b8)
(ontable b6)
(on b7 b9)
(on b8 b7)
(on b9 b1)
(ontable b10)
(clear b3)
(clear b4)
(clear b10)
)
(:goal :tasks (
(tag t1 (do_put_on b2 b4))
(tag t2 (do_put_on b5 b2))
(tag t3 (do_put_on b9 b5))
(tag t4 (do_put_on b10 b9))
(tag t5 (do_put_on b1 b10))
(tag t6 (do_put_on b7 b1))
(tag t7 (do_put_on b8 b7))
(tag t8 (do_put_on b2 b4))
(tag t9 (do_put_on b5 b2))
(tag t10 (do_put_on b9 b5))
(tag t11 (do_put_on b10 b9))
(tag t12 (do_put_on b1 b10))
(tag t13 (do_put_on b7 b1))
(tag t14 (do_put_on b8 b7))
(tag t15 (do_put_on b3 b8))
(tag t16 (do_put_on b6 b3))
) :constraints(and (after 
(and
(on b1 b10)
(on b2 b4)
(on b3 b8)
(on b5 b2)
(on b6 b3)
(on b7 b1)
(on b8 b7)
(on b9 b5)
(on b10 b9)) t16)
)
)
)
