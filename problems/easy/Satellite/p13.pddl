; ./satgen 15831 13 3 3 5 5
(define (problem strips-sat-x-1)
(:domain satellite) (:requirements :strips :typing :negative-preconditions :htn :equality)
(:objects
satellite0 - satellite
instrument0 - instrument
satellite1 - satellite
instrument1 - instrument
instrument2 - instrument
satellite2 - satellite
instrument3 - instrument
satellite3 - satellite
instrument4 - instrument
instrument5 - instrument
instrument6 - instrument
satellite4 - satellite
instrument7 - instrument
instrument8 - instrument
instrument9 - instrument
satellite5 - satellite
instrument10 - instrument
instrument11 - instrument
satellite6 - satellite
instrument12 - instrument
instrument13 - instrument
satellite7 - satellite
instrument14 - instrument
satellite8 - satellite
instrument15 - instrument
instrument16 - instrument
instrument17 - instrument
satellite9 - satellite
instrument18 - instrument
instrument19 - instrument
instrument20 - instrument
satellite10 - satellite
instrument21 - instrument
instrument22 - instrument
instrument23 - instrument
satellite11 - satellite
instrument24 - instrument
satellite12 - satellite
instrument25 - instrument
instrument26 - instrument
instrument27 - instrument
thermograph1 - mode
spectrograph2 - mode
thermograph0 - mode
GroundStation3 - direction
Star0 - direction
GroundStation1 - direction
Star4 - direction
Star2 - direction
Phenomenon5 - direction
Star6 - direction
Phenomenon7 - direction
Planet8 - direction
Phenomenon9 - direction
)
(:init
(supports instrument0 thermograph0)
(supports instrument0 spectrograph2)
(calibration_target instrument0 Star4)
(on_board instrument0 satellite0)
(power_avail satellite0)
(pointing satellite0 Star6)
(supports instrument1 thermograph0)
(supports instrument1 thermograph1)
(calibration_target instrument1 GroundStation1)
(supports instrument2 spectrograph2)
(supports instrument2 thermograph1)
(calibration_target instrument2 Star0)
(on_board instrument1 satellite1)
(on_board instrument2 satellite1)
(power_avail satellite1)
(pointing satellite1 Star2)
(supports instrument3 thermograph0)
(calibration_target instrument3 Star2)
(on_board instrument3 satellite2)
(power_avail satellite2)
(pointing satellite2 Star0)
(supports instrument4 spectrograph2)
(supports instrument4 thermograph0)
(supports instrument4 thermograph1)
(calibration_target instrument4 Star4)
(supports instrument5 spectrograph2)
(calibration_target instrument5 GroundStation3)
(supports instrument6 thermograph1)
(calibration_target instrument6 GroundStation3)
(on_board instrument4 satellite3)
(on_board instrument5 satellite3)
(on_board instrument6 satellite3)
(power_avail satellite3)
(pointing satellite3 Planet8)
(supports instrument7 thermograph0)
(supports instrument7 spectrograph2)
(supports instrument7 thermograph1)
(calibration_target instrument7 GroundStation3)
(supports instrument8 spectrograph2)
(supports instrument8 thermograph0)
(calibration_target instrument8 Star2)
(supports instrument9 thermograph0)
(calibration_target instrument9 GroundStation1)
(on_board instrument7 satellite4)
(on_board instrument8 satellite4)
(on_board instrument9 satellite4)
(power_avail satellite4)
(pointing satellite4 GroundStation1)
(supports instrument10 spectrograph2)
(supports instrument10 thermograph0)
(calibration_target instrument10 Star4)
(supports instrument11 thermograph0)
(supports instrument11 spectrograph2)
(calibration_target instrument11 Star0)
(on_board instrument10 satellite5)
(on_board instrument11 satellite5)
(power_avail satellite5)
(pointing satellite5 Star6)
(supports instrument12 spectrograph2)
(supports instrument12 thermograph1)
(calibration_target instrument12 GroundStation3)
(supports instrument13 spectrograph2)
(supports instrument13 thermograph1)
(supports instrument13 thermograph0)
(calibration_target instrument13 Star2)
(on_board instrument12 satellite6)
(on_board instrument13 satellite6)
(power_avail satellite6)
(pointing satellite6 Planet8)
(supports instrument14 thermograph1)
(supports instrument14 thermograph0)
(calibration_target instrument14 Star4)
(on_board instrument14 satellite7)
(power_avail satellite7)
(pointing satellite7 Phenomenon5)
(supports instrument15 spectrograph2)
(calibration_target instrument15 Star0)
(supports instrument16 spectrograph2)
(supports instrument16 thermograph1)
(supports instrument16 thermograph0)
(calibration_target instrument16 GroundStation1)
(supports instrument17 spectrograph2)
(calibration_target instrument17 GroundStation1)
(on_board instrument15 satellite8)
(on_board instrument16 satellite8)
(on_board instrument17 satellite8)
(power_avail satellite8)
(pointing satellite8 Planet8)
(supports instrument18 thermograph0)
(supports instrument18 thermograph1)
(calibration_target instrument18 Star2)
(supports instrument19 thermograph1)
(supports instrument19 spectrograph2)
(supports instrument19 thermograph0)
(calibration_target instrument19 Star2)
(supports instrument20 thermograph1)
(supports instrument20 spectrograph2)
(calibration_target instrument20 Star2)
(on_board instrument18 satellite9)
(on_board instrument19 satellite9)
(on_board instrument20 satellite9)
(power_avail satellite9)
(pointing satellite9 Phenomenon7)
(supports instrument21 thermograph1)
(supports instrument21 spectrograph2)
(supports instrument21 thermograph0)
(calibration_target instrument21 Star4)
(supports instrument22 spectrograph2)
(calibration_target instrument22 Star4)
(supports instrument23 spectrograph2)
(supports instrument23 thermograph1)
(calibration_target instrument23 Star4)
(on_board instrument21 satellite10)
(on_board instrument22 satellite10)
(on_board instrument23 satellite10)
(power_avail satellite10)
(pointing satellite10 Planet8)
(supports instrument24 spectrograph2)
(calibration_target instrument24 GroundStation1)
(on_board instrument24 satellite11)
(power_avail satellite11)
(pointing satellite11 Phenomenon7)
(supports instrument25 spectrograph2)
(supports instrument25 thermograph0)
(calibration_target instrument25 Star4)
(supports instrument26 spectrograph2)
(supports instrument26 thermograph1)
(supports instrument26 thermograph0)
(calibration_target instrument26 Star4)
(supports instrument27 thermograph0)
(calibration_target instrument27 Star2)
(on_board instrument25 satellite12)
(on_board instrument26 satellite12)
(on_board instrument27 satellite12)
(power_avail satellite12)
(pointing satellite12 Star6)
)
(:goal :tasks (
(tag t1 (do_turning satellite0 Phenomenon5))
(tag t2 (do_turning satellite2 GroundStation1))
(tag t3 (do_turning satellite4 Star0))
(tag t4 (do_turning satellite5 Phenomenon7))
(tag t5 (do_turning satellite6 Star2))
(tag t6 (do_turning satellite8 GroundStation3))
(tag t7 (do_mission Phenomenon5 thermograph1))
(tag t8 (do_mission Star6 thermograph0))
(tag t9 (do_mission Phenomenon7 thermograph1))
(tag t10 (do_mission Phenomenon9 spectrograph2))
)
:constraints(and (after (and
(pointing satellite0 Phenomenon5)
(pointing satellite2 GroundStation1)
(pointing satellite4 Star0)
(pointing satellite5 Phenomenon7)
(pointing satellite6 Star2)
(pointing satellite8 GroundStation3)
(have_image Phenomenon5 thermograph1)
(have_image Star6 thermograph0)
(have_image Phenomenon7 thermograph1)
(have_image Phenomenon9 spectrograph2)
) t10) )))
