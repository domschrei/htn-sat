; ./satgen 23258 11 3 3 4 4
(define (problem strips-sat-x-1)
(:domain satellite) (:requirements :strips :typing :negative-preconditions :htn :equality)
(:objects
satellite0 - satellite
instrument0 - instrument
instrument1 - instrument
satellite1 - satellite
instrument2 - instrument
instrument3 - instrument
satellite2 - satellite
instrument4 - instrument
instrument5 - instrument
satellite3 - satellite
instrument6 - instrument
instrument7 - instrument
satellite4 - satellite
instrument8 - instrument
satellite5 - satellite
instrument9 - instrument
instrument10 - instrument
instrument11 - instrument
satellite6 - satellite
instrument12 - instrument
satellite7 - satellite
instrument13 - instrument
satellite8 - satellite
instrument14 - instrument
satellite9 - satellite
instrument15 - instrument
instrument16 - instrument
instrument17 - instrument
satellite10 - satellite
instrument18 - instrument
instrument19 - instrument
instrument20 - instrument
thermograph1 - mode
image2 - mode
spectrograph0 - mode
Star2 - direction
Star0 - direction
Star3 - direction
Star1 - direction
Planet4 - direction
Phenomenon5 - direction
Star6 - direction
Phenomenon7 - direction
)
(:init
(supports instrument0 image2)
(calibration_target instrument0 Star1)
(supports instrument1 thermograph1)
(calibration_target instrument1 Star0)
(on_board instrument0 satellite0)
(on_board instrument1 satellite0)
(power_avail satellite0)
(pointing satellite0 Planet4)
(supports instrument2 image2)
(supports instrument2 spectrograph0)
(calibration_target instrument2 Star1)
(supports instrument3 thermograph1)
(supports instrument3 image2)
(supports instrument3 spectrograph0)
(calibration_target instrument3 Star1)
(on_board instrument2 satellite1)
(on_board instrument3 satellite1)
(power_avail satellite1)
(pointing satellite1 Star2)
(supports instrument4 thermograph1)
(supports instrument4 image2)
(supports instrument4 spectrograph0)
(calibration_target instrument4 Star0)
(supports instrument5 image2)
(supports instrument5 spectrograph0)
(supports instrument5 thermograph1)
(calibration_target instrument5 Star1)
(on_board instrument4 satellite2)
(on_board instrument5 satellite2)
(power_avail satellite2)
(pointing satellite2 Star2)
(supports instrument6 spectrograph0)
(calibration_target instrument6 Star3)
(supports instrument7 spectrograph0)
(supports instrument7 image2)
(calibration_target instrument7 Star1)
(on_board instrument6 satellite3)
(on_board instrument7 satellite3)
(power_avail satellite3)
(pointing satellite3 Star2)
(supports instrument8 thermograph1)
(calibration_target instrument8 Star2)
(on_board instrument8 satellite4)
(power_avail satellite4)
(pointing satellite4 Planet4)
(supports instrument9 thermograph1)
(calibration_target instrument9 Star2)
(supports instrument10 image2)
(calibration_target instrument10 Star0)
(supports instrument11 image2)
(supports instrument11 spectrograph0)
(supports instrument11 thermograph1)
(calibration_target instrument11 Star1)
(on_board instrument9 satellite5)
(on_board instrument10 satellite5)
(on_board instrument11 satellite5)
(power_avail satellite5)
(pointing satellite5 Star3)
(supports instrument12 spectrograph0)
(calibration_target instrument12 Star3)
(on_board instrument12 satellite6)
(power_avail satellite6)
(pointing satellite6 Star0)
(supports instrument13 spectrograph0)
(supports instrument13 image2)
(supports instrument13 thermograph1)
(calibration_target instrument13 Star0)
(on_board instrument13 satellite7)
(power_avail satellite7)
(pointing satellite7 Planet4)
(supports instrument14 spectrograph0)
(supports instrument14 thermograph1)
(calibration_target instrument14 Star2)
(on_board instrument14 satellite8)
(power_avail satellite8)
(pointing satellite8 Star1)
(supports instrument15 image2)
(supports instrument15 spectrograph0)
(supports instrument15 thermograph1)
(calibration_target instrument15 Star3)
(supports instrument16 spectrograph0)
(supports instrument16 thermograph1)
(supports instrument16 image2)
(calibration_target instrument16 Star2)
(supports instrument17 spectrograph0)
(supports instrument17 thermograph1)
(supports instrument17 image2)
(calibration_target instrument17 Star0)
(on_board instrument15 satellite9)
(on_board instrument16 satellite9)
(on_board instrument17 satellite9)
(power_avail satellite9)
(pointing satellite9 Star1)
(supports instrument18 image2)
(supports instrument18 spectrograph0)
(calibration_target instrument18 Star3)
(supports instrument19 thermograph1)
(supports instrument19 spectrograph0)
(calibration_target instrument19 Star1)
(supports instrument20 spectrograph0)
(supports instrument20 image2)
(calibration_target instrument20 Star1)
(on_board instrument18 satellite10)
(on_board instrument19 satellite10)
(on_board instrument20 satellite10)
(power_avail satellite10)
(pointing satellite10 Phenomenon5)
)
(:goal :tasks (
(tag t1 (do_turning satellite0 Phenomenon7))
(tag t2 (do_turning satellite2 Phenomenon5))
(tag t3 (do_turning satellite3 Star2))
(tag t4 (do_turning satellite5 Planet4))
(tag t5 (do_turning satellite6 Star0))
(tag t6 (do_turning satellite8 Phenomenon5))
(tag t7 (do_turning satellite9 Star2))
(tag t8 (do_mission Planet4 image2))
(tag t9 (do_mission Phenomenon5 spectrograph0))
(tag t10 (do_mission Star6 image2))
(tag t11 (do_mission Phenomenon7 thermograph1))
)
:constraints(and (after (and
(pointing satellite0 Phenomenon7)
(pointing satellite2 Phenomenon5)
(pointing satellite3 Star2)
(pointing satellite5 Planet4)
(pointing satellite6 Star0)
(pointing satellite8 Phenomenon5)
(pointing satellite9 Star2)
(have_image Planet4 image2)
(have_image Phenomenon5 spectrograph0)
(have_image Star6 image2)
(have_image Phenomenon7 thermograph1)
) t11) )))
