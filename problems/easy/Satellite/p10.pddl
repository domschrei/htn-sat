; ./satgen 22874 10 3 3 4 4
(define (problem strips-sat-x-1)
(:domain satellite) (:requirements :strips :typing :negative-preconditions :htn :equality)
(:objects
satellite0 - satellite
instrument0 - instrument
satellite1 - satellite
instrument1 - instrument
satellite2 - satellite
instrument2 - instrument
satellite3 - satellite
instrument3 - instrument
instrument4 - instrument
instrument5 - instrument
satellite4 - satellite
instrument6 - instrument
instrument7 - instrument
satellite5 - satellite
instrument8 - instrument
satellite6 - satellite
instrument9 - instrument
instrument10 - instrument
satellite7 - satellite
instrument11 - instrument
instrument12 - instrument
satellite8 - satellite
instrument13 - instrument
instrument14 - instrument
instrument15 - instrument
satellite9 - satellite
instrument16 - instrument
instrument17 - instrument
infrared1 - mode
spectrograph2 - mode
infrared0 - mode
GroundStation2 - direction
Star3 - direction
GroundStation0 - direction
GroundStation1 - direction
Star4 - direction
Planet5 - direction
Star6 - direction
Star7 - direction
)
(:init
(supports instrument0 spectrograph2)
(calibration_target instrument0 GroundStation2)
(on_board instrument0 satellite0)
(power_avail satellite0)
(pointing satellite0 Star4)
(supports instrument1 spectrograph2)
(calibration_target instrument1 Star3)
(on_board instrument1 satellite1)
(power_avail satellite1)
(pointing satellite1 GroundStation2)
(supports instrument2 infrared0)
(calibration_target instrument2 GroundStation0)
(on_board instrument2 satellite2)
(power_avail satellite2)
(pointing satellite2 Star6)
(supports instrument3 spectrograph2)
(calibration_target instrument3 GroundStation2)
(supports instrument4 infrared0)
(supports instrument4 infrared1)
(supports instrument4 spectrograph2)
(calibration_target instrument4 GroundStation2)
(supports instrument5 infrared1)
(calibration_target instrument5 GroundStation1)
(on_board instrument3 satellite3)
(on_board instrument4 satellite3)
(on_board instrument5 satellite3)
(power_avail satellite3)
(pointing satellite3 Planet5)
(supports instrument6 spectrograph2)
(calibration_target instrument6 GroundStation1)
(supports instrument7 infrared1)
(supports instrument7 infrared0)
(supports instrument7 spectrograph2)
(calibration_target instrument7 GroundStation2)
(on_board instrument6 satellite4)
(on_board instrument7 satellite4)
(power_avail satellite4)
(pointing satellite4 GroundStation0)
(supports instrument8 spectrograph2)
(supports instrument8 infrared1)
(supports instrument8 infrared0)
(calibration_target instrument8 Star3)
(on_board instrument8 satellite5)
(power_avail satellite5)
(pointing satellite5 Star3)
(supports instrument9 spectrograph2)
(supports instrument9 infrared1)
(supports instrument9 infrared0)
(calibration_target instrument9 Star3)
(supports instrument10 infrared1)
(supports instrument10 spectrograph2)
(supports instrument10 infrared0)
(calibration_target instrument10 GroundStation2)
(on_board instrument9 satellite6)
(on_board instrument10 satellite6)
(power_avail satellite6)
(pointing satellite6 Star3)
(supports instrument11 spectrograph2)
(calibration_target instrument11 Star3)
(supports instrument12 spectrograph2)
(supports instrument12 infrared1)
(supports instrument12 infrared0)
(calibration_target instrument12 GroundStation2)
(on_board instrument11 satellite7)
(on_board instrument12 satellite7)
(power_avail satellite7)
(pointing satellite7 Star4)
(supports instrument13 infrared0)
(calibration_target instrument13 Star3)
(supports instrument14 infrared0)
(supports instrument14 spectrograph2)
(calibration_target instrument14 GroundStation1)
(supports instrument15 spectrograph2)
(supports instrument15 infrared0)
(calibration_target instrument15 GroundStation0)
(on_board instrument13 satellite8)
(on_board instrument14 satellite8)
(on_board instrument15 satellite8)
(power_avail satellite8)
(pointing satellite8 Planet5)
(supports instrument16 spectrograph2)
(supports instrument16 infrared0)
(supports instrument16 infrared1)
(calibration_target instrument16 GroundStation1)
(supports instrument17 infrared0)
(calibration_target instrument17 GroundStation1)
(on_board instrument16 satellite9)
(on_board instrument17 satellite9)
(power_avail satellite9)
(pointing satellite9 GroundStation0)
)
(:goal :tasks (
(tag t1 (do_turning satellite0 Star4))
(tag t2 (do_turning satellite2 Star7))
(tag t3 (do_turning satellite3 Star6))
(tag t4 (do_turning satellite4 Star6))
(tag t5 (do_turning satellite6 Star4))
(tag t6 (do_turning satellite9 Star7))
(tag t7 (do_mission Star4 infrared1))
(tag t8 (do_mission Star6 infrared0))
(tag t9 (do_mission Star7 spectrograph2))
)
:constraints(and (after (and
(pointing satellite0 Star4)
(pointing satellite2 Star7)
(pointing satellite3 Star6)
(pointing satellite4 Star6)
(pointing satellite6 Star4)
(pointing satellite9 Star7)
(have_image Star4 infrared1)
(have_image Star6 infrared0)
(have_image Star7 spectrograph2)
) t9) )))
