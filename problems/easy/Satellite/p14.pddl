; ./satgen 459 14 3 3 5 5
(define (problem strips-sat-x-1)
(:domain satellite) (:requirements :strips :typing :negative-preconditions :htn :equality)
(:objects
satellite0 - satellite
instrument0 - instrument
instrument1 - instrument
satellite1 - satellite
instrument2 - instrument
instrument3 - instrument
instrument4 - instrument
satellite2 - satellite
instrument5 - instrument
instrument6 - instrument
instrument7 - instrument
satellite3 - satellite
instrument8 - instrument
instrument9 - instrument
satellite4 - satellite
instrument10 - instrument
instrument11 - instrument
satellite5 - satellite
instrument12 - instrument
satellite6 - satellite
instrument13 - instrument
instrument14 - instrument
satellite7 - satellite
instrument15 - instrument
instrument16 - instrument
satellite8 - satellite
instrument17 - instrument
instrument18 - instrument
satellite9 - satellite
instrument19 - instrument
instrument20 - instrument
instrument21 - instrument
satellite10 - satellite
instrument22 - instrument
satellite11 - satellite
instrument23 - instrument
instrument24 - instrument
instrument25 - instrument
satellite12 - satellite
instrument26 - instrument
instrument27 - instrument
instrument28 - instrument
satellite13 - satellite
instrument29 - instrument
infrared1 - mode
thermograph2 - mode
image0 - mode
GroundStation2 - direction
GroundStation3 - direction
GroundStation1 - direction
Star0 - direction
GroundStation4 - direction
Star5 - direction
Phenomenon6 - direction
Phenomenon7 - direction
Star8 - direction
Planet9 - direction
)
(:init
(supports instrument0 image0)
(supports instrument0 infrared1)
(supports instrument0 thermograph2)
(calibration_target instrument0 GroundStation1)
(supports instrument1 infrared1)
(supports instrument1 thermograph2)
(calibration_target instrument1 GroundStation1)
(on_board instrument0 satellite0)
(on_board instrument1 satellite0)
(power_avail satellite0)
(pointing satellite0 Star8)
(supports instrument2 infrared1)
(supports instrument2 image0)
(calibration_target instrument2 GroundStation2)
(supports instrument3 infrared1)
(calibration_target instrument3 GroundStation2)
(supports instrument4 thermograph2)
(supports instrument4 infrared1)
(calibration_target instrument4 GroundStation1)
(on_board instrument2 satellite1)
(on_board instrument3 satellite1)
(on_board instrument4 satellite1)
(power_avail satellite1)
(pointing satellite1 GroundStation1)
(supports instrument5 infrared1)
(calibration_target instrument5 GroundStation1)
(supports instrument6 thermograph2)
(calibration_target instrument6 Star0)
(supports instrument7 image0)
(supports instrument7 infrared1)
(supports instrument7 thermograph2)
(calibration_target instrument7 Star0)
(on_board instrument5 satellite2)
(on_board instrument6 satellite2)
(on_board instrument7 satellite2)
(power_avail satellite2)
(pointing satellite2 Planet9)
(supports instrument8 infrared1)
(calibration_target instrument8 Star0)
(supports instrument9 image0)
(supports instrument9 thermograph2)
(supports instrument9 infrared1)
(calibration_target instrument9 GroundStation4)
(on_board instrument8 satellite3)
(on_board instrument9 satellite3)
(power_avail satellite3)
(pointing satellite3 Star5)
(supports instrument10 thermograph2)
(supports instrument10 image0)
(supports instrument10 infrared1)
(calibration_target instrument10 GroundStation2)
(supports instrument11 image0)
(calibration_target instrument11 Star0)
(on_board instrument10 satellite4)
(on_board instrument11 satellite4)
(power_avail satellite4)
(pointing satellite4 Star0)
(supports instrument12 infrared1)
(supports instrument12 thermograph2)
(supports instrument12 image0)
(calibration_target instrument12 GroundStation4)
(on_board instrument12 satellite5)
(power_avail satellite5)
(pointing satellite5 Planet9)
(supports instrument13 thermograph2)
(supports instrument13 infrared1)
(supports instrument13 image0)
(calibration_target instrument13 GroundStation4)
(supports instrument14 infrared1)
(supports instrument14 thermograph2)
(supports instrument14 image0)
(calibration_target instrument14 GroundStation3)
(on_board instrument13 satellite6)
(on_board instrument14 satellite6)
(power_avail satellite6)
(pointing satellite6 Star8)
(supports instrument15 thermograph2)
(calibration_target instrument15 GroundStation1)
(supports instrument16 thermograph2)
(calibration_target instrument16 GroundStation1)
(on_board instrument15 satellite7)
(on_board instrument16 satellite7)
(power_avail satellite7)
(pointing satellite7 Phenomenon6)
(supports instrument17 infrared1)
(supports instrument17 image0)
(calibration_target instrument17 GroundStation2)
(supports instrument18 infrared1)
(supports instrument18 image0)
(supports instrument18 thermograph2)
(calibration_target instrument18 Star0)
(on_board instrument17 satellite8)
(on_board instrument18 satellite8)
(power_avail satellite8)
(pointing satellite8 GroundStation4)
(supports instrument19 infrared1)
(supports instrument19 image0)
(calibration_target instrument19 GroundStation3)
(supports instrument20 thermograph2)
(supports instrument20 infrared1)
(supports instrument20 image0)
(calibration_target instrument20 GroundStation2)
(supports instrument21 thermograph2)
(supports instrument21 infrared1)
(supports instrument21 image0)
(calibration_target instrument21 GroundStation3)
(on_board instrument19 satellite9)
(on_board instrument20 satellite9)
(on_board instrument21 satellite9)
(power_avail satellite9)
(pointing satellite9 Phenomenon6)
(supports instrument22 thermograph2)
(supports instrument22 infrared1)
(calibration_target instrument22 GroundStation1)
(on_board instrument22 satellite10)
(power_avail satellite10)
(pointing satellite10 GroundStation4)
(supports instrument23 image0)
(calibration_target instrument23 GroundStation3)
(supports instrument24 thermograph2)
(supports instrument24 infrared1)
(calibration_target instrument24 Star0)
(supports instrument25 image0)
(supports instrument25 thermograph2)
(calibration_target instrument25 GroundStation4)
(on_board instrument23 satellite11)
(on_board instrument24 satellite11)
(on_board instrument25 satellite11)
(power_avail satellite11)
(pointing satellite11 Star5)
(supports instrument26 thermograph2)
(supports instrument26 image0)
(calibration_target instrument26 GroundStation3)
(supports instrument27 image0)
(supports instrument27 thermograph2)
(calibration_target instrument27 GroundStation1)
(supports instrument28 image0)
(calibration_target instrument28 Star0)
(on_board instrument26 satellite12)
(on_board instrument27 satellite12)
(on_board instrument28 satellite12)
(power_avail satellite12)
(pointing satellite12 Phenomenon7)
(supports instrument29 image0)
(calibration_target instrument29 GroundStation4)
(on_board instrument29 satellite13)
(power_avail satellite13)
(pointing satellite13 Planet9)
)
(:goal :tasks (
(tag t1 (do_turning satellite2 Phenomenon7))
(tag t2 (do_turning satellite4 GroundStation2))
(tag t3 (do_turning satellite5 GroundStation2))
(tag t4 (do_turning satellite6 Star0))
(tag t5 (do_turning satellite11 Star5))
(tag t6 (do_turning satellite13 GroundStation1))
(tag t7 (do_mission Star5 infrared1))
(tag t8 (do_mission Phenomenon6 thermograph2))
(tag t9 (do_mission Phenomenon7 thermograph2))
(tag t10 (do_mission Star8 infrared1))
(tag t11 (do_mission Planet9 thermograph2))
)
:constraints(and (after (and
(pointing satellite2 Phenomenon7)
(pointing satellite4 GroundStation2)
(pointing satellite5 GroundStation2)
(pointing satellite6 Star0)
(pointing satellite11 Star5)
(pointing satellite13 GroundStation1)
(have_image Star5 infrared1)
(have_image Phenomenon6 thermograph2)
(have_image Phenomenon7 thermograph2)
(have_image Star8 infrared1)
(have_image Planet9 thermograph2)
) t11) )))
