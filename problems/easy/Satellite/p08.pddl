; ./satgen 32284 8 3 3 3 3
(define (problem strips-sat-x-1)
(:domain satellite) (:requirements :strips :typing :negative-preconditions :htn :equality)
(:objects
satellite0 - satellite
instrument0 - instrument
satellite1 - satellite
instrument1 - instrument
instrument2 - instrument
instrument3 - instrument
satellite2 - satellite
instrument4 - instrument
satellite3 - satellite
instrument5 - instrument
satellite4 - satellite
instrument6 - instrument
instrument7 - instrument
instrument8 - instrument
satellite5 - satellite
instrument9 - instrument
instrument10 - instrument
instrument11 - instrument
satellite6 - satellite
instrument12 - instrument
satellite7 - satellite
instrument13 - instrument
image0 - mode
spectrograph2 - mode
infrared1 - mode
GroundStation0 - direction
Star1 - direction
GroundStation2 - direction
Planet3 - direction
Planet4 - direction
Star5 - direction
)
(:init
(supports instrument0 spectrograph2)
(calibration_target instrument0 GroundStation0)
(on_board instrument0 satellite0)
(power_avail satellite0)
(pointing satellite0 GroundStation0)
(supports instrument1 image0)
(calibration_target instrument1 GroundStation0)
(supports instrument2 spectrograph2)
(supports instrument2 image0)
(supports instrument2 infrared1)
(calibration_target instrument2 GroundStation0)
(supports instrument3 infrared1)
(supports instrument3 spectrograph2)
(calibration_target instrument3 GroundStation2)
(on_board instrument1 satellite1)
(on_board instrument2 satellite1)
(on_board instrument3 satellite1)
(power_avail satellite1)
(pointing satellite1 Planet3)
(supports instrument4 infrared1)
(supports instrument4 image0)
(supports instrument4 spectrograph2)
(calibration_target instrument4 GroundStation0)
(on_board instrument4 satellite2)
(power_avail satellite2)
(pointing satellite2 GroundStation0)
(supports instrument5 image0)
(calibration_target instrument5 Star1)
(on_board instrument5 satellite3)
(power_avail satellite3)
(pointing satellite3 Star1)
(supports instrument6 image0)
(supports instrument6 infrared1)
(supports instrument6 spectrograph2)
(calibration_target instrument6 GroundStation2)
(supports instrument7 image0)
(supports instrument7 infrared1)
(calibration_target instrument7 GroundStation0)
(supports instrument8 infrared1)
(calibration_target instrument8 Star1)
(on_board instrument6 satellite4)
(on_board instrument7 satellite4)
(on_board instrument8 satellite4)
(power_avail satellite4)
(pointing satellite4 Star5)
(supports instrument9 infrared1)
(supports instrument9 spectrograph2)
(calibration_target instrument9 GroundStation2)
(supports instrument10 image0)
(supports instrument10 infrared1)
(supports instrument10 spectrograph2)
(calibration_target instrument10 GroundStation0)
(supports instrument11 image0)
(supports instrument11 infrared1)
(calibration_target instrument11 GroundStation0)
(on_board instrument9 satellite5)
(on_board instrument10 satellite5)
(on_board instrument11 satellite5)
(power_avail satellite5)
(pointing satellite5 GroundStation0)
(supports instrument12 spectrograph2)
(supports instrument12 image0)
(calibration_target instrument12 Star1)
(on_board instrument12 satellite6)
(power_avail satellite6)
(pointing satellite6 Star1)
(supports instrument13 infrared1)
(calibration_target instrument13 GroundStation2)
(on_board instrument13 satellite7)
(power_avail satellite7)
(pointing satellite7 GroundStation0)
)
(:goal :tasks (
(tag t1 (do_turning satellite0 Star1))
(tag t2 (do_turning satellite3 GroundStation2))
(tag t3 (do_mission Planet3 spectrograph2))
(tag t4 (do_mission Planet4 spectrograph2))
(tag t5 (do_mission Star5 image0))
)
:constraints(and (after (and
(pointing satellite0 Star1)
(pointing satellite3 GroundStation2)
(have_image Planet3 spectrograph2)
(have_image Planet4 spectrograph2)
(have_image Star5 image0)
) t5) )))
