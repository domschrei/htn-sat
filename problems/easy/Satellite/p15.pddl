; ./satgen 14132 15 3 3 6 6
(define (problem strips-sat-x-1)
(:domain satellite) (:requirements :strips :typing :negative-preconditions :htn :equality)
(:objects
satellite0 - satellite
instrument0 - instrument
instrument1 - instrument
satellite1 - satellite
instrument2 - instrument
instrument3 - instrument
satellite2 - satellite
instrument4 - instrument
satellite3 - satellite
instrument5 - instrument
satellite4 - satellite
instrument6 - instrument
instrument7 - instrument
instrument8 - instrument
satellite5 - satellite
instrument9 - instrument
instrument10 - instrument
instrument11 - instrument
satellite6 - satellite
instrument12 - instrument
satellite7 - satellite
instrument13 - instrument
satellite8 - satellite
instrument14 - instrument
satellite9 - satellite
instrument15 - instrument
instrument16 - instrument
satellite10 - satellite
instrument17 - instrument
instrument18 - instrument
instrument19 - instrument
satellite11 - satellite
instrument20 - instrument
instrument21 - instrument
satellite12 - satellite
instrument22 - instrument
instrument23 - instrument
instrument24 - instrument
satellite13 - satellite
instrument25 - instrument
satellite14 - satellite
instrument26 - instrument
image2 - mode
infrared1 - mode
spectrograph0 - mode
GroundStation0 - direction
GroundStation5 - direction
GroundStation3 - direction
GroundStation1 - direction
Star4 - direction
Star2 - direction
Star6 - direction
Planet7 - direction
Star8 - direction
Star9 - direction
Planet10 - direction
Star11 - direction
)
(:init
(supports instrument0 image2)
(supports instrument0 infrared1)
(supports instrument0 spectrograph0)
(calibration_target instrument0 GroundStation0)
(supports instrument1 image2)
(supports instrument1 spectrograph0)
(calibration_target instrument1 GroundStation1)
(calibration_target instrument1 Star4)
(on_board instrument0 satellite0)
(on_board instrument1 satellite0)
(power_avail satellite0)
(pointing satellite0 Star6)
(supports instrument2 image2)
(calibration_target instrument2 GroundStation3)
(supports instrument3 infrared1)
(supports instrument3 spectrograph0)
(calibration_target instrument3 Star2)
(on_board instrument2 satellite1)
(on_board instrument3 satellite1)
(power_avail satellite1)
(pointing satellite1 Star8)
(supports instrument4 image2)
(calibration_target instrument4 GroundStation3)
(on_board instrument4 satellite2)
(power_avail satellite2)
(pointing satellite2 GroundStation1)
(supports instrument5 infrared1)
(supports instrument5 image2)
(calibration_target instrument5 GroundStation3)
(calibration_target instrument5 GroundStation0)
(on_board instrument5 satellite3)
(power_avail satellite3)
(pointing satellite3 Star4)
(supports instrument6 infrared1)
(supports instrument6 image2)
(calibration_target instrument6 GroundStation3)
(calibration_target instrument6 Star2)
(supports instrument7 spectrograph0)
(supports instrument7 image2)
(supports instrument7 infrared1)
(calibration_target instrument7 Star4)
(calibration_target instrument7 GroundStation3)
(supports instrument8 spectrograph0)
(supports instrument8 infrared1)
(calibration_target instrument8 GroundStation5)
(on_board instrument6 satellite4)
(on_board instrument7 satellite4)
(on_board instrument8 satellite4)
(power_avail satellite4)
(pointing satellite4 Planet7)
(supports instrument9 spectrograph0)
(supports instrument9 image2)
(calibration_target instrument9 GroundStation3)
(supports instrument10 image2)
(supports instrument10 infrared1)
(supports instrument10 spectrograph0)
(calibration_target instrument10 Star2)
(calibration_target instrument10 GroundStation0)
(supports instrument11 image2)
(calibration_target instrument11 GroundStation3)
(on_board instrument9 satellite5)
(on_board instrument10 satellite5)
(on_board instrument11 satellite5)
(power_avail satellite5)
(pointing satellite5 Planet10)
(supports instrument12 spectrograph0)
(supports instrument12 infrared1)
(calibration_target instrument12 Star2)
(on_board instrument12 satellite6)
(power_avail satellite6)
(pointing satellite6 Star11)
(supports instrument13 spectrograph0)
(supports instrument13 image2)
(calibration_target instrument13 GroundStation3)
(calibration_target instrument13 Star2)
(on_board instrument13 satellite7)
(power_avail satellite7)
(pointing satellite7 Star2)
(supports instrument14 infrared1)
(supports instrument14 image2)
(supports instrument14 spectrograph0)
(calibration_target instrument14 GroundStation5)
(on_board instrument14 satellite8)
(power_avail satellite8)
(pointing satellite8 GroundStation0)
(supports instrument15 image2)
(supports instrument15 infrared1)
(supports instrument15 spectrograph0)
(calibration_target instrument15 Star2)
(supports instrument16 infrared1)
(calibration_target instrument16 Star4)
(calibration_target instrument16 Star2)
(on_board instrument15 satellite9)
(on_board instrument16 satellite9)
(power_avail satellite9)
(pointing satellite9 Star8)
(supports instrument17 infrared1)
(calibration_target instrument17 GroundStation1)
(supports instrument18 infrared1)
(supports instrument18 spectrograph0)
(supports instrument18 image2)
(calibration_target instrument18 GroundStation1)
(supports instrument19 infrared1)
(calibration_target instrument19 Star4)
(on_board instrument17 satellite10)
(on_board instrument18 satellite10)
(on_board instrument19 satellite10)
(power_avail satellite10)
(pointing satellite10 Star2)
(supports instrument20 infrared1)
(supports instrument20 spectrograph0)
(calibration_target instrument20 GroundStation1)
(calibration_target instrument20 GroundStation3)
(supports instrument21 spectrograph0)
(calibration_target instrument21 Star4)
(calibration_target instrument21 GroundStation5)
(on_board instrument20 satellite11)
(on_board instrument21 satellite11)
(power_avail satellite11)
(pointing satellite11 GroundStation0)
(supports instrument22 infrared1)
(supports instrument22 image2)
(supports instrument22 spectrograph0)
(calibration_target instrument22 GroundStation1)
(calibration_target instrument22 Star4)
(supports instrument23 infrared1)
(supports instrument23 spectrograph0)
(calibration_target instrument23 GroundStation5)
(supports instrument24 spectrograph0)
(supports instrument24 infrared1)
(calibration_target instrument24 GroundStation3)
(calibration_target instrument24 Star2)
(on_board instrument22 satellite12)
(on_board instrument23 satellite12)
(on_board instrument24 satellite12)
(power_avail satellite12)
(pointing satellite12 Star6)
(supports instrument25 infrared1)
(supports instrument25 spectrograph0)
(calibration_target instrument25 GroundStation1)
(on_board instrument25 satellite13)
(power_avail satellite13)
(pointing satellite13 Star2)
(supports instrument26 spectrograph0)
(supports instrument26 infrared1)
(supports instrument26 image2)
(calibration_target instrument26 Star2)
(calibration_target instrument26 Star4)
(on_board instrument26 satellite14)
(power_avail satellite14)
(pointing satellite14 GroundStation5)
)
(:goal :tasks (
(tag t1 (do_turning satellite3 Star9))
(tag t2 (do_turning satellite4 GroundStation3))
(tag t3 (do_turning satellite12 Planet10))
(tag t4 (do_mission Star6 spectrograph0))
(tag t5 (do_mission Planet7 infrared1))
(tag t6 (do_mission Star8 image2))
(tag t7 (do_mission Star9 infrared1))
)
:constraints(and (after (and
(pointing satellite3 Star9)
(pointing satellite4 GroundStation3)
(pointing satellite12 Planet10)
(have_image Star6 spectrograph0)
(have_image Planet7 infrared1)
(have_image Star8 image2)
(have_image Star9 infrared1)
) t7) )))
