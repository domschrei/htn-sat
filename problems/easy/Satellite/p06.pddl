; ./satgen 7085 6 3 3 3 3
(define (problem strips-sat-x-1)
(:domain satellite) (:requirements :strips :typing :negative-preconditions :htn :equality)
(:objects
satellite0 - satellite
instrument0 - instrument
instrument1 - instrument
instrument2 - instrument
satellite1 - satellite
instrument3 - instrument
satellite2 - satellite
instrument4 - instrument
satellite3 - satellite
instrument5 - instrument
instrument6 - instrument
instrument7 - instrument
satellite4 - satellite
instrument8 - instrument
satellite5 - satellite
instrument9 - instrument
instrument10 - instrument
infrared0 - mode
infrared1 - mode
infrared2 - mode
GroundStation1 - direction
GroundStation0 - direction
GroundStation2 - direction
Planet3 - direction
Star4 - direction
Star5 - direction
)
(:init
(supports instrument0 infrared0)
(calibration_target instrument0 GroundStation2)
(supports instrument1 infrared2)
(calibration_target instrument1 GroundStation0)
(supports instrument2 infrared0)
(calibration_target instrument2 GroundStation2)
(on_board instrument0 satellite0)
(on_board instrument1 satellite0)
(on_board instrument2 satellite0)
(power_avail satellite0)
(pointing satellite0 GroundStation0)
(supports instrument3 infrared0)
(supports instrument3 infrared1)
(supports instrument3 infrared2)
(calibration_target instrument3 GroundStation0)
(on_board instrument3 satellite1)
(power_avail satellite1)
(pointing satellite1 GroundStation2)
(supports instrument4 infrared0)
(supports instrument4 infrared2)
(supports instrument4 infrared1)
(calibration_target instrument4 GroundStation1)
(on_board instrument4 satellite2)
(power_avail satellite2)
(pointing satellite2 GroundStation2)
(supports instrument5 infrared2)
(calibration_target instrument5 GroundStation2)
(supports instrument6 infrared1)
(calibration_target instrument6 GroundStation2)
(supports instrument7 infrared0)
(supports instrument7 infrared1)
(supports instrument7 infrared2)
(calibration_target instrument7 GroundStation2)
(on_board instrument5 satellite3)
(on_board instrument6 satellite3)
(on_board instrument7 satellite3)
(power_avail satellite3)
(pointing satellite3 Star5)
(supports instrument8 infrared2)
(calibration_target instrument8 GroundStation0)
(on_board instrument8 satellite4)
(power_avail satellite4)
(pointing satellite4 GroundStation2)
(supports instrument9 infrared1)
(supports instrument9 infrared2)
(supports instrument9 infrared0)
(calibration_target instrument9 GroundStation2)
(supports instrument10 infrared2)
(calibration_target instrument10 GroundStation2)
(on_board instrument9 satellite5)
(on_board instrument10 satellite5)
(power_avail satellite5)
(pointing satellite5 Star5)
)
(:goal :tasks (
(tag t1 (do_mission Planet3 infrared2))
(tag t2 (do_mission Star4 infrared2))
(tag t3 (do_mission Star5 infrared1))
)
:constraints(and (after (and
(have_image Planet3 infrared2)
(have_image Star4 infrared2)
(have_image Star5 infrared1)
) t3) )))
