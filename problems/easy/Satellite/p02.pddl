; ./satgen 32106 2 3 3 1 1
(define (problem strips-sat-x-1)
(:domain satellite) (:requirements :strips :typing :negative-preconditions :htn :equality)
(:objects
satellite0 - satellite
instrument0 - instrument
satellite1 - satellite
instrument1 - instrument
instrument2 - instrument
instrument3 - instrument
infrared0 - mode
thermograph2 - mode
infrared1 - mode
GroundStation0 - direction
Phenomenon1 - direction
)
(:init
(supports instrument0 thermograph2)
(supports instrument0 infrared0)
(supports instrument0 infrared1)
(calibration_target instrument0 GroundStation0)
(on_board instrument0 satellite0)
(power_avail satellite0)
(pointing satellite0 Phenomenon1)
(supports instrument1 thermograph2)
(supports instrument1 infrared0)
(calibration_target instrument1 GroundStation0)
(supports instrument2 thermograph2)
(supports instrument2 infrared1)
(calibration_target instrument2 GroundStation0)
(supports instrument3 infrared1)
(calibration_target instrument3 GroundStation0)
(on_board instrument1 satellite1)
(on_board instrument2 satellite1)
(on_board instrument3 satellite1)
(power_avail satellite1)
(pointing satellite1 GroundStation0)
)
(:goal :tasks (
(tag t1 (do_mission Phenomenon1 infrared1))
)
:constraints(and (after (and
(have_image Phenomenon1 infrared1)
) t1) )))
