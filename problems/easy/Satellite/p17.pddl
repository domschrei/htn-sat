; ./satgen 5937 17 3 3 6 6
(define (problem strips-sat-x-1)
(:domain satellite) (:requirements :strips :typing :negative-preconditions :htn :equality)
(:objects
satellite0 - satellite
instrument0 - instrument
satellite1 - satellite
instrument1 - instrument
instrument2 - instrument
instrument3 - instrument
satellite2 - satellite
instrument4 - instrument
instrument5 - instrument
instrument6 - instrument
satellite3 - satellite
instrument7 - instrument
instrument8 - instrument
instrument9 - instrument
satellite4 - satellite
instrument10 - instrument
satellite5 - satellite
instrument11 - instrument
satellite6 - satellite
instrument12 - instrument
satellite7 - satellite
instrument13 - instrument
instrument14 - instrument
satellite8 - satellite
instrument15 - instrument
instrument16 - instrument
satellite9 - satellite
instrument17 - instrument
instrument18 - instrument
instrument19 - instrument
satellite10 - satellite
instrument20 - instrument
instrument21 - instrument
satellite11 - satellite
instrument22 - instrument
instrument23 - instrument
instrument24 - instrument
satellite12 - satellite
instrument25 - instrument
satellite13 - satellite
instrument26 - instrument
satellite14 - satellite
instrument27 - instrument
instrument28 - instrument
satellite15 - satellite
instrument29 - instrument
satellite16 - satellite
instrument30 - instrument
thermograph2 - mode
thermograph1 - mode
spectrograph0 - mode
Star4 - direction
GroundStation5 - direction
GroundStation3 - direction
Star1 - direction
GroundStation2 - direction
Star0 - direction
Star6 - direction
Planet7 - direction
Planet8 - direction
Phenomenon9 - direction
Phenomenon10 - direction
Planet11 - direction
)
(:init
(supports instrument0 spectrograph0)
(supports instrument0 thermograph2)
(calibration_target instrument0 GroundStation3)
(on_board instrument0 satellite0)
(power_avail satellite0)
(pointing satellite0 GroundStation3)
(supports instrument1 thermograph2)
(supports instrument1 thermograph1)
(calibration_target instrument1 GroundStation2)
(calibration_target instrument1 GroundStation3)
(supports instrument2 thermograph1)
(supports instrument2 thermograph2)
(supports instrument2 spectrograph0)
(calibration_target instrument2 GroundStation3)
(supports instrument3 thermograph1)
(calibration_target instrument3 GroundStation5)
(on_board instrument1 satellite1)
(on_board instrument2 satellite1)
(on_board instrument3 satellite1)
(power_avail satellite1)
(pointing satellite1 GroundStation3)
(supports instrument4 thermograph1)
(calibration_target instrument4 Star0)
(calibration_target instrument4 Star4)
(supports instrument5 thermograph1)
(calibration_target instrument5 GroundStation2)
(supports instrument6 thermograph2)
(supports instrument6 spectrograph0)
(supports instrument6 thermograph1)
(calibration_target instrument6 Star0)
(calibration_target instrument6 GroundStation3)
(on_board instrument4 satellite2)
(on_board instrument5 satellite2)
(on_board instrument6 satellite2)
(power_avail satellite2)
(pointing satellite2 GroundStation5)
(supports instrument7 thermograph1)
(supports instrument7 spectrograph0)
(calibration_target instrument7 Star0)
(calibration_target instrument7 Star1)
(supports instrument8 thermograph2)
(supports instrument8 thermograph1)
(calibration_target instrument8 GroundStation2)
(supports instrument9 spectrograph0)
(supports instrument9 thermograph1)
(calibration_target instrument9 Star4)
(calibration_target instrument9 GroundStation5)
(on_board instrument7 satellite3)
(on_board instrument8 satellite3)
(on_board instrument9 satellite3)
(power_avail satellite3)
(pointing satellite3 Planet7)
(supports instrument10 thermograph2)
(calibration_target instrument10 Star1)
(calibration_target instrument10 Star0)
(on_board instrument10 satellite4)
(power_avail satellite4)
(pointing satellite4 Planet11)
(supports instrument11 thermograph2)
(supports instrument11 spectrograph0)
(calibration_target instrument11 GroundStation5)
(calibration_target instrument11 Star0)
(on_board instrument11 satellite5)
(power_avail satellite5)
(pointing satellite5 Phenomenon10)
(supports instrument12 spectrograph0)
(supports instrument12 thermograph1)
(calibration_target instrument12 Star4)
(on_board instrument12 satellite6)
(power_avail satellite6)
(pointing satellite6 Star1)
(supports instrument13 thermograph2)
(supports instrument13 spectrograph0)
(supports instrument13 thermograph1)
(calibration_target instrument13 Star0)
(supports instrument14 thermograph1)
(supports instrument14 spectrograph0)
(supports instrument14 thermograph2)
(calibration_target instrument14 Star4)
(on_board instrument13 satellite7)
(on_board instrument14 satellite7)
(power_avail satellite7)
(pointing satellite7 Star6)
(supports instrument15 thermograph1)
(supports instrument15 spectrograph0)
(supports instrument15 thermograph2)
(calibration_target instrument15 Star1)
(supports instrument16 spectrograph0)
(calibration_target instrument16 GroundStation3)
(on_board instrument15 satellite8)
(on_board instrument16 satellite8)
(power_avail satellite8)
(pointing satellite8 Star4)
(supports instrument17 thermograph2)
(supports instrument17 thermograph1)
(supports instrument17 spectrograph0)
(calibration_target instrument17 Star4)
(calibration_target instrument17 GroundStation5)
(supports instrument18 spectrograph0)
(supports instrument18 thermograph1)
(calibration_target instrument18 Star0)
(supports instrument19 thermograph2)
(supports instrument19 spectrograph0)
(supports instrument19 thermograph1)
(calibration_target instrument19 Star1)
(on_board instrument17 satellite9)
(on_board instrument18 satellite9)
(on_board instrument19 satellite9)
(power_avail satellite9)
(pointing satellite9 Planet8)
(supports instrument20 spectrograph0)
(calibration_target instrument20 GroundStation3)
(supports instrument21 thermograph1)
(supports instrument21 thermograph2)
(supports instrument21 spectrograph0)
(calibration_target instrument21 GroundStation3)
(calibration_target instrument21 Star0)
(on_board instrument20 satellite10)
(on_board instrument21 satellite10)
(power_avail satellite10)
(pointing satellite10 GroundStation3)
(supports instrument22 thermograph1)
(supports instrument22 spectrograph0)
(supports instrument22 thermograph2)
(calibration_target instrument22 GroundStation2)
(calibration_target instrument22 GroundStation5)
(supports instrument23 spectrograph0)
(supports instrument23 thermograph2)
(calibration_target instrument23 GroundStation2)
(calibration_target instrument23 GroundStation5)
(supports instrument24 thermograph2)
(supports instrument24 spectrograph0)
(supports instrument24 thermograph1)
(calibration_target instrument24 GroundStation3)
(on_board instrument22 satellite11)
(on_board instrument23 satellite11)
(on_board instrument24 satellite11)
(power_avail satellite11)
(pointing satellite11 GroundStation2)
(supports instrument25 thermograph1)
(supports instrument25 spectrograph0)
(supports instrument25 thermograph2)
(calibration_target instrument25 Star0)
(on_board instrument25 satellite12)
(power_avail satellite12)
(pointing satellite12 GroundStation5)
(supports instrument26 thermograph1)
(supports instrument26 spectrograph0)
(calibration_target instrument26 GroundStation2)
(on_board instrument26 satellite13)
(power_avail satellite13)
(pointing satellite13 Planet7)
(supports instrument27 thermograph1)
(supports instrument27 thermograph2)
(calibration_target instrument27 GroundStation3)
(calibration_target instrument27 GroundStation5)
(supports instrument28 spectrograph0)
(supports instrument28 thermograph1)
(supports instrument28 thermograph2)
(calibration_target instrument28 Star1)
(calibration_target instrument28 GroundStation2)
(on_board instrument27 satellite14)
(on_board instrument28 satellite14)
(power_avail satellite14)
(pointing satellite14 Star0)
(supports instrument29 spectrograph0)
(calibration_target instrument29 GroundStation2)
(on_board instrument29 satellite15)
(power_avail satellite15)
(pointing satellite15 Phenomenon9)
(supports instrument30 spectrograph0)
(supports instrument30 thermograph1)
(supports instrument30 thermograph2)
(calibration_target instrument30 Star0)
(on_board instrument30 satellite16)
(power_avail satellite16)
(pointing satellite16 Planet8)
)
(:goal :tasks (
(tag t1 (do_turning satellite0 Star0))
(tag t2 (do_turning satellite3 Star0))
(tag t3 (do_turning satellite5 Star6))
(tag t4 (do_turning satellite6 GroundStation3))
(tag t5 (do_turning satellite8 Planet8))
(tag t6 (do_turning satellite10 Star1))
(tag t7 (do_turning satellite12 Star1))
(tag t8 (do_turning satellite14 GroundStation3))
(tag t9 (do_mission Star6 spectrograph0))
(tag t10 (do_mission Planet8 thermograph1))
(tag t11 (do_mission Phenomenon9 thermograph2))
(tag t12 (do_mission Phenomenon10 thermograph2))
(tag t13 (do_mission Planet11 spectrograph0))
)
:constraints(and (after (and
(pointing satellite0 Star0)
(pointing satellite3 Star0)
(pointing satellite5 Star6)
(pointing satellite6 GroundStation3)
(pointing satellite8 Planet8)
(pointing satellite10 Star1)
(pointing satellite12 Star1)
(pointing satellite14 GroundStation3)
(have_image Star6 spectrograph0)
(have_image Planet8 thermograph1)
(have_image Phenomenon9 thermograph2)
(have_image Phenomenon10 thermograph2)
(have_image Planet11 spectrograph0)
) t13) )))
