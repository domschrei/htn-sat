; ./satgen 15444 12 3 3 5 5
(define (problem strips-sat-x-1)
(:domain satellite) (:requirements :strips :typing :negative-preconditions :htn :equality)
(:objects
satellite0 - satellite
instrument0 - instrument
instrument1 - instrument
instrument2 - instrument
satellite1 - satellite
instrument3 - instrument
instrument4 - instrument
instrument5 - instrument
satellite2 - satellite
instrument6 - instrument
instrument7 - instrument
instrument8 - instrument
satellite3 - satellite
instrument9 - instrument
satellite4 - satellite
instrument10 - instrument
instrument11 - instrument
satellite5 - satellite
instrument12 - instrument
instrument13 - instrument
instrument14 - instrument
satellite6 - satellite
instrument15 - instrument
instrument16 - instrument
satellite7 - satellite
instrument17 - instrument
instrument18 - instrument
satellite8 - satellite
instrument19 - instrument
satellite9 - satellite
instrument20 - instrument
instrument21 - instrument
instrument22 - instrument
satellite10 - satellite
instrument23 - instrument
instrument24 - instrument
instrument25 - instrument
satellite11 - satellite
instrument26 - instrument
instrument27 - instrument
instrument28 - instrument
spectrograph1 - mode
image0 - mode
infrared2 - mode
GroundStation2 - direction
Star3 - direction
GroundStation4 - direction
GroundStation1 - direction
Star0 - direction
Star5 - direction
Phenomenon6 - direction
Planet7 - direction
Star8 - direction
Planet9 - direction
)
(:init
(supports instrument0 infrared2)
(supports instrument0 spectrograph1)
(supports instrument0 image0)
(calibration_target instrument0 GroundStation1)
(supports instrument1 spectrograph1)
(calibration_target instrument1 GroundStation4)
(supports instrument2 spectrograph1)
(supports instrument2 image0)
(supports instrument2 infrared2)
(calibration_target instrument2 Star0)
(on_board instrument0 satellite0)
(on_board instrument1 satellite0)
(on_board instrument2 satellite0)
(power_avail satellite0)
(pointing satellite0 Planet7)
(supports instrument3 image0)
(calibration_target instrument3 Star3)
(supports instrument4 infrared2)
(calibration_target instrument4 GroundStation1)
(supports instrument5 spectrograph1)
(supports instrument5 image0)
(calibration_target instrument5 GroundStation1)
(on_board instrument3 satellite1)
(on_board instrument4 satellite1)
(on_board instrument5 satellite1)
(power_avail satellite1)
(pointing satellite1 GroundStation4)
(supports instrument6 image0)
(supports instrument6 infrared2)
(supports instrument6 spectrograph1)
(calibration_target instrument6 Star3)
(supports instrument7 image0)
(supports instrument7 spectrograph1)
(calibration_target instrument7 GroundStation4)
(supports instrument8 spectrograph1)
(supports instrument8 infrared2)
(supports instrument8 image0)
(calibration_target instrument8 Star3)
(on_board instrument6 satellite2)
(on_board instrument7 satellite2)
(on_board instrument8 satellite2)
(power_avail satellite2)
(pointing satellite2 Star3)
(supports instrument9 infrared2)
(supports instrument9 image0)
(calibration_target instrument9 GroundStation2)
(on_board instrument9 satellite3)
(power_avail satellite3)
(pointing satellite3 Planet7)
(supports instrument10 infrared2)
(supports instrument10 image0)
(calibration_target instrument10 GroundStation4)
(supports instrument11 image0)
(calibration_target instrument11 GroundStation4)
(on_board instrument10 satellite4)
(on_board instrument11 satellite4)
(power_avail satellite4)
(pointing satellite4 Star8)
(supports instrument12 infrared2)
(supports instrument12 spectrograph1)
(supports instrument12 image0)
(calibration_target instrument12 GroundStation1)
(supports instrument13 infrared2)
(calibration_target instrument13 GroundStation4)
(supports instrument14 image0)
(calibration_target instrument14 Star0)
(on_board instrument12 satellite5)
(on_board instrument13 satellite5)
(on_board instrument14 satellite5)
(power_avail satellite5)
(pointing satellite5 Star3)
(supports instrument15 infrared2)
(supports instrument15 image0)
(supports instrument15 spectrograph1)
(calibration_target instrument15 GroundStation4)
(supports instrument16 spectrograph1)
(supports instrument16 infrared2)
(supports instrument16 image0)
(calibration_target instrument16 Star3)
(on_board instrument15 satellite6)
(on_board instrument16 satellite6)
(power_avail satellite6)
(pointing satellite6 GroundStation1)
(supports instrument17 infrared2)
(supports instrument17 spectrograph1)
(supports instrument17 image0)
(calibration_target instrument17 GroundStation4)
(supports instrument18 spectrograph1)
(calibration_target instrument18 GroundStation2)
(on_board instrument17 satellite7)
(on_board instrument18 satellite7)
(power_avail satellite7)
(pointing satellite7 Planet9)
(supports instrument19 spectrograph1)
(supports instrument19 infrared2)
(supports instrument19 image0)
(calibration_target instrument19 Star3)
(on_board instrument19 satellite8)
(power_avail satellite8)
(pointing satellite8 GroundStation4)
(supports instrument20 image0)
(calibration_target instrument20 GroundStation2)
(supports instrument21 spectrograph1)
(calibration_target instrument21 Star0)
(supports instrument22 spectrograph1)
(calibration_target instrument22 GroundStation1)
(on_board instrument20 satellite9)
(on_board instrument21 satellite9)
(on_board instrument22 satellite9)
(power_avail satellite9)
(pointing satellite9 GroundStation1)
(supports instrument23 spectrograph1)
(supports instrument23 infrared2)
(supports instrument23 image0)
(calibration_target instrument23 GroundStation1)
(supports instrument24 spectrograph1)
(supports instrument24 infrared2)
(calibration_target instrument24 Star0)
(supports instrument25 spectrograph1)
(supports instrument25 infrared2)
(calibration_target instrument25 Star3)
(on_board instrument23 satellite10)
(on_board instrument24 satellite10)
(on_board instrument25 satellite10)
(power_avail satellite10)
(pointing satellite10 Phenomenon6)
(supports instrument26 infrared2)
(calibration_target instrument26 GroundStation4)
(supports instrument27 infrared2)
(supports instrument27 spectrograph1)
(calibration_target instrument27 GroundStation1)
(supports instrument28 infrared2)
(supports instrument28 image0)
(calibration_target instrument28 Star0)
(on_board instrument26 satellite11)
(on_board instrument27 satellite11)
(on_board instrument28 satellite11)
(power_avail satellite11)
(pointing satellite11 GroundStation2)
)
(:goal :tasks (
(tag t1 (do_turning satellite0 Planet7))
(tag t2 (do_turning satellite1 Star3))
(tag t3 (do_turning satellite2 Star8))
(tag t4 (do_turning satellite7 Star0))
(tag t5 (do_turning satellite8 Star5))
(tag t6 (do_turning satellite10 Star8))
(tag t7 (do_turning satellite11 GroundStation2))
(tag t8 (do_mission Star5 infrared2))
(tag t9 (do_mission Phenomenon6 infrared2))
(tag t10 (do_mission Planet7 infrared2))
(tag t11 (do_mission Star8 image0))
(tag t12 (do_mission Planet9 infrared2))
)
:constraints(and (after (and
(pointing satellite0 Planet7)
(pointing satellite1 Star3)
(pointing satellite2 Star8)
(pointing satellite7 Star0)
(pointing satellite8 Star5)
(pointing satellite10 Star8)
(pointing satellite11 GroundStation2)
(have_image Star5 infrared2)
(have_image Phenomenon6 infrared2)
(have_image Planet7 infrared2)
(have_image Star8 image0)
(have_image Planet9 infrared2)
) t12) )))
