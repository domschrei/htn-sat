; ./satgen 31555 1 3 3 1 1
(define (problem strips-sat-x-1)
(:domain satellite) (:requirements :strips :typing :negative-preconditions :htn :equality)
(:objects
satellite0 - satellite
instrument0 - instrument
infrared0 - mode
spectrograph2 - mode
spectrograph1 - mode
Star0 - direction
Planet1 - direction
)
(:init
(supports instrument0 spectrograph1)
(supports instrument0 spectrograph2)
(calibration_target instrument0 Star0)
(on_board instrument0 satellite0)
(power_avail satellite0)
(pointing satellite0 Planet1)
)
(:goal :tasks (
(tag t1 (do_mission Planet1 spectrograph2))
)
:constraints(and (after (and
(have_image Planet1 spectrograph2)
) t1) )))
