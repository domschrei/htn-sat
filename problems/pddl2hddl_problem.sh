#!/bin/bash
# Script to convert PDDL files (as used by HTN-SAT / T-REX) to HDDL files (as used by Panda / totSAT)

problemfile="$1"

if [ ! $problemfile ]; then
    echo "Provide a PDDL problem file."
    exit 0
fi

rm tmp_* 2> /dev/null

# Extract initial task network
cat "$problemfile" | grep -E "\(tag t" | grep -oE "\(tag t.*\)" | sed 's/(tag t/(task/g' > tmp_tasks

init=false
goal=false

# Parse PDDL file, write applicable parts into new problem file
while read -r line ; do
    
    if echo $line | grep -q "(:init"; then
        init=true
    elif echo $line | grep -q "(:goal"; then
        init=false
        goal=true
    fi
    
    if $init; then
        # Extract initial state
        echo $line >> tmp_init
    elif $goal; then
        break
    else
        if echo $line | grep -q "(:requirement"; then
            # Adjust requirements definition
            echo "(:requirements :typing :hierachie)" >> tmp_problem
        else
            # Copy content
            echo $line >> tmp_problem
        fi
    fi
    
done < "$problemfile"

# Assemble complete problem file
echo "(:htn :parameters () :ordered-subtasks (and" >> tmp_problem
cat tmp_tasks >> tmp_problem
echo "))" >> tmp_problem
cat tmp_init >> tmp_problem
echo ")" >> tmp_problem

# Convert all types to lower case
cat tmp_problem | sed 's/ - \(.*\)/ - \L\1/' > new_problem.hddl
