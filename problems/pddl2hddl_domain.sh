#!/bin/bash

# Appends certain lines of new_file to old_file.
# All lines whose first word does not occur in old_file yet are appended.
function add_unincluded_lines() {
    old_file="$1"
    new_file="$2"
    if [ ! -s $old_file ]; then
        cp $new_file $old_file
    else
        while read -r param; do
            if ! grep -q `echo $param | awk '{print $1}'`" " "$old_file"; then
                echo $param >> "$old_file"
            fi
        done < "$new_file"
    fi
}

set -e

domainfile="$1"

# Remove comments, remove requirements, convert newlines/tabs into spaces
cat $domainfile | sed 's/^\([ \tA-Za-z0-9():_-]*\);\+.*/\1/g' \
| sed 's/(:requirements.*)/(:requirements)/g' | tr '\t' ' ' \
| tr '\n' ' ' | tr '\r' ' ' | sed 's/ $//g' > tmp1

# Remove multi-spaces, 
# add newline for each expression like "(:this"
cat tmp1 | sed 's/  \+/ /g' | sed 's/\((:[A-Za-z0-9_-]\+\)/\n\1/g' \
| sed 's/( /(/g' | sed 's/ )/)/g' > tmp2

# Convert all types to lower case
cat tmp2 | sed 's/ - \(.*\)/ - \L\1/' | sed 's/:types\(.*\)$/:types.*\L\1/g' > tmp1

# Expand compact typing notations (v1 v2 - t => v1 - t v2 - t)
# as long as necessary
new_file=tmp1
old_file=tmp2
while ! cmp -s tmp1 tmp2 ; do
    cat $old_file | sed 's/\(?[a-z0-9_-]\+\) \(?[a-z0-9_-]\+\) - \([a-z0-9_-]\+\)/\1 - \3 \2 - \3/g' > $new_file
    tmp=$new_file
    new_file=$old_file
    old_file=$tmp
done

# Swap positions of actions and methods, insert TASKS marker
# for later insertion of tasks
grep "(:action" tmp2 > tmp_actions
grep "(:method" tmp2 | sed 's/) \?) \?) \?)/)))/g' > tmp_methods
grep -v "(:method\|(:action" tmp2 > tmp1
cat tmp1 > tmp2
echo "TASKS" >> tmp2
cat tmp_methods tmp_actions >> tmp2
echo ")" >> tmp2

# Swap positioning on subtasks and preconditions, 
# rename identifiers
cat tmp2 | sed 's/\(:expansion.*\)\(:constraints.*\))/\2 \1 )/g' \
| sed 's/:constraints/:precondition/g' \
| sed 's/:expansion \?(\(.*\))/:ordered-subtasks(and \1)/g' > tmp1

# Set correct requirements
cat tmp1 | sed 's/(:requirements)/(:requirements :typing :hierachie)/' > tmp2

# Build task definitions
cat tmp_methods | sed 's/:method/:task/g' \
| sed 's/\(:parameters.*:expansion\).*/\1/g' \
| sed 's/:expansion/)/g' | awk '!a[$0]++' > tmp_tasks

# Insert task definitions
> tmp
methodcount=1
while read -r line; do
    if echo $line | grep -q TASKS; then
        cat tmp_tasks >> tmp
    elif echo $line | grep -q "(:method"; then
        methodname=`echo $line | awk '{print $2}'`
        task=`echo $line | grep -oP ":parameters \(.*?\)" \
        | sed 's/:parameters \?(/:task ('"$methodname "'/' \
        | sed 's/ - [A-Za-z0-9_-]\+//g'`
        echo $line | sed 's/:precondition/'"$task"' :precondition/g' >> tmp
    else
        echo $line >> tmp
    fi
done < tmp2

# Rename methods to guarantee unique identifiers
awk 'BEGIN {c=0}; /:method/ {printf("(:method m%i_", c); for (i=2;i<=NF;i++) printf("%s ", $(i)); printf("\n"); c+=1} !/:method/' tmp > tmp2

# Change "before" constraints into preconditions,
# remove "series", "between", "after" constraints
cat tmp2 | sed 's/(before (and\(.*\)) [a-z0-9_-]\+)/\1/' \
| sed 's/(before \(.*\) [a-z0-9_-]\+)/\1/g' \
| sed 's/(\(series\|between\|after\) [ A-Za-z0-9_-]\+) \?//g' > tmp1

# Remove "tag" from subtask descriptors,
# remove empty conjunctions
cat tmp1 | sed 's/(tag /(/g' | sed 's/( \?and \?)/()/g' > tmp2

# Extract predicates
grep -oE ":predicates.*$" tmp2 | sed 's/(/\n(/g' \
| grep -v ":predicates" | sed 's/(\|)//g' > tmp_predicates

# Build table of predicate parameter types
sed 's/?[a-z0-9_-]\+ - //g' tmp_predicates > tmp_predicate_params

# Build table of all parameter types
> tmp_all_params
cat tmp_tasks tmp_actions | awk '!a[$0]++' > tmp_all_tasks
while read -r line; do
    echo $line | grep -oE " - [a-z0-9_-]+" | grep -oE "[A-Za-z0-9][a-z0-9_-]*" | tr '\n' ' ' > tmp_task_p
    echo $line | awk '{print $2}' > tmp_task_n
    paste tmp_task_{n,p} >> tmp_all_params
done < tmp_all_tasks
sed -i 's/\t/ /g' tmp_all_params
#cat tmp_predicate_params >> tmp_all_params

# Update methods list
grep "(:method" tmp2 > tmp_methods

# Include implicit parameters into the definition of each method
while read -r methodline; do
    
    # Find ALL occurring parameters of the method (signature, tasks, actions, predicates)
    echo $methodline | grep -oE "\([A-Za-z0-9_-]+( \?[A-Za-z0-9_-]+)*\)" \
    | sed 's/(\|)//g' > tmp_params
    
    # Extract explicit parameters of the method
    > tmp_method_params
    echo $methodline | grep -oE ":parameters ?\(( ?\?[A-Za-z0-9_-]+ - [A-Za-z0-9_-]+ ?)* ?\)" \
    | sed 's/(\|)//g' | sed 's/:parameters //g' | sed 's/?/\n?/g' > tmp_method_params
    
    # Join the method's parameter names with their respective types
    sort tmp_params -k1,1 -o tmp_params
    sort tmp_all_params -k1,1 -o tmp_all_params
    join tmp_params tmp_all_params \
    | awk '{num_params=(NF-1)/2; for (i=1; i <= num_params; i++) { print $(i+1),"-",$(i+1+num_params) }}'\
    > tmp_joined_params
    
    # Add found parameters, if not already specified as explicit parameters
    add_unincluded_lines tmp_method_params tmp_joined_params
    
    # Also consider the predicate's params, if some parameter 
    # only occurs in predicates, but not in tasks/actions
    sort tmp_predicate_params -k1,1 -o tmp_predicate_params
    join tmp_params tmp_predicate_params \
    | awk '{num_params=(NF-1)/2; for (i=1; i <= num_params; i++) { print $(i+1),"-",$(i+1+num_params) }}'\
    > tmp_joined_params
    
    # Add found parameters, if not already specified as explicit parameters
    add_unincluded_lines tmp_method_params tmp_joined_params
    
    # Insert full parameter list into method definition
    params_to_insert=`cat tmp_method_params | tr '\n' ' '`
    new_methodline=`echo $methodline \
    | sed 's/:parameters \?([ ?A-Za-z0-9_-]*)/:parameters ('"$params_to_insert"')/g'`
    cat tmp2 | sed 's/'"$methodline"'/'"$new_methodline"'/' > tmp
    cp tmp tmp2
    
    # debug a single method with this
    #if echo $methodline | grep -q "m7_do_start_throw_all"; then
    #    exit 0
    #fi
    
done < tmp_methods

# Make file readable by adding linebreaks
sed -i 's/(:/\n(:/g' tmp2
sed -i 's/ :/\n  :/g' tmp2

# Tidy up
cp tmp2 new_domain.hddl
rm tmp* 2> /dev/null
